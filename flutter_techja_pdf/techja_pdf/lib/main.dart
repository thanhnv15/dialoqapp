import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pspdfkit_flutter/pspdfkit.dart';

const String _documentPath = 'assets/file.pdf';

void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _frameworkVersion = '';

  Future<String> prepareTestPdf() async {
    final ByteData bytes =
        await DefaultAssetBundle.of(context).load(_documentPath);
    final Uint8List list = bytes.buffer.asUint8List();

    final tempDir = await getTemporaryDirectory();
    final tempDocumentPath = '${tempDir.path}/$_documentPath';

    final file = await File(tempDocumentPath).create(recursive: true);
    file.writeAsBytesSync(list);
    return tempDocumentPath;
  }

  @override
  initState() {
    super.initState();
    initPlatformState();
  }

  void initPlatformState() async {
    String frameworkVersion;
    try {
      frameworkVersion = await Pspdfkit.frameworkVersion;
    } on PlatformException {
      frameworkVersion = 'Failed to get platform version. ';
    }

    if (!mounted) return;

    setState(() {
      _frameworkVersion = frameworkVersion;
    });

    // Replace
    Pspdfkit.setLicenseKey(
        "KZ12mS6G85OS84zBPmskUn8d4JdJxXN__Q9tqfie5jO89viO408ST3eet-kdpdaL6l1p6pIX6fAWZvCphtx4x9qrTi3qV2wika02DUwji7qNAz5QK3UsSFBjJZnEOFph7NoABcCpZ9d0tjZ3fjCPDbUiX_GxQ7XfrObaUtPyve_kTuOca-bKkb5EuXlKGb_HnZFetW_8WnXzq-UIhhR4O-UqjQEgYBQHHZBimc9ngHXvJP-NhJRJvEn0a4zNHo71tVRd95V2SpxHAM6DiW85_PV2JDSLNLKr9BPfIpy65cT6wkrIFEgzJR_A7igPZpPqCoA1188pu66NMwLVhBDlo-7wB9jlVG6TFh4L3x-lFi738qu76qCt5zNRbDgh6Qmtr3fgjFLRc8ATznOINJcczOBB39iA5QknNz1FI6LC_q1P5Bf9O56kle5lU7kRYYaD");
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);

    return new MaterialApp(
      home: new Scaffold(
          appBar: new AppBar(
            title: new Text('PSPDFKit Flutter Plugin example app'),
          ),
          body: Builder(
            builder: (BuildContext context) {
              return Center(
                  child: new Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                    new Text('PSPDFKit for $_frameworkVersion\n',
                        style: themeData.textTheme.display1
                            .copyWith(fontSize: 21.0)),
                    new RaisedButton(
                        child: new Text('Tap to Open Document',
                            style: themeData.textTheme.display1
                                .copyWith(fontSize: 21.0)),
                        onPressed: () => prepareTestPdf().then((path) {
                              Pspdfkit.present(path);
                            }))
                  ]));
            },
          )),
    );
  }
}
