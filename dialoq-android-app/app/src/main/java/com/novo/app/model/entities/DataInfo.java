package com.novo.app.model.entities;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataInfo implements Comparable<DataInfo>, Serializable {
    private String name;
    @SerializedName("result")
    private ResultInfo result;
    @SerializedName("scheduleId")
    private String scheduleId;
    @SerializedName("activityId")
    private String activityId;
    @SerializedName("activityDate")
    private String activityDate;
    @SerializedName("activityStatus")
    private String activityStatus;
    @SerializedName("adherenceStatus")
    private String adherenceStatus;

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(String activityDate) {
        this.activityDate = activityDate;
    }

    public String getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }

    public String getAdherenceStatus() {
        return adherenceStatus;
    }

    public void setAdherenceStatus(String adherenceStatus) {
        this.adherenceStatus = adherenceStatus;
    }

    public ResultInfo getResult() {
        return result;
    }

    public void setResult(ResultInfo result) {
        this.result = result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(@NonNull DataInfo o2) {
        return -result.getReportedDate().compareTo(o2.result.getReportedDate());
    }
}