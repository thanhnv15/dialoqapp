package com.novo.app.view.widget;

public final class NVDateFormat {

    public static final String TIME_RANGE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";//2019-01-10T07:35:43+0000

    public static final String GROUPED_DATE_FORMAT_FULL = "EEEE, MMMM dd, yyyy";//Sunday, January 06, 2019

    public static final String LAST_SYNC_FORMAT_FULL = "EEEE, MMMM dd 'at' hh:mm a";

    public static final String LAST_SYNC_FORMAT_SIMPLE = "MMMM dd 'at' hh:mm a";

    public static final String GROUPED_DATE_FORMAT_SIMPLE = "MMMM dd, yyyy";

    public static final String TIME_SIMPLE_FORMAT = "hh:mm a"; //01:30 PM

    public static final String TIME_SIMPLE_FORMAT_24 = "HH:mm"; //13:30

    public static final String DOSE_DETAILS_TIME_FORMAT = "EEEE, MMMM dd, yyyy 'at' hh:mm a";
    public static final String DOSE_DETAILS_TIME_FORMAT_24H = "EEEE, MMMM dd, yyyy 'at' HH:mm";

    public static final String DOSE_DETAILS_TIME_SIMPLE_FORMAT = "'at' hh:mm a";
    public static final String DOSE_DETAILS_TIME_SIMPLE_FORMAT_24H = "'at' HH:mm";

    public static final String NOTE_DATE_FORMAT_FULL = "EEE, MMMM dd, yyyy";//Mon, January 06, 2019

    public static final String LAST_UPDATED_TIME_FORMAT = "yyyy-MM-dd";//2019-01-10

    public static final String SHARE_PDF_ST_FORMAT = "MMM d'st', hh:mm a";//Feb 1st, 14:03

    public static final String SHARE_PDF_ND_FORMAT = "MMM d'nd', hh:mm a";//Feb 2nd, 14:03

    public static final String SHARE_PDF_RD_FORMAT = "MMM d'rd', hh:mm a";//Feb 3rd, 14:03

    public static final String SHARE_PDF_TH_FORMAT = "MMM d'th', hh:mm a";//Feb 4th, 14:03

    public static final String MANDATORY_ACCEPTED_FORMAT = "MM/dd/yyyy";//07/28/ 2019

    private NVDateFormat() {

    }
}
