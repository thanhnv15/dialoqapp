package com.novo.app.view.dialog;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.utils.ColorTemplate;
import com.novo.app.R;
import com.novo.app.presenter.M015ChartDetailsPresenter;
import com.novo.app.view.base.BaseDialog;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.event.OnM015ChartDetailsCallBack;
import com.novo.app.view.widget.novocharts.NovoDetailsChart;
import com.novo.app.view.widget.novocharts.configs.NovoBarDetailsConfig;

public class M015ChartDetailsDialog extends BaseDialog<M015ChartDetailsPresenter, OnM015ChartDetailsCallBack> implements OnCallBackToView {
    private static final String TAG = M015ChartDetailsDialog.class.getName();
    private static final int TRESIBA_COLOR_TYPE = -3547901;
    private static final int FIASP_COLOR_TYPE = -1688529;
    private static final int BOTH_COLOR_TYPE = -11113455;
    private int colorType;
//    private String drugName = "";
    private String drugDescription;
    //  private String drugDate = "22 June 2018";
    private TextView tvDrugName;
    private TextView tvDrugDescription;
    private TextView tvDrugDate;
    private NovoDetailsChart novoDetailsChart;
    private String mBarColor = "e63c2f";
    private String mValueColor = "566c11";

    private Context context;
    private Spannable span;

    public M015ChartDetailsDialog(Context context, boolean isCancel, int colorType) {
        super(context, isCancel, R.style.dialog_style);

        this.context = context;

        this.colorType = colorType;
    }

    @Override
    public void showM001LandingScreen(String err) {
        mCallBack.showM001LandingScreen(err);
    }

    @Override
    protected M015ChartDetailsPresenter getPresenter() {
        return new M015ChartDetailsPresenter(this);
    }

    @Override
    protected void initViews() {
        novoDetailsChart = findViewById(R.id.tv_m015_chart_detail);
        findViewById(R.id.iv_m015_exit, this);
        tvDrugName = findViewById(R.id.tv_m015_drug_name);
        tvDrugDescription = findViewById(R.id.tv_m015_drug_description);
        tvDrugDate = findViewById(R.id.tv_m015_drug_date);
    }

    public void setUpChart() {
        String drugName;
        NovoBarDetailsConfig config = new NovoBarDetailsConfig();
        config.setBothType(false);

        switch (colorType) {
            case TRESIBA_COLOR_TYPE:
                drugName = "Tresiba®";
                span = new SpannableString(drugName);
                span.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorM014Lime)), 0, drugName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                span.setSpan(new StyleSpan(Typeface.BOLD), 0, drugName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                drugDescription = "Short-acting Insulin";
                config.setFirstBarColor(ColorTemplate.rgb("c9dd03"));
                config.setFirstValueColor(ColorTemplate.rgb(mValueColor));
                break;
            case FIASP_COLOR_TYPE:
                drugName = "Fiasp®";
                span = new SpannableString(drugName);
                span.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorFiasp)), 0, drugName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                span.setSpan(new StyleSpan(Typeface.BOLD), 0, drugName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                drugDescription = "Rapid-acting Insulin";

                config.setFirstBarColor(ColorTemplate.rgb(mBarColor));
                config.setFirstValueColor(ColorTemplate.rgb(mValueColor));
                break;
            case BOTH_COLOR_TYPE:
                drugName = "Tresiba® + Fiasp";
                span = new SpannableString(drugName);
                span.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorM015CamouflageGreen)), 0, 8, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                span.setSpan(new StyleSpan(Typeface.BOLD), 0, drugName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                span.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorM015DarkBlue)), 9, 10, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                span.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorFiasp)), 11, drugName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                drugDescription = "";
                config.setBothType(true);
                config.setFirstBarColor(ColorTemplate.rgb(mValueColor));
                config.setSecondBarColor(ColorTemplate.rgb(mBarColor));
                config.setFirstValueColor(ColorTemplate.rgb(mValueColor));
                config.setSecondValueColor(ColorTemplate.rgb(mBarColor));
                break;
        }

        novoDetailsChart.setMockData(config);
    }

    public void setUpInfoChart() {
        tvDrugName.setText(span);

        if (drugDescription.isEmpty()) {
            tvDrugDescription.setVisibility(View.GONE);
        } else {
            tvDrugDescription.setText(drugDescription);
        }

        String drugDate = "22 June 2018";

        tvDrugDate.setText(drugDate);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    public int getLayoutId() {
        return R.layout.view_m015_chart_detail;
    }

    @Override
    public void backToPreviousScreen() {

    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m015_exit:
                dismiss();
                break;
            default:
                break;
        }
    }
}
