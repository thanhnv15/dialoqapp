/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.presenter;

import com.novo.app.manager.CADBManager;
import com.novo.app.manager.entities.RDoseEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM062AddNoteCallBack;

public class M062AddNotePresenter extends BasePresenter<OnM062AddNoteCallBack> {

    private static final String TAG = M062AddNotePresenter.class.getName();

    public M062AddNotePresenter(OnM062AddNoteCallBack event) {
        super(event);
    }

    public void addNewNoteToDB(String assessmentType, String notes, String dateNote) {
        RDoseEntity note = new RDoseEntity();
        note.setAssessmentType(assessmentType);
        note.setNotes(notes);
        note.setStatusFlag(RDoseEntity.STATE_ADD);
        note.setReportedDate(dateNote);
        note.setCustom1(System.currentTimeMillis() + "");
        note.setCustom2(System.currentTimeMillis() + "");

        boolean result = CADBManager.getInstance().addRDoseEntity(note, assessmentType);
        if (result) {
            mListener.showDoseLog("OK");
        } else {
            mListener.showAddNoteFail();
        }

        CommonUtil.wtfi(TAG, "addNewNoteToDB...." + result);
    }
}
