package com.novo.app.view.event;

/**
 * Created by LinhNT50 on 03/08/2019
 * Callbacks for updating view in when user input email to get confirmation email
 */
public interface OnM040RecoveryCallBack extends OnCallBackToView {
    /**
     * Request View instance to show error message
     *
     * @param message is the error message to be displayed
     */
    void onIA04Fail(String message);

    /**
     * Request View instance to show the home screen after successfully logged in with the new password
     */
    void loginSuccess();

    /**
     * Called requesting view instance to show a dialog with some text
     *
     * @param textCode string id of message text which will be shown
     * @param TAG      designated tag for the dialog
     */
    void showDialog(int textCode, String TAG);

    /**
     * Called requesting view instance to show a dialog with some text
     *
     * @param text string id of message text which will be shown
     * @param TAG  designated tag for the dialog
     */
    void showDialog(String text, String TAG);

    /**
     * Show login result with invalid app's version
     *
     * @param isActive state of installed app's version, possible states are:
     * @see com.novo.app.utils.CommonUtil#RECALL_VERSION,
     * @see com.novo.app.utils.CommonUtil#RECALL_COUNTRY, and
     * @see com.novo.app.utils.CommonUtil#RECALL_VERSION
     */
    default void showM001LandingFrg(int recallCode) {

    }

    /**
     * Request View instance to show error message when can not login with the new password
     * @param code is the request code getting from the response
     */
    default void wrongAccount(String code) {
    }
}
