package com.novo.app.presenter;

import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.model.entities.TopicEntity;
import com.novo.app.view.event.OnM048SupportVideoCallBack;

import java.util.ArrayList;
import java.util.List;

public class M048SupportVideoPresenter extends BasePresenter<OnM048SupportVideoCallBack> {
    private List<TextEntity> entityList = new ArrayList<>();
    private static final String TYPE_VALUE = "VideoType";

    public M048SupportVideoPresenter(OnM048SupportVideoCallBack event) {
        super(event);
    }

    public List<TextEntity> getData(){
        List<TopicEntity> videoData = getStorage().getM001ConfigSet().getListVideos();
        for (int i = 0; i < videoData.size(); i++ ){
            TextEntity typeEntity = new TextEntity(videoData.get(i).getTopicKey(), TYPE_VALUE);
            entityList.add(typeEntity);
            for (int j = 0; j < videoData.get(i).getListQnA().size(); j++){
                String videoName = LangMgr.getInstance().getLangList().get(videoData.get(i).getListQnA().get(j).getKey());
                String videoURL = LangMgr.getInstance().getLangList().get(videoData.get(i).getListQnA().get(j).getValue());
                TextEntity videoEntity = new TextEntity(videoName, videoURL);
                entityList.add(videoEntity);
            }
        }
        return entityList;
    }
}
