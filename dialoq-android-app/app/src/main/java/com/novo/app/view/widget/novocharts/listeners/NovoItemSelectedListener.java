package com.novo.app.view.widget.novocharts.listeners;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;

public interface NovoItemSelectedListener {
    void onNovoItemSelected(int colorType, BarData mData, Entry entry, Highlight highlight);
}
