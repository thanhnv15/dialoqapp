package com.novo.app.view.fragment;

import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.presenter.M019SignUpPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.CASpinnerAdapter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM019SignUpCallBack;
import com.novo.app.view.event.OnSpinnerCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

import java.util.ArrayList;
import java.util.List;

public class M019SignUpHowLongFrg extends BaseFragment<M019SignUpPresenter, OnHomeBackToView> implements OnM019SignUpCallBack, OnSpinnerCallBack {

    public static final String TAG = M019SignUpHowLongFrg.class.getName();
    private static final int KEY_SPINNER_DIABETES_YEAR_TYPE = 5;
    private Button mBtNext;
    private TextView mTvHowLong;
    private Spinner mSpinnerHowLong;

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m019_back, this);
        findViewById(R.id.tv_m019_profile_setup, CAApplication.getInstance().getBoldFont());
        TextView mTvDescription = findViewById(R.id.tv_m019_description, CAApplication.getInstance().getRegularFont());
        if (!getStorage().isMature())
            mTvDescription.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m019_description_immature)));
        mBtNext = findViewById(R.id.bt_m019_next, this, CAApplication.getInstance().getMediumFont());
        mBtNext.setEnabled(false);
        findViewById(R.id.tv_m019_not_now, this, CAApplication.getInstance().getMediumFont());

        mTvHowLong = findViewById(R.id.tv_m019_how_long, this, CAApplication.getInstance().getRegularFont());

        if (getStorage().getM001ProfileEntity().getYearDiabetes() != null && !getStorage().getM001ProfileEntity().getYearDiabetes().getValue().equals("")) {
            mTvHowLong.setText(getStorage().getM001ProfileEntity().getYearDiabetes().getValue());
            mTvHowLong.setTypeface(CAApplication.getInstance().getBoldFont());
            mBtNext.setEnabled(true);
        }

        findViewById(R.id.iv_m019_how_long, this);

        mSpinnerHowLong = findViewById(R.id.spin_m019_how_long);
        CASpinnerAdapter<OnSpinnerCallBack> adapter = new CASpinnerAdapter<>(KEY_SPINNER_DIABETES_YEAR_TYPE,
                prepareListData(), mContext);
        adapter.setOnSpinnerCallBack(this);

        mTvHowLong.setTag(getStorage().getM001ProfileEntity().getYearDiabetes());
        mSpinnerHowLong.setAdapter(adapter);
    }

    private List<TextEntity> prepareListData() {
        List<TextEntity> data = getStorage().getM001ConfigSet().getYearEntity();
        List<TextEntity> listData = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            String key = data.get(i).getKey();
            String value = LangMgr.getInstance().getLangList().get(data.get(i).getValue());
            listData.add(new TextEntity(key, value));
        }
        return listData;
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m019_signup_how_long;
    }

    @Override
    protected M019SignUpPresenter getPresenter() {
        return new M019SignUpPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //Wait for customer confirmation
    }

    @Override
    public void backToPreviousScreen() {
        mCallBack.showFrgScreen(TAG, M008SignUpDiabetesTypeFrg.TAG);
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m019_next:
                getStorage().getM001ProfileEntity().setYearDiabetes(((TextEntity) mTvHowLong.getTag()));
                mCallBack.showFrgScreen(TAG, M021SignUpProfileThinkFrg.TAG);
                break;
            case R.id.tv_m019_not_now:
                getStorage().getM001ProfileEntity().setYearDiabetes(new TextEntity("", ""));
                mCallBack.showFrgScreen(TAG, M021SignUpProfileThinkFrg.TAG);
                break;
            case R.id.iv_m019_back:
                backToPreviousScreen();
                break;
            case R.id.tv_m019_how_long:
            case R.id.iv_m019_how_long:
                mSpinnerHowLong.performClick();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(int key, int pos, Object data) {
        if (key == KEY_SPINNER_DIABETES_YEAR_TYPE) {
            TextEntity entity = ((List<TextEntity>) data).get(pos);
            mTvHowLong.setText(entity.getValue());
            mTvHowLong.setTag(entity);

            mTvHowLong.setTypeface(CAApplication.getInstance().getBoldFont());
            mBtNext.setEnabled(true);
            closeSpinner(mSpinnerHowLong);
        }
    }
}
