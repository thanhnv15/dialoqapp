package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM042MoreCallBack;
import com.novo.app.view.event.OnM065ReminderCallBack;
import com.novo.app.view.widget.ProgressLoading;

import static com.novo.app.utils.CommonUtil.P_API_KEY;

public class M042MorePresenter extends BasePresenter<OnM042MoreCallBack> implements OnM065ReminderCallBack {
    private static final String KEY_API_AS23_DELETE_RESULT = "KEY_API_AS23_DELETE_RESULT";
    private static final String URL_API_AS23_DELETE_RESULT = CommonUtil.HOT_URL + "/api/v1/users/%s/assessments/%s/results/%s?";

    private static final String TAG = M042MorePresenter.class.getName();

    private String assessmentId;
    private DeviceEntity mEntity;

    public M042MorePresenter(OnM042MoreCallBack event) {
        super(event);
    }

    @Override
    protected void handleSuccess(String data, String tag) {
        if (KEY_API_AS23_DELETE_RESULT.equals(tag)) {
            prepareAS23(data);
        }
    }

    void prepareAS23(String data) {
        CommonUtil.wtfd(TAG, "Data: " + data);
        if (mEntity == null || !mEntity.isTresiba()) {
            ProgressLoading.dismiss();
            mListener.closeDeviceDetail();
            return;
        }

        for (int i = 0; i < getStorage().getM025ListDevices().size(); i++) {
            String assessmentIdDevices = getStorage().getM025ListDevices().get(i).getAssessmentId();
            if (assessmentIdDevices != null && assessmentIdDevices.equals(this.assessmentId)) {
                M065AddEditReminderPresenter m065AddEditPresenter = new M065AddEditReminderPresenter(this);
                m065AddEditPresenter.delReminder();

                mListener.closeDeviceDetail();
                return;
            }
        }
    }

    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tagRequest);
        ProgressLoading.dismiss();

        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        String sms = LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON);

        if (responseEntity != null) {
            sms = responseEntity.getErrorEntity()[0].getMessage();
        }

        if (KEY_API_AS23_DELETE_RESULT.equals(tag)) {
            mListener.showAlertDialog(sms);
        }
    }

    @Override
    public void hideLockDialog(String key) {
        //super.hideLockDialog(key);
    }

    public void callAS23DeleteAssessmentResult(DeviceEntity entity) {
        mEntity = entity;
        CommonUtil.wtfi(TAG, "callAS23DeleteAssessmentResult ");
        assessmentId = getAssessmentId(CommonUtil.DEVICE_ASSESSMENT_TYPE) + "";

        CARequest request = new CARequest(TAG, CARequest.METHOD_DELETE);
        request.addTAG(KEY_API_AS23_DELETE_RESULT);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, "token: " + token);
        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_AS23_DELETE_RESULT, getStorage().getM007UserEntity().getUserId(),
                assessmentId, entity.getResultId() + ""));
        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    @Override
    public void closeDialog(String key) {
        ProgressLoading.dismiss();
        CommonUtil.getInstance().cancelAppReminder(false, CAApplication.getInstance(), null);
    }

    @Override
    public void toM051More() {
        // do nothing
    }
}
