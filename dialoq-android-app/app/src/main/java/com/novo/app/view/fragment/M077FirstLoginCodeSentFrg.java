/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.view.fragment;

import android.text.Editable;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableRow;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M077FirstLoginPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM077FirstLoginCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.ProgressLoading;
import com.novo.app.view.widget.TextAdapter;

import static com.novo.app.presenter.BasePresenter.ERR_NO_CONNECTION;

/**
 * This fragment is visible to user after request sending confirmation code to user's email got
 * succeed
 *
 * This screen simply contains a text box for user to input received confirmation code, in order
 * to continue logging in
 *
 * This acts as `View` object in MVP pattern
 * Linked `Presenter` is an instance of {@link M077FirstLoginPresenter}
 * Linked `Presenter` will communicate back to this `View` via protocol of {@link OnM077FirstLoginCallBack}
 */
public class M077FirstLoginCodeSentFrg extends BaseFragment<M077FirstLoginPresenter, OnHomeBackToView> implements OnM077FirstLoginCallBack {

    // String tag for this class
    public static final String TAG = M077FirstLoginCodeSentFrg.class.getName();

    // View components in this fragment
    private EditText mEdtVerificationCode;
    private Button mBtVerify;
    private TableRow mTrError;

    // Variable represents length of confirmation code
    private int codeLength = 6;

    /**
     * Initialize fragment's content views
     */
    @Override
    protected void initViews() {
        codeLength = Integer.parseInt(getStorage().getM001ConfigSet().getConfirmCode());
        findViewById(R.id.iv_m077_sent);
        ImageView mIvBack = findViewById(R.id.iv_m077_back, this);
        findViewById(R.id.tv_m077_code_sent, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m077_description, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m077_didnt_get, this, CAApplication.getInstance().getMediumFont());

        mBtVerify = findViewById(R.id.bt_m077_verify, this, CAApplication.getInstance().getMediumFont());
        mBtVerify.setEnabled(false);

        mTrError = findViewById(R.id.tr_m077_error);
        mTrError.setVisibility(View.GONE);

        mEdtVerificationCode = findViewById(R.id.tv_m077_detail, CAApplication.getInstance().getRegularFont());
        mEdtVerificationCode.setFilters(new InputFilter[]{new InputFilter.LengthFilter(codeLength)});
        if (getStorage().getM001ProfileEntity().getVerifyCode() != null) {
            mEdtVerificationCode.setText(getStorage().getM001ProfileEntity().getVerifyCode());
            mEdtVerificationCode.setTypeface(CAApplication.getInstance().getBoldFont());
        }
        checkFalseVerifyCode();
        mEdtVerificationCode.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                mTrError.setVisibility(View.GONE);
                mEdtVerificationCode.setTextColor(getResources().getColor(R.color.colorBlack));
                if (mEdtVerificationCode.getText().length() < codeLength) {
                    mBtVerify.setEnabled(false);
                } else {
                    validateCode();
                }
            }
        });

        if (getStorage().getM077VerifyFail()) {
            getStorage().setM077VerifyFail(false);
            mTrError.setVisibility(View.VISIBLE);
            mIvBack.setVisibility(View.VISIBLE);
            mEdtVerificationCode.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
        }

    }

    /**
     * Check if previously `request to verify code` got failed, then show up error message
     * when back to this fragment
     */
    private void checkFalseVerifyCode() {
        String message = getStorage().getM078ErrorMessage();
        if (message != null){
            mTrError.setVisibility(View.VISIBLE);
            mEdtVerificationCode.setText(getStorage().getM077VerifyCode());
            mEdtVerificationCode.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
        }
    }

    /**
     * Handle message from {@link M077FirstLoginPresenter}
     * @see OnM077FirstLoginCallBack#showM001LandingScreen(String)
     */
    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    /**
     * Validate inputted code is in valid format
     * And updating UI corresponding to validation status
     */
    private void validateCode() {
        if (mEdtVerificationCode.getText().toString().isEmpty()) {
            mBtVerify.setEnabled(false);
            mEdtVerificationCode.setTextColor(getResources().getColor(R.color.colorBlack));
            mTrError.setVisibility(View.GONE);
        } else if (mEdtVerificationCode.getText().length() == codeLength) {
            mBtVerify.setEnabled(true);
        }
    }

    /**
     * Layout id which will be inflated to be content of this fragment
     * @return layout id
     */
    @Override
    protected int getLayoutId() {
        return R.layout.frg_m077_first_login_code_sent;
    }

    /**
     * Get linked `Presenter` object
     * @return linked {@link M077FirstLoginPresenter}
     */
    @Override
    protected M077FirstLoginPresenter getPresenter() {

        return new M077FirstLoginPresenter(this);
    }

    /**
     * Get string tag of this class
     * @return string tag
     */
    @Override
    protected String getTAG() {
        return TAG;
    }

    /**
     * Do nothing with 'Back' key
     */
    @Override
    protected void defineBackKey() {
        //Wait for customer confirmation
    }

    /**
     * Handle message from linked `Presenter` object
     *
     * This define `previous screen` of this fragment is {@link M076FirstLoginCodeFrg}
     * And back to that screen when requested by `Presenter`
     */
    @Override
    public void backToPreviousScreen() {
        mCallBack.showFrgScreen(TAG, M076FirstLoginCodeFrg.TAG);
    }

    /**
     * Handle onclick event triggered from view components of this fragment
     * @param idView id of clicked view
     */
    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m077_verify:
                ProgressLoading.dismiss();
                if (CommonUtil.getInstance().isConnectToNetwork()) {
                    getStorage().setM077VerifyCode(mEdtVerificationCode.getText().toString());
                    mCallBack.showFrgScreen(TAG, M078FirstLoginVerifyingCodeFrg.TAG);
                }else {
                    showAlertDialog(LangMgr.getInstance().getLangList().get(ERR_NO_CONNECTION));
                }
                break;
            case R.id.tv_m077_didnt_get:
                CommonUtil.getInstance().forceHideKeyBoard(mEdtVerificationCode);
                mCallBack.showFrgScreen(TAG, M080FirstLoginDidntGetFrg.TAG);
                break;

            case R.id.iv_m077_back:
                CommonUtil.getInstance().forceHideKeyBoard(mEdtVerificationCode);
                backToPreviousScreen();
                break;
            default:
                break;
        }
    }

    /**
     * Handle message from linked `Presenter` object
     * show login screen
     */
    @Override
    public void showM023Login() {
        mCallBack.showFrgScreen(TAG, M023LoginFrg.TAG);
    }
}
