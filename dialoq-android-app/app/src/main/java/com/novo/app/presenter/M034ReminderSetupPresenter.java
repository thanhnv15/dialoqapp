package com.novo.app.presenter;

import com.novo.app.view.event.OnM034ReminderSetupCallBack;

public class M034ReminderSetupPresenter extends BasePresenter<OnM034ReminderSetupCallBack> {
    public M034ReminderSetupPresenter(OnM034ReminderSetupCallBack event) {
        super(event);
    }
}
