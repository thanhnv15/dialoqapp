package com.novo.app.presenter;

import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RReminderEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM041ReminderAddCallBack;
import com.novo.app.view.widget.ProgressLoading;

import java.util.Locale;

public class M041ReminderAddPresenter extends BasePresenter<OnM041ReminderAddCallBack> {
    private static final String TAG = M041ReminderAddPresenter.class.getName();

    public M041ReminderAddPresenter(OnM041ReminderAddCallBack event) {
        super(event);
    }

    private void prepareAS02() {
        CommonUtil.wtfi(TAG, "prepareAS02...");
        RReminderEntity entity = CADBManager.getInstance().getRReminderEntity();

        getStorage().setM065ScheduleEntity(entity);
        String[] reminder = entity.getStartTime().split(":");

        CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_HOUR, Integer.valueOf(reminder[0]));
        CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_MIN, Integer.valueOf(reminder[1]));
        mListener.reminderAdded();
    }

    private void prepareAS10() {
        CommonUtil.wtfi(TAG, "prepareAS10...");
        RReminderEntity entity = CADBManager.getInstance().getRReminderEntity();
        if (entity == null) {
            mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            ProgressLoading.dismiss();
            return;
        }
        String[] reminder = entity.getStartTime().split(":");

        CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_HOUR, Integer.valueOf(reminder[0]));
        CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_MIN, Integer.valueOf(reminder[1]));
        getStorage().setM065ScheduleEntity(entity);
        mListener.reminderAdded();
    }

    public void addReminder(String time) {
        CommonUtil.wtfi(TAG, "addReminder");
        String scheduleNotificationId = getStorage().getM065ScheduleEntity() == null ? "" :
                getStorage().getM065ScheduleEntity().getScheduleNotificationId();

        RReminderEntity entity = new RReminderEntity();
        CommonUtil.getInstance().prepareReminder(entity, time, scheduleNotificationId);
        entity.setStatusFlag(RReminderEntity.STATE_ADD);

        if (CADBManager.getInstance().addRReminderEntity(entity)) {
            prepareAS02();
        }
    }

    public void activeReminder() {
        String time = String.format(Locale.getDefault(), "%02d:%02d",
                CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_HOUR, -1),
                CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_MIN, -1));
        String scheduleNotificationId = getStorage().getM065ScheduleEntity()
                .getScheduleNotificationId();

        RReminderEntity entity = CADBManager.getInstance().getRReminderEntity();
        CommonUtil.wtfi(TAG, "cancelReminder: RReminderEntity " + entity);
        if (entity == null) {
            return;
        }
        CommonUtil.getInstance().prepareReminder(entity, time, scheduleNotificationId);

        if (CADBManager.getInstance().editRReminderEntity(entity)) {
            prepareAS10();
        }
    }
}
