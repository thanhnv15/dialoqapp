/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.manager.entities;

import io.realm.RealmObject;

public class RTokenEntity extends RealmObject {

    private String userName;
    private String password;
    private String dataIA02;
    private String dataUN09;
    private String dataUN07;
    private String dataCP03;
    private String dataCP02;
    private String dataCP01;
    private String configSet;

    public String getDataIA02() {
        return dataIA02;
    }

    public void setDataIA02(String dataIA02) {
        this.dataIA02 = dataIA02;
    }

    public String getDataUN09() {
        return dataUN09;
    }

    public void setDataUN09(String dataUN09) {
        this.dataUN09 = dataUN09;
    }

    public String getDataUN07() {
        return dataUN07;
    }

    public void setDataUN07(String dataUN07) {
        this.dataUN07 = dataUN07;
    }

    public String getDataCP03() {
        return dataCP03;
    }

    public void setDataCP03(String dataCP03) {
        this.dataCP03 = dataCP03;
    }

    public String getDataCP02() {
        return dataCP02;
    }

    public void setDataCP02(String dataCP02) {
        this.dataCP02 = dataCP02;
    }

    public String getDataCP01() {
        return dataCP01;
    }

    public void setDataCP01(String dataCP01) {
        this.dataCP01 = dataCP01;
    }

    public String getConfigSet() {
        return configSet;
    }

    public void setConfigSet(String configSet) {
        this.configSet = configSet;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
