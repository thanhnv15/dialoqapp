package com.novo.app.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.view.event.OnM042MoreCallBack;

import java.util.List;

public class M042MoreDevicesAdapter extends BaseRecycleAdapter<OnM042MoreCallBack, DeviceEntity, M042MoreDevicesAdapter.DeviceHolder> {

    public static final String KEY_LAST_USED = "MORE.DEVICE.LAST.USED";
    private static final String KEY_DEVICE_PAIRED = "MORE.DEVICE.PAIRED";

    public M042MoreDevicesAdapter(Context mContext, List<DeviceEntity> mListData, OnM042MoreCallBack mCallBack) {
        super(mContext, mListData, mCallBack);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_m042_device;
    }

    @Override
    protected DeviceHolder getViewHolder(int viewType, View itemView) {
        return new DeviceHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        DeviceHolder item = (DeviceHolder) holder;
        DeviceEntity data = mListData.get(position);

        item.tvDeviceID.setText(data.getDeviceID());
        item.tvTime.setText(String.format("%s ", data.getLastPair()));
        item.tvDrug.setText(data.getDrugType());
        if (data.getLastUse() != null && !data.getLastUse().isEmpty()) {
            item.tvLastUse.setText(LangMgr.getInstance().getLangList().get(KEY_LAST_USED));
            item.tvTime.setText(String.format("%s with ", data.getLastUse()));
        } else {
            item.tvLastUse.setText(LangMgr.getInstance().getLangList().get(KEY_DEVICE_PAIRED));
        }

        if (!data.isLowBattery()){
            item.ivBattery.setVisibility(View.GONE);
        }else{
            item.ivBattery.setVisibility(View.VISIBLE);
        }

        switch (data.getDrugType()) {
            case DeviceEntity.FIASP:
                item.ivDrug.setImageResource(R.drawable.ic_fiasp);
                item.tvDrug.setText(mContext.getString(R.string.txt_fiasp));
                break;
            case DeviceEntity.TRESIBA:
                if (data.getDrugCode() == DeviceEntity.CODE_TRESIBA_100) {
                    item.ivDrug.setImageResource(R.drawable.ic_tresiba);
                } else {
                    item.ivDrug.setImageResource(R.drawable.ic_m012_drug_t);
                }
                item.tvDrug.setText(mContext.getString(R.string.txt_tresiba));
                break;
            case DeviceEntity.NO_DOSE:
            case DeviceEntity.UNKNOWN:
                item.ivDrug.setVisibility(View.GONE);
                if (data.getLastUse() != null && !data.getLastUse().isEmpty()) {
                    item.tvDrug.setText(mContext.getString(R.string.txt_unknown));
                } else {
                    item.tvDrug.setVisibility(View.GONE);
                }

                break;
        }
    }

    public void setData(List<DeviceEntity> mListData) {
        this.mListData = mListData;
        notifyDataSetChanged();
    }

    public class DeviceHolder extends BaseHolder {
        TextView tvDeviceID;
        TextView tvTime;
        TextView tvLastUse;
        TextView tvDrug;
        ImageView ivArrow;
        ImageView ivBattery;
        ImageView ivDrug;

        DeviceHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void initView() {
            tvDeviceID = findViewById(R.id.tv_m042_detail_device_id, CAApplication.getInstance().getMediumFont());
            tvLastUse = findViewById(R.id.tv_m042_last_used, CAApplication.getInstance().getBoldFont());
            tvTime = findViewById(R.id.tv_m042_time, CAApplication.getInstance().getRegularFont());
            tvDrug = findViewById(R.id.tv_m042_drug, CAApplication.getInstance().getRegularFont());

            ivBattery = findViewById(R.id.iv_m042_battery);
            ivArrow = findViewById(R.id.iv_m042_arrow, this);
            ivDrug = findViewById(R.id.iv_m042_drug);
        }

        @Override
        protected void onClickView(int idView) {
            if (idView == R.id.iv_m042_arrow) {
                getStorage().setM042CurrentItem(mListData.get(getAdapterPosition()));
                mCallBack.showDeviceDetail();
            }
        }
    }

}