package com.novo.app.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.DiabeteEntity;
import com.novo.app.view.event.OnM008ListDiabetesCallBack;

import java.util.List;

public class M008ListDiabeteAdapter extends BaseRecycleAdapter<OnM008ListDiabetesCallBack, DiabeteEntity, M008ListDiabeteAdapter.DiabeteHolder> {
    private int lastCheckedPos = -1;
    public M008ListDiabeteAdapter(Context mContext, List<DiabeteEntity> mListData, OnM008ListDiabetesCallBack mCallBack) {
        super(mContext, mListData, mCallBack);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_m002_bt;
    }

    @Override
    protected M008ListDiabeteAdapter.DiabeteHolder getViewHolder(int viewType, View itemView) {
        return new M008ListDiabeteAdapter.DiabeteHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        M008ListDiabeteAdapter.DiabeteHolder item = (M008ListDiabeteAdapter.DiabeteHolder) holder;
        DiabeteEntity data = mListData.get(position);
        data.setSelected(lastCheckedPos == position);
        if (data.isSelected()) {
            item.ivChecked.setVisibility(View.VISIBLE);
            item.view.setBackground(mContext.getResources().getDrawable(R.drawable.bg_m025_fill_dark_blue));
        } else {
            item.ivChecked.setVisibility(View.GONE);
            item.view.setBackground(mContext.getResources().getDrawable(R.drawable.bg_m004_fill_bt));
        }
        item.tvDiabete.setText(LangMgr.getInstance().getLangList().get(data.getEntity().getValue()));
        item.tvDiabete.setTag(data);
    }

    public class DiabeteHolder extends BaseHolder {
        TextView tvDiabete;
        ImageView ivChecked;
        View view;
        DiabeteHolder(View itemView) {
            super(itemView);
            view = itemView;
        }

        @Override
        protected void onClickView(int idView) {
            switch (idView) {
                case R.id.tv_m002_lang:
                    lastCheckedPos = getAdapterPosition();
                    notifyDataSetChanged();
                    mCallBack.clickOnItem(mListData.get(lastCheckedPos).getEntity().getKey());
                    break;
                default:
                    break;
            }
        }

        @Override
        protected void initView() {
            tvDiabete = findViewById(R.id.tv_m002_lang, this, CAApplication.getInstance().getBoldFont());
            ivChecked = findViewById(R.id.iv_m002_checked);
            if (lastCheckedPos != -1) mCallBack.itemSelected();
        }
    }

}