package com.novo.app.presenter;

import com.novo.app.view.event.OnM036DoseDetailCallBack;

public class M036DoseDetailPresenter extends BasePresenter<OnM036DoseDetailCallBack> {
    public M036DoseDetailPresenter(OnM036DoseDetailCallBack event) {
        super(event);
    }
}
