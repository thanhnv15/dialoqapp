package com.novo.app.view.widget;

import android.text.format.DateUtils;

import com.novo.app.CAApplication;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public final class NVDateUtils {


    //Region app implementation:
    public static final String TODAY_AT = "Today at ";
    public static final String TODAY = "Today";
    public static final String YESTERDAY_AT = "Yesterday at ";
    private static final String TODAY_COMMA = "Today, ";
    private static final String YESTERDAY_COMMA = "Yesterday, ";
    private static final String YESTERDAY = "Yesterday";
    private static final String M_ENGLISH = "en";

    private NVDateUtils() {
    }

    public static String getDoseDetailsTimeString(String strReportedDate) {

        Date reportedDate = stringToDateLocal(strReportedDate, NVDateFormat.TIME_RANGE_FORMAT);
        String formatDate = android.text.format.DateFormat.is24HourFormat(CAApplication.getInstance())
                ? NVDateFormat.DOSE_DETAILS_TIME_SIMPLE_FORMAT_24H : NVDateFormat.DOSE_DETAILS_TIME_SIMPLE_FORMAT;

        if (reportedDate == null) return "";
        if (DateUtils.isToday(reportedDate.getTime())) {
            return TODAY_COMMA + dateToStringLocal(reportedDate, formatDate);
        } else if (isYesterday(reportedDate)) {
            return YESTERDAY_COMMA + dateToStringLocal(reportedDate, formatDate);
        }


        formatDate = android.text.format.DateFormat.is24HourFormat(CAApplication.getInstance())
                ? NVDateFormat.DOSE_DETAILS_TIME_FORMAT_24H : NVDateFormat.DOSE_DETAILS_TIME_FORMAT;
        return dateToStringLocal(reportedDate, formatDate);
    }

    public static String getLastSyncFormattedString(String strReportedDate) {

        String lastSync = "Last sync: ";

        Date reportedDate = stringToDateLocal(strReportedDate, NVDateFormat.TIME_RANGE_FORMAT);

        if (reportedDate == null) return "";

        if (DateUtils.isToday(reportedDate.getTime())) {

            return lastSync + TODAY_COMMA + dateToStringLocal(reportedDate, NVDateFormat.LAST_SYNC_FORMAT_SIMPLE);
        } else if (isYesterday(reportedDate)) {

            return lastSync + YESTERDAY_COMMA + dateToStringLocal(reportedDate, NVDateFormat.LAST_SYNC_FORMAT_SIMPLE);
        }


        return lastSync + dateToStringLocal(reportedDate, NVDateFormat.LAST_SYNC_FORMAT_FULL);
    }

    private static String getTimeRangeFormattedString(Date date) {

        return dateToStringUniversal(date, NVDateFormat.TIME_RANGE_FORMAT);
    }

    public static String getTimeBefore2WeeksString(Date date) {

        Date startOfDay = getStartOfDayUniversal(date);
        long currentTime = startOfDay.getTime();
        long before2WeeksTime = currentTime - (14 * 24 * 60 * 60 * 1000);
        Date dateBefore2Weeks = new Date(before2WeeksTime);

        return getTimeRangeFormattedString(dateBefore2Weeks);
    }

    private static Date getStartOfDayUniversal(Date inputDate) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));
        cal.setTime(inputDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static String getGroupItemDateTitle(String todayType, String yesterdayType, String strReportedDate) {
        Date reportedDate = stringToDateLocal(strReportedDate, NVDateFormat.TIME_RANGE_FORMAT);
        if (reportedDate == null) return "";
        if (DateUtils.isToday(reportedDate.getTime())) {
            return todayType + dateToStringLocal(reportedDate, NVDateFormat.GROUPED_DATE_FORMAT_SIMPLE);
        } else if (isYesterday(reportedDate)) {
            return yesterdayType + dateToStringLocal(reportedDate, NVDateFormat.GROUPED_DATE_FORMAT_SIMPLE);
        }

        return dateToStringLocal(reportedDate, NVDateFormat.GROUPED_DATE_FORMAT_FULL);
    }

    public static String getGroupItemTimeTitle(String todayType, String yesterdayType, String strReportedDate) {
        Date reportedDate = stringToDateLocal(strReportedDate, NVDateFormat.TIME_RANGE_FORMAT);
        if (reportedDate == null) return "";
        if (DateUtils.isToday(reportedDate.getTime())) {
            return todayType + dateToStringLocal(reportedDate, NVDateFormat.TIME_SIMPLE_FORMAT);
        } else if (isYesterday(reportedDate)) {
            return yesterdayType + dateToStringLocal(reportedDate, NVDateFormat.TIME_SIMPLE_FORMAT);
        }

        return dateToStringLocal(reportedDate, NVDateFormat.LAST_SYNC_FORMAT_SIMPLE);
    }

    public static String getGroupItemDateTitle(String strReportedDate) {
        Date reportedDate = stringToDateLocal(strReportedDate, NVDateFormat.TIME_RANGE_FORMAT);
        if (reportedDate == null) return "";
        if (DateUtils.isToday(reportedDate.getTime())) {
            return TODAY_COMMA + dateToStringLocal(reportedDate, NVDateFormat.GROUPED_DATE_FORMAT_SIMPLE);
        } else if (isYesterday(reportedDate)) {
            return YESTERDAY_COMMA + dateToStringLocal(reportedDate, NVDateFormat.GROUPED_DATE_FORMAT_SIMPLE);
        }

        return dateToStringLocal(reportedDate, NVDateFormat.GROUPED_DATE_FORMAT_FULL);
    }

    public static String getOnlyTime(String strReportedDate) {

        Date reportedDate = stringToDateLocal(strReportedDate, NVDateFormat.TIME_RANGE_FORMAT);
        return dateToStringLocal(reportedDate, android.text.format.DateFormat.is24HourFormat(CAApplication.getInstance()) ? NVDateFormat.TIME_SIMPLE_FORMAT_24 : NVDateFormat.TIME_SIMPLE_FORMAT);
    }
    //End region app implementation.


    public static String dateToStringUniversal(Date date, String dateFormat) {

        return dateToString(date, dateFormat, TimeZone.getTimeZone("GMT"));
    }

    public static String dateToStringLocal(Date date, String dateFormat) {

        return dateToString(date, dateFormat, TimeZone.getDefault());
    }

    private static String dateToString(Date date, String dateFormat, TimeZone timeZone) {
        SimpleDateFormat simpleDateFormat;
        simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.getDefault());
        simpleDateFormat.setTimeZone(timeZone);
        return simpleDateFormat.format(date);
    }

    private static Date stringToDate(String inputStrDate, String dateFormat, TimeZone timeZone) {
        if (inputStrDate == null) return null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.getDefault());
        simpleDateFormat.setTimeZone(timeZone);
        try {
            return simpleDateFormat.parse(inputStrDate);
        } catch (Exception e) {

            return null;
        }
    }

    public static Date stringToDateLocal(String inputStrDate, String dateFormat) {

        return stringToDate(inputStrDate, dateFormat, TimeZone.getDefault());
    }

    public static Date stringToDateUniversal(String inputStrDate, String dateFormat) {

        return stringToDate(inputStrDate, dateFormat, TimeZone.getTimeZone("GMT"));
    }

    private static boolean isYesterday(Date d) {
        return DateUtils.isToday(d.getTime() + DateUtils.DAY_IN_MILLIS);
    }

    public static boolean isSameDay(Date firstDate, Date secondDate) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        dateFormat.setTimeZone(TimeZone.getDefault());

        String strFirstDate = dateFormat.format(firstDate);

        String strSecondDate = dateFormat.format(secondDate);

        return strFirstDate.equals(strSecondDate);
    }

    public static boolean isToday(String strReportedDate) {
        Date reportedDate = stringToDateLocal(strReportedDate, NVDateFormat.TIME_RANGE_FORMAT);

        if (reportedDate == null) return false;

        return DateUtils.isToday(reportedDate.getTime());
    }

    public static String getNoteDetailsTimeString(String strReportedDate) {

        Date reportedDate = stringToDateLocal(strReportedDate, NVDateFormat.TIME_RANGE_FORMAT);

        if (reportedDate == null) return "";

        if (DateUtils.isToday(reportedDate.getTime())) {

            return TODAY;
        } else if (isYesterday(reportedDate)) {

            return YESTERDAY;

        } else {

            return dateToStringLocal(reportedDate, NVDateFormat.NOTE_DATE_FORMAT_FULL);
        }
    }

    public static String getLastUpdatedTerm(String strLastUpdate) {
        Date reportedDate = stringToDateLocal(strLastUpdate, NVDateFormat.LAST_UPDATED_TIME_FORMAT);
        if (reportedDate == null) return "";
        return dateToStringLocal(reportedDate, NVDateFormat.GROUPED_DATE_FORMAT_SIMPLE);
    }

    public static String getPDFNoteReportDate(String strDate) {
        Date reportedDate = stringToDateLocal(strDate, NVDateFormat.TIME_RANGE_FORMAT);
        if (reportedDate == null) return "";

        if (strDate.endsWith("01") && !strDate.endsWith("11"))
            return dateToStringLocal(reportedDate, NVDateFormat.SHARE_PDF_ST_FORMAT);

        else if (strDate.endsWith("02") && !strDate.endsWith("12"))
            return dateToStringLocal(reportedDate, NVDateFormat.SHARE_PDF_ND_FORMAT);

        else if (strDate.endsWith("03") && !strDate.endsWith("13"))
            return dateToStringLocal(reportedDate, NVDateFormat.SHARE_PDF_RD_FORMAT);

        else
            return dateToStringLocal(reportedDate, NVDateFormat.SHARE_PDF_TH_FORMAT);
    }

    public static String getDoseTimingString(String strDate) {
        Date dateToConvert = stringToDateLocal(strDate, NVDateFormat.TIME_SIMPLE_FORMAT_24);
        return dateToStringLocal(dateToConvert, NVDateFormat.TIME_SIMPLE_FORMAT);
    }

    public static String getPDFPageReportDate(String strDate) {
        Date reportedDate = stringToDateLocal(strDate, NVDateFormat.TIME_RANGE_FORMAT);
        if (reportedDate == null) return "";
        return dateToStringLocal(reportedDate, NVDateFormat.GROUPED_DATE_FORMAT_SIMPLE);
    }

    public static String getRegistrationDate(String strRegistrationDate) {
        Date reportedDate = stringToDateLocal(strRegistrationDate, NVDateFormat.TIME_RANGE_FORMAT);
        if (reportedDate == null) return "";
        return dateToStringLocal(reportedDate, NVDateFormat.MANDATORY_ACCEPTED_FORMAT);
    }
}
