package com.novo.app.view.widget.novocharts.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public final class NovoChartUtils {

    private NovoChartUtils() {
        throw new IllegalStateException(NovoChartUtils.class.getName());
    }

    public static String[] getDateStringsWithTimeRange(Date startDate, Date endDate) {

        //int days = (int) TimeUnit.DAYS.convert(endDate.getTime() - startDate.getTime(), TimeUnit.MILLISECONDS) + 1;
        int days = getDaysCountBetweenTwoDate(startDate, endDate);

        Calendar now = Calendar.getInstance();
        now.setTimeZone(TimeZone.getDefault());
        now.setTime(endDate);
        SimpleDateFormat formatFull = new SimpleDateFormat("MMM\ndd", Locale.getDefault());
        SimpleDateFormat formatSingle = new SimpleDateFormat("dd\n", Locale.getDefault());
        String[] arrDayName = new String[days];
        now.add(Calendar.DAY_OF_MONTH, -days + 1);

        for (int i = 0; i < days; i++) {
            if (i == 0) {       // currently, show only name of the month at the 1st item.
                arrDayName[i] = formatFull.format(now.getTime());
            } else {
                arrDayName[i] = formatSingle.format(now.getTime());
            }
            now.add(Calendar.DAY_OF_MONTH, 1);
        }

        return arrDayName;
    }

    public static int[] getDateWithTimeRange(Date startDate, Date endDate) {

        int days = getDaysCountBetweenTwoDate(startDate, endDate);

        Calendar now = Calendar.getInstance();
        now.setTimeZone(TimeZone.getDefault());
        now.setTime(endDate);
        SimpleDateFormat formatSingle = new SimpleDateFormat("d", Locale.getDefault());
        int[] arrDayName = new int[days];
        now.add(Calendar.DAY_OF_MONTH, -days + 1);

        for (int i = 0; i < days; i++) {
            arrDayName[i] = Integer.parseInt(formatSingle.format(now.getTime()));
            now.add(Calendar.DAY_OF_MONTH, 1);
        }

        return arrDayName;
    }

    public static int getDaysCountBetweenTwoDate(Date startDate, Date endDate) {

        long startTime = getStartOfDay(startDate).getTime();
        long endTime = getStartOfDay(endDate).getTime();

        if (endTime < startTime) {

            return 0;
        }

        if (startTime == endTime) {

            return 1;
        }

        return (int) TimeUnit.DAYS.convert(endTime - startTime, TimeUnit.MILLISECONDS) + 1;
    }

    public static Date getStartOfDay(Date inputDate) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getDefault());
        cal.setTime(inputDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static boolean isSameDate(Date firstDate, Date secondDate) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        dateFormat.setTimeZone(TimeZone.getDefault());

        String strFirstDate = dateFormat.format(firstDate);

        String strSecondDate = dateFormat.format(secondDate);

        return strFirstDate.equals(strSecondDate);
    }
}
