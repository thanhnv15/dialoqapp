package com.novo.app.view.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.novo.app.CAApplication;
import com.novo.app.manager.LangMgr;
import com.novo.app.utils.CommonUtil;
import com.novo.app.utils.StorageCommon;
import com.novo.app.view.event.OnCallBackToView;

import java.util.ArrayList;
import java.util.List;

public abstract class BasePageAdapter<T extends OnCallBackToView, G> extends PagerAdapter
        implements View.OnClickListener {
    private static final CharSequence PLACE_HOLDER = "PLACEHOLDER";
    private static final String TAG = "BASE ADAPTER";
    protected final T mCallBack;
    protected final ArrayList<G> listData = new ArrayList<>();
    protected final Context mContext;

    public BasePageAdapter(List<G> listData, Context mContext, T callBack) {
        this.listData.addAll(listData);
        this.mContext = mContext;
        this.mCallBack = callBack;
    }

    public void showNotify(int text) {
        Toast.makeText(CAApplication.getInstance(), text, Toast.LENGTH_SHORT).show();
    }

    public void showNotify(String text) {
        Toast.makeText(CAApplication.getInstance(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public final int getCount() {
        return listData.size();
    }

    protected final <K extends View> K findViewById(View view, int id, View.OnClickListener event, Typeface typeface) {
        K childView = view.findViewById(id);
        childView.setEnabled(true);
        if (childView instanceof TextView && typeface != null) {
            ((TextView) childView).setTypeface(typeface);
        }
        if (childView.getContentDescription() == null) {
            CommonUtil.wtfe(TAG, "No. ContentDescription");
        } else if (childView instanceof TextView) {
            String key = childView.getContentDescription().toString();
            if (LangMgr.getInstance().getLangList() == null) {
                CommonUtil.wtfe(TAG, "Err: No current language");
            } else {
                if (key.contains(PLACE_HOLDER)) {
                    ((TextView) childView).setHint(LangMgr.getInstance().getLangList().get(key));
                } else {
                    ((TextView) childView).setText(LangMgr.getInstance().getLangList().get(key));
                }
            }
        }
        if (event != null) {
            childView.setOnClickListener(event);
        }
        return childView;
    }

    protected final <K extends View> K findViewById(View view, int id, View.OnClickListener event) {
        return findViewById(view, id, event, null);
    }

    protected final <K extends View> K findViewById(View view, int id, Typeface typeface) {
        return findViewById(view, id, null, typeface);
    }

    protected final <K extends View> K findViewById(View view, int id) {
        return findViewById(view, id, null, null);
    }

    @Override
    public final boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public final View instantiateItem(ViewGroup container, int position) {
        View view = View.inflate(mContext, getLayoutId(position), null);
        G data = listData.get(position);
        initViews(view, data, position);
        view.setTag(data);
        view.setOnClickListener(this);
        container.addView(view);
        onAddedView(position, view);
        return view;
    }

    protected void onAddedView(int position, View view) {
        // do nothing
    }

    protected abstract void initViews(View rootView, G data, int pos);

    protected abstract int getLayoutId(int pos);

    @Override
    public final void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public final StorageCommon getStorage() {
        return CAApplication.getInstance().getStorageCommon();
    }
}
