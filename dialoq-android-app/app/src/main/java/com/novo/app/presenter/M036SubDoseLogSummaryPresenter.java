package com.novo.app.presenter;

import com.novo.app.manager.CADBManager;
import com.novo.app.manager.entities.RDoseEntity;
import com.novo.app.manager.entities.RInjectionEntity;
import com.novo.app.manager.entities.RInjectionItem;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM036SubDoseLogSummaryCallBack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.RealmList;

public class M036SubDoseLogSummaryPresenter extends BasePresenter<OnM036SubDoseLogSummaryCallBack> {

    private final ArrayList<DataInfo> mListDataInfo = new ArrayList<>();

    public M036SubDoseLogSummaryPresenter(OnM036SubDoseLogSummaryCallBack event) {
        super(event);
    }

    public void loadOffline() {
        mListDataInfo.clear();
        List<RDoseEntity> listData = CADBManager.getInstance().getAllRDoseEntity(CommonUtil.INJECTION_ASSESSMENT_TYPE);
        if (!listData.isEmpty()) {
            for (int i = 0; i < listData.size(); i++) {
                RDoseEntity entity = listData.get(i);

                if (entity == null) continue;
                mListDataInfo.add(entity.toResultInfo());
            }
        }
        //for done
        List<RDoseEntity> listDBNotes = CADBManager.getInstance().getAllNote();
        if (!listDBNotes.isEmpty()) {
            for (int i = 0; i < listDBNotes.size(); i++) {
                mListDataInfo.add(listDBNotes.get(i).toResultInfo());
            }
        }

        List<RInjectionEntity> listInjection = CADBManager.getInstance().getAllNotSyncDose();

        if (listInjection.size() > 0) {
            for (int i = 0; i < listInjection.size(); i++) {
                RInjectionEntity injectionEntity = listInjection.get(i);

                if (injectionEntity.getInjectionItems() == null) continue;
                RealmList<RInjectionItem> listDose = injectionEntity.getInjectionItems();

                for (int j = 0; j < listDose.size(); j++) {
                    RInjectionItem item = listDose.get(j);
                    if (item == null) continue;
                    mListDataInfo.add(item.toResultInfo());
                }
            }
        }

        Collections.sort(mListDataInfo);
        CommonUtil.convertToLocalDateData(mListDataInfo);
        mListener.dataAS05Ready();
    }

    public ArrayList<DataInfo> getListDataInfo() {
        return mListDataInfo;
    }
}
