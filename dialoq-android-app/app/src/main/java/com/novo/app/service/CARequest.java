/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.service;

import com.novo.app.utils.CommonUtil;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThanhNv on 4/23/2017.
 */
public class CARequest implements Serializable {

    public static final String METHOD_GET = "GET";
    public static final String METHOD_DELETE = "DELETE";
    public static final String METHOD_POST = "POST";
    public static final String TOKEN_KEY = "TOKEN_KEY";
    public static final String METHOD_PUT = "PUT";
    private static final String TAG = CARequest.class.getName();

    private List<String[]> mParams = new ArrayList<>();
    private String mBody;

    private List<String[]> mHeader = new ArrayList<>();
    private List<String[]> mQuery = new ArrayList<>();

    private boolean isUsedBaseURL;
    private String mKey;
    private boolean isExpired;
    private String mMethod;
    private String mTag;
    private String mPathSegment;
    private String mRequestID;
    private String mToken;
    private List<String[]> mFilePath = new ArrayList<>();

    public CARequest(String key, String method, boolean isUsedBaseURL) {
        this(key, method);
        this.isUsedBaseURL = isUsedBaseURL;
    }

    public CARequest(String key, String method) {
        this.mKey = key;
        mMethod = method;
        isUsedBaseURL = true;
    }

    String getPathSegment() {
        return mPathSegment;
    }

    List<String[]> getParams() {
        return mParams;
    }

    List<String[]> getHeader() {
        return mHeader;
    }

    public String getKey() {
        return mKey;
    }

    public CARequest addParams(String name, String value) {
        mParams.add(new String[]{name, value});
        return this;
    }

    public CARequest addHeaders(String name, String value) {
        mHeader.add(new String[]{name, value});
        return this;
    }

    public void addPathSegment(String pathSegment) {
        mPathSegment = pathSegment;
    }

    public String getMethod() {
        return mMethod;
    }

    public CARequest addQueryParams(String name, String value) {
        addQueryParams(name, value, false);
        return this;
    }

    public CARequest addFile(String name, String path) {
        mFilePath.add(new String[]{name, path});
        return this;
    }

    public CARequest addQueryParams(String name, String value, boolean isUTF8) {
        if (isUTF8) {
            try {
                value = URLEncoder.encode(value, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                CommonUtil.wtfe(TAG, e.getLocalizedMessage());
            }
        }
        mQuery.add(new String[]{name, value});
        return this;
    }

    List<String[]> getQuery() {
        return mQuery;
    }

    public List<String[]> getFiles() {
        return mFilePath;
    }

    public void addTAG(String tag) {
        mTag = tag;
    }

    public String getTag() {
        return mTag;
    }

    public String getBody() {
        return mBody;
    }

    public void addBody(String mBody) {
        this.mBody = mBody;
    }

    public boolean isUsedBaseURL() {
        return isUsedBaseURL;
    }

    public String getRequestID() {
        return mRequestID;
    }

    public void addRequestID(String mRequestID) {
        this.mRequestID = mRequestID;
    }

    public boolean isExpired() {
        return isExpired;
    }

    public void setExpired(boolean expired) {
        isExpired = expired;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String mToken) {
        this.mToken = mToken;
    }
}
