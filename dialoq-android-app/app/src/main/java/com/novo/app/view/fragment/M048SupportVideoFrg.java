package com.novo.app.view.fragment;

import android.content.res.Configuration;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M048SupportVideoPresenter;
import com.novo.app.utils.CATask;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.M048VideoAdapter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.dialog.M048VideoDialog;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM048SupportVideoCallBack;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;

public class M048SupportVideoFrg extends BaseFragment<M048SupportVideoPresenter, OnM024DoseLogCallBack> implements OnM048SupportVideoCallBack, CATask.OnTaskCallBackToView {
    public static final String TAG = M048SupportVideoFrg.class.getName();
    private M048VideoDialog mDialog;


    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m048_back, this);
        findViewById(R.id.tv_m048_title, CAApplication.getInstance().getBoldFont());

        RecyclerView rvVideo = findViewById(R.id.rv_m048_video_list);
        rvVideo.setLayoutManager(new LinearLayoutManager(mContext));
        M048VideoAdapter adapter = new M048VideoAdapter(mContext, mPresenter.getData(), this);
        rvVideo.setAdapter(adapter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m048_support_video;
    }

    @Override
    protected M048SupportVideoPresenter getPresenter() {
        return new M048SupportVideoPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mDialog == null) return;
        mDialog.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onClickView(int idView) {
        if (idView == R.id.iv_m048_back) {
            mCallBack.showChildFrgScreen(TAG, M026SupportFrg.TAG);
        }
    }

    @Override
    public void toVideoPlayer(String url) {
        if (url == null) {
            showNotify(R.string.txt_err_null_link_video);
            return;
        }

        getStorage().setM048VideoPath(url);
        String fileName = url.substring(url.lastIndexOf("/") + 1);
        String folderName = CommonUtil.getInstance().getPrefContent(CommonUtil.LANG_KEY);
        String path = CAApplication.getInstance().getExternalFilesDir(null) + "/"+folderName +"/"+ fileName;
        File file = new File(path);

        if (CommonUtil.getInstance().isConnectToNetwork()) {
            if (!file.exists()) {
                downloadVideo(url);
            }
            mDialog = new M048VideoDialog(mContext, false);
            mDialog.setOnCallBack(this);
            mDialog.show();
        } else {
            if (!file.exists()) {
                showNotify(R.string.txt_lost_network);
                return;
            }

            getStorage().setM048VideoPath(path);
            mDialog = new M048VideoDialog(mContext, false);
            mDialog.setOnCallBack(this);
            mDialog.show();
        }
    }

    private void downloadVideo(String url) {
        CATask<M048SupportVideoFrg, M048SupportVideoFrg> taskDL = new CATask<>(new WeakReference<>(this), this);
        taskDL.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
    }

    @Override
    public void showChildFrgScreen(String tagSource, String tagChild) {
        //This feature isn't require here
    }

    @Override
    public void updateChildContent() {
        //This feature isn't require here
    }

    @SuppressWarnings("squid:S4042")
    @Override
    public Object doBGTask(String key) {
        FileOutputStream out = null;
        try {
            if (key == null || !key.toUpperCase().endsWith(".MP4")) {
                return new Object();
            }

            URLConnection conn = new URL(key).openConnection();
            InputStream in = conn.getInputStream();
            int len;
            byte[] buff = new byte[1024];
            String fileName = key.substring(key.lastIndexOf("/") + 1);
            String folderName = CommonUtil.getInstance().getPrefContent(CommonUtil.LANG_KEY);
            String path = CAApplication.getInstance().getExternalFilesDir(null) + "/"+folderName +"/"+ fileName;
            CommonUtil.wtfi(TAG, "PATH: downloadVideo: " + path);

            File file = new File(path);
            file.delete();
            file.createNewFile();
            out = new FileOutputStream(file);
            len = in.read(buff);

            while (len > 0) {
                out.write(buff, 0, len);
                len = in.read(buff);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new Object();
    }
}
