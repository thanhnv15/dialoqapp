package com.novo.app.view.widget.novocharts.formatter;

import com.github.mikephil.charting.formatter.ValueFormatter;
import com.novo.app.utils.CommonUtil;

import java.util.Locale;

public class NovoScatterChartXValueFormatter extends ValueFormatter {

    @Override
    public String getFormattedValue(float value) {
        return getValueFormatString(value);
    }

    private String getValueFormatString(float value) {
        if (value % 4 != 0) {
            return "";
        } else if (value / 12 == 1) {
            return "12:00 PM";
        } else if (value / 12 == 2) {
            return "12:00 AM";
        } else if (value < 12) {
            return String.format(Locale.US, "%d:00 AM", (int) (value));
        } else {
            if ((int) value - 12 > 12) {
                return "";
            } else {
                return String.format(Locale.US, "%d:00 PM", (int) (value - 12));
            }
        }
    }
}