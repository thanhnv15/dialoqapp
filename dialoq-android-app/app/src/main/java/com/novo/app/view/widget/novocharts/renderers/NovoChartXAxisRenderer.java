package com.novo.app.view.widget.novocharts.renderers;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.renderer.XAxisRenderer;
import com.github.mikephil.charting.utils.FSize;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.novo.app.view.widget.novocharts.formatter.NovoXValueFormatter;

public class NovoChartXAxisRenderer extends XAxisRenderer {
    private float expandSize = Utils.convertDpToPixel(30);

    public NovoChartXAxisRenderer(ViewPortHandler viewPortHandler, XAxis xAxis, Transformer trans) {
        super(viewPortHandler, xAxis, trans);
    }

    @Override
    protected void drawLabel(Canvas c, String formattedLabel, float x, float y, MPPointF anchor, float angleDegrees) {
        String line[] = formattedLabel.split("\n");
        if (line.length == 2) {
            int day = ((NovoXValueFormatter) mAxis.getValueFormatter()).getmArrayDate().length;
            switch (day) {
                case 7:
                    Utils.drawXAxisValue(c, line[0], x - Utils.convertPixelsToDp(200), y, mAxisLabelPaint, anchor, angleDegrees);
                    break;
                case 14:
                    Utils.drawXAxisValue(c, line[0], x - Utils.convertPixelsToDp(150), y, mAxisLabelPaint, anchor, angleDegrees);
                    break;
                case 28:
                    Utils.drawXAxisValue(c, line[0], x - Utils.convertPixelsToDp(150), y, mAxisLabelPaint, anchor, angleDegrees);
                    break;
                default:
                    break;
            }
            Utils.drawXAxisValue(c, line[1], x, y, mAxisLabelPaint, anchor, angleDegrees);

        } else {
            Utils.drawXAxisValue(c, line[0], x, y, mAxisLabelPaint, anchor, angleDegrees);
        }

        Drawable drawable = ((NovoXValueFormatter) mAxis.getValueFormatter()).getNoteIconDrawable();
        boolean isHasNote = ((NovoXValueFormatter) mAxis.getValueFormatter()).getHashMapDateNote().get(formattedLabel);

        if (drawable != null && isHasNote) {
            Utils.drawImage(c, drawable, (int) x, (int) (y + expandSize), (int) Utils.convertDpToPixel(15), (int) Utils.convertDpToPixel(16));
        }
    }

    @Override
    protected void computeSize() {
        String longest = mXAxis.getLongestLabel();

        mAxisLabelPaint.setTypeface(mXAxis.getTypeface());
        mAxisLabelPaint.setTextSize(mXAxis.getTextSize());

        final FSize labelSize = Utils.calcTextSize(mAxisLabelPaint, longest);

        final float labelWidth = labelSize.width;
        final float labelHeight = Utils.calcTextHeight(mAxisLabelPaint, "Q");

        final FSize labelRotatedSize = Utils.getSizeOfRotatedRectangleByDegrees(
                labelWidth,
                labelHeight,
                mXAxis.getLabelRotationAngle());


        mXAxis.mLabelWidth = Math.round(labelWidth);
        mXAxis.mLabelHeight = Math.round(labelHeight);
        mXAxis.mLabelRotatedWidth = Math.round(labelRotatedSize.width);
        mXAxis.mLabelRotatedHeight = Math.round(labelRotatedSize.height + expandSize);

        FSize.recycleInstance(labelRotatedSize);
        FSize.recycleInstance(labelSize);
    }

}
