package com.novo.app.view.fragment;

import android.content.Intent;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M059MoreProductPresenter;
import com.novo.app.service.SyncScheduleService;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.dialog.M051MorePrivacyPolicyDialog;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM051MoreCallBack;
import com.novo.app.view.event.OnM059MoreProductCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class M059MoreProductFrg extends BaseFragment<M059MoreProductPresenter, OnM024DoseLogCallBack> implements OnM059MoreProductCallBack, OnM051MoreCallBack {
    public static final String TAG = M059MoreProductFrg.class.getName();
    private M051MorePrivacyPolicyDialog m051MorePrivacyPolicyDialog;
    private RadioButton mRbAgree;
    private RadioButton mRbDecline;
    private TextView mTvOptedOutA;
    private TextView mTvOptedOutD;
    private List<Boolean> mRbState = new ArrayList<>();

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m059_back, this);

        findViewById(R.id.tv_m059_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m059_help, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m059_description, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m059_detail, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m059_read_more, this, CAApplication.getInstance().getBoldFont());

        findViewById(R.id.tv_m059_agree, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m059_decline, CAApplication.getInstance().getRegularFont());
        mTvOptedOutA = findViewById(R.id.tv_m059_opted_out_agree, CAApplication.getInstance().getRegularFont());
        mTvOptedOutD = findViewById(R.id.tv_m059_opted_out_decline, CAApplication.getInstance().getRegularFont());
        mTvOptedOutA.setText(getLastUpdate());

        mRbAgree = findViewById(R.id.rb_m059_agree, this);
        mRbDecline = findViewById(R.id.rb_m059_decline, this);

        showUserChoice();
    }

    private void showUserChoice() {
        String value = getStorage().getM007UserEntity().getAllergies();
        if (value != null) {
            if (value.equals(Integer.toString(CommonUtil.AGREE_VALUE))) {
                mRbAgree.setChecked(true);
                mRbDecline.setChecked(false);
                mTvOptedOutA.setText(getLastUpdate());
                mTvOptedOutA.setVisibility(View.VISIBLE);
                mTvOptedOutD.setVisibility(View.GONE);
            } else {
                mRbAgree.setChecked(false);
                mRbDecline.setChecked(true);
                mTvOptedOutD.setText(getLastUpdate());
                mTvOptedOutA.setVisibility(View.GONE);
                mTvOptedOutD.setVisibility(View.VISIBLE);
            }
        } else {
            mRbAgree.setChecked(false);
            mRbDecline.setChecked(false);
            mTvOptedOutD.setText(getLastUpdate());
            mTvOptedOutA.setVisibility(View.GONE);
            mTvOptedOutD.setVisibility(View.GONE);
        }
        mRbState.add(0, mRbAgree.isChecked());
        mRbState.add(1, mRbDecline.isChecked());
    }

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }


    private String getLastUpdate() {
        String optedOut = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m058_more_product_improvement_opted_out));
        String lastUpdate = getStorage().getM007UserEntity().getStreetAddress1();

        if (lastUpdate != null && !lastUpdate.equals("")) {
            lastUpdate = lastUpdate.substring(2, lastUpdate.indexOf('T'));
            String[] tmpString = lastUpdate.split("-");
            lastUpdate = tmpString[2] + "/" + tmpString[1] + "/" + tmpString[0];
            optedOut = optedOut.substring(0, optedOut.indexOf('$')) + lastUpdate + ")";
        }
        return optedOut;
    }

    private String getCurrentDate() {
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String dateToString = format.format(today);
        return String.format("%sZ", dateToString.replace(" ", "T"));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m059_more_product;
    }

    @Override
    protected M059MoreProductPresenter getPresenter() {
        return new M059MoreProductPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        // This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m059_back:
                mCallBack.showChildFrgScreen(TAG, M052MoreMyAccountFrg.TAG);
                break;
            case R.id.tv_m059_read_more:
                if (m051MorePrivacyPolicyDialog == null) {
                    m051MorePrivacyPolicyDialog = new M051MorePrivacyPolicyDialog(mContext, true);
                    m051MorePrivacyPolicyDialog.setOnCallBack(this);
                }
                m051MorePrivacyPolicyDialog.show();
                break;
            case R.id.rb_m059_agree:
                mRbDecline.setChecked(false);
                changeStatus(idView);
                break;
            case R.id.rb_m059_decline:
                mRbAgree.setChecked(false);
                changeStatus(idView);
                break;
        }
    }

    private void changeStatus(int idView) {
        String title = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m059_more_my_account_communication_selection_confirmation_title));
        String msg = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m059_more_my_account_communication_selection_confirmation_message));
        String bt1 = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m059_more_my_account_communication_selection_confirmation_agree));
        String bt2 = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m059_more_my_account_communication_selection_confirmation_cancel));

        CommonUtil.getInstance().showDialog(mContext, title, msg, bt1, bt2, new OnOKDialogCallBack() {
            @Override
            public void handleOKButton1() {
                if (idView == R.id.rb_m059_agree)
                    getStorage().getM007UserEntity().setAllergies(Integer.toString(CommonUtil.AGREE_VALUE));

                if (idView == R.id.rb_m059_decline)
                    getStorage().getM007UserEntity().setAllergies(Integer.toString(CommonUtil.NOT_AGREE_VALUE));

                getStorage().getM007UserEntity().setStreetAddress1(getCurrentDate());
                mPresenter.callIUN04UpdatePatient();
            }

            @Override
            public void handleOKButton2() {
                stateReverse();
            }
        });
    }

    @Override
    public void updateUserChoice() {
        showUserChoice();
    }

    @Override
    public void showLoginScreen() {
        mCallBack.showM023LoginScreen();
    }

    @Override
    public void showDialog() {
        CommonUtil.getInstance().showDialog(mContext, LangMgr.getInstance().getLangList().get(getString(R.string.lang_m059_more_my_account_product_message)), null);
    }

    @Override
    public void stateReverse() {
        mRbAgree.setChecked(mRbState.get(0));
        mRbDecline.setChecked(mRbState.get(1));
    }

    @Override
    public void deleteSuccess() {
        mContext.stopService(new Intent(mContext, SyncScheduleService.class));
        mCallBack.showM001LandingScreen();
    }
}
