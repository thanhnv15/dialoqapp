package com.novo.app.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.view.event.OnM002SignUpChooseLanguageCallBack;

import java.util.List;

public class M002LangAdapter extends BaseRecycleAdapter<OnM002SignUpChooseLanguageCallBack, TextEntity, M002LangAdapter.LangHolder> {
    private int lastCheckedPos = -1;
    private boolean isFirstTime = true;
    public M002LangAdapter(Context mContext, List<TextEntity> mListData, OnM002SignUpChooseLanguageCallBack mCallBack) {
        super(mContext, mListData, mCallBack);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_m002_bt;
    }

    @Override
    protected M002LangAdapter.LangHolder getViewHolder(int viewType, View itemView) {
        return new M002LangAdapter.LangHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        M002LangAdapter.LangHolder item = (M002LangAdapter.LangHolder) holder;
        TextEntity data = mListData.get(position);
        data.setSelected(lastCheckedPos == position);
        if (isFirstTime) {
            String currentLang = LangMgr.getInstance().getCurrentLang();
            if (data.getValue().equalsIgnoreCase(currentLang)) {
                data.setSelected(true);
                lastCheckedPos = position;
                isFirstTime = false;
            } else {
                data.setSelected(false);
            }
        }

        if (data.isSelected()) {
            item.ivChecked.setVisibility(View.VISIBLE);
            item.view.setBackground(mContext.getResources().getDrawable(R.drawable.bg_m025_fill_dark_blue));
        } else {
            item.ivChecked.setVisibility(View.GONE);
            item.view.setBackground(mContext.getResources().getDrawable(R.drawable.bg_m004_fill_bt));
        }
        item.tvLanguage.setText(data.getValue());
        item.tvLanguage.setTag(data);
    }

    public void setLastCheckedPos(int lastCheckedPos) {
        this.lastCheckedPos = lastCheckedPos;
    }

    public class LangHolder extends BaseHolder {
        TextView tvLanguage;
        ImageView ivChecked;
        View view;
        LangHolder(View itemView) {
            super(itemView);
            view = itemView;
        }

        @Override
        protected void onClickView(int idView) {
            switch (idView) {
                case R.id.tv_m002_lang:
                    lastCheckedPos = getAdapterPosition();
                    notifyDataSetChanged();
                    mCallBack.clickOnItem((TextEntity) tvLanguage.getTag());
                    break;
                default:
                    break;
            }
        }

        @Override
        protected void initView() {
            tvLanguage = findViewById(R.id.tv_m002_lang, this, CAApplication.getInstance().getBoldFont());
            ivChecked = findViewById(R.id.iv_m002_checked);
        }
    }

}