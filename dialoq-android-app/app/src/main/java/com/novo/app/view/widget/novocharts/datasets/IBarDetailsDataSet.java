package com.novo.app.view.widget.novocharts.datasets;

import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

public interface IBarDetailsDataSet extends IBarDataSet {

    int getUnitColor();
    
}
