package com.novo.app.presenter;

import com.novo.app.view.event.OnM066SignUpCallBack;

public class M066SignUpPresenter extends BasePresenter<OnM066SignUpCallBack>{
    public M066SignUpPresenter(OnM066SignUpCallBack event) {
        super(event);
    }
}
