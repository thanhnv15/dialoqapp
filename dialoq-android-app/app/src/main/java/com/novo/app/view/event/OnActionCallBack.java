package com.novo.app.view.event;

public interface OnActionCallBack {
    void onCallBack(Object data);
    default void closeRefresh(){

    }
}
