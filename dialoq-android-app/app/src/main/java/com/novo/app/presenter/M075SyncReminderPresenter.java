/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RReminderEntity;
import com.novo.app.model.entities.AssessmentScheduleEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM075SyncReminderCallBack;
import com.novo.app.view.widget.ProgressLoading;

import java.util.TimeZone;

import static com.novo.app.utils.CommonUtil.P_API_KEY;

public class M075SyncReminderPresenter extends BasePresenter<OnM075SyncReminderCallBack> {
    private static final String KEY_API_AS02_GET_LIST_SCHEDULES_OF_ASSESSMENT = "KEY_API_AS02_GET_LIST_SCHEDULES_OF_ASSESSMENT";
    private static final String KEY_API_AS29_GET_LIST_SCHEDULES_OF_ASSESSMENT = "KEY_API_AS29_GET_LIST_SCHEDULES_OF_ASSESSMENT";
    private static final String URL_API_AS29_GET_LIST_SCHEDULES_OF_ASSESSMENT = CommonUtil.HOT_URL + "/api/v1/assessments/%s/schedules?";

    private static final String KEY_API_AS09_DELETE_ASSESSMENT_SCHEDULE = "KEY_API_AS09_DELETE_ASSESSMENT_SCHEDULE";
    private static final String KEY_API_AS02_CREATE_ASSESSMENT_SCHEDULE = "KEY_API_AS02_CREATE_ASSESSMENT_SCHEDULE";
    private static final String URL_API_AS02_CREATE_ASSESSMENT_SCHEDULE = CommonUtil.HOT_URL + "/api/v1/users/%s/assessments/%s/schedules?";
    private static final String BODY_AS02 = "{\"scheduleName\": %s,\"freqType\": \"DAILY\"," +
            "\"freqInterval\": \"\", \"freqSubdayType\": \"SPECIFIED_TIME\",\"startTime\": %s,\"startDate\": %s,\"endDate\": %s,\"scheduleNotifications\": " +
            "[{\"notificationChannel\":\"[PUSH_APPLICATION]\",\"notificationType\":\"REMIND_TAKE_MEDICATION\",\"reminderTime\":\"300\"}]}";
    private static final String KEY_API_AS10_UPDATE_ASSESSMENT_SCHEDULE = "KEY_API_AS10_UPDATE_ASSESSMENT_SCHEDULE";
    private static final String URL_API_AS10_UPDATE_ASSESSMENT_SCHEDULE = CommonUtil.HOT_URL + "/api/v1/users/%s/assessments/%s/schedules/%s?";
    private static final String BODY_AS10 = "{\"scheduleName\": %s,\"freqType\": \"DAILY\"," +
            "\"freqInterval\": \"\", \"freqSubdayType\": \"SPECIFIED_TIME\",\"startTime\": %s,\"startDate\": %s,\"endDate\": %s,\"scheduleNotifications\": " +
            "[{\"scheduleNotificationId\": %s,\"notificationChannel\": %s,\"notificationType\":\"REMIND_TAKE_MEDICATION\",\"reminderTime\":\"300\"}]}";
    private static final String URL_API_AS09_DELETE_ASSESSMENT_SCHEDULE = CommonUtil.HOT_URL + "/api/v1/assessments/%s/schedules/%s?";
    private static final String TAG = M075SyncReminderPresenter.class.getName();
    private transient RReminderEntity mEntity;

    private static final String TOKEN = "token: ";

    public M075SyncReminderPresenter(OnM075SyncReminderCallBack event, RReminderEntity entity) {
        super(event);
        this.mEntity = entity;
    }

    @Override
    public void doFailed(String tag, Exception obj, int i, String s) {
        if ((tag.equals(KEY_API_AS29_GET_LIST_SCHEDULES_OF_ASSESSMENT) || tag.equals(KEY_API_AS02_CREATE_ASSESSMENT_SCHEDULE)) && i == 204) {
            mListener.syncReminderReady();
            return;
        }

        mListener.syncFailed("Sync reminder failed!");
    }

    @Override
    protected void handleSuccess(String text, String tag) {
        if (KEY_API_AS02_GET_LIST_SCHEDULES_OF_ASSESSMENT.equals(tag)) {
            CommonUtil.wtfi(TAG, "GET-AAFFTER_upADDReminderData...Reminder ADD success!");
        } else if (KEY_API_AS02_CREATE_ASSESSMENT_SCHEDULE.equals(tag)) {
            callAS029GetListSchedulesOfAssessment(KEY_API_AS02_GET_LIST_SCHEDULES_OF_ASSESSMENT);
            CommonUtil.wtfi(TAG, "upADDReminderData...Reminder ADD success!");
            return;
        } else if (tag.contains(KEY_API_AS10_UPDATE_ASSESSMENT_SCHEDULE)) {
            CommonUtil.wtfi(TAG, "EditReminderData...Reminder EDIT success!");
            CADBManager.getInstance().resetFlagReminder();
        } else if (tag.contains(KEY_API_AS09_DELETE_ASSESSMENT_SCHEDULE)) {
            CommonUtil.wtfi(TAG, "DeleteReminderData...Reminder DEL success!");
            mListener.delReminderSuccess();
        } else if (tag.contains(KEY_API_AS29_GET_LIST_SCHEDULES_OF_ASSESSMENT)) {
            prepareAS29(text);
            return;
        }

        mListener.syncReminderSuccess();
    }

    @Override
    public void hideLockDialog(String key) {
        //do nothing
    }

    public void callAS029GetListSchedulesOfAssessment() {
        callAS029GetListSchedulesOfAssessment(KEY_API_AS29_GET_LIST_SCHEDULES_OF_ASSESSMENT);
    }

    private void callAS029GetListSchedulesOfAssessment(String key) {
        CommonUtil.wtfi(TAG, "callAS029GetListSchedulesOfAssessment");
        ProgressLoading.dontShow();
        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(key);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, TOKEN + token);
        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);


        request.addPathSegment(String.format(URL_API_AS29_GET_LIST_SCHEDULES_OF_ASSESSMENT, getAssessmentId(CommonUtil.INJECTION_ASSESSMENT_TYPE) + ""));
        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    public void callAS10UpdateAssessmentSchedule(RReminderEntity entity) {
        CommonUtil.wtfi(TAG, "callAS10UpdateAssessmentSchedule");
        CARequest request = new CARequest(TAG, CARequest.METHOD_PUT);
        request.addTAG(KEY_API_AS10_UPDATE_ASSESSMENT_SCHEDULE);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, TOKEN + token);
        UserEntity user = getStorage().getM007UserEntity();
        if (entity == null) {
            return;
        }

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_AS10_UPDATE_ASSESSMENT_SCHEDULE, user.getUserId(),
                getAssessmentId(CommonUtil.INJECTION_ASSESSMENT_TYPE), entity.getScheduleId() + ""));
        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY_AS10, System.currentTimeMillis() + "", entity.getStartTime(), entity.getStartDate(),
                entity.getEndDate(), entity.getScheduleNotificationId(), entity.getNotificationChannel()));
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    public void callAS02CreateAssessmentSchedule(RReminderEntity entity) {
        CommonUtil.wtfi(TAG, "callAS02CreateAssessmentSchedule");

        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_AS02_CREATE_ASSESSMENT_SCHEDULE);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, TOKEN + token);
        UserEntity user = getStorage().getM007UserEntity();
        if (entity == null) {
            return;
        }

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_AS02_CREATE_ASSESSMENT_SCHEDULE, user.getUserId(),
                getAssessmentId(CommonUtil.INJECTION_ASSESSMENT_TYPE) + ""));
        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY_AS02, System.currentTimeMillis() + "", entity.getStartTime(), entity.getStartDate(), entity.getEndDate()));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }


    public void callAS09DeleteAssessmentSchedule(RReminderEntity entity) {
        CommonUtil.wtfi(TAG, "callAS09DeleteAssessmentSchedule");

        CARequest request = new CARequest(TAG, CARequest.METHOD_DELETE);
        request.addTAG(KEY_API_AS09_DELETE_ASSESSMENT_SCHEDULE);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, TOKEN + token);
        String scheduleId = getStorage().getM065ScheduleEntity().getScheduleId();

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_AS09_DELETE_ASSESSMENT_SCHEDULE, getAssessmentId(CommonUtil.INJECTION_ASSESSMENT_TYPE), scheduleId + ""));
        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }


    private void prepareAS29(String data) {
        CommonUtil.wtfi(TAG, "prepareAS29..." + data);
        AssessmentScheduleEntity entity = generateData(AssessmentScheduleEntity.class, data);
        if (entity == null) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            ProgressLoading.dismiss();
            return;
        }

        String startDate = CommonUtil.convertToLocalDate(entity.getData().get(0).getStartDate());
        String endDate = CommonUtil.convertToLocalDate(entity.getData().get(0).getStartDate());
        String startTime = CommonUtil.toLocalTime(entity.getData().get(0).getStartTime());
        String[] reminder = startTime.split(":");

        if (!TimeZone.getDefault().useDaylightTime()) {
            startTime = (Integer.valueOf(reminder[0]) - 1) + ":" + reminder[1];
        }

        entity.getData().get(0).setStartDate(startDate);
        entity.getData().get(0).setEndDate(endDate);
        entity.getData().get(0).setStartTime(startTime);

        RReminderEntity reminderItem = CADBManager.getInstance().addRReminder(entity);
        if (reminderItem == null) return;

        getStorage().setM065ScheduleEntity(reminderItem);
        String tmp = reminderItem.getStartTime();
        assert tmp != null;

        reminder = tmp.split(":");
        CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_HOUR, Integer.valueOf(reminder[0]));
        CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_MIN, Integer.valueOf(reminder[1]));
        if (getStorage().getM065ScheduleEntity().getNotificationChannel().equals(CommonUtil.REMINDER_ACTIVE)) {
            CommonUtil.getInstance().setAppReminder(CAApplication.getInstance());
        }
        mListener.syncReminderReady();
    }
}
