package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM061MorePasswordCallBack;
import com.novo.app.view.widget.ProgressLoading;

public class M061MorePasswordPresenter extends BasePresenter<OnM061MorePasswordCallBack> {
    private static final String TAG = M061MorePasswordPresenter.class.getName();
    private static final String KEY_API_CHANGE_PASSWORD = "KEY_API_CHANGE_PASSWORD";
    private static final String KEY_API_AUTHEN = "KEY_API_AUTHEN";
    private static final String URL_API_CHANGE_PASSWORD = "user-service/api/v1/change-password?";
    private static final String URL_API_AUTHEN = "user-service/api/v1/authenticate?";
    private static final String BODY_CHANGE_PASSWORD = "{\"userName\": %s,\"password\": %s,\"currentPassword\": %s}";
    private static final String BODY_AUTHEN = "{\"userName\": %s,\"password\": %s}";
    private static final String RESPONSE_CHANGE_PASSWORD = "{\"success\": %s}";

    public M061MorePasswordPresenter(OnM061MorePasswordCallBack event) {
        super(event);
    }

    @Override
    protected void handleSuccess(String data, String tag) {
        ProgressLoading.dismiss();
        switch (tag) {
            case KEY_API_CHANGE_PASSWORD:
                if (data.contains("true")) {
                    mListener.changePasswordSuccess();
                }
                break;
            case KEY_API_AUTHEN:
                break;
            default:
                break;
        }
    }

    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        ProgressLoading.dismiss();
        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        String sms;
        if (responseEntity != null) {
            sms = responseEntity.getErrorEntity()[0].getMessage();
            if (KEY_API_CHANGE_PASSWORD.equals(tag)) {
                mListener.showDialog();
            } else {
                showNotify(sms);
            }
        } else {
            mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(ERR_500));
        }
    }

    /**
     * @param userName
     * @param currentPassword
     * @param newPassword
     */
    public void callingChangePassword(String userName, String currentPassword, String newPassword) {
        CommonUtil.wtfi(TAG, "callChangePassword");

        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_CHANGE_PASSWORD);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, "token: " + token);
        request.addHeaders(CommonUtil.AUTHOR, token);

        request.addPathSegment(URL_API_CHANGE_PASSWORD);

        request.addHeaders(CommonUtil.P_API_KEY, CommonUtil.getInstance().getAPIKey());

        request.addBody(generateJson(BODY_CHANGE_PASSWORD, userName, newPassword, currentPassword));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }


    /**
     * use when token expired
     *
     * @param userName
     * @param currentPassword
     */
    public void callingAuthentication(String userName, String currentPassword) {
        CommonUtil.wtfi(TAG, "callChangePassword");

        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_AUTHEN);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addPathSegment(URL_API_AUTHEN);

        request.addHeaders(CommonUtil.P_API_KEY, CommonUtil.getInstance().getAPIKey());

        request.addBody(generateJson(BODY_AUTHEN, userName, currentPassword));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }
}
