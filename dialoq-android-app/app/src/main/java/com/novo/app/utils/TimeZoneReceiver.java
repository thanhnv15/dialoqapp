package com.novo.app.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.novo.app.CAApplication;

import java.util.TimeZone;

public final class TimeZoneReceiver extends BroadcastReceiver {
    public static final String ACTION_SHOW_TIME_ZONE_CHANGE = "ACTION_SHOW_TIME_ZONE_CHANGE";
    private static final String TAG = TimeZoneReceiver.class.toString();

    @Override
    public void onReceive(Context context, Intent intent) {
        CommonUtil.wtfd(TAG, "onReceive.. time changed");

        String oldTimezone = CommonUtil.getInstance().getPrefContent(CommonUtil.PREF_TIMEZONE);
        String newTimezone = TimeZone.getDefault().getID();
        CAApplication.getInstance().getStorageCommon().callBackDoseCallBack(true);
        CAApplication.getInstance().getStorageCommon().notifyDialogCallBack();

        long now = System.currentTimeMillis();

        if (oldTimezone != null && TimeZone.getTimeZone(oldTimezone).getOffset(now) != TimeZone.getTimeZone(newTimezone).getOffset(now)) {
            CommonUtil.wtfe(TAG, "TimeZone time change");
            String value = CommonUtil.getInstance().getPrefContent(CommonUtil.REMINDER_EXIST);

            if (value == null || value.equals("0")) {
                CommonUtil.wtfe(TAG, "Not had reminder");
                return;
            }

            context.sendBroadcast(new Intent(ACTION_SHOW_TIME_ZONE_CHANGE));
        }
    }
}
