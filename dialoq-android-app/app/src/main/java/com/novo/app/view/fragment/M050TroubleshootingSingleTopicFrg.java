package com.novo.app.view.fragment;

import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M050TroubleshootingSingleTopicPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM050TroubleshootingSingleTopicCallBack;

public class M050TroubleshootingSingleTopicFrg extends BaseFragment<M050TroubleshootingSingleTopicPresenter, OnM024DoseLogCallBack> implements OnM050TroubleshootingSingleTopicCallBack {
    public static final String TAG = M050TroubleshootingSingleTopicFrg.class.getName();

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m050_back, this);
        findViewById(R.id.tv_m050_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m050_contact_support, this, CAApplication.getInstance().getMediumFont());

        TextView tvQuestion = findViewById(R.id.tv_m050_question, CAApplication.getInstance().getBoldFont());
        TextView tvAnswer = findViewById(R.id.tv_m050_answer, CAApplication.getInstance().getRegularFont());

        String question = getStorage().getM001ConfigSet().getListQnA().get(getStorage().getM047ItemPosition()).getListQnA().get(0).getKey();
        question = LangMgr.getInstance().getLangList().get(question);
        tvQuestion.setText(question);

        String answer = getStorage().getM001ConfigSet().getListQnA().get(getStorage().getM047ItemPosition()).getListQnA().get(0).getValue();
        answer = LangMgr.getInstance().getLangList().get(answer);
        tvAnswer.setText(answer);
    }

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m050_troubleshooting_single_topic;
    }

    @Override
    protected M050TroubleshootingSingleTopicPresenter getPresenter() {
        return new M050TroubleshootingSingleTopicPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        if (getTagSource().equals(M047SupportTroubleshootingFrg.TAG)
                || getTagSource().equals(M049SupportContactFrg.TAG)) {
            CommonUtil.getInstance().defineBackTag(getTagSource(), TAG);
        }
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m050_back:
                mCallBack.showChildFrgScreen(TAG, M047SupportTroubleshootingFrg.TAG);
                break;
            case R.id.tv_m050_contact_support:
                mCallBack.showChildFrgScreen(TAG, M049SupportContactFrg.TAG);
                break;
        }
    }
}
