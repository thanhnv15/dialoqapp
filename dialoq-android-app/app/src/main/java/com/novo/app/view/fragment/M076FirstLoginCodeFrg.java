/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.view.fragment;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M076FirstLoginPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM076FirstLoginCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.ProgressLoading;

/**
 * This fragment shows an email confirmation screen, guiding user send verification code to his/her
 * email.
 *
 * This acts as `View` in MVP pattern
 * Linked `Presenter` of this fragment is an instance of {@link M076FirstLoginPresenter}
 * Linked `Presenter` will communicate to this `View` using protocol of {@link OnM076FirstLoginCallBack}
 *
 * Linked to other views will be represented by an instance of {@link OnHomeBackToView}, which is a
 * delegation
 */
public class M076FirstLoginCodeFrg extends BaseFragment<M076FirstLoginPresenter, OnHomeBackToView> implements OnM076FirstLoginCallBack {

    // String tag for this class
    public static final String TAG = M076FirstLoginCodeFrg.class.getName();

    /**
     * Initialize fragment view
     */
    @Override
    protected void initViews() {
        findViewById(R.id.iv_m076_email);
        findViewById(R.id.tv_m076_description, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m076_detail, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m076_cancel, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.bt_m076_send_code, this, CAApplication.getInstance().getMediumFont());
        ProgressLoading.dismiss();
    }

    /**
     * Layout to be inflated for this fragment
     * @return id of layout to be inflated
     */
    @Override
    protected int getLayoutId() {
        return R.layout.frg_m076_first_login_code;
    }

    /**
     * Get linked `Presenter` instance of this `View` object
     * @return linked {@link M076FirstLoginPresenter}
     */
    @Override
    protected M076FirstLoginPresenter getPresenter() {

        return new M076FirstLoginPresenter(this);
    }

    /**
     * Show dialog for finishing this fragment with {@link M001LandingPageFrg} screen
     * @param message content message will be show up in dialog
     */
    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    /**
     * Get string tag of this class
     * @return string tag
     */
    @Override
    protected String getTAG() {
        return TAG;
    }

    /**
     * Do nothing with 'Back' key
     */
    @Override
    protected void defineBackKey() {
        //Wait for customer confirmation
    }

    /**
     * There is no `previous screen` of this screen
     * So just left this method empty
     */
    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }

    /**
     * Handle onclick event triggered from view components of this fragment
     * @param idView id of clicked view
     */
    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m076_send_code:
                mPresenter.requestCodeIA04();
                break;

            case R.id.tv_m076_cancel:
                mCallBack.registrationCancel(TAG);
                getStorage().getM001ProfileEntity().setVerifyCode(null);
                getStorage().setM077VerifyCode(null);
                getStorage().setM078ErrorMessage(null);
                break;
            default:
                break;
        }
    }

    /**
     * Handle message from `Presenter` (an instance of {@link M076FirstLoginPresenter})
     * @see OnM076FirstLoginCallBack#showM023Login()
     */
    @Override
    public void showM023Login() {
        mCallBack.showFrgScreen(TAG, M023LoginFrg.TAG);
    }

    /**
     * Handle message from `Presenter` (an instance of {@link M076FirstLoginPresenter})
     * @see OnM076FirstLoginCallBack#verifyEmailFail(String)
     */
    @Override
    public void verifyEmailFail(String sms) {
        mCallBack.showFrgScreen(TAG, M023LoginFrg.TAG);
        CommonUtil.getInstance().backAndNotify(mContext, null, sms);
    }

    /**
     * Handle message from `Presenter` (an instance of {@link M076FirstLoginPresenter})
     * @see OnM076FirstLoginCallBack#verifyEmailSuccess(String)
     */
    @Override
    public void verifyEmailSuccess(String success) {
        if (success.equals(CommonUtil.SUCCESS_VALUE)) {
            mCallBack.showFrgScreen(TAG, M077FirstLoginCodeSentFrg.TAG);
        } else {
            showNotify(LangMgr.getInstance().getLangList().get(getString(R.string.lang_err_unknown_reason)));
        }
    }


}
