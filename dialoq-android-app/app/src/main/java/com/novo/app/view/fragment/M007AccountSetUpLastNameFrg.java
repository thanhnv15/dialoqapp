package com.novo.app.view.fragment;

import android.text.Editable;
import android.widget.Button;
import android.widget.EditText;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M007AccountSetUpLastNamePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM007AccountSetUpLastNameCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.TextAdapter;

public class M007AccountSetUpLastNameFrg extends BaseFragment<M007AccountSetUpLastNamePresenter, OnHomeBackToView> implements OnM007AccountSetUpLastNameCallBack {

    public static final String TAG = M007AccountSetUpLastNameFrg.class.getName();

    private EditText mEditLastName;
    private Button btnNext;

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m007_logo);
        findViewById(R.id.iv_m007_back, this);

        findViewById(R.id.tv_m007_account_setup, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m007_what_name, CAApplication.getInstance().getRegularFont());
        btnNext = findViewById(R.id.bt_m007_next, this, false, CAApplication.getInstance().getMediumFont());

        findViewById(R.id.tv_m007_cancel, this, CAApplication.getInstance().getMediumFont());
        mEditLastName = findViewById(R.id.ed_m007_last_name, CAApplication.getInstance().getRegularFont());
        if (getStorage().getM001ProfileEntity().getLastName() != null) {
            mEditLastName.setText(getStorage().getM001ProfileEntity().getLastName());
            mEditLastName.setTypeface(CAApplication.getInstance().getBoldFont());
        }

        if (!mEditLastName.getText().toString().isEmpty()) btnNext.setEnabled(true);
        mEditLastName.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0 && editable.length() < 100 && !editable.toString().equals(" ")) {
                    btnNext.setEnabled(true);
                    mEditLastName.setTypeface(CAApplication.getInstance().getBoldFont());
                    getStorage().getM001ProfileEntity().setLastName(mEditLastName.getText().toString());
                } else if (editable.length() == 0) {
                    btnNext.setEnabled(false);
                    mEditLastName.setTypeface(CAApplication.getInstance().getRegularFont());
                }
            }
        });
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m007_account_set_up_last_name;
    }

    @Override
    protected M007AccountSetUpLastNamePresenter getPresenter() {
        return new M007AccountSetUpLastNamePresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m007_back:
                CommonUtil.getInstance().forceHideKeyBoard(mEditLastName);
                backToPreviousScreen();
                break;
            case R.id.bt_m007_next:
                CommonUtil.getInstance().forceHideKeyBoard(mEditLastName);
                mCallBack.showFrgScreen(TAG, M009SignUpEmailFrg.TAG);
                getStorage().getM001ProfileEntity().setLastName(mEditLastName.getText().toString());
                break;
            case R.id.tv_m007_cancel:
                CommonUtil.getInstance().forceHideKeyBoard(mEditLastName);
                mCallBack.registrationCancel(TAG);
                break;
            default:
                break;
        }
    }

    @Override
    public void backToPreviousScreen() {
        mCallBack.showFrgScreen(TAG, M005AccountSetUpNameFrg.TAG);
    }
}
