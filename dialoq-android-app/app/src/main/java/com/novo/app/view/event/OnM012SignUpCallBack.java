package com.novo.app.view.event;

public interface OnM012SignUpCallBack extends OnCallBackToView {
    void showM023Login();

    void verifyEmailFail(String message);

    void verifyEmailSuccess(String success);
}
