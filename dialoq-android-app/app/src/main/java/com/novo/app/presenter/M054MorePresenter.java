package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RUserEntity;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM054MoreCallBack;

import static com.novo.app.utils.CommonUtil.P_API_KEY;

public class M054MorePresenter extends BasePresenter<OnM054MoreCallBack> {
    private static final String TAG = M054MorePresenter.class.getName();

    public M054MorePresenter(OnM054MoreCallBack event) {
        super(event);
    }

    public void editUserInDB(UserEntity userEntity) {
        CommonUtil.wtfi(TAG, "editNoteInDB....");
        RUserEntity userProfile = new RUserEntity();

        userProfile.setUserId(userEntity.getUserId());
        userProfile.setFirstName(userEntity.getFirstName());
        userProfile.setLastName(userEntity.getLastName());
        userProfile.setEmail(userEntity.getEmail());
        userProfile.setGender(userEntity.getGender());
        userProfile.setDateOfBirth(userEntity.getDateOfBirth());
        userProfile.setMobilePhoneNumber(userEntity.getMobilePhoneNumber());
        userProfile.setCountry(userEntity.getCountry());
        userProfile.setMedicationHistory(userEntity.getMedicationHistory());
        userProfile.setFamilyHistory(userEntity.getFamilyHistory());
        userProfile.setPersonalHistory(userEntity.getPersonalHistory());
        userProfile.setAllergies(userEntity.getAllergies());
        userProfile.setRace(userEntity.getRace());
        userProfile.setStreetAddress1(userEntity.getStreetAddress1());
        userProfile.setStreetAddress2(userEntity.getStreetAddress2());
        userProfile.setCustom1(userEntity.getCustom1());

        if (CADBManager.getInstance().editRUserEntity(userProfile)) {
            mListener.updateSuccess();
        } else {
            mListener.updateFailed();
        }
    }
}
