package com.novo.app.view.fragment;

import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M023LoginPresenter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM023LoginCallBack;

public class M044PairCompleteFrg extends BaseFragment<M023LoginPresenter, OnHomeBackToView> implements OnM023LoginCallBack {
    public static final String TAG = M044PairCompleteFrg.class.getName();

    @Override
    protected void initViews() {
        getStorage().getM032PairedDevice().clear();
        findViewById(R.id.iv_m044_close, this);
        findViewById(R.id.tv_m044_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m044_guide, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m044_suggest_1, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m044_suggest_2, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m044_suggest_3, CAApplication.getInstance().getRegularFont());

        findViewById(R.id.bt_m044_take_tour, this, CAApplication.getInstance().getMediumFont());
        TextView mTvViewTitle = findViewById(R.id.tv_m044_view_dose_log, this, CAApplication.getInstance().getMediumFont());

        if (getStorage().isM042IsCheckIn()) mTvViewTitle.setText(R.string.txt_m044_view_device);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m044_pair_complete;
    }

    @Override
    protected M023LoginPresenter getPresenter() {
        return new M023LoginPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        // This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        // This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m044_take_tour:
                mCallBack.showFrgScreen(TAG, M028PairVideoFrg.TAG);
                break;
            case R.id.iv_m044_close:
            case R.id.tv_m044_view_dose_log:
                if (getStorage().isM042IsCheckIn()) {
                    getStorage().setM042AddNewDevice(true);
                    getStorage().setPreTAG(M042MoreDevicesFrg.TAG);
                } else {
                    getStorage().setPreTAG(null);
                }
                mCallBack.showFrgScreen(TAG, M024DoseLogFrg.TAG);
                break;
        }
    }

    @Override
    public void showM001LandingScreen(String sms) {
        // This feature isn't require here
    }

    @Override
    public void loginSuccess() {
        mCallBack.showFrgScreen(TAG, M024DoseLogFrg.TAG);
        mCallBack.startScan();
        CAApplication.getInstance().startSyncService();
    }

    @Override
    public void updateFingerPrintResult() {
        // This feature isn't require here
    }

    @Override
    public void showDialog(int txt, String tag) {
        // This feature isn't require here
    }

    @Override
    public void showDialog(String txt, String tag) {
        // This feature isn't require here
    }
}
