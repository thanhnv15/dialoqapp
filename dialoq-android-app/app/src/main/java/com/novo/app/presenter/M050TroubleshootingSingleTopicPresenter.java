package com.novo.app.presenter;

import com.novo.app.view.event.OnM050TroubleshootingSingleTopicCallBack;

public class M050TroubleshootingSingleTopicPresenter extends BasePresenter<OnM050TroubleshootingSingleTopicCallBack> {
    public M050TroubleshootingSingleTopicPresenter(OnM050TroubleshootingSingleTopicCallBack event) {
        super(event);
    }
}
