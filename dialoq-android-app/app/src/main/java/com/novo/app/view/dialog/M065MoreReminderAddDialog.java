/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.view.dialog;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M065AddEditReminderPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseDialog;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.event.OnM065ReminderCallBack;
import com.novo.app.view.widget.ProgressLoading;

import java.util.Calendar;
import java.util.Locale;

public class M065MoreReminderAddDialog extends BaseDialog<M065AddEditReminderPresenter, OnM065ReminderCallBack> implements OnCallBackToView, OnM065ReminderCallBack {
    public static final int EXISTED = 1;
    public static final int EMPTY = 0;
    private TextView mTvAlarm;

    public M065MoreReminderAddDialog(Context context, boolean isCancel) {
        super(context, isCancel, R.style.dialog_style);
    }

    @Override
    public void showM001LandingScreen(String err) {
        mCallBack.showM001LandingScreen(err);
    }


    @Override
    protected void initViews() {
        findViewById(R.id.iv_m065_dialog_back, this);
        TextView mTvTitle = findViewById(R.id.tv_m065_dialog_title, CAApplication.getInstance().getMediumFont());

        findViewById(R.id.tv_m065_reminder_description, CAApplication.getInstance().getRegularFont());
        mTvAlarm = findViewById(R.id.tv_m065_alarm, this, CAApplication.getInstance().getRegularFont());
        mTvAlarm.setText(currentReminder());

        findViewById(R.id.tv_m065_alarm_every_day, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m065_alarm_repeat, CAApplication.getInstance().getRegularFont());

        Button mBtSave = findViewById(R.id.bt_m065_save, this, CAApplication.getInstance().getMediumFont());
        TextView mTvDelete = findViewById(R.id.tv_m065_delete, this, CAApplication.getInstance().getMediumFont());
        mTvDelete.setVisibility(View.GONE);

        if (getStorage().getM065ScheduleEntity() != null) {
            mTvTitle.setText(mContext.getString(R.string.txt_m065_title_edit));
            mBtSave.setText(mContext.getString(R.string.txt_m065_save));
            mTvDelete.setVisibility(View.VISIBLE);
            mTvDelete.setText(mContext.getString(R.string.txt_m065_delete));
        } else {
            mTvTitle.setText(mContext.getString(R.string.txt_m065_title_add));
            mBtSave.setText(mContext.getString(R.string.txt_m065_add));
        }
    }

    private String currentReminder() {
        int hour = CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_HOUR, Calendar.getInstance().get(Calendar.HOUR));
        int min = CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_MIN, Calendar.getInstance().get(Calendar.MINUTE));
        boolean isPM = (hour >= 12);
        return String.format(Locale.getDefault(), "%02d:%02d %s", (hour == 12 || hour == 0) ? 12 : hour % 12, min, isPM ? "PM" : "AM");
    }

    @Override
    public int getLayoutId() {
        return R.layout.view_m065_more_reminder_add;
    }

    @Override
    protected M065AddEditReminderPresenter getPresenter() {
        return new M065AddEditReminderPresenter(this);
    }

    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m065_dialog_back:
                dismiss();
                break;
            case R.id.tv_m065_alarm:
                CommonUtil.getInstance().showTimeChoosing(mContext, mTvAlarm);
                break;
            case R.id.bt_m065_save:
                getStorage().setM065ReminderChanged(true);

                String time = String.format(Locale.getDefault(), "%02d:%02d",
                        CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_HOUR, 12),
                        CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_MIN, 0));

                if (getStorage().getM065ScheduleEntity() == null) {
                    mPresenter.addReminder(time);
                } else {
                    mPresenter.activeReminder(time);
                }

                break;
            case R.id.tv_m065_delete:
                mPresenter.delReminder();
                break;
        }
    }

    @Override
    public void showAlert(String sms) {
        CommonUtil.getInstance().showDialog(mContext, sms, null);
    }

    @Override
    public void closeDialog(String key) {
        switch (key) {
            case M065AddEditReminderPresenter.KEY_API_AS02_CREATE_ASSESSMENT_SCHEDULE:
            case M065AddEditReminderPresenter.KEY_API_AS10_UPDATE_ASSESSMENT_SCHEDULE:
                CommonUtil.getInstance().setAppReminder(mContext);

                ProgressLoading.dismiss();
                mCallBack.showAlert(key);
                break;
            case M065AddEditReminderPresenter.KEY_API_AS09_DELETE_ASSESSMENT_SCHEDULE:
                CommonUtil.getInstance().cancelAppReminder(false, mContext, null);
                mCallBack.toM051More();
                break;
        }
    }

    @Override
    public void toM051More() {
        //This feature isn't require here
    }
}
