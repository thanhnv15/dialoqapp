/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.manager;

import com.novo.app.CAApplication;
import com.novo.app.manager.entities.RDecryptionKeyEntity;
import com.novo.app.manager.entities.RDoseEntity;
import com.novo.app.manager.entities.RInjectionEntity;
import com.novo.app.manager.entities.RInjectionItem;
import com.novo.app.manager.entities.RReminderEntity;
import com.novo.app.manager.entities.RTokenEntity;
import com.novo.app.manager.entities.RUserEntity;
import com.novo.app.model.entities.AssessmentScheduleEntity;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.DecryptionKeyEntity;
import com.novo.app.model.entities.ResultEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.DoseLogAdapter;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

import static com.novo.app.service.SyncScheduleService.KEY_APP_DOSE_ADD_DELETE_TIME;

/**
 * Created by ThanhNv on 6/27/2017.
 */
public class CADBManager extends BaseMgr {
    public static final long VERSION_DB = 1;
    public static final int EDIT_FAILED = 1;
    public static final int EDIT_SUCCESS = 2;
    public static final int EDIT_HAS_BEEN_DELETED = 3;
    private static final String TAG = CADBManager.class.getName();
    private static final String ADD_DOSE_ENTITY = "addRDoseEntity...";
    private static final String ADD_DOSE_OPEN_REALM_FAILED = "addRDoseEntity::Open realm failed...";
    private static final String REALM_DONE = "editRNoteEntity::Realm done...: ";
    private static final String REALM_FAILED = "editRNoteEntity::Realm failed...";
    private static final String ADD_TOKEN_ENTITY = "addRTokenEntity...";
    private static final String ADD_TOKEN_ENTITY_OPEN_REALM_FAIDED = "addRTokenEntity::Open realm failed...";
    private static final String ADD_TO_TOKEN_ENTITY_DEL = "addRTokenEntity...del: ";
    private static final String OPEN_REALM_FAIDED = "Open realm failed...";
    private static final String GET_TOKEN_ENTITY = "getRTokenEntity...";
    private static final String DELETE_NOTE = "deleteRNote...";
    private static final String DELETE_NOTE_OPEN_REALM_FAILED = "deleteRNote::Open realm failed...";
    private static final String CHANGE_STATUS_FLAG_NOTE = "Change status flag ADD";
    private static CADBManager instance;
    private Realm realm;

    /**
     * Create an instance for this object CADBManager, using Singleton pattern design
     *
     * @return instance - CADBManager
     */
    public static CADBManager getInstance() {
        if (instance == null) {
            instance = new CADBManager();
        }
        return instance;
    }

    public static void setInstance(CADBManager cadbManager) {
        instance = cadbManager;
    }

    private boolean openDB() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return false;
        if (realm == null) {
            realm = CAApplication.getInstance().getRealm();
        }
        return realm == null;
    }

    public Realm getDB() {
        return realm;
    }

    public synchronized boolean addRDoseEntity(RDoseEntity data, String type) {
        if (data == null) return false;
        CommonUtil.wtfe(TAG, ADD_DOSE_ENTITY);
        if (openDB()) {
            CommonUtil.wtfe(TAG, ADD_DOSE_OPEN_REALM_FAILED);
            return false;
        }
        if (data.getStatusFlag() != null && data.getResultId() == null) {
            doAddRNoteEntity(data);
            return true;
        }

        RDoseEntity mEntity = realm.where(RDoseEntity.class).equalTo(RDoseEntity.PRIMARY_KEY, data.getCustom2()).findFirst();

        if (mEntity == null) {
            doAddRNoteEntity(data);
            if (getStorage().getM063Note() != null) {
                if (data.getCustom1().equals(getStorage().getM063Note().getResult().getCustom1())) {
                    getStorage().getM063Note().getResult().setCustom2(data.getCustom2());
                    getStorage().getM063Note().getResult().setResultId(data.getResultId());
                }
            }
            return true;
        }

        mEntity = realm.copyFromRealm(mEntity);
        if (type.equals(DoseLogAdapter.TYPE_ASSESSMENT_NOTE) &&
                (Long.parseLong((mEntity.getCustom1() == null || mEntity.getCustom1().isEmpty()) ? "0" : mEntity.getCustom1())
                        <= Long.parseLong((data.getCustom1() == null || data.getCustom1().isEmpty()) ? "0" : data.getCustom1()))) {


            if (Long.parseLong((mEntity.getCustom1() == null || mEntity.getCustom1().isEmpty()) ? "0" : mEntity.getCustom1())
                    == Long.parseLong((data.getCustom1() == null || data.getCustom1().isEmpty()) ? "0" : data.getCustom1())) {
                data.setStatusFlag(mEntity.getStatusFlag());
            }

            if (!realm.isInTransaction()) {
                realm.beginTransaction();
            }
            realm.copyToRealmOrUpdate(data);
            if (realm.isInTransaction()) {
                realm.commitTransaction();
            }

            CommonUtil.wtfi(TAG, mEntity.toString());
            CommonUtil.wtfi(TAG, data.toString());
            return true;
        } else if (!type.equals(DoseLogAdapter.TYPE_ASSESSMENT_NOTE)) {
            doAddRNoteEntity(data);
        }
        return false;
    }

    private synchronized void doAddRNoteEntity(RDoseEntity data) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        if (!realm.isInTransaction()) {
            realm.beginTransaction();
        }
        realm.copyToRealmOrUpdate(data);
        if (realm.isInTransaction()) {
            realm.commitTransaction();
        }
    }

    public synchronized int editRNoteEntity(RDoseEntity data) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return EDIT_FAILED;

        if (data == null) return EDIT_FAILED;
        try {
            RDoseEntity mEntity = realm.where(RDoseEntity.class)
                    .beginGroup()
                    .equalTo(RDoseEntity.PRIMARY_KEY, data.getCustom2())
                    .or()
                    .equalTo(RDoseEntity.ID, data.getResultId() == null ? "" : data.getResultId())
                    .endGroup()
                    .findFirst();
            if (mEntity == null) {
                CommonUtil.wtfi(TAG, "editRNoteEntity: getCustom2: " + data.getCustom2());
                return EDIT_HAS_BEEN_DELETED;
            }

            if (!realm.isInTransaction()) {
                realm.beginTransaction();
            }

            RDoseEntity item = realm.copyFromRealm(mEntity);
            item.setStatusFlag(item.getResultId() != null && (item.getStatusFlag() == null || item.getStatusFlag().isEmpty()
                    || item.getStatusFlag().equals(RDoseEntity.STATE_EDIT))
                    ? RDoseEntity.STATE_EDIT : RDoseEntity.STATE_ADD);
            String timeStamp = System.currentTimeMillis() + "";
            item.setCustom1(timeStamp);
            item.setNotes(data.getNotes());
            item.setReportedDate(data.getReportedDate());
            item.setAssessmentType(data.getAssessmentType());
            item.setDeviceId(data.getDeviceId());
            item.setAdherenceStatus(data.getAdherenceStatus());
            item.setActivityStatus(data.getActivityStatus());
            item.setActivityId(data.getActivityId());
            item.setActivityDate(data.getActivityDate());
            item.setName(data.getName());

            RDoseEntity rs = realm.copyToRealmOrUpdate(item);

            if (realm.isInTransaction()) {
                realm.commitTransaction();
            }
            CommonUtil.wtfe(TAG, REALM_DONE + rs);
            return EDIT_SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtil.wtfe(TAG, REALM_FAILED);
        }
        return EDIT_FAILED;
    }

    public synchronized void addRTokenEntity(RTokenEntity data) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        if (data == null) return;

        CommonUtil.wtfe(TAG, ADD_TOKEN_ENTITY);
        if (openDB()) {
            CommonUtil.wtfe(TAG, ADD_TOKEN_ENTITY_OPEN_REALM_FAIDED);
            return;
        }

        if (!realm.isInTransaction()) {
            realm.beginTransaction();
        }
        RTokenEntity result = realm.where(RTokenEntity.class).findFirst();
        if (result != null) result.deleteFromRealm();
        CommonUtil.wtfe(TAG, ADD_TO_TOKEN_ENTITY_DEL);
        realm.copyToRealm(data);
        if (realm.isInTransaction()) {
            realm.commitTransaction();
        }
    }

    public synchronized RTokenEntity getRTokenEntity() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return null;

        CommonUtil.wtfe(TAG, GET_TOKEN_ENTITY);
        if (openDB()) {
            CommonUtil.wtfe(TAG, OPEN_REALM_FAIDED);
            return null;
        }

        return realm.where(RTokenEntity.class).findFirst();
    }

    /**
     * This method will get all Dose items from RealmDB by type
     *
     * @param type String (INJECTION_ASSESSMENT_TYPE, OTHERS_ASSESSMENT_TYPE, DEVICE_ASSESSMENT_TYPE
     * @return RDoseEntity
     */
    public synchronized List<RDoseEntity> getAllRDoseEntity(String type) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return new ArrayList<>();

        CommonUtil.wtfe(TAG, "getAllRDoseEntity...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, REALM_FAILED);
            return new ArrayList<>();
        }

        RealmResults<RDoseEntity> mRealmResultNotify = realm.where(RDoseEntity.class).equalTo(RDoseEntity.ASSESSMENT_TYPE, type).findAll();
        if (mRealmResultNotify == null || mRealmResultNotify.isEmpty()) return new ArrayList<>();
        return realm.copyFromRealm(mRealmResultNotify);
    }

    public synchronized List<RDecryptionKeyEntity> getAllNNDMEntity() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return new ArrayList<>();

        CommonUtil.wtfe(TAG, "getAllRDoseEntity...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, OPEN_REALM_FAIDED);
            return new ArrayList<>();
        }

        RealmResults<RDecryptionKeyEntity> mRealmResultNotify = realm.where(RDecryptionKeyEntity.class).findAll();
        if (mRealmResultNotify == null || mRealmResultNotify.isEmpty()) return new ArrayList<>();
        return realm.copyFromRealm(mRealmResultNotify);
    }

    /**
     * Return all Note items from RealmDB
     *
     * @return RDoseEntity
     */
    public synchronized List<RDoseEntity> getAllNote() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return new ArrayList<>();

        CommonUtil.wtfe(TAG, "getAllNote...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, OPEN_REALM_FAIDED);
            return new ArrayList<>();
        }

        RealmResults<RDoseEntity> mRealmNoteNotify = realm.where(RDoseEntity.class)
                .equalTo(RDoseEntity.ASSESSMENT_TYPE, CommonUtil.NOTES_ASSESSMENT_TYPE)
                .notEqualTo(RDoseEntity.STATUS_FLAG, RDoseEntity.STATE_DEL).findAll();
        if (mRealmNoteNotify == null || mRealmNoteNotify.isEmpty()) return new ArrayList<>();
        return realm.copyFromRealm(mRealmNoteNotify);
    }

    public synchronized List<RDoseEntity> getAllNotNullNote() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return new ArrayList<>();

        CommonUtil.wtfe(TAG, "getAllNotNullNote...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, OPEN_REALM_FAIDED);
            return new ArrayList<>();
        }

        RealmResults<RDoseEntity> mRealmNoteNotify = realm.where(RDoseEntity.class)
                .equalTo(RDoseEntity.ASSESSMENT_TYPE, DoseLogAdapter.TYPE_ASSESSMENT_NOTE).isNotNull(RDoseEntity.STATUS_FLAG).findAll();
        if (mRealmNoteNotify == null || mRealmNoteNotify.isEmpty()) return new ArrayList<>();
        return realm.copyFromRealm(mRealmNoteNotify);
    }

    private synchronized void deleteAllRDose(String type) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        CommonUtil.wtfe(TAG, "deleteAllRDose...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, "deleteAllRDose::Open realm failed...");
            return;
        }

        if (!realm.isInTransaction()) {
            realm.beginTransaction();
        }

        RealmResults<RDoseEntity> result = realm.where(RDoseEntity.class)
                .isNotNull(RDoseEntity.ID)
                .equalTo(RDoseEntity.ASSESSMENT_TYPE, type)
                .findAll();
        boolean rs = result.deleteAllFromRealm();
        CommonUtil.wtfe(TAG, "deleteAllRDose...rs: " + rs);
        if (realm.isInTransaction()) {
            realm.commitTransaction();
        }
    }

    public synchronized boolean deleteRNote(DataInfo data) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return false;

        CommonUtil.wtfe(TAG, DELETE_NOTE);
        if (openDB()) {
            CommonUtil.wtfe(TAG, DELETE_NOTE_OPEN_REALM_FAILED);
            return false;
        }

        if (!realm.isInTransaction()) {
            realm.beginTransaction();
        }

        RDoseEntity result = realm.where(RDoseEntity.class)
                .equalTo(RDoseEntity.ASSESSMENT_TYPE, DoseLogAdapter.TYPE_ASSESSMENT_NOTE)
                .beginGroup()
                .equalTo(RDoseEntity.PRIMARY_KEY, data.getResult().getCustom2())
                .or()
                .equalTo(RDoseEntity.ID, data.getResult().getResultId() == null ? "" : data.getResult().getResultId())
                .endGroup()
                .findFirst();

        if (result == null) return true;
        if (result.getResultId() != null && (result.getStatusFlag() == null
                || result.getStatusFlag().equals(RDoseEntity.STATE_EDIT))) {
            result.setStatusFlag(RDoseEntity.STATE_DEL);
            result.setCustom1(System.currentTimeMillis() + "");
            CommonUtil.wtfe(TAG, "deleteRNote...change to state: DEL");
        } else {
            result.deleteFromRealm();
            CommonUtil.wtfe(TAG, "deleteRNote...remove from DB");
        }
        if (realm.isInTransaction()) {
            realm.commitTransaction();
        }
        return true;
    }

    public synchronized void deleteAllRTokenEntity() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        CommonUtil.wtfe(TAG, "deleteAllRTokenEntity...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, "deleteAllRTokenEntity::Open realm failed...");
            return;
        }

        realm.beginTransaction();
        RTokenEntity result = realm.where(RTokenEntity.class).findFirst();
        if (result == null) return;
        result.deleteFromRealm();
        CommonUtil.wtfe(TAG, "deleteAllRTokenEntity...rs: ");
        if (realm.isInTransaction()) {
            realm.commitTransaction();
        }
    }

    /**
     * Add an ResultEntity into the database
     *
     * @param entity
     * @return
     */
    public synchronized List<RDoseEntity> addRDoseEntity(ResultEntity entity) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return new ArrayList<>();

        if (entity == null) return new ArrayList<>();
        if (entity.getEvents() == null) return new ArrayList<>();
        if (entity.getEvents().getData() == null) return new ArrayList<>();
        List<RDoseEntity> entities = new ArrayList<>();
        for (int i = 0; i < entity.getEvents().getData().length; i++) {
            DataInfo item = entity.getEvents().getData()[i];
            RDoseEntity noteEntity = new RDoseEntity();
            noteEntity.setCustom3(item.getResult().getCustom3());
            noteEntity.setCustom4(item.getResult().getCustom4());
            noteEntity.setCustom2(item.getResult().getCustom2());
            noteEntity.setCustom1(item.getResult().getCustom1());
            noteEntity.setDeviceId(item.getResult().getDeviceId());
            noteEntity.setResult(item.getResult().getResult());

            noteEntity.setAssessmentId(entity.getAssessmentId() + "");
            noteEntity.setResultId(item.getResult().getResultId());
            noteEntity.setStatusFlag(null);
            noteEntity.setNotes(item.getResult().getAssessmentType().equals(DoseLogAdapter.TYPE_ASSESSMENT_NOTE)
                    ? CommonUtil.getInstance().decodeNoteContent(item.getResult().getNotes()) : item.getResult().getNotes());
            noteEntity.setReportedDate(item.getResult().getReportedDate());
            noteEntity.setAssessmentType(item.getResult().getAssessmentType());

            noteEntity.setName(item.getName());
            noteEntity.setScheduleId(item.getScheduleId());
            noteEntity.setActivityDate(item.getActivityDate());
            noteEntity.setActivityId(item.getActivityId());
            noteEntity.setActivityStatus(item.getActivityStatus());
            noteEntity.setAdherenceStatus(item.getAdherenceStatus());

            addRDoseEntity(noteEntity, entity.getType());

            entities.add(noteEntity);
        }
        return entities;
    }

    public synchronized void deleteAllADDRNote(String type, String timeS) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        long time;
        try {
            time = Long.parseLong(timeS);
        } catch (Exception e) {
            return;
        }

        CommonUtil.wtfe(TAG, DELETE_NOTE);
        if (openDB()) {
            CommonUtil.wtfe(TAG, DELETE_NOTE_OPEN_REALM_FAILED);
            return;
        }

        if (!realm.isInTransaction()) {
            realm.beginTransaction();
        }

        RealmResults<RDoseEntity> results = realm.where(RDoseEntity.class)
                .equalTo(RDoseEntity.ASSESSMENT_TYPE, type)
                .isNotNull(RDoseEntity.STATUS_FLAG)
                .and().equalTo(RDoseEntity.STATUS_FLAG, RDoseEntity.STATE_ADD).findAll();
        if (results == null) return;

        List<RDoseEntity> listRS = realm.copyFromRealm(results);
        int count = 0;

        CommonUtil.wtfe(TAG, "deleteRNote...all TIME: " + time);
        for (int i = 0; i < listRS.size(); i++) {
            if (listRS.get(i).getCustom1() == null || listRS.get(i).getCustom1().isEmpty()
                    || Long.parseLong(listRS.get(i).getCustom1()) < time) {
                results.get(i).deleteFromRealm();
                CommonUtil.wtfe(TAG, "deleteRNote...all ADD ITEM: " + listRS.get(i));

                listRS.remove(i);
                i--;
                count++;
            }
        }

        CommonUtil.wtfe(TAG, "deleteRNote...all ADD: " + count);
        if (realm.isInTransaction()) {
            realm.commitTransaction();
        }
    }

    public synchronized void changeStatusFlagAllADDRNote(String type, String timeS) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        long time;
        try {
            time = Long.parseLong(timeS);
        } catch (Exception e) {
            return;
        }

        CommonUtil.wtfe(TAG, CHANGE_STATUS_FLAG_NOTE);
        if (openDB()) {
            CommonUtil.wtfe(TAG, CHANGE_STATUS_FLAG_NOTE);
            return;
        }

        if (!realm.isInTransaction()) {
            realm.beginTransaction();
        }

        RealmResults<RDoseEntity> results = realm.where(RDoseEntity.class)
                .equalTo(RDoseEntity.ASSESSMENT_TYPE, type)
                .isNotNull(RDoseEntity.STATUS_FLAG)
                .and().equalTo(RDoseEntity.STATUS_FLAG, RDoseEntity.STATE_ADD).findAll();
        if (results == null) return;

        List<RDoseEntity> listRS = realm.copyFromRealm(results);
        int count = 0;

        CommonUtil.wtfe(TAG, "changeStatus...all TIME: " + time);
        for (int i = 0; i < listRS.size(); i++) {
            if (listRS.get(i).getCustom1() == null || listRS.get(i).getCustom1().isEmpty()
                    || Long.parseLong(listRS.get(i).getCustom1()) < time) {

                listRS.get(i).setStatusFlag(null);
                realm.copyToRealmOrUpdate(listRS.get(i));
                CommonUtil.wtfe(TAG, "changeStatus...all ADD ITEM: " + listRS.get(i));
                count++;
            }
        }

        CommonUtil.wtfe(TAG, "changeStatus...all ADD: " + count);
        if (realm.isInTransaction()) {
            realm.commitTransaction();
        }
    }

    public synchronized void resetFlagNote(String resultId) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        CommonUtil.wtfi(TAG, "resetFlagNote..." + resultId);
        if (resultId == null) return;
        try {
            RDoseEntity mEntity = realm.where(RDoseEntity.class).equalTo(RDoseEntity.ID, resultId).findFirst();
            if (mEntity == null) {
                return;
            }
            if (!realm.isInTransaction()) {
                realm.beginTransaction();
            }

            RDoseEntity item = realm.copyFromRealm(mEntity);
            item.setStatusFlag(null);
            realm.copyToRealmOrUpdate(item);

            if (realm.isInTransaction()) {
                realm.commitTransaction();
            }

            CommonUtil.wtfe(TAG, REALM_DONE);
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtil.wtfe(TAG, REALM_FAILED);
        }
    }

    public synchronized void resetFlagReminder() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        try {

            RReminderEntity mEntity = realm.where(RReminderEntity.class).findFirst();
            if (mEntity == null) {
                return;
            }
            if (!realm.isInTransaction()) {
                realm.beginTransaction();
            }

            RReminderEntity item = realm.copyFromRealm(mEntity);
            item.setStatusFlag(null);
            mEntity.deleteFromRealm();

            realm.copyToRealm(item);
            if (realm.isInTransaction()) {
                realm.commitTransaction();
            }

            CommonUtil.wtfe(TAG, "resetFlagReminder::Realm done...");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtil.wtfe(TAG, "resetFlagReminder::Realm failed...");
        }
    }

    public void deleteRNote(String resultId) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        if (resultId == null) return;
        try {
            RDoseEntity mEntity = realm.where(RDoseEntity.class).equalTo(RDoseEntity.ID, resultId).findFirst();
            if (mEntity == null) {
                return;
            }
            if (!realm.isInTransaction()) {
                realm.beginTransaction();
            }

            mEntity.deleteFromRealm();
            if (realm.isInTransaction()) {
                realm.commitTransaction();
            }

            CommonUtil.wtfe(TAG, "deleteRNote::Realm done...");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtil.wtfe(TAG, "deleteRNote::Realm failed...");
        }
    }

    public boolean addRInjectionEntity(RInjectionEntity entity) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return false;

        if (entity == null) return false;

        CommonUtil.wtfe(TAG, "addRInjectionEntity...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, "addRInjectionEntity::Open realm failed...");
            return false;
        }
        if (!realm.isInTransaction()) {
            realm.beginTransaction();
        }
        RInjectionEntity item = realm.copyToRealm(entity);
        if (realm.isInTransaction()) {
            realm.commitTransaction();
        }
        return item != null;
    }

    public void deleteAllInjection() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        CommonUtil.wtfe(TAG, "deleteAllInjection...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, "deleteAllInjection::Open realm failed...");
            return;
        }

        String timeS = CommonUtil.getInstance().getPrefContent(KEY_APP_DOSE_ADD_DELETE_TIME);
        long time = System.currentTimeMillis();
        try {
            time = Long.parseLong(timeS);
        } catch (Exception ignore) {
        }

        realm.beginTransaction();
        RealmResults<RInjectionEntity> result = realm.where(RInjectionEntity.class).findAll();
        List<RInjectionEntity> listRS = realm.copyFromRealm(result);
        int count = 0;
        for (int i = 0; i < listRS.size(); i++) {
            if (listRS.get(i).getTime() < time) {
                result.get(i).deleteFromRealm();
                listRS.remove(i);

                i--;
                count++;
            }
        }

        boolean rs = result.deleteAllFromRealm();
        CommonUtil.wtfe(TAG, "deleteAllInjection...rs: " + rs + ": " + count);
        if (realm.isInTransaction()) {
            realm.commitTransaction();
        }
    }

    public void deleteAllInjectionItem() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        CommonUtil.wtfe(TAG, "deleteAllInjection...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, "deleteAllInjection::Open realm failed...");
            return;
        }

        String timeS = CommonUtil.getInstance().getPrefContent(KEY_APP_DOSE_ADD_DELETE_TIME);
        long time = System.currentTimeMillis();
        try {
            time = Long.parseLong(timeS);
        } catch (Exception ignore) {
        }

        realm.beginTransaction();
        RealmResults<RInjectionItem> result = realm.where(RInjectionItem.class).findAll();
        List<RInjectionItem> listRS = realm.copyFromRealm(result);
        int count = 0;
        for (int i = 0; i < listRS.size(); i++) {
            if (listRS.get(i).getCustom2() != null || listRS.get(i).getCustom2().isEmpty() || Long.parseLong(listRS.get(i).getCustom2()) < time) {
                result.get(i).deleteFromRealm();
                listRS.remove(i);

                i--;
                count++;
            }
        }

        boolean rs = result.deleteAllFromRealm();
        CommonUtil.wtfe(TAG, "deleteAllInjectionItem...rs: " + rs + ": " + count);
        if (realm.isInTransaction()) {
            realm.commitTransaction();
        }
    }

    public void deleteAllRDose() {
        deleteAllRDose(CommonUtil.INJECTION_ASSESSMENT_TYPE);
        deleteAllRDose(CommonUtil.DEVICE_ASSESSMENT_TYPE);
        deleteAllRDose(CommonUtil.OTHERS_ASSESSMENT_TYPE);
    }

    public synchronized void addRUserEntity(UserEntity entity) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        if (entity == null) return;
        try {
            RUserEntity mEntity = realm.where(RUserEntity.class)
                    .equalTo(RUserEntity.ID, entity.getUserId()).and().isNotNull(RUserEntity.ID).findFirst();
            if (mEntity == null
                    || entity.getCustom1() == null || entity.getCustom1().isEmpty()
                    || Long.parseLong(entity.getCustom1()) > Long.parseLong(mEntity.getCustom1())
                    || mEntity.getCustom1().isEmpty()) {
                if (!realm.isInTransaction()) {
                    realm.beginTransaction();
                }
                RUserEntity item = (mEntity != null ? realm.copyFromRealm(mEntity) : new RUserEntity());

                item.setUserId(entity.getUserId());
                item.setStatusFlag("");
                item.setCustom1(entity.getCustom1());
                item.setFirstName(entity.getFirstName());
                item.setLastName(entity.getLastName());
                item.setEmail(entity.getEmail());
                item.setGender(entity.getGender());
                item.setDateOfBirth(entity.getDateOfBirth());
                item.setMobilePhoneNumber(entity.getMobilePhoneNumber());
                item.setCountry(entity.getCountry());
                item.setMedicationHistory(entity.getMedicationHistory());
                item.setFamilyHistory(entity.getFamilyHistory());
                item.setPersonalHistory(entity.getPersonalHistory());
                item.setAllergies(entity.getAllergies());
                item.setRace(entity.getRace());
                item.setStreetAddress1(entity.getStreetAddress1());
                item.setStreetAddress2(entity.getStreetAddress2());
                realm.copyToRealmOrUpdate(item);
                if (realm.isInTransaction()) {
                    realm.commitTransaction();
                }
                CommonUtil.wtfe(TAG, "addUserEntity::Realm done...");
            } else {
                CommonUtil.wtfe(TAG, "addUserEntity::UserEntity existed...");
            }

        } catch (Exception e) {
            e.printStackTrace();
            CommonUtil.wtfe(TAG, "addUserEntity::Realm failed...");
        }
    }

    public synchronized boolean editRUserEntity(RUserEntity data) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return false;

        if (data == null) return false;
        try {
            RUserEntity mEntity = realm.where(RUserEntity.class)
                    .equalTo(RUserEntity.ID, data.getUserId()).and().isNotNull(RUserEntity.ID).findFirst();
            if (mEntity == null) {
                return false;
            }
            if (!realm.isInTransaction()) {
                realm.beginTransaction();
            }

            RUserEntity item = realm.copyFromRealm(mEntity);

            item.setStatusFlag(RUserEntity.STATE_UPDATE);
            String timeStamp = System.currentTimeMillis() + "";
            item.setCustom1(timeStamp);
            item.setFirstName(data.getFirstName());
            item.setLastName(data.getLastName());
            item.setEmail(data.getEmail());
            item.setGender(data.getGender());
            item.setDateOfBirth(data.getDateOfBirth());
            item.setMobilePhoneNumber(data.getMobilePhoneNumber());
            item.setCountry(data.getCountry());
            item.setMedicationHistory(data.getMedicationHistory());
            item.setFamilyHistory(data.getFamilyHistory());
            item.setPersonalHistory(data.getPersonalHistory());
            item.setAllergies(data.getAllergies());
            item.setRace(data.getRace());
            item.setStreetAddress1(data.getStreetAddress1());
            item.setStreetAddress2(data.getStreetAddress2());
            realm.copyToRealmOrUpdate(item);
            if (realm.isInTransaction()) {
                realm.commitTransaction();
            }

            CommonUtil.wtfe(TAG, "editUserEntity::Realm done...");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtil.wtfe(TAG, "editUserEntity::Realm failed...");
        }
        return false;
    }

    public synchronized RUserEntity getRUserProfile() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return null;

        CommonUtil.wtfe(TAG, "getRUserProfileFromDB...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, OPEN_REALM_FAIDED);
            return null;
        }

        RealmResults<RUserEntity> mRealmUserProfile = realm.where(RUserEntity.class).findAll();
        if (mRealmUserProfile == null || mRealmUserProfile.isEmpty()) return null;
        return mRealmUserProfile.first();
    }

    public synchronized void resetFlagUserProfile(String userId) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        if (userId == null) return;
        try {
            RUserEntity mEntity = realm.where(RUserEntity.class).equalTo(RUserEntity.ID, userId).findFirst();
            if (mEntity == null) {
                return;
            }
            if (!realm.isInTransaction()) {
                realm.beginTransaction();
            }

            RUserEntity item = realm.copyFromRealm(mEntity);
            item.setStatusFlag("");
            realm.copyToRealmOrUpdate(item);

            if (realm.isInTransaction()) {
                realm.commitTransaction();
            }

            CommonUtil.wtfe(TAG, "resetFlagUserProfile::Realm done...");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtil.wtfe(TAG, "resetFlagUserProfile::Realm failed...");
        }
    }

    public void setupUserProfile(UserEntity item) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;
        if (item == null) return;
        try {
            RUserEntity data = getRUserProfile();
            item.setCustom1(data.getCustom1());
            item.setFirstName(data.getFirstName());
            item.setLastName(data.getLastName());
            item.setEmail(data.getEmail());
            item.setGender(data.getGender());
            item.setDateOfBirth(data.getDateOfBirth());
            item.setMobilePhoneNumber(data.getMobilePhoneNumber());
            item.setCountry(data.getCountry());
            item.setMedicationHistory(data.getMedicationHistory());
            item.setFamilyHistory(data.getFamilyHistory());
            item.setPersonalHistory(data.getPersonalHistory());
            item.setAllergies(data.getAllergies());
            item.setRace(data.getRace());
            item.setStreetAddress1(data.getStreetAddress1());
            item.setStreetAddress2(data.getStreetAddress2());

            CommonUtil.wtfe(TAG, "setupUserProfile::Done...");
        } catch (Exception e) {
            e.printStackTrace();
            CommonUtil.wtfe(TAG, "setupUserProfile::Failed...");
        }
    }

    public void deleteRUserProfile() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        CommonUtil.wtfe(TAG, "deleteRUserProfile...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, "deleteRUserProfile::Open realm failed...");
            return;
        }

        if (!realm.isInTransaction()) {
            realm.beginTransaction();
        }

        RealmResults<RUserEntity> result = realm.where(RUserEntity.class).findAll();
        boolean rs = result.deleteAllFromRealm();
        CommonUtil.wtfe(TAG, "deleteRUserProfile...rs: " + rs);
        if (realm.isInTransaction()) {
            realm.commitTransaction();
        }
    }

    public RReminderEntity addRReminder(AssessmentScheduleEntity data) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return null;

        if (data == null) return null;
        try {
            RReminderEntity mEntity = realm.where(RReminderEntity.class).findFirst();

            //add to db
            boolean isOkDate = false;
            if (mEntity != null) {
                try {
                    //for case: 2 times is equivalent
                    if (Long.parseLong(data.getData().get(0).getScheduleName()) == Long.parseLong(mEntity.getScheduleName())
                            && mEntity.getStatusFlag() != null && mEntity.getStatusFlag().equals(RReminderEntity.STATE_DEL)) {
                        return realm.copyFromRealm(mEntity);
                    }

                    isOkDate = Long.parseLong(data.getData().get(0).getScheduleName()) >= Long.parseLong(mEntity.getScheduleName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (!realm.isInTransaction()) {
                realm.beginTransaction();
            }

            if (mEntity == null || mEntity.getStatusFlag() == null || isOkDate) {
                RReminderEntity item = new RReminderEntity();

                if (mEntity != null) {
                    item = realm.copyFromRealm(mEntity);
                    mEntity.deleteFromRealm();
                }

                AssessmentScheduleEntity.DataInfo entity = data.getData().get(0);
                AssessmentScheduleEntity.DataInfo.ScheduleNotification noti = entity.getScheduleNotifications().get(0);

                item.setEndDate(entity.getEndDate());
                item.setFreqInterval(entity.getFreqInterval());
                item.setFreqSubdayType(entity.getFreqSubdayType());
                item.setNotificationChannel(noti.getNotificationChannel());
                item.setFreqType(entity.getFreqType());
                item.setNotificationType(noti.getNotificationType());
                item.setReminderTime(noti.getReminderTime());
                item.setScheduleId(entity.getScheduleId() + "");
                item.setScheduleName(entity.getScheduleName());
                item.setScheduleNotificationId(noti.getScheduleNotificationId());
                item.setStartDate(entity.getStartDate());
                item.setStartTime(entity.getStartTime());
                item.setStatusFlag(null);

                realm.copyToRealm(item);
                if (realm.isInTransaction()) {
                    realm.commitTransaction();
                }
                return item;
            }
            return realm.copyFromRealm(mEntity);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean editRReminderEntity(RReminderEntity data) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return false;

        if (data == null) return false;
        CommonUtil.wtfi(TAG, "editRReminderEntity..." + data.getStatusFlag());
        try {
            RealmResults<RReminderEntity> mListEntity = realm.where(RReminderEntity.class).findAll();
            if (mListEntity == null) {
                return false;
            }

            if (!realm.isInTransaction()) {
                realm.beginTransaction();
            }

            mListEntity.deleteAllFromRealm();
            realm.copyToRealm(data);
            if (realm.isInTransaction()) {
                realm.commitTransaction();
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public RReminderEntity getRReminderEntity() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return null;

        CommonUtil.wtfe(TAG, "getAllRReminderEntity...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, OPEN_REALM_FAIDED);
            return null;
        }

        RReminderEntity mRReminderEntity = realm.where(RReminderEntity.class).findFirst();
        if (mRReminderEntity == null) return null;
        return realm.copyFromRealm(mRReminderEntity);
    }

    public synchronized boolean addRReminderEntity(RReminderEntity data) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return true;

        if (data == null) return false;

        CommonUtil.wtfe(TAG, "addRReminderEntity...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, "addRReminderEntity::Open realm failed...");
            return false;
        }
        RReminderEntity entity = getRReminderEntity();
        if (entity == null) {
            if (!realm.isInTransaction()) {
                realm.beginTransaction();
            }
            realm.copyToRealm(data);
            if (realm.isInTransaction()) {
                realm.commitTransaction();
            }
        } else {
            entity.setStatusFlag(RReminderEntity.STATE_EDIT);
            entity.setNotificationChannel(data.getNotificationChannel());
            entity.setEndDate(data.getEndDate());
            entity.setReminderTime(data.getReminderTime());
            entity.setNotificationType(data.getNotificationType());
            entity.setScheduleNotificationId(data.getScheduleNotificationId());
            entity.setStartDate(data.getStartDate());
            entity.setFreqInterval(data.getFreqInterval());
            entity.setFreqType(data.getFreqType());
            entity.setFreqSubdayType(data.getFreqSubdayType());
            entity.setStartTime(data.getStartTime());
            entity.setScheduleName(System.currentTimeMillis() + "");

            if (!realm.isInTransaction()) {
                realm.beginTransaction();
            }
            realm.copyToRealmOrUpdate(entity);
            if (realm.isInTransaction()) {
                realm.commitTransaction();
            }
        }
        return true;
    }

    public boolean delReminderEntity() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return false;
        try {
            CommonUtil.wtfe(TAG, "delReminderEntity...");
            if (openDB()) {
                CommonUtil.wtfe(TAG, "delReminderEntity::Open realm failed...");
                return false;
            }

            if (!realm.isInTransaction()) {
                realm.beginTransaction();
            }

            RReminderEntity result = realm.where(RReminderEntity.class).findFirst();
            if (result == null) return false;

            RReminderEntity item = realm.copyFromRealm(result);
            if (item.getStatusFlag() != null) {
                result.deleteFromRealm();
            } else {
                item.setStatusFlag(RReminderEntity.STATE_DEL);
                realm.copyToRealmOrUpdate(item);
            }
            if (realm.isInTransaction()) {
                realm.commitTransaction();
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void delAllReminder() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;
        try {
            CommonUtil.wtfe(TAG, "delReminderEntity...");
            if (openDB()) {
                CommonUtil.wtfe(TAG, "delReminderEntity::Open realm failed...");
                return;
            }

            if (!realm.isInTransaction()) {
                realm.beginTransaction();
            }

            RealmResults<RReminderEntity> rs = realm.where(RReminderEntity.class).findAll();
            if (rs == null) return;

            rs.deleteAllFromRealm();
            if (realm.isInTransaction()) {
                realm.commitTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method will add new an DecryptionKeyEntity entity to RealmDB
     *
     * @param entity DecryptionKeyEntity
     * @return RDecryptionKeyEntity - item get from RealmDB after adding successfully
     */
    public RDecryptionKeyEntity addRNNDMEntity(DecryptionKeyEntity entity) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return null;

        if (entity == null) return null;

        CommonUtil.wtfe(TAG, "addRNNDMEntity...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, "addRNNDMEntity::Open realm failed...");
            return null;
        }

        RDecryptionKeyEntity keyEntity = new RDecryptionKeyEntity();
        keyEntity.setDecryptionKey(entity.getDecryptionKey());
        keyEntity.setSystemId(entity.getSystemId());

        if (!realm.isInTransaction()) {
            realm.beginTransaction();
        }
        realm.copyToRealm(keyEntity);
        if (realm.isInTransaction()) {
            realm.commitTransaction();
        }
        return keyEntity;
    }

    public void deleteAllNNDM() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        CommonUtil.wtfe(TAG, "deleteAllNNDM...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, "deleteAllNNDM::Open realm failed...");
            return;
        }

        if (!realm.isInTransaction()) {
            realm.beginTransaction();
        }

        RealmResults<RDecryptionKeyEntity> result = realm.where(RDecryptionKeyEntity.class).findAll();
        boolean rs = result.deleteAllFromRealm();
        CommonUtil.wtfe(TAG, "deleteAllNNDM...rs: " + rs);
        if (realm.isInTransaction()) {
            realm.commitTransaction();
        }
    }

    public synchronized List<RInjectionEntity> getAllNotSyncDose() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return new ArrayList<>();

        CommonUtil.wtfe(TAG, "getAllNotSyncDose...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, OPEN_REALM_FAIDED);
            return new ArrayList<>();
        }

        RealmResults<RInjectionEntity> mRealmNoteNotify = realm.where(RInjectionEntity.class).sort(RInjectionEntity.TIME).findAll();
        if (mRealmNoteNotify == null || mRealmNoteNotify.isEmpty()) return new ArrayList<>();
        return realm.copyFromRealm(mRealmNoteNotify);
    }

    /**
     * Return all Injection items from RealmDB
     *
     * @return RInjectionEntity
     */
    public List<RInjectionEntity> getAllInjectionItem() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return new ArrayList<>();

        CommonUtil.wtfe(TAG, "getAllInjectionItem...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, OPEN_REALM_FAIDED);
            return new ArrayList<>();
        }

        RealmResults<RInjectionEntity> mRealmResultNotify = realm.where(RInjectionEntity.class).findAll();
        if (mRealmResultNotify == null || mRealmResultNotify.isEmpty()) return new ArrayList<>();
        return realm.copyFromRealm(mRealmResultNotify);
    }

    public void deleteAllDeletedSynRNote(List<ResultEntity> mListSaveData) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        if (mListSaveData == null || mListSaveData.isEmpty()) return;
        CommonUtil.wtfe(TAG, "getAllNotSyncDose...");
        if (openDB()) {
            CommonUtil.wtfe(TAG, OPEN_REALM_FAIDED);
            return;
        }

        RealmResults<RDoseEntity> mRealmNoteNotify = realm.where(RDoseEntity.class).isNotNull(RDoseEntity.ID).findAll();

        if (mRealmNoteNotify == null || mRealmNoteNotify.isEmpty()) return;
        List<RDoseEntity> rs = realm.copyFromRealm(mRealmNoteNotify);

        StringBuilder deletedResultId = new StringBuilder();
        if (!realm.isInTransaction()) {
            realm.beginTransaction();
        }

        for (int i = 0; i < rs.size(); i++) {
            RDoseEntity item = rs.get(i);
            boolean isExist = false;

            for (int j = 0; j < mListSaveData.size(); j++) {
                ResultEntity entity = mListSaveData.get(j);
                if (!entity.getType().equals(DoseLogAdapter.TYPE_ASSESSMENT_NOTE)) continue;
                if (entity.getEvents() == null || entity.getEvents().getData() == null) continue;

                for (int k = 0; k < entity.getEvents().getData().length; k++) {
                    DataInfo data = entity.getEvents().getData()[k];
                    if (data.getResult().getResultId() == null || data.getResult().getResultId().isEmpty())
                        continue;
                    if (item.getResultId().equals(data.getResult().getResultId())) {
                        isExist = true;
                        break;
                    }
                }
                if (isExist) break;
            }

            if (!isExist) {
                try {
                    mRealmNoteNotify.get(i).deleteFromRealm();
                    deletedResultId.append(item.getResultId()).append(",");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (realm.isInTransaction()) {
            realm.commitTransaction();
        }
        CommonUtil.wtfe(TAG, "deleteAllDeletedSynRNote...rs: " + deletedResultId.toString());
    }

    public void updateRTokenEntity(String data) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return;

        if (data == null) return;

        CommonUtil.wtfe(TAG, ADD_TOKEN_ENTITY);
        if (openDB()) {
            CommonUtil.wtfe(TAG, ADD_TOKEN_ENTITY_OPEN_REALM_FAIDED);
            return;
        }

        if (!realm.isInTransaction()) {
            realm.beginTransaction();
        }
        RTokenEntity result = realm.where(RTokenEntity.class).findFirst();

        if (result == null) return;
        RTokenEntity rs = realm.copyFromRealm(result);
        rs.setDataIA02(data);

        result.deleteFromRealm();
        realm.copyToRealm(rs);

        CommonUtil.wtfe(TAG, ADD_TO_TOKEN_ENTITY_DEL + rs);
        if (realm.isInTransaction()) {
            realm.commitTransaction();
        }
    }
}
