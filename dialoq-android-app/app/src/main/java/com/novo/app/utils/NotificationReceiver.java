package com.novo.app.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.novo.app.CAApplication;
import com.novo.app.R;

import java.util.Calendar;
import java.util.Date;

public class NotificationReceiver extends BroadcastReceiver {
    private String TAG = NotificationReceiver.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String mMinPlus7 = CommonUtil.getInstance().getPrefContent(CommonUtil.KEY_NOTIFY_7_DAY);
        String mMinPlus14 = CommonUtil.getInstance().getPrefContent(CommonUtil.KEY_NOTIFY_14_DAY);
        String mCurrent = CommonUtil.getDateNow(CommonUtil.DATE_NOW_DY);

        Date mMin7 = CommonUtil.getInstance().getDateAfter(mMinPlus7);
        Date mMin14 = CommonUtil.getInstance().getDateAfter(mMinPlus14);
        Date mCurrentDate = CommonUtil.getInstance().getDateAfter(mCurrent);

        if (!CAApplication.getInstance().getStorageCommon().isAppActive()) {
            CommonUtil.wtfi(TAG, "App is being block");
            return;
        }
        String email = CAApplication.getInstance().getStorageCommon().getM007UserEntity().getEmail();
        String key = intent.getStringExtra(CommonUtil.KEY_NOTIFY);
        Log.i("Fist dose reminder", "Alarm fired, key = " + key);

        Calendar calendarCurrent = Calendar.getInstance();
        calendarCurrent.setTime(mCurrentDate);
        int getHours = calendarCurrent.get(Calendar.HOUR_OF_DAY);
        int getMinutes = calendarCurrent.get(Calendar.MINUTE);
        int getSeconds = calendarCurrent.get(Calendar.SECOND);

        if (key.equals(CommonUtil.KEY_NOTIFY_7_DAY + email) && mMin7.equals(mCurrentDate)
                && (getHours == CommonUtil.HOUR_REMINDER && getMinutes == CommonUtil.MINUTE_REMINDER && getSeconds == CommonUtil.SECOND_REMINDER)) {
            CommonUtil.wtfi(TAG, "Show Notification after 7 days");
            CommonUtil.getInstance().showNotification(context, context.getString(R.string.txt_noti_dose_title),
                    context.getString(R.string.txt_noti_dose_one_week), CommonUtil.KEY_NOTIFY_7_DAY);

        }
        if (key.equals(CommonUtil.KEY_NOTIFY_14_DAY) && mMin14.equals(mCurrentDate)
                && (getHours == CommonUtil.HOUR_REMINDER && getMinutes == CommonUtil.MINUTE_REMINDER && getSeconds == CommonUtil.SECOND_REMINDER)) {
            CommonUtil.wtfi(TAG, "Show Notification after 14 days");
            CommonUtil.getInstance().showNotification(context, context.getString(R.string.txt_noti_dose_title),
                    context.getString(R.string.txt_noti_dose_two_week), CommonUtil.KEY_NOTIFY_14_DAY);

        }

    }
}
