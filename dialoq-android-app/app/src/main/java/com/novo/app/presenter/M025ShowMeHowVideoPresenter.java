package com.novo.app.presenter;

import com.novo.app.view.event.OnM025ShowMeHowVideoCallBack;

public class M025ShowMeHowVideoPresenter extends BLEPresenter<OnM025ShowMeHowVideoCallBack> {
    public M025ShowMeHowVideoPresenter(OnM025ShowMeHowVideoCallBack event) {
        super(event);
    }
}
