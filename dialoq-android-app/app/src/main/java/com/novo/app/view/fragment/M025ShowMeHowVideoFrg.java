package com.novo.app.view.fragment;

import android.content.res.AssetFileDescriptor;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.ble.BLEInfoEntity;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.presenter.M025ShowMeHowVideoPresenter;
import com.novo.app.presenter.M025UpdatingPresenter;
import com.novo.app.utils.CATask;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseBLEFragment;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM025ShowMeHowVideoCallBack;
import com.novo.app.view.event.OnM025UpdatingCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.widget.ProgressLoading;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.novo.app.model.entities.DeviceEntity.SYS_EOL_WARNING;

/**
 * Created by ThanhNv on 14/12/2017.
 */
public class M025ShowMeHowVideoFrg extends BaseBLEFragment<M025ShowMeHowVideoPresenter, OnM024DoseLogCallBack>
        implements OnM025ShowMeHowVideoCallBack, TextureView.SurfaceTextureListener, MediaPlayer.OnPreparedListener,
        View.OnClickListener, View.OnTouchListener, MediaPlayer.OnCompletionListener, SeekBar.OnSeekBarChangeListener,
        CATask.OnTaskCallBackToView, OnCallBackToView, MediaPlayer.OnVideoSizeChangedListener, OnM025UpdatingCallBack {
    public static final String TAG = M025ShowMeHowVideoFrg.class.getName();
    private static final int PLAY_STATE = 0;
    private static final int STOP_STATE = 1;
    private static final long TIME_HIDE = 3000;
    private static final long TIME_DELAY = 500;
    private static final String KEY_CURRENT_TIME = "KEY_CURRENT_TIME";
    private static final int IDLE = 0;
    private static final int PAUSED = 1;
    private static final int PLAYED = 2;
    private MediaPlayer mPlayer;

    private ImageView ivPlay;
    private TextView tvDuration, tvStart;
    private SeekBar seekBar;
    private TextureView mTextureView;
    private View mTrPlayer;
    private Handler mHandler = new Handler();
    private CATask<M025ShowMeHowVideoFrg, M025ShowMeHowVideoFrg> mTimer;
    private int mState;

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mTrPlayer.setVisibility(View.GONE);
        }
    };
    private BLEInfoEntity mData;

    @Override
    protected void initViews() {
        mCallBack.hideBottomBar(true);
        super.initViews();
        TextView mTvGuide = findViewById(R.id.tv_m025_guide, this, CAApplication.getInstance().getBoldFont());
        if (getStorage().getM025ListDevices().size() > 1) {
            mTvGuide.setText(R.string.txt_m025_press_and_release_one);
        } else {
            mTvGuide.setText(R.string.txt_m025_press_and_release_multi);
        }

        String highLightGuide = mContext.getString(R.string.txt_m025_press_and_release_highlight);
        highLightText(mTvGuide, highLightGuide, R.color.colorM001Cyan);

        mTextureView = findViewById(R.id.texture_m025_video);
        mTextureView.setOnTouchListener(this);
        ivPlay = findViewById(R.id.iv_m025_play);
        tvStart = findViewById(R.id.tv_m025_start);
        mTrPlayer = findViewById(R.id.tr_m025_player);

        findViewById(R.id.iv_m025_close, this);

        ivPlay.setOnClickListener(this);
        tvDuration = findViewById(R.id.tv_m025_duration);
        seekBar = findViewById(R.id.seek_bar_m028);
        seekBar.setOnSeekBarChangeListener(this);
        mTextureView.setSurfaceTextureListener(this);
        mState = IDLE;

        mCallBack.stopBGScan();
    }

    @Override
    public void onDetach() {
        mCallBack.hideBottomBar(false);
        super.onDetach();
    }

    @Override
    protected void doMessage(Message msg) {
        if (msg == null) {
            mScanUtil.startScan();
            return;
        }

        DeviceEntity item = isExistDevicePaired();
        if (item == null) {
            startAgain();
            return;
        }


        mData = (BLEInfoEntity) msg.obj;

        if (mData.getSystemFlag() != null && (mData.getSystemFlag().equals(DeviceEntity.SYS_EOL + "")
                || mData.getSystemFlag().equals(DeviceEntity.SYS_ERR + ""))) {

            CommonUtil.getInstance().showDialog(mContext,
                    CAApplication.getInstance().getString(R.string.app_name),
                    mData.getSystemFlag().equals(DeviceEntity.SYS_EOL + "") ?
                            CAApplication.getInstance().getString(R.string.txt_err_eol) :
                            CAApplication.getInstance().getString(R.string.txt_err_err),
                    CAApplication.getInstance().getString(R.string.txt_sync_success_ok),
                    null, new OnOKDialogCallBack() {
                        @Override
                        public void handleOKButton1() {
                            if (mData.getSystemFlag().equals(DeviceEntity.SYS_EOL + "")) {
                                getStorage().setSystemIdEOL(mDeviceName);
                            }
                            mCallBack.showChildFrgScreen(TAG, M025SubDoseLogFrg.TAG);
                        }
                    });
            return;
        }

        mData.setDeviceName(mDeviceName);
        if (mData.getDoseItems() == null || mData.getDoseItems().isEmpty()) {
            syncSuccess();
            return;
        }
        try {
            int lastNumber = getLastNumber(item.getDeviceID());
            if (lastNumber < mData.getDoseItems().size()) {
                if (mState == PLAYED) {
                    pauseVideo();
                }

                M025UpdatingPresenter presenter = new M025UpdatingPresenter(this);
                presenter.syncDataOffline(item, lastNumber, mData);
            } else {
                syncSuccess();
            }
        } catch (Exception ignored) {

        }
    }

    private int getLastNumber(String deviceID) {
        if (getStorage().getM025ListResult() == null || getStorage().getM025ListResult().isEmpty())
            return 0;
        int max = -1;
        for (int i = 0; i < getStorage().getM025ListResult().size(); i++) {
            DataInfo item = getStorage().getM025ListResult().get(i);
            int number;
            try {
                number = Integer.parseInt(item.getResult().getNotes());
            } catch (Exception e) {
                number = -1;
            }
            if (item.getResult().getCustom1() != null && item.getResult().getCustom1().equals(deviceID) && number > max) {
                max = number;
            }
        }
        return max;
    }

    @Override
    public int getLayoutId() {
        return R.layout.view_m025_show_me_how_video;
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected M025ShowMeHowVideoPresenter getPresenter() {
        return new M025ShowMeHowVideoPresenter(this);
    }

    @Override
    public void showM001LandingScreen(String err) {
        mCallBack.showM001LandingScreen(err);
    }


    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        CommonUtil.wtfi(TAG, "onPrepared....");
        String time = CommonUtil.getInstance().getPrefContent(KEY_CURRENT_TIME);
        if (time != null) {
            try {
                int timeNow = Integer.parseInt(time);
                CommonUtil.getInstance().clearPrefContent(KEY_CURRENT_TIME);
                mPlayer.seekTo(timeNow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        playVideo();

        mTimer = new CATask<>(new WeakReference<>(this), this, TIME_DELAY);
        mTimer.setLoop(true);
        seekBar.setMax(mPlayer.getDuration());
        findViewById(R.id.tr_m025_player).setVisibility(View.VISIBLE);

        mTimer.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        new Handler().postDelayed(() -> findViewById(R.id.v_bg_white).setVisibility(View.GONE), 1000);

        mHandler.postDelayed(mRunnable, TIME_HIDE);
        tvDuration.setText(getDuration(mPlayer.getDuration()));
        ProgressLoading.dismiss();
    }

    private String getDuration(int time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss");
        return dateFormat.format(new Date(time));
    }

    @Override
    public void onPause() {
        if (mState == PLAYED) {
            pauseVideo();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTextureView.setVisibility(View.VISIBLE);
        if (mState == PAUSED) {
            playVideo();
        }
    }

    @Override
    protected void defineBackKey() {

    }

    @Override
    public void onDestroy() {
        stopVideo();
        super.onDestroy();
    }

    @Override
    public void onStop() {
        if (mState == PLAYED) {
            pauseVideo();
        }
        super.onStop();
    }

    public void stopVideo() {
        if (mPlayer != null) {
            mPlayer.reset();
            mPlayer.release();
            mPlayer = null;
            mTimer.cancel();
            mState = IDLE;
        }
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m025_play:
                if (mPlayer != null && mPlayer.isPlaying()) {
                    pauseVideo();
                } else if (mPlayer != null) {
                    playVideo();
                }
                break;
            case R.id.iv_m025_close:
                stopVideo();
                mCallBack.showChildFrgScreen(TAG, M025SubDoseLogFrg.TAG);
                break;
            default:
                break;
        }
    }


    @Override
    public void backToPreviousScreen() {
        // do nothing
    }

    public void pauseVideo() {
        if (mPlayer != null) {
            mPlayer.pause();
            mState = PAUSED;
            ivPlay.setImageLevel(PLAY_STATE);
        }
    }

    private void playVideo() {
        CommonUtil.wtfi(TAG, "playVideo....");
        mPlayer.start();
        mState = PLAYED;
        ivPlay.setImageLevel(STOP_STATE);
    }

    private void initVideo(Surface s) {
        CommonUtil.wtfi(TAG, "initVideo....");
        //get video info
        if (getStorage() == null) return;
        if (getStorage().getM025VideoData() == null) return;
        String videoPath = getStorage().getM025VideoData()[0];
        try (AssetFileDescriptor afd = CAApplication.getInstance().getAssets().openFd(videoPath)) {

            if (mPlayer != null) {
                mPlayer.stop();
                mPlayer.reset();
            }

            mPlayer = new MediaPlayer();
            mPlayer.setOnPreparedListener(this);
            mPlayer.setOnCompletionListener(this);
            mPlayer.setOnVideoSizeChangedListener(this);
            mPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mPlayer.setSurface(s);
            mPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
            ProgressLoading.dismiss();
        }
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (mTrPlayer.getVisibility() == View.GONE) {
                    mTrPlayer.setVisibility(View.VISIBLE);
                    mHandler.removeCallbacks(mRunnable);
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mTrPlayer.getVisibility() == View.VISIBLE) {
                    mHandler.postDelayed(mRunnable, TIME_HIDE);
                }
                break;
        }
        return true;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        ivPlay.setImageLevel(PLAY_STATE);
//        mCallBack.closePushReleaseDialog();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (mPlayer != null) {
            mPlayer.seekTo(seekBar.getProgress());
            playVideo();
        }
    }

    @Override
    public Object doBGTask(String key) {
        try {
            Thread.sleep(TIME_DELAY);
            if (mPlayer == null) {
                mTimer.cancel();
                return false;
            }
            if (mPlayer.isPlaying()) {
                mTimer.updateUI(mPlayer.getCurrentPosition(), mPlayer.getDuration());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void updateUITask(Object[] values) {
        tvStart.setText(getDuration((int) values[0]));
        seekBar.setProgress((int) values[0]);
    }

    @Override
    public void doneTask(Object data) {

    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        ProgressLoading.show(mContext);
        initVideo(new Surface(surface));
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
        CommonUtil.wtfi(TAG, "WIDTH: " + width);
        CommonUtil.wtfi(TAG, "HEIGHT: " + height);
        int wSurface = mTextureView.getWidth();
        int hSurface = mTextureView.getHeight();

        float boxWidth = wSurface;
        float boxHeight = hSurface;
        float wr = boxWidth / (float) width;
        float hr = boxHeight / (float) height;
        float ar = (float) width / (float) height;

        if (wr > hr) {
            wSurface = (int) (boxHeight * ar);
        } else {
            hSurface = (int) (boxWidth / ar);
        }
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(wSurface, hSurface);
        params.gravity = Gravity.CENTER;
        mTextureView.setLayoutParams(params);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mCallBack.startBGScan();
        CommonUtil.wtfe(TAG, "onDestroyView()....");
    }

    @Override
    public void syncSuccess() {
        getStorage().getM025ListResult().clear();
        if (mData != null
                && mData.getDoseItems().size() > 0
                && mData.getSystemFlag().equals(SYS_EOL_WARNING + "")) {
            getStorage().setSystemIdEOLWarning(mDeviceName);
        }
        mCallBack.showChildFrgScreen(TAG, M025SubDoseLogFrg.TAG);
    }

    @Override
    public void syncFailed() {
        CommonUtil.wtfd(TAG, "syncFailed...");
        showAlertDialog("Pen synchronization failed, (saved offline failed) please try again!");
    }
}


