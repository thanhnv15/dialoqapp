/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.view.dialog;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M023FingerPrintPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.utils.FingerprintHandler;
import com.novo.app.view.base.BaseDialog;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.event.OnM023LoginCallBack;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import static android.content.Context.FINGERPRINT_SERVICE;

/**
 * Dialog for guiding users input finger sprint for authentication
 * During appearance of this dialog, user should put his finger on the fingerprint-sensor
 * for authentication
 *
 * MVP pattern:
 * This acts as the 'View' object in MVP pattern
 * This uses an instance of {@link M023FingerPrintPresenter} as linked 'Presenter'
 * Callbacks to this fragment will be defined by {@link OnM023LoginCallBack} interface
 */
public class M023FingerPrintDialog extends BaseDialog<M023FingerPrintPresenter, OnM023LoginCallBack> implements FingerprintHandler.OnActionCallBack, OnCallBackToView {

    /**
     * Shared preference keys for storing secret key, which is required for using {@link FingerprintManager}
     */
    private static final String KEY_NAME = "DIALOQ";
    private static final String KEY_STORE = "AndroidKeyStore";

    // String tag for this dialog
    private static final String TAG = M023FingerPrintDialog.class.getName();

    // View components of dialog
    private TextView mTvUserEmail;

    // Key store used in finger sprint module
    private KeyStore keyStore;

    // Secret-key encoder/decoder used in finger sprint module
    private Cipher cipher;

    // Handler which handler authenticating using fingerprint
    private FingerprintHandler mHelper;

    /**
     * Constructor
     * @param context where this dialog will be shown
     * @param isCancel true if this dialog should be cancellable
     */
    public M023FingerPrintDialog(Context context, boolean isCancel) {
        super(context, isCancel, R.style.dialog_style);
    }

    /**
     * Get an instance of `Presenter` for this dialog (MVP pattern)
     * @return an instance of {@link M023FingerPrintPresenter}
     */
    @Override
    protected M023FingerPrintPresenter getPresenter() {
        return new M023FingerPrintPresenter(this);
    }

    /**
     * Callback when this dialog is going to be displayed
     * Here we start activating fingerprint authentication module
     */
    @Override
    protected void onStart() {
        super.onStart();
        initFingerPrint();
    }

    /**
     * Handle callback from {@link OnM023LoginCallBack}
     * @param err description of error in {@link String}
     */
    @Override
    public void showM001LandingScreen(String err) {
        mCallBack.showM001LandingScreen(err);
    }

    /**
     * Initialize device's fingerprint module and start authentication
     */
    private void initFingerPrint() {
        CommonUtil.wtfi(TAG, "initFingerPrint...");

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            FingerprintManager fingerprintManager = (FingerprintManager) mContext.getSystemService(FINGERPRINT_SERVICE);
            if (fingerprintManager == null || !fingerprintManager.isHardwareDetected()) {
                showNotify(R.string.txt_m005_not_support_fingerprint);
            } else {
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                    showNotify(R.string.txt_m005_not_support_fingerprint);
                } else {
                    if (!fingerprintManager.hasEnrolledFingerprints()) {
                        showNotify(R.string.txt_m005_recommend_fingerprint_setup);
                    } else {
                        generateKey();
                        if (cipherInit()) {
                            FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                            mHelper = new FingerprintHandler(this);
                            mHelper.startAuth(fingerprintManager, cryptoObject);
                        }
                    }
                }
            }
        }
    }

    /**
     * Preparing view components of this dialog
     */
    @Override
    protected void initViews() {
        findViewById(R.id.tv_m023_title_dialog, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m023_guide_dialog, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m023_cancel_dialog, this, CAApplication.getInstance().getRegularFont());
        mTvUserEmail = findViewById(R.id.tv_m023_email_dialog, CAApplication.getInstance().getRegularFont());
    }

    /**
     * Get layout for inflating this dialog's content view
     * {@link BaseDialog#getLayoutId()}
     *
     * @return layout id used for inflating this dialog's view
     */
    @Override
    public int getLayoutId() {
        return R.layout.view_m023_fingerprint;
    }

    /**
     * Handler callback {@link OnM023LoginCallBack#backToPreviousScreen}
     */
    @Override
    public void backToPreviousScreen() {
        //Do nothing here
    }

    /**
     * Handle when some view components of this dialog have been clicked
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_m023_cancel_dialog) {
            dismiss();
        }
    }

    /**
     * Initialize displaying email before showing this dialog
     */
    @Override
    public void show() {
        super.show();
        mTvUserEmail.setText(getStorage().getM001ProfileEntity().getEmail());
        getStorage().setFlagUsedTouchID(true);
    }

    /**
     * Dismiss dialog, also cancel authentication if progressing
     */
    @Override
    public void dismiss() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mHelper != null) {
                mHelper.cancel();
            }
        }
        super.dismiss();
    }


    /**
     * Create a key which is used for {@link FingerprintHandler}
     */
    @TargetApi(Build.VERSION_CODES.M)
    private void generateKey() {
        try {
            keyStore = KeyStore.getInstance(KEY_STORE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, KEY_STORE);
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            e.printStackTrace();
            return;
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Init fingerprint's secret key
     * @return true if secret key initialized successfully
     */
    @TargetApi(Build.VERSION_CODES.M)
    private boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES
                    + "/" + KeyProperties.BLOCK_MODE_CBC + "/"
                    + KeyProperties.ENCRYPTION_PADDING_PKCS7);
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Handle fingerprint authentication result
     * @see FingerprintHandler.OnActionCallBack
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void updateFingerPrintResult(int code, String sms, boolean success) {
        CommonUtil.wtfi(TAG, sms + ": " + success);
        switch (code) {
            case FingerprintHandler.CODE_ERR:
                showNotify(sms);
                break;
            case FingerprintHandler.CODE_FAIL:
                showNotify(sms);
                break;
            case FingerprintHandler.CODE_HELP:
                showNotify(sms);
                break;
            case FingerprintHandler.CODE_SUCCESS:
                if (success) {
                    mCallBack.updateFingerPrintResult();
                }
                break;
            default:
                showNotify(sms);
                break;
        }
    }
}
