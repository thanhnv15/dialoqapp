package com.novo.app.view.fragment;

import android.text.Editable;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableRow;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M013SignUpPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM013SignUpCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.TextAdapter;

public class M013SignUpCodeSentFrg extends BaseFragment<M013SignUpPresenter, OnHomeBackToView> implements OnM013SignUpCallBack {

    public static final String TAG = M013SignUpCodeSentFrg.class.getName();
    private EditText mEdtVerificationCode;
    private Button mBtVerify;
    private TableRow mTrError;
    private int codeLength;

    @Override
    protected void initViews() {
        codeLength = Integer.parseInt(getStorage().getM001ConfigSet().getConfirmCode());
        findViewById(R.id.iv_m013_sent);
        ImageView mIvBack = findViewById(R.id.iv_m013_back, this);
        mIvBack.setVisibility(View.GONE);

        findViewById(R.id.tv_m013_code_sent, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m013_description, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m013_didnt_get, this, CAApplication.getInstance().getMediumFont());

        mBtVerify = findViewById(R.id.bt_m013_verify, this, CAApplication.getInstance().getMediumFont());
        mBtVerify.setEnabled(false);

        mTrError = findViewById(R.id.tr_m013_error);
        mTrError.setVisibility(View.GONE);

        mEdtVerificationCode = findViewById(R.id.tv_m013_detail, CAApplication.getInstance().getRegularFont());
        mEdtVerificationCode.setFilters(new InputFilter[]{new InputFilter.LengthFilter(codeLength)});
        if (getStorage().getM001ProfileEntity().getVerifyCode() != null) {
            mEdtVerificationCode.setText(getStorage().getM001ProfileEntity().getVerifyCode());
            mEdtVerificationCode.setTypeface(CAApplication.getInstance().getBoldFont());

        }
        mEdtVerificationCode.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                mTrError.setVisibility(View.GONE);
                mEdtVerificationCode.setTextColor(getResources().getColor(R.color.colorBlack));
                if (mEdtVerificationCode.getText().length() < codeLength) {
                    mBtVerify.setEnabled(false);
                } else {
                    validateCode();
                }
            }
        });

        if (getStorage().getM014VerifyFail()) {
            getStorage().setM014VerifyFail(false);
            mTrError.setVisibility(View.VISIBLE);
            mIvBack.setVisibility(View.VISIBLE);
            mEdtVerificationCode.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
        }
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    private void validateCode() {
        if (mEdtVerificationCode.getText().toString().isEmpty()) {
            mBtVerify.setEnabled(false);
            mEdtVerificationCode.setTextColor(getResources().getColor(R.color.colorBlack));
            mTrError.setVisibility(View.GONE);
        } else if (mEdtVerificationCode.getText().length() == codeLength) {
            mBtVerify.setEnabled(true);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m013_signup_code_sent;
    }

    @Override
    protected M013SignUpPresenter getPresenter() {

        return new M013SignUpPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //Wait for customer confirmation
    }

    @Override
    public void backToPreviousScreen() {
        mCallBack.showFrgScreen(TAG, M012SignUpEmailCodeFrg.TAG);
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m013_verify:
                getStorage().getM001ProfileEntity().setVerifyCode(mEdtVerificationCode.getText().toString());
                mCallBack.showFrgScreen(TAG, M014SignUpVerifyingCodeFrg.TAG);
                break;

            case R.id.tv_m013_didnt_get:
                CommonUtil.getInstance().forceHideKeyBoard(mEdtVerificationCode);
                mCallBack.showFrgScreen(TAG, M043SignUpDidntGetFrg.TAG);
                break;

            case R.id.iv_m013_back:
                CommonUtil.getInstance().forceHideKeyBoard(mEdtVerificationCode);
                backToPreviousScreen();
                break;
            default:
                break;
        }
    }

    @Override
    public void showM023Login() {
        mCallBack.showFrgScreen(TAG, M023LoginFrg.TAG);
    }
}
