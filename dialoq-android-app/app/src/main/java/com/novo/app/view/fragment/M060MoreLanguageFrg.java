package com.novo.app.view.fragment;

import android.widget.Spinner;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.ConfigEntity;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.presenter.M060MoreLanguagePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.CASpinnerAdapter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnM002SignUpChooseLanguageCallBack;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM060MoreLanguageCallBack;
import com.novo.app.view.event.OnSpinnerCallBack;
import com.novo.app.view.widget.ProgressLoading;

import java.util.List;

public class M060MoreLanguageFrg extends BaseFragment<M060MoreLanguagePresenter, OnM024DoseLogCallBack> implements OnM060MoreLanguageCallBack, OnM002SignUpChooseLanguageCallBack, OnSpinnerCallBack {
    public static final String TAG = M060MoreLanguageFrg.class.getName();
    private static final int KEY_SPINNER_LANGUAGE_TYPE = 1;
    private TextView mTvLanguage;
    private Spinner mSpinnerLanguage;

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m060_back, this);
        findViewById(R.id.tv_m060_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m060_please_choose, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.iv_m060_language, this);
        mTvLanguage = findViewById(R.id.tv_m060_language, this, CAApplication.getInstance().getRegularFont());

        ConfigEntity configEntity = getStorage().getM001ConfigSet();
        mSpinnerLanguage = findViewById(R.id.spin_m060_language);
        CASpinnerAdapter<OnSpinnerCallBack> adapter = new CASpinnerAdapter<>(KEY_SPINNER_LANGUAGE_TYPE,
                configEntity.getLanguageEntity(), mContext);
        adapter.setOnSpinnerCallBack(this);
        mSpinnerLanguage.setAdapter(adapter);
    }

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m060_more_language;
    }

    @Override
    protected M060MoreLanguagePresenter getPresenter() {
        return new M060MoreLanguagePresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m060_back:
                mCallBack.showChildFrgScreen(TAG, M051MoreFrg.TAG);
                break;
            case R.id.tv_m060_language:
            case R.id.iv_m060_language:
                mSpinnerLanguage.performClick();
                break;
        }
    }

    @Override
    public void clickOnItem(TextEntity mData) {
        LangMgr.getInstance().configLanguage(mData);
    }

    @Override
    public void updateView() {
        mCallBack.updateBottomBar();
        mCallBack.showChildFrgScreen(TAG, M025SubDoseLogFrg.TAG);
    }

    @Override
    public void revertChoice() {
        mTvLanguage.setTypeface(CAApplication.getInstance().getRegularFont());
        mTvLanguage.setText(getString(R.string.txt_m060_choose_language));
    }

    @Override
    public void dataLanguageReady(String data) {
        LangMgr.getInstance().dataLanguageReady(data);
    }

    @Override
    public void onResume() {
        super.onResume();
        getStorage().setM002CallBack(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        CommonUtil.wtfe(TAG, "onDestroyView...setM002CallBack null");
        getStorage().setM002CallBack(null);
    }

    @Override
    public void onItemClick(int key, int pos, Object data) {
        mTvLanguage.setTypeface(CAApplication.getInstance().getBoldFont());
        if (key == KEY_SPINNER_LANGUAGE_TYPE) {
            TextEntity entity = ((List<TextEntity>) data).get(pos);
            mTvLanguage.setText(entity.getValue());
            mTvLanguage.setTag(entity);

            LangMgr.getInstance().configLanguage((TextEntity) mTvLanguage.getTag());
            closeSpinner(mSpinnerLanguage);
            ProgressLoading.show(mContext);
        }
    }
}
