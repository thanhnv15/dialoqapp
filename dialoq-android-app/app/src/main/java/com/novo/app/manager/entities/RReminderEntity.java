package com.novo.app.manager.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RReminderEntity extends RealmObject {
    public static final String ID = "scheduleId";
    public static final String STATUS_FLAG = "statusFlag";
    public static final String TIME_STAMP = "custom1";
    public static final String STATE_ADD = "ADD";
    public static final String STATE_EDIT = "EDIT";
    public static final String STATE_DEL = "DEL";

    @PrimaryKey
    private String scheduleId;

    private String scheduleName;
    private String statusFlag;
    private String freqType;
    private String freqInterval;
    private String freqSubdayType;
    private String startTime;
    private String startDate;
    private String endDate;
    private String scheduleNotificationId;
    private String notificationChannel;
    private String notificationType;
    private String reminderTime;

    public String getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(String statusFlag) {
        this.statusFlag = statusFlag;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setFreqType(String freqType) {
        this.freqType = freqType;
    }

    public String getFreqType() {
        return freqType;
    }

    public void setFreqInterval(String freqInterval) {
        this.freqInterval = freqInterval;
    }

    public String getFreqInterval() {
        return freqInterval;
    }

    public void setFreqSubdayType(String freqSubdayType) {
        this.freqSubdayType = freqSubdayType;
    }

    public String getFreqSubdayType() {
        return freqSubdayType;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setScheduleNotificationId(String scheduleNotificationId) {
        this.scheduleNotificationId = scheduleNotificationId;
    }

    public String getScheduleNotificationId() {
        return scheduleNotificationId;
    }

    public void setNotificationChannel(String notificationChannel) {
        this.notificationChannel = notificationChannel;
    }

    public String getNotificationChannel() {
        return notificationChannel;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setReminderTime(String reminderTime) {
        this.reminderTime = reminderTime;
    }

    public String getReminderTime() {
        return reminderTime;
    }
}