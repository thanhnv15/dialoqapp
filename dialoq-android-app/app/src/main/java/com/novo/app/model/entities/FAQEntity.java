package com.novo.app.model.entities;

import com.novo.app.model.BaseModel;

public class FAQEntity extends BaseModel {
    public static final int LEVEL_DEFAULT = 0;

    private String text;
    private int image;
    private boolean isShow;
    private int level;
    private boolean isShowChild;
    private FAQEntity root;
    private boolean isLastChild;

    public FAQEntity(String text, boolean isShow, int level) {
        this.text = text;
        this.isShow = isShow;
        this.level = level;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isShowChild() {
        return isShowChild;
    }

    public void setShowChild(boolean showChild) {
        isShowChild = showChild;
    }

    public FAQEntity getRoot() {
        return root;
    }

    public void setRoot(FAQEntity root) {
        this.root = root;
    }

    public boolean isLast() {
        return isLastChild;
    }

    public boolean isLastChild() {
        return isLastChild;
    }

    public void setLastChild(boolean lastChild) {
        isLastChild = lastChild;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
