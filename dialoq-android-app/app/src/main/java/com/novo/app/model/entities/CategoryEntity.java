package com.novo.app.model.entities;

import java.util.List;

public class CategoryEntity {
    private String categoryKey;
    private List<TopicEntity> listTopic;

    public CategoryEntity(String categoryKey, List<TopicEntity> listTopic) {
        this.categoryKey = categoryKey;
        this.listTopic = listTopic;
    }

    public String getCategoryKey() {
        return categoryKey;
    }

    public List<TopicEntity> getlistTopic() {
        return listTopic;
    }
}
