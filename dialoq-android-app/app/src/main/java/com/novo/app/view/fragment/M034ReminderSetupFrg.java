package com.novo.app.view.fragment;

import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M034ReminderSetupPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM034ReminderSetupCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

public class M034ReminderSetupFrg extends BaseFragment<M034ReminderSetupPresenter, OnHomeBackToView> implements OnM034ReminderSetupCallBack {
    public static final String TAG = M034ReminderSetupFrg.class.getName();
    @Override
    protected void initViews() {
        TextView mTvDescription = findViewById(R.id.tv_m034_description, CAApplication.getInstance().getRegularFont());
        String highLight = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m034_smart_reminder));
        highLightText(mTvDescription, highLight, R.color.colorM015DarkBlue, true);

        findViewById(R.id.tv_m034_detail_1, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m034_detail_2, CAApplication.getInstance().getRegularFont());

        findViewById(R.id.tv_m034_not_now, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.bt_m034_set, this, CAApplication.getInstance().getMediumFont());
    }
    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m034_reminder_setup;
    }

    @Override
    protected M034ReminderSetupPresenter getPresenter() {
        return new M034ReminderSetupPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This function isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        //This function isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m034_set:
                CommonUtil.getInstance().showDialog(mContext, R.string.txt_m034_would_like,
                        R.string.txt_m034_notification_may, R.string.txt_m016_btl_ok, R.string.txt_m034_don_allow, new OnOKDialogCallBack() {
                    @Override
                    public void handleOKButton1() {
                        mCallBack.showFrgScreen(TAG, M041ReminderAddFrg.TAG);
                    }

                    @Override
                    public void handleOKButton2() {
                        mCallBack.showFrgScreen(TAG, M044PairCompleteFrg.TAG);
                    }
                });
                break;
            case R.id.tv_m034_not_now:
                mCallBack.showFrgScreen(TAG, M044PairCompleteFrg.TAG);
                break;
            default:
                break;
        }
    }
}
