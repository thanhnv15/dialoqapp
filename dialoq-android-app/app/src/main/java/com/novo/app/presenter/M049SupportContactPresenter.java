package com.novo.app.presenter;

import com.novo.app.view.event.OnM049SupportContactCallBack;

public class M049SupportContactPresenter extends BasePresenter<OnM049SupportContactCallBack> {
    public M049SupportContactPresenter(OnM049SupportContactCallBack event) {
        super(event);
    }
}
