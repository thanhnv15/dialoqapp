package com.novo.app.presenter;

import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.FAQEntity;
import com.novo.app.model.entities.TopicEntity;
import com.novo.app.view.event.OnM027FAQOnCallBack;

import java.util.ArrayList;
import java.util.List;

public class M027FAQPresenter extends BasePresenter<OnM027FAQOnCallBack> {

    public M027FAQPresenter(OnM027FAQOnCallBack event) {
        super(event);
    }

    public List<FAQEntity> getData(String text) {
        List<FAQEntity> entityList = new ArrayList<>();
        int currentTopic = getStorage().getM047ItemPosition();

        // Get list QnA
        for (int i = 0; i < getStorage().getM001ConfigSet().getListQnA().get(currentTopic).getListQnA().size(); i++) {
            TopicEntity entity = getStorage().getM001ConfigSet().getListQnA().get(currentTopic);
            String question = LangMgr.getInstance().getLangList().get(entity.getListQnA().get(i).getKey());
            String answer = LangMgr.getInstance().getLangList().get(entity.getListQnA().get(i).getValue());
            if (question == null || answer == null) continue;

            if (question.toUpperCase().contains(text) || answer.toUpperCase().contains(text)) {
                FAQEntity questionEntity = new FAQEntity(question, true, FAQEntity.LEVEL_DEFAULT);
                entityList.add(questionEntity);

                FAQEntity answerEntity = new FAQEntity(answer, false, FAQEntity.LEVEL_DEFAULT + 1);
                answerEntity.setRoot(questionEntity);
                answerEntity.setLastChild(true);
                entityList.add(answerEntity);
            }
        }
        return entityList;
    }
}
