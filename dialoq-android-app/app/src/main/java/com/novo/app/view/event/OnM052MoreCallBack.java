package com.novo.app.view.event;

public interface OnM052MoreCallBack extends OnCallBackToView {
    void toLandingScreen();

    void showLoginScreen();

    default void stopService() {

    }
}
