package com.novo.app.presenter;

import com.novo.app.view.event.OnM017ProfileBornCallBack;

public class M017ProfileBornPresenter extends BasePresenter<OnM017ProfileBornCallBack> {
    public M017ProfileBornPresenter(OnM017ProfileBornCallBack event) {
        super(event);
    }
}
