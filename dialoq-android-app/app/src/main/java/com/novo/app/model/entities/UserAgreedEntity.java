package com.novo.app.model.entities;

import com.novo.app.model.BaseModel;

import java.io.Serializable;

public class UserAgreedEntity extends BaseModel {
    private int totalCount;
    private TermEntity[] data;
    private PagingInfo paging;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public TermEntity[] getData() {
        return data;
    }

    public void setData(TermEntity[] data) {
        this.data = data;
    }

    public PagingInfo getPaging() {
        return paging;
    }

    public void setPaging(PagingInfo paging) {
        this.paging = paging;
    }

    public class PagingInfo implements Serializable {
        private String first;
        private String previous;
        private String next;
        private String last;

        public String getFirst() {
            return first;
        }

        public void setFirst(String first) {
            this.first = first;
        }

        public String getPrevious() {
            return previous;
        }

        public void setPrevious(String previous) {
            this.previous = previous;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public String getLast() {
            return last;
        }

        public void setLast(String last) {
            this.last = last;
        }
    }
}
