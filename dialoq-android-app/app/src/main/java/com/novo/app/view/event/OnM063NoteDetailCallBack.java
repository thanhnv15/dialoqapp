package com.novo.app.view.event;

public interface OnM063NoteDetailCallBack extends OnCallBackToView {
    void refreshDataM025();

    void showDeleteDialog();

    void showDeleteFailed();

    void showDeleteSuccess();

    void editNoteFailed();
}
