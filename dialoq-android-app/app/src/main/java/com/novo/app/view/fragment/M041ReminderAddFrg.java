package com.novo.app.view.fragment;

import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M041ReminderAddPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM041ReminderAddCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

import java.util.Locale;

public class M041ReminderAddFrg extends BaseFragment<M041ReminderAddPresenter, OnHomeBackToView> implements OnM041ReminderAddCallBack {
    public static final String TAG = M041ReminderAddFrg.class.getName();
    private TextView mTvAlarm;

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m041_reminder_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m041_reminder_description, CAApplication.getInstance().getRegularFont());

        mTvAlarm = findViewById(R.id.tv_m041_alarm, this, CAApplication.getInstance().getRegularFont());
        mTvAlarm.setText(currentReminder());


        findViewById(R.id.bt_m041_save, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m041_not_now, this, CAApplication.getInstance().getMediumFont());
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    private String currentReminder() {
        int hour = CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_HOUR, 12);
        int min = CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_MIN, 0);
        boolean isPM = (hour >= 12);
        return String.format(Locale.getDefault(), "%02d:%02d %s", (hour == 12 || hour == 0) ? 12 : hour % 12, min, isPM ? "PM" : "AM");
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m041_reminder_add;
    }

    @Override
    protected M041ReminderAddPresenter getPresenter() {
        return new M041ReminderAddPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This function isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        //This function isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m041_alarm:
                CommonUtil.getInstance().showTimeChoosing(mContext, mTvAlarm);
                break;
            case R.id.bt_m041_save:
                String time = String.format(Locale.getDefault(), "%02d:%02d"
                        , CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_HOUR, -1)
                        , CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_MIN, -1));
                if (getStorage().getM065ScheduleEntity() != null) {
                    mPresenter.activeReminder();
                    break;
                }
                mPresenter.addReminder(time);
                break;
            case R.id.tv_m041_not_now:
                mCallBack.showFrgScreen(TAG, M044PairCompleteFrg.TAG);
                break;

        }
    }

    @Override
    public void reminderAdded() {
        CommonUtil.getInstance().setAppReminder(mContext);
        getStorage().setM065ReminderChanged(true);
        mCallBack.showFrgScreen(TAG, M035ReminderDoneFrg.TAG);
    }
}
