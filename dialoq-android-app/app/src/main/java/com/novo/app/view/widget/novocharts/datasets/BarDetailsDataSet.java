package com.novo.app.view.widget.novocharts.datasets;

import android.graphics.Color;

import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.List;

public class BarDetailsDataSet extends BarDataSet implements IBarDetailsDataSet{

    private int mUnitColor = Color.BLUE;

    public BarDetailsDataSet(List<BarEntry> yVal, String label) {
        super(yVal, label);
    }

    @Override
    public int getUnitColor() {
        return mUnitColor;
    }

    public void setUnitColor(int unitColor) {
        this.mUnitColor = unitColor;
    }
}
