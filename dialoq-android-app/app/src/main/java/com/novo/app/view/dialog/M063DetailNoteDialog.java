/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.view.dialog;

import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M063NoteDetailPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseDialog;
import com.novo.app.view.event.OnM025SubDoseLogCallBack;
import com.novo.app.view.event.OnM063NoteDetailCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.widget.NVDateUtils;

public class M063DetailNoteDialog extends BaseDialog<M063NoteDetailPresenter, OnM025SubDoseLogCallBack> implements OnM063NoteDetailCallBack {
    public static final String TAG = M063DetailNoteDialog.class.getName();
    private TextView mTvNoteDate, mTvNoteTime, mTvNoteDescription;

    public M063DetailNoteDialog(Context context, boolean isCancel) {
        super(context, isCancel, R.style.dialog_style);
    }

    @Override
    protected M063NoteDetailPresenter getPresenter() {
        return new M063NoteDetailPresenter(this);
    }

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m063_back, this);
        findViewById(R.id.tv_m063_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m063_edit, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m063_note_time_date_breaker, CAApplication.getInstance().getRegularFont());
        mTvNoteDate = findViewById(R.id.tv_m063_v_detail_note_date, CAApplication.getInstance().getRegularFont());
        mTvNoteTime = findViewById(R.id.tv_m063_v_detail_note_time, CAApplication.getInstance().getRegularFont());
        mTvNoteDescription = findViewById(R.id.tv_m063_note_description, CAApplication.getInstance().getRegularFont());

        findViewById(R.id.bt_m063_delete, this, CAApplication.getInstance().getBoldFont());
    }

    @Override
    protected void warningForTimeZoneChange() {
        initData();
    }

    @Override
    protected void initData() {
        String reportDate = getStorage().getM063Note().getResult().getReportedDate();
        mTvNoteDate.setText(NVDateUtils.getNoteDetailsTimeString(reportDate));
        mTvNoteTime.setText(NVDateUtils.getOnlyTime(reportDate));
        mTvNoteDescription.setMovementMethod(new ScrollingMovementMethod());
        mTvNoteDescription.setText(getStorage().getM063Note().getResult().getNotes());
    }

    @Override
    public int getLayoutId() {
        return R.layout.view_m063_detail_note;
    }


    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m063_back:
                dismiss();
                break;
            case R.id.tv_m063_edit:
                M063EditNoteDialog mEditNoteDialog = new M063EditNoteDialog(mContext, false);
                mEditNoteDialog.setOnCallBack(this);
                mEditNoteDialog.show();
                break;
            case R.id.bt_m063_delete:
                showDeleteDialog();
                break;
            default:
                break;
        }
    }

    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }

    @Override
    public void showM001LandingScreen(String err) {
        mCallBack.showM001LandingScreen(err);
    }

    @Override
    public void refreshDataM025() {
        mCallBack.refreshData();
        dismiss();
    }


    public void showDeleteDialog() {
        CommonUtil.getInstance().showDialog(mContext,
                LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m063_v_support_title_delete_note_title)),
                LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m063_v_support_title_delete_note_description)),
                LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m063_v_support_title_delete_note_delete)),
                LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m063_v_support_title_delete_note_cancel)),
                new OnOKDialogCallBack() {
                    @Override
                    public void handleOKButton1() {
                        mPresenter.delNoteInDB(getStorage().getM063Note());
                    }
                });
    }

    @Override
    public void showDeleteFailed() {
        mCallBack.showAlertDialog(LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m063_delete_note_failed)));
        dismiss();
    }

    @Override
    public void showDeleteSuccess() {
        refreshDataM025();
    }

    @Override
    public void editNoteFailed() {
        dismiss();
    }

}
