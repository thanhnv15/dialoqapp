package com.novo.app.view.fragment;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M051MorePresenter;
import com.novo.app.service.SyncScheduleService;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.dialog.M051MorePrivacyPolicyDialog;
import com.novo.app.view.dialog.M051MoreTermDialog;
import com.novo.app.view.event.OnActionCallBack;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM051MoreCallBack;
import com.novo.app.view.widget.ProgressLoading;

public class M051MoreFrg extends BaseFragment<M051MorePresenter, OnM024DoseLogCallBack> implements OnM051MoreCallBack, OnActionCallBack {
    public static final String TAG = M051MoreFrg.class.getName();
    private M051MorePrivacyPolicyDialog m051MorePrivacyPolicyDialog;
    private M051MoreTermDialog m051MoreTermDialog;

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m051_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m051_profile_setting, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m051_other, CAApplication.getInstance().getBoldFont());

        findViewById(R.id.tv_m051_my_account, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m051_devices, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m051_notifications, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m051_reminder, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m051_language, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m051_term, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m051_privacy, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m051_log_out, this, CAApplication.getInstance().getRegularFont());

        TextView mTvVersion = findViewById(R.id.tv_m051_version, CAApplication.getInstance().getRegularFont());
        try {
            PackageInfo pInfo = CAApplication.getInstance().getPackageManager().getPackageInfo(CAApplication.getInstance().getPackageName(), 0);
            mTvVersion.setText(String.format("%s %s",
                    LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_key_m051_app_name)),
                    pInfo.versionName));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        findViewById(R.id.iv_m051_my_account, this);
        findViewById(R.id.iv_m051_devices, this);
        findViewById(R.id.iv_m051_notifications, this);
        findViewById(R.id.iv_m051_reminder, this);
        findViewById(R.id.iv_m051_language, this);
        findViewById(R.id.iv_m051_term, this);
        findViewById(R.id.iv_m051_privacy, this);

        findViewById(R.id.tr_m051_reminder).setVisibility(isTresibaExist());
        findViewById(R.id.v_m051_line).setVisibility(isTresibaExist());
        CADBManager.getInstance().setupUserProfile(getStorage().getM007UserEntity());
    }

    private int isTresibaExist() {
        int visibilityState = View.GONE;
        if (!getStorage().getM025ListDevices().isEmpty()) {
            for (int i = 0; i < getStorage().getM025ListDevices().size(); i++) {
                if (getStorage().getM025ListDevices().get(i).isTresiba()) {
                    visibilityState = View.VISIBLE;
                    return visibilityState;
                }
            }
        } else {
            return visibilityState;
        }
        return visibilityState;
    }

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m051_more;
    }

    @Override
    protected M051MorePresenter getPresenter() {
        return new M051MorePresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m051_my_account:
            case R.id.iv_m051_my_account:
                mCallBack.showChildFrgScreen(TAG, M052MoreMyAccountFrg.TAG);
                break;
            case R.id.tv_m051_term:
            case R.id.iv_m051_term:
                if (m051MoreTermDialog == null) {
                    m051MoreTermDialog = new M051MoreTermDialog(mContext, true);
                    m051MoreTermDialog.setOnCallBack(this);
                }
                m051MoreTermDialog.show();
                break;
            case R.id.tv_m051_privacy:
            case R.id.iv_m051_privacy:
                if (m051MorePrivacyPolicyDialog == null) {
                    m051MorePrivacyPolicyDialog = new M051MorePrivacyPolicyDialog(mContext, true);
                    m051MorePrivacyPolicyDialog.setOnCallBack(this);
                }
                m051MorePrivacyPolicyDialog.show();
                break;
            case R.id.tv_m051_language:
            case R.id.iv_m051_language:
                mCallBack.showChildFrgScreen(TAG, M060MoreLanguageFrg.TAG);
                break;
            case R.id.tv_m051_devices:
            case R.id.iv_m051_devices:
                mCallBack.showChildFrgScreen(TAG, M042MoreDevicesFrg.TAG);
                break;
            case R.id.tv_m051_reminder:
            case R.id.iv_m051_reminder:
                if (getStorage().getM065ScheduleEntity() != null) {
                    showReminder();
                    break;
                }
                showEmptyReminder();
                break;
            case R.id.tv_m051_notifications:
            case R.id.iv_m051_notifications:
                break;

            case R.id.tv_m051_log_out:
                if (!CommonUtil.getInstance().isConnectToNetwork()) {
                    mCallBack.showNetworkAlert();
                    return;
                }

                if (getStorage().getCallBackToSync() != null) {
                    getStorage().setM0DoseCallBack(TAG, this);

                    ProgressLoading.show(mContext);
                    getStorage().getCallBackToSync().onCallBack(null);
                }
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getStorage().removeM0DoseCallBack(TAG);
    }

    @Override
    public void showM023Login() {
        CommonUtil.getInstance().savePrefContent(CommonUtil.IS_LOGIN,CommonUtil.IS_LOGIN_FALSE);
        mContext.stopService(new Intent(mContext, SyncScheduleService.class));
        mCallBack.showM023LoginScreen();
        mCallBack.unregisterTimeZoneChange();
        getStorage().clearAllDataLogin();
    }

    @Override
    public void showReminder() {
        mCallBack.showChildFrgScreen(TAG, M065MoreReminderNotEmptyFrg.TAG);
    }

    @Override
    public void showEmptyReminder() {
        mCallBack.showChildFrgScreen(TAG, M064MoreReminderEmptyFrg.TAG);
    }

    @Override
    public void showNetworkAlert() {
        mCallBack.showNetworkAlert();
    }

    @Override
    public void deleteSuccess() {
        mContext.stopService(new Intent(mContext, SyncScheduleService.class));
        CommonUtil.getInstance().clearPrefContent(CommonUtil.KEEP);
        CommonUtil.getInstance().clearPrefContent(CommonUtil.USER_NAME);
        CommonUtil.getInstance().clearPrefContent(CommonUtil.PASS);
        mCallBack.showM001LandingScreen();
    }

    @Override
    public void onCallBack(Object data) {
        if (data == null || !((boolean) data)) {
            showNotify(R.string.txt_sync_err_not_log_out);
            ProgressLoading.dismiss();
        } else {
            mPresenter.callIA03LogOut();
        }
    }
}
