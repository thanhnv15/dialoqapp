package com.novo.app.view.fragment;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.presenter.M011SignUpPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.dialog.M051MorePrivacyPolicyDialog;
import com.novo.app.view.dialog.M051MoreTermDialog;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM011SignUpCallBack;
import com.novo.app.view.event.OnM051MoreCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

import java.util.ArrayList;
import java.util.List;

public class M011SignUpDataFrg extends BaseFragment<M011SignUpPresenter, OnHomeBackToView> implements OnM011SignUpCallBack, OnM051MoreCallBack {

    public static final String TAG = M011SignUpDataFrg.class.getName();
    private static final String TERM_1_CHECKED = "Checked";
    private M051MorePrivacyPolicyDialog morePrivacyDialog;
    private M051MoreTermDialog moreTermDialog;
    private Button mBtCreate;
    private CheckBox mCbFirst;
    private String termAndCondition, privacyNotice;
    private TextEntity termVersion;

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m011_logo);
        findViewById(R.id.iv_m011_back, this);
        findViewById(R.id.tv_m011_description, CAApplication.getInstance().getRegularFont());
        TextView mTvDetail = findViewById(R.id.tv_m011_detail, this, CAApplication.getInstance().getRegularFont());
        if (!getStorage().isMature()) {
            mTvDetail.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m011_detail_immature)));
            termVersion = new TextEntity(getStorage().getUserCountryEntity().getChildTerm().getValue(), getStorage().getUserCountryEntity().getChildVersion().getValue());
        } else {
            termVersion = new TextEntity(getStorage().getUserCountryEntity().getAdultTerm().getValue(), getStorage().getUserCountryEntity().getAdultVersion().getValue());
        }

        termAndCondition = getString(R.string.lang_m006_detail_link_1);
        termAndCondition = LangMgr.getInstance().getLangList().get(termAndCondition);
        privacyNotice = getString(R.string.lang_m006_detail_link_2);
        privacyNotice = LangMgr.getInstance().getLangList().get(privacyNotice);

        List<String> highlight = new ArrayList<>();
        highlight.add(termAndCondition);
        highlight.add(privacyNotice);
        setClickableText(mTvDetail, highlight, R.color.colorM020Teal);

        findViewById(R.id.tv_m011_cancel, this, CAApplication.getInstance().getMediumFont());

        mBtCreate = findViewById(R.id.bt_m011_next, this, CAApplication.getInstance().getMediumFont());
        mBtCreate.setEnabled(false);

        mCbFirst = findViewById(R.id.cb_m011_first_term, this);
        mCbFirst.setChecked(getStorage().isM001FirstCheckBox());
        validateTerm();

        TextView mTvFirstTerm = findViewById(R.id.tv_m011_first_term, this, CAApplication.getInstance().getRegularFont());
        String highLightTerm = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m011_first_term_t_c));
        highLightText(mTvFirstTerm, highLightTerm, R.color.colorM020Teal, true);
    }


    private void validateTerm() {
        if (mCbFirst.isChecked()) {
            mBtCreate.setEnabled(true);
            CommonUtil.getInstance().savePrefContent(TERM_1_CHECKED, TERM_1_CHECKED);
        } else {
            mBtCreate.setEnabled(false);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m011_signup_data;
    }

    @Override
    protected M011SignUpPresenter getPresenter() {
        return new M011SignUpPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        mCallBack.showFrgScreen(TAG, getTagSource());
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m011_detail:
                onLinkClicked();
                break;

            case R.id.cb_m011_first_term:
                getStorage().setM001FirstCheckBox(mCbFirst.isChecked());
                validateTerm();
                break;

            case R.id.tv_m011_first_term:
                if (moreTermDialog == null) {
                    moreTermDialog = new M051MoreTermDialog(mContext, true);
                    moreTermDialog.setOnCallBack(this);
                }
                moreTermDialog.show();
                break;

            case R.id.bt_m011_next:
                getStorage().getM001ProfileEntity().setTermVersion(termVersion);
                mCallBack.showFrgScreen(TAG, M005AccountSetUpNameFrg.TAG);
                break;

            case R.id.tv_m011_cancel:
                mCallBack.registrationCancel(TAG);
                break;

            case R.id.iv_m011_back:
                backToPreviousScreen();
                break;

            default:
                break;
        }
    }

    @Override
    public void showM023Login() {
        mCallBack.showFrgScreen(TAG, M023LoginFrg.TAG);
    }

    @Override
    public void showReminder() {
        //This feature isn't require here
    }

    @Override
    public void showEmptyReminder() {
        //This feature isn't require here
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    public void callAPISignUpSuccess() {
        mCallBack.showFrgScreen(TAG, M015SignUpSuccessFrg.TAG);
    }

    @Override
    public void onLinkClicked() {
        if (getStorage().getLinkTag() != null && getStorage().getLinkTag().equals(termAndCondition)) {
            if (moreTermDialog == null) {
                moreTermDialog = new M051MoreTermDialog(mContext, true);
            }
            moreTermDialog.show();
        }
        if (getStorage().getLinkTag() != null && getStorage().getLinkTag().equals(privacyNotice)) {
            if (morePrivacyDialog == null) {
                morePrivacyDialog = new M051MorePrivacyPolicyDialog(mContext, true);
            }
            morePrivacyDialog.show();
        }
        getStorage().setLinkTag(null);
    }
}
