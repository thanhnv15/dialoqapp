package com.novo.app.presenter;

import com.novo.app.view.event.OnM029PairAttachCallBack;

public class M029PairAttachPresenter extends BasePresenter<OnM029PairAttachCallBack> {
    public M029PairAttachPresenter(OnM029PairAttachCallBack event) {
        super(event);
    }
}
