package com.novo.app.model.constants;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@StringDef({DrugColorType.DEFAULT_COLOR_TYPE_TRESIBA, DrugColorType.DEFAULT_COLOR_TYPE_FIASP, DrugColorType.DEFAULT_COLOR_TYPE_BOTH, DrugColorType.DEFAULT_COLOR_NO_DOSE})
@Retention(RetentionPolicy.SOURCE)
public @interface DrugColorType {
    String DEFAULT_COLOR_TYPE_TRESIBA = "#c9dd03";
    String DEFAULT_COLOR_TYPE_FIASP = "#e63c2f";
    String DEFAULT_COLOR_TYPE_BOTH = "#ffffff";
    String DEFAULT_COLOR_NO_DOSE = "#111111";
}
