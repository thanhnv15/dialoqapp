package com.novo.app.view.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.ConfigEntity;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.presenter.M002SignUpChooseLanguagePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.CASpinnerAdapter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM002SignUpChooseLanguageCallBack;
import com.novo.app.view.event.OnSpinnerCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.ProgressLoading;

import java.util.List;

public class M002SignUpChooseLanguageFrg extends BaseFragment<M002SignUpChooseLanguagePresenter, OnHomeBackToView> implements OnM002SignUpChooseLanguageCallBack, OnSpinnerCallBack {

    public static final String TAG = M002SignUpChooseLanguageFrg.class.getName();
    private static final int KEY_SPINNER_LANGUAGE_TYPE = 1;
    private Button mBtNext;
    private Spinner mSpinnerLanguage;
    private TextView mTvLanguage;

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m002_please_choose, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.iv_m002_language, this);
        mTvLanguage = findViewById(R.id.tv_m002_language, this, CAApplication.getInstance().getRegularFont());
        mBtNext = findViewById(R.id.bt_m002_next, this, CAApplication.getInstance().getBoldFont());
        mBtNext.setVisibility(View.INVISIBLE);
        String hint = getString(R.string.txt_m002_choose_language);

        if (!mTvLanguage.getText().toString().equals(hint)) mBtNext.setVisibility(View.VISIBLE);
        findViewById(R.id.iv_m002_language, this);
        findViewById(R.id.tv_m002_cancel, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.iv_m002_back, this);

        ConfigEntity configEntity = getStorage().getM001ConfigSet();
        mSpinnerLanguage = findViewById(R.id.spin_m002_language);
        CASpinnerAdapter<OnSpinnerCallBack> adapter = new CASpinnerAdapter<>(KEY_SPINNER_LANGUAGE_TYPE,
                configEntity.getLanguageEntity(), mContext);
        adapter.setOnSpinnerCallBack(this);
        mSpinnerLanguage.setAdapter(adapter);
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m002_signup_choose_language;
    }

    @Override
    protected M002SignUpChooseLanguagePresenter getPresenter() {
        return new M002SignUpChooseLanguagePresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        if (getTagSource().equals(M002SignUpChooseLanguageFrg.TAG)) {
            CommonUtil.getInstance().defineBackTag(getTagSource(), TAG);
        }
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m002_cancel:
            case R.id.iv_m002_back:
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
                break;
            case R.id.tv_m002_language:
            case R.id.iv_m002_language:
                mSpinnerLanguage.performClick();
                break;
            case R.id.bt_m002_next:
                LangMgr.getInstance().configLanguage((TextEntity) mTvLanguage.getTag());
                break;
            default:
                break;
        }
    }

    @Override
    public void backToPreviousScreen() {
        mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
    }

    @Override
    public void clickOnItem(TextEntity mData) {
        LangMgr.getInstance().configLanguage(mData);
    }

    @Override
    public void updateView() {
        initViews();
        backToPreviousScreen();
    }

    @Override
    public void revertChoice() {

    }

    @Override
    public void onResume() {
        super.onResume();
        getStorage().setM002CallBack(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        CommonUtil.wtfe(TAG, "onDestroyView...setM002CallBack null");
        getStorage().setM002CallBack(null);
    }

    @Override
    public void onItemClick(int key, int pos, Object data) {
        if (key == KEY_SPINNER_LANGUAGE_TYPE) {
            TextEntity entity = ((List<TextEntity>) data).get(pos);
            mTvLanguage.setText(entity.getValue());
            mTvLanguage.setTag(entity);

            LangMgr.getInstance().configLanguage((TextEntity) mTvLanguage.getTag());
            mTvLanguage.setTypeface(CAApplication.getInstance().getBoldFont());
            mBtNext.setVisibility(View.VISIBLE);
            closeSpinner(mSpinnerLanguage);
            ProgressLoading.show(mContext);
        }
    }
}
