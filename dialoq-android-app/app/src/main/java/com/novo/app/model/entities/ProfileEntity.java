package com.novo.app.model.entities;

import java.io.Serializable;

public class ProfileEntity implements Serializable {
    private String firstName;
    private String lastName;
    private TextEntity country;
    private TextEntity typeOfDiabetes;
    private String email;
    private String password;
    private String userId;
    private String[] roleTypes;
    private String yearBorn;
    private String dateOfBirth;
    private TextEntity gender;
    private TextEntity yearDiabetes;
    private String verifyCode;
    private int allErgies;
    private int race;
    private TextEntity termVersion;
    private String streetAddress1;
    private String streetAddress2;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public TextEntity getCountry() {
        return country;
    }

    public void setCountry(TextEntity country) {
        this.country = country;
    }

    public TextEntity getTypeOfDiabetes() {
        return typeOfDiabetes;
    }

    public void setTypeOfDiabetes(TextEntity typeOfDiabetes) {
        this.typeOfDiabetes = typeOfDiabetes;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setRoleType(String[] roleTypes) {
        this.roleTypes = roleTypes;
    }

    public String[] getRoleTypes() {
        return roleTypes;
    }

    public void setRoleTypes(String[] roleTypes) {
        this.roleTypes = roleTypes;
    }

    public String getYearBorn() {
        return yearBorn;
    }

    public void setYearBorn(String value) {
        yearBorn = value;
    }

    public TextEntity getGender() {
        return gender;
    }

    public void setGender(TextEntity value) {
        gender = value;
    }

    public TextEntity getYearDiabetes() {
        return yearDiabetes;
    }

    public void setYearDiabetes(TextEntity value) {
        yearDiabetes = value;
    }

    public int getAllErgies() {
        return allErgies;
    }

    public void setAllErgies(int agreeValue) {
        allErgies = agreeValue;
    }

    public int getRace() {
        return race;
    }

    public void setRace(int value) {
        race = value;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public TextEntity getTermVersion() {
        return termVersion;
    }

    public void setTermVersion(TextEntity termVersion) {
        this.termVersion = termVersion;
    }

    public String getStreetAddress1() {
        return streetAddress1;
    }

    public void setStreetAddress1(String streetAddress1) {
        this.streetAddress1 = streetAddress1;
    }

    public String getStreetAddress2() {
        return streetAddress2;
    }

    public void setStreetAddress2(String streetAddress2) {
        this.streetAddress2 = streetAddress2;
    }
}
