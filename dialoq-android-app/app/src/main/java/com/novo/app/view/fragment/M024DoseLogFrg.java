/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.view.fragment;

import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M024DoseLogPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.dialog.M068GeneratingDialog;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM068ShareCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

public class M024DoseLogFrg extends BaseFragment<M024DoseLogPresenter, OnHomeBackToView> implements OnM024DoseLogCallBack, OnM068ShareCallBack {

    public static final String TAG = M024DoseLogFrg.class.getName();
    public static final String KEY_SAVE_LOGGED_IN = "KEY_SAVE_LOGGED_IN";

    private ImageView[] ivIconMenu = new ImageView[5];
    private View[] vIconLine = new View[5];
    private TextView[] tvMenu = new TextView[5];
    private LinearLayout mLnBottomBar;
    private M068GeneratingDialog m068GeneratingDialog;

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected void initViews() {
        mCallBack.detectAdultUser();

        mCallBack.startScan();
        CommonUtil.getInstance().savePrefContent(KEY_SAVE_LOGGED_IN, "true");
        CommonUtil.getInstance().savePrefContent(M023LoginFrg.KEY_FIRST_TIME, Boolean.TRUE + "");
        mLnBottomBar = findViewById(R.id.ln_m024_bottom_bar);
        findViewById(R.id.ln_m024_bottom_bar_dose_log, this);
        findViewById(R.id.ln_m024_bottom_bar_summary, this);
        findViewById(R.id.ln_m024_bottom_bar_share, this);
        findViewById(R.id.ln_m024_bottom_bar_support, this);
        findViewById(R.id.ln_m024_bottom_bar_more, this);

        ivIconMenu[0] = findViewById(R.id.iv_m024_bottom_bar_dose_log);
        ivIconMenu[1] = findViewById(R.id.iv_m024_bottom_bar_summary);
        ivIconMenu[2] = findViewById(R.id.iv_m024_bottom_bar_share);
        ivIconMenu[3] = findViewById(R.id.iv_m024_bottom_bar_support);
        ivIconMenu[4] = findViewById(R.id.iv_m024_bottom_bar_more);

        vIconLine[0] = findViewById(R.id.v_m024_bottom_bar_dose);
        vIconLine[1] = findViewById(R.id.v_m024_bottom_bar_summary);
        vIconLine[2] = findViewById(R.id.v_m024_bottom_bar_share);
        vIconLine[3] = findViewById(R.id.v_m024_bottom_bar_support);
        vIconLine[4] = findViewById(R.id.v_m024_bottom_bar_more);

        tvMenu[0] = findViewById(R.id.tv_m024_bottom_bar_dose, this);
        tvMenu[1] = findViewById(R.id.tv_m024_bottom_bar_summary, this);
        tvMenu[2] = findViewById(R.id.tv_m024_bottom_bar_share, this);
        tvMenu[3] = findViewById(R.id.tv_m024_bottom_bar_support, this);
        tvMenu[4] = findViewById(R.id.tv_m024_bottom_bar_more, this);


        redirectScreen();
        mCallBack.doTimeZoneChange();
        if (getStorage().getKeyTagNotify() == null) return;
        if (getStorage().getKeyTagNotify().equalsIgnoreCase(CommonUtil.KEY_NOTIFY_7_DAY) || getStorage().getKeyTagNotify().equalsIgnoreCase(CommonUtil.KEY_NOTIFY_14_DAY)) {
            changeTab(CommonUtil.TAB_MENU_SUMMARY);
            showChildFrgScreen(TAG, M036SubDoseLogSummaryFrg.TAG);
        } else if (getStorage().getKeyTagNotify().equalsIgnoreCase(CommonUtil.KEY_NOTIFY_14_DAY_LAST_DOSE)) {
            changeTab(CommonUtil.TAB_MENU_DOSE_LOG);
            showChildFrgScreen(TAG, M025SubDoseLogFrg.TAG);
        }
    }

    private void redirectScreen() {
        if (getStorage().getPreTAG() == null) {
            showChildFrgScreen(TAG, M025SubDoseLogFrg.TAG);
            return;
        }

        String preTAG = getStorage().getPreTAG();
        if (preTAG.equals(M070PairTroubleFrg.TAG)) {
            changeTab(CommonUtil.TAB_MENU_SUPPORT);
            showChildFrgScreen(TAG, M049SupportContactFrg.TAG);
        } else if (preTAG.equals(M042MoreDevicesFrg.TAG)) {
            changeTab(CommonUtil.TAB_MENU_MORE);
            showChildFrgScreen(TAG, M042MoreDevicesFrg.TAG);
        } else if (preTAG.equals(M065MoreReminderNotEmptyFrg.TAG)) {
            changeTab(CommonUtil.TAB_MENU_MORE);
            showChildFrgScreen(TAG, M065MoreReminderNotEmptyFrg.TAG);
        }
    }

    @Override
    protected int getContentLayout() {
        return R.id.ln_m024_content_layout;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m024_dose_log;
    }

    @Override
    protected M024DoseLogPresenter getPresenter() {
        return new M024DoseLogPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        // wait for customer confirmation
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.ln_m024_bottom_bar_dose_log:
                changeTab(CommonUtil.TAB_MENU_DOSE_LOG);
                showChildFrgScreen(TAG, M025SubDoseLogFrg.TAG);
                break;

            case R.id.ln_m024_bottom_bar_summary:
                changeTab(CommonUtil.TAB_MENU_SUMMARY);
                showChildFrgScreen(TAG, M036SubDoseLogSummaryFrg.TAG);
                break;

            case R.id.ln_m024_bottom_bar_share:
                //mCallBack.reloadConfigSet();
                changeTab(CommonUtil.TAB_MENU_SHARE);
                showChildFrgScreen(TAG, M068ShareChooseFrg.TAG);
                break;

            case R.id.ln_m024_bottom_bar_support:
                //mCallBack.reloadConfigSet();
                changeTab(CommonUtil.TAB_MENU_SUPPORT);
                showChildFrgScreen(TAG, M026SupportFrg.TAG);
                break;

            case R.id.ln_m024_bottom_bar_more:
                //mCallBack.reloadConfigSet();
                changeTab(CommonUtil.TAB_MENU_MORE);
                showChildFrgScreen(TAG, M051MoreFrg.TAG);
                break;
            default:
                break;
        }
    }

    @Override
    public void changeTab(int tab) {
        for (int i = 0; i < ivIconMenu.length; i++) {
            ivIconMenu[i].setColorFilter(getResources().getColor(R.color.colorM024InactiveIcon));
            tvMenu[i].setTextColor(CAApplication.getInstance().getResources().getColor(R.color.colorM024InactiveIcon));
            vIconLine[i].setBackgroundResource(R.color.colorWhite);
        }

        ivIconMenu[tab].setColorFilter(getResources().getColor(R.color.colorM001Cyan));
        tvMenu[tab].setTextColor(CAApplication.getInstance().getResources().getColor(R.color.colorM001Cyan));
        vIconLine[tab].setBackgroundResource(R.color.colorM001Cyan);
    }

    @Override
    public void showM023LoginScreen() {
        mCallBack.showFrgScreen(TAG, M023LoginFrg.TAG);
    }

    @Override
    public void showM001LandingScreen() {
        mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
    }

    @Override
    public void showM029PairScreen() {
        getStorage().setM029FirstPair(true);
        mCallBack.showFrgScreen(TAG, M029PairAttachFrg.TAG);
    }

    @Override
    public void showM032PairScreen() {
        mCallBack.showFrgScreen(TAG, M032PairPushReleaseFrg.TAG);
    }

    @Override
    public void showM070PairTroubleScreen() {
        mCallBack.showFrgScreen(TAG, M070PairTroubleFrg.TAG);
    }

    @Override
    public void showChildFrgScreen(String tagSource, String tagChild) {
        super.showChildFrgScreen(tagSource, tagChild);
    }

    @Override
    public void updateBottomBar() {
        for (TextView tvMenu1 : tvMenu) {
            String key = tvMenu1.getContentDescription().toString();
            tvMenu1.setText(LangMgr.getInstance().getLangList().get(key));
        }
    }

    @Override
    public void unregisterTimeZoneChange() {
        mCallBack.unregisterTimeZoneChange();
    }

    @Override
    public void setupReminder() {
        mCallBack.setupReminder();
    }

    @Override
    public void stopBGScan() {
        mCallBack.stopScan();
    }

    @Override
    public void startBGScan() {
        mCallBack.startScan();
    }

    @Override
    public void hideBottomBar(boolean hide) {
        if (hide) {
            mLnBottomBar.setVisibility(View.GONE);
        } else {
            mLnBottomBar.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void preLoginTimeZoneChange() {
        mCallBack.preLoginTimeZoneChange();
    }

    @Override
    public void showNetworkAlert() {
        mCallBack.showNetworkAlert();
    }

    @Override
    public void doTimeZoneChange() {
        mCallBack.doTimeZoneChange();
    }

    @Override
    public void showGeneratingProgress() {
        if (m068GeneratingDialog == null) {
            m068GeneratingDialog = new M068GeneratingDialog(mContext, false);
            m068GeneratingDialog.setOnCallBack(this);
        }
        m068GeneratingDialog.show();
    }

    @Override
    public void dismissGeneratingProgress() {
        new Handler().postDelayed(() -> {
            if (m068GeneratingDialog != null) {
                m068GeneratingDialog.dismiss();
            }
        }, 800);
    }
}
