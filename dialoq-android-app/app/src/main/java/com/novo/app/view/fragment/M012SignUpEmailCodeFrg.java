package com.novo.app.view.fragment;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M012SignUpPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM012SignUpCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

public class M012SignUpEmailCodeFrg extends BaseFragment<M012SignUpPresenter, OnHomeBackToView> implements OnM012SignUpCallBack {

    public static final String TAG = M012SignUpEmailCodeFrg.class.getName();

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m012_email);
        findViewById(R.id.tv_m012_description, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m012_detail, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m012_cancel, this, CAApplication.getInstance().getMediumFont());

        findViewById(R.id.bt_m012_send_code, this, CAApplication.getInstance().getMediumFont());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m012_signup_email_code;
    }

    @Override
    protected M012SignUpPresenter getPresenter() {

        return new M012SignUpPresenter(this);
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //Wait for customer confirmation
    }

    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m012_send_code:
                mPresenter.sendCodeUN42();
                break;

            case R.id.tv_m012_cancel:
                mCallBack.registrationCancel(TAG);
                break;
            default:
                break;
        }
    }

    @Override
    public void showM023Login() {
        mCallBack.showFrgScreen(TAG, M023LoginFrg.TAG);
    }

    @Override
    public void verifyEmailFail(String sms) {
        mCallBack.showFrgScreen(TAG, M009SignUpEmailFrg.TAG);
        CommonUtil.getInstance().backAndNotify(mContext, null, sms);
    }

    @Override
    public void verifyEmailSuccess(String success) {
        if (success.equals(CommonUtil.SUCCESS_VALUE)) {
            mCallBack.showFrgScreen(TAG, M013SignUpCodeSentFrg.TAG);
        } else {
            showNotify(LangMgr.getInstance().getLangList().get(getString(R.string.lang_err_unknown_reason)));
        }
    }

}
