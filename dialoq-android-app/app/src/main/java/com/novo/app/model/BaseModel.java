package com.novo.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.reflect.Field;

/**
 * Created by ThanhNv on 4/23/2017.
 */
public abstract class BaseModel implements Serializable {
    private static final String TAG = BaseModel.class.toString();
    @SerializedName("errors")
    private ErrorEntity[] errorEntity;


    public ErrorEntity[] getErrorEntity() {
        return errorEntity;
    }

    public void setErrorEntity(ErrorEntity[] errorEntity) {
        this.errorEntity = errorEntity;
    }

    public <T extends BaseModel> void copyObj(T entity) {
        try {
            String name = getTag();
            Class cls = Class.forName(name);
            Field[] f = cls.getDeclaredFields();
            for (Field field1 : f) {
                field1.setAccessible(true);
                Object obj = field1.get(entity);
                if (obj != null) {
                    field1.set(this, obj);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected String getTag() {
        return TAG;
    }

    public class ErrorEntity implements Serializable {
        @SerializedName("field")
        private String field;
        @SerializedName("code")
        private String code;
        @SerializedName("message")
        private String message;

        public String getField() {
            return field;
        }

        public void setField(String field) {
            this.field = field;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }
}
