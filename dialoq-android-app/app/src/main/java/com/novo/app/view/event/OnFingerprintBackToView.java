package com.novo.app.view.event;

public interface OnFingerprintBackToView extends OnCallBackToView {
    void showNotify(int sms);

    void showNotify(String sms);

    void updateFingerPrintResult();
}
