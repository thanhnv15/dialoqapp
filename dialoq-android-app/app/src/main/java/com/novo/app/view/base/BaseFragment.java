/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.view.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.heapanalytics.android.Heap;
import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.BasePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.utils.StorageCommon;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.widget.CustomTypefaceSpan;
import com.novo.app.view.widget.ProgressLoading;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ThanhNv on 4/23/2017.
 * Prototype for all fragments used in this app
 *
 * <p>Since we applied MVP pattern with fragment acted as View object, each instance of
 * {@link BaseFragment} must be provided a corresponding {@link BasePresenter}</p>
 *
 * <p>{@link OnCallBackToView} also must be provided for communication with other related view
 * typically another fragment, or host activity</p>
 *
 * <p>The true class type of {@link BaseFragment}, {@link BasePresenter} and {@link OnCallBackToView}
 * will be defined by child classes of this class.</p>
 */
public abstract class BaseFragment<T extends BasePresenter, G extends OnCallBackToView> extends
        Fragment implements Animation.AnimationListener, View.OnClickListener, OnCallBackToView {
    public static final String TAG = BaseFragment.class.getName();
    private static final int LAYOUT_NONE = -1;
    private static final CharSequence PLACE_HOLDER = "PLACEHOLDER";
    private static final CharSequence PLACAEHOLDER = "PLACAEHOLDER";
    protected final HashMap<String, BaseFragment> mFragChild = new HashMap<>();

    // Presenter module for this fragment (fragment acted as a View in MVP pattern)
    protected T mPresenter;

    // View of this fragment
    protected View mRootView;

    // Context of this fragment, it's where the fragment was attached
    protected Context mContext;

    // For communicating with other view object, typically host activity or other fragment
    protected G mCallBack;

    // This variable holds tag string, usually represent to previous fragment in the navigation stack
    // Navigating up from this fragment usually back to the fragment whole tag is saved in this var
    protected String mTagSource;

    // TODO: remove this unused variable
    protected String mTagCurrentChildSource;

    // Tag of this fragment's parent (typically host activity or a fragment)
    // TODO remove this unused variable
    protected String mParentTag;

    // Variables used for animating clicked view
    protected boolean isAnimEnd = true;
    protected Animation mAnim;
    protected int mId;
    protected View mClickedView;

    /**
     * Hide soft-keyboard if visibility
     * @param context calling context, this should be where the keyboard is showing
     */
    public static void hideKeyboard(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    /**
     * Init the views of this fragment
     * This function will be called manually inside {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * Classes which extend {@link BaseFragment} should implement this function differently since
     * they have different layout, and they have to prepare data for displaying contents
     */
    protected abstract void initViews();

    /**
     * Get layout id for the fragment
     * Returns id will be used to instantiate fragment's view in {@link #onCreateView}
     * @return layout id to be inflated as the view of this fragment
     */
    protected abstract int getLayoutId();

    /**
     * Get fragment's presenter object (fragment acted as View in MVP pattern)
     * @param <T> a child class of {@link BasePresenter}
     * @return an instance of {@link BasePresenter} corresponding to this fragment
     */
    protected abstract <T> T getPresenter();

    /**
     * Get fragment's tag which is used for logging purpose
     * Tag name will be defined by child class, typically it will be fragment's class name
     *
     * @return tag name in string
     */
    protected abstract String getTAG();

    /**
     * Search for a view inside this fragment's view
     * This function wrapping {@link View#findViewById(int)} with fragment's view as the context
     *
     * @param id of view used to search
     * @param <G> a View or it's child class type
     * @return view if found, or null
     */
    protected final <G extends View> G findViewById(int id) {
        return mRootView.findViewById(id);
    }

    /**
     * Find a view in this fragment, and also apply {@link View.OnClickListener}
     *
     * @param id view id to be searched
     * @param event {@link View.OnClickListener} to be applied to the found view
     * @param <G> view type (must be child class of {@link View}
     *
     * @return view if found, or null
     */
    protected final <G extends View> G findViewById(int id, View.OnClickListener event) {
        return findViewById(id, event, true, null);
    }

    /**
     * Show progress bar
     */
    @Override
    public void showLockDialog() {
        ProgressLoading.show(mContext);
    }

    /**
     * Overridng {@link android.support.v4.app.Fragment#onStart()}
     * adding initializing data if necessary
     */
    @Override
    public void onStart() {
        super.onStart();
        initData();
    }

    /**
     * Init necessary data while starting fragment
     * Child classes should override this specific data need to be initialized
     */
    protected void initData() {
    }

    /**
     * getTagCurrentChildSource
     * @return tag in string
     * TODO: This function is unused and should be removed
     */
    public String getTagCurrentChildSource() {
        return mTagCurrentChildSource;
    }

    /**
     * setTagCurrentChildSource
     *
     * @param mTagCurrentChildSource tag in string
     * TODO: This function is unused and should be removed
     */
    public void setTagCurrentChildSource(String mTagCurrentChildSource) {
        this.mTagCurrentChildSource = mTagCurrentChildSource;
    }

    /**
     * Find a view in this fragment. In case of search for an {@link TextView} instance,
     * apply a {@link Typeface}
     *
     * @param id view id to be searched
     * @param typeFace applied to {@link TextView}
     * @param <G> view type (must be child class of {@link View}
     *
     * @return view if found, or null
     */
    protected final <G extends View> G findViewById(int id, Typeface typeFace) {
        return findViewById(id, null, true, typeFace);
    }

    /**
     * Find a view in this fragment, and also apply {@link View.OnClickListener}
     * In case of search for an {@link TextView} instance, also apply a {@link Typeface}
     *
     * @param id view id to be searched
     * @param event {@link View.OnClickListener} to be applied to the found view
     * @param typeFace applied to {@link TextView}
     * @param <G> view type (must be child class of {@link View}
     *
     * @return view if found, or null
     */
    protected final <G extends View> G findViewById(int id, View.OnClickListener event, Typeface typeFace) {
        return findViewById(id, event, true, typeFace);
    }

    /**
     * Find a view in this fragment, and also apply {@link View.OnClickListener}, {@link View#isEnabled()}
     * In case of search for an {@link TextView} instance, also apply a {@link Typeface}
     *
     * @param id view id to be searched
     * @param event {@link View.OnClickListener} to be applied to the found view
     * @param enable boolean value to be applied to the found view as view's enability
     * @param typeFace applied to {@link TextView}
     * @param <G> view type (must be child class of {@link View}
     *
     * @return view if found, or null
     */
    protected final <G extends View> G findViewById(int id, View.OnClickListener event, boolean enable, Typeface typeFace) {
        G view = mRootView.findViewById(id);

        // TODO handle case of null value returned by findViewById()
        view.setOnClickListener(event);
        view.setEnabled(enable);
        if (typeFace != null && view instanceof TextView) {
            ((TextView) view).setTypeface(typeFace);
        }

        CharSequence keyDes = view.getContentDescription();

        if (keyDes == null) {
            CommonUtil.wtfe(getTAG(), "No. ContentDescription");
        } else if (view instanceof TextView) {
            String key = keyDes.toString();
            CommonUtil.wtfd(TAG, "KEY DES: " + keyDes.toString());
            if (LangMgr.getInstance().getLangList() == null) {
                CommonUtil.wtfe(TAG, "Err: No current language");
            } else {
                String text = LangMgr.getInstance().getLangList().get(key);
                if (text == null) return view;
                if (key.contains(PLACE_HOLDER) || key.contains(PLACAEHOLDER)) {
                    ((TextView) view).setHint(text);
                } else {
                    ((TextView) view).setText(text);
                 }
            }
        }
        return view;
    }

    /**
     * Call back when a view is clicked
     * And apply {@link #mAnim} to clicked view
     * @param v the view which has been clicked
     */
    @Override
    public final void onClick(View v) {
        if (!isAnimEnd) return;
        isAnimEnd = false;
        mId = v.getId();
        mClickedView = v;
        v.startAnimation(mAnim);
    }

    /**
     * Bind the communication between this fragment and other 'view' object
     * typically with host activity or other fragment
     *
     * @param event instance of OnCallBackToView
     */
    public final void setOnCallBack(G event) {
        mCallBack = event;
    }

    /**
     * setTagCurrentChildFrg
     * @param tag in string
     */
    private void setTagCurrentChildFrg(String tag) {
        mTagCurrentChildSource = tag;
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        CommonUtil.wtfd(TAG, "onCreateView..." + getTAG());
        setTagCurrentFrg();

        //setRetainInstance(true);
        if (mCallBack instanceof BaseFragment) {
            ((BaseFragment) mCallBack).setTagCurrentChildFrg(getTAG());
        }

        int layoutId = getLayoutId();
        mContext = getActivity();
        mPresenter = getPresenter();

        mRootView = inflater.inflate(layoutId, container, false);
        mAnim = AnimationUtils.loadAnimation(mContext, R.anim.alpha);
        mAnim.setAnimationListener(this);
        initViews();

        //Heap.init(CAApplication.getInstance(), getString(R.string.id_heap_ev));
        Heap.identify(getTag());

        //add event to handle notification
        return mRootView;
    }

    /**
     * Mark this fragment as the top-most being created fragment
     * @see BaseActivity#currentTag
     */
    protected void setTagCurrentFrg() {
        ((BaseActivity) getActivity()).currentTag = getTAG();
    }

    /**
     * Attach {@link #mContext} when fragment attached
     * @param context attached context
     */
    @Override
    public final void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    /**
     * Get the tag of previous fragment in navigation stack which is saved in {@link #mTagSource}
     *
     * @return tag in {@link String}
     */
    public final String getTagSource() {
        return mTagSource;
    }

    /**
     * Set the tag which is marked as previous fragment ({@link #mTagSource}) in navigation stack
     */
    public final void setTagSource(String mTagSource) {
        this.mTagSource = mTagSource;
        defineBackKey();
    }

    /**
     * Generic method for showing a number in Toast notification
     * @param text number to be shown -> variable name and type is ambiguous
     */
    public void showNotify(int text) {
        Toast.makeText(CAApplication.getInstance(), text, Toast.LENGTH_SHORT).show();
    }

    /**
     * Generic method for showing a number in Toast notification
     * @param text number to be shown -> refactor: name and type is ambiguous
     */
    public void showNotify(String text) {
        Toast.makeText(CAApplication.getInstance(), text, Toast.LENGTH_SHORT).show();
    }

    /**
     * Place holder for handling 'Back' key
     * Child classes must define how to handle 'Back' key, depending on their own context
     */
    protected abstract void defineBackKey();

    /**
     * Get tag of the object marked as this fragment's parent (see {@link #mParentTag})
     * TODO remove this unused method
     * @return tag in String
     */
    public final String getParentTag() {
        return mParentTag;
    }

    /**
     * Get tag of the object marked as this fragment's parent (see {@link #mParentTag})
     * TODO remove this unused method
     */
    public final void setParentTag(String mTagParent) {
        this.mParentTag = mTagParent;
    }

    /**
     * Call back when a view is starting animated
     * @see Animation.AnimationListener
     * @param animation animation object
     */
    @Override
    public final void onAnimationStart(Animation animation) {

    }

    /**
     * Call back when a view has ended animating
     * @see Animation.AnimationListener
     * @param animation animation object
     */
    @Override
    public final void onAnimationEnd(Animation animation) {
        onClickView(mId);
        isAnimEnd = true;
    }

    /**
     * Call back when a view is clicked
     * @param idView id of clicked view
     */
    protected void onClickView(int idView) {

    }

    /**
     * Call back when a view has repeated animating
     * @see Animation.AnimationListener
     * @param animation animation object
     */
    @Override
    public final void onAnimationRepeat(Animation animation) {

    }

    /**
     * Common method might be used by inherited classes for getting common storage object
     * @return storage common object
     */
    public final StorageCommon getStorage() {
        return CAApplication.getInstance().getStorageCommon();
    }

    /**
     * show another fragment from this fragment
     * @param tagSource tag of fragment which will be marked as previous fragment of being shown
     *                  fragment
     * @param tagChild tag of fragment which will be shown
     */
    @SuppressLint("ResourceType")
    protected void showChildFrgScreen(String tagSource, String
            tagChild) {
        if (getContentLayout() == LAYOUT_NONE) return;

        CommonUtil.wtfd(TAG, "showChildFrgScreen...");
        BaseFragment frg = mFragChild.get(tagChild);
        try {
            Class<?> clazz = Class.forName(tagChild);
            Constructor<?> constructor = clazz.getConstructor();
            frg = (BaseFragment) constructor.newInstance();
            frg.setOnCallBack(this);

            mFragChild.put(tagChild, frg);
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
        }
        assert frg != null;

        frg.setTagSource(tagSource);
        try {
            getChildFragmentManager().beginTransaction()
                    .setCustomAnimations(R.animator.alpha_in, R.animator.alpha_out)
                    .replace(getContentLayout(), frg).commit();
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
        }
    }

    /**
     * Get content layout of this fragment
     * @return layout Id
     */
    protected int getContentLayout() {
        return LAYOUT_NONE;
    }

    /**
     * Close spinner
     * @param spinner to be closed
     */
    protected void closeSpinner(Spinner spinner) {
        try {
            Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
            method.setAccessible(true);
            method.invoke(spinner);
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
        }
    }

    /**
     * Highlight a {@link TextView} with specific text style in a specific range
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param start start range of text will be highlighted (index of character in TextView's text)
     * @param end end range of text will be highlighted (index of character in TextView's text)
     * @param color highlighted color
     */
    protected void highLightText(TextView mTvTitle, int start, int end, int color) {
        highLightText(mTvTitle, start, end, color, false, 0, null);
    }

    /**
     * Highlight a {@link TextView} with specific text style in a specific range
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param start start range of text will be highlighted (index of character in TextView's text)
     * @param end end range of text will be highlighted (index of character in TextView's text)
     * @param color highlighted color
     * @param isBold highlighted style
     */
    protected void highLightText(TextView mTvTitle, int start, int end, int color, boolean isBold) {
        highLightText(mTvTitle, start, end, color, isBold, 0, null);
    }

    /**
     * Highlight a {@link TextView} with specific text style in a specific range
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param start start range of text will be highlighted (index of character in TextView's text)
     * @param end end range of text will be highlighted (index of character in TextView's text)
     * @param color highlighted color
     * @param size highlighted size
     */
    protected void highLightText(TextView mTvTitle, int start, int end, int color, int size) {
        highLightText(mTvTitle, start, end, color, false, size, null);
    }

    /**
     * Highlight a {@link TextView} with specific text style in a specific range
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param start start range of text will be highlighted (index of character in TextView's text)
     * @param end end range of text will be highlighted (index of character in TextView's text)
     * @param color highlighted color
     * @param isBold highlighted style
     * @param size highlighted size
     */
    protected void highLightText(TextView mTvTitle, int start, int end, int color, boolean isBold, int size) {
        highLightText(mTvTitle, start, end, color, isBold, size, null);
    }

    /**
     * Highlight a {@link TextView} with specific text style in a specific range
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param start start range of text will be highlighted (index of character in TextView's text)
     * @param end end range of text will be highlighted (index of character in TextView's text)
     * @param color highlighted color
     * @param size highlighted size
     * @param typeface highlighted typeface
     */
    protected void highLightText(TextView mTvTitle, int start, int end, int color, int size, Typeface typeface) {
        highLightText(mTvTitle, start, end, color, false, size, typeface);
    }

    /**
     * Highlight a {@link TextView} with specific text style in a specific range
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param start start range of text will be highlighted (index of character in TextView's text)
     * @param end end range of text will be highlighted (index of character in TextView's text)
     * @param color highlighted color
     * @param typeface highlighted typeface
     */
    protected void highLightText(TextView mTvTitle, int start, int end, int color, Typeface typeface) {
        highLightText(mTvTitle, start, end, color, false, 0, typeface);
    }

    /**
     * Highlight a {@link TextView} with specific text style in a specific range
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param start start range of text will be highlighted (index of character in TextView's text)
     * @param end end range of text will be highlighted (index of character in TextView's text)
     * @param color highlighted color
     * @param isBold highlighted style
     * @param size highlighted size
     * @param typeface highlighted typeface
     */
    protected void highLightText(TextView mTvTitle, int start, int end, int color, boolean isBold, int size, Typeface typeface) {
        try {
            Spannable textToSpan = new SpannableString(mTvTitle.getText().toString());
            if (isBold) {
                textToSpan.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else if (typeface != null) {
                textToSpan.setSpan(new CustomTypefaceSpan(typeface), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            if (size > 0) {
                textToSpan.setSpan(new RelativeSizeSpan((mTvTitle.getTextSize() + size) / mTvTitle.getTextSize()), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            textToSpan.setSpan(new ForegroundColorSpan(getResources().getColor(color)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mTvTitle.setText(textToSpan);
        } catch (Exception ignored) {
            //do nothing for temporary
        }
    }

    /**
     * Highlight a {@link TextView} with specific text styles in 02 specific ranges
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param start1 start of 1st range of text, which will be highlighted (index of character in TextView's text)
     * @param end1 end of 1st range of text, which will be highlighted (index of character in TextView's text)
     * @param start2 start of 2nd range of text, which will be highlighted (index of character in TextView's text)
     * @param end2 end of 2nd range of text, which will be highlighted (index of character in TextView's text)
     * @param color1 highlighted color on 1st range
     * @param color2 highlighted color on 2nd range
     * @param isBold highlighted style for both ranges
     * @param size highlighted size for both ranges
     * @param typeface highlighted typeface for both ranges
     */
    protected void highLightText(TextView mTvTitle, int start1, int end1, int start2, int end2,
                                 int color1, int color2, boolean isBold, int size, Typeface typeface) {
        try {
            Spannable textToSpan = new SpannableString(mTvTitle.getText().toString());
            if (isBold) {
                textToSpan.setSpan(new StyleSpan(Typeface.BOLD), start1, end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                textToSpan.setSpan(new StyleSpan(Typeface.BOLD), start2, end2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else if (typeface != null) {
                textToSpan.setSpan(new CustomTypefaceSpan(typeface), start1, end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                textToSpan.setSpan(new CustomTypefaceSpan(typeface), start2, end2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            if (size > 0) {
                textToSpan.setSpan(new RelativeSizeSpan((mTvTitle.getTextSize() + size) / mTvTitle.getTextSize()), start1, end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                textToSpan.setSpan(new RelativeSizeSpan((mTvTitle.getTextSize() + size) / mTvTitle.getTextSize()), start2, end2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            textToSpan.setSpan(new ForegroundColorSpan(getResources().getColor(color1)), start1, end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            textToSpan.setSpan(new ForegroundColorSpan(getResources().getColor(color2)), start2, end2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mTvTitle.setText(textToSpan);
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
        }
    }

    /**
     * Highlight a sub-text in a {@link TextView}
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param highLight text will be highlighted
     * @param color highlighted color
     * @param size highlighted size
     * @param typeface highlighted typeface
     */
    protected void highLightText(TextView mTvTitle, String highLight, int color, int size, Typeface typeface) {
        highLightText(mTvTitle, highLight, color, size, false, typeface);
    }

    /**
     * Highlight a sub-text in a {@link TextView}
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param highLight text will be highlighted
     * @param color highlighted color
     * @param typeface highlighted typeface
     */
    protected void highLightText(TextView mTvTitle, String highLight, int color, Typeface typeface) {
        highLightText(mTvTitle, highLight, color, 0, false, typeface);
    }

    /**
     * Highlight a sub-text in a {@link TextView}
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param highLight text will be highlighted
     * @param color highlighted color
     * @param isBold highlighted style
     */
    protected void highLightText(TextView mTvTitle, String highLight, int color, boolean isBold) {
        highLightText(mTvTitle, highLight, color, 0, isBold, null);
    }

    /**
     * Highlight a sub-text in a {@link TextView}
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param highLight text will be highlighted
     * @param color highlighted color
     * @param size highlighted size
     */
    protected void highLightText(TextView mTvTitle, String highLight, int color, int size) {
        highLightText(mTvTitle, highLight, color, size, false, null);
    }

    /**
     * Highlight a sub-text in a {@link TextView}
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param highLight text will be highlighted
     * @param color highlighted color
     * @param size highlighted size
     * @param isBold highlighted style
     */
    protected void highLightText(TextView mTvTitle, String highLight, int color, int size, boolean isBold) {
        highLightText(mTvTitle, highLight, color, size, isBold, null);
    }

    /**
     * Highlight a sub-text in a {@link TextView}
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param highLight text will be highlighted
     * @param color highlighted color
     */
    protected void highLightText(TextView mTvTitle, String highLight, int color) {
        highLightText(mTvTitle, highLight, color, 0, false, null);
    }

    /**
     * Highlight 02 sub-texts in a {@link TextView} with styles
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param highLight1 1st sub-text will be highlighted
     * @param highLight2 2nd sub-text will be highlighted
     * @param color1 highlighted color for 1st sub-text
     * @param color2 highlighted color for 2nd sub-text
     * @param isBold highlighted style
     */
    protected void highLightText(TextView mTvTitle, String highLight1, String highLight2, int color1, int color2, boolean isBold) {
        highLightText(mTvTitle, highLight1, highLight2, color1, color2, 0, isBold, null);
    }

    /**
     * Highlight a sub-text in a {@link TextView}
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param highLight text will be highlighted
     * @param color highlighted color
     * @param size highlighted size
     * @param isBold highlighted style
     * @param typeface highlighted typeface
     */
    private void highLightText(TextView mTvTitle, String highLight, int color, int size, boolean isBold, Typeface typeface) {
        if (mTvTitle == null || highLight == null) {
            CommonUtil.wtfe(TAG, "highLightText...Err: null TextView");
            return;
        }

        int start = mTvTitle.getText().toString().indexOf(highLight);
        int end = start + highLight.length();
        if (start < 0) return;

        highLightText(mTvTitle, start, end, color, isBold, size, typeface);
    }

    /**
     * Highlight 02 sub-texts in a {@link TextView} with styles
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param highLight1 1st sub-text will be highlighted
     * @param highLight2 2nd sub-text will be highlighted
     * @param color1 highlighted color for 1st sub-text
     * @param color2 highlighted color for 2nd sub-text
     * @param isBold highlighted style
     * @param typeface highlighted typeface
     */
    private void highLightText(TextView mTvTitle, String highLight1, String highLight2, int color1, int color2, int size, boolean isBold, Typeface typeface) {
        int start1 = mTvTitle.getText().toString().indexOf(highLight1);
        int start2 = mTvTitle.getText().toString().indexOf(highLight2);
        int end1 = start1 + highLight1.length();
        int end2 = start2 + highLight2.length();
        if (start1 < 0) return;

        highLightText(mTvTitle, start1, end1, start2, end2, color1, color2, isBold, size, typeface);
    }

    /**
     * Set sub-text portions of {@link TextView} clickable
     * Later, text portion clicked events will be delivered by {@link #mCallBack}
     *
     * @param mTvTitle {@link TextView} instance which has text portions clickable
     * @param start start indexes of clickable text portions
     * @param end end indexes of clickable text portions
     * @param color of clickable text portions
     * @param highlight tag of each text portion in {@link String}
     */
    protected void setClickableText(TextView mTvTitle, List<Integer> start, List<Integer> end, int color, List<String> highlight) {
        Spannable textToSpan = new SpannableString(mTvTitle.getText().toString());

        for (int i = 0; i < start.size(); i++) {
            final String tag = highlight.get(i);
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    getStorage().setLinkTag(tag);
                    mCallBack.onLinkClicked();
                }
            };
            textToSpan.setSpan(clickableSpan, start.get(i), end.get(i), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            textToSpan.setSpan(new ForegroundColorSpan(getResources().getColor(color)), start.get(i), end.get(i), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        mTvTitle.setMovementMethod(LinkMovementMethod.getInstance());
        mTvTitle.setText(textToSpan);
    }

    /**
     * Set sub-text portions of a {@link TextView} clickable
     * @param mTvTitle {@link TextView} instance which has text portions clickable
     * @param highlight list of sub-text portion should be clickable
     * @param color highlighed color will be applied for clickable text portions
     */
    protected void setClickableText(TextView mTvTitle, List<String> highlight, int color) {
        List<Integer> start = new ArrayList<>();
        List<Integer> end = new ArrayList<>();
        for (int i = 0; i < highlight.size(); i++) {
            start.add(mTvTitle.getText().toString().indexOf(highlight.get(i)));
            end.add(start.get(i) + highlight.get(i).length());
            if (start.get(i) < 0) return;
        }
        setClickableText(mTvTitle, start, end, color, highlight);
    }

    /**
     * Request to refresh contents
     * TODO remove this unused method
     */
    protected void updateChildContent() {
        initViews();
    }

    /**
     * Common method for showing an alert dialog
     * @param txtErr dialog string content will be shown
     */
    @Override
    public void showAlertDialog(String txtErr) {
        CommonUtil.getInstance().showDialog(mContext, txtErr, null);
    }

    /**
     * Common method for showing an alert dialog
     * @param txtErrId string id of content will be shown
     */
    @Override
    public void showAlertDialog(int txtErrId) {
        CommonUtil.getInstance().showDialog(mContext, txtErrId, null);
    }
}
