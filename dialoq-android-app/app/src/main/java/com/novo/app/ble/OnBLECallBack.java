package com.novo.app.ble;

import android.bluetooth.BluetoothDevice;

public interface OnBLECallBack {
    void onRequestPermission();

    void onStartPairing(BluetoothDevice myDevice, String deviceName);

    void onReceiveDoseLog(BLEInfoEntity mData);

    void onDisconnect();

    void onConnect();

    void onScanFailed();
}
