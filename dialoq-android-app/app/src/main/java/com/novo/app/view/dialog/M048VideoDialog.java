/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.view.dialog;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M048SupportVideoPresenter;
import com.novo.app.utils.CATask;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseDialog;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.event.OnM048SupportVideoCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.ProgressLoading;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ThanhNv on 14/12/2017.
 */
public class M048VideoDialog extends BaseDialog<M048SupportVideoPresenter, OnM048SupportVideoCallBack> implements OnM048SupportVideoCallBack, TextureView.SurfaceTextureListener, MediaPlayer.OnPreparedListener, View.OnClickListener, View.OnTouchListener, MediaPlayer.OnCompletionListener, SeekBar.OnSeekBarChangeListener, CATask.OnTaskCallBackToView, OnCallBackToView, MediaPlayer.OnVideoSizeChangedListener {
    public static final String TAG = M048VideoDialog.class.getName();

    private static final int PLAY_STATE = 0;
    private static final int STOP_STATE = 1;
    private static final long TIME_HIDE = 3000;
    private static final long TIME_DELAY = 500;
    private static final String KEY_CURRENT_TIME = "KEY_CURRENT_TIME";

    private static final int IDLE = 0;
    private static final int PAUSED = 1;
    private static final int PLAYED = 2;

    private MediaPlayer mPlayer;

    private ImageView ivPlay;
    private TextView tvDuration, tvStart;
    private SeekBar seekBar;
    private TextureView mTextureView;
    private View mTrPlayer;
    private Handler mHandler = new Handler();
    private CATask<M048VideoDialog, M048VideoDialog> mTimer;
    private int mState;

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mTrPlayer.setVisibility(View.GONE);
        }
    };
    private int mWidth;
    private int mHeight;
    private int wSurface;
    private int hSurface;
    private boolean mIsLandscape;
    private FrameLayout mFrMain;
    private ImageView mIvClose;

    public M048VideoDialog(Context context, boolean isCancel) {
        super(context, isCancel, R.style.AppTheme);
    }

    @Override
    public void showM001LandingScreen(String err) {
        mCallBack.showM001LandingScreen(err);
    }

    @Override
    protected void initViews() {
        mFrMain = findViewById(R.id.fr_m048_main);
        mTextureView = findViewById(R.id.texture_m048_video);
        mTextureView.setOnTouchListener(this);

        ivPlay = findViewById(R.id.iv_m048_play);
        tvStart = findViewById(R.id.tv_m048_start);
        mTrPlayer = findViewById(R.id.tr_m048_player);

        mIvClose = findViewById(R.id.iv_m048_close_video, this);

        ivPlay.setOnClickListener(this);
        tvDuration = findViewById(R.id.tv_m048_duration);
        seekBar = findViewById(R.id.seek_bar_m048);
        seekBar.setOnSeekBarChangeListener(this);
        mTextureView.setSurfaceTextureListener(this);
        mState = IDLE;

        //get orientation
        mIsLandscape = CAApplication.getInstance().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    @Override
    public int getLayoutId() {
        return R.layout.view_m048_video;
    }

    @Override
    protected M048SupportVideoPresenter getPresenter() {
        return new M048SupportVideoPresenter(this);
    }


    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        CommonUtil.wtfi(TAG, "onPrepared....");
        String time = CommonUtil.getInstance().getPrefContent(KEY_CURRENT_TIME);
        if (time != null) {
            try {
                int timeNow = Integer.parseInt(time);
                CommonUtil.getInstance().clearPrefContent(KEY_CURRENT_TIME);
                mPlayer.seekTo(timeNow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        playVideo();

        mTimer = new CATask<>(new WeakReference<>(this), this, TIME_DELAY);
        mTimer.setLoop(true);
        seekBar.setMax(mPlayer.getDuration());
        findViewById(R.id.tr_m048_player).setVisibility(View.VISIBLE);

        mTimer.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        new Handler().postDelayed(() -> findViewById(R.id.v_bg_white).setVisibility(View.GONE), 1000);

        mHandler.postDelayed(mRunnable, TIME_HIDE);
        tvDuration.setText(getDuration(mPlayer.getDuration()));
        ProgressLoading.dismiss();
    }

    private String getDuration(int time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss");
        return dateFormat.format(new Date(time));
    }

    @Override
    public void onStart() {
        mTextureView.setVisibility(View.VISIBLE);
        if (mState == PAUSED) {
            playVideo();
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (mState == PLAYED) {
            pauseVideo();
        }
        super.onStop();
    }

    public void stopVideo() {
        if (mPlayer != null) {
            mPlayer.reset();
            mPlayer.release();
            mPlayer = null;
            if (mTimer != null) {
                mTimer.cancel();
            }
            mState = IDLE;
        }
    }

    @Override
    public void dismiss() {
        stopVideo();
        super.dismiss();
    }


    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m048_play:
                if (mPlayer != null && mPlayer.isPlaying()) {
                    pauseVideo();
                } else if (mPlayer != null) {
                    playVideo();
                }

                break;
            case R.id.iv_m048_close_video:
                stopVideo();
                dismiss();
                break;
            default:
                break;
        }
    }


    @Override
    public void backToPreviousScreen() {
        // do nothing
    }

    public void pauseVideo() {
        if (mPlayer != null) {
            mPlayer.pause();
            mState = PAUSED;
            ivPlay.setImageLevel(PLAY_STATE);
        }
    }

    private void playVideo() {
        CommonUtil.wtfi(TAG, "playVideo....");
        mPlayer.start();
        mState = PLAYED;
        ivPlay.setImageLevel(STOP_STATE);
    }

    private void initVideo(Surface s) {
        CommonUtil.wtfi(TAG, "initVideo....");
        //get video info
        if (getStorage() == null) return;
        if (getStorage().getM048VideoPath() == null) return;
        String videoPath = getStorage().getM048VideoPath();
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.reset();
        }

        mPlayer = new MediaPlayer();
        mPlayer.setOnErrorListener((mp, what, extra) -> {
            ProgressLoading.dismiss();
            CommonUtil.getInstance().showDialog(mContext, "Could not play this video!", new OnOKDialogAdapter() {
                @Override
                public void handleOKButton1() {
                    dismiss();
                    M048VideoDialog.this.dismiss();
                }
            });
            return true;
        });
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnCompletionListener(this);
        mPlayer.setOnVideoSizeChangedListener(this);
        try {
            mPlayer.setDataSource(videoPath);
        } catch (IOException e) {
            e.printStackTrace();
            ProgressLoading.dismiss();
        }
        mPlayer.setSurface(s);
        mPlayer.prepareAsync();
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (mTrPlayer.getVisibility() == View.GONE) {
                    mTrPlayer.setVisibility(View.VISIBLE);
                    mHandler.removeCallbacks(mRunnable);
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mTrPlayer.getVisibility() == View.VISIBLE) {
                    mHandler.postDelayed(mRunnable, TIME_HIDE);
                }
                break;
        }
        return true;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        ivPlay.setImageLevel(PLAY_STATE);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (mPlayer != null) {
            mPlayer.seekTo(seekBar.getProgress());
            playVideo();
        }
    }

    @Override
    public Object doBGTask(String key) {
        try {
            Thread.sleep(TIME_DELAY);
            if (mPlayer == null) {
                mTimer.cancel();
                return false;
            }
            if (mPlayer.isPlaying()) {
                mTimer.updateUI(mPlayer.getCurrentPosition(), mPlayer.getDuration());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void updateUITask(Object[] values) {
        tvStart.setText(getDuration((int) values[0]));
        seekBar.setProgress((int) values[0]);
    }

    @Override
    public void doneTask(Object data) {

    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        ProgressLoading.show(mContext);
        initVideo(new Surface(surface));
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
        CommonUtil.wtfi(TAG, "WIDTH: " + width);
        CommonUtil.wtfi(TAG, "HEIGHT: " + height);
        mWidth = width;
        mHeight = height;

        configVideo(mIsLandscape);
    }

    private void configVideo(boolean isLandscape) {
        if (wSurface == 0) {
            wSurface = mTextureView.getWidth();
            hSurface = mTextureView.getHeight();
            if (isLandscape) {
                int tmp = wSurface;
                wSurface = hSurface;
                hSurface = tmp;
            }
        }

        int wSur = wSurface;
        int hSur = hSurface;

        if (isLandscape) {
            wSur = hSurface;
            hSur = wSurface;
        }


        float boxWidth = wSur;
        float boxHeight = hSur;

        float wr = boxWidth / (float) mWidth;
        float hr = boxHeight / (float) mHeight;
        float ar = (float) mWidth / (float) mHeight;

        if (wr > hr) {
            if (!mIsLandscape) {
                if (isLandscape) {
                    wSur = (int) (boxHeight * ar);
                } else {
                    wSur = (int) (boxHeight / ar);
                }
            } else {
                wSur = (int) (boxHeight * ar);
            }
        } else {
            if (!mIsLandscape) {
                if (isLandscape) {
                    hSur = (int) (boxHeight * ar);
                } else {
                    hSur = (int) (boxWidth / ar);
                }
            } else {
                hSur = (int) (boxWidth / ar);
            }
        }

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(wSur, hSur);
        params.gravity = Gravity.CENTER;
        if (isLandscape) {
            mFrMain.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorBlack));
            mIvClose.setColorFilter(ContextCompat.getColor(mContext, R.color.colorWhite));
        } else {
            mFrMain.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            mIvClose.setColorFilter(ContextCompat.getColor(mContext, R.color.colorM001Cyan));
        }
        mTextureView.setLayoutParams(params);
    }

    @Override
    public void toVideoPlayer(String url) {

    }

    public void onConfigurationChanged(Configuration newConfig) {
        configVideo(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE);
    }
}


