package com.novo.app.view.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.constants.DrugColorType;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.DateEntity;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM036SubDoseLogSummaryCallBack;
import com.novo.app.view.widget.ChartGestureAdapter;
import com.novo.app.view.widget.NVDateFormat;
import com.novo.app.view.widget.NVDateUtils;
import com.novo.app.view.widget.novocharts.NovoBarChart;
import com.novo.app.view.widget.novocharts.NovoScatterChart;
import com.novo.app.view.widget.novocharts.listeners.NovoItemSelectedListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.novo.app.view.fragment.M036SubDoseLogSummaryFrg.KEY_LAST_14_DAYS;
import static com.novo.app.view.fragment.M036SubDoseLogSummaryFrg.KEY_LAST_28_DAYS;
import static com.novo.app.view.fragment.M036SubDoseLogSummaryFrg.KEY_LAST_7_DAYS;

public class SummaryTabPageAdapter extends BasePageAdapter<OnM036SubDoseLogSummaryCallBack, Integer> implements NovoItemSelectedListener {
    private static final int LV_SEE_LESS = 1;
    private static final int LV_SEE_MORE = 0;
    private static final int KEY_HORIZONTAL_VIEW_BAR_CHART = 101;
    private static final int KEY_HORIZONTAL_VIEW_SECOND_BAR_CHART = 102;
    private final ArrayList<DataInfo> mData;
    private boolean isFirstLoad;
    private String mColorType;
    private String[] PAGE_TITLE;

    private HorizontalScrollView horizontalScrollViewBarChart;
    private HorizontalScrollView horizontalScrollViewBarSecondChart;
    private NovoBarChart mBarChart;
    private NovoBarChart mBarSecondChart;

    public SummaryTabPageAdapter(@DrugColorType String colorType, ArrayList<DataInfo> listDataInfo,
                                 List<Integer> listData, Context mContext, OnM036SubDoseLogSummaryCallBack callBack) {
        super(listData, mContext, callBack);
        mColorType = colorType;
        mData = listDataInfo;
        PAGE_TITLE = new String[]{
                LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m036_last_7_days)),
                LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m036_last_14_days)),
                LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m036_last_28_days))
        };
    }

    private ChartGestureAdapter getChartGestureAdapter(int key) {
        ChartGestureAdapter onChartGestureListener = new ChartGestureAdapter() {
            @Override
            public void onChartTranslate(MotionEvent me, float dX, float dY) {
                if (me.getAction() == MotionEvent.ACTION_MOVE) {
                    HorizontalScrollView horizontalScrollView = getHorizontalScrollView(getKey());
                    NovoBarChart barChart = getBarChartView(getKey());
                    assert horizontalScrollView != null;
                    assert barChart != null;

                    horizontalScrollView.setSmoothScrollingEnabled(true);
                    ViewPortHandler portHandler = barChart.getViewPortHandler();

                    float transX = -portHandler.getTransX();

                    horizontalScrollView.smoothScrollTo((int) (transX * 3), 0);
                }
            }
        };
        onChartGestureListener.setKey(key);
        return onChartGestureListener;
    }

    private HorizontalScrollView getHorizontalScrollView(int key) {
        if (key == KEY_HORIZONTAL_VIEW_BAR_CHART) return horizontalScrollViewBarChart;
        if (key == KEY_HORIZONTAL_VIEW_SECOND_BAR_CHART) return horizontalScrollViewBarSecondChart;
        return null;
    }

    private NovoBarChart getBarChartView(int key) {
        if (key == KEY_HORIZONTAL_VIEW_BAR_CHART) return mBarChart;
        if (key == KEY_HORIZONTAL_VIEW_SECOND_BAR_CHART) return mBarSecondChart;
        return null;
    }

    @Override
    protected void initViews(View rootView, Integer data, int pos) {

    }

    public void initDataForDays(int number, View rootView) {
        if (rootView == null) return;

        horizontalScrollViewBarChart = findViewById(rootView, R.id.scrollview_container_for_chart_28_days);
        horizontalScrollViewBarChart.smoothScrollTo(0, 0);
        mBarChart = findViewById(rootView, R.id.m014_bar_chart);

        long today = System.currentTimeMillis();
        Date endDate = new Date(today);
        String strStartDate = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, CommonUtil.getDateNow(CommonUtil.DATE_STYLE), -number + 1);

        assert strStartDate != null;
        strStartDate = strStartDate.replace(strStartDate.substring(11, 19), "00:00:00");
        Date startDate = NVDateUtils.stringToDateLocal(strStartDate, NVDateFormat.TIME_RANGE_FORMAT);

        findViewById(rootView, R.id.tv_m014_title_barchart, CAApplication.getInstance().getRegularFont());
        findViewById(rootView, R.id.tv_m014_title_scatter, CAApplication.getInstance().getRegularFont());

        NovoBarChart chart = findViewById(rootView, R.id.m014_bar_chart);

        ImageView iv = findViewById(rootView, R.id.iv_see_less_more);
        iv.setImageLevel(LV_SEE_MORE);

        TextView tvSeeMoreLess = findViewById(rootView, R.id.tv_see_less_more);
        tvSeeMoreLess.setText(LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m014_see_more)));
        findViewById(rootView, R.id.scrollview_container_for_chart_28_days).setVisibility(View.GONE);
        findViewById(rootView, R.id.m014_bar_chart).setVisibility(View.GONE);
        findViewById(rootView, R.id.tv_m014_daily_dose_note).setVisibility(View.GONE);

        switch (mColorType) {
            case DrugColorType.DEFAULT_COLOR_TYPE_TRESIBA:
                chart.setNovoItemSelectedListener(this);
                chart.setOnChartGestureListener(getChartGestureAdapter(KEY_HORIZONTAL_VIEW_BAR_CHART));
                // disable chart scrollable
                List<DateEntity> dateEntityList = chart.setData(startDate, endDate, mData, DoseLogAdapter.TYPE_ASSESSMENT_INJECTION,
                        DrugColorType.DEFAULT_COLOR_TYPE_TRESIBA);
                initTresibaDrug(true, rootView, startDate, dateEntityList, number);
                break;
            case DrugColorType.DEFAULT_COLOR_TYPE_FIASP:
                chart = findViewById(rootView, R.id.m014_bar_chart);
                chart.setNovoItemSelectedListener(this);
                chart.setDrawValueAboveBar(true);
                chart.setOnChartGestureListener(getChartGestureAdapter(KEY_HORIZONTAL_VIEW_BAR_CHART));
                dateEntityList = chart.setData(startDate, endDate, mData, DoseLogAdapter.TYPE_ASSESSMENT_INJECTION,
                        DrugColorType.DEFAULT_COLOR_TYPE_FIASP);
                initFiapsDrug(true, rootView, startDate, endDate, dateEntityList, number);
                break;
            case DrugColorType.DEFAULT_COLOR_NO_DOSE:
                initDataForNoDose(rootView, endDate, startDate, number);
                break;
            default:
                chart = findViewById(rootView, R.id.m014_bar_chart);
                chart.setNovoItemSelectedListener(this);
                chart.setDrawValueAboveBar(true);
                chart.setOnChartGestureListener(getChartGestureAdapter(KEY_HORIZONTAL_VIEW_BAR_CHART));

                dateEntityList = chart.setData(startDate, endDate, mData, DoseLogAdapter.TYPE_ASSESSMENT_INJECTION,
                        DrugColorType.DEFAULT_COLOR_TYPE_TRESIBA);
                initTresibaDrug(false, rootView, startDate, dateEntityList, number);

                View secondView = View.inflate(mContext, R.layout.item_m014_one_drug_chart, null);
                horizontalScrollViewBarSecondChart = findViewById(secondView, R.id.scrollview_container_for_chart_28_days);
                horizontalScrollViewBarSecondChart.smoothScrollTo(0, 0);
                mBarSecondChart = findViewById(secondView, R.id.m014_bar_chart);

                NovoBarChart chart2 = findViewById(secondView, R.id.m014_bar_chart);
                chart2.setNovoItemSelectedListener(this);
                chart2.setDrawValueAboveBar(true);
                chart2.setOnChartGestureListener(getChartGestureAdapter(KEY_HORIZONTAL_VIEW_SECOND_BAR_CHART));
                List<DateEntity> dateEntityListForDualChart = chart2.setData(startDate, endDate, mData, DoseLogAdapter.TYPE_ASSESSMENT_INJECTION,
                        DrugColorType.DEFAULT_COLOR_TYPE_FIASP);
                initFiapsDrug(false, secondView, startDate, endDate, dateEntityListForDualChart, number);

                LinearLayout secondChartView = rootView.findViewById(R.id.ln_second_chart);
                secondChartView.removeAllViews();
                secondChartView.addView(secondView);
                break;
        }
    }

    private void initDataForNoDose(View rootView, Date endDate, Date startDate, int numberDays) {
        findViewById(rootView, R.id.tv_m014_title_drug).setVisibility(View.GONE);
        findViewById(rootView, R.id.ln_m014_daily_timing).setVisibility(View.VISIBLE);
        findViewById(rootView, R.id.tr_m036_see_less_more).setVisibility(View.VISIBLE);
        findViewById(rootView, R.id.tv_m014_end_timing).setVisibility(View.INVISIBLE);

        TextView mTvPrimingDose = findViewById(rootView, R.id.tv_m014_daily_dose_note);
        mTvPrimingDose.setVisibility(View.GONE);

        TextView tvTimeTiming = findViewById(rootView, R.id.tv_m014_time_timing);
        TextView tvSubTiming = findViewById(rootView, R.id.tv_m014_sub_timing);

        findViewById(rootView, R.id.ln_m014_scatter).setVisibility(View.GONE);
        findViewById(rootView, R.id.ln_second_chart).setVisibility(View.GONE);
        findViewById(rootView, R.id.view_divider_1).setVisibility(View.GONE);
        findViewById(rootView, R.id.view_m014_scatter).setVisibility(View.GONE);

        FrameLayout mFrameNoData = findViewById(rootView, R.id.fr_m014_chart);
        TextView mTvNoData = findViewById(rootView, R.id.tv_m014_no_data);
        mTvNoData.setText(LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m014_no_data)));
        mTvNoData.setVisibility(View.GONE);

        TextView mTvSeeMoreLess = findViewById(rootView, R.id.tv_see_less_more);
        mTvSeeMoreLess.setText(LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m014_see_more)));

        NovoBarChart chart = findViewById(rootView, R.id.m014_bar_chart);
        chart.setOnChartGestureListener(getChartGestureAdapter(KEY_HORIZONTAL_VIEW_BAR_CHART));

        findViewById(rootView, R.id.tr_m036_see_less_more, v -> {
            ImageView iv = findViewById(rootView, R.id.iv_see_less_more);
            if (iv.getDrawable().getLevel() == LV_SEE_MORE) {
                iv.setImageLevel(LV_SEE_LESS);
                mTvSeeMoreLess.setText(LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m014_see_less)));
                if (numberDays == 28) {
                    findViewById(rootView, R.id.scrollview_container_for_chart_28_days).setVisibility(View.VISIBLE);
                }

                mTvNoData.setVisibility(View.VISIBLE);
                chart.setVisibility(View.VISIBLE);
                mFrameNoData.setVisibility(View.VISIBLE);

            } else {
                iv.setImageLevel(LV_SEE_MORE);
                mTvSeeMoreLess.setText(LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m014_see_more)));
                if (numberDays == 28) {
                    findViewById(rootView, R.id.scrollview_container_for_chart_28_days).setVisibility(View.GONE);
                }

                mTvNoData.setVisibility(View.GONE);
                chart.setVisibility(View.GONE);
                mFrameNoData.setVisibility(View.GONE);
            }
        });

        tvTimeTiming.setText(CAApplication.getInstance().getString(R.string.txt_m014_item_no_data_available));
        tvTimeTiming.setTextSize(18);
        tvTimeTiming.setTextColor(CAApplication.getInstance().getResources().getColor(R.color.colorM014NoData));

        String textVersion = CAApplication.getInstance().getString(R.string.txt_m014_sub_timing).replace("(%s)", "(90%)");
        tvSubTiming.setText(textVersion);

        List<DateEntity> entities = chart.setData(startDate, endDate, mData, DoseLogAdapter.TYPE_ASSESSMENT_INJECTION,
                DrugColorType.DEFAULT_COLOR_NO_DOSE);

        RecyclerView rvCalendar = findViewById(rootView, R.id.rv_m014_calendar);
        GridLayoutManager layoutManager = new GridLayoutManager(mContext, 7);
        rvCalendar.setLayoutManager(layoutManager);
        CalendarAdapter adapter = new CalendarAdapter(CommonUtil.COLOR_TYPE_NO_DOSE, mContext, entities, mCallBack);
        rvCalendar.setAdapter(adapter);
    }

    private void initFiapsDrug(boolean isOneChart, View rootView, Date startDate, Date endDate,
                               List<DateEntity> entities, int numberDays) {
        TextView tvDrugName = findViewById(rootView, R.id.tv_m014_title_drug, CAApplication.getInstance().getBoldFont());
        tvDrugName.setText(mContext.getString(R.string.txt_fiasp));
        tvDrugName.setTextColor(CAApplication.getInstance().getResources().getColor(R.color.colorBlack));
        tvDrugName.setVisibility(isOneChart ? View.GONE : View.VISIBLE);

        findViewById(rootView, R.id.tv_m014_bar_chart_note);
        findViewById(rootView, R.id.tv_m014_title_timing);
        findViewById(rootView, R.id.tv_m014_sub_timing);
        findViewById(rootView, R.id.tv_m014_end_timing);
        TextView mTvPrimingDoseChart = findViewById(rootView, R.id.tv_m014_daily_dose_note);
        mTvPrimingDoseChart.setVisibility(View.GONE);

        findViewById(rootView, R.id.tv_m014_daily_pattern);
        findViewById(rootView, R.id.tv_m014_title_barchart, CAApplication.getInstance().getRegularFont());
        findViewById(rootView, R.id.tv_m014_title_scatter, CAApplication.getInstance().getRegularFont());
        TextView tvSeeMoreLess = findViewById(rootView, R.id.tv_see_less_more);
        tvSeeMoreLess.setText(LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m014_see_more)));
        TextView mTvNoData = findViewById(rootView, R.id.tv_m014_no_data);
        mTvNoData.setVisibility(View.GONE);
        FrameLayout frChart = findViewById(rootView, R.id.fr_m014_chart);
        frChart.setVisibility(View.GONE);

        findViewById(rootView, R.id.tr_m036_see_less_more, v -> {
            ImageView iv = findViewById(rootView, R.id.iv_see_less_more);
            if (iv.getDrawable().getLevel() == LV_SEE_MORE) {
                iv.setImageLevel(LV_SEE_LESS);
                tvSeeMoreLess.setText(LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m014_see_less)));
                if (numberDays == 28) {
                    findViewById(rootView, R.id.scrollview_container_for_chart_28_days).setVisibility(View.VISIBLE);
                }
                frChart.setVisibility(View.VISIBLE);

                if (!isNoDoseData(entities, false)) {
                    mTvNoData.setVisibility(View.GONE);
                    mTvPrimingDoseChart.setVisibility(View.VISIBLE);
                } else {
                    mTvNoData.setVisibility(View.VISIBLE);
                    mTvPrimingDoseChart.setVisibility(View.GONE);
                }

                findViewById(rootView, R.id.m014_bar_chart).setVisibility(View.VISIBLE);

            } else {
                iv.setImageLevel(LV_SEE_MORE);
                tvSeeMoreLess.setText(LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m014_see_more)));
                if (numberDays == 28) {
                    findViewById(rootView, R.id.scrollview_container_for_chart_28_days).setVisibility(View.GONE);
                }
                frChart.setVisibility(View.GONE);
                mTvPrimingDoseChart.setVisibility(View.GONE);
                mTvNoData.setVisibility(View.GONE);
                findViewById(rootView, R.id.m014_bar_chart).setVisibility(View.GONE);
            }
        });

        TextView mTvNoDataScatter = findViewById(rootView, R.id.tv_m014_no_data_scatter);
        mTvNoDataScatter.setText(LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m014_no_data)));
        TextView mTvPrimingDoseScatter = findViewById(rootView, R.id.tv_m014_bar_chart_note);

        findViewById(rootView, R.id.fr_m014_scatter_chart).setVisibility(View.VISIBLE);
        findViewById(rootView, R.id.view_m014_scatter).setVisibility(View.VISIBLE);
        findViewById(rootView, R.id.ln_m014_scatter).setVisibility(View.VISIBLE);
        findViewById(rootView, R.id.ln_m014_daily_timing).setVisibility(View.GONE);

        NovoScatterChart scatterChart = findViewById(rootView, R.id.m014_scatter);
        scatterChart.setData(startDate, endDate, mData);
        if (!isNoDoseData(entities, false)) {
            mTvNoDataScatter.setVisibility(View.GONE);
            mTvPrimingDoseScatter.setVisibility(View.VISIBLE);
        } else {
            mTvNoDataScatter.setVisibility(View.VISIBLE);
            mTvPrimingDoseScatter.setVisibility(View.GONE);
        }

        RecyclerView rvCalendar = findViewById(rootView, R.id.rv_m014_calendar);
        GridLayoutManager layoutManager = new GridLayoutManager(mContext, 7);

        rvCalendar.setLayoutManager(layoutManager);
        CalendarAdapter adapter = new CalendarAdapter(CommonUtil.COLOR_TYPE_FIASP, mContext, entities, mCallBack);
        rvCalendar.setAdapter(adapter);
    }

    private void initTresibaDrug(boolean isOneChart, View rootView, Date startDate, List<DateEntity> entities, int numberDays) {
        findViewById(rootView, R.id.ln_m014_scatter).setVisibility(View.GONE);
        findViewById(rootView, R.id.view_m014_scatter).setVisibility(View.GONE);

        TextView mTvPrimingDoseTiming = findViewById(rootView, R.id.tv_m014_end_timing, CAApplication.getInstance().getRegularFont());

        TextView mTvNoData = findViewById(rootView, R.id.tv_m014_no_data);
        mTvNoData.setText(LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m014_no_data)));
        mTvNoData.setVisibility(View.GONE);

        View view = findViewById(rootView, R.id.ln_m014_daily_timing);
        view.setVisibility(View.VISIBLE);

        TextView mTvTiming = findViewById(rootView, R.id.tv_m014_time_timing, CAApplication.getInstance().getRegularFont());
        String doseTimeRange = CommonUtil.getInstance().calcTimingDose(getDataInRange(mData, startDate));
        if (doseTimeRange.equals("")) {
            doseTimeRange = LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m014_no_data));
            mTvTiming.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
            mTvTiming.setTextColor(CAApplication.getInstance().getResources().getColor(R.color.colorM014NoData));
            mTvPrimingDoseTiming.setVisibility(View.INVISIBLE);
        }
        mTvTiming.setText(doseTimeRange);

        TextView mTvPrimingDoseChart = findViewById(rootView, R.id.tv_m014_daily_dose_note);
        FrameLayout frChart = findViewById(rootView, R.id.fr_m014_chart);
        frChart.setVisibility(View.GONE);

        findViewById(rootView, R.id.tv_m014_bar_chart_note, CAApplication.getInstance().getRegularFont());
        findViewById(rootView, R.id.tv_m014_title_timing, CAApplication.getInstance().getRegularFont());
        findViewById(rootView, R.id.tv_m014_sub_timing, CAApplication.getInstance().getRegularFont());

        findViewById(rootView, R.id.tv_m014_daily_pattern);
        findViewById(rootView, R.id.tv_m014_title_barchart, CAApplication.getInstance().getRegularFont());
        findViewById(rootView, R.id.tv_m014_title_scatter, CAApplication.getInstance().getRegularFont());

        TextView tvSeeMoreLess = findViewById(rootView, R.id.tv_see_less_more);
        tvSeeMoreLess.setText(LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m014_see_more)));

        findViewById(rootView, R.id.tr_m036_see_less_more, v -> {
            ImageView iv = findViewById(rootView, R.id.iv_see_less_more);

            if (iv.getDrawable().getLevel() == LV_SEE_MORE) {
                iv.setImageLevel(LV_SEE_LESS);
                tvSeeMoreLess.setText(LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m014_see_less)));
                if (numberDays == 28) {
                    findViewById(rootView, R.id.scrollview_container_for_chart_28_days).setVisibility(View.VISIBLE);
                }


                if (!isNoDoseData(entities, true)) {
                    mTvNoData.setVisibility(View.GONE);
                    mTvPrimingDoseChart.setVisibility(View.VISIBLE);
                } else {
                    mTvNoData.setVisibility(View.VISIBLE);
                    mTvPrimingDoseChart.setVisibility(View.GONE);
                }
                frChart.setVisibility(View.VISIBLE);
                findViewById(rootView, R.id.m014_bar_chart).setVisibility(View.VISIBLE);
            } else {
                iv.setImageLevel(LV_SEE_MORE);
                tvSeeMoreLess.setText(LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m014_see_more)));
                if (numberDays == 28) {
                    findViewById(rootView, R.id.scrollview_container_for_chart_28_days).setVisibility(View.GONE);
                }

                findViewById(rootView, R.id.m014_bar_chart).setVisibility(View.GONE);
                mTvNoData.setVisibility(View.GONE);
                frChart.setVisibility(View.GONE);
                mTvPrimingDoseChart.setVisibility(View.GONE);
            }
        });

        TextView tvDrugName = findViewById(rootView, R.id.tv_m014_title_drug, CAApplication.getInstance().getBoldFont());
        tvDrugName.setText(mContext.getString(R.string.txt_tresiba));

        tvDrugName.setTextColor(CAApplication.getInstance().getResources().getColor(R.color.colorBlack));
        tvDrugName.setVisibility(!isOneChart ? View.VISIBLE : View.GONE);

        RecyclerView rvCalendar = findViewById(rootView, R.id.rv_m014_calendar);
        GridLayoutManager layoutManager = new GridLayoutManager(mContext, 7);

        rvCalendar.setLayoutManager(layoutManager);
        CalendarAdapter adapter = new CalendarAdapter(CommonUtil.COLOR_TYPE_TRESIBA, mContext, entities, mCallBack);
        rvCalendar.setAdapter(adapter);
    }

    private List<DataInfo> getDataInRange(ArrayList<DataInfo> mData, Date startDate) {
        List<DataInfo> listDataInRange = new ArrayList<>();
        for (int i = 0; i < mData.size(); i++) {
            DataInfo item = mData.get(i);
            Date reportedDate = NVDateUtils.stringToDateLocal(item.getResult().getReportedDate(), NVDateFormat.TIME_RANGE_FORMAT);
            if (reportedDate.compareTo(startDate) >= 0) {
                listDataInRange.add(item);
            }
        }
        return listDataInRange;
    }

    private boolean isNoDoseData(List<DateEntity> entities, boolean isTresiba) {
        if (entities.isEmpty()) {
            return true;
        }
        for (int i = 0; i < entities.size(); i++) {
            DateEntity item = entities.get(i);
            if (!item.getDailyDetail().getDoseList().isEmpty()) {
                for (int j = 0; j < item.getDailyDetail().getDoseList().size(); j++) {
                    String drugCode = item.getDailyDetail().getDoseList().get(j).getResult().getCustom3();
                    if (isTresiba) {
                        if (drugCode.equals(DeviceEntity.CODE_TRESIBA_100 + "")
                                || drugCode.equals(DeviceEntity.CODE_TRESIBA_200 + ""))
                            return false;
                    } else {
                        if (drugCode.equals(DeviceEntity.CODE_FIASP + ""))
                            return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    protected int getLayoutId(int pos) {
        switch (listData.get(pos)) {
            case KEY_LAST_7_DAYS:
            case KEY_LAST_14_DAYS:
            case KEY_LAST_28_DAYS:
                return R.layout.item_m014_one_drug_chart;
            default:
                break;
        }
        return R.layout.item_m014_one_drug_chart;
    }

    @Override
    public void onClick(View v) {
        // This feature isn't require here
    }

    @Nullable
    @Override
    public String getPageTitle(int position) {
        if (PAGE_TITLE != null && PAGE_TITLE.length > position) {
            return PAGE_TITLE[position];
        } else {
            return "";
        }

    }

    @Override
    public void onNovoItemSelected(int colorType, BarData mData, Entry entry, Highlight highlight) {
        if (mCallBack != null && mData != null) {
            mCallBack.onNovoItemSelected(ColorTemplate.rgb(mColorType), mData, entry, highlight);
        }
    }

    @Override
    protected void onAddedView(int position, View view) {
        if (isFirstLoad) return;
        isFirstLoad = true;
        mCallBack.onTabSelected(null);
    }
}
