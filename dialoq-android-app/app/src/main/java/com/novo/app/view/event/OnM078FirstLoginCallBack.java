package com.novo.app.view.event;

/**
 * Created by HungPLD1 on 10/17/2019
 * Callbacks for updating view in when user input email to get confirmation email
 */
public interface OnM078FirstLoginCallBack extends OnCallBackToView {
    /**
     * Request View instance to display an alert message
     *
     * @param message is the message should be display
     */
    void verifyCodeFail(String message);

    /**
     * Request View instance to proceed to the next screen
     */
    void verifyCodeSuccess();
}
