package com.novo.app.view.event;

public interface OnSpinnerCallBack {
    void onItemClick(int key, int pos, Object data);
}
