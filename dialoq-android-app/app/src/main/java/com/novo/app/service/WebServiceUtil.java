/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.service;

import android.support.annotation.NonNull;

import com.novo.app.utils.CommonUtil;
import com.novo.app.utils.ssl.TrustManagerManipulator;
import com.novo.app.view.event.OnWebServiceUtilCallBack;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public final class WebServiceUtil {
    private static final int TIME_OUT = 60;
    private static final String TAG = WebServiceUtil.class.getName();
    private static final MediaType TYPE_JSON = MediaType.parse("application/json; charset=utf-8");

    private OnExecuteListener mListener;

    public void setOnExecuteListener(OnExecuteListener event) {
        mListener = event;
    }

    public void callRequest(OnWebServiceUtilCallBack callBack, CARequest request) {
        CommonUtil.wtfi(TAG, "onHandleIntent..." + request.getPathSegment());
        mListener.showLockDialog(new Object[]{request.getKey(), null});
        doRequest(callBack, request);
    }

    private void doRequest(OnWebServiceUtilCallBack callBack, CARequest caRequest) {
        CommonUtil.wtfi(TAG, "doGet...");
        try {
            TrustManagerManipulator[] trustManagers = new TrustManagerManipulator[]{new TrustManagerManipulator()};
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustManagers, new java.security.SecureRandom());
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient client = new OkHttpClient.Builder().
                    sslSocketFactory(sslSocketFactory, trustManagers[0]).
                    connectTimeout(TIME_OUT, TimeUnit.SECONDS).
                    readTimeout(TIME_OUT, TimeUnit.SECONDS).
                    hostnameVerifier((hostname, session) -> true).
                    build();

            String url = getURL(caRequest);
            Request.Builder requestBuilder = new Request.Builder();

            //build request
            requestBuilder.url(url);
            if (caRequest.getMethod().equals(CARequest.METHOD_POST)
                    || caRequest.getMethod().equals(CARequest.METHOD_PUT)
                    || caRequest.getMethod().equals(CARequest.METHOD_DELETE)) {
                requestBuilder.method(caRequest.getMethod(), new FormBody.Builder().build());
            }

            prepareRequest(requestBuilder, caRequest);

            //call api
            client.newCall(requestBuilder.build()).enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    CommonUtil.wtfi(TAG, "onFailure...");
                    if (mListener != null) {
                        mListener.hideLockDialog(new Object[]{caRequest.getKey(), caRequest.getTag()});
                        mListener.execFailed(new Object[]{caRequest.getKey(), new FailedException(e.getMessage()), caRequest.getTag()});
                    }
                    if (callBack != null) {
                        callBack.breakService();
                    }
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) {
                    CommonUtil.wtfi(TAG, "onResponse... - " + caRequest.getTag());
                    if (mListener != null) {
                        mListener.hideLockDialog(new Object[]{caRequest.getKey(), caRequest.getTag()});
                        mListener.execSuccess(new Object[]{caRequest.getKey(), response, caRequest.getTag(), caRequest.getRequestID(), caRequest.getToken()});
                    }
                    if (callBack != null) {
                        callBack.breakService();
                    }
                }
            });

        } catch (Exception e) {
            mListener.hideLockDialog(new Object[]{caRequest.getKey(), caRequest.getTag()});

            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
            if (mListener != null) {
                mListener.execFailed(new Object[]{caRequest.getKey(), new FailedException(e
                        .getMessage()), caRequest.getTag()});
            }
            if (callBack != null) {
                callBack.breakService();
            }
        }
    }

    private void prepareRequest(Request.Builder requestBuilder, CARequest caRequest) {
        //add headers
        for (int i = 0; i < caRequest.getHeader().size(); i++) {
            String[] keyValue = caRequest.getHeader().get(i);
            requestBuilder.addHeader(keyValue[0], keyValue[1]);
        }

        //add params
        if (!caRequest.getParams().isEmpty()) {
            FormBody.Builder bodyBuilder = new FormBody.Builder();
            for (int i = 0; i < caRequest.getParams().size(); i++) {
                String[] keyValue = caRequest.getParams().get(i);
                bodyBuilder.add(keyValue[0], keyValue[1]);
            }
            switch (caRequest.getMethod()) {
                case CARequest.METHOD_POST:
                    requestBuilder.post(bodyBuilder.build());
                    break;
                case CARequest.METHOD_PUT:
                    requestBuilder.put(bodyBuilder.build());
                    break;
                case CARequest.METHOD_DELETE:
                    requestBuilder.delete(bodyBuilder.build());
                    break;
            }
        }
        prepareBody(requestBuilder, caRequest);
    }

    private void prepareBody(Request.Builder requestBuilder, CARequest caRequest) {
        //add body
        if (caRequest.getBody() != null) {
            RequestBody body = RequestBody.create(TYPE_JSON, caRequest.getBody());
            switch (caRequest.getMethod()) {
                case CARequest.METHOD_POST:
                    requestBuilder.post(body);
                    break;
                case CARequest.METHOD_PUT:
                    requestBuilder.put(body);
                    break;
                case CARequest.METHOD_DELETE:
                    requestBuilder.delete(body);
                    break;
            }

        }

        //add images
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        for (int i = 0; i < caRequest.getFiles().size(); i++) {
            File file = new File(caRequest.getFiles().get(i)[1]);
            builder.addFormDataPart(caRequest.getFiles().get(i)[0], file.getName(),
                    RequestBody.create(MediaType.parse("text/csv"), file)).build();
        }

        if (!caRequest.getFiles().isEmpty()) {
            switch (caRequest.getMethod()) {
                case CARequest.METHOD_POST:
                    requestBuilder.post(builder.build());
                    break;
                case CARequest.METHOD_PUT:
                    requestBuilder.put(builder.build());
                    break;
                case CARequest.METHOD_DELETE:
                    requestBuilder.delete(builder.build());
                    break;
            }
        }
    }

    private String getURL(CARequest caRequest) {
        StringBuilder url = new StringBuilder((caRequest.isUsedBaseURL() ? CommonUtil.BASE_URL : "") + caRequest.getPathSegment());
        //add params
        boolean isExist = false;
        for (int i = 0; i < caRequest.getQuery().size() - 1; i++) {
            String[] keyValue = caRequest.getQuery().get(i);
            url.append(keyValue[0]).append("=").append(keyValue[1]).append("&");
            isExist = true;
        }
        if (isExist || caRequest.getQuery().size() == 1) {
            String[] keyValue = caRequest.getQuery().get(caRequest.getQuery().size() - 1);
            url.append(keyValue[0]).append("=").append(keyValue[1]);
        }
        return url.toString();
    }

    public interface OnExecuteListener {
        void execSuccess(Object data);

        void execFailed(Object data);

        void showLockDialog(Object data);

        void hideLockDialog(Object data);
    }

    public class FailedException extends Exception {
        FailedException(String message) {
            super(message);
        }
    }
}
