package com.novo.app.presenter;

import com.novo.app.view.event.OnM033PairSuccessCallBack;

public class M033PairSuccessPresenter extends BasePresenter<OnM033PairSuccessCallBack> {
    public M033PairSuccessPresenter(OnM033PairSuccessCallBack event) {
        super(event);
    }
}
