package com.novo.app.view.event;

public interface OnM060MoreLanguageCallBack extends OnCallBackToView {
    void dataLanguageReady(String data);

    default void updateBottomBar() {
    }

}
