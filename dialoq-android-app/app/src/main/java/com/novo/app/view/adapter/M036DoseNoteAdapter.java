package com.novo.app.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.model.entities.NoteEntity;
import com.novo.app.view.event.OnM036DoseDetailCallBack;

import java.util.List;

public class M036DoseNoteAdapter extends BaseRecycleAdapter<OnM036DoseDetailCallBack, NoteEntity, M036DoseNoteAdapter.NoteHolder> {
    public M036DoseNoteAdapter(Context mContext, List<NoteEntity> mListData, OnM036DoseDetailCallBack mCallBack) {
        super(mContext, mListData, mCallBack);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_m036_note;
    }

    @Override
    protected NoteHolder getViewHolder(int viewType, View itemView) {
        return new NoteHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        NoteHolder item = (NoteHolder) holder;

        NoteEntity data = mListData.get(position);

        item.tvTime.setText(data.getTime());
        item.tvNote.setText(data.getNote());

    }

    public class NoteHolder extends BaseHolder {
        TextView tvTime;
        TextView tvNote;

        NoteHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void initView() {
            tvTime = findViewById(R.id.tv_m036_note_time, CAApplication.getInstance().getRegularFont());
            tvNote = findViewById(R.id.tv_m036_note, CAApplication.getInstance().getRegularFont());
        }
    }

}