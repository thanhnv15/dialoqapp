package com.novo.app.view.fragment;

import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M065MorePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.dialog.M065MoreReminderAddDialog;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM065MoreCallBack;
import com.novo.app.view.event.OnM065ReminderCallBack;
import com.novo.app.view.widget.ProgressLoading;

import java.util.Locale;

public class M065MoreReminderNotEmptyFrg extends BaseFragment<M065MorePresenter, OnM024DoseLogCallBack> implements OnM065MoreCallBack, OnM065ReminderCallBack {
    public static final String TAG = M065MoreReminderNotEmptyFrg.class.getName();
    private View vAlert;
    private Switch mSwReminderState;
    private TextView mTvRemindTime;
    private TextView mTvAlertTime;
    private M065MoreReminderAddDialog mDialog;

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m065_back, this);
        findViewById(R.id.tv_m065_title, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m065_edit, this, CAApplication.getInstance().getMediumFont());

        vAlert = findViewById(R.id.v_m065_alert, this);
        if (getStorage().isM065ReminderChanged()) vAlert.setVisibility(View.VISIBLE);
        else vAlert.setVisibility(View.GONE);
        findViewById(R.id.iv_m065_alert_close, this);

        findViewById(R.id.tv_m065_drug, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m065_about, CAApplication.getInstance().getBoldFont());

        mTvRemindTime = findViewById(R.id.tv_m065_remind_time, CAApplication.getInstance().getMediumFont());
        mTvAlertTime = findViewById(R.id.tv_m065_alert_time, CAApplication.getInstance().getRegularFont());

        findViewById(R.id.tv_m065_detail_1, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m065_detail_2, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m065_detail_3, CAApplication.getInstance().getRegularFont());

        mSwReminderState = findViewById(R.id.sw_m065_switch, this);
        mSwReminderState.setChecked(false);

        initReminder();
    }

    private void initReminder() {
        mTvRemindTime.setText(currentReminder());
        mTvAlertTime.setText(currentReminder());

        if (getStorage().getM065ScheduleEntity() != null) {
            if (getStorage().getM065ScheduleEntity().getNotificationChannel().equals(CommonUtil.REMINDER_ACTIVE)) {
                CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_SET, "true");
                mSwReminderState.setChecked(true);
            } else {
                CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_SET, "false");
                mSwReminderState.setChecked(false);
            }
        }
    }

    private String currentReminder() {
        int hour = CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_HOUR, -1);
        int min = CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_MIN, -1);
        boolean isPM = (hour >= 12);
        return String.format(Locale.getDefault(), "%02d:%02d %s", (hour == 12 || hour == 0) ? 12 : hour % 12, min, isPM ? "PM" : "AM");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getStorage().setM065ReminderChanged(false);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m065_more_reminder_not_empty;
    }

    @Override
    protected M065MorePresenter getPresenter() {
        return new M065MorePresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This function isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        //This function isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.sw_m065_switch:
                String time = String.format(Locale.getDefault(), "%02d:%02d",
                        CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_HOUR, -1),
                        CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_MIN, -1));

                mPresenter.activeReminder(time, mSwReminderState.isChecked() ? CommonUtil.REMINDER_ACTIVE : CommonUtil.REMINDER_INACTIVE);
                break;
            case R.id.iv_m065_back:
                mCallBack.showChildFrgScreen(TAG, M051MoreFrg.TAG);
                break;
            case R.id.tv_m065_edit:
                mDialog = new M065MoreReminderAddDialog(mContext, false);
                mDialog.setOnCallBack(this);
                mDialog.show();
                break;
            case R.id.iv_m065_alert_close:
                vAlert.setVisibility(View.GONE);
                getStorage().setM065ReminderChanged(false);
                break;
            default:
                break;
        }
    }


    @Override
    public void showAlert(String sms) {
        initReminder();
        vAlert.setVisibility(View.VISIBLE);
        mSwReminderState.setChecked(true);
        ProgressLoading.dismiss();
        mDialog.dismiss();
    }

    @Override
    public void closeDialog(String key) {
        initReminder();
    }

    @Override
    public void toM051More() {
        mCallBack.showChildFrgScreen(TAG, M051MoreFrg.TAG);
        ProgressLoading.dismiss();
        mDialog.dismiss();
    }

    @Override
    public void changeState() {
        if (CommonUtil.getInstance().getPrefContent(CommonUtil.REMINDER_SET) != null
                && CommonUtil.getInstance().getPrefContent(CommonUtil.REMINDER_SET).equals("true")) {
            CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_SET, "false");
            CommonUtil.getInstance().cancelAppReminder(true, mContext, mSwReminderState);
        } else {
            CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_SET, "true");
            CommonUtil.getInstance().setAppReminder(mContext);
            mSwReminderState.setChecked(true);
        }
    }


}
