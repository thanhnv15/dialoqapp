package com.novo.app.view.event;

public interface OnM059MoreProductCallBack extends OnCallBackToView {
    void updateUserChoice();

    void showLoginScreen();

    void showDialog();

    void stateReverse();
}
