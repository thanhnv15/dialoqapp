package com.novo.app.view.fragment;

import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.model.constants.DrugColorType;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.DateEntity;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.presenter.M036SubDoseLogSummaryPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.activity.HomeActivity;
import com.novo.app.view.adapter.SummaryTabPageAdapter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.dialog.M036DoseDetailDialog;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM036DoseDetailDialog;
import com.novo.app.view.event.OnM036SubDoseLogSummaryCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public class M036SubDoseLogSummaryFrg extends BaseFragment<M036SubDoseLogSummaryPresenter, OnHomeBackToView>
        implements OnM036SubDoseLogSummaryCallBack, TabLayout.OnTabSelectedListener, OnM036DoseDetailDialog {
    public static final String TAG = M036SubDoseLogSummaryFrg.class.getName();
    public static final int KEY_LAST_7_DAYS = 0;
    public static final int KEY_LAST_14_DAYS = 1;
    public static final int KEY_LAST_28_DAYS = 2;

    private static final int LEVEL_SELECTED = 1;
    private static final int LEVEL_DESELECT = 0;
    private static final int TAB_DRUG1 = 0;
    private static final int TAB_DRUG2 = 1;
    private static final Integer[] LIST_KEYS_LAST = new Integer[]{KEY_LAST_7_DAYS, KEY_LAST_14_DAYS, KEY_LAST_28_DAYS};

    public int colorType;
    private ImageView[] ivDrugs = new ImageView[2];
    private TableRow[] trDrugs = new TableRow[2];
    private SummaryTabPageAdapter mAdapter;
    private TabLayout tb;
    private View.OnClickListener clickListener = v -> {
        switch (v.getId()) {
            case R.id.iv_m036_summary_drug1:
            case R.id.tv_m036_summary_drug1:
                M036SubDoseLogSummaryFrg.this.onClick(trDrugs[0]);
                break;
            case R.id.iv_m036_summary_drug2:
            case R.id.tv_m036_summary_drug2:
                M036SubDoseLogSummaryFrg.this.onClick(trDrugs[1]);
                break;
            default:
                break;
        }
    };
    private ViewPager mVp;
    private int currentPage;
    private M036DoseDetailDialog m036DoseDetailDialog;

    private TextView mTvTab1;
    private TextView mTvTab2;
    private TableRow mTrTab;

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m036_title, CAApplication.getInstance().getBoldFont());
        mVp = findViewById(R.id.m036_vp_summary);
        mTrTab = findViewById(R.id.tr_m036_tab);

        mTvTab1 = findViewById(R.id.tv_m036_summary_drug1, clickListener, CAApplication.getInstance().getBoldFont());
        mTvTab2 = findViewById(R.id.tv_m036_summary_drug2, clickListener, CAApplication.getInstance().getBoldFont());

        ivDrugs[0] = findViewById(R.id.iv_m036_summary_drug1, clickListener);
        ivDrugs[1] = findViewById(R.id.iv_m036_summary_drug2, clickListener);

        trDrugs[0] = findViewById(R.id.tr_m036_summary_drug1, this);
        trDrugs[1] = findViewById(R.id.tr_m036_summary_drug2, this);

        tb = findViewById(R.id.tb_m036_summary);
        tb.addOnTabSelectedListener(M036SubDoseLogSummaryFrg.this);
        tb.setSelectedTabIndicatorColor(CAApplication.getInstance().getResources().getColor(R.color.colorM012Cyan));
        tb.setupWithViewPager(mVp);

        mPresenter.loadOffline();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m036_dose_log_summary;
    }

    @Override
    protected M036SubDoseLogSummaryPresenter getPresenter() {
        return new M036SubDoseLogSummaryPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tr_m036_summary_drug1:
            case R.id.tr_m036_summary_drug2:
                if (mVp.getAdapter() == null) return;
                if (changeTab(idView == R.id.tr_m036_summary_drug1 ? TAB_DRUG1 : TAB_DRUG2)) {
                    mAdapter = new SummaryTabPageAdapter(DrugColorType.DEFAULT_COLOR_TYPE_BOTH,
                            mPresenter.getListDataInfo(), Arrays.asList(LIST_KEYS_LAST), mContext, this);
                } else {
                    mAdapter = new SummaryTabPageAdapter(trDrugs[TAB_DRUG1].getBackground().getLevel() == LEVEL_SELECTED ?
                            DrugColorType.DEFAULT_COLOR_TYPE_TRESIBA : DrugColorType.DEFAULT_COLOR_TYPE_FIASP,
                            mPresenter.getListDataInfo(), Arrays.asList(LIST_KEYS_LAST), mContext, this);
                }
                mVp.setAdapter(mAdapter);
                break;
            default:
                break;
        }
    }

    public boolean changeTab(int index) {
        if (trDrugs[index].getBackground().getLevel() == LEVEL_DESELECT) {
            trDrugs[index].getBackground().setLevel(LEVEL_SELECTED);
            ivDrugs[index].getBackground().setLevel(LEVEL_SELECTED);
        } else {
            boolean isExistCheck = false;
            for (int i = 0; i < trDrugs.length; i++) {
                if (i != index && trDrugs[i].getBackground().getLevel() == LEVEL_SELECTED) {
                    isExistCheck = true;
                    break;
                }
            }

            if (isExistCheck) {
                trDrugs[index].getBackground().setLevel(LEVEL_DESELECT);
                ivDrugs[index].getBackground().setLevel(LEVEL_DESELECT);
                ivDrugs[index].setColorFilter(ContextCompat.getColor(mContext, R.color.colorWhite));
            }
        }

        boolean isExistAllCheck = true;
        for (TableRow trDrug : trDrugs) {
            if (trDrug.getBackground().getLevel() == LEVEL_DESELECT) {
                isExistAllCheck = false;
                break;
            }
        }
        return isExistAllCheck;
    }

    @Override
    protected void defineBackKey() {
        if (getTagSource().equals(HomeActivity.TAG)) {
            CommonUtil.getInstance().defineBackTag(getTagSource(), TAG);
        }
    }

    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (tab != null) {
            CommonUtil.wtfi(TAG, "TAB pos: " + tab.getPosition());
            currentPage = tab.getPosition();
        } else {
            currentPage = 0;
        }

        int numberDays = 0;
        switch (currentPage) {
            case 0:
                numberDays = 7;
                break;
            case 1:
                numberDays = 14;
                break;
            case 2:
                numberDays = 28;
                break;
        }

        mAdapter.initDataForDays(numberDays, getCurrentView(mVp));
        LinearLayout tabLayout = (LinearLayout) ((ViewGroup) tb.getChildAt(0)).getChildAt(currentPage);
        TextView tabTextView = (TextView) tabLayout.getChildAt(1);
        tabTextView.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
    }

    private View getCurrentView(ViewPager viewPager) {
        try {
            final int currentItem = viewPager.getCurrentItem();
            CommonUtil.wtfi(TAG, "currentItem..." + currentItem);
            for (int i = 0; i < viewPager.getChildCount(); i++) {
                final View child = viewPager.getChildAt(i);
                final ViewPager.LayoutParams layoutParams = (ViewPager.LayoutParams) child.getLayoutParams();

                Field f = layoutParams.getClass().getDeclaredField("position"); //NoSuchFieldException
                f.setAccessible(true);
                int position = (Integer) f.get(layoutParams); //IllegalAccessException

                if (!layoutParams.isDecor && currentItem == position) {
                    return child;
                }
            }
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
            Log.e(TAG, e.toString());
        }
        return null;
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

        LinearLayout tabLayout = (LinearLayout) ((ViewGroup) tb.getChildAt(0)).getChildAt(tab.getPosition());
        TextView tabTextView = (TextView) tabLayout.getChildAt(1);
        tabTextView.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        // This feature isn't require here
    }

    @Override
    public void onNovoItemSelected(int colorType, BarData mData, Entry entry, Highlight highlight) {
        if (mData != null && entry != null) {
            this.colorType = colorType;
            DateEntity entryData = (DateEntity) entry.getData();
            CommonUtil.wtfi(TAG, "Color Type: " + ((DateEntity) entry.getData()).getColor());
            showSummaryDialog(entryData, entryData.getColor());
        }
    }

    @Override
    public void dataAS05Ready() {
        CommonUtil.convertToLocalDateData(mPresenter.getListDataInfo());

        List<DataInfo> listResult = mPresenter.getListDataInfo();
        boolean isShowDrug1 = false;
        boolean isShowDrug2 = false;
        for (DataInfo dataInfo : listResult) {
            String fieldCustom3Value = dataInfo.getResult().getCustom3();

            if (String.valueOf(DeviceEntity.CODE_FIASP).equalsIgnoreCase(fieldCustom3Value)) {
                isShowDrug2 = true;
            }

            if (String.valueOf(DeviceEntity.CODE_TRESIBA_100).equalsIgnoreCase(fieldCustom3Value) || String.valueOf(DeviceEntity.CODE_TRESIBA_200).equalsIgnoreCase(fieldCustom3Value)) {
                isShowDrug1 = true;
            }
        }

        if (isShowDrug1 && isShowDrug2) {
            // two drug
            changeTab(TAB_DRUG1);
            mTrTab.setVisibility(View.VISIBLE);
            mTvTab1.setText(DeviceEntity.TRESIBA);
            mTvTab2.setText(DeviceEntity.FIASP);

            mAdapter = new SummaryTabPageAdapter(DrugColorType.DEFAULT_COLOR_TYPE_TRESIBA,
                    mPresenter.getListDataInfo(), Arrays.asList(LIST_KEYS_LAST), mContext, this);
        } else if (isShowDrug1 || isShowDrug2) {
            // one drug
            mTrTab.setVisibility(View.GONE);


            String name = isShowDrug2 ? getString(R.string.txt_fiasp) : getString(R.string.txt_tresiba);
            String color = DrugColorType.DEFAULT_COLOR_TYPE_TRESIBA;
            if (DeviceEntity.FIASP.equalsIgnoreCase(name)) {
                color = DrugColorType.DEFAULT_COLOR_TYPE_FIASP;
            }

            mAdapter = new SummaryTabPageAdapter(color,
                    mPresenter.getListDataInfo(), Arrays.asList(LIST_KEYS_LAST), mContext, this);
        } else {
            // no drug
            mTrTab.setVisibility(View.GONE);
            mAdapter = new SummaryTabPageAdapter(DrugColorType.DEFAULT_COLOR_NO_DOSE,
                    mPresenter.getListDataInfo(), Arrays.asList(LIST_KEYS_LAST), mContext, this);
            findViewById(R.id.ln_m036_main).setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
            findViewById(R.id.view_divider_1).setVisibility(View.GONE);
        }

        mVp.setAdapter(mAdapter);
        mVp.setCurrentItem(0);
    }

    @Override
    public void showSummaryDialog(DateEntity item, int colorType) {
        if (m036DoseDetailDialog == null) {
            m036DoseDetailDialog = new M036DoseDetailDialog(mContext, true);
            m036DoseDetailDialog.setOnCallBack(this);
        }
        m036DoseDetailDialog.show();
        m036DoseDetailDialog.setUpInfo(item, colorType);
        if (m036DoseDetailDialog.isShowing()) m036DoseDetailDialog.initData();
    }

    @Override
    public void toPreviousDay(int colorType, int currentPos) {
        switch (getCurrentPage()) {
            case 0:
                showSummaryDialog(getStorage().getList7DayData().get(currentPos - 1), colorType);
                break;
            case 1:
                showSummaryDialog(getStorage().getList14DayData().get(currentPos - 1), colorType);
                break;
            case 2:
                showSummaryDialog(getStorage().getList28DayData().get(currentPos - 1), colorType);
                break;
        }
    }

    @Override
    public void toNextDay(int colorType, int currentPos) {
        switch (getCurrentPage()) {
            case 0:
                showSummaryDialog(getStorage().getList7DayData().get(currentPos + 1), colorType);
                break;

            case 1:
                showSummaryDialog(getStorage().getList14DayData().get(currentPos + 1), colorType);
                break;

            case 2:
                showSummaryDialog(getStorage().getList28DayData().get(currentPos + 1), colorType);
                break;
        }
    }

    @Override
    public int getCurrentPage() {
        return currentPage;
    }
}
