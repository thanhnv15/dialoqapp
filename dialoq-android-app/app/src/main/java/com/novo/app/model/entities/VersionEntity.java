package com.novo.app.model.entities;

import java.io.Serializable;

public class VersionEntity implements Serializable {
    private TextEntity versionName;
    private TextEntity versionComparison;

    public VersionEntity(TextEntity versionName, TextEntity versionComparison) {
        this.versionName = versionName;
        this.versionComparison = versionComparison;
    }

    public TextEntity getVersionName() {
        return versionName;
    }

    public void setVersionName(TextEntity versionName) {
        this.versionName = versionName;
    }

    public TextEntity getVersionComparison() {
        return versionComparison;
    }

    public void setVersionComparison(TextEntity versionComparison) {
        this.versionComparison = versionComparison;
    }
}
