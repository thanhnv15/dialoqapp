package com.novo.app.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.novo.app.CAApplication;
import com.novo.app.R;

import java.util.Calendar;
import java.util.Date;

public class NotificationPlusReminder extends BroadcastReceiver {
    private static final String TAG = NotificationPlusReminder.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String mMaxPlus14 = CommonUtil.getInstance().getPrefContent(CommonUtil.KEY_NOTIFY_14_DAY_LAST_DOSE);
        String mCurrent = CommonUtil.getDateNow(CommonUtil.DATE_NOW_DY);

        Date mMax14 = CommonUtil.getInstance().getDateAfter(mMaxPlus14);
        Date mCurrentDate = CommonUtil.getInstance().getDateAfter(mCurrent);

        CommonUtil.wtfi(TAG, "NotificationPlusReminder : onReceive...");
        if (!CAApplication.getInstance().getStorageCommon().isAppActive()) {
            CommonUtil.wtfi(TAG, "App is being block");
            return;
        }
        Calendar calendarCurrent = Calendar.getInstance();
        calendarCurrent.setTime(mCurrentDate);
        int getHours = calendarCurrent.get(Calendar.HOUR_OF_DAY);
        int getMinutes = calendarCurrent.get(Calendar.MINUTE);
        int getSeconds = calendarCurrent.get(Calendar.SECOND);

        if (mMax14.equals(mCurrentDate) && (getHours == CommonUtil.HOUR_REMINDER && getMinutes == CommonUtil.MINUTE_REMINDER && getSeconds == CommonUtil.SECOND_REMINDER)) {
            CommonUtil.getInstance().showNotification(context, context.getString(R.string.txt_noti_sync_title),
                    context.getString(R.string.txt_noti_sync_description), CommonUtil.KEY_NOTIFY_14_DAY_LAST_DOSE);
        }
    }
}
