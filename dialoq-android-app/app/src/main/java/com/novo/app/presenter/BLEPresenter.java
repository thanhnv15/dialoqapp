package com.novo.app.presenter;

import com.novo.app.view.event.OnCallBackToView;

public class BLEPresenter<G extends OnCallBackToView> extends BasePresenter<G> {
    public BLEPresenter(G event) {
        super(event);
    }
}
