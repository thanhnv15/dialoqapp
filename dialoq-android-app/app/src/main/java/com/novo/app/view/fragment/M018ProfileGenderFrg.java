package com.novo.app.view.fragment;

import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.presenter.M018ProfileGenderPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.CASpinnerAdapter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM018ProfileGenderCallBack;
import com.novo.app.view.event.OnSpinnerCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

import java.util.ArrayList;
import java.util.List;

public class M018ProfileGenderFrg extends BaseFragment<M018ProfileGenderPresenter, OnHomeBackToView> implements OnM018ProfileGenderCallBack, OnSpinnerCallBack {
    public static final String TAG = M018ProfileGenderFrg.class.getName();

    private static final int KEY_SPINNER_GENDER_TYPE = 1;
    private Spinner mSpinnerGender;
    private TextView mTvGender;
    private Button mBtNext;

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m018_profile_setup, CAApplication.getInstance().getBoldFont());
        TextView mTvWhatGender = findViewById(R.id.tv_m018_what_gender, CAApplication.getInstance().getRegularFont());
        if (!getStorage().isMature())
            mTvWhatGender.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m018_what_gender_immature)));
        findViewById(R.id.tv_m018_not_now, this, CAApplication.getInstance().getMediumFont());

        mBtNext = findViewById(R.id.bt_m018_next, this, CAApplication.getInstance().getMediumFont());
        mBtNext.setEnabled(false);

        mTvGender = findViewById(R.id.tv_m018_gender, this, CAApplication.getInstance().getRegularFont());

        if (getStorage().getM001ProfileEntity().getGender() != null && !getStorage().getM001ProfileEntity().getGender().getValue().equals("")) {
            mTvGender.setText(getStorage().getM001ProfileEntity().getGender().getValue());
            mTvGender.setTypeface(CAApplication.getInstance().getBoldFont());
            mBtNext.setEnabled(true);
        }

        findViewById(R.id.iv_m018_gender, this);
        findViewById(R.id.iv_m018_back, this);


        mSpinnerGender = findViewById(R.id.spin_m018_gender);
        mTvGender.setTag(getStorage().getM001ProfileEntity().getGender());
        createGender();
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    private void createGender() {
        List<TextEntity> listGender = new ArrayList<>();
        List<TextEntity> data = getStorage().getM001ConfigSet().getGenderEntity();
        for (int i = 0; i < data.size(); i++) {
            String createGender = LangMgr.getInstance().getLangList().get(data.get(i).getValue());
            listGender.add(new TextEntity(data.get(i).getKey(), createGender));
        }

        CASpinnerAdapter<OnSpinnerCallBack> adapter = new CASpinnerAdapter<>(KEY_SPINNER_GENDER_TYPE, listGender, mContext);
        adapter.setOnSpinnerCallBack(this);
        mSpinnerGender.setAdapter(adapter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m018_profile_gender;
    }

    @Override
    protected M018ProfileGenderPresenter getPresenter() {
        return new M018ProfileGenderPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        mCallBack.showFrgScreen(TAG, M017ProfileBornFrg.TAG);
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m018_gender:
            case R.id.iv_m018_gender:
                mSpinnerGender.performClick();
                break;
            case R.id.tv_m018_not_now:
                getStorage().getM001ProfileEntity().setGender(new TextEntity("", ""));
                mCallBack.showFrgScreen(TAG, M008SignUpDiabetesTypeFrg.TAG);
                break;
            case R.id.bt_m018_next:
                getStorage().getM001ProfileEntity().setGender(((TextEntity) mTvGender.getTag()));
                mCallBack.showFrgScreen(TAG, M008SignUpDiabetesTypeFrg.TAG);
                break;
            case R.id.iv_m018_back:
                backToPreviousScreen();
                break;
            default:
                break;
        }
    }

    @Override
    public void backToPreviousScreen() {
        mCallBack.showFrgScreen(TAG, M016ProfileStartFrg.TAG);
    }

    @Override
    public void onItemClick(int key, int pos, Object data) {
        if (key == KEY_SPINNER_GENDER_TYPE) {
            TextEntity entity = ((List<TextEntity>) data).get(pos);
            mTvGender.setText(entity.getValue());
            mTvGender.setTag(entity);

            mTvGender.setTypeface(CAApplication.getInstance().getBoldFont());
            mBtNext.setEnabled(true);
            closeSpinner(mSpinnerGender);
        }
    }
}
