package com.novo.app.utils;

import android.os.Handler;
import android.os.Message;

import com.novo.app.view.event.OnHandleMessage;

import java.lang.ref.WeakReference;

public class MyHandler<T, G extends OnHandleMessage> extends Handler {
    private WeakReference<T> reference;
    private G mCallBack;

    public MyHandler(WeakReference<T> reference, G event) {
        this.reference = reference;
        mCallBack = event;
    }

    public WeakReference<T> getReference() {
        return reference;
    }

    @Override
    public void handleMessage(Message msg) {
        if (reference == null || reference.get() == null) return;
        mCallBack.doMessage(msg);
    }
}
