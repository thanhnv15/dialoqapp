package com.novo.app.presenter;

import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RReminderEntity;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM065ReminderCallBack;
import com.novo.app.view.widget.ProgressLoading;

public class M065AddEditReminderPresenter extends BasePresenter<OnM065ReminderCallBack> {
    public static final String KEY_API_AS02_CREATE_ASSESSMENT_SCHEDULE = "KEY_API_AS02_CREATE_ASSESSMENT_SCHEDULE";
    public static final String KEY_API_AS10_UPDATE_ASSESSMENT_SCHEDULE = "KEY_API_AS10_UPDATE_ASSESSMENT_SCHEDULE";
    public static final String KEY_API_AS09_DELETE_ASSESSMENT_SCHEDULE = "KEY_API_AS09_DELETE_ASSESSMENT_SCHEDULE";
    private static final String TAG = M065AddEditReminderPresenter.class.getName();

    public M065AddEditReminderPresenter(OnM065ReminderCallBack event) {
        super(event);
    }

    private void prepareAS02() {
        CommonUtil.wtfi(TAG, "prepareAS02...");
        RReminderEntity entity = CADBManager.getInstance().getRReminderEntity();
        if (entity == null) {
            mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            ProgressLoading.dismiss();
            return;
        }
        getStorage().setM065ScheduleEntity(entity);
        String[] reminder = entity.getStartTime().split(":");

        CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_HOUR, Integer.valueOf(reminder[0]));
        CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_MIN, Integer.valueOf(reminder[1]));
        mListener.closeDialog(M065AddEditReminderPresenter.KEY_API_AS02_CREATE_ASSESSMENT_SCHEDULE);
    }

    private void prepareAS10() {
        CommonUtil.wtfi(TAG, "prepareAS10...");
        RReminderEntity entity = CADBManager.getInstance().getRReminderEntity();
        if (entity == null) {
            mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            ProgressLoading.dismiss();
            return;
        }
        String[] reminder = entity.getStartTime().split(":");

        CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_HOUR, Integer.valueOf(reminder[0]));
        CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_MIN, Integer.valueOf(reminder[1]));
        mListener.closeDialog(KEY_API_AS10_UPDATE_ASSESSMENT_SCHEDULE);
    }

    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tagRequest);
        ProgressLoading.dismiss();
        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        if (responseEntity == null) {
            mListener.showAlert(LangMgr.getInstance().getLangList().get(ERR_NO_CONNECTION));
        }
    }

    public void delReminder() {
        CommonUtil.wtfi(TAG, "delReminder");
        if (CADBManager.getInstance().delReminderEntity()) {
            mListener.closeDialog(KEY_API_AS09_DELETE_ASSESSMENT_SCHEDULE);
        }
    }


    @Override
    public void hideLockDialog(String key) {
        // This feature isn't require here
    }

    public void addReminder(String time) {
        CommonUtil.wtfi(TAG, "addReminder");
        RReminderEntity entity = new RReminderEntity();
        CommonUtil.getInstance().prepareReminder(entity, time, "");
        entity.setStatusFlag(RReminderEntity.STATE_ADD);

        if (CADBManager.getInstance().addRReminderEntity(entity)) {
            prepareAS02();
        }
    }

    public void activeReminder(String time) {
        String scheduleNotificationId = getStorage().getM065ScheduleEntity()
                .getScheduleNotificationId();
        RReminderEntity entity = CADBManager.getInstance().getRReminderEntity();
        CommonUtil.wtfi(TAG, "cancelReminder: RReminderEntity " + entity);
        if (entity == null) {
            return;
        }
        CommonUtil.getInstance().prepareReminder(entity, time, scheduleNotificationId);

        if (CADBManager.getInstance().editRReminderEntity(entity)) {
            prepareAS10();
        }
    }

}
