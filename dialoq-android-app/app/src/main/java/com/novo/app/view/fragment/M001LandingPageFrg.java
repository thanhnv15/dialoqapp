package com.novo.app.view.fragment;

import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.ConfigEntity;
import com.novo.app.model.entities.ProfileEntity;
import com.novo.app.presenter.M001LandingPagePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM001LandingPageCallBack;

import static com.novo.app.presenter.M025SubDoseLogPresenter.KEY_SAVE_CALLING_AS5;
import static com.novo.app.service.SyncScheduleService.KEY_SYNC_READY;

public class M001LandingPageFrg extends BaseFragment<M001LandingPagePresenter, OnHomeBackToView> implements OnM001LandingPageCallBack {
    public static final String TAG = M001LandingPageFrg.class.getName();
    private ConfigEntity configEntity;
    private TextView mTvCreateAcc, mTvLogin, mTvLanguage;

    @Override
    protected void initViews() {
        getStorage().clearAllDataLogin();
        mTvLanguage = findViewById(R.id.tv_m001_language, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.iv_m001_arrow_down, this);
        findViewById(R.id.tv_m001_an_easy, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m001_slogan, CAApplication.getInstance().getBoldFont());

        configEntity = getStorage().getM001ConfigSet();
        mTvCreateAcc = findViewById(R.id.tv_m001_create_acc, this, false, CAApplication.getInstance().getBoldFont());
        mTvLogin = findViewById(R.id.tv_m001_login, this, false, CAApplication.getInstance().getBoldFont());

        mTvLanguage.setText(LangMgr.getInstance().getCurrentLang());
        if (LangMgr.getInstance().getLangList() != null && LangMgr.getInstance().getLangList().size() > 0
                && configEntity != null && CommonUtil.getInstance().isConnectToNetwork()) {
            mTvCreateAcc.setEnabled(true);
            mTvLogin.setEnabled(true);
        } else {
            mTvCreateAcc.setEnabled(false);
            mTvLogin.setEnabled(false);
        }

        //delete old data from last session
        CommonUtil.wtfi(TAG, "initView...");
//        CADBManager.getInstance().deleteAllRTokenEntity();
//        CADBManager.getInstance().deleteAllRDose();
//        CADBManager.getInstance().deleteAllRNote();
//        CADBManager.getInstance().deleteAllInjection();
        CommonUtil.getInstance().clearPrefContent(KEY_SYNC_READY);
        CommonUtil.getInstance().clearPrefContent(KEY_SAVE_CALLING_AS5);

        checkForConnection();
        getStorage().setM029FirstPair(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        checkBlockApp();
    }

    private void checkForConnection() {
        if (!CommonUtil.getInstance().isConnectToNetwork()) {
            mTvCreateAcc.setEnabled(false);
            mTvLogin.setEnabled(false);
            mTvLanguage.setEnabled(false);
            mCallBack.showNetworkAlert();
        }
    }

    public void checkBlockApp() {
        if (mTvCreateAcc != null && mTvLogin != null && mTvLanguage != null) {
            boolean isNoAccount;
            if (CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME) == null) {
                isNoAccount = true;
            } else isNoAccount = CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME).equals("");
            mTvCreateAcc.setEnabled(getStorage().isAppActive() && CommonUtil.getInstance().isConnectToNetwork() && isNoAccount);
            mTvLogin.setEnabled(getStorage().isAppActive() && CommonUtil.getInstance().isConnectToNetwork());
            mTvLanguage.setEnabled(getStorage().isAppActive() && CommonUtil.getInstance().isConnectToNetwork());
        }
    }

    @Override
    protected void initData() {
        CommonUtil.wtfd(TAG, "initData.......");
        //Call API get config set
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m001_landing_page;
    }

    @Override
    protected M001LandingPagePresenter getPresenter() {
        return new M001LandingPagePresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        if (getTagSource().equals(M001LandingPageFrg.TAG)) {
            CommonUtil.getInstance().defineBackTag(getTagSource(), TAG);
        }
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m001_language:
            case R.id.iv_m001_arrow_down:
                if (!mTvLanguage.isEnabled()) return;
                if (LangMgr.getInstance().getLangList()
                        != null && LangMgr.getInstance().getLangList().size() > 0 && configEntity != null) {
                    mCallBack.showFrgScreen(TAG, M002SignUpChooseLanguageFrg.TAG);
                } else {
                    showNotify("Null lang list");
                }
                break;
            case R.id.tv_m001_create_acc:
                getStorage().setM001ProfileEntity(new ProfileEntity());
                mCallBack.showFrgScreen(TAG, M003LoginIntroVideoFrg.TAG);
                break;
            case R.id.tv_m001_login:
                mCallBack.showFrgScreen(TAG, M023LoginFrg.TAG);
                break;
            default:
                break;
        }
    }

    @Override
    public void backToPreviousScreen() {
        mCallBack.backToPreviousScreen();
    }

}
