package com.novo.app.model.entities;

import java.io.Serializable;

public class CountryVersionEntity implements Serializable {
    private TextEntity countryCode;
    private TextEntity versionName;
    private TextEntity versionComparison;

    public CountryVersionEntity(TextEntity countryCode, TextEntity versionName, TextEntity versionComparison) {
        this.countryCode = countryCode;
        this.versionName = versionName;
        this.versionComparison = versionComparison;
    }

    public TextEntity getVersionName() {
        return versionName;
    }

    public void setVersionName(TextEntity versionName) {
        this.versionName = versionName;
    }

    public TextEntity getVersionComparison() {
        return versionComparison;
    }

    public void setVersionComparison(TextEntity versionComparison) {
        this.versionComparison = versionComparison;
    }

    public TextEntity getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(TextEntity countryCode) {
        this.countryCode = countryCode;
    }
}
