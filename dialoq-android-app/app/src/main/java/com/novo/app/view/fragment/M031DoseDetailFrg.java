/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.view.fragment;

import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M031DoseDetailPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM031DoseDetailCallBack;
import com.novo.app.view.widget.NVDateUtils;

/**
 * Dose Detail Fragment
 * <p>
 * This fragment is 'View' object in MVP pattern
 * This fragment uses an {@link M031DoseDetailPresenter} as linked Presenter
 * Callbacks to this fragment will be defined by {@link OnM031DoseDetailCallBack} interface
 * <p>
 * This fragment is used in View Dose Log feature of dialog app, where user can see the detail of an error dose log
 */
public class M031DoseDetailFrg extends BaseFragment<M031DoseDetailPresenter, OnM024DoseLogCallBack> implements OnM031DoseDetailCallBack {
    public static final String TAG = M031DoseDetailFrg.class.getName();
    private TextView mTvTime;

    /**
     * Get presenter for this fragment
     *
     * @return {@link M031DoseDetailPresenter} instance
     * @see BaseFragment#getPresenter()
     */
    @Override
    protected M031DoseDetailPresenter getPresenter() {
        return new M031DoseDetailPresenter(this);
    }

    /**
     * Handle callback from {@link M031DoseDetailPresenter}
     * {@link OnM024DoseLogCallBack#showM001LandingScreen(String err)}
     *
     * @param message error string description
     */
    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    /**
     * Initiate views component for Dose Detail Fragment
     *
     * @see BaseFragment#initViews()
     */
    @Override
    protected void initViews() {
        mCallBack.changeTab(CommonUtil.TAB_MENU_DOSE_LOG);
        mTvTime = findViewById(R.id.tv_m031_time, CAApplication.getInstance().getRegularFont());

        TextView mTvType = findViewById(R.id.tv_m031_drug_type, CAApplication.getInstance().getBoldFont());

        if (CommonUtil.isUnknownDoseSize(getStorage().getM025ResultDataItem().getResult().getResult(),
                getStorage().getM025ResultDataItem().getResult().getCustom4())) {
            mTvType.setText(getString(R.string.txt_m031_dose_size_not_detected));
        } else if (CommonUtil.isUnknownDrug(getStorage().getM025ResultDataItem().getResult().getCustom3()
                , getStorage().getM025ResultDataItem().getResult().getCustom4())) {
            mTvType.setText(getString(R.string.txt_m031_drug_type));
        }

        findViewById(R.id.tv_m031_what_happened, CAApplication.getInstance().getBoldFont());

        TextView mTvContent1 = findViewById(R.id.tv_m031_content_1, CAApplication.getInstance().getRegularFont());
        String highLight = mContext.getString(R.string.txt_m031_content_1_highlight);
        highLightText(mTvContent1, highLight, R.color.colorBlack, true);

        findViewById(R.id.tv_m031_help, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m031_device_id, CAApplication.getInstance().getMediumFont());

        TextView mTvDeviceId = findViewById(R.id.tv_m031_id, CAApplication.getInstance().getRegularFont());
        mTvDeviceId.setText(getStorage().getM025ResultDataItem().getResult().getCustom1());

        findViewById(R.id.iv_m031_back, this);
        mCallBack.doTimeZoneChange();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mTvTime != null) {
            getStorage().getM025ResultDataItem().getResult()
                    .setReportedDate(CommonUtil.convertToLocalDate(getStorage()
                            .getM025ResultDataItem().getResult().getReportedDate()));
            getStorage().getM025ResultDataItem().setActivityDate(CommonUtil.convertToLocalDate(getStorage()
                    .getM025ResultDataItem().getActivityDate()));

            String date = NVDateUtils.getDoseDetailsTimeString(getStorage().getM025ResultDataItem().getResult().getReportedDate());
            mTvTime.setText(date);
        }
    }

    /**
     * Get layout id for Dose Detail Fragment
     *
     * @return layout id
     * @see BaseFragment#getLayoutId()
     */
    @Override
    public int getLayoutId() {
        return R.layout.frg_m031_dose_detail;
    }

    /**
     * @return tag in {@link String}
     * @see BaseFragment#getTAG()
     */
    @Override
    protected String getTAG() {
        return TAG;
    }

    /**
     * @see BaseFragment#defineBackKey()
     */
    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    /**
     * @param idView id of clicked view
     * @see BaseFragment#onClickView(int)
     */
    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m031_back:
                mCallBack.showChildFrgScreen(TAG, M025SubDoseLogFrg.TAG);
                break;

            case R.id.tv_m031_help:
                mCallBack.showChildFrgScreen(TAG, M047SupportTroubleshootingFrg.TAG);
                mCallBack.changeTab(CommonUtil.TAB_MENU_SUPPORT);
                break;
            default:
                break;
        }
    }

}
