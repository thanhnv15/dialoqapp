package com.novo.app.view.event;

public interface OnM061MorePasswordCallBack extends OnCallBackToView{
    void changePasswordSuccess();

    void showDialog();
}
