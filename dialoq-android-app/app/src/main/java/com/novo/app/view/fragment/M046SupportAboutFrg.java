package com.novo.app.view.fragment;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M046SupportAboutPresenter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM046SupportAboutCallBack;

public class M046SupportAboutFrg extends BaseFragment<M046SupportAboutPresenter, OnM024DoseLogCallBack> implements OnM046SupportAboutCallBack {
    public static final String TAG = M046SupportAboutFrg.class.getName();

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m046_logo);
        findViewById(R.id.iv_m046_back, this);
        findViewById(R.id.tv_m046_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m046_lorem_ipsum, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m046_version, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m046_product, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m046_dialoq_company, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m046_country, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m046_dialoq_version, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m046_update, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.iv_m046_europe);
        findViewById(R.id.tv_m046_medical, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m046_address, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m046_phone_number, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m046_email, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m046_legal, CAApplication.getInstance().getRegularFont());
    }

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m046_support_about;
    }

    @Override
    protected M046SupportAboutPresenter getPresenter() {
        return new M046SupportAboutPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {

    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m046_back:
                mCallBack.showChildFrgScreen(TAG, M026SupportFrg.TAG);
                break;
        }
    }

}
