/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.ble.BLEInfoEntity;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RTokenEntity;
import com.novo.app.model.entities.ConfigEntity;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.model.entities.TopicEntity;
import com.novo.app.presenter.BasePresenter;
import com.novo.app.presenter.HomePresenter;
import com.novo.app.service.SyncScheduleService;
import com.novo.app.utils.CATask;
import com.novo.app.utils.CommonUtil;
import com.novo.app.utils.NetworkChangeReceiver;
import com.novo.app.utils.TimeZoneReceiver;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnLanguageCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.fragment.M001LandingPageFrg;
import com.novo.app.view.fragment.M023LoginFrg;
import com.novo.app.view.fragment.M024DoseLogFrg;
import com.novo.app.view.fragment.M025SubDoseLogFrg;
import com.novo.app.view.fragment.M065MoreReminderNotEmptyFrg;
import com.novo.app.view.widget.NVDateUtils;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.ProgressLoading;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import static com.novo.app.model.entities.DeviceEntity.SYS_EOL_WARNING;
import static com.novo.app.presenter.M025SubDoseLogPresenter.KEY_SAVE_CALLING_AS5;
import static com.novo.app.service.SyncScheduleService.KEY_SYNC_READY;
import static com.novo.app.utils.NetworkChangeReceiver.KEY_SAVE_TIME_OFFLINE;

/**
 * Created by ThanhNv on 4/23/2017.
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
public class HomeActivity extends BLEActivity<HomePresenter> implements OnHomeBackToView,
        OnLanguageCallBack, CATask.OnTaskCallBackToView {
    public static final String TAG = HomeActivity.class.getName();
    private static final int REQUEST_CODE = 101;
    private static final String ALREADY_VALUE = "YES";
    private static final String KEY_SHOW_BG_SYNC = "KEY_SHOW_BG_SYNC";

    private static final String SLASH = "/"; // Ignore warning of hard-coded string
    private static final String DOT = "."; // Ignore warning of hard-coded string

    private final HashMap<String, BaseFragment> mFrags = new HashMap<>();
    private AlertDialog mAlertDialog;
    private AlertDialog mAlertNetwork;
    private BLEInfoEntity mData;
    private int filesRemain;
    private boolean isNewLanguage;

    private BroadcastReceiver mTimeZoneReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null
                    && intent.getAction() != null
                    && intent.getAction().equals(TimeZoneReceiver.ACTION_SHOW_TIME_ZONE_CHANGE)) {

                getStorage().notifyDialogCallBack();
                warningForTimeZoneChange();
            }
        }
    };

    private void warningForTimeZoneChange() {
        CommonUtil.wtfi(TAG, "warningForTimeZoneChange...");
        CommonUtil.getInstance().savePrefContent(CommonUtil.PREF_TIMEZONE, TimeZone.getDefault().getID());
        if (CommonUtil.getInstance().getPrefContent(CommonUtil.REMINDER_SET) != null
                && CommonUtil.getInstance().getPrefContent(CommonUtil.REMINDER_SET).equals("true")) {
            CommonUtil.getInstance().showDialog(HomeActivity.this,
                    CAApplication.getInstance().getString(R.string.txt_alert_time_zone_title),
                    CAApplication.getInstance().getString(R.string.txt_alert_time_zone_content),
                    CAApplication.getInstance().getString(R.string.txt_alert_time_zone_settings),
                    CAApplication.getInstance().getString(R.string.txt_alert_time_zone_ok), new OnOKDialogAdapter() {
                        @Override
                        public void handleOKButton2() {
                            mPresenter.cancelReminder();
                        }

                        @Override
                        public void handleOKButton1() {
                            getStorage().setPreTAG(M065MoreReminderNotEmptyFrg.TAG);
                            showFrgScreen(TAG, M024DoseLogFrg.TAG);
                        }
                    });
        }
    }

    @Override
    public void cancelReminder() {
        mPresenter.cancelReminder();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_home;
    }

    @Override
    protected void initViews() {
        super.initViews();
        CommonUtil.wtfd(TAG, "initViews...");
        //save time-zone
        if (CommonUtil.getInstance().getPrefContent(CommonUtil.PREF_TIMEZONE) == null) {
            CommonUtil.getInstance().savePrefContent(CommonUtil.PREF_TIMEZONE, TimeZone.getDefault().getID());
        }
        LangMgr.getInstance().setCallBack(this);
        requestPermission();
        registerTimeZoneChange();

    }

    private void registerTimeZoneChange() {
        try {
            IntentFilter filter = new IntentFilter();
            filter.addAction(Intent.ACTION_TIME_CHANGED);
            filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
            registerReceiver(new TimeZoneReceiver(), filter);
            CommonUtil.wtfi(TAG, "registerTimeZoneChange...");
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getMessage());
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            String keyTag = intent.getStringExtra(CommonUtil.KEY_NOTIFY);
            getStorage().setKeyTagNotify(keyTag);
        }
        if (getStorage().getKeyTagNotify() == null) return;
        if (getStorage().getKeyTagNotify() != null) {
            showFrgScreen(TAG, M024DoseLogFrg.TAG);
        }
    }

    @Override
    protected void doMessage(Message msg) {
        if (msg == null) {
            mScanUtil.startScan();
            return;
        }

        if (!getStorage().isAppActive()) {
            CommonUtil.wtfe(TAG, "mBlockApp...true => Could not sync in background");
            startAgain();
            return;
        }

        DeviceEntity item = isExistDevicePaired();
        if (item == null) {
            startAgain();
            return;
        }


        mData = (BLEInfoEntity) msg.obj;

        if (mData.getSystemFlag().equals(DeviceEntity.SYS_EOL + "")
                || mData.getSystemFlag().equals(DeviceEntity.SYS_ERR + "")) {

            CommonUtil.getInstance().showDialog(this,
                    CAApplication.getInstance().getString(R.string.app_name),
                    mData.getSystemFlag().equals(DeviceEntity.SYS_EOL + "") ?
                            CAApplication.getInstance().getString(R.string.txt_err_eol) :
                            CAApplication.getInstance().getString(R.string.txt_err_err),
                    CAApplication.getInstance().getString(R.string.txt_sync_success_ok),
                    null, new OnOKDialogCallBack() {
                        @Override
                        public void handleOKButton1() {
                            // Do nothing
                        }
                    });
            startAgain();
            return;
        }

        mData.setDeviceName(mDeviceName);
        if (mData.getDoseItems() == null || mData.getDoseItems().isEmpty()) {
            startAgain();
            return;
        }

        try {
            int lastNumber = getLastNumber(item.getDeviceID());
            CommonUtil.wtfi(TAG, "lastNumber:..." + lastNumber);
            if (lastNumber < mData.getDoseItems().size()) {
                mPresenter.syncDataOffline(item, lastNumber, mData);
            } else {
                startAgain();
            }
        } catch (Exception e) {
            CommonUtil.wtfd(TAG, e.getMessage());
        }
    }


    private int getLastNumber(String deviceID) {
        if (getStorage().getM025ListResult() == null || getStorage().getM025ListResult().isEmpty())
            return 0;
        int max = 0;
        for (int i = 0; i < getStorage().getM025ListResult().size(); i++) {
            DataInfo item = getStorage().getM025ListResult().get(i);
            int number;
            try {
                number = Integer.parseInt(item.getResult().getNotes());
            } catch (Exception e) {
                number = 0;
            }
            if (item.getResult().getCustom1() != null && item.getResult().getCustom1().equals(deviceID) && number > max) {
                max = number;
            }
        }
        return max;
    }

    @Override
    public void setupReminder() {
        mPresenter.getReminder();
    }

    @Override
    public void startScan() {
        startAgain();
    }

    @Override
    public void preLoginTimeZoneChange() {
        if (CommonUtil.getInstance().getPrefContent(CommonUtil.PREF_TIMEZONE) != null
                && !CommonUtil.getInstance().getPrefContent(CommonUtil.PREF_TIMEZONE).equals(TimeZone.getDefault().getID())) {
            warningForTimeZoneChange();
        }
    }

    @Override
    public void registrationCancel(String tag) {
        String title = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m012_registration_cancel_title));
        String message = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m012_registration_cancel_detail));
        String yes = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m012_registration_cancel_ok));
        String no = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m012_registration_cancel_no));

        CommonUtil.getInstance().showDialog(this, title, message, yes, no, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton1() {
                CommonUtil.wtfd(TAG, "M001LandingPageFrg1");
                showFrgScreen(tag, M001LandingPageFrg.TAG);
            }
        });
    }

    /**
     * <b></b>showLandingFrgScreen</b><br/>
     * <br/> show the landing page screen
     */
    @Override
    public void showLandingFrgScreen() {
        if (getStorage().isStateTokenExpired()) {
            stopService(new Intent(this, SyncScheduleService.class));
        }
        CommonUtil.wtfd(TAG, "M001LandingPageFrg2");
        showFrgScreen(TAG, M001LandingPageFrg.TAG);
    }

    @Override
    public void loginSuccess() {
        if (CommonUtil.getInstance().getPrefContent(CommonUtil.IS_LOGIN) != null
                && CommonUtil.getInstance().getPrefContent(CommonUtil.IS_LOGIN).equals(CommonUtil.IS_LOGIN_TRUE)
                && CommonUtil.getInstance().getPrefContent(CommonUtil.KEEP) != null
                && CommonUtil.getInstance().getPrefContent(CommonUtil.KEEP).equals(CommonUtil.KEEP)) {
            if (!isLoginSectionValid()) {
                String message = LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_err_online_section_expired));
                if (message != null)
                    message = message.replace("${number_days}", getStorage().getM001ConfigSet().getMaximumSection() + "");
                showFrgScreen(TAG, M001LandingPageFrg.TAG);
                forceLogOut(message);
            } else {
                showFrgScreen(TAG, M024DoseLogFrg.TAG);
                startScan();
                CAApplication.getInstance().startSyncService();
            }
        } else {
            showFrgScreen(TAG, M001LandingPageFrg.TAG);
        }
    }

    /**
     * <b>forceLogOut</b><br/>
     * <br/> Force app log out in some cases lead to the token is expired
     * @param message String
     */
    private void forceLogOut(String message) {
        if (getStorage().hasBeenForcedLogOut()) {
            return;
        }
        getStorage().setHasBeenForcedLogOut(true);
        stopService(new Intent(this, SyncScheduleService.class));
        CommonUtil.getInstance().showDialog(this, message, new OnOKDialogCallBack() {
            @Override
            public void handleDialogDismiss() {
                if (!(mFrags.get(currentTag) instanceof M001LandingPageFrg)) {
                    showFrgScreen(TAG, M001LandingPageFrg.TAG);
                }
            }
        });
        CADBManager.getInstance().deleteAllRTokenEntity();
        CommonUtil.getInstance().clearPrefContent(CommonUtil.LOGIN_SECTION_START_DATE);
        CommonUtil.getInstance().clearPrefContent(KEY_SAVE_TIME_OFFLINE);
    }

    @Override
    public void showNetworkAlert() {
        if (mAlertNetwork == null || !mAlertNetwork.isShowing()) {
            mAlertNetwork = CommonUtil.getInstance().showDialog(this, getString(R.string.txt_error_network), null);
        }
    }

    @Override
    public void doTimeZoneChange() {
        try {
            IntentFilter filter = new IntentFilter();
            filter.addAction(TimeZoneReceiver.ACTION_SHOW_TIME_ZONE_CHANGE);
            registerReceiver(mTimeZoneReceiver, filter);
            CommonUtil.wtfi(TAG, "doTimeZoneChange...");
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getMessage());
        }
    }

    @Override
    public void unregisterTimeZoneChange() {
        try {
            unregisterReceiver(mTimeZoneReceiver);
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getMessage());
        }
    }

    @Override
    public void pauseReminder() {
        if (CommonUtil.getInstance().getPrefContent(CommonUtil.REMINDER_SET) != null
                && CommonUtil.getInstance().getPrefContent(CommonUtil.REMINDER_SET).equals("true")) {
            CommonUtil.getInstance().cancelAppReminder(true, this, findViewById(R.id.sw_m065_switch));
        }
    }

    @Override
    public void onDestroy() {
        unregisterTimeZoneChange();
        getStorage().setFlag("NO");
        getStorage().clearHomeCallBack();
        CommonUtil.getInstance().clearPrefContent(M025SubDoseLogFrg.KEY_FIRST_TIME_OPEN_APP);
        CommonUtil.getInstance().clearPrefContent(KEY_SYNC_READY);
        CommonUtil.getInstance().clearPrefContent(KEY_SAVE_CALLING_AS5);
        stopService(new Intent(this, SyncScheduleService.class));

        getStorage().setFlagUsedTouchID(false);
        super.onDestroy();
    }

    @Override
    public void showM001LandingScreen(String err) {
        CommonUtil.wtfd(TAG, "M001LandingPageFrg3");
        showFrgScreen(TAG, M001LandingPageFrg.TAG);
    }

    private void requestPermission() {
        CommonUtil.wtfd(TAG, "requestPermission...");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
            } else {
                getStorage().clearAllData();
            }
        } else {
            getStorage().clearAllData();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        CommonUtil.wtfd(TAG, "onRequestPermissionsResult...");
        for (int i = 0; i < permissions.length; i++) {
            String permission = permissions[i];
            int grantResult = grantResults[i];

            if (permission.equals(Manifest.permission.BLUETOOTH_ADMIN)
                    || permission.equals(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    getStorage().clearAllData();
                    prepareConfigSet();
                } else {
                    closeApp();
                }
            }
        }
    }

    private void prepareConfigSet() {
        CommonUtil.wtfi(TAG, "prepareConfigSet...");
        if (CommonUtil.getInstance().isConnectToNetwork()) {
            mPresenter.getConfigSet();
        } else {
            String time = CommonUtil.getInstance().getPrefContent(KEY_SAVE_TIME_OFFLINE);
            if (time == null || time.isEmpty()) {
                CommonUtil.wtfd(TAG, "M001LandingPageFrg4");
                showFrgScreen(TAG, M001LandingPageFrg.TAG);
                return;
            }

            long afterTime = CommonUtil.getDateAfter(Long.parseLong(time), 30);
            if (afterTime < System.currentTimeMillis()) {
                String message = LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_err_offline_section_expired));
                CommonUtil.wtfi(TAG, "prepareConfigSet...");
                forceLogOut(message);
                return;
            }

            try {
                RTokenEntity data = CADBManager.getInstance().getRTokenEntity();
                if (data == null || data.getConfigSet() == null || data.getConfigSet().isEmpty()) {
                    CommonUtil.wtfd(TAG, "M001LandingPageFrg6");
                    showFrgScreen(TAG, M001LandingPageFrg.TAG);
                    return;
                }

                mPresenter.prepareConfigSet(data.getConfigSet());
            } catch (Exception e) {
                CommonUtil.wtfe(TAG, e.getMessage());
                CommonUtil.wtfd(TAG, "M001LandingPageFrg7");
                showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }
    }

    /**
     * <b>isLoginSectionValid</b><br/>
     * <br/>This method will validate if the login section of the user are still valid or meet the expire date
     * @return True if "KeepMeLogin" is still working.<br/>False if exceed maximum days(refer the days get from the config set)
     */
    private boolean isLoginSectionValid() {
        String keep = CommonUtil.getInstance().getPrefContent(CommonUtil.KEEP);
        String start = CommonUtil.getInstance().getPrefContent(CommonUtil.LOGIN_SECTION_START_DATE);
        if (keep == null
                || !keep.equals(CommonUtil.KEEP)
                || start == null
                || start.equals(CommonUtil.LOGIN_SECTION_DO_NOT_KEEP)) {
            return true;
        } else {
            String today = CommonUtil.getDateNow(CommonUtil.DATE_NOW_DY);
            String endDate = CommonUtil.getDateNow(CommonUtil.DATE_NOW_DY, start, getStorage().getM001ConfigSet().getMaximumSection() - 1);
            Date current = NVDateUtils.stringToDateLocal(today, CommonUtil.DATE_NOW_DY);
            Date end = NVDateUtils.stringToDateLocal(endDate, CommonUtil.DATE_NOW_DY);
            return current.compareTo(end) < 0;
        }
    }

    @Override
    public void onStart() {
        CommonUtil.wtfi(TAG, "onStart...");
        getStorage().setAppActive(true);
        super.onStart();

        if (getStorage().isFirstStart()) {
            getStorage().setM001ConfigSet(null);
        }

        getStorage().addHomeCallBack(TAG, this::doHomeAction);
        getStorage().setLoginSessionExpire(data -> validateLoginSection());
        getStorage().setCredentialLost(data -> onCredentialLost());

        prepareConfigSet();
    }

    private void doHomeAction(Object data) {
        if (data == null) {
            return;
        }

        switch ((String) data) {
            case NetworkChangeReceiver.ACTION_NETWORK_RESTORED:
                if (currentTag != null) {
                    CommonUtil.getInstance().showDialog(HomeActivity.this, R.string.txt_available_network, null);
                }
                reloadConfigSet();
                break;
            case NetworkChangeReceiver.ACTION_NETWORK_LOST:
                doHomeActionNetworkLost();
                break;
            default:
                if (((String) data).startsWith(NetworkChangeReceiver.ACTION_SHOW_LANDING_ACTIVE_KEY)) {
                    String key = ((String) data).replace(NetworkChangeReceiver.ACTION_SHOW_LANDING_ACTIVE_KEY, "");
                    try {
                        checkBlockApp(Integer.parseInt(key));
                    } catch (Exception e) {
                        CommonUtil.wtfe(TAG, e.getMessage());
                        CommonUtil.wtfd(TAG, "M001LandingPageFrg8");
                        showFrgScreen(TAG, M001LandingPageFrg.TAG);
                    }
                } else if (((String) data).startsWith(BasePresenter.KEY_TOKEN_EXPIRED)) {
                    String key = ((String) data).replace(BasePresenter.KEY_TOKEN_EXPIRED, "");
                    getStorage().saveStateTokenExpired(true);
                    forceLogOut(key);
                }
                break;
        }
    }

    @Override
    public void reloadConfigSet() {
        mPresenter.getConfigSet();
    }

    private void doHomeActionNetworkLost() {
        BaseFragment frg = mFrags.get(currentTag);
        if (frg instanceof M001LandingPageFrg) {
            ((M001LandingPageFrg) frg).checkBlockApp();
            if (frg.isVisible()) {
                showNetworkAlert();
            }
            return;
        }

        if (currentTag != null && !(mFrags.get(currentTag) instanceof M023LoginFrg)) {
            CommonUtil.getInstance().showDialog(HomeActivity.this, R.string.txt_lost_network, null);
        }
        CommonUtil.getInstance().clearPrefContent(NetworkChangeReceiver.KEY_SAVE_FLAG_OFFLINE);
    }


    @Override
    public void onStop() {
        if (mAlertDialog != null) {
            mAlertDialog.dismiss();
        }
        super.onStop();
    }

    private void hideSystemUI() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    protected void initData() {
        CommonUtil.wtfd(TAG, "innitData...");
    }

    @Override
    protected HomePresenter getPresenter() {
        return new HomePresenter(this);
    }

    @Override
    public void backToPreviousScreen() {
        super.onBackPressed();
    }

    @SuppressLint("ResourceType")
    @Override
    public void showFrgScreen(String tagSource, String tagChild) {
        CommonUtil.wtfd(TAG, "showChildFrgScreen...");

        BaseFragment frg = mFrags.get(tagChild);
        try {
            Class<?> clazz = Class.forName(tagChild);
            Constructor<?> constructor = clazz.getConstructor();
            frg = (BaseFragment) constructor.newInstance();
            frg.setOnCallBack(this);

            mFrags.put(tagChild, frg);
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getMessage());
        }
        assert frg != null;

        frg.setTagSource(tagSource);
        try {
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.animator.alpha_in, R.animator.alpha_out)
                    .replace(R.id.content, frg).commit();
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        CommonUtil.wtfi(TAG, "Current fragment is : " + currentTag);
        if (currentTag == null) {
            super.onBackPressed();
            return;
        }
        BaseFragment frg = mFrags.get(currentTag);
        if (frg == null) {
            super.onBackPressed();
            return;
        }
        frg.backToPreviousScreen();
    }

    @Override
    public void closeApp() {
        finish();
    }

    @Override
    public void hideBottomBar() {
        hideSystemUI();
    }


    @Override
    public void loadConfigFail() {
        getStorage().setAppActive(false);

        LangMgr.getInstance().configLanguage(CommonUtil.DEFAULT_LANG);
        CommonUtil.getInstance().savePrefContent(CommonUtil.LANG_KEY, CommonUtil.DEFAULT_LANG.getKey());
        CommonUtil.getInstance().savePrefContent(CommonUtil.LANG_VALUE, CommonUtil.DEFAULT_LANG.getValue());
        CommonUtil.wtfd(TAG, "M001LandingPageFrg9");
        showFrgScreen(TAG, M001LandingPageFrg.TAG);
    }

    @Override
    public void loadConfigSuccess(ConfigEntity configEntity) {
        getStorage().setFirstStart(false);
        getStorage().setM001ConfigSet(configEntity);

        String langKey = CommonUtil.getInstance().getPrefContent(CommonUtil.LANG_KEY);
        String langValue = CommonUtil.getInstance().getPrefContent(CommonUtil.LANG_VALUE);

        if (langKey == null) {
            CommonUtil.wtfi(TAG, "loadConfigSuccess... load default language...");
            LangMgr.getInstance().configLanguage(null);
        } else {
            CommonUtil.wtfi(TAG, "loadConfigSuccess... load language..." + langKey + ":" + langValue);
            LangMgr.getInstance().configLanguage(new TextEntity(langKey, langValue));
        }

        configEntity.loadConfigSet();

        if (CommonUtil.getInstance().getPrefContent(M024DoseLogFrg.KEY_SAVE_LOGGED_IN) != null
                && CommonUtil.getInstance().getPrefContent(M024DoseLogFrg.KEY_SAVE_LOGGED_IN).equals(Boolean.TRUE + "")) {
            detectAdultUser();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //This feature isn't require here
    }

    @Override
    public void dataLanguageReady(String key) {
        LangMgr.getInstance().dataLanguageReady(key);
        isNewLanguage = true;
        prepareDataExternal(key);
    }

    @Override
    public void filterLanguage() {
        mPresenter.filterLanguage();
    }

    @Override
    public void configLanguageDone() {
        if (!isNewLanguage) {
            ProgressLoading.dismiss();
            if (getStorage().getM002CallBack() != null) {
                getStorage().getM002CallBack().updateView();
            }
        }
        mPresenter.checkBlockApp();
    }

    @Override
    public void filterLanguageNoConnection() {
        CommonUtil.getInstance().showDialog(this, R.string.txt_this_language_wrong, null);
        if (getStorage().getM002CallBack() != null) {
            getStorage().getM002CallBack().revertChoice();
        }
    }

    /**
     * checkAppVersion<br/>
     * <br/>Check version of the app:
     * <br/>If it is not newest version, the update dialog will be shown
     */
    private void checkAppVersion() {
        mPresenter.loadingLoginOff();
        String data = CommonUtil.getInstance().getPrefContent(NetworkChangeReceiver.KEY_SAVE_FLAG_OFFLINE);
        if (data != null && data.equals("true") && !CommonUtil.getInstance().isConnectToNetwork()) {
            CommonUtil.getInstance().clearPrefContent(NetworkChangeReceiver.KEY_SAVE_FLAG_OFFLINE);

            CommonUtil.getInstance().showDialog(
                    HomeActivity.this, R.string.txt_lost_network,
                    null);
        }

        if (getStorage().isAppActive()
                && !getStorage().getFlag().equals(ALREADY_VALUE)) {
            if (getStorage().getM001ConfigSet() != null
                    && getStorage().getM001ConfigSet().isNewVersion()) {
                CommonUtil.getInstance().showDialog(this,
                        CAApplication.getInstance().getString(R.string.txt_alert_new_version),
                        CAApplication.getInstance().getString(R.string.txt_content_new_version),
                        CAApplication.getInstance().getString(R.string.txt_alert_new_version_no),
                        CAApplication.getInstance().getString(R.string.txt_alert_new_version_yes),
                        new OnOKDialogCallBack() {
                            @Override
                            public void handleOKButton2() {
                                openCHApp();
                            }

                            @Override
                            public void handleOKButton1() {
                                checkMemoryLow();
                            }
                        });
            } else {
                checkMemoryLow();
            }
        }
    }

    private void checkMemoryLow() {
        if (getStorage().getM001ConfigSet() != null
                && getStorage().getM001ConfigSet().isNotEnoughSpace()) {
            CommonUtil.getInstance().showDialog(this,
                    CAApplication.getInstance().getString(R.string.txt_alert_phone_memory_low_title),
                    CAApplication.getInstance().getString(R.string.txt_alert_phone_memory_low_content),
                    CAApplication.getInstance().getString(R.string.txt_alert_phone_memory_low_ok), null, new OnOKDialogAdapter() {
                        @Override
                        public void handleOKButton1() {
                            CommonUtil.getInstance().checkBluetoothOn(HomeActivity.this);
                        }
                    });
        } else {
            CommonUtil.getInstance().checkBluetoothOn(HomeActivity.this);
        }
    }

    private void openCHApp() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(CAApplication.getInstance().getString(R.string.link_app)));
        startActivity(intent);
    }

    @Override
    public void showWaringDialog(String text, boolean isActive) {
        //This feature isn't require here
    }

    @Override
    public void checkBlockApp(int isActive) {
        if (mAlertDialog != null && mAlertDialog.isShowing()) return;
        switch (isActive) {
            case CommonUtil.BLACK_LIST:
                getStorage().setAppActive(false);
                CommonUtil.getInstance().cancelAppReminder(false, this, null);
                String text = getString(R.string.txt_m001_block_all);
                mAlertDialog = CommonUtil.getInstance().showDialog(this, text, null);
                checkAppVersion();
                break;
            case CommonUtil.GREY_LIST:
                getStorage().setAppActive(true);
                String flagAlreadyCheck = getStorage().getFlag();
                if (flagAlreadyCheck == null || !flagAlreadyCheck.equals(ALREADY_VALUE)) {
                    text = getString(R.string.txt_m001_allow_all);
                    mAlertDialog = CommonUtil.getInstance().showDialog(this, text, null);
                    checkAppVersion();
                    getStorage().setFlag(ALREADY_VALUE);
                }
                ProgressLoading.dismiss();
                break;
            case CommonUtil.WHITE_LIST:
                getStorage().setAppActive(true);
                if (currentTag == null) {
                    checkAppVersion();
                }
                ProgressLoading.dismiss();
                break;
            case CommonUtil.RECALL_VERSION:
                getStorage().setAppActive(false);
                CommonUtil.getInstance().cancelAppReminder(false, this, null);
                String versionApp = "";
                try {
                    PackageInfo pInfo = CAApplication.getInstance().getPackageManager().getPackageInfo(CAApplication.getInstance().getPackageName(), 0);
                    versionApp = pInfo.versionName;

                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                String textVersion = getString(R.string.txt_m001_recall_version).replace("${dialoq_version}", versionApp);
                mAlertDialog = CommonUtil.getInstance().showDialog(this, textVersion, new OnOKDialogAdapter() {
                    @Override
                    public void handleDialogDismiss() {
                        CommonUtil.wtfd(TAG, "M001LandingPageFrg10");
                        showFrgScreen(TAG, M001LandingPageFrg.TAG);
                    }
                });
                checkAppVersion();
                break;
            case CommonUtil.RECALL_COUNTRY_VERSION:
                getStorage().setAppActive(false);
                CommonUtil.getInstance().cancelAppReminder(false, this, null);
                String countryVersionApp = "";
                try {
                    PackageInfo pInfo = CAApplication.getInstance().getPackageManager().getPackageInfo(CAApplication.getInstance().getPackageName(), 0);
                    countryVersionApp = pInfo.versionName;

                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                String country = getStorage().getM007UserEntity().getCountry();
                String textCountryVersion = getString(R.string.txt_m023_recall_country_version).replace("${dialoq_version}", countryVersionApp).replace("${country_name}", country);

                CommonUtil.getInstance().showDialog(this, textCountryVersion, new OnOKDialogCallBack() {
                    @Override
                    public void handleOKButton1() {
                        checkAppVersion();
                    }

                    @Override
                    public void handleDialogDismiss() {
                        CommonUtil.wtfd(TAG, "M001LandingPageFrg11");
                        showFrgScreen(TAG, M001LandingPageFrg.TAG);
                    }
                });
                break;
            case CommonUtil.RECALL_COUNTRY:
                getStorage().setAppActive(false);
                CommonUtil.getInstance().cancelAppReminder(false, this, null);
                String recallCountry = getStorage().getM007UserEntity().getCountry();
                String textCountry = getString(R.string.txt_m023_recall_country).replace("${country_name}", recallCountry);
                CommonUtil.getInstance().showDialog(this, textCountry, new OnOKDialogCallBack() {
                    @Override
                    public void handleOKButton1() {
                        checkAppVersion();
                    }

                    @Override
                    public void handleDialogDismiss() {
                        CommonUtil.wtfd(TAG, "M001LandingPageFrg12");
                        showFrgScreen(TAG, M001LandingPageFrg.TAG);
                    }
                });
                break;
            default:
                break;
        }

        BaseFragment frg = mFrags.get(currentTag);
        if (frg instanceof M001LandingPageFrg) {
            ((M001LandingPageFrg) frg).checkBlockApp();
        }
    }

    @Override
    public void syncSuccess() {
        CommonUtil.wtfi(TAG, "syncSuccess....");
        mScanUtil.startScan();
        getStorage().callBackDoseCallBack(true);

        if (CommonUtil.getInstance().getPrefContent(KEY_SHOW_BG_SYNC) == null) {
            CommonUtil.getInstance().savePrefContent(KEY_SHOW_BG_SYNC, "true");
            CommonUtil.getInstance().showNotification(this,
                    CAApplication.getInstance().getString(R.string.txt_act_title_noti_new_doselog_syn),
                    CAApplication.getInstance().getString(R.string.txt_act_content_noti_new_doselog_syn));
        }

        if (mData != null
                && !mData.getDoseItems().isEmpty()
                && mData.getSystemFlag().equals(SYS_EOL_WARNING + "")) {
            CommonUtil.getInstance().showDialog(this,
                    LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m025_eol_warning_title)),
                    LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m025_eol_warning_message)),
                    LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m025_eol_warning_cancel)),
                    LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m025_eol_warning_support)),
                    null);
            getStorage().setSystemIdEOLWarning(mDeviceName);
        }
    }

    @Override
    public void syncFailed() {
        // Do nothing
    }

    @Override
    public void detectAdultUser() {
        if (mPresenter.checkForAdultUser()) {
            CommonUtil.getInstance().showDialog(this,
                    CAApplication.getInstance().getString(R.string.app_name),
                    CAApplication.getInstance().getString(R.string.txt_legal_age_content),
                    CAApplication.getInstance().getString(R.string.txt_legal_age_ok), null,
                    new OnOKDialogCallBack() {
                        @Override
                        public void handleOKButton1() {
                            CommonUtil.getInstance().savePrefContent(HomePresenter.KEY_ACCEPTED_ADULT_WARNING, "true");
                        }
                    });
        }
    }

    @Override
    public void getLanguageError() {
        if (getStorage().getM002CallBack() != null) {
            getStorage().getM002CallBack().revertChoice();
        }
    }

    @Override
    public void downloadIntroVideo(String url) {
        if (url != null) {
            CATask<HomeActivity, HomeActivity> taskDL = new CATask<>(new WeakReference<>(this), this);
            taskDL.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
        }
    }

    private void prepareDataExternal(String key) {
        //create new folder with key
        File fSupport = new File(CAApplication.getInstance().getExternalFilesDir(null) + SLASH + key);
        if (!fSupport.mkdirs()) {
            CommonUtil.wtfe(TAG, "Failed to create external data folder");
        }
        //Download video
        List<TopicEntity> videoData = getStorage().getM001ConfigSet().getListVideos();
        for (int i = 0; i < videoData.size(); i++) {
            for (int j = 0; j < videoData.get(i).getListQnA().size(); j++) {
                CATask<HomeActivity, HomeActivity> taskDL = new CATask<>(new WeakReference<>(this), this);
                String videoURL = LangMgr.getInstance().getLangList().get(videoData.get(i).getListQnA().get(j).getValue());
                taskDL.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, videoURL);
            }
        }
        //Download pdf
        CATask<HomeActivity, HomeActivity> taskDLPDF = new CATask<>(new WeakReference<>(this), this);
        if (mPresenter.getIFU() != null) {
            String pdfURL = mPresenter.getIFU().getKey();
            String fileName = pdfURL.substring(pdfURL.lastIndexOf(SLASH) + 1);
            String folderName = CommonUtil.getInstance().getPrefContent(CommonUtil.LANG_KEY);
            String path = CAApplication.getInstance().getExternalFilesDir(null) + SLASH + folderName + SLASH + fileName;
            CommonUtil.getInstance().savePrefContent(CommonUtil.KEY_IFU_URL, pdfURL);
            CommonUtil.getInstance().savePrefContent(CommonUtil.KEY_IFU_PATH, path);
            taskDLPDF.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, pdfURL);
        } else {
            CommonUtil.getInstance().showDialog(this, LangMgr.getInstance().getLangList().get(getResources().getString(R.string.lang_err_500)), null);
        }
        //Setup intro Video
        if (mPresenter.getIntroVideo() != null) {
            String introVideoURL = mPresenter.getIntroVideo().getKey();
            String fileNameIntroVideo = introVideoURL.substring(introVideoURL.lastIndexOf(SLASH) + 1);
            String folderNameIntroVideo = CommonUtil.getInstance().getPrefContent(CommonUtil.LANG_KEY);
            String pathIntroVideo = CAApplication.getInstance().getExternalFilesDir(null) + SLASH + folderNameIntroVideo + SLASH + fileNameIntroVideo;
            CommonUtil.getInstance().savePrefContent(CommonUtil.KEY_INTRO_URL, introVideoURL);
            CommonUtil.getInstance().savePrefContent(CommonUtil.KEY_INTRO_PATH, pathIntroVideo);
        } else {
            CommonUtil.getInstance().showDialog(this, LangMgr.getInstance().getLangList().get(getResources().getString(R.string.lang_err_500)), null);
        }
    }

    @Override
    public Object doBGTask(String key) {
        InputStream in = null;

        if (key == null) {
            return new Object();
        }

        String fileName = key.substring(key.lastIndexOf(SLASH) + 1);
        String folderName = CommonUtil.getInstance().getPrefContent(CommonUtil.LANG_KEY);
        String path = CAApplication.getInstance().getExternalFilesDir(null) + SLASH + folderName + SLASH + fileName;
        CommonUtil.wtfi(TAG, "PATH: downloadPath: " + path);
        File file = new File(path);

        try {
            if (!file.createNewFile()) {
                CommonUtil.wtfe(TAG, "Failed to create new file");
            }
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getMessage());
            return new Object();
        }

        try (FileOutputStream out = new FileOutputStream(file)) {
            String ex = key.substring(key.lastIndexOf(DOT) + 1);
            if (!ex.equalsIgnoreCase("MP4") && !ex.equalsIgnoreCase("PDF")) {
                return new Object();
            }

            int len;
            byte[] buff = new byte[1024];
            filesRemain++;

            URLConnection conn = new URL(key).openConnection();
            in = conn.getInputStream();
            len = in.read(buff);

            while (len > 0) {
                out.write(buff, 0, len);
                len = in.read(buff);
            }
            in.close();
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getMessage());
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                CommonUtil.wtfe(TAG, e.getMessage());
            }
        }
        return new Object();
    }

    @Override
    public void doneTask(Object data) {
        filesRemain--;
        if (filesRemain < 1) {
            CommonUtil.wtfi(TAG, "PATH: downloadPath: Completed");
            ProgressLoading.dismiss();
            if (getStorage().getM002CallBack() != null) {
                getStorage().getM002CallBack().updateView();
                isNewLanguage = false;
            }
        }
    }

    private void validateLoginSection() {
        if (!isLoginSectionValid()) {
            new Handler(Looper.getMainLooper()).post(() -> {
                String message = LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_err_online_section_expired));
                if (message != null) {
                    message = message.replace("${number_days}", getStorage().getM001ConfigSet().getMaximumSection() + "");
                }
                forceLogOut(message);
            });
        } else if (!CommonUtil.getInstance().isConnectToNetwork()) {
            long startOfflineDate = Long.parseLong(CommonUtil.getInstance().getPrefContent(KEY_SAVE_TIME_OFFLINE));
            long expireOfflineDate = CommonUtil.getDateAfter(startOfflineDate, 30);
            if (expireOfflineDate < System.currentTimeMillis()) {
                new Handler(Looper.getMainLooper()).post(() -> {
                    String message = LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_err_offline_section_expired));
                    forceLogOut(message);
                });
            }
        }
    }

    private void onCredentialLost() {
        String message = "Your account has been deleted or the password has been reset on other device!";
        forceLogOut(message);
    }
}
