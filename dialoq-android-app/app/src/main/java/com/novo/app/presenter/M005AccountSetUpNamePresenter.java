package com.novo.app.presenter;

import com.novo.app.view.event.OnM005AccountSetUpNameCallBack;

public class M005AccountSetUpNamePresenter extends BasePresenter<OnM005AccountSetUpNameCallBack> {
    public M005AccountSetUpNamePresenter(OnM005AccountSetUpNameCallBack event) {
        super(event);
    }
}
