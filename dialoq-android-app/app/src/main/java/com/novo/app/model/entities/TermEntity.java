package com.novo.app.model.entities;

import com.google.gson.annotations.SerializedName;
import com.novo.app.model.BaseModel;

public class TermEntity extends BaseModel {
    @SerializedName("termConditionId")
    private int termConditionId;
    @SerializedName("version")
    private int version;
    @SerializedName("content")
    private String content;
    @SerializedName("createdOn")
    private String createdOn;
    @SerializedName("updatedOn")
    private String updatedOn;
    @SerializedName("createdBy")
    private int createdBy;
    @SerializedName("updatedBy")
    private int updatedBy;
    @SerializedName("isLatest")
    private boolean isLatest;

    @SerializedName("userId")
    private int userId;
    @SerializedName("userTermConditionId")
    private int userTermConditionId;
    @SerializedName("termConditionVersion")
    private String termConditionVersion;
    @SerializedName("acceptDate")
    private String acceptDate;
    @SerializedName("userAgent")
    private String userAgent;
    @SerializedName("mobileVersion")
    private String mobileVersion;
    @SerializedName("mobileDeviceId")
    private String mobileDeviceId;

    public int getTermConditionId() {
        return termConditionId;
    }

    public void setTermConditionId(int termConditionId) {
        this.termConditionId = termConditionId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public int getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(int updatedBy) {
        this.updatedBy = updatedBy;
    }

    public boolean getIsLatest() {
        return isLatest;
    }

    public void setIsLatest(boolean isLatest) {
        this.isLatest = isLatest;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserTermConditionId() {
        return userTermConditionId;
    }

    public void setUserTermConditionId(int userTermConditionId) {
        this.userTermConditionId = userTermConditionId;
    }

    public String getTermConditionVersion() {
        return termConditionVersion;
    }

    public void setTermConditionVersion(String termConditionVersion) {
        this.termConditionVersion = termConditionVersion;
    }

    public String getAcceptDate() {
        return acceptDate;
    }

    public void setAcceptDate(String acceptDate) {
        this.acceptDate = acceptDate;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getMobileVersion() {
        return mobileVersion;
    }

    public void setMobileVersion(String mobileVersion) {
        this.mobileVersion = mobileVersion;
    }

    public String getMobileDeviceId() {
        return mobileDeviceId;
    }

    public void setMobileDeviceId(String mobileDeviceId) {
        this.mobileDeviceId = mobileDeviceId;
    }
}
