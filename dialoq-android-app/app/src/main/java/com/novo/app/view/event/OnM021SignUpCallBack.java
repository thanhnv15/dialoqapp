package com.novo.app.view.event;

public interface OnM021SignUpCallBack extends OnCallBackToView {

    void showM022SignUpSuccess();
}
