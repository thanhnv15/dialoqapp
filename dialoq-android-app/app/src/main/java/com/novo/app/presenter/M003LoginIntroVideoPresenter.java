package com.novo.app.presenter;

import com.novo.app.view.event.OnM003LoginIntroVideoCallBack;

public class M003LoginIntroVideoPresenter extends BasePresenter<OnM003LoginIntroVideoCallBack> {
    public M003LoginIntroVideoPresenter(OnM003LoginIntroVideoCallBack event) {
        super(event);
    }
}
