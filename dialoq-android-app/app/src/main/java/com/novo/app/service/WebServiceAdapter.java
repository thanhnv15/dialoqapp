/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.service;

import android.os.Message;

import com.novo.app.utils.CommonUtil;
import com.novo.app.utils.MyHandler;
import com.novo.app.view.event.OnHandleMessage;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

public class WebServiceAdapter implements WebServiceUtil.OnExecuteListener, OnHandleMessage {
    private static final String TAG = WebServiceAdapter.class.getName();
    private static final int ERROR_CASE = 105;
    private static final int SUCCESS_CASE = 106;
    private static final int SHOW_LOCK_DIALOG_CASE = 108;
    private static final int HIDE_LOCK_DIALOG_CASE = 109;
    private Map<String, OnRequestCallBackListener> mListener;
    private MyHandler<WebServiceAdapter, WebServiceAdapter> mHandler;

    public WebServiceAdapter() {
        this.mListener = new HashMap<>();
        mHandler = new MyHandler<>(new WeakReference<>(this), this);
    }

    @Override
    public void doMessage(Message msg) {
        CommonUtil.wtfi(TAG, "doMessage-msg.what = " + msg.what);
        if (mListener == null) {
            CommonUtil.wtfe(TAG, "ERROR: mListener is NULL");
            return;
        }

        Object[] data = (Object[]) msg.obj;
        String key = (String) data[0];
        switch (msg.what) {
            case ERROR_CASE:
                String tag = (String) data[2];
                mListener.get(key).doFailed(tag, (Exception) data[1], -1, "");
                break;
            case SUCCESS_CASE:
                Response response = (Response) data[1];
                tag = (String) data[2];
                String requestId = (String) data[3];
                String token = (String) data[4];
                mListener.get(key).doSuccess(tag, response, requestId, token);
                break;
            case SHOW_LOCK_DIALOG_CASE:
                mListener.get(key).showLockDialog();
                break;
            case HIDE_LOCK_DIALOG_CASE:
                tag = (String) data[1];
                mListener.get(key).hideLockDialog(tag);
                break;
            default:
                break;
        }
    }

    @Override
    public void execSuccess(Object data) {
        CommonUtil.wtfi(TAG, "execSuccess: ");
        Message msg = new Message();
        msg.what = SUCCESS_CASE;
        msg.obj = data;
        if (!CommonUtil.getInstance().isUnitTest()) {
            msg.setTarget(mHandler);
            msg.sendToTarget();
        } else {
            doMessage(msg);
        }
    }

    @Override
    public void execFailed(Object data) {
        CommonUtil.wtfi(TAG, "execFailed: " + data);

        Message msg = new Message();
        msg.what = ERROR_CASE;
        msg.obj = data;
        msg.setTarget(mHandler);
        msg.sendToTarget();
    }

    @Override
    public void showLockDialog(Object data) {
        CommonUtil.wtfd(TAG, "showLockDialog");

        Message msg = new Message();
        msg.what = SHOW_LOCK_DIALOG_CASE;
        msg.setTarget(mHandler);
        msg.obj = data;
        msg.sendToTarget();
    }

    @Override
    public void hideLockDialog(Object data) {
        CommonUtil.wtfd(TAG, "hideLockDialog");

        Message msg = new Message();
        msg.what = HIDE_LOCK_DIALOG_CASE;
        msg.setTarget(mHandler);
        msg.obj = data;
        msg.sendToTarget();
    }

    public void put(String key, OnRequestCallBackListener event) {
        mListener.put(key, event);
    }

    /**
     * Prototype callbacks for starting request, receiving and handling responses from server
     */
    public interface OnRequestCallBackListener extends Serializable {

        /**
         * Request sent to server got failed
         * @param tag string tag of sent request
         * @param obj exception
         * @param i request code
         * @param s error response data from server
         */
        void doFailed(String tag, Exception obj, int i, String s);

        /**
         * Request sent to server got success and response received
         * @param tag string tag of sent request
         * @param data response data from server
         * @param requestId request id
         * @param token token of sent request
         */
        void doSuccess(String tag, Response data, String requestId, String token);

        /**
         * This will be called when a request start to be sent to server
         */
        void showLockDialog();

        /**
         * This will be called when response from server received or error occured
         * @param key tag for the dialog
         */
        void hideLockDialog(String key);
    }
}
