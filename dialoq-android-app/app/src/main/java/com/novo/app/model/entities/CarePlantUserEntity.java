package com.novo.app.model.entities;

import com.google.gson.annotations.SerializedName;
import com.novo.app.model.BaseModel;

import java.io.Serializable;
import java.util.List;

public class CarePlantUserEntity extends BaseModel {
    @SerializedName("totalCount")
    private int totalCount;
    @SerializedName("data")
    private List<DataInfo> data;
    @SerializedName("paging")
    private PageInfo pageInfo;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<DataInfo> getData() {
        return data;
    }

    public void setData(List<DataInfo> data) {
        this.data = data;
    }

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public class DataInfo implements Serializable {

        @SerializedName("careplanId")
        private String careplanId;
        @SerializedName("name")
        private String name;
        @SerializedName("description")
        private String description;
        @SerializedName("assessments")
        private List<AssessmentInfo> assessments;

        public String getCareplanId() {
            return careplanId;
        }

        public void setCareplanId(String careplanId) {
            this.careplanId = careplanId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<AssessmentInfo> getAssessments() {
            return assessments;
        }

        public void setAssessments(List<AssessmentInfo> assessments) {
            this.assessments = assessments;
        }
    }

    public class AssessmentInfo implements Serializable {
        @SerializedName("assessmentId")
        private String assessmentId;
        @SerializedName("assessmentTemplateId")
        private int assessmentTemplateId;
        @SerializedName("name")
        private String name;
        @SerializedName("description")
        private String description;
        @SerializedName("type")
        private String type;
        @SerializedName("instructions")
        private String instructions;
        @SerializedName("unitOfMeasure")
        private String unitOfMeasure;
        @SerializedName("windowTime")
        private String windowTime;

        public String getAssessmentId() {
            return assessmentId;
        }

        public void setAssessmentId(String assessmentId) {
            this.assessmentId = assessmentId;
        }

        public int getAssessmentTemplateId() {
            return assessmentTemplateId;
        }

        public void setAssessmentTemplateId(int assessmentTemplateId) {
            this.assessmentTemplateId = assessmentTemplateId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getInstructions() {
            return instructions;
        }

        public void setInstructions(String instructions) {
            this.instructions = instructions;
        }

        public String getUnitOfMeasure() {
            return unitOfMeasure;
        }

        public void setUnitOfMeasure(String unitOfMeasure) {
            this.unitOfMeasure = unitOfMeasure;
        }

        public String getWindowTime() {
            return windowTime;
        }

        public void setWindowTime(String windowTime) {
            this.windowTime = windowTime;
        }
    }

    public class PageInfo implements Serializable {
        @SerializedName("first")
        private String first;
        @SerializedName("previous")
        private String previous;
        @SerializedName("next")
        private String next;
        @SerializedName("last")
        private String last;

        public String getFirst() {
            return first;
        }

        public void setFirst(String first) {
            this.first = first;
        }

        public String getPrevious() {
            return previous;
        }

        public void setPrevious(String previous) {
            this.previous = previous;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public String getLast() {
            return last;
        }

        public void setLast(String last) {
            this.last = last;
        }
    }
}
