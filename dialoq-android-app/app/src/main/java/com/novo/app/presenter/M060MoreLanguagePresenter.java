package com.novo.app.presenter;

import com.novo.app.view.event.OnM060MoreLanguageCallBack;

public class M060MoreLanguagePresenter extends BasePresenter<OnM060MoreLanguageCallBack> {
    public M060MoreLanguagePresenter(OnM060MoreLanguageCallBack event) {
        super(event);
    }
}
