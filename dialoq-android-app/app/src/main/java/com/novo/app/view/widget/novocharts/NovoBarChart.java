package com.novo.app.view.widget.novocharts;

import android.content.Context;
import android.util.AttributeSet;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.model.constants.DrugColorType;
import com.novo.app.model.entities.DailyDetailEntity;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.DateEntity;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.DoseLogAdapter;
import com.novo.app.view.widget.novocharts.formatter.NovoBarChartYValueFormatter;
import com.novo.app.view.widget.novocharts.formatter.NovoXValueFormatter;
import com.novo.app.view.widget.novocharts.listeners.NovoItemSelectedListener;
import com.novo.app.view.widget.novocharts.renderers.NovoBarChartRenderer;
import com.novo.app.view.widget.novocharts.renderers.NovoChartXAxisRenderer;
import com.novo.app.view.widget.novocharts.utils.NovoChartUtils;

import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NovoBarChart extends BarChart implements OnChartValueSelectedListener {

    private int mColorType;
    private NovoItemSelectedListener novoItemSelectedListener;
    private boolean doHaveNote;

    public NovoBarChart(Context context) {
        super(context);
    }

    public NovoBarChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NovoBarChart(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setNovoItemSelectedListener(NovoItemSelectedListener novoItemSelectedListener) {
        this.novoItemSelectedListener = novoItemSelectedListener;
    }


    @Override
    protected void init() {
        super.init();
        mRenderer = new NovoBarChartRenderer(this, mAnimator, mViewPortHandler);
        setOnChartValueSelectedListener(this);

    }

    private void setupChart(int totalUnit) {
        if (totalUnit > 0) {
            setHighlightPerDragEnabled(false);
            getXAxis().setDrawGridLines(false);
            getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

            getAxisLeft().setDrawGridLines(false);
            getAxisLeft().setAxisMinimum(0);
            getAxisLeft().setAxisMaximum(totalUnit);
            getAxisLeft().setDrawAxisLine(false);
            getAxisLeft().setLabelCount(7, totalUnit != 0);
            getAxisLeft().setValueFormatter(new NovoBarChartYValueFormatter(totalUnit));
            getAxisRight().setEnabled(false);

            fitScreen();
            setDrawGridBackground(false);
            setHighlightFullBarEnabled(true);
            getLegend().setEnabled(false);
            getDescription().setText("");
            setPinchZoom(false);
            setScaleEnabled(false);
//            setExtraBottomOffset(20);
        } else {
            getAxisLeft().setLabelCount(6);
            getAxisLeft().setAxisMaximum(12);
            setHighlightPerDragEnabled(false);
            getXAxis().setDrawGridLines(false);
            getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

            getAxisLeft().setDrawGridLines(false);
            getAxisLeft().setDrawAxisLine(false);
            getAxisLeft().setDrawLabels(false);
            getAxisRight().setEnabled(false);

            fitScreen();
            setDrawGridBackground(false);
            setHighlightFullBarEnabled(true);
            getLegend().setEnabled(false);
            getDescription().setText("");
            setPinchZoom(false);
            setScaleEnabled(false);
//            setExtraBottomOffset(20);
        }
        if (doHaveNote) setXAxisRenderer(new NovoChartXAxisRenderer(getViewPortHandler(), getXAxis(), getTransformer(YAxis.AxisDependency.LEFT)));
    }

    public List<DateEntity> setData(Date startDate, Date endDate,
                                    ArrayList<DataInfo> mData,
                                    String type, @DrugColorType String color) {
        List<DateEntity> entities = new ArrayList<>();
        List<DailyDetailEntity> listDay = new ArrayList<>();

        mColorType = ColorTemplate.rgb(color);
        if (getData() != null) {
            clearValues();
            clearAllViewportJobs();
            clear();
        }

        int mCount = NovoChartUtils.getDaysCountBetweenTwoDate(startDate, endDate);

        ArrayList<BarEntry> values = new ArrayList<>();
        int maxUnit = 0;

        for (int i = 0; i < mCount; i++) {
            String date = CommonUtil.getDateNow(CommonUtil.DATE_NOW_DY, -mCount + i + 1);
            List<Float> listValues = new ArrayList<>();
            List<DataInfo> dailyNoteList = new ArrayList<>();
            List<DataInfo> dailyDoseList = new ArrayList<>();
            List<DataInfo> dailySuccessList = new ArrayList<>();
            DailyDetailEntity dailyEntity = new DailyDetailEntity();
            DateEntity dateEntity = new DateEntity();
            dailyEntity.setDate(date);
            dateEntity.setDailyDetail(dailyEntity);

            boolean isHeader = false;
            boolean isNote = false;
            boolean isUnknownDrug = false;
            boolean isUnknownFiaspSize = false;
            boolean isUnknownTresibaSize = false;
            int totalUnit = 0;


            for (int j = 0; j < mData.size(); j++) {
                DataInfo item = mData.get(j);
                CommonUtil.wtfi("NovoBarChart", "Date: " + date);
                dailyEntity.setDate(date);

                assert date != null;
                if (item.getResult().getAssessmentType().equals(type)
                        && item.getResult().getReportedDate().contains(date)) {
                    boolean isUnsupport = false;

                    switch (item.getResult().getCustom3()) {
                        case DeviceEntity.CODE_TRESIBA_100 + "":
                            dailyEntity.setDrugName(DeviceEntity.TRESIBA);
                            dailyEntity.setColorType(CommonUtil.COLOR_TYPE_TRESIBA);
                            break;
                        case DeviceEntity.CODE_TRESIBA_200 + "":
                            dailyEntity.setDrugName(DeviceEntity.TRESIBA);
                            dailyEntity.setColorType(CommonUtil.COLOR_TYPE_TRESIBA_200);
                            break;
                        case DeviceEntity.CODE_FIASP + "":
                            dailyEntity.setDrugName(DeviceEntity.FIASP);
                            dailyEntity.setColorType(CommonUtil.COLOR_TYPE_FIASP);
                            break;
                        default:
                            break;
                    }

                    if (!CommonUtil.isUnknownDoseSize(item.getResult().getResult(), item.getResult().getCustom4())
                            && !CommonUtil.isUnknownDrug(item.getResult().getCustom3(), item.getResult().getCustom4())) {
                        if (((item.getResult().getCustom3().equals(DeviceEntity.CODE_TRESIBA_100 + "")
                                || item.getResult().getCustom3().equals(DeviceEntity.CODE_TRESIBA_200 + ""))
                                && color.equals(DrugColorType.DEFAULT_COLOR_TYPE_TRESIBA))
                                || ((item.getResult().getCustom3().equals(DeviceEntity.CODE_FIASP + "")
                                && color.equals(DrugColorType.DEFAULT_COLOR_TYPE_FIASP)))) {

                            totalUnit += item.getResult().getResult();
                            listValues.add((float) item.getResult().getResult());
                            dailySuccessList.add(item);
                            if (item.getResult().getResult() > 0) {
                                isHeader = true;
                            }

                        }
                    }
                    if (CommonUtil.isUnknownDrug(item.getResult().getCustom3(), item.getResult().getCustom4())) {
                        if (item.getResult().getCustom3().equals(DeviceEntity.CODE_FIASP + "")
                                || item.getResult().getCustom3().equals(DeviceEntity.CODE_TRESIBA_100 + "")
                                || item.getResult().getCustom3().equals(DeviceEntity.CODE_TRESIBA_200 + "")) {
                            isUnknownDrug = true;
                        } else {
                            isUnsupport = true;
                        }
                    }
                    if (CommonUtil.isUnknownDoseSize(item.getResult().getResult(), item.getResult().getCustom4())) {
                        if (item.getResult().getCustom3().equals(DeviceEntity.CODE_FIASP + "")) {
                            isUnknownFiaspSize = true;
                        } else {
                            isUnknownTresibaSize = true;
                        }
                    }
                    if (!isUnsupport) dailyDoseList.add(item);
                } else if (item.getResult().getAssessmentType().equals(DoseLogAdapter.TYPE_ASSESSMENT_NOTE)
                        && item.getResult().getReportedDate().contains(date)) {
                    isNote = true;
                    doHaveNote = true;
                    dailyNoteList.add(item);
                }

                dailyEntity.setColorType(mColorType);
                dailyEntity.setPosition(i);
                dailyEntity.setDoseList(dailyDoseList);
                dailyEntity.setNoteList(dailyNoteList);
                dailyEntity.setSuccessList(dailySuccessList);
                dateEntity.setDailyDetail(dailyEntity);
            }

            if (maxUnit < totalUnit) {
                maxUnit = totalUnit;
            }

            dateEntity.setHeader(isHeader);
            dateEntity.setNote(isNote);
            dateEntity.setUnknownDrug(isUnknownDrug);
            dateEntity.setUnknownFiaspSize(isUnknownFiaspSize);
            dateEntity.setUnknownTresibaSize(isUnknownTresibaSize);
            dateEntity.setColor(dailyEntity.getColorType());
            dateEntity.setDate(Integer.parseInt(date.split("-")[2]));
            dateEntity.setStringDate(date);
            entities.add(dateEntity);

            float[] valueReal;
            if (listValues.isEmpty()) {
                // ensure drawable icon error in bar chart
                valueReal = new float[1];
            } else {
                valueReal = new float[listValues.size()];
                for (int j = 0; j < listValues.size(); j++) {
                    valueReal[j] = listValues.get(j);
                }
            }

            // in order to draw chart correctly as reported_date
            ArrayUtils.reverse(valueReal);

            listDay.add(dailyEntity);
            BarEntry barEntry;
            if (dateEntity.getColor() == CommonUtil.COLOR_TYPE_TRESIBA && dateEntity.isUnknownTresibaSize()
                    || dateEntity.getColor() == CommonUtil.COLOR_TYPE_TRESIBA_200 && dateEntity.isUnknownTresibaSize()
                    || dateEntity.getColor() == CommonUtil.COLOR_TYPE_FIASP && dateEntity.isUnknownFiaspSize()
                    || dateEntity.isUnknownDrug()) {
                barEntry = new BarEntry(i, valueReal, getContext().getDrawable(R.drawable.ic_m014_error));
            } else {
                barEntry = new BarEntry(i, valueReal, null);
            }
            barEntry.setData(dateEntity);
            values.add(barEntry);
        }

        switch (mCount) {
            case 7:
                CAApplication.getInstance().getStorageCommon().set7DayData(entities);
                break;
            case 14:
                CAApplication.getInstance().getStorageCommon().set14DayData(entities);
                break;
            case 28:
                CAApplication.getInstance().getStorageCommon().set28DayData(entities);
                break;
            default:
                break;
        }

        setupChart(cellValue(maxUnit));
        getXAxis().setValueFormatter(new NovoXValueFormatter(startDate, endDate, getContext().getDrawable(R.drawable.ic_note), entities));

        BarDataSet set1 = new BarDataSet(values, "");
        set1.setDrawIcons(true);
        List<Integer> colors = new ArrayList<>();
        if (DrugColorType.DEFAULT_COLOR_TYPE_FIASP.equalsIgnoreCase(color)) {
            set1.setColor(mColorType);
        } else {
            for (BarEntry barEntry : values) {
                DateEntity dateEntity = (DateEntity) barEntry.getData();
                if (dateEntity.getDailyDetail().getSuccessList().isEmpty()) {
                    // if a day has no dose log, we still must add a dummy color in able to show color chart correctly
                    colors.add(CommonUtil.COLOR_TYPE_TRESIBA);
                } else {
                    for (DataInfo dataInfo : dateEntity.getDailyDetail().getSuccessList()) {
                        if (String.valueOf(DeviceEntity.CODE_TRESIBA_200).equalsIgnoreCase(dataInfo.getResult().getCustom3())) {
                            colors.add(CommonUtil.COLOR_TYPE_TRESIBA_200);
                        } else {
                            colors.add(CommonUtil.COLOR_TYPE_TRESIBA);
                        }
                    }
                }
            }
            // ensure no crash app in case has no dose data
            if (colors.isEmpty()) {
                colors.add(getContext().getResources().getColor(R.color.colorTresiba100));
            }
            set1.setColors(colors);
        }

        set1.setDrawValues(false);
        set1.setHighLightAlpha(0);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        BarData data = new BarData(dataSets);

        if (mCount >= 14) {
            data.setBarWidth(0.5f);
        } else {
            data.setBarWidth(mCount / 28f);
        }

        setData(data);
        notifyDataSetChanged();

        //must be set after set data
        setVisibleXRangeMaximum(14);

        if (mCount > 14) {
            getXAxis().setLabelCount(14);
        } else {
            getXAxis().setLabelCount(mCount);
        }
        return entities;
    }

    private int cellValue(int totalUnit) {
        int result = 0;
        if (totalUnit % 5 == 0) {
            result = totalUnit;
        } else if (totalUnit % 5 != 0) {
            int lastDigit = totalUnit % 10;
            if (lastDigit > 5) {
                result = totalUnit + 10 - lastDigit;
            } else {
                result = totalUnit + 5 - lastDigit;
            }
        }
        return result + result / 5;
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (novoItemSelectedListener != null && e != null) {
            novoItemSelectedListener.onNovoItemSelected(mColorType, mData, e, h);
        }
    }

    @Override
    public void onNothingSelected() {
        //This feature isn't require here
    }
}
