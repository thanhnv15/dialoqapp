package com.novo.app.presenter;

import com.novo.app.view.event.OnM035ReminderDoneCallBack;

public class M035ReminderDonePresenter extends BasePresenter<OnM035ReminderDoneCallBack> {
    public M035ReminderDonePresenter(OnM035ReminderDoneCallBack event) {
        super(event);
    }
}
