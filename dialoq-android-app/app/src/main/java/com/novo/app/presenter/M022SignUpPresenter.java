package com.novo.app.presenter;

import com.novo.app.view.event.OnM022SignUpCallBack;

public class M022SignUpPresenter extends BasePresenter<OnM022SignUpCallBack> {
    public M022SignUpPresenter(OnM022SignUpCallBack event) {
        super(event);
    }
}
