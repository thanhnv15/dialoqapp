/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.presenter;

import android.util.Log;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM076FirstLoginCallBack;
import com.novo.app.view.widget.ProgressLoading;

/**
 * This is the class that handles all business logic for screen of guiding user to send verification
 * code to his/her email
 *
 * This is the linked `Presenter` for the `View` of {@link com.novo.app.view.fragment.M076FirstLoginCodeFrg}
 * in MVP pattern
 *
 * Communicate to `View` object will be delegated to an instance of {@link OnM076FirstLoginCallBack}
 * A delegation object must be provided along with every presenter object
 */
public class M076FirstLoginPresenter extends BasePresenter<OnM076FirstLoginCallBack> {
    private static final String KEY_API_VERIFY_EMAIL = "KEY_API_VERIFY_EMAIL";
    private static final String URL_API_IA04_RESET_PASSWORD_STEP_1 = "user-service/api/v1/reset-password?";
    private static final String BODY_IA04 = "{\"action\": \"initiateReset\",\"userName\": %s,\"delivery\": \"SMTP\"}";
    private static final String TAG = M012SignUpPresenter.class.getName();

    /**
     * Constructor
     * @param event delegation object must be provided
     */
    public M076FirstLoginPresenter(OnM076FirstLoginCallBack event) {
        super(event);
    }

    /**
     * Request server to send confirmation code to user's email
     */
    public void requestCodeIA04() {
        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_VERIFY_EMAIL);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addPathSegment(URL_API_IA04_RESET_PASSWORD_STEP_1);

        request.addHeaders(CommonUtil.P_API_KEY, CommonUtil.getInstance().getAPIKey());
        Log.d(TAG, "requestCodeIA04: " + getStorage().getM075UserLoginInfo().getKey());
        request.addBody(generateJson(BODY_IA04, getStorage().getM075UserLoginInfo().getKey()));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Handle success response of {@link #requestCodeIA04(}
     * @param data response data from server
     */
    @Override
    protected void handleSuccess(String data, String tag) {
        if (KEY_API_VERIFY_EMAIL.equals(tag)) {
            CommonUtil.wtfi(TAG, "DATA: " + data);
            ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
            if (responseEntity == null) {
                mListener.showAlertDialog(R.string.txt_token_err);
                mListener.showM023Login();
                return;
            }

            //For abnormal case due to server error
            mListener.verifyEmailSuccess(CommonUtil.SUCCESS_VALUE);
        }
    }

    /**
     * Handle error response of {@link #requestCodeIA04(}
     * @param data response data from server
     */
    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tagRequest);
        ProgressLoading.dismiss();

        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        if (responseEntity != null && tag.equals(KEY_API_VERIFY_EMAIL)) {
            String sms = responseEntity.getErrorEntity()[0].getMessage();
            String responseCode = Integer.toString(code);
            switch (responseCode) {
                case CommonUtil.ERROR_400:
                    mListener.verifyEmailFail(sms);
                    break;
                case CommonUtil.ERROR_401:
                case CommonUtil.ERROR_403:
                default:
                    showNotify(LangMgr.getInstance().getLangList().get(ERR_500));
                    break;
            }
        } else if (!CommonUtil.getInstance().isConnectToNetwork()) {
            showNotify(LangMgr.getInstance().getLangList().get(ERR_NO_CONNECTION));
        }
    }


}

