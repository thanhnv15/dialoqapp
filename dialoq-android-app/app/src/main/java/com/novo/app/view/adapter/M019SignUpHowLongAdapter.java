package com.novo.app.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.HowLongEntity;
import com.novo.app.view.event.OnM019ListHowLongCallBack;
import com.novo.app.view.fragment.M019SignUpHowLongFrg;

import java.util.List;

public class M019SignUpHowLongAdapter extends BaseRecycleAdapter<OnM019ListHowLongCallBack, HowLongEntity, M019SignUpHowLongAdapter.HowLongHolder> {
    private int lastCheckedPos = -1;


    public M019SignUpHowLongAdapter(Context mContext, List<HowLongEntity> mListData, OnM019ListHowLongCallBack mCallBack) {
        super(mContext, mListData, mCallBack);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_m002_bt;
    }

    @Override
    protected M019SignUpHowLongAdapter.HowLongHolder getViewHolder(int viewType, View itemView) {
        return new M019SignUpHowLongAdapter.HowLongHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        M019SignUpHowLongAdapter.HowLongHolder item = (M019SignUpHowLongAdapter.HowLongHolder) holder;
        HowLongEntity data = mListData.get(position);
        data.setSelected(lastCheckedPos == position);
        if (data.isSelected()) {
            item.ivChecked.setVisibility(View.VISIBLE);
            item.view.setBackground(mContext.getResources().getDrawable(R.drawable.bg_m025_fill_dark_blue));
        } else {
            item.ivChecked.setVisibility(View.GONE);
            item.view.setBackground(mContext.getResources().getDrawable(R.drawable.bg_m004_fill_bt));
        }
        item.tvDiabete.setText(LangMgr.getInstance().getLangList().get(data.getName()));
        item.tvDiabete.setTag(data);
    }

    public class HowLongHolder extends BaseHolder {
        TextView tvDiabete;
        ImageView ivChecked;
        View view;

        HowLongHolder(View itemView) {
            super(itemView);
            view = itemView;
        }

        @Override
        protected void onClickView(int idView) {
            switch (idView) {
                case R.id.tv_m002_lang:
                    lastCheckedPos = getAdapterPosition();
                    notifyDataSetChanged();
                    mCallBack.clickOnItem(tvDiabete.getText().toString());
                    break;
                default:
                    break;
            }
        }

        @Override
        protected void initView() {
            tvDiabete = findViewById(R.id.tv_m002_lang, this, CAApplication.getInstance().getBoldFont());
            ivChecked = findViewById(R.id.iv_m002_checked);
        }
    }
}
