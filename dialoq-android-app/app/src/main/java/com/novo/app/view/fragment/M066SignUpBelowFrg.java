package com.novo.app.view.fragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M066SignUpPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.dialog.M051MorePrivacyPolicyDialog;
import com.novo.app.view.dialog.M051MoreTermDialog;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM066SignUpCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

import java.util.ArrayList;
import java.util.List;

public class M066SignUpBelowFrg extends BaseFragment<M066SignUpPresenter, OnHomeBackToView> implements OnM066SignUpCallBack {

    public static final String TAG = M066SignUpBelowFrg.class.getName();
    private static final int LEVEL_OPEN = 1;
    private static final int LEVEL_CLOSE = 0;
    private M051MorePrivacyPolicyDialog morePrivacyDialog;
    private M051MoreTermDialog moreTermDialog;
    private TextView mTvAnswer;
    private ImageView mIvLevel;
    private String termAndCondition, privacyNotice;

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m066_logo);
        findViewById(R.id.iv_m066_back, this);
        mIvLevel = findViewById(R.id.iv_m066_faq_level, this);
        findViewById(R.id.tv_m066_account_setup, CAApplication.getInstance().getBoldFont());
        TextView mTvBelow = findViewById(R.id.tv_m066_below, CAApplication.getInstance().getRegularFont());
        String below = mTvBelow.getText().toString();
        below = below.replace("${age}", getStorage().getUserCountryEntity().getAge().getValue());
        mTvBelow.setText(below);

        findViewById(R.id.tv_m066_faq_question, this, CAApplication.getInstance().getBoldFont());
        mTvAnswer = findViewById(R.id.tv_m066_faq_answer, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.bt_m066_register, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m066_cancel, this, CAApplication.getInstance().getMediumFont());

        TextView mTvDetail = findViewById(R.id.tv_m066_detail, this, CAApplication.getInstance().getRegularFont());
        termAndCondition = getString(R.string.lang_m006_detail_link_1);
        termAndCondition = LangMgr.getInstance().getLangList().get(termAndCondition);
        privacyNotice = getString(R.string.lang_m006_detail_link_2);
        privacyNotice =  LangMgr.getInstance().getLangList().get(privacyNotice);
        List<String> highlight = new ArrayList<>();
        highlight.add(termAndCondition);
        highlight.add(privacyNotice);
        setClickableText(mTvDetail, highlight, R.color.colorM020Teal);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m066_account_set_up_below;
    }

    @Override
    protected M066SignUpPresenter getPresenter() {
        return new M066SignUpPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {

    }

    @Override
    public void backToPreviousScreen() {
    }

    @Override
    public void onLinkClicked() {
        if (getStorage().getLinkTag() != null && getStorage().getLinkTag().equals(termAndCondition)) {
            if (moreTermDialog == null) {
                moreTermDialog = new M051MoreTermDialog(mContext, true);
            }
            moreTermDialog.show();
        }
        if (getStorage().getLinkTag() != null && getStorage().getLinkTag().equals(privacyNotice)) {
            if (morePrivacyDialog == null) {
                morePrivacyDialog = new M051MorePrivacyPolicyDialog(mContext, true);
            }
            morePrivacyDialog.show();
        }
        getStorage().setLinkTag(null);
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m066_detail:
                onLinkClicked();
                break;
            case R.id.iv_m066_faq_level:
            case R.id.tv_m066_faq_question:
                if (mTvAnswer.getVisibility() == View.GONE) {
                    mIvLevel.setImageLevel(LEVEL_OPEN);
                    mTvAnswer.setVisibility(View.VISIBLE);
                } else {
                    mIvLevel.setImageLevel(LEVEL_CLOSE);
                    mTvAnswer.setVisibility(View.GONE);
                }
                break;
            case R.id.iv_m066_back:
                mCallBack.showFrgScreen(TAG, M017ProfileBornFrg.TAG);
                break;
            case R.id.bt_m066_register:
                mCallBack.showFrgScreen(TAG, M011SignUpDataFrg.TAG);
                break;
            case R.id.tv_m066_cancel:
                mCallBack.registrationCancel(TAG);
                break;
            default:
                break;
        }
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);

    }

}
