package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.ResultEntity;
import com.novo.app.model.entities.ResultInfo;
import com.novo.app.model.entities.ShareDataEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.DoseLogAdapter;
import com.novo.app.view.event.OnM068ShareCallBack;
import com.novo.app.view.widget.NVDateFormat;
import com.novo.app.view.widget.NVDateUtils;
import com.novo.app.view.widget.ProgressLoading;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class M068SharePresenter extends BasePresenter<OnM068ShareCallBack> {
    private static final int KEY_LAST_7_DAYS = 0;
    private static final int KEY_LAST_14_DAYS = 1;
    private static final int KEY_LAST_28_DAYS = 2;

    private static final String KEY_API_AS05_GET_RESULT = "KEY_API_AS05_GET_RESULT";
    private static final String URL_API_AS05_GET_RESULT = CommonUtil.HOT_URL + "/api/v1/assessments/%s/results/%s/%s?";
    private static final String KEY_API_SYNC_RESULT = "KEY_API_SYNC_RESULT";
    private static final String P_SIZE = "size";
    private static final String P_SORT = "sort";
    private static final String TAG = M068SharePresenter.class.getName();
    private final ArrayList<DataInfo> mListDataInfo = new ArrayList<>();
    private int mIndex = 0;
    private String mTime1;
    private String mTime2;
    private List<String> listDate = new ArrayList<>();
    private boolean isSharingNote;
    private String mStartDate;
    private String mEndDate;

    public M068SharePresenter(OnM068ShareCallBack event) {
        super(event);
    }

    @Override
    protected void handleSuccess(String data, String tag) {
        CommonUtil.wtfi(TAG, "DATA: " + data);
        if (KEY_API_AS05_GET_RESULT.equals(tag)) {
            prepareAS05(data);
        }
    }

    void doAllAS05GetResult() {
        mIndex = 0;
        for (int i = 0; i < getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments().size(); i++) {
            callAS05GetResult(i);
        }
    }

    void prepareAS05(String data) {
        CommonUtil.wtfi(TAG, "prepareAS05..." + data);
        mIndex++;
        ResultEntity entity = generateData(ResultEntity.class, data);

        if (entity == null) {
            mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(ERR_500));
            return;
        }
        mListDataInfo.addAll(Arrays.asList(entity.getEvents().getData()));
        if (mIndex == getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments().size()) {
            initDataAS05();
        }
    }

    public void initCondition(int typeDate, boolean shareNote) {
        int numDays = 0;
        switch (typeDate) {
            case KEY_LAST_7_DAYS:
                numDays = 7;
                break;
            case KEY_LAST_14_DAYS:
                numDays = 14;
                break;
            case KEY_LAST_28_DAYS:
                numDays = 28;
                break;
        }
        isSharingNote = shareNote;
        mListDataInfo.clear();
        listDate.clear();
        mTime1 = CommonUtil.getDateNow(CommonUtil.DATE_STYLE);
        mEndDate = NVDateUtils.getPDFPageReportDate(mTime1);
        mTime2 = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, mTime1, -numDays + 1);
        mStartDate = NVDateUtils.getPDFPageReportDate(mTime2);
        mTime2 = mTime2.replace(mTime2.substring(11, 19), "00:00:00");

        for (int i = 0; i < numDays; i++) {
            String[] dateOnly = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, mTime1, -numDays + 1 + i).split("T");
            listDate.add(dateOnly[0]);
        }
        doAllAS05GetResult();
    }

    private void initDataAS05() {
        ProgressLoading.dismiss();
        convertDataToLocal();
        Collections.sort(mListDataInfo);
        initShareData();
    }

    private void convertDataToLocal() {
        for (int i = 0; i < mListDataInfo.size(); i++) {
            ResultInfo item = mListDataInfo.get(i).getResult();
            String convertedTime = item.getReportedDate();
            convertedTime = NVDateUtils.dateToStringLocal(NVDateUtils.stringToDateLocal(convertedTime, NVDateFormat.TIME_RANGE_FORMAT), NVDateFormat.TIME_RANGE_FORMAT);
            item.setReportedDate(convertedTime);
        }
    }

    private void initShareData() {
        ShareDataEntity tresibaEntity = new ShareDataEntity();
        tresibaEntity.setDateData(initDateData(true));
        tresibaEntity.setNoteData(initNoteData(mListDataInfo, isSharingNote));
        tresibaEntity.setEmpty(true);
        for (int i = 0; i < tresibaEntity.getDateData().size(); i++) {
            if (!(tresibaEntity.getDateData().get(i).getUnit().size() == 1
                    && tresibaEntity.getDateData().get(i).getUnit().get(0).getValue().equals("0")
                    && tresibaEntity.getDateData().get(i).getError().equals("0"))) {
                tresibaEntity.setEmpty(false);
            }
        }
        tresibaEntity.setInfo(initInfoData(true, tresibaEntity.isEmpty()));
        getStorage().setM068TresibaShareData(tresibaEntity);

        ShareDataEntity fiaspEntity = new ShareDataEntity();
        fiaspEntity.setDateData(initDateData(false));
        fiaspEntity.setNoteData(initNoteData(mListDataInfo, isSharingNote));
        fiaspEntity.setEmpty(true);
        for (int i = 0; i < fiaspEntity.getDateData().size(); i++) {
            if (!(fiaspEntity.getDateData().get(i).getUnit().size() == 1
                    && fiaspEntity.getDateData().get(i).getUnit().get(0).getValue().equals("0")
                    && fiaspEntity.getDateData().get(i).getError().equals("0")))
                fiaspEntity.setEmpty(false);
        }
        fiaspEntity.setInfo(initInfoData(false, tresibaEntity.isEmpty()));
        getStorage().setM068FiaspShareData(fiaspEntity);
        mListener.dataAS05Ready();
    }

    private ShareDataEntity.Info initInfoData(boolean forTresiba, boolean isTresibaEmpty) {
        ShareDataEntity.Info info = new ShareDataEntity().new Info();
        info.setEmail(getStorage().getM007UserEntity().getEmail());
        info.setStartDate(mStartDate);
        info.setEndDate(mEndDate);
        if (forTresiba) {
            info.setName(DeviceEntity.TRESIBA);
        } else {
            info.setName(DeviceEntity.FIASP);
        }
        info.setTakenPercent("90");
        if (isTresibaEmpty) {
            info.setTimeEarlyTaken("");
            info.setTimeLateTaken("");
        } else {
            String[] timeTaken = CommonUtil.getInstance().calcTimingDose(mListDataInfo).split("-");
            if (timeTaken.length == 2) {
                info.setTimeEarlyTaken(timeTaken[0]);
                info.setTimeLateTaken(timeTaken[1]);
            } else {
                info.setTimeEarlyTaken("");
                info.setTimeLateTaken("");
            }
        }
        return info;
    }

    private List<ShareDataEntity.DateData> initDateData(boolean forTresiba) {
        List<ShareDataEntity.DateData> listDateData = new ArrayList<>();
        for (int i = 0; i < listDate.size(); i++) {
            ShareDataEntity.DateData dateData = new ShareDataEntity().new DateData();
            String[] dateNum = listDate.get(i).split("-");

            dateData.setNote("0");
            dateData.setPriming("0");
            dateData.setError("0");
            String date = NVDateUtils.getPDFNoteReportDate(listDate.get(i));
            dateData.setDate(date);
            dateData.setDateNum(String.valueOf(Integer.valueOf(dateNum[2])));
            if (forTresiba) {
                dateData.setUnit(dailyTresibaUnit(listDate.get(i), dateData));
            } else {
                dateData.setUnit(dailyFiaspUnit(listDate.get(i), dateData));
            }
            listDateData.add(dateData);
        }
        return listDateData;
    }

    private List<ShareDataEntity.DateData.Unit> dailyFiaspUnit(String date, ShareDataEntity.DateData dateData) {
        List<ShareDataEntity.DateData.Unit> listUnit = new ArrayList<>();
        listUnit.add(new ShareDataEntity().new DateData().new Unit("0", ""));
        for (int j = 0; j < mListDataInfo.size(); j++) {
            ResultInfo item = mListDataInfo.get(j).getResult();
            if (item.getReportedDate().contains(date)
                    && item.getAssessmentType().equals(DoseLogAdapter.TYPE_ASSESSMENT_INJECTION)
                    && item.getCustom3().equals(DeviceEntity.CODE_FIASP + "")) {
                if (CommonUtil.isUnknownDrug(item.getCustom3(), item.getCustom4())
                        || CommonUtil.isUnknownDoseSize(item.getResult(), item.getCustom4())) {
                    dateData.setError("1");
                } else {
                    String value = String.valueOf(item.getResult());
                    String time = CommonUtil.convertShortTime(item.getReportedDate());
                    listUnit.add(new ShareDataEntity().new DateData().new Unit(value, time));
                    dateData.setDate(NVDateUtils.getPDFNoteReportDate(item.getReportedDate()));
                }
            }
            if (item.getReportedDate().contains(date) && item.getAssessmentType().equals(DoseLogAdapter.TYPE_ASSESSMENT_NOTE)) {
                dateData.setNote("1");
            }
        }
        Collections.reverse(listUnit);
        return listUnit;
    }

    private List<ShareDataEntity.DateData.Unit> dailyTresibaUnit(String date, ShareDataEntity.DateData dateData) {
        List<ShareDataEntity.DateData.Unit> listUnit = new ArrayList<>();
        listUnit.add(new ShareDataEntity().new DateData().new Unit("0", ""));
        for (int j = 0; j < mListDataInfo.size(); j++) {
            ResultInfo item = mListDataInfo.get(j).getResult();
            if (item.getReportedDate().contains(date)
                    && item.getAssessmentType().equals(DoseLogAdapter.TYPE_ASSESSMENT_INJECTION)
                    && !item.getCustom3().equals(DeviceEntity.CODE_FIASP + "")) {
                if (item.getCustom3().equals(String.valueOf(DeviceEntity.CODE_TRESIBA_200))) {
                    dateData.setPriming("1");
                }
                if (CommonUtil.isUnknownDrug(item.getCustom3(), item.getCustom4())
                        || CommonUtil.isUnknownDoseSize(item.getResult(), item.getCustom4())) {
                    dateData.setError("1");
                } else {
                    String value = String.valueOf(item.getResult());
                    String time = CommonUtil.convertShortTime(item.getReportedDate());
                    listUnit.add(new ShareDataEntity().new DateData().new Unit(value, time));
                    dateData.setDate(NVDateUtils.getPDFNoteReportDate(item.getReportedDate()));
                }
            }
            if (item.getReportedDate().contains(date) && item.getAssessmentType().equals(DoseLogAdapter.TYPE_ASSESSMENT_NOTE)) {
                dateData.setNote("1");
            }
        }
        Collections.reverse(listUnit);
        return listUnit;
    }

    private List<ShareDataEntity.NoteData> initNoteData(ArrayList<DataInfo> mListDataInfo, boolean isSharingNote) {
        List<ShareDataEntity.NoteData> listNoteData = new ArrayList<>();
        if (isSharingNote) {
            for (int i = 0; i < mListDataInfo.size(); i++) {
                ShareDataEntity.NoteData noteData = new ShareDataEntity().new NoteData();
                ResultInfo item = mListDataInfo.get(i).getResult();

                if (item.getAssessmentType().equals(DoseLogAdapter.TYPE_ASSESSMENT_NOTE)) {
                    noteData.setDate(NVDateUtils.getPDFNoteReportDate(item.getReportedDate()));
                    noteData.setNote(CommonUtil.getInstance().decodeNoteContent(item.getNotes()));
                    listNoteData.add(noteData);
                }
            }
        }
        return listNoteData;
    }

    void callAS05GetResult(int index) {
        CommonUtil.wtfi(TAG, "callAS05GetResult");
        if (getStorage().getM001CarePlanUserEntity() == null
                || getStorage().getM001CarePlanUserEntity().getData() == null
                || getStorage().getM001CarePlanUserEntity().getData().isEmpty()
                || getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments() == null
                || getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments().isEmpty()) {
            mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(ERR_500));
            return;
        }

        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(KEY_API_AS05_GET_RESULT);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, "token: " + token);
        CarePlantUserEntity.AssessmentInfo item = getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments().get(index);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_AS05_GET_RESULT, item.getAssessmentId(), mTime2, mTime1));

        request.addHeaders(CommonUtil.P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addQueryParams(P_SIZE, "1000000");
        request.addQueryParams(P_SORT, "reportedDate,desc");

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        super.doFailed(tag, obj, code, data);
        if (tag.equals(KEY_API_SYNC_RESULT)) {
            showNotify(R.string.txt_sync_err);
            mListener.generateFailed();
        }
        mListener.generateFailed();
    }
}
