package com.novo.app.manager.entities;

import android.support.annotation.NonNull;

import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.ResultInfo;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RDoseEntity extends RealmObject {
    public static final String ID = "resultId";
    public static final String STATUS_FLAG = "statusFlag";
    public static final String TIME_STAMP = "custom1";
    public static final String PRIMARY_KEY = "custom2";
    public static final String STATE_ADD = "ADD";
    public static final String STATE_EDIT = "EDIT";
    public static final String STATE_DEL = "DEL";
    public static final String ASSESSMENT_TYPE = "assessmentType";

    @PrimaryKey
    private String custom2;

    private String statusFlag;
    private String resultId;
    private String deviceId;
    private String assessmentId;
    private String assessmentType;
    private long result;
    private String reportedDate;
    private String notes;
    private String custom1;
    private String custom3;
    private String custom4;

    private String name;
    private String scheduleId;
    private String activityId;
    private String activityDate;
    private String activityStatus;
    private String adherenceStatus;

    public String getResultId() {
        return resultId;
    }

    public void setResultId(String resultId) {
        this.resultId = resultId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAssessmentId() {
        return assessmentId;
    }

    public void setAssessmentId(String assessmentId) {
        this.assessmentId = assessmentId;
    }

    public String getAssessmentType() {
        return assessmentType;
    }

    public void setAssessmentType(String assessmentType) {
        this.assessmentType = assessmentType;
    }

    public long getResult() {
        return result;
    }

    public void setResult(long result) {
        this.result = result;
    }

    /**
     * This method will return reportedDate value of Dose Entity
     * @return String - reportedDate
     */
    public String getReportedDate() {
        return reportedDate;
    }

    public void setReportedDate(String reportedDate) {
        this.reportedDate = reportedDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCustom1() {
        return custom1;
    }

    public void setCustom1(String custom1) {
        this.custom1 = custom1;
    }

    public String getCustom2() {
        return custom2;
    }

    public void setCustom2(String custom2) {
        this.custom2 = custom2;
    }

    public String getCustom3() {
        return custom3;
    }

    public void setCustom3(String custom3) {
        this.custom3 = custom3;
    }

    public String getCustom4() {
        return custom4;
    }

    public void setCustom4(String custom4) {
        this.custom4 = custom4;
    }

    public String getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(String statusFlag) {
        this.statusFlag = statusFlag;
    }

    public DataInfo toResultInfo() {
        DataInfo info = new DataInfo();
        info.setActivityDate(activityDate);
        info.setActivityId(assessmentId);
        info.setActivityStatus(activityStatus);
        info.setAdherenceStatus(adherenceStatus);
        info.setName(name);

        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setCustom1(custom1);
        resultInfo.setCustom2(custom2);
        resultInfo.setCustom3(custom3);
        resultInfo.setCustom4(custom4);

        resultInfo.setDeviceId(deviceId);
        resultInfo.setNotes(notes);
        resultInfo.setResult(result);
        resultInfo.setResultId(resultId);
        resultInfo.setReportedDate(reportedDate);
        resultInfo.setAssessmentType(assessmentType);
        resultInfo.setAssessmentId(assessmentId);
        info.setResult(resultInfo);
        return info;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(String activityDate) {
        this.activityDate = activityDate;
    }

    public String getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }

    public String getAdherenceStatus() {
        return adherenceStatus;
    }

    public void setAdherenceStatus(String adherenceStatus) {
        this.adherenceStatus = adherenceStatus;
    }

    @NonNull
    @Override
    public String toString() {
        return "resultId: " + resultId + "\n"
                + "assessmentId: " + assessmentId + "\n"
                + "assessmentType: " + assessmentType + "\n"
                + "result: " + result + "\n"
                + "reportedDate: " + reportedDate + "\n"
                + "notes: " + notes + "\n"
                + "custom1: " + custom1 + "\n"
                + "custom2: " + custom2 + "\n"
                + "custom3: " + custom3 + "\n"
                + "custom4: " + custom4 + "\n"
                + "name: " + name + "\n"
                + "status: " + statusFlag + "\n"
                + "custom4: " + custom4 + "\n"
                + "scheduleId: " + scheduleId + "\n"
                + "activityDate: " + activityDate + "\n"
                + "activityId: " + activityId + "\n"
                + "deviceId: " + deviceId;
    }
}