package com.novo.app.presenter;

import com.novo.app.view.event.OnM057SupportVideoCollectCallBack;

public class M057SupportVideoCollectPresenter extends BasePresenter<OnM057SupportVideoCollectCallBack> {
    public M057SupportVideoCollectPresenter(OnM057SupportVideoCollectCallBack event) {
        super(event);
    }
}
