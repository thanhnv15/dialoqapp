package com.novo.app.view.fragment;

import android.text.Editable;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M009SignUpPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseEditText;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM009SignUpCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.TextAdapter;

import java.util.Objects;

public class M009SignUpEmailFrg extends BaseFragment<M009SignUpPresenter, OnHomeBackToView> implements OnM009SignUpCallBack {
    public static final String TAG = M009SignUpEmailFrg.class.getName();
    private Button mBtNext;
    private BaseEditText mEdtEmail;
    private TextView mTvError;
    private TableRow mTrError;

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m009_logo, this);
        findViewById(R.id.iv_m009_back, this);
        findViewById(R.id.tv_m009_account_setup, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m009_description, CAApplication.getInstance().getRegularFont());
        mTvError = findViewById(R.id.tv_m009_error, CAApplication.getInstance().getRegularFont());

        findViewById(R.id.tv_m009_cancel, this, CAApplication.getInstance().getBoldFont());

        mBtNext = findViewById(R.id.bt_m009_next, this, CAApplication.getInstance().getRegularFont());
        mBtNext.setEnabled(false);

        mTrError = findViewById(R.id.tr_m009_error);
        mTrError.setVisibility(View.GONE);

        mEdtEmail = findViewById(R.id.tv_m009_email, CAApplication.getInstance().getRegularFont());
        if (getStorage().getM001ProfileEntity().getEmail() != null) {
            mEdtEmail.setText(getStorage().getM001ProfileEntity().getEmail());
            mEdtEmail.setTypeface(CAApplication.getInstance().getBoldFont());
            mBtNext.setEnabled(true);
        }
        mEdtEmail.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                String email = editable.toString();
                CommonUtil.getInstance().initTypeFace(mEdtEmail);
                if (CommonUtil.isNotEmail(editable.toString())) {
                    mTvError.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_err_invalid_email)));
                    mTrError.setVisibility(View.VISIBLE);
                    mBtNext.setEnabled(false);
                } else {
                    mEdtEmail.setTextColor(getResources().getColor(R.color.colorBlack));
                    mTrError.setVisibility(View.GONE);
                    mBtNext.setEnabled(true);
                }
                if (editable.length() == 0) {
                    mTvError.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_err_empty_email)));
                    mBtNext.setEnabled(false);
                }
            }
        });
        onEmailChange();
        onEmailFocusChange();
        onKeyBoardDismiss();
    }

    private void onKeyBoardDismiss() {
        mEdtEmail.setOnEditTextImeBackListener((ctrl, text) -> {
            mEdtEmail.clearFocus();
            if (mEdtEmail.getText() != null && !mEdtEmail.getText().toString().isEmpty())
                mBtNext.setEnabled(!CommonUtil.isNotEmail(mEdtEmail.getText().toString()));
        });

        mEdtEmail.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEdtEmail.clearFocus();
                if (mEdtEmail.getText() != null && !mEdtEmail.getText().toString().isEmpty())
                    mBtNext.setEnabled(!CommonUtil.isNotEmail(mEdtEmail.getText().toString()));
            }
            return false;
        });
    }

    private void onEmailFocusChange() {
        mEdtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String email = Objects.requireNonNull(mEdtEmail.getText()).toString();
                if (!hasFocus) {
                    CommonUtil.getInstance().forceHideKeyBoard(mEdtEmail);
                    showEmailError(CommonUtil.isNotEmail(email), mEdtEmail.getText().toString().isEmpty());
                } else {
                    showEmailError(false, Objects.requireNonNull(mEdtEmail.getText()).toString().isEmpty());
                    mBtNext.setEnabled(false);
                }
            }
        });
    }

    private void onEmailChange() {
        mEdtEmail.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                CommonUtil.getInstance().initTypeFace(mEdtEmail);
                showEmailError(false, Objects.requireNonNull(mEdtEmail.getText()).toString().isEmpty());
                mBtNext.setEnabled(!CommonUtil.isNotEmail(editable.toString()));
            }
        });
    }

    private void showEmailError(boolean show, boolean isEmpty) {
        if (show) {
            int color;
            String msg;
            if (isEmpty) {
                msg = getString(R.string.txt_m009_email_require);
                color = getResources().getColor(R.color.colorBlack);
            } else {
                msg = LangMgr.getInstance().getLangList().get(getString(R.string.lang_err_invalid_email));
                color = getResources().getColor(R.color.colorM009RustyRed);
            }
            mTvError.setText(msg);
            mEdtEmail.setTextColor(color);
            mTrError.setVisibility(View.VISIBLE);
        } else {
            mEdtEmail.setTextColor(getResources().getColor(R.color.colorBlack));
            mTrError.setVisibility(View.GONE);
        }
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m009_signup_email;
    }

    @Override
    protected M009SignUpPresenter getPresenter() {
        return new M009SignUpPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //Wait for customer confirmation
    }

    @Override
    public void backToPreviousScreen() {
        mCallBack.showFrgScreen(TAG, M007AccountSetUpLastNameFrg.TAG);
    }


    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m009_next:
                CommonUtil.getInstance().forceHideKeyBoard(mEdtEmail);
                getStorage().getM001ProfileEntity().setEmail(Objects.requireNonNull(mEdtEmail.getText()).toString());
                mCallBack.showFrgScreen(TAG, M045SignUpEmailConfirmFrg.TAG);
                break;
            case R.id.tv_m009_cancel:
                CommonUtil.getInstance().forceHideKeyBoard(mEdtEmail);
                mCallBack.registrationCancel(TAG);
                break;
            case R.id.iv_m009_back:
                CommonUtil.getInstance().forceHideKeyBoard(mEdtEmail);
                backToPreviousScreen();
                break;
            default:
                break;
        }
    }
}
