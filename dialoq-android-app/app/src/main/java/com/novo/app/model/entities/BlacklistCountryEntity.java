package com.novo.app.model.entities;

import java.io.Serializable;

public class BlacklistCountryEntity implements Serializable {
    private TextEntity blacklistCountry;

    public BlacklistCountryEntity(TextEntity blacklistCountry) {
        this.blacklistCountry = blacklistCountry;
    }

    public TextEntity getBlacklistCountry() {
        return blacklistCountry;
    }

    public void setBlacklistCountry(TextEntity blacklistCountry) {
        this.blacklistCountry = blacklistCountry;
    }
}
