/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.view.base;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.BasePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.utils.StorageCommon;
import com.novo.app.view.event.OnActionCallBack;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.widget.CustomTypefaceSpan;
import com.novo.app.view.widget.ProgressLoading;

/**
 * Created by ThanhNv on 6/9/2017.
 * This is the prototype for all dialog used in this app
 * <p>
 * This act as 'View' object in MVP pattern
 * Linked `Presenter` will be an inherited instance of {@link BasePresenter}
 * This fragment will is possible to trigger callbacks defined in {@link OnCallBackToView} interface
 * <p>
 * This Dialog is also possible to support animating it's view components when user clicked
 */
public abstract class BaseDialog<T extends BasePresenter, H extends OnCallBackToView> extends
        Dialog implements View.OnClickListener, Animation.AnimationListener, OnActionCallBack {

    // Identifier for checking if a text is place holder or content
    private static final CharSequence PLACE_HOLDER = "PLACEHOLDER";

    // Tag for this dialog
    private static final String TAG = "DIALOG";

    // MVP Presenter
    protected T mPresenter;

    // Delegate
    protected H mCallBack;

    // Context
    protected Context mContext;

    // For view components animation
    private boolean isAnimEnd = true;
    private Animation mAnim;
    private int mId;
    private BroadcastReceiver mTimeZoneReceiver;

    /**
     * Constructor
     *
     * @param context where the dialog show
     */
    public BaseDialog(Context context) {
        this(context, false);
    }

    /**
     * Constructor
     *
     * @param context where the dialog show
     * @param style   for the dialog
     */
    public BaseDialog(Context context, int style) {
        this(context, true, style);
    }

    /**
     * Constructor
     *
     * @param context  where the dialog show
     * @param style    for the dialog
     * @param isCancel true if the dialog is cancellable
     */
    public BaseDialog(Context context, boolean isCancel, int style) {
        super(context, style);
        mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(getLayoutId());
        setCancelable(isCancel);
        setCanceledOnTouchOutside(isCancel);
        mPresenter = getPresenter();
        mAnim = AnimationUtils.loadAnimation(mContext, R.anim.alpha);
        mAnim.setAnimationListener(this);
        initViews();
        registerTimeZoneChange();
    }

    /**
     * Constructor
     *
     * @param context  where the dialog show
     * @param isCancel true if the dialog is cancellable
     */
    public BaseDialog(Context context, boolean isCancel) {
        super(context);
        mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(getLayoutId());
        setCancelable(isCancel);
        setCanceledOnTouchOutside(isCancel);
        mPresenter = getPresenter();
        mAnim = AnimationUtils.loadAnimation(mContext, R.anim.alpha);
        mAnim.setAnimationListener(this);
        initViews();
        registerTimeZoneChange();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initData();
    }

    protected void initData() {

    }

    /**
     * Show progress bar indicating that user has to wait for something
     */
    public void showLockDialog() {
        ProgressLoading.show(mContext);
    }

    /**
     * Find a view in this fragment, and also apply {@link View.OnClickListener}
     *
     * @param id    view id to be searched
     * @param event {@link View.OnClickListener} to be applied to the found view
     * @param <G>   view type (must be child class of {@link View}
     * @return view if found, or null
     */
    protected final <G extends View> G findViewById(int id, View.OnClickListener event) {
        return findViewById(id, event, true, null);
    }

    /**
     * Find a view in this fragment. In case of search for an {@link TextView} instance,
     * apply a {@link Typeface}
     *
     * @param id       view id to be searched
     * @param typeFace applied to {@link TextView}
     * @param <G>      view type (must be child class of {@link View}
     * @return view if found, or null
     */
    protected final <G extends View> G findViewById(int id, Typeface typeFace) {
        return findViewById(id, null, true, typeFace);
    }

    /**
     * Find a view in this fragment, and also apply {@link View.OnClickListener}
     * In case of search for an {@link TextView} instance, also apply a {@link Typeface}
     *
     * @param id       view id to be searched
     * @param event    {@link View.OnClickListener} to be applied to the found view
     * @param typeFace applied to {@link TextView}
     * @param <G>      view type (must be child class of {@link View}
     * @return view if found, or null
     */
    protected final <G extends View> G findViewById(int id, View.OnClickListener event, Typeface typeFace) {
        return findViewById(id, event, true, typeFace);
    }

    /**
     * Find a view in this fragment, and also apply {@link View.OnClickListener}, {@link View#isEnabled()}
     * In case of search for an {@link TextView} instance, also apply a {@link Typeface}
     *
     * @param id       view id to be searched
     * @param event    {@link View.OnClickListener} to be applied to the found view
     * @param enable   boolean value to be applied to the found view as view's enability
     * @param typeFace applied to {@link TextView}
     * @param <G>      view type (must be child class of {@link View}
     * @return view if found, or null
     */
    protected final <G extends View> G findViewById(int id, View.OnClickListener event, boolean enable, Typeface typeFace) {
        G view = findViewById(id);
        view.setOnClickListener(event);
        view.setEnabled(enable);
        if (typeFace != null && view instanceof TextView) {
            ((TextView) view).setTypeface(typeFace);
        }

        if (view.getContentDescription() == null) {
            CommonUtil.wtfe(TAG, "No. ContentDescription");
        } else if (view instanceof TextView) {
            String key = view.getContentDescription().toString();
            if (LangMgr.getInstance().getCurrentLang() == null) {
                CommonUtil.wtfe(TAG, "Err: No current language");
            } else {
                String text = LangMgr.getInstance().getLangList().get(key);
                if (text == null) return view;
                if (key.contains(PLACE_HOLDER)) {
                    ((TextView) view).setHint(text);
                } else {
                    ((TextView) view).setText(text);
                }
            }
        }
        return view;
    }

    /**
     * Set delegate
     *
     * @param event delegate which handle callback triggered by this dialog
     */
    public void setOnCallBack(H event) {
        mCallBack = event;
    }

    /**
     * get Presenter
     *
     * @return inherited instance of {@link BasePresenter}
     */
    protected abstract T getPresenter();

    /**
     * Initialize view related tasks
     * Override this method if there is some data need to be initialized
     * or preparing content for the view of this dialog
     */
    protected abstract void initViews();

    /**
     * Get layout id for this dialog
     * Inherited classes must provide layout id to be inflated
     *
     * @return layout id of dialog's content view
     */
    public abstract int getLayoutId();

    /**
     * Triger animation of clicked view
     *
     * @param v view which has been clicked
     */
    @Override
    public void onClick(View v) {
        if (!isAnimEnd) return;
        isAnimEnd = false;
        mId = v.getId();
        v.startAnimation(mAnim);
    }

    /**
     * Hanlde {@link Animation.AnimationListener} callback
     */
    @Override
    public final void onAnimationStart(Animation animation) {

    }

    /**
     * Hanlde {@link Animation.AnimationListener} callback
     */
    @Override
    public final void onAnimationEnd(Animation animation) {
        onClickView(mId);
        isAnimEnd = true;
    }

    /**
     * After animation view component, onClick callback should be handled here
     */
    protected void onClickView(int idView) {

    }

    /**
     * Hanlde {@link Animation.AnimationListener} callback
     */
    @Override
    public final void onAnimationRepeat(Animation animation) {

    }

    /**
     * Common method might be used by inherited classes for getting common storage object
     *
     * @return storage common object
     */
    public final StorageCommon getStorage() {
        return CAApplication.getInstance().getStorageCommon();
    }

    /**
     * Highlight a {@link TextView} with specific text style in a specific range
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param start    start range of text will be highlighted (index of character in TextView's text)
     * @param end      end range of text will be highlighted (index of character in TextView's text)
     * @param color    highlighted color
     */
    protected void highLightText(TextView mTvTitle, int start, int end, int color) {
        highLightText(mTvTitle, start, end, color, false, 0, null);
    }

    /**
     * Highlight a {@link TextView} with specific text style in a specific range
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param start    start range of text will be highlighted (index of character in TextView's text)
     * @param end      end range of text will be highlighted (index of character in TextView's text)
     * @param color    highlighted color
     * @param isBold   highlighted style
     */
    protected void highLightText(TextView mTvTitle, int start, int end, int color, boolean isBold) {
        highLightText(mTvTitle, start, end, color, isBold, 0, null);
    }

    /**
     * Highlight a {@link TextView} with specific text style in a specific range
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param start    start range of text will be highlighted (index of character in TextView's text)
     * @param end      end range of text will be highlighted (index of character in TextView's text)
     * @param color    highlighted color
     * @param size     highlighted size
     */
    protected void highLightText(TextView mTvTitle, int start, int end, int color, int size) {
        highLightText(mTvTitle, start, end, color, false, size, null);
    }

    /**
     * Highlight a {@link TextView} with specific text style in a specific range
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param start    start range of text will be highlighted (index of character in TextView's text)
     * @param end      end range of text will be highlighted (index of character in TextView's text)
     * @param color    highlighted color
     * @param isBold   highlighted style
     * @param size     highlighted size
     */
    protected void highLightText(TextView mTvTitle, int start, int end, int color, boolean isBold, int size) {
        highLightText(mTvTitle, start, end, color, isBold, size, null);
    }

    /**
     * Highlight a {@link TextView} with specific text style in a specific range
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param start    start range of text will be highlighted (index of character in TextView's text)
     * @param end      end range of text will be highlighted (index of character in TextView's text)
     * @param color    highlighted color
     * @param size     highlighted size
     * @param typeface highlighted typeface
     */
    protected void highLightText(TextView mTvTitle, int start, int end, int color, int size, Typeface typeface) {
        highLightText(mTvTitle, start, end, color, false, size, typeface);
    }

    /**
     * Highlight a {@link TextView} with specific text style in a specific range
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param start    start range of text will be highlighted (index of character in TextView's text)
     * @param end      end range of text will be highlighted (index of character in TextView's text)
     * @param color    highlighted color
     * @param typeface highlighted typeface
     */
    protected void highLightText(TextView mTvTitle, int start, int end, int color, Typeface typeface) {
        highLightText(mTvTitle, start, end, color, false, 0, typeface);
    }

    /**
     * Highlight a {@link TextView} with specific text style in a specific range
     *
     * @param mTvTitle {@link TextView} instance which will be highlighted
     * @param start    start range of text will be highlighted (index of character in TextView's text)
     * @param end      end range of text will be highlighted (index of character in TextView's text)
     * @param color    highlighted color
     * @param isBold   highlighted style
     * @param size     highlighted size
     * @param typeface highlighted typeface
     */
    protected void highLightText(TextView mTvTitle, int start, int end, int color, boolean isBold, int size, Typeface typeface) {
        try {
            Spannable textToSpan = new SpannableString(mTvTitle.getText().toString());
            if (isBold) {
                textToSpan.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else if (typeface != null) {
                textToSpan.setSpan(new CustomTypefaceSpan(typeface), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            if (size > 0) {
                textToSpan.setSpan(new RelativeSizeSpan((mTvTitle.getTextSize() + size) / mTvTitle.getTextSize()), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            textToSpan.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(color)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mTvTitle.setText(textToSpan);
        } catch (Exception ignored) {
            //do nothing for temporary
        }
    }

    /**
     * Highlight a sub-text in a {@link TextView}
     *
     * @param mTvTitle  {@link TextView} instance which will be highlighted
     * @param highLight text will be highlighted
     * @param color     highlighted color
     * @param size      highlighted size
     * @param typeface  highlighted typeface
     */
    protected void highLightText(TextView mTvTitle, String highLight, int color, int size, Typeface typeface) {
        highLightText(mTvTitle, highLight, color, size, false, typeface);
    }

    /**
     * Highlight a sub-text in a {@link TextView}
     *
     * @param mTvTitle  {@link TextView} instance which will be highlighted
     * @param highLight text will be highlighted
     * @param color     highlighted color
     * @param typeface  highlighted typeface
     */
    protected void highLightText(TextView mTvTitle, String highLight, int color, Typeface typeface) {
        highLightText(mTvTitle, highLight, color, 0, false, typeface);
    }

    /**
     * Highlight a sub-text in a {@link TextView}
     *
     * @param mTvTitle  {@link TextView} instance which will be highlighted
     * @param highLight text will be highlighted
     * @param color     highlighted color
     * @param isBold    highlighted style
     */
    protected void highLightText(TextView mTvTitle, String highLight, int color, boolean isBold) {
        highLightText(mTvTitle, highLight, color, 0, isBold, null);
    }

    /**
     * Highlight a sub-text in a {@link TextView}
     *
     * @param mTvTitle  {@link TextView} instance which will be highlighted
     * @param highLight text will be highlighted
     * @param color     highlighted color
     * @param size      highlighted size
     */
    protected void highLightText(TextView mTvTitle, String highLight, int color, int size) {
        highLightText(mTvTitle, highLight, color, size, false, null);
    }

    /**
     * Highlight a sub-text in a {@link TextView}
     *
     * @param mTvTitle  {@link TextView} instance which will be highlighted
     * @param highLight text will be highlighted
     * @param color     highlighted color
     * @param size      highlighted size
     * @param isBold    highlighted style
     */
    protected void highLightText(TextView mTvTitle, String highLight, int color, int size, boolean isBold) {
        highLightText(mTvTitle, highLight, color, size, isBold, null);
    }

    /**
     * Highlight a sub-text in a {@link TextView}
     *
     * @param mTvTitle  {@link TextView} instance which will be highlighted
     * @param highLight text will be highlighted
     * @param color     highlighted color
     */
    protected void highLightText(TextView mTvTitle, String highLight, int color) {
        highLightText(mTvTitle, highLight, color, 0, false, null);
    }

    /**
     * Highlight a sub-text in a {@link TextView}
     *
     * @param mTvTitle  {@link TextView} instance which will be highlighted
     * @param highLight text will be highlighted
     * @param color     highlighted color
     * @param size      highlighted size
     * @param isBold    highlighted style
     * @param typeface  highlighted typeface
     */
    private void highLightText(TextView mTvTitle, String highLight, int color, int size, boolean isBold, Typeface typeface) {
        int start = mTvTitle.getText().toString().indexOf(highLight);
        int end = start + highLight.length();
        if (start < 0) return;

        highLightText(mTvTitle, start, end, color, isBold, size, typeface);
    }

    /**
     * Generic method for showing a number in Toast notification
     *
     * @param text number to be shown -> variable name and type is ambiguous
     */
    public void showNotify(int text) {
        Toast.makeText(CAApplication.getInstance(), text, Toast.LENGTH_SHORT).show();
    }

    /**
     * Generic method for showing a number in Toast notification
     *
     * @param text number to be shown -> refactor: name and type is ambiguous
     */
    public void showNotify(String text) {
        Toast.makeText(CAApplication.getInstance(), text, Toast.LENGTH_SHORT).show();
    }

    /**
     * Common method for showing an alert dialog
     *
     * @param txtErr dialog string content will be shown
     */
    public void showAlertDialog(String txtErr) {
        CommonUtil.getInstance().showDialog(mContext, txtErr, null);
    }

    /**
     * Common method for showing an alert dialog
     *
     * @param txtErrId string id of content will be shown
     */
    public void showAlertDialog(int txtErrId) {
        CommonUtil.getInstance().showDialog(mContext, txtErrId, null);
    }

    private void registerTimeZoneChange() {
        try {
            CommonUtil.wtfi(TAG, "doTimeZoneChange...");
            getStorage().addDialogCallBack(this);
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getMessage());
        }
    }

    @Override
    public void dismiss() {
        getStorage().removeDialogCallBack(this);
        super.dismiss();
    }

    /**
     * warning if time zone/time format is changed in the phone setting
     */
    protected void warningForTimeZoneChange() {
        //do nothing
    }

    @Override
    public void onCallBack(Object data) {
        warningForTimeZoneChange();
    }
}
