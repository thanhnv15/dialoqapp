/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.presenter;

import android.annotation.SuppressLint;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RDecryptionKeyEntity;
import com.novo.app.manager.entities.RDoseEntity;
import com.novo.app.manager.entities.RInjectionEntity;
import com.novo.app.manager.entities.RInjectionItem;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.DecryptionKeyEntity;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.model.entities.ResultEntity;
import com.novo.app.model.entities.ResultInfo;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.DoseLogAdapter;
import com.novo.app.view.event.OnM025SubDoseLogCallBack;
import com.novo.app.view.widget.NVDateUtils;
import com.novo.app.view.widget.ProgressLoading;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.realm.RealmList;

import static com.novo.app.service.SyncScheduleService.KEY_APP_DOSE_ADD_DELETE;
import static com.novo.app.service.SyncScheduleService.KEY_APP_DOSE_ADD_DELETE_TIME;
import static com.novo.app.service.SyncScheduleService.KEY_APP_NOTE_ADD_DELETE;
import static com.novo.app.service.SyncScheduleService.KEY_APP_NOTE_ADD_DELETE_TIME;
import static com.novo.app.utils.CommonUtil.P_API_KEY;

public class M025SubDoseLogPresenter extends BasePresenter<OnM025SubDoseLogCallBack> {

    public static final String KEY_SAVE_CALLING_AS5 = "KEY_SAVE_CALLING_AS5";
    static final String KEY_API_NNDM_SYSTEM_ID_GET_DECRYPTION = "KEY_API_NNDM_SYSTEM_ID_GET_DECRYPTION";
    private static final String KEY_TIME2_LAST = "KEY_TIME2_LAST";
    private static final String KEY_API_AS05_GET_RESULT = "KEY_API_AS05_GET_RESULT";
    private static final String URL_API_AS05_GET_RESULT = CommonUtil.HOT_URL + "/api/v1/assessments/%s/results/%s/%s?";
    private static final String KEY_API_AS05_FOR_DEVICE_LIST = "KEY_API_AS05_FOR_DEVICE_LIST";
    private static final String URL_API_NNDM_SYSTEM_ID_GET_DECRYPTION = "https://flexdigitalhealth-dev.apigee.net/dev1-dialoq-product/dialoq-service/api/v1/devices/%s?";
    private static final String P_SORT = "sort";
    private static final String P_SIZE = "size";
    private static final String EXCLUDE_TYPE = "Devices";
    private static final String EXCLUDE_TYPE_INJECTIONS = "Injections";
    private static final String TAG = M025SubDoseLogPresenter.class.getName();
    private final ArrayList<DataInfo> mListDataInfo = new ArrayList<>();
    private List<Date> mListReportDate = new ArrayList<>();
    private Set<Date> setDate = new HashSet<>();
    private int mIndex = 0;
    private List<ResultEntity> mListSaveData = new ArrayList<>();
    private int mTimeCount;

    /**
     * The constructor of M025SubDoseLogPresenter
     *
     * @param event is the interface listen to this class callback
     */
    public M025SubDoseLogPresenter(OnM025SubDoseLogCallBack event) {
        super(event);
    }

    /**
     * Handle each successful request
     *
     * @see BasePresenter#handleSuccess(String, String)
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void handleSuccess(String data, String tag) {
        //CommonUtil.wtfi(TAG, "DATA: " + data);
        CommonUtil.wtfi(TAG, "handleSuccess-tagRequest: " + tag);
        switch (tag) {
            case KEY_API_AS05_GET_RESULT:
                prepareAS05(data);
                break;
            case KEY_API_AS05_FOR_DEVICE_LIST:
                prepareAS05DeviceList(data);
                break;
            case KEY_API_NNDM_SYSTEM_ID_GET_DECRYPTION:
                prepareNNDMSystemIdForDecryption(data);
                break;
            default:
                break;
        }
    }

    /**
     * Start a sequence of method to load the dose log data from the Realm local database
     */
    public void loadOffline() {
        List<RDoseEntity> listData = CADBManager.getInstance().getAllRDoseEntity(CommonUtil.INJECTION_ASSESSMENT_TYPE);
        List<RDoseEntity> listOther = CADBManager.getInstance().getAllRDoseEntity(CommonUtil.OTHERS_ASSESSMENT_TYPE);
        if (listData != null) {
            if (listOther != null && listOther.size() > 0) {
                listData.addAll(listOther);
            }

            for (int i = 0; i < listData.size(); i++) {
                DataInfo entity = listData.get(i).toResultInfo();
                if (entity == null) continue;
                mListDataInfo.add(entity);
            }
        }

        listData = CADBManager.getInstance().getAllRDoseEntity(CommonUtil.DEVICE_ASSESSMENT_TYPE);
        if (!listData.isEmpty()) {
            RDoseEntity entity = listData.get(0);
            if (entity != null) {
                extractDeviceList(Integer.parseInt(entity.getAssessmentId()), listData);
            }
        }

        List<RDecryptionKeyEntity> listNNDM = CADBManager.getInstance().getAllNNDMEntity();
        if (!listNNDM.isEmpty()) {
            for (int i = 0; i < listNNDM.size(); i++) {
                getStorage().getNNMDDecryptionKey().add(listNNDM.get(i));
            }
        }

        //for done
        initDataAS05();
    }

    /**
     * Prepare the Decryption key to decrypt bluetooth data when synchronize with the Dialoq cap
     *
     * @param data is the response date from the NNDM service
     */
    private synchronized void prepareNNDMSystemIdForDecryption(String data) {
        CommonUtil.wtfi(TAG, "prepareNNDMSystemIdForDecryption..." + data);
        DecryptionKeyEntity entity = generateData(DecryptionKeyEntity.class, data);
        if (entity == null) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }

        RDecryptionKeyEntity item = CADBManager.getInstance().addRNNDMEntity(entity);
        if (item != null) {
            getStorage().getNNMDDecryptionKey().add(item);
        }
    }

    /**
     * Prepare the response data after successfully get list devices from the backend
     *
     * @param data is the response data from backend
     */
    private void prepareAS05DeviceList(String data) {
        CommonUtil.wtfi(TAG, "prepareAS05DeviceList..." + data);
        ResultEntity entity = generateData(ResultEntity.class, data);

        if (entity == null) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }

        //CADBManager.getInstance().deleteAllRDose(CommonUtil.DEVICE_ASSESSMENT_TYPE);
        CADBManager.getInstance().deleteAllNNDM();

//        if (entity.getEvents() != null && entity.getEvents().getData() != null) {
//            for (int i = 0; i < entity.getEvents().getData().length; i++) {
//                entity.getEvents().getData()[i].getResult().setCustom2(entity.getEvents().getData()[i].getResult().getResultId());
//            }
//        }

        List<RDoseEntity> listDevices = CADBManager.getInstance().addRDoseEntity(entity);
        if (listDevices != null) {
            extractDeviceList(entity.getAssessmentId(), listDevices);
        }

        if (getStorage().getNNMDDecryptionKey().isEmpty()) {
            doCallNNDMSystemIdForDecryption();
        }
    }

    /**
     * Make a set of request to the NNDM service to prepare the decryption key to decrypt the bluetooth data when
     * synchronize with the Dialoq cap
     */
    private void doCallNNDMSystemIdForDecryption() {
        CommonUtil.wtfi(TAG, "doCallNNDMSystemIdForDecryption...");
        if (getStorage().getM025ListDevices() == null) return;
        for (int i = 0; i < getStorage().getM025ListDevices().size(); i++) {
            callNNDMSystemIdForDecryption(i);
        }
    }

    /**
     * Call to the NNDM service to prepare the decryption key to decrypt the bluetooth data when
     * synchronize with the Dialoq cap
     */
    private void callNNDMSystemIdForDecryption(int i) {
        CommonUtil.wtfi(TAG, "callNNDMSystemIdForDecryption...");
        CARequest request = new CARequest(TAG, CARequest.METHOD_GET, false);
        request.addTAG(KEY_API_NNDM_SYSTEM_ID_GET_DECRYPTION);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, LOG_TEXT_TOKEN + token);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        String systemId = getStorage().getM025ListDevices().get(i).getSystemId().replace("0x", "");

        request.addPathSegment(String.format(URL_API_NNDM_SYSTEM_ID_GET_DECRYPTION, systemId));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Get the assessment id of the device assessment from the user's care plan
     *
     * @return assessment id of the device assessment
     */
    private String getDeviceAssessmentId() {
        CarePlantUserEntity entity = getStorage().getM001CarePlanUserEntity();
        for (int i = 0; i < entity.getData().size(); i++) {
            for (int f = 0; f < entity.getData().get(i).getAssessments().size(); f++) {
                String type = entity.getData().get(i).getAssessments().get(f).getType();
                if (type.equals(EXCLUDE_TYPE)) {
                    return entity.getData().get(i).getAssessments().get(f).getAssessmentId();
                }
            }
        }
        return "";
    }

    private void prepareAS05(String data) {
        mIndex++;
        //CommonUtil.wtfi(TAG, "mIndex..." + mIndex + ">" + (mTimeCount * (getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments().size() - 1)));
        //CommonUtil.wtfi(TAG, "prepareAS05..." + data);
        ResultEntity entity = generateData(ResultEntity.class, data);
        if (entity == null) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }

//        if (entity.getEvents() != null && entity.getEvents().getData() != null) {
//            for (int i = 0; i < entity.getEvents().getData().length; i++) {
//                entity.getEvents().getData()[i].getResult().setCustom2(entity.getEvents().getData()[i].getResult().getResultId());
//            }
//        }

        mListSaveData.add(entity);
        if (!entity.getType().equals(DoseLogAdapter.TYPE_ASSESSMENT_NOTE)) {
            mListDataInfo.addAll(Arrays.asList(entity.getEvents().getData()));
        }

        //For done
        if (mIndex % (mTimeCount * (getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments().size() - 1)) == 0) {
            CommonUtil.getInstance().clearPrefContent(KEY_SAVE_CALLING_AS5);
            initDataAS05();
        }
    }

    /**
     * Prepare Dose DataInfo by doing some steps:
     * <br/>1. convert the reportedDate of each Dose DataInfo item to short date format (yyyy-MM-dd)
     * <br/>2. Set reminder base on mMinDate, mMaxDate the reportedDate of all Dose DataInfo items
     */
    public void prepareDoseDataInfo() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat(CommonUtil.DATE_NOW_DY);
        for (int i = 0; i < mListDataInfo.size(); i++) {
            String type = mListDataInfo.get(i).getResult().getAssessmentType();
            if (type.equals(EXCLUDE_TYPE_INJECTIONS)) {
                List<ResultInfo> mListResultInfo = Collections.singletonList(mListDataInfo.get(i).getResult());
                for (int j = 0; j < mListResultInfo.size(); j++) {
                    String reportDate = mListResultInfo.get(j).getReportedDate();
                    String shortDate = CommonUtil.convertShortDate(reportDate);
                    Date localDate = null;
                    try {
                        localDate = sdf.parse(shortDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    setDate.addAll(Collections.singleton(localDate));
                }
            }
        }
        mListReportDate.addAll(setDate);
        if (!mListReportDate.isEmpty()) {
            Date mMinDate = Collections.min(mListReportDate);
            Date mMaxDate = Collections.max(mListReportDate);
            String minDate = sdf.format(mMinDate);
            String maxDate = sdf.format(mMaxDate);

            CommonUtil.wtfi(TAG, "minDate" + minDate);
            CommonUtil.wtfi(TAG, "maxDate" + maxDate);
            mListener.setReminderNotification(minDate, maxDate);
        } else {
            CommonUtil.wtfi(TAG, "No dose detected");
        }
    }

    /**
     * Initiate the time range to get dose log data
     *
     * @param isLoadMore to distinguish between 2 behavior: "pull to refresh" and "load more"
     */
    public void initTime(boolean isLoadMore) {
        String lasTime2 = CommonUtil.getInstance().getPrefContent(KEY_TIME2_LAST);
        if (lasTime2 == null || lasTime2.isEmpty()) {
            lasTime2 = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, -90);
        }

        if (isLoadMore) {
            lasTime2 = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, lasTime2, -90);
        }
        CommonUtil.getInstance().savePrefContent(KEY_TIME2_LAST, lasTime2);
    }

    /**
     * Initiate the dose log data to display
     */
    private synchronized void initDataAS05() {
        storeData();
        List<RDoseEntity> listDBNotes = CADBManager.getInstance().getAllNote();
        if (!listDBNotes.isEmpty()) {
            for (int i = 0; i < listDBNotes.size(); i++) {
                mListDataInfo.add(listDBNotes.get(i).toResultInfo());
            }
        }

        List<RInjectionEntity> listInjection = CADBManager.getInstance().getAllInjectionItem();

        if (!listInjection.isEmpty()) {
            for (int i = 0; i < listInjection.size(); i++) {
                RInjectionEntity injectionEntity = listInjection.get(i);

                if (injectionEntity.getInjectionItems() == null) continue;
                RealmList<RInjectionItem> listDose = injectionEntity.getInjectionItems();

                for (int j = 0; j < listDose.size(); j++) {
                    RInjectionItem item = listDose.get(j);
                    if (item == null) continue;
                    mListDataInfo.add(item.toResultInfo());
                }
            }

            if (listInjection.size() > 0) {
                RInjectionEntity injectionEntity = listInjection.get(listInjection.size() - 1);
                RInjectionItem device = injectionEntity.getDevice();
                if (device != null) {

                    for (int i = 0; i < getStorage().getM025ListDevices().size(); i++) {
                        DeviceEntity deviceEntity = getStorage().getM025ListDevices().get(i);
                        if (deviceEntity.getDeviceID().equals(device.getDeviceId())) {
                            getStorage().getM025ListDevices().set(i, updateDevice(device));
                            break;
                        }
                    }
                }
            }
        }

        Collections.sort(mListDataInfo);
        CommonUtil.convertToLocalDateData(mListDataInfo);
        saveLastTresibaDose(mListDataInfo);
        mListener.dataAS05Ready();
    }

    /**
     * Clean and store new dose log data into the Realm local database
     */
    private void storeData() {
        if (mListSaveData.isEmpty()) {
            return;
        }

        CADBManager.getInstance().deleteAllDeletedSynRNote(mListSaveData);
//        CADBManager.getInstance().deleteAllSynRNote();
//        CADBManager.getInstance().deleteAllRDose(DoseLogAdapter.TYPE_ASSESSMENT_INJECTION);
//        CADBManager.getInstance().deleteAllRDose(DoseLogAdapter.TYPE_ASSESSMENT_OTHER);
        for (int i = 0; i < mListSaveData.size(); i++) {
            CADBManager.getInstance().addRDoseEntity(mListSaveData.get(i));
        }

        mListSaveData.clear();
    }

    /**
     * This method will save info of the last used of Tresiba Dose item.
     *
     * @param listDataInfo ArrayList<DataInfo>
     */
    private void saveLastTresibaDose(ArrayList<DataInfo> listDataInfo) {
        for (int i = 0; i < listDataInfo.size(); i++) {
            DataInfo item = listDataInfo.get(i);
            if (item.getResult().getAssessmentType().equals(DoseLogAdapter.TYPE_ASSESSMENT_INJECTION)) {
                if (item.getResult().getCustom3().equals(DeviceEntity.CODE_TRESIBA_100 + "")
                        || item.getResult().getCustom3().equals(DeviceEntity.CODE_TRESIBA_200 + "")) {
                    CommonUtil.getInstance().savePrefContent(CommonUtil.LAST_TRESIBA_DOSE, item.getResult().getReportedDate());
                    return;
                }
            }
        }
    }

    /**
     * Make a call request to the Assessment service to get all assessment result which type is Devices
     */
    private void callAS05ForDeviceList() {
        CommonUtil.wtfi(TAG, "callAS05ForDevicesList...");
        if (getStorage().getM001CarePlanUserEntity() == null
                || getStorage().getM007UserEntity() == null
                || getStorage().getM001CarePlanUserEntity().getData() == null
                || getStorage().getM001CarePlanUserEntity().getData().isEmpty()
                || getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments() == null
                || getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments().isEmpty()) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }

        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(KEY_API_AS05_FOR_DEVICE_LIST);

        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }

        CommonUtil.wtfi(TAG, LOG_TEXT_TOKEN + token);

        String currentDate = CommonUtil.getDateNow(CommonUtil.DATE_STYLE);
        currentDate = NVDateUtils.dateToStringUniversal(NVDateUtils.stringToDateUniversal(currentDate, CommonUtil.DATE_STYLE), CommonUtil.DATE_STYLE);

        String registrationDate = getStorage().getM007UserEntity().getRegistrationDate();
        registrationDate = registrationDate.replace("Z", "+0000");

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_AS05_GET_RESULT, getDeviceAssessmentId(), registrationDate, currentDate));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addQueryParams(P_SIZE, "1000000");
        request.addQueryParams(P_SORT, "reportedDate,desc");

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Make a set of call request to get all Injections, Others & Note assessment
     */
    public void doAllAS05GetResult() {
        CommonUtil.wtfe(TAG, "doAllAS05GetResult...");
        String stateCallAPI = CommonUtil.getInstance().getPrefContent(KEY_SAVE_CALLING_AS5);
        if (stateCallAPI != null && stateCallAPI.equals("true")) {
            mListener.getAS5NoteReady();
            return;
        }

        CommonUtil.getInstance().savePrefContent(KEY_SAVE_CALLING_AS5, "true");

        mListSaveData.clear();
        String key = CommonUtil.getInstance().getPrefContent(KEY_APP_NOTE_ADD_DELETE);
        if (key != null && key.equals("true")) {
            //for note
//            CADBManager.getInstance().deleteAllADDRNote(DoseLogAdapter.TYPE_ASSESSMENT_NOTE,
//                    CommonUtil.getInstance().getPrefContent(KEY_APP_NOTE_ADD_DELETE_TIME));

            CADBManager.getInstance().changeStatusFlagAllADDRNote(DoseLogAdapter.TYPE_ASSESSMENT_NOTE,
                    CommonUtil.getInstance().getPrefContent(KEY_APP_NOTE_ADD_DELETE_TIME));
            //for dose
            CADBManager.getInstance().deleteAllADDRNote(DoseLogAdapter.TYPE_ASSESSMENT_INJECTION,
                    CommonUtil.getInstance().getPrefContent(KEY_APP_DOSE_ADD_DELETE_TIME));

            CommonUtil.getInstance().clearPrefContent(KEY_APP_NOTE_ADD_DELETE);
            CommonUtil.getInstance().clearPrefContent(KEY_APP_NOTE_ADD_DELETE_TIME);
            CommonUtil.getInstance().clearPrefContent(KEY_APP_DOSE_ADD_DELETE);
            CommonUtil.getInstance().clearPrefContent(KEY_APP_DOSE_ADD_DELETE_TIME);
        }

        mIndex = 0;
        mTimeCount = getTimesCount();
        String lastTime2 = CommonUtil.getInstance().getPrefContent(KEY_TIME2_LAST);

        long timeInt1 = CommonUtil.getDateNowInt(CommonUtil.DATE_STYLE, lastTime2, 90);
        while (timeInt1 <= System.currentTimeMillis()) {
            String time1 = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, lastTime2, 90);

            for (int i = 0; i < getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments().size(); i++) {
                String assessmentType = getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments().get(i).getType();
                if (!assessmentType.equals(EXCLUDE_TYPE)) callAS05GetResult(i, lastTime2, time1);
            }

            lastTime2 = time1;
            timeInt1 = CommonUtil.getDateNowInt(CommonUtil.DATE_STYLE, lastTime2, 90);
        }

        String time1 = CommonUtil.getDateNow(CommonUtil.DATE_STYLE);
        for (int i = 0; i < getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments().size(); i++) {
            String assessmentType = getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments().get(i).getType();
            if (!assessmentType.equals(EXCLUDE_TYPE)) callAS05GetResult(i, lastTime2, time1);
        }

        //for device
        callAS05ForDeviceList();
    }

    /**
     * @return the number of call request need to make to get all dose log data
     */
    private int getTimesCount() {
        int count = 1;
        String lastTime2 = CommonUtil.getInstance().getPrefContent(KEY_TIME2_LAST);
        long timeInt1 = CommonUtil.getDateNowInt(CommonUtil.DATE_STYLE, lastTime2, 90);
        while (timeInt1 <= System.currentTimeMillis()) {
            count++;

            lastTime2 = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, lastTime2, 90);
            timeInt1 = CommonUtil.getDateNowInt(CommonUtil.DATE_STYLE, lastTime2, 90);
        }
        return count;
    }

    /**
     * Make a call request to the Assessment service to get the all the dose log data in a time range
     *
     * @param index is the index of the call request
     * @param time1 is the start date of the time range
     * @param time2 is the end date of the time range
     */
    private void callAS05GetResult(int index, String time1, String time2) {
        CommonUtil.wtfi(TAG, "callAS05GetResult..." + time1 + "         :         " + time2);
        if (getStorage().getM001CarePlanUserEntity() == null
                || getStorage().getM001CarePlanUserEntity().getData() == null
                || getStorage().getM001CarePlanUserEntity().getData().isEmpty()
                || getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments() == null
                || getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments().isEmpty()) {
            mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }

        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(KEY_API_AS05_GET_RESULT);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, LOG_TEXT_TOKEN + token);
        CarePlantUserEntity.AssessmentInfo item = getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments().get(index);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);

        request.addPathSegment(String.format(URL_API_AS05_GET_RESULT, item.getAssessmentId(), time1, time2));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addQueryParams(P_SIZE, "1000000");
        request.addQueryParams(P_SORT, "reportedDate,desc");

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * @return the list dose log data that need to be displayed
     */
    public ArrayList<DataInfo> getListDataInfo() {
        return mListDataInfo;
    }

    /**
     * Handle the behaviour when the user want to load more data from the back end
     */
    public void loadMore() {
        mListDataInfo.clear();
        initTime(true);
        doAllAS05GetResult();
    }

    /**
     * Handle each unsuccessful request
     *
     * @see BasePresenter#doFailed(String, Exception, int, String)
     */
    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tag);

        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        String sms = LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON);
        if (responseEntity != null && responseEntity.getErrorEntity() != null && responseEntity.getErrorEntity().length > 0) {
            sms = responseEntity.getErrorEntity()[0].getMessage();
        }
        if (sms == null && !CommonUtil.getInstance().isConnectToNetwork()) {
            sms = CAApplication.getInstance().getString(R.string.txt_error_network);
        }

        switch (tag) {
            case KEY_API_NNDM_SYSTEM_ID_GET_DECRYPTION:
                ProgressLoading.dismiss();
                break;
            case KEY_API_AS05_FOR_DEVICE_LIST:
                mListener.showAlertDialog("Error devices: " + sms);
                break;

            case KEY_API_AS05_GET_RESULT:
                mIndex++;
                //For done
                if (mIndex % (mTimeCount * (getStorage().getM001CarePlanUserEntity().getData().get(0).getAssessments().size() - 1)) == 0) {
                    CommonUtil.getInstance().clearPrefContent(KEY_SAVE_CALLING_AS5);
                    ProgressLoading.dismiss();
                    mListener.showAlertDialog(sms);
                    mListener.dataAS05Ready();
                }
                break;
            default:
                break;
        }
        mListener.closeRefresh();
    }

    /**
     * Clear the index of the call request and list dose log data to start refresh dose log data
     */
    public void resetData() {
        mIndex = 0;
        mListDataInfo.clear();
    }

    /**
     * Decide when to close the spinning progress bar
     *
     * @param key is the key of the success response that will trigger this action
     */
    @Override
    public void hideLockDialog(String key) {
        if (key.equals(KEY_API_AS05_GET_RESULT) && !getStorage().isCallLoggedOut()) {
            ProgressLoading.dismiss();
        }
    }
}