/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM038RecoveryCallBack;
import com.novo.app.view.widget.ProgressLoading;

import static com.novo.app.utils.CommonUtil.P_API_KEY;

public class M038RecoveryPresenter extends BasePresenter<OnM038RecoveryCallBack> {
    private static final String URL_API_IA04_RESET_PASSWORD_STEP_1 = "user-service/api/v1/reset-password?";
    private static final String KEY_API_IA04_RESET_PASSWORD_STEP_1 = "KEY_API_IA04_RESET_PASSWORD_STEP_1";
    private static final String BODY_IA04 = "{\"action\": \"initiateReset\",\"userName\": %s,\"delivery\": \"SMTP\"}";
    private static final String TAG = M038RecoveryPresenter.class.getName();

    /**
     * The constructor of M038RecoveryPresenter
     *
     * @param event is the interface listen to this class callback
     */
    public M038RecoveryPresenter(OnM038RecoveryCallBack event) {
        super(event);
    }

    /**
     * Handle each successful request
     *
     * @see BasePresenter#handleSuccess(String, String)
     */
    @Override
    protected void handleSuccess(String data, String tag) {
        CommonUtil.wtfi(TAG, "DATA: " + data);
        if (tag.equals(KEY_API_IA04_RESET_PASSWORD_STEP_1)) {
            mListener.showM039RecoveryCodeSent();
        }
    }

    /**
     * Handle each unsuccessful request
     *
     * @see BasePresenter#doFailed(String, Exception, int, String)
     */
    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tagRequest);
        ProgressLoading.dismiss();

        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        if (responseEntity != null && tag.equals(KEY_API_IA04_RESET_PASSWORD_STEP_1)) {
            String responseCode = Integer.toString(code);
            if (responseCode.equals(CommonUtil.ERROR_400)) {
                mListener.onIA04Fail();
            } else {
                showNotify(LangMgr.getInstance().getLangList().get(ERR_500));
            }
        } else if (!CommonUtil.getInstance().isConnectToNetwork()) {
            showNotify(LangMgr.getInstance().getLangList().get(ERR_NO_CONNECTION));
        }
    }

    /**
     * Make a request to the backend service to send a confirmation email to the email that the user
     * has inputted
     *
     * @param userName is the email that the user has inputted or pre - filed by previous login
     */
    public void callIA04ResetPasswordStep1(String userName) {
        CommonUtil.wtfi(TAG, "callIA04ResetPasswordStep1");

        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_IA04_RESET_PASSWORD_STEP_1);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addPathSegment(URL_API_IA04_RESET_PASSWORD_STEP_1);
        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());

        request.addBody(generateJson(BODY_IA04, userName));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

}
