package com.novo.app.view.event;

public interface OnM043SignUpDidntGetCallBack extends OnCallBackToView{
    void showM013SignUpCode();

    void verifyEmailSuccess(String success);

    void verifyEmailFail(String message);
}
