package com.novo.app.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.view.event.OnM033PairSuccessCallBack;

import java.util.List;

public class M033PairedDevicesAdapter extends BaseRecycleAdapter<OnM033PairSuccessCallBack, DeviceEntity, M033PairedDevicesAdapter.DeviceHolder> {

    public M033PairedDevicesAdapter(Context mContext, List<DeviceEntity> mListData, OnM033PairSuccessCallBack mCallBack) {
        super(mContext, mListData, mCallBack);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_m033_paired_device;
    }

    @Override
    protected DeviceHolder getViewHolder(int viewType, View itemView) {
        return new DeviceHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        DeviceHolder item = (DeviceHolder) holder;
        DeviceEntity data = mListData.get(position);
        switch (data.getDrugType()) {
            case DeviceEntity.TRESIBA:
                item.tvDrug.setText(DeviceEntity.TRESIBA);
                if (data.getDrugCode() == DeviceEntity.CODE_TRESIBA_100) {
                    item.ivDrug.setImageResource(R.drawable.ic_tresiba);
                } else {
                    item.ivDrug.setImageResource(R.drawable.ic_m012_drug_t);
                }
                break;
            case DeviceEntity.FIASP:
                item.tvDrug.setText(DeviceEntity.FIASP);
                item.ivDrug.setImageResource(R.drawable.ic_fiasp);
                break;
            case DeviceEntity.NO_DOSE:
                item.tvDrug.setText(DeviceEntity.NO_DOSE);
                item.ivDrug.setVisibility(View.GONE);
                break;
            default:
                item.tvDrug.setText(DeviceEntity.UNKNOWN);
                item.ivDrug.setVisibility(View.GONE);
                break;
        }
    }

    public class DeviceHolder extends BaseHolder {
        TextView tvDrug;
        ImageView ivDrug;

        DeviceHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void initView() {
            findViewById(R.id.tv_m033_detected, CAApplication.getInstance().getBoldFont());
            tvDrug = findViewById(R.id.tv_m033_drug, CAApplication.getInstance().getBoldFont());
            ivDrug = findViewById(R.id.iv_m033_drug);
        }

    }

}