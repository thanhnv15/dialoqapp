/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.view.fragment;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.Editable;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M040RecoveryPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseEditText;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM040RecoveryCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.TextAdapter;

import java.util.Objects;

/**
 * Recovery Password Fragment
 * <p>
 * This fragment is 'View' object in MVP pattern
 * This fragment uses an {@link M040RecoveryPresenter} as linked Presenter
 * Callbacks to this fragment will be defined by {@link OnM040RecoveryCallBack} interface
 * <p>
 * This fragment is used in Reset password features of dialog app, where user has to input
 * a new password
 */
public class M040RecoveryPasswordFrg extends BaseFragment<M040RecoveryPresenter, OnHomeBackToView> implements OnM040RecoveryCallBack {
    public static final String TAG = M040RecoveryPasswordFrg.class.getName();

    // Minimum length of a valid password
    private static final int MIN = 8;

    // View items used in Recovery Email Fragment
    private Button mBtSave;
    private BaseEditText mEdtPass;
    private TextView mTvShow;
    private TextView mTvError;
    private ImageView mIvError;

    /**
     * Handle callback from {@link M040RecoveryPresenter}
     * {@link OnHomeBackToView#showM001LandingScreen(String err)}
     *
     * @param message error string description
     */
    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    /**
     * Handle callback from {@link OnM040RecoveryCallBack}
     * {@link OnM040RecoveryCallBack#loginSuccess()}
     */
    @Override
    public void loginSuccess() {
        getStorage().setM038UserName("");
        getStorage().setM039Verify("");
        CommonUtil.getInstance().showDialog(mContext, getString(R.string.txt_m025_reset), null);
        mCallBack.showFrgScreen(TAG, M024DoseLogFrg.TAG);
        mCallBack.startScan();
        CAApplication.getInstance().startSyncService();
    }

    /**
     * Handle callback from {@link OnM040RecoveryCallBack}
     * {@link OnM040RecoveryCallBack#showDialog(int, String)}
     *
     * @param textCode is the resource code of the String message to be displayed
     * @param tag      is the tag code of the error
     */
    @Override
    public void showDialog(int textCode, String tag) {
        showDialog(getString(textCode), tag);
    }

    /**
     * Handle callback from {@link OnM040RecoveryCallBack}
     * {@link OnM040RecoveryCallBack#showDialog(String, String)}
     *
     * @param text is the string message to be displayed
     * @param tag  is the tag code of the error
     */
    @Override
    public void showDialog(String text, String tag) {
        CommonUtil.getInstance().showDialog(mContext, text, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton1() {
                if (tag.equals(M040RecoveryPresenter.KEY_API_IA02_AUTHENTICATE_USER)) {
                    mCallBack.showFrgScreen(tag, M023LoginFrg.TAG);
                }
                if (tag.equals(M040RecoveryPresenter.KEY_BLOCKED_BY_COUNTRY)) {
                    getStorage().setAppActive(true);
                    mCallBack.showFrgScreen(tag, M001LandingPageFrg.TAG);
                }
            }
        });
    }

    /**
     * Initiate views component for Recovery Email Fragment
     *
     * @see BaseFragment#initViews()
     */
    @Override
    protected void initViews() {
        findViewById(R.id.tv_m040_account_recovery, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m040_description, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m040_error, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m040_cancel, this, CAApplication.getInstance().getBoldFont());

        mBtSave = findViewById(R.id.bt_m040_save, this, CAApplication.getInstance().getRegularFont());
        mBtSave.setEnabled(false);

        findViewById(R.id.tr_m040_error, this, CAApplication.getInstance().getRegularFont());
        mTvError = findViewById(R.id.tv_m040_error);
        mTvError.setTextColor(getResources().getColor(R.color.colorBlack));
        mIvError = findViewById(R.id.ic_m040_error);
        mIvError.setVisibility(View.GONE);

        mEdtPass = findViewById(R.id.tv_m040_password, CAApplication.getInstance().getRegularFont());
        onPasswordChange();
        onPasswordFocusChange();
        onKeyBoardDismiss();
        mTvShow = findViewById(R.id.tv_m040_show, this, CAApplication.getInstance().getRegularFont());
    }

    /**
     * Reset layout when keyboard dismissed
     */
    private void onKeyBoardDismiss() {
        mEdtPass.setOnEditTextImeBackListener((ctrl, text) -> {
            mEdtPass.clearFocus();
        });

        mEdtPass.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEdtPass.clearFocus();
                CommonUtil.getInstance().forceHideKeyBoard(mEdtPass);
            }
            return false;
        });
    }

    /**
     * Reset layout when the focus of {@link #mEdtPass} changed
     */
    private void onPasswordFocusChange() {
        mEdtPass.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                String pass = Objects.requireNonNull(mEdtPass.getText()).toString();
                validatePass(pass);
            } else {
                showPassError(false);
                mBtSave.setEnabled(false);
            }
        });
    }

    /**
     * Reset layout when input text of {@link #mEdtPass} changed
     */
    private void onPasswordChange() {
        mEdtPass.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                CommonUtil.getInstance().initTypeFace(mEdtPass);
                validatePass(mEdtPass.getText().toString());
                showPassError(false);
            }
        });
    }

    /**
     * Check if a password valid/invalid according password rule
     * Also UI will be updated according to valid/invalid password
     *
     * @param pass password to be checked (in {@link String})
     */
    private void validatePass(String pass) {
        mBtSave.setEnabled(false);
        boolean isIncorrectPassword = pass.length() < getStorage().getM001ConfigSet().getMinPass();
        showPassError(isIncorrectPassword);
        mBtSave.setEnabled(!isIncorrectPassword);
    }

    /**
     * Show or hide password error layout
     *
     * @param show true if password error layout should be shown
     */
    private void showPassError(boolean show) {
        if (show) {
            mEdtPass.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
            mTvError.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
            mIvError.setVisibility(View.VISIBLE);
        } else {
            mEdtPass.setTextColor(getResources().getColor(R.color.colorBlack));
            mTvError.setTextColor(getResources().getColor(R.color.colorBlack));
            mIvError.setVisibility(View.GONE);
        }
    }

    /**
     * Handle callback from {@link OnM040RecoveryCallBack}
     * {@link OnM040RecoveryCallBack#onIA04Fail(String sms)}
     *
     * @param message is the message to show in the dialog
     */
    @Override
    public void onIA04Fail(String message) {
        CommonUtil.getInstance().showDialog(mContext, message, null);
    }

    /**
     * Get layout id for Recovery Email Fragment
     *
     * @return layout id
     * @see BaseFragment#getLayoutId()
     */
    @Override
    protected int getLayoutId() {
        return R.layout.frg_m040_recovery_password;
    }

    /**
     * Get presenter for this fragment
     *
     * @return {@link M040RecoveryPresenter} instance
     * @see BaseFragment#getPresenter()
     */
    @Override
    protected M040RecoveryPresenter getPresenter() {
        return new M040RecoveryPresenter(this);
    }

    /**
     * @return tag in {@link String}
     * @see BaseFragment#getTAG()
     */
    @Override
    protected String getTAG() {
        return TAG;
    }

    /**
     * @see BaseFragment#defineBackKey()
     */
    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    /**
     * @param idView id of clicked view
     * @see BaseFragment#onClickView(int)
     */
    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m040_show:
                if (mTvShow.getText().toString().equals(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m023_show)))) {
                    mEdtPass.setInputType(InputType.TYPE_CLASS_TEXT);
                    mTvShow.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m023_hide)));

                } else {
                    mEdtPass.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    mEdtPass.setSelection(mEdtPass.getText().length());
                    mTvShow.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m023_show)));
                }
                CommonUtil.getInstance().initTypeFace(mEdtPass);
                validatePass(mEdtPass.getText().toString());
                break;

            case R.id.bt_m040_save:
                mEdtPass.clearFocus();
                CommonUtil.getInstance().forceHideKeyBoard(mEdtPass);
                mPresenter.callIA04ResetPasswordStep3(getStorage().getM038UserName(), getStorage().getM039VerifyCode(), mEdtPass.getText().toString());
                break;

            case R.id.tv_m040_cancel:
                mEdtPass.clearFocus();
                CommonUtil.getInstance().forceHideKeyBoard(mEdtPass);
                mCallBack.showFrgScreen(TAG, M023LoginFrg.TAG);
                break;

            default:
                break;
        }
    }

    /**
     * Handle callback from {@link OnM040RecoveryCallBack}
     *
     * @see OnM040RecoveryCallBack#showM001LandingFrg
     */
    @Override
    public void showM001LandingFrg(int recallCode) {
        switch (recallCode) {
            case CommonUtil.RECALL_COUNTRY_VERSION:
                String versionApp = "";
                try {
                    PackageInfo pInfo = CAApplication.getInstance().getPackageManager().getPackageInfo(CAApplication.getInstance().getPackageName(), 0);
                    versionApp = pInfo.versionName;

                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                String country = getStorage().getM007UserEntity().getCountry();
                String textVersion = getString(R.string.txt_m023_recall_country_version).replace("${dialoq_version}", versionApp).replace("${country_name}", country);

                CommonUtil.getInstance().showDialog(mContext, textVersion, new OnOKDialogCallBack() {
                    @Override
                    public void handleOKButton1() {
                        mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
                    }
                });
                getStorage().setAppActive(false);
                mCallBack.cancelReminder();
                break;
            case CommonUtil.RECALL_COUNTRY:
                String recallCountry = getStorage().getM007UserEntity().getCountry();
                String textCountry = getString(R.string.txt_m023_recall_country).replace("${country_name}", recallCountry);
                CommonUtil.getInstance().showDialog(mContext, textCountry, new OnOKDialogCallBack() {
                    @Override
                    public void handleOKButton1() {
                        mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
                    }
                });
                getStorage().setAppActive(false);
                mCallBack.cancelReminder();
                break;
            default:
                break;
        }
    }
}
