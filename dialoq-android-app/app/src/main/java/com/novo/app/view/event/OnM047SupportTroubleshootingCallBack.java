package com.novo.app.view.event;

public interface OnM047SupportTroubleshootingCallBack extends OnCallBackToView{
    void toSingleQA();

    void toMultipleQA();
}
