package com.novo.app.view.widget.novocharts.datasets;

import com.github.mikephil.charting.data.BarLineScatterCandleBubbleData;

import java.util.List;

public class BarDetailsData extends BarLineScatterCandleBubbleData<IBarDetailsDataSet> {

    private float mBarWidth = 0.85f;

    public BarDetailsData(List<IBarDetailsDataSet> dataSets) {
        super(dataSets);
    }

    public float getBarWidth() {
        return mBarWidth;
    }

    public void setBarWidth(float mBarWidth) {
        this.mBarWidth = mBarWidth;
    }
}
