/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.presenter;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RReminderEntity;
import com.novo.app.manager.entities.RTokenEntity;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.ConfigEntity;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.model.entities.TokenEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.widget.M023LoginAdapter;
import com.novo.app.view.widget.NVDateFormat;
import com.novo.app.view.widget.NVDateUtils;
import com.novo.app.view.widget.ProgressLoading;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.novo.app.manager.LangMgr.LANGUAGE_FILE;
import static com.novo.app.utils.CommonUtil.P_API_KEY;
import static com.novo.app.utils.CommonUtil.RECALL_EQUAL;
import static com.novo.app.utils.CommonUtil.RECALL_GREATER;
import static com.novo.app.utils.CommonUtil.RECALL_GREATER_OR_EQUAL;
import static com.novo.app.utils.CommonUtil.RECALL_LESS;
import static com.novo.app.utils.CommonUtil.RECALL_LESS_OR_EQUAL;

public class HomePresenter extends M025UpdatingPresenter<OnHomeBackToView> {
    public static final String KEY_ACCEPTED_ADULT_WARNING = "KEY_ACCEPTED_ADULT_WARNING";
    private static final String KEY_API_CONFIG_SET = "KEY_API_CONFIG_SET";
    private static final String KEY_API_VERSION_CONFIG_SET = "KEY_API_VERSION_CONFIG_SET";
    private static final String URL_API_CONFIG_SET = "configuration-service/api/v1/configurations/config-sets/public/DIALOG_COMMON/latest?";
    private static final String P_DETAILS = "details";
    private static final String KEY_API_LANGUAGE_SET = "KEY_API_LANGUAGE_SET";
    private static final String URL_API_LANGUAGE_SET = "configuration-service/api/v1/configurations/config-sets/public/dialog_%s/latest?";
    private static final String KEY_API_AS04_CREATE_ASSESSMENT_RESULT = "KEY_API_AS04_CREATE_ASSESSMENT_RESULT";
    private static final String URL_API_AS4_CREATE_A_RESULT_OF_AN_ASSESSMENT = CommonUtil.HOT_URL + "/api/v1/users/%s/assessments/%s/results?";
    private static final String BODY_AS4 = "{\"activityId\": \"\",\"assessmentType\": \"Others\",\"reportedDate\": %s,\"custom1\": %s, \"custom2\": %s, \"custom3\": %s}";
    private static final String KEY_REMINDER_PAUSED = "DOSELOG.LIST.SCREEN.ITEM.ERROR.REMINDER.PAUSED";
    private static final String KEY_TERM_FOR_ADULT = "TERM.ADULT";

    private static final String TAG = HomePresenter.class.getName();

    private int langNumber = 0;
    private boolean isLoadingConfigSet;
    private TextEntity mIFU;
    private TextEntity mIntroVideo;


    public HomePresenter(OnHomeBackToView event) {
        super(event);
    }

    public void getConfigSet() {
        if (isLoadingConfigSet) return;
        isLoadingConfigSet = true;

        boolean isCheckVersion = true;
        RTokenEntity data = CADBManager.getInstance().getRTokenEntity();
        if (data == null || data.getConfigSet() == null || data.getConfigSet().isEmpty()) {
            isCheckVersion = false;
        }

        CommonUtil.wtfi(TAG, "getConfigSet...isCheckVersion: " + isCheckVersion);
        getConfigSet(isCheckVersion);
    }

    private void getConfigSet(boolean isCheckVersion) {
        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(isCheckVersion ? KEY_API_VERSION_CONFIG_SET : KEY_API_CONFIG_SET);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addPathSegment(URL_API_CONFIG_SET);

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addQueryParams(P_DETAILS, isCheckVersion ? "false" : "true");

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * <b>loadingLoginOff</b><br/>
     * <br/>UserInfo will be loaded from RealmDB.
     * <br/>If the token data is null, LandingPage screen will be shown.
     * <br/>If the token data is not null, call back to the loginSuccess method
     *
     * @see OnHomeBackToView#loginSuccess()
     * @see OnHomeBackToView#showLandingFrgScreen()
     */
    public void loadingLoginOff() {
        RTokenEntity data = CADBManager.getInstance().getRTokenEntity();
        if (data == null || data.getConfigSet() == null || data.getConfigSet().isEmpty()) {
            mListener.showLandingFrgScreen();
            return;
        }

        TokenEntity tokenEntity = generateData(TokenEntity.class, data.getDataIA02());
        CommonUtil.getInstance().savePrefContent(CommonUtil.TOKEN_KEY, tokenEntity.getToken());
        UserEntity entity = generateData(UserEntity.class, data.getDataUN07());
        getStorage().setM007UserEntity(entity);

        entity = generateData(UserEntity.class, data.getDataUN09());
        getStorage().setM007UserEntity(entity);
        M023LoginPresenter presenter = new M023LoginPresenter(new M023LoginAdapter());

        if (!presenter.checkBlacklistCountry(CommonUtil.IN_CASE_OFFLINE_MODE)) {
            CarePlantUserEntity carePlantUserEntity = generateData(CarePlantUserEntity.class, data.getDataCP03());
            getStorage().setM001CarePlanUserEntity(carePlantUserEntity);
            mListener.loginSuccess();
        }
    }

    /**
     * <b>handleSuccess</b><br/>
     * Handle cases:
     * <br/>1. Check version of the config set
     * <br/>2. Reload data of the config set
     * <br/>3. Pause Reminder
     * <br/>4. Reload the language data
     * @see BasePresenter#handleSuccess(String, String)
     * @see HomePresenter#checkVersionConfig(String)
     * @see HomePresenter#prepareConfigSet(String)
     * @see OnHomeBackToView#pauseReminder()
     * @see HomePresenter#prepareLanguage(String)
     * @param data response data from server
     */
    @Override
    protected void handleSuccess(String data, String tag) {
        CommonUtil.wtfi(TAG, "DATA: " + data);
        switch (tag) {
            case KEY_API_VERSION_CONFIG_SET:
                checkVersionConfig(data);
                break;
            case KEY_API_CONFIG_SET:
                getStorage().setConfigSetData(data);
                prepareConfigSet(data);
                break;
            case KEY_API_AS04_CREATE_ASSESSMENT_RESULT:
                mListener.pauseReminder();
                break;
            default:
                prepareLanguage(data);
                break;
        }
    }

    private synchronized void prepareLanguage(String data) {
        String key = tagRequest.replaceAll(KEY_API_LANGUAGE_SET, "");
        if (data != null && !data.isEmpty()) {
            CommonUtil.getInstance().saveObjData(data, LANGUAGE_FILE + key);
        }

        ConfigEntity configEntity = generateData(ConfigEntity.class, data);
        configEntity.loadConfigSet();
        mIFU = configEntity.getmIFU();
        mIntroVideo = configEntity.getmIntroVideo();
        checkDoneLoadLanguageList(key);
    }

    private void checkDoneLoadLanguageList(String key) {
        String langKey = getStorage().getLanguageKeyToGet();
        if (langKey.equals(key)) {
            String langValue = getStorage().getM001ConfigSet().getDefaultLanguage().getValue();
            CommonUtil.getInstance().savePrefContent(CommonUtil.LANG_KEY, langKey);
            CommonUtil.getInstance().savePrefContent(CommonUtil.LANG_VALUE, langValue);
            mListener.dataLanguageReady(key);
        }
    }

    private void checkVersionConfig(String data) {
        try {
            RTokenEntity tokenEntity = CADBManager.getInstance().getRTokenEntity();
            if (tokenEntity == null || tokenEntity.getConfigSet() == null ||  tokenEntity.getConfigSet().isEmpty()) {
                getConfigSet(false);
                return;
            }

            ConfigEntity configOld = generateData(ConfigEntity.class, tokenEntity.getConfigSet());
            ConfigEntity configNew = generateData(ConfigEntity.class, data);

            if (configNew.getVersion().compareTo(configOld.getVersion()) > 0) {
                getConfigSet(false);
                return;
            }

            prepareConfigSet(tokenEntity.getConfigSet());
        } catch (Exception e) {
            ProgressLoading.dismiss();
            e.printStackTrace();
        }
    }

    public void prepareConfigSet(String data) {
        CommonUtil.wtfi(TAG, "prepareConfigSet..." + data);
        ConfigEntity configEntity = generateData(ConfigEntity.class, data);
        if (configEntity == null) {
            showNotify(R.string.txt_get_config_err);
            mListener.loadConfigFail();
            return;
        }
        getStorage().setM001ConfigSet(configEntity);

        configEntity.loadConfigSet();
        //saveDefaultLang();
        mListener.loadConfigSuccess(configEntity);
        isLoadingConfigSet = false;
    }

    private void prepareAS10() {
        CommonUtil.wtfi(TAG, "prepareAS10...");
        RReminderEntity entity = CADBManager.getInstance().getRReminderEntity();
        if (entity == null) {
            mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            ProgressLoading.dismiss();
            return;
        }

        getStorage().setM065ScheduleEntity(entity);
        callAS4ResultOfAssessment();
    }


    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tagRequest + " " + tag);

        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        String sms = LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON);
        if (responseEntity != null && responseEntity.getErrorEntity()[0] != null) {
            sms = responseEntity.getErrorEntity()[0].getMessage();
        }
        if (KEY_API_CONFIG_SET.equals(tag)) {
            showNotify(sms);
            mListener.loadConfigFail();
            ProgressLoading.dismiss();
            isLoadingConfigSet = false;
        } else if (tag.contains(KEY_API_LANGUAGE_SET)) {
            ProgressLoading.dismiss();
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            mListener.getLanguageError();
        }
    }

    public void filterLanguage() {
        String langKey = CommonUtil.getInstance().getPrefContent(CommonUtil.LANG_KEY);
        if (langKey == null) {
            //Load default language for first time using app
            langKey = getStorage().getM001ConfigSet().getDefaultLanguage().getKey();
            getStorage().setLanguageKeyToGet(langKey);
        } else {
            if (getStorage().getLanguageKeyToGet() != null && !getStorage().getLanguageKeyToGet().equals(langKey)) {
                langKey = getStorage().getLanguageKeyToGet();
            }
        }
        doGetLanguage(langKey);
    }

    private void doGetLanguage(String key) {
        CommonUtil.wtfi(TAG, "getting language...");
        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(KEY_API_LANGUAGE_SET + key);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addPathSegment(String.format(URL_API_LANGUAGE_SET, key));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addQueryParams(P_DETAILS, "true");

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    public void checkBlockApp() {
        CommonUtil.wtfi(TAG, "checkBlockApp...........");
        ConfigEntity configSet = getStorage().getM001ConfigSet();

        //block all
        if (configSet == null) {
            mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(ERR_500));
            return;
        }

        if (configSet.getBlackList().isAll()) {
            getStorage().setAppActive(false);
            mListener.checkBlockApp(CommonUtil.BLACK_LIST);
            return;
        }

        String model = Build.MODEL.replace(" ", "");
        String release = Build.VERSION.RELEASE;
        String versionApp = "";
        try {
            PackageInfo pInfo = CAApplication.getInstance().getPackageManager().getPackageInfo(CAApplication.getInstance().getPackageName(), 0);
            versionApp = pInfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String textCompare = String.format("%s=%s", model, release).toUpperCase();
        String textCompareAllModel = String.format("%s=all", model).toUpperCase();
        String textCompareAllOS = String.format("all=%s", release).toUpperCase();

        CommonUtil.wtfi(TAG, "textCompare:..." + textCompare);
        CommonUtil.wtfi(TAG, "textCompareAllModel:..." + textCompareAllModel);
        CommonUtil.wtfi(TAG, "textCompareAllOS:..." + textCompareAllOS);

        if (configSet.getBlackList() != null && configSet.getBlackList().getDevices() != null
                && (configSet.getBlackList().getDevices().contains(textCompareAllModel)
                || configSet.getBlackList().getDevices().contains(textCompare)
                || configSet.getBlackList().getDevices().contains(textCompareAllOS))) {
            mListener.checkBlockApp(CommonUtil.BLACK_LIST);
            getStorage().setAppActive(false);
            CommonUtil.wtfi(TAG, "This device is in the black list");
            return;
        }

        for (int i = 0; i < getStorage().getM001ConfigSet().getVersionEntity().size(); i++) {
            String versionConfigset = configSet.getVersionEntity().get(i).getVersionName().getValue();
            String comparisonMode = configSet.getVersionEntity().get(i).getVersionComparison().getValue();
            CommonUtil.wtfi(TAG, "versionConfigset:..." + versionConfigset);

            switch (comparisonMode) {
                case CommonUtil.RECALL_GREATER:
                    if (gotoLandingPage((versionApp.compareTo(versionConfigset) > 0))) return;
                    break;
                case CommonUtil.RECALL_EQUAL:
                    if (gotoLandingPage((versionApp.compareTo(versionConfigset) == 0))) return;
                    break;
                case CommonUtil.RECALL_LESS:
                    if (gotoLandingPage((versionApp.compareTo(versionConfigset) < 0))) return;
                    break;
                case CommonUtil.RECALL_GREATER_OR_EQUAL:
                    if (gotoLandingPage((versionApp.compareTo(versionConfigset) >= 0))) return;
                    break;
                case CommonUtil.RECALL_LESS_OR_EQUAL:
                    if (gotoLandingPage((versionApp.compareTo(versionConfigset) <= 0))) return;
                    break;
                default:
                    break;
            }
        }

        if (configSet.getWhiteList().getDevices().contains(textCompareAllModel)
                || configSet.getWhiteList().getDevices().contains(textCompare)
                || configSet.getWhiteList().getDevices().contains(textCompareAllOS)) {
            mListener.checkBlockApp(CommonUtil.WHITE_LIST);
            getStorage().setAppActive(true);
            CommonUtil.wtfi(TAG, "This device is in the white list");
            return;
        }

        CommonUtil.wtfi(TAG, "This device is in the grey list");
        checkCountryEntity();
    }

    private void checkCountryEntity() {
        UserEntity userEntity = getStorage().getM007UserEntity();
        if (userEntity != null) {
            checkBlacklistCountry();
        } else {
            mListener.checkBlockApp(CommonUtil.GREY_LIST);
        }
    }

    private void checkBlacklistCountry() {
        ConfigEntity configSet = getStorage().getM001ConfigSet();
        if (getStorage().getM007UserEntity().getCountry() == null) {
            CommonUtil.wtfi("CountryUser", "CountryUser is null");
        } else {
            String country = getStorage().getM007UserEntity().getCountry().toUpperCase();
            CommonUtil.wtfi("CountryUser", "CountryUser:" + country);
            CommonUtil.wtfi("BlacklistCountry", "Blacklist countries:" + getStorage().getM001ConfigSet().getBlacklistCountryEntity().get(0).getBlacklistCountry().getValue().toUpperCase());

            for (int i = 0; i < getStorage().getM001ConfigSet().getBlacklistCountryEntity().size(); i++) {
                String blacklistCountryConfigset = configSet.getBlacklistCountryEntity().get(i).getBlacklistCountry().getValue().toUpperCase();
                if (country.equals(blacklistCountryConfigset)) {
                    mListener.checkBlockApp(CommonUtil.RECALL_COUNTRY);
                    getStorage().setAppActive(false);
                    ProgressLoading.dismiss();
                    CommonUtil.wtfi(TAG, "This country is in the black list");
                    return;
                }
            }
        }
        checkCountryVersionEntity();
    }

    private void checkCountryVersionEntity() {
        UserEntity userEntity = getStorage().getM007UserEntity();
        if (userEntity != null) {
            setupUserCountry(userEntity.getCountry());
            checkCountryVersionLockApp();
        }
    }

    private void setupUserCountry(String userCountry) {
        for (int i = 0; i < getStorage().getM001ConfigSet().getCountryEntity().size(); i++) {
            if (getStorage().getM001ConfigSet().getCountryEntity().get(i).getCountryName().getValue().equals(userCountry)) {
                getStorage().setUserCountryEntity(getStorage().getM001ConfigSet().getCountryEntity().get(i));
                return;
            }
        }
    }

    private void checkCountryVersionLockApp() {
        ConfigEntity configSet = getStorage().getM001ConfigSet();
        String versionApp = "";
        try {
            PackageInfo pInfo = CAApplication.getInstance().getPackageManager().getPackageInfo(CAApplication.getInstance().getPackageName(), 0);
            versionApp = pInfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < getStorage().getM001ConfigSet().getCountryVersionEntity().size(); i++) {
            String countryConfigSet = configSet.getCountryVersionEntity().get(i).getCountryCode().getValue();
            String versionConfigSet = configSet.getCountryVersionEntity().get(i).getVersionName().getValue();
            String comparisonMode = configSet.getCountryVersionEntity().get(i).getVersionComparison().getValue();

            if (countryConfigSet.equals(getStorage().getUserCountryEntity().getCountryCode().getValue())) {
                switch (comparisonMode) {
                    case RECALL_GREATER:
                        if (gotoLandingPageCountry((versionApp.compareTo(versionConfigSet) > 0)))
                            return;
                        break;
                    case RECALL_EQUAL:
                        if (gotoLandingPageCountry((versionApp.compareTo(versionConfigSet) == 0)))
                            return;
                        break;
                    case RECALL_LESS:
                        if (gotoLandingPageCountry((versionApp.compareTo(versionConfigSet) < 0)))
                            return;
                        break;
                    case RECALL_GREATER_OR_EQUAL:
                        if (gotoLandingPageCountry((versionApp.compareTo(versionConfigSet) >= 0)))
                            return;
                        break;
                    case RECALL_LESS_OR_EQUAL:
                        if (gotoLandingPageCountry((versionApp.compareTo(versionConfigSet) <= 0)))
                            return;
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private boolean gotoLandingPage(boolean isBack) {
        if (!isBack) return false;
        mListener.checkBlockApp(CommonUtil.RECALL_VERSION);
        getStorage().setAppActive(false);
        CommonUtil.wtfi(TAG, "Check for recall version");
        mListener.cancelReminder();
        ProgressLoading.dismiss();
        return true;
    }

    private boolean gotoLandingPageCountry(boolean isBack) {
        if (!isBack) return false;
        mListener.checkBlockApp(CommonUtil.RECALL_COUNTRY_VERSION);
        getStorage().setAppActive(false);
        CommonUtil.wtfi(TAG, "Check for recall version");
        mListener.cancelReminder();
        ProgressLoading.dismiss();
        return true;
    }

    private void callAS4ResultOfAssessment() {
        CommonUtil.wtfi(TAG, "addNewNoteToDB");
        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_AS04_CREATE_ASSESSMENT_RESULT);

        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, "token: " + token);
        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);

        request.addPathSegment(String.format(URL_API_AS4_CREATE_A_RESULT_OF_AN_ASSESSMENT,
                getStorage().getM007UserEntity().getUserId() + "",
                getAssessmentId(CommonUtil.OTHERS_ASSESSMENT_TYPE) + ""));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());

        String newTimezone = TimeZone.getDefault().getID();
        request.addBody(generateJson(BODY_AS4, CommonUtil.getDateNow(CommonUtil.DATE_STYLE), System.currentTimeMillis(), newTimezone, KEY_REMINDER_PAUSED));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    @Override
    public void hideLockDialog(String key) {
        if (key.contains(KEY_API_LANGUAGE_SET))
            langNumber++;
        if (getStorage().getM001ConfigSet() != null && getStorage().getM001ConfigSet().getLanguageEntity() != null
                && key.contains(KEY_API_LANGUAGE_SET) && langNumber == getStorage().getM001ConfigSet().getLanguageEntity().size() - 1
                || !key.equals(KEY_API_CONFIG_SET)
                && !key.equals(KEY_API_VERSION_CONFIG_SET)
                && !key.contains(KEY_API_LANGUAGE_SET)) {
            langNumber = 0;
            super.hideLockDialog(key);
        }
    }

    public boolean checkForAdultUser() {
        boolean result = false;
        if (CADBManager.getInstance().getRUserProfile() != null) {
            if (!CADBManager.getInstance().getRUserProfile().getFamilyHistory().contains(KEY_TERM_FOR_ADULT)) {
                String dateOfBirth = CADBManager.getInstance().getRUserProfile().getDateOfBirth();
                if ((CommonUtil.getInstance().getPrefContent(KEY_ACCEPTED_ADULT_WARNING) == null
                        || !CommonUtil.getInstance().getPrefContent(KEY_ACCEPTED_ADULT_WARNING).equals("true"))
                        && isAdult(dateOfBirth)) {
                    result = true;
                }
            }
        }
        return result;
    }

    private boolean isAdult(String dateOfBirth) {
        int birthYear = Integer.parseInt(dateOfBirth.split("-")[0]);
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        String thisBirthDay = dateOfBirth.replace(String.valueOf(birthYear), String.valueOf(currentYear));
        String userCountry = CADBManager.getInstance().getRUserProfile().getCountry();

        for (int i = 0; i < getStorage().getM001ConfigSet().getCountryEntity().size(); i++) {
            if (getStorage().getM001ConfigSet().getCountryEntity().get(i).getCountryCode().getValue().equals(userCountry)) {
                getStorage().setUserCountryEntity(getStorage().getM001ConfigSet().getCountryEntity().get(i));
                break;
            }
        }

        Date userDOB = NVDateUtils.stringToDateLocal(thisBirthDay, NVDateFormat.LAST_UPDATED_TIME_FORMAT);
        Date today = new Date(System.currentTimeMillis());

        return ((currentYear - birthYear) >= Integer.parseInt(getStorage().getUserCountryEntity().getAge().getValue()) && userDOB.compareTo(today) < 1);
    }

    public TextEntity getIFU() {
        return mIFU;
    }


    public TextEntity getIntroVideo() {
        return mIntroVideo;
    }


    public void cancelReminder() {
        String time = String.format(Locale.getDefault(), "%02d:%02d",
                CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_HOUR, -1),
                CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_MIN, -1));

        RReminderEntity entity = CADBManager.getInstance().getRReminderEntity();
        CommonUtil.wtfi(TAG, "cancelReminder: RReminderEntity " + entity);
        if (entity == null) {
            return;
        }

        String startDate = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, 1);
        String endDate = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, 3650);
        String scheduleNotificationId = getStorage().getM065ScheduleEntity().getScheduleNotificationId();

        entity.setScheduleName(System.currentTimeMillis() + "");
        entity.setFreqType("DAILY");
        entity.setFreqInterval("");
        entity.setFreqSubdayType("SPECIFIED_TIME");
        entity.setStartTime(time);
        entity.setStartDate(startDate);
        entity.setEndDate(endDate);
        entity.setScheduleNotificationId(scheduleNotificationId);
        entity.setNotificationChannel(CommonUtil.REMINDER_INACTIVE);
        entity.setNotificationType("REMIND_TAKE_MEDICATION");
        entity.setReminderTime("300");
        entity.setStatusFlag((entity.getStatusFlag() != null && entity.getStatusFlag().equals(RReminderEntity.STATE_ADD))
                ? RReminderEntity.STATE_ADD : RReminderEntity.STATE_EDIT);

        if (CADBManager.getInstance().editRReminderEntity(entity)) {
            prepareAS10();
        }
    }

    public void getReminder() {
        RReminderEntity data = CADBManager.getInstance().getRReminderEntity();
        CommonUtil.wtfi(TAG, "getReminder..." + data);
        if (data == null) {
            //showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            ProgressLoading.dismiss();
            return;
        }

        getStorage().setM065ScheduleEntity(data);
        String tmp = data.getStartTime();
        assert tmp != null;

        String[] reminder = tmp.split(":");

        CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_HOUR, Integer.valueOf(reminder[0]));
        CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_MIN, Integer.valueOf(reminder[1]));
        if (getStorage().getM065ScheduleEntity().getNotificationChannel().equals(CommonUtil.REMINDER_ACTIVE)) {
            CommonUtil.getInstance().setAppReminder(CAApplication.getInstance());
        }
    }
}
