package com.novo.app.model.entities;

import java.io.Serializable;

public class DeviceEntity implements Serializable {
    public static final String FIASP = "Fiasp®";
    public static final String TRESIBA = "Tresiba®";
    public static final String UNKNOWN = "Unknown";
    public static final String NO_DOSE = "No Drug";

    public static final int CODE_FIASP = 3;
    public static final int CODE_TRESIBA_100 = 4;
    public static final int CODE_TRESIBA_200 = 6;
    public static final int CODE_NO_DOSE = -1;
    public static final int CODE_UNKNOWN = 0;

    //9: EOL WARNING
    //10: EOL
    //12: ERR

    public static final int SYS_EOL_WARNING = 9;
    public static final int SYS_EOL = 10;
    public static final int SYS_ERR = 12;

    public static final int EOL_WARNING = 3;
    public static final int EOL_WARNING_DOSE_MISSING = 10;
    public static final int EOL_WARNING_DRUG_MISSING = 2050;
    public static final String DRUG_MISSING = "2048";
    public static final String DOSE_MISSING = "8";

    private String assessmentId;
    private String deviceID;
    private String drugType;
    private String resultId;
    private String systemId;
    private boolean isTresiba;
    private long drugCode;
    private String lastUse;
    private String lastPair;
    private String lastPairNative;
    private boolean isLowBattery;
    private String flagCode;
    private String mNotes;

    public DeviceEntity() {

    }

    public DeviceEntity(String systemId, String flagCode, String deviceID, int drugCode, String lastUse, String lastPair, String lastPairNative) {
        this.systemId = systemId;
        this.flagCode = flagCode;
        this.deviceID = deviceID;
        this.drugCode = drugCode;
        setDrugType(drugCode);
        this.lastUse = lastUse;
        this.lastPair = lastPair;
        this.lastPairNative = lastPairNative;
        this.isLowBattery = flagCode != null && (flagCode.equals("1"));
    }

    public static String getDrugType(String drugCode) {
        if (drugCode == null) return UNKNOWN;

        switch (drugCode) {
            case CODE_NO_DOSE + "":
                return NO_DOSE;
            case CODE_FIASP + "":
                return FIASP;
            case CODE_TRESIBA_100 + "":
            case CODE_TRESIBA_200 + "":
                return TRESIBA;
            default:
                return UNKNOWN;
        }
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(int drugCode) {
        switch (drugCode) {
            case CODE_FIASP:
                this.drugType = FIASP;
                break;
            case CODE_TRESIBA_100:
            case CODE_TRESIBA_200:
                this.drugType = TRESIBA;
                break;
            case CODE_NO_DOSE:
                this.drugType = NO_DOSE;
                break;
            default:
                this.drugType = UNKNOWN;
                break;
        }
    }

    public String getLastUse() {
        return lastUse;
    }

    public void setLastUse(String lastUse) {
        this.lastUse = lastUse;
    }

    public boolean isLowBattery() {
        return isLowBattery;
    }

    public String getLastPair() {
        return lastPair;
    }

    public void setLastPair(String lastPair) {
        this.lastPair = lastPair;
    }

    public long getDrugCode() {
        return drugCode;
    }

    public void setDrugCode(long drugCode) {
        this.drugCode = drugCode;
    }

    public String getResultId() {
        return resultId;
    }

    public void setResultId(String resultId) {
        this.resultId = resultId;
    }

    public String getAssessmentId() {
        return assessmentId;
    }

    public void setAssessmentId(String assessmentId) {
        this.assessmentId = assessmentId;
    }

    public String getNotes() {
        return mNotes;
    }

    public void setNotes(String notes) {
        mNotes = notes;
    }

    public String getLastPairNative() {
        return lastPairNative;
    }

    public void setLastPairNative(String lastPairNative) {
        this.lastPairNative = lastPairNative;
    }

    public String getFlagCode() {
        return flagCode;
    }

    public void setFlagCode(String flagCode) {
        this.flagCode = flagCode;
        isLowBattery = flagCode != null && flagCode.equals("1");
    }

    public boolean isTresiba() {
        return isTresiba;
    }

    public void setTresiba(boolean isTresiba) {
        this.isTresiba = isTresiba;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }
}
