package com.novo.app.view.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.presenter.M033PairSuccessPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.M033PairedDevicesAdapter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM033PairSuccessCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

public class M033PairSuccessFrg extends BaseFragment<M033PairSuccessPresenter, OnHomeBackToView> implements OnM033PairSuccessCallBack {
    public static final String TAG = M033PairSuccessFrg.class.getName();

    @Override
    protected void initViews() {
        TextView mTvTitle = findViewById(R.id.tv_m033_title, CAApplication.getInstance().getBoldFont());

        if (getStorage().getM025ListDevices().size() >= 1 && getStorage().isM33ContinuePairing()) {
            mTvTitle.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m033_title)));
        } else {
            mTvTitle.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m033_title_one_drug)));
        }

        getStorage().setM33ContinuePairing(true);

        String highLightTitle = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m033_success));
        highLightText(mTvTitle, highLightTitle, R.color.colorM001Cyan, true);

        findViewById(R.id.tv_m033_not_now, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.bt_m033_add, this, CAApplication.getInstance().getMediumFont());

        RecyclerView mRvPairedDevices = findViewById(R.id.rv_m033_devices_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        mRvPairedDevices.setLayoutManager(layoutManager);

        M033PairedDevicesAdapter adapter = new M033PairedDevicesAdapter(mContext, getStorage().getM032PairedDevice(), this);
        mRvPairedDevices.setAdapter(adapter);

        if (getStorage().getM025ListDevices().size() >= getStorage().getM001ConfigSet().getMaxPairDevices()) {
            findViewById(R.id.bt_m033_add).setEnabled(false);
        }
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m033_pair_success;
    }

    @Override
    protected M033PairSuccessPresenter getPresenter() {
        return new M033PairSuccessPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        // This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        // This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m033_add:
                getStorage().setM029FirstPair(false);
                mCallBack.showFrgScreen(TAG, M029PairAttachFrg.TAG);
                break;
            case R.id.tv_m033_not_now:
                getStorage().setM33ContinuePairing(false);
                //has reminder
                if (getStorage().getM065ScheduleEntity() != null) {
                    mCallBack.showFrgScreen(TAG, M044PairCompleteFrg.TAG);
                    return;
                }

                for (int i = 0; i < getStorage().getM032PairedDevice().size(); i++) {
                    DeviceEntity item = getStorage().getM032PairedDevice().get(i);
                    if (item.getDrugCode() == DeviceEntity.CODE_TRESIBA_100
                            || item.getDrugCode() == DeviceEntity.CODE_TRESIBA_200) {
                        mCallBack.showFrgScreen(TAG, M034ReminderSetupFrg.TAG);
                        return;
                    }
                }

                mCallBack.showFrgScreen(TAG, M044PairCompleteFrg.TAG);
                break;
            default:
                break;
        }
    }
}
