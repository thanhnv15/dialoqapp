/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.view.base;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Toast;

import com.heapanalytics.android.Heap;
import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.BasePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.utils.StorageCommon;
import com.novo.app.view.widget.ProgressLoading;

public abstract class BaseActivity<T extends BasePresenter> extends Activity {
    protected static final int NO_LAYOUT = -1;
    private static final String TAG = BaseActivity.class.getName();
    public transient String currentTag;
    protected T mPresenter;
    protected boolean isDestroyed;

    protected abstract int getLayoutId();

    protected abstract void initViews();

    protected abstract void initData();

    protected void handleOnDestroy() {
        isDestroyed = true;
    }

    protected abstract T getPresenter();

    public void showLockDialog() {
        ProgressLoading.show(this);
    }

    protected final <G extends View> G findViewById(int id, View.OnClickListener event) {
        G view = findViewById(id);
        view.setOnClickListener(event);
        return view;
    }

    protected void showNotify(String text) {
        Toast.makeText(CAApplication.getInstance(), text, Toast.LENGTH_SHORT).show();
    }

    protected void showNotify(int text) {
        Toast.makeText(CAApplication.getInstance(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonUtil.wtfi(TAG, "BaseActivity: onCreate...");
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Heap.init(CAApplication.getInstance(), getString(R.string.id_heap_ev));

        int layoutId = getLayoutId();
        if (layoutId != NO_LAYOUT) {
            setContentView(layoutId);
        }

        mPresenter = getPresenter();
        initViews();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initData();
    }

    @Override
    protected final void onResume() {
        super.onResume();
    }

    @Override
    public final void onPause() {
        super.onPause();
    }

    protected void onClickView(int idView) {

    }

    @Override
    protected void onDestroy() {
        CommonUtil.wtfi(TAG, "BaseActivity: onDestroy...");
        handleOnDestroy();
        super.onDestroy();
        currentTag = null;
    }

    public StorageCommon getStorage() {
        return CAApplication.getInstance().getStorageCommon();
    }


    public void setTagCurrentFrg(String tag) {
        currentTag = tag;
    }

    @Override
    public boolean isDestroyed() {
        return isDestroyed;
    }

    public void showAlertDialog(String txtErr) {
        CommonUtil.getInstance().showDialog(this, txtErr, null);
    }

    public void showAlertDialog(int txtErrId) {
        CommonUtil.getInstance().showDialog(this, txtErrId, null);
    }
}
