package com.novo.app.view.fragment;

/**
 * Temporally disable due to config set update
 * Will be enable when enhance More > About me function (update for showing date of birth instead of birth year)
 */

import android.content.Context;
import android.widget.Spinner;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RUserEntity;
import com.novo.app.model.entities.CountryEntity;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.presenter.M053MorePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.CASpinnerAdapter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnActionCallBack;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM053MoreCallBack;
import com.novo.app.view.event.OnSpinnerCallBack;

import java.util.ArrayList;
import java.util.List;

public class M053MoreAboutMeFrg extends BaseFragment<M053MorePresenter, OnM024DoseLogCallBack> implements OnM053MoreCallBack, OnSpinnerCallBack, OnActionCallBack {
    public static final String TAG = M053MoreAboutMeFrg.class.getName();
    private static final int KEY_SPINNER_COUNTRY_TYPE = 1;
    private static final int KEY_SPINNER_DIABETE_TYPE = 2;
    private static final int KEY_SPINNER_BORN_YEAR_TYPE = 3;
    private static final int KEY_SPINNER_GENDER_TYPE = 4;
    private static final int KEY_SPINNER_DIABETE_YEAR_TYPE = 5;

    private Spinner mSpinnerCountry;
    private Spinner mSpinnerBornYear;
    private Spinner mSpinnerGender;
    private Spinner mSpinnerDiabeteYear;
    private Spinner mSpinnerDiabete;

    private TextView mTvUserCountry;
    private TextView mTvUserName;
    private TextView mTvUserDiabete;
    private TextView mTvUserBirthYear;
    private TextView mTvUserGender;
    private TextView mTvUserDiabeteYear;

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m053_title, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m053_required, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m053_optional, CAApplication.getInstance().getBoldFont());

        findViewById(R.id.tv_m053_name, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m053_country, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m053_diabete, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m053_birth_year, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m053_gender, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m053_year_diabete, CAApplication.getInstance().getRegularFont());

        mTvUserName = findViewById(R.id.tv_m053_user_name, this, CAApplication.getInstance().getBoldFont());
        mTvUserCountry = findViewById(R.id.tv_m053_user_country, this, CAApplication.getInstance().getBoldFont());
        mTvUserDiabete = findViewById(R.id.tv_m053_user_diabete, this, CAApplication.getInstance().getBoldFont());
        mTvUserBirthYear = findViewById(R.id.tv_m053_user_b_year, this, CAApplication.getInstance().getBoldFont());
        mTvUserGender = findViewById(R.id.tv_m053_user_gender, this, CAApplication.getInstance().getBoldFont());
        mTvUserDiabeteYear = findViewById(R.id.tv_m053_user_d_year, this, CAApplication.getInstance().getBoldFont());

        findViewById(R.id.iv_m053_name, this);
        findViewById(R.id.iv_m053_country, this);
        findViewById(R.id.iv_m053_diabete, this);
        findViewById(R.id.iv_m053_birth_year, this);
        findViewById(R.id.iv_m053_gender, this);
        findViewById(R.id.iv_m053_year_diabete, this);
        findViewById(R.id.iv_m053_back, this);

        getUserProfile();
        mSpinnerCountry = findViewById(R.id.spin_m053_country);
        mSpinnerDiabete = findViewById(R.id.spin_m053_diabete);
        mSpinnerBornYear = findViewById(R.id.spin_m053_birth_year);
        mSpinnerGender = findViewById(R.id.spin_m053_gender);
        mSpinnerDiabeteYear = findViewById(R.id.spin_m053_year_diabete);

    }

    private void getUserProfile() {
        RUserEntity userProfile = CADBManager.getInstance().getRUserProfile();
        String userName = userProfile.getFirstName() + " " + userProfile.getLastName();

        mTvUserName.setText(userName);
        mTvUserCountry.setText(getCountryName(userProfile.getCountry()));
        mTvUserDiabete.setText(setText(userProfile.getMedicationHistory(), getStorage().getM001ConfigSet().getDiabeteEntity()));
        mTvUserBirthYear.setText(userProfile.getDateOfBirth());
        mTvUserGender.setText(setText(userProfile.getGender(), getStorage().getM001ConfigSet().getGenderEntity()));
        mTvUserDiabeteYear.setText(setText(userProfile.getPersonalHistory(), getStorage().getM001ConfigSet().getYearEntity()));
    }

    private String getCountryName(String countryCode) {
        for (int i = 0; i < getStorage().getM001ConfigSet().getCountryEntity().size(); i++) {
            CountryEntity entity = getStorage().getM001ConfigSet().getCountryEntity().get(i);
            if (countryCode.equals(entity.getCountryCode().getValue()))
                return entity.getCountryName().getValue();
        }
        return countryCode;
    }

    private String setText(String key, List<TextEntity> data) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getKey().equals(key))
                return LangMgr.getInstance().getLangList().get(data.get(i).getValue());
        }
        return key;
    }

    private List<TextEntity> makeCountriesList() {
        List<TextEntity> countriesList = new ArrayList<>();
        List<CountryEntity> data = getStorage().getM001ConfigSet().getCountryEntity();
        for (int i = 0; i < data.size(); i++) {
            countriesList.add(new TextEntity(data.get(i).getCountryCode().getValue(), data.get(i).getCountryName().getValue()));
        }
        return countriesList;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m053_more_about_me;
    }

    @Override
    protected M053MorePresenter getPresenter() {
        return new M053MorePresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m053_back:
                mCallBack.showChildFrgScreen(TAG, M052MoreMyAccountFrg.TAG);
                break;
            case R.id.tv_m053_name:
            case R.id.iv_m053_name:
            case R.id.tv_m053_user_name:
                mCallBack.showChildFrgScreen(TAG, M054MoreNameFrg.TAG);
                break;
            case R.id.iv_m053_country:
            case R.id.tv_m053_user_country:
//                showDropDownList(KEY_SPINNER_COUNTRY_TYPE, makeCountriesList(), mContext);
//                Disable due to the conflict about business rule of T&C
                break;

            case R.id.iv_m053_diabete:
            case R.id.tv_m053_user_diabete:
                showDropDownList(KEY_SPINNER_DIABETE_TYPE, prepareListData(getStorage().getM001ConfigSet().getDiabeteEntity()), mContext);
                break;

            case R.id.iv_m053_birth_year:
            case R.id.tv_m053_user_b_year:
//                CommonUtil.getInstance().showDateChoosing(mContext, mTvUserBirthYear, getStorage().getM007UserEntity().getDateOfBirth(), this);
//                Disable due to the conflict about business rule of T&C
                break;

            case R.id.iv_m053_gender:
            case R.id.tv_m053_user_gender:
                showDropDownList(KEY_SPINNER_GENDER_TYPE, prepareListData(getStorage().getM001ConfigSet().getGenderEntity()), mContext);
                break;

            case R.id.iv_m053_year_diabete:
            case R.id.tv_m053_user_d_year:
                showDropDownList(KEY_SPINNER_DIABETE_YEAR_TYPE, prepareListData(getStorage().getM001ConfigSet().getYearEntity()), mContext);
                break;
        }
    }

    private List<TextEntity> prepareListData(List<TextEntity> data) {
        List<TextEntity> listData = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            String key = data.get(i).getKey();
            String value = LangMgr.getInstance().getLangList().get(data.get(i).getValue());
            listData.add(new TextEntity(key, value));
        }
        return listData;
    }

    private void showDropDownList(int type, List<TextEntity> list, Context mContext) {
        CASpinnerAdapter<OnSpinnerCallBack> adapter = new CASpinnerAdapter<>(type, list, mContext);
        adapter.setOnSpinnerCallBack(this);
        switch (type) {
            case KEY_SPINNER_COUNTRY_TYPE:
                mSpinnerCountry.setAdapter(adapter);
                mSpinnerCountry.performClick();
                break;
            case KEY_SPINNER_DIABETE_TYPE:
                mSpinnerDiabete.setAdapter(adapter);
                mSpinnerDiabete.performClick();
                break;
            case KEY_SPINNER_BORN_YEAR_TYPE:
                mSpinnerBornYear.setAdapter(adapter);
                mSpinnerBornYear.performClick();
                break;
            case KEY_SPINNER_GENDER_TYPE:
                mSpinnerGender.setAdapter(adapter);
                mSpinnerGender.performClick();
                break;
            case KEY_SPINNER_DIABETE_YEAR_TYPE:
                mSpinnerDiabeteYear.setAdapter(adapter);
                mSpinnerDiabeteYear.performClick();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(int key, int pos, Object data) {
        switch (key) {
            case KEY_SPINNER_COUNTRY_TYPE:
                TextEntity entity = ((List<TextEntity>) data).get(pos);
                mTvUserCountry.setText(entity.getValue());
                getStorage().getM007UserEntity().setCountry(entity.getKey());
                mTvUserCountry.setTag(entity);

                mTvUserCountry.setTypeface(CAApplication.getInstance().getBoldFont());
                closeSpinner(mSpinnerCountry);
                mPresenter.editUserInDB(getStorage().getM007UserEntity());
                break;

            case KEY_SPINNER_DIABETE_TYPE:
                entity = ((List<TextEntity>) data).get(pos);
                mTvUserDiabete.setText(entity.getValue());
                getStorage().getM007UserEntity().setMedicationHistory(entity.getKey());
                mTvUserDiabete.setTag(entity);

                mTvUserDiabete.setTypeface(CAApplication.getInstance().getBoldFont());
                closeSpinner(mSpinnerDiabete);
                mPresenter.editUserInDB(getStorage().getM007UserEntity());
                break;
            case KEY_SPINNER_BORN_YEAR_TYPE:
                entity = ((List<TextEntity>) data).get(pos);
                mTvUserBirthYear.setText(entity.getValue());
                getStorage().getM007UserEntity().setFamilyHistory(entity.getValue());
                mTvUserBirthYear.setTag(entity);

                mTvUserCountry.setTypeface(CAApplication.getInstance().getBoldFont());
                closeSpinner(mSpinnerBornYear);
                mPresenter.editUserInDB(getStorage().getM007UserEntity());
                break;
            case KEY_SPINNER_GENDER_TYPE:
                entity = ((List<TextEntity>) data).get(pos);
                mTvUserGender.setText(entity.getValue());
                getStorage().getM007UserEntity().setGender(entity.getKey());
                mTvUserGender.setTag(entity);

                mTvUserGender.setTypeface(CAApplication.getInstance().getBoldFont());
                closeSpinner(mSpinnerGender);
                mPresenter.editUserInDB(getStorage().getM007UserEntity());
                break;

            case KEY_SPINNER_DIABETE_YEAR_TYPE:
                entity = ((List<TextEntity>) data).get(pos);
                mTvUserDiabeteYear.setText(entity.getValue());
                getStorage().getM007UserEntity().setPersonalHistory(entity.getKey());
                mTvUserDiabeteYear.setTag(entity);

                mTvUserDiabeteYear.setTypeface(CAApplication.getInstance().getBoldFont());
                closeSpinner(mSpinnerDiabeteYear);
                mPresenter.editUserInDB(getStorage().getM007UserEntity());
                break;
            default:
                break;
        }
    }

    @Override
    public void showDialog() {
        CommonUtil.getInstance().showDialog(mContext, LangMgr.getInstance().getLangList().get(getString(R.string.lang_m053_more_my_account_about_me_dialog_message)), null);
    }

    @Override
    public void updateSuccess() {
        CADBManager.getInstance().setupUserProfile(getStorage().getM007UserEntity());
    }

    @Override
    public void updateFailed() {
        CommonUtil.getInstance().showDialog(mContext, "Fail to update into DB", null);
    }

    @Override
    public void showLoginScreen() {
        mCallBack.showM023LoginScreen();
    }

    @Override
    public void dateSet() {
        getStorage().getM007UserEntity().setDateOfBirth(getStorage().getM001ProfileEntity().getDateOfBirth());
        mPresenter.editUserInDB(getStorage().getM007UserEntity());
    }

    @Override
    public void showAlertDialog(String txtErr) {
        super.showAlertDialog(txtErr);
    }

    @Override
    public void onCallBack(Object data) {
        if (data != null && (boolean) data) {
            getUserProfile();
        }
    }
}
