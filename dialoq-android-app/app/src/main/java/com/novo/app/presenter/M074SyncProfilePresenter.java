/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RUserEntity;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM074SyncProfileCallBack;
import com.novo.app.view.widget.ProgressLoading;

import static com.novo.app.utils.CommonUtil.P_API_KEY;

public class M074SyncProfilePresenter extends BasePresenter<OnM074SyncProfileCallBack> {
    private static final String KEY_UN7_GET_USER_LOGGED_IN_PROFILE = "KEY_UN07_GET_USER_LOGGED_IN_PROFILE";
    private static final String URL_API_UN7_GET_USER_LOGGED_IN_PROFILE = "user-service/api/v1/users/%s?";

    private static final String KEY_API_UN04_UPDATE_PATIENT = "KEY_API_UN04_UPDATE_PATIENT";
    private static final String URL_API_UN04_UPDATE_PATIENT = "user-service/api/v1/users/patients/%s?";
    private static final String BODY_UN04 = "{\"firstName\":%s,\"lastName\":%s,\"email\":%s,\"gender\": %s,\"dateOfBirth\":%s,\"mobilePhoneNumber\": \"84978678205\",\"country\":%s,\"medicationHistory\":%s,\"familyHistory\":%s,\"personalHistory\":%s,\"allergies\":%s,\"race\":%s,\"streetAddress1\":%s,\"streetAddress2\":%s,\"custom1\":%s}";
    private static final String TAG = M074SyncProfilePresenter.class.getName();
    private String userId = "";

    private static final String TOKEN = "token: ";

    public M074SyncProfilePresenter(OnM074SyncProfileCallBack event) {
        super(event);
    }

    @Override
    protected void handleSuccess(String data, String tag) {
        CommonUtil.wtfi(TAG, "DATA: " + data);
        if (tag.equals(KEY_API_UN04_UPDATE_PATIENT)) {
            CADBManager.getInstance().resetFlagUserProfile(userId);
            mListener.syncProfileSuccess();
        } else if (tag.equals(KEY_UN7_GET_USER_LOGGED_IN_PROFILE)) {
            prepareUN07(data);
        }
    }

    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tag);

        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        String sms = LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON);
        if (responseEntity != null) {
            if (tag.equals(KEY_API_UN04_UPDATE_PATIENT)) {
                mListener.showAlertDialog(sms);
                mListener.syncFailed("Sync Profile failed!");
            } else if (tag.equals(KEY_UN7_GET_USER_LOGGED_IN_PROFILE)) {
                mListener.getProfileFailed(sms);
            }
        } else if (CommonUtil.getInstance().isConnectToNetwork()){
            showNotify(LangMgr.getInstance().getLangList().get(ERR_NO_CONNECTION));
        }
    }

    private void prepareUN07(String data) {
        CommonUtil.wtfi(TAG, "prepareUN07..." + data);
        UserEntity entity = generateData(UserEntity.class, data);
        if (entity == null) {
            ProgressLoading.dismiss();
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        if (entity.getState() != null && entity.getState().equals(CommonUtil.INACTIVE)) {
            ProgressLoading.dismiss();
            mListener.showM001LandingScreen("Invalid Account");
            return;
        }

        mListener.getProfileSuccess(entity);
    }

    public void callUN04UpdatePatient() {
        CommonUtil.wtfi(TAG, "callIUN04UpdatePatient");

        CARequest request = new CARequest(TAG, CARequest.METHOD_PUT);
        request.addTAG(KEY_API_UN04_UPDATE_PATIENT);

        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, TOKEN + token);
        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);

        RUserEntity entity = CADBManager.getInstance().getRUserProfile();
        userId = entity.getUserId();
        request.addPathSegment(String.format(URL_API_UN04_UPDATE_PATIENT, entity.getUserId() + ""));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY_UN04, entity.getFirstName(), entity.getLastName(),
                entity.getEmail(), entity.getGender(), entity.getDateOfBirth(), entity.getCountry(),
                entity.getMedicationHistory(), entity.getFamilyHistory(), entity.getPersonalHistory(),
                entity.getAllergies(), entity.getRace(), entity.getStreetAddress1(), entity.getStreetAddress2(), entity.getCustom1()));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    public void callUN07GetUserLoggedInProfile() {
        CommonUtil.wtfi(TAG, "callIUN07GetUserLoggedInProfile");

        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(KEY_UN7_GET_USER_LOGGED_IN_PROFILE);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            CommonUtil.wtfi(TAG, TOKEN + token);
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, TOKEN + token);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_UN7_GET_USER_LOGGED_IN_PROFILE,
                getStorage().getM007UserEntity().getUserId()));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }
}
