package com.novo.app.presenter;

import com.novo.app.view.event.OnM002SignUpChooseLanguageCallBack;

public class M002SignUpChooseLanguagePresenter extends BasePresenter<OnM002SignUpChooseLanguageCallBack> {
    public M002SignUpChooseLanguagePresenter(OnM002SignUpChooseLanguageCallBack event) {
        super(event);
    }
}
