package com.novo.app.presenter;

import com.novo.app.view.event.OnM030PairRepeatVideoCallBack;

public class M030PairRepeatVideoPresenter extends BasePresenter<OnM030PairRepeatVideoCallBack> {
    public M030PairRepeatVideoPresenter(OnM030PairRepeatVideoCallBack event) {
        super(event);
    }
}
