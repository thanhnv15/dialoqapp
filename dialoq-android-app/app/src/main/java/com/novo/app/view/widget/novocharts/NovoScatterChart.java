package com.novo.app.view.widget.novocharts;

import android.content.Context;
import android.util.AttributeSet;

import com.github.mikephil.charting.charts.ScatterChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.ScatterData;
import com.github.mikephil.charting.data.ScatterDataSet;
import com.github.mikephil.charting.utils.EntryXComparator;
import com.github.mikephil.charting.utils.Utils;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.ResultInfo;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.DoseLogAdapter;
import com.novo.app.view.widget.novocharts.formatter.NovoFiaspYValueFormatter;
import com.novo.app.view.widget.novocharts.formatter.NovoScatterChartXValueFormatter;
import com.novo.app.view.widget.novocharts.renderers.NovoScatterChartXAxisRenderer;
import com.novo.app.view.widget.novocharts.utils.NovoChartUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class NovoScatterChart extends ScatterChart {

    public NovoScatterChart(Context context) {
        super(context);
    }

    public NovoScatterChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NovoScatterChart(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    private void setupChart(boolean isEmpty) {
        if (isEmpty) {
            getAxisLeft().setEnabled(false);
            getAxisLeft().setDrawGridLines(false);
        } else {
            getAxisLeft().setEnabled(true);
            getAxisLeft().setDrawGridLines(true);
            getAxisLeft().setGridColor(0x26000000);
            getAxisLeft().setDrawAxisLine(false);
        }
        getDescription().setEnabled(false);
        getAxisRight().setEnabled(false);
        getAxisRight().setDrawLabels(false);

        setDragEnabled(true);
        setScaleEnabled(false);
        setPinchZoom(false);
        getLegend().setEnabled(false);

        getXAxis().setDrawGridLines(false);
        getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        getXAxis().setAxisMinimum(3.9833333f);
        getXAxis().setAxisMaximum(30);
        getXAxis().setLabelCount(24);
        setVisibleXRangeMaximum(30);
        setXAxisRenderer(new NovoScatterChartXAxisRenderer(getViewPortHandler(), getXAxis(), getTransformer(YAxis.AxisDependency.LEFT)));

        getXAxis().setValueFormatter(new NovoScatterChartXValueFormatter());
        getDescription().setText("");
    }

    public void setData(Date startDate, Date endDate, ArrayList<DataInfo> mData) {
        int mCount = NovoChartUtils.getDaysCountBetweenTwoDate(startDate, endDate);

        List<Entry> listEntry = new ArrayList<>();
        int maxUnit = 0;
        for (int i = 0; i < mCount; i++) {
            String date = CommonUtil.getDateNow(CommonUtil.DATE_NOW_DY, -mCount + i + 1);
            for (int j = 0; j < mData.size(); j++) {
                DataInfo item = mData.get(j);
                if (item.getResult().getAssessmentType().equals(DoseLogAdapter.TYPE_ASSESSMENT_INJECTION)
                        && item.getResult().getReportedDate().contains(date)
                        && item.getResult().getCustom3().equals(DeviceEntity.CODE_FIASP + "")
                        && !CommonUtil.isUnknownDrug(item.getResult().getCustom3(), item.getResult().getCustom4())
                        && !CommonUtil.isUnknownDoseSize(item.getResult().getResult(), item.getResult().getCustom4())) {

                    ResultInfo itemResult = item.getResult();
                    if (maxUnit < item.getResult().getResult()) {
                        maxUnit = (int) item.getResult().getResult();
                    }

                    float minute = CommonUtil.getMinute(CommonUtil.convertToLocalDate(itemResult.getReportedDate()), 4) + Utils.convertPixelsToDp(250);
                    Entry entry = new Entry(minute / 60, Float.parseFloat(String.valueOf(itemResult.getResult())));
                    listEntry.add(entry);

                }
            }
        }
        setupChart(listEntry.isEmpty());

        maxUnit = cellValue(maxUnit);

        getAxisLeft().setValueFormatter(new NovoFiaspYValueFormatter());
        getAxisLeft().setAxisMinimum(0);
        getAxisLeft().setAxisMaximum(maxUnit == 0 ? 12 : maxUnit);
        getAxisLeft().setLabelCount(maxUnit == 0 ? 6 : 7, maxUnit != 0);

        //Must be sort:
        Collections.sort(listEntry, new EntryXComparator());

        ScatterDataSet set = new ScatterDataSet(listEntry, "");
        set.setScatterShape(ScatterShape.CIRCLE);
        set.setColor(0X80fedd2f);

        set.setScatterShapeSize(40);
        set.setDrawValues(false);
        set.setHighlightEnabled(false);

        ScatterData data = new ScatterData(set);
        setData(data);
    }

    private int cellValue(int totalUnit) {
        int result = 0;
        if (totalUnit % 5 == 0) {
            result = totalUnit;
        } else if (totalUnit % 5 != 0) {
            int lastDigit = totalUnit % 10;
            if (lastDigit > 5) {
                result = totalUnit + 10 - lastDigit;
            } else {
                result = totalUnit + 5 - lastDigit;
            }
        }
        return result + result / 5;
    }
}
