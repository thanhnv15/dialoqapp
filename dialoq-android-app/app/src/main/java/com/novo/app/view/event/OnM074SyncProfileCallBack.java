package com.novo.app.view.event;

import com.novo.app.model.entities.UserEntity;

public interface OnM074SyncProfileCallBack extends OnCallBackToView {
    void syncProfileSuccess();

    void syncFailed(String sms);

    void getProfileSuccess(UserEntity entity);

    void getProfileFailed(String sms);
}
