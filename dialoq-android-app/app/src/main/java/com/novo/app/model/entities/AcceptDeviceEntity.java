package com.novo.app.model.entities;

import java.io.Serializable;

public class AcceptDeviceEntity implements Serializable {

    private boolean isAll;
    private String devices;
    private String countries;
    private String version;

    public void setAll(boolean all) {
        isAll = all;
    }

    public void setDevices(String devices) {
        this.devices = devices;
    }

    public void setCountries(String countries) {
        this.countries = countries;
    }

    public boolean isAll() {
        return isAll;
    }

    public String getDevices() {
        return devices;
    }

    public String getCountries() {
        return countries;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
