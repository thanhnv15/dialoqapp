package com.novo.app.presenter;

import com.novo.app.view.event.OnM018ProfileGenderCallBack;

public class M018ProfileGenderPresenter extends BasePresenter<OnM018ProfileGenderCallBack> {
    public M018ProfileGenderPresenter(OnM018ProfileGenderCallBack event) {
        super(event);
    }
}
