/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.entities.RDoseEntity;
import com.novo.app.manager.entities.RInjectionEntity;
import com.novo.app.manager.entities.RReminderEntity;
import com.novo.app.manager.entities.RUserEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.presenter.M025SubDoseLogPresenter;
import com.novo.app.presenter.M072SyncNotePresenter;
import com.novo.app.presenter.M073SyncDosePresenter;
import com.novo.app.presenter.M074SyncProfilePresenter;
import com.novo.app.presenter.M075SyncReminderPresenter;
import com.novo.app.view.event.OnM072SyncNoteCallBack;
import com.novo.app.utils.CATask;
import com.novo.app.utils.CommonUtil;
import com.novo.app.utils.MyHandler;
import com.novo.app.utils.StorageCommon;
import com.novo.app.view.activity.HomeActivity;
import com.novo.app.view.adapter.DoseLogAdapter;
import com.novo.app.view.event.OnActionCallBack;
import com.novo.app.view.event.OnHandleMessage;
import com.novo.app.view.event.OnM025SubDoseLogCallBack;
import com.novo.app.view.event.OnM073SyncDoseCallBack;
import com.novo.app.view.event.OnM074SyncProfileCallBack;
import com.novo.app.view.event.OnM075SyncReminderCallBack;

import java.lang.ref.WeakReference;
import java.util.List;

public class SyncScheduleService extends Service implements CATask.OnTaskCallBackToView, OnHandleMessage,
        OnM025SubDoseLogCallBack, OnActionCallBack, OnM072SyncNoteCallBack, OnM073SyncDoseCallBack, OnM074SyncProfileCallBack, OnM075SyncReminderCallBack {

    public static final String KEY_APP_DOSE_ADD_DELETE = "KEY_APP_DOSE_ADD_DELETE";
    public static final String KEY_APP_DOSE_ADD_DELETE_TIME = "KEY_APP_DOSE_ADD_DELETE_TIME";
    public static final String KEY_APP_NOTE_ADD_DELETE = "KEY_APP_NOTE_ADD_DELETE";
    public static final String KEY_APP_NOTE_ADD_DELETE_TIME = "KEY_APP_NOTE_ADD_DELETE_TIME";
    public static final String KEY_SYNC_READY = "KEY_SYNC_READY";
    private static final String TAG = SyncScheduleService.class.getName();
    private static final long DELAY_TIME = 1000 * 60 * 1;//1 minutes
    private static final String CHANNEL_ID = "novo_dialoq_channel";

    private static CATask<SyncScheduleService, SyncScheduleService> mTask;
    private MyHandler<SyncScheduleService, SyncScheduleService> mHandler;
    private boolean isReload;
    private boolean mIsRefresh;
    private boolean isSyncedNote;

    public SyncScheduleService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground();
    }

    private void startForeground() {
        Intent intent = new Intent(this, HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1002, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Action action =
                new NotificationCompat.Action.Builder(
                        0, getString(R.string.txt_return_dms), pendingIntent
                ).build();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(CAApplication.getInstance());
        builder.setChannelId(CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setContentTitle(getString(R.string.app_name));
        builder.setContentText(getString(R.string.txt_dms_using));
        builder.addAction(action);
        NotificationManager notificationMgr = (NotificationManager) CAApplication.getInstance().getSystemService(NOTIFICATION_SERVICE);

        assert notificationMgr != null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_NONE);
            notificationMgr.createNotificationChannel(mChannel);
        }

        startForeground(101, builder.build());
        getStorage().setCallBackToSync(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public synchronized int onStartCommand(Intent intent, int flags, int startId) {
        mHandler = new MyHandler<>(new WeakReference<>(this), this);

        closeTask();

        mTask = new CATask<>(new WeakReference<>(this), this, DELAY_TIME);
        mTask.setLoop(true);
        mTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        return START_STICKY;
    }

    private synchronized void closeTask() {
        try {
            if (mTask != null) {
                mTask.cancel(true);
                mTask = null;
            }
        } catch (Exception ignored) {
            // do nothing
        }
    }

    @Override
    public void backToPreviousScreen() {

    }

    @Override
    public void onDestroy() {
        closeTask();
        super.onDestroy();
    }

    @Override
    public Object doBGTask(String key) {
        if (getStorage().getLoginSessionExpire() != null) {
            getStorage().getLoginSessionExpire().onCallBack(null);
        }
        if (!CommonUtil.getInstance().isConnectToNetwork()) {

            return new Object();
        }

        if (isReload) {
            isReload = false;
            return new Object();
        }
        mHandler.sendMessage(Message.obtain());
        return new Object();
    }

    @Override
    public void updateUITask(Object[] values) {

    }

    @Override
    public void doneTask(Object data) {

    }

    @Override
    public synchronized void doMessage(Message msg) {
        String key = CommonUtil.getInstance().getPrefContent(KEY_SYNC_READY);
        if (key != null && key.equals("true")) {
            CommonUtil.wtfe(TAG, "Sync data is not ready for now!");
            getStorage().callBackDoseCallBack(false);
            return;
        }

        CommonUtil.wtfi(TAG, "doMessage...KEY_SYNC_READY...true");
        CommonUtil.getInstance().savePrefContent(KEY_SYNC_READY, "true");
        M025SubDoseLogPresenter presenter = new M025SubDoseLogPresenter(this);
        presenter.initTime(false);
        presenter.resetData();
        presenter.doAllAS05GetResult();
    }

    @Override
    public void onCallBack(Object data) {
        isReload = true;
        doMessage(null);
    }

    @Override
    public void closeRefresh() {
        getStorage().callBackDoseToCloseRefresh();
    }

    @Override
    public void dataAS05Ready() {
        CommonUtil.wtfi(TAG, "dataAS05Ready...Sync");
        if (mIsRefresh) {
            isSyncedNote = false;
            CADBManager.getInstance().deleteAllInjection();
            CADBManager.getInstance().deleteAllInjectionItem();
            getStorage().callBackDoseCallBack(true);
            mIsRefresh = false;
            return;
        }
        getOnlineReminder();

//        getOnlineProfile();
//        syncProfile();
//        syncNote();
//        syncDose();
    }

    @Override
    public void syncReminderReady() {
        syncReminder();
    }

    @Override
    public void delReminderSuccess() {
        if (CADBManager.getInstance().delReminderEntity()) {
            CommonUtil.getInstance().cancelAppReminder(false, CAApplication.getInstance(), null);
        }
    }

    private void getOnlineReminder() {
        M075SyncReminderPresenter presenter = new M075SyncReminderPresenter(this, null);
        presenter.callAS029GetListSchedulesOfAssessment();
    }

    @Override
    public void getAS5NoteReady() {
        Toast.makeText(CAApplication.getInstance(), "Syncing data is not ready for now!", Toast.LENGTH_SHORT).show();
        CommonUtil.getInstance().clearPrefContent(KEY_SYNC_READY);
    }

    private void syncDose() {
        String key = CommonUtil.getInstance().getPrefContent(KEY_APP_DOSE_ADD_DELETE);

        if (key != null && key.equals("true")) {
            CADBManager.getInstance().deleteAllADDRNote(DoseLogAdapter.TYPE_ASSESSMENT_INJECTION,
                    CommonUtil.getInstance().getPrefContent(KEY_APP_DOSE_ADD_DELETE_TIME));
            CommonUtil.getInstance().clearPrefContent(KEY_APP_DOSE_ADD_DELETE);
            CommonUtil.getInstance().clearPrefContent(KEY_APP_DOSE_ADD_DELETE_TIME);
        }

        List<RInjectionEntity> mListDose = CADBManager.getInstance().getAllNotSyncDose();
        if (mListDose == null || mListDose.size() == 0) {
            syncDoseSuccess(0);
            return;
        }

        M073SyncDosePresenter presenter = new M073SyncDosePresenter(this, mListDose);
        CommonUtil.getInstance().savePrefContent(KEY_APP_DOSE_ADD_DELETE_TIME, System.currentTimeMillis() + "");
        presenter.syncData();
    }

    private void syncNote() {
        String key = CommonUtil.getInstance().getPrefContent(KEY_APP_NOTE_ADD_DELETE);
        if (key != null && key.equals("true")) {
//            CADBManager.getInstance().deleteAllADDRNote(DoseLogAdapter.TYPE_ASSESSMENT_NOTE,
////                    CommonUtil.getInstance().getPrefContent(KEY_APP_NOTE_ADD_DELETE_TIME));

            CADBManager.getInstance().changeStatusFlagAllADDRNote(DoseLogAdapter.TYPE_ASSESSMENT_NOTE,
                    CommonUtil.getInstance().getPrefContent(KEY_APP_NOTE_ADD_DELETE_TIME));
            CommonUtil.getInstance().clearPrefContent(KEY_APP_NOTE_ADD_DELETE);
            CommonUtil.getInstance().clearPrefContent(KEY_APP_NOTE_ADD_DELETE_TIME);
        }

        List<RDoseEntity> mListNote = CADBManager.getInstance().getAllNotNullNote();
        if (mListNote == null || mListNote.size() == 0) {
            syncNoteSuccess(0, 0, 0);
            return;
        }

        M072SyncNotePresenter presenter = new M072SyncNotePresenter(this, mListNote);
        CommonUtil.getInstance().savePrefContent(KEY_APP_NOTE_ADD_DELETE_TIME, System.currentTimeMillis() + "");
        presenter.upADDNoteData();
    }

    private void syncProfile() {
        CommonUtil.wtfi(TAG, "syncProfile...");
        RUserEntity entity = CADBManager.getInstance().getRUserProfile();
        if (entity != null && entity.getStatusFlag() != null && entity.getStatusFlag().equals(RUserEntity.STATE_UPDATE)) {
            M074SyncProfilePresenter presenter = new M074SyncProfilePresenter(this);
            presenter.callUN04UpdatePatient();
        } else {
            syncNote();
        }
    }

    private void syncReminder() {
        CommonUtil.wtfi(TAG, "syncReminder...");
        RReminderEntity entity = CADBManager.getInstance().getRReminderEntity();
        if (entity == null || entity.getStatusFlag() == null || entity.getStatusFlag().isEmpty()) {
            syncProfile();
            return;
        }

        M075SyncReminderPresenter presenter = new M075SyncReminderPresenter(this, entity);
        switch (entity.getStatusFlag()) {
            case RReminderEntity.STATE_ADD:
                presenter.callAS02CreateAssessmentSchedule(entity);
                break;
            case RReminderEntity.STATE_EDIT:
                presenter.callAS10UpdateAssessmentSchedule(entity);
                break;
            case RReminderEntity.STATE_DEL:
                presenter.callAS09DeleteAssessmentSchedule(entity);
                break;
            default:
                break;
        }
    }

    private void getOnlineProfile() {
        CommonUtil.wtfi(TAG, "getOnlineProfile...");
        if (CADBManager.getInstance().getRUserProfile().getStatusFlag().equals(RUserEntity.STATE_UPDATE)) {
            M074SyncProfilePresenter presenter = new M074SyncProfilePresenter(this);
            presenter.callUN07GetUserLoggedInProfile();
        } else {
            syncNote();
        }
    }

    @Override
    public void refreshData() {

    }

    @Override
    public void reloadData() {

    }

    @Override
    public void setReminderNotification(String minDate, String maxDate) {

    }

    public final StorageCommon getStorage() {
        return CAApplication.getInstance().getStorageCommon();
    }

    @Override
    public void syncNoteSuccess(int sumAdd, int sumEdit, int sumDel) {
        CommonUtil.wtfi(TAG, "syncSuccess...Note");
        if (sumAdd > 0) {
            CommonUtil.getInstance().savePrefContent(KEY_APP_NOTE_ADD_DELETE, "true");
        }

        if (sumAdd + sumEdit + sumDel > 0) {
            isSyncedNote = true;
        }
        Toast.makeText(CAApplication.getInstance(), "Sync note success: Add: " + sumAdd + "+, Edit: " + sumEdit + ", Del: " + sumDel, Toast.LENGTH_SHORT).show();
        syncDose();
    }

    @Override
    public void syncDoseSuccess(int index) {
        CommonUtil.wtfi(TAG, "syncSuccess...Dose");
        mIsRefresh = true;
        CommonUtil.getInstance().clearPrefContent(KEY_SYNC_READY);

        if (index > 0) {
            CommonUtil.getInstance().savePrefContent(KEY_APP_DOSE_ADD_DELETE, "true");
        }

        if (index == 0 && !isSyncedNote) {
            dataAS05Ready();
            return;
        }

        M025SubDoseLogPresenter presenter = new M025SubDoseLogPresenter(this);
        presenter.initTime(false);
        presenter.resetData();
        presenter.doAllAS05GetResult();

        Toast.makeText(CAApplication.getInstance(), "Sync dose success: " + index + " times", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void syncProfileSuccess() {
        Toast.makeText(CAApplication.getInstance(), "Sync profile success!", Toast.LENGTH_SHORT).show();
        CommonUtil.wtfi(TAG, "syncSuccess...Profile");
        syncNote();
    }

    @Override
    public void syncReminderSuccess() {
        CommonUtil.wtfi(TAG, "syncReminderSuccess...");
        Toast.makeText(CAApplication.getInstance(), "Sync ReminderSuccess success!", Toast.LENGTH_SHORT).show();
        getOnlineProfile();
    }

    @Override
    public void syncFailed(String sms) {
        Toast.makeText(CAApplication.getInstance(), sms, Toast.LENGTH_SHORT).show();

        isSyncedNote = false;
        getStorage().callBackDoseCallBack(null);
        CommonUtil.getInstance().clearPrefContent(KEY_SYNC_READY);
    }

    @Override
    public void getProfileSuccess(UserEntity entity) {
        CommonUtil.wtfi(TAG, "getProfileSuccess...");
        if (entity.getCustom1() == null) {
            if (CADBManager.getInstance().getRUserProfile().getCustom1() != null) syncProfile();
        } else {
            long onlineTimeStamp = Long.parseLong(entity.getCustom1());
            long offlineTimeStamp = Long.parseLong(CADBManager.getInstance().getRUserProfile().getCustom1());
            if (onlineTimeStamp < offlineTimeStamp) {
                CommonUtil.wtfi(TAG, "getProfileSuccess:: Start sync profile");
                syncProfile();
            } else {
                CommonUtil.wtfi(TAG, "getProfileSuccess:: Update latest profile");
                CADBManager.getInstance().addRUserEntity(entity);
                syncNote();
            }
        }
    }

    @Override
    public void getProfileFailed(String sms) {
        CommonUtil.wtfi(TAG, "getProfileFailed...");
        syncNote();
    }
}