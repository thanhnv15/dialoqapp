package com.novo.app.view.base;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.ble.BLEInfoEntity;
import com.novo.app.ble.BLEService;
import com.novo.app.ble.DeviceScanUtil;
import com.novo.app.ble.OnBLECallBack;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.presenter.BLEPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.fragment.M032PairPushReleaseFrg;

public abstract class BaseBLEFragment<G extends BLEPresenter, T extends OnCallBackToView> extends BaseFragment<G, T> implements OnCallBackToView, OnBLECallBack {

    public static final String TAG = M032PairPushReleaseFrg.class.getName();
    protected static final int REQUEST_ENABLE_BLE = 105;
    private static final int CASE_ERROR = 102;
    protected String mDeviceAddress;
    protected BLEService mBluetoothLeService;
    protected final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            CommonUtil.wtfi(TAG, "onServiceConnected...");

            mBluetoothLeService = ((BLEService.LocalBinder) service).getService();
            initBLE();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            //mBluetoothLeService = null;
        }
    };

    protected Handler mHandler;
    protected DeviceScanUtil mScanUtil;
    protected String mDeviceName;

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mContext.unbindService(mServiceConnection);
        } catch (Exception ignored) {
        }

        mScanUtil.disconnectBLE();
        if (mBluetoothLeService != null) {
            mBluetoothLeService.disconnectBLE();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mScanUtil.disconnectBLE();
        mScanUtil.stopScan();
        if (mBluetoothLeService != null) {
            mBluetoothLeService.disconnectBLE();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mScanUtil.initBLE(mContext)) {
            mScanUtil.startScan();
        }
    }

    protected final void initBLE() {
        if (mBluetoothLeService.init()) {
            CommonUtil.wtfe(TAG, "Unable to initialize Bluetooth");
            return;
        }

        mBluetoothLeService.setCallBack(this);
        mBluetoothLeService.connect(mDeviceAddress);
    }

    @Override
    protected void initViews() {
        mHandler = new Handler(msg -> {
            if (msg.what == CASE_ERROR) {
                showAlertDialog(CAApplication.getInstance().getString(msg.arg1));
            } else {
                doMessage(msg);
            }
            return false;
        });

        mScanUtil = new DeviceScanUtil();
        mScanUtil.setCallBack(this);
    }

    protected abstract void doMessage(Message msg);


    protected final void startAgain() {
        mScanUtil.disconnectBLE();
        mScanUtil.stopScan();
        if (mBluetoothLeService != null) {
            mBluetoothLeService.disconnectBLE();
        }
        if (mScanUtil.initBLE(mContext)) {
            mScanUtil.startScan();
        }
    }

    protected final DeviceEntity isExistDevicePaired() {
        for (int i = 0; i < getStorage().getM025ListDevices().size(); i++) {
            if (getStorage().getM025ListDevices().get(i).getDeviceID().equals(mDeviceName))
                return getStorage().getM025ListDevices().get(i);
        }
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BLE && resultCode == Activity.RESULT_OK && mScanUtil != null) {
            mScanUtil.startScan();
        }
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        // This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        // This feature isn't require here
    }

    protected final void displayData(BLEInfoEntity data) {
        CommonUtil.wtfi(TAG, "DATA: " + data);
        Message message = new Message();
        message.obj = data;
        mHandler.sendMessage(message);
    }

    @Override
    public final void onRequestPermission() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BLE);
    }

    @Override
    public final void onStartPairing(BluetoothDevice device, String deviceName) {
        CommonUtil.wtfi(TAG, "onStartPairing..." + device.getName());
        // Sets up UI references.
        mDeviceAddress = device.getAddress();
        mDeviceName = deviceName;

        if (mBluetoothLeService == null) {
            Intent gattServiceIntent = new Intent(mContext, BLEService.class);
            mContext.bindService(gattServiceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
            CommonUtil.wtfi(TAG, "onStartPairing...nd");
        } else {
            initBLE();
        }
    }

    @Override
    public final void onReceiveDoseLog(BLEInfoEntity mData) {
        CommonUtil.wtfi(TAG, mData.toString());
        displayData(mData);
    }

    @Override
    public final void onDisconnect() {
        CommonUtil.wtfe(TAG, "Disconnected...");
        if (!mBluetoothLeService.isPaired()) {
            CommonUtil.wtfe(TAG, "After Pair Success!");
            if (mScanUtil.initBLE(mContext)) {
                mScanUtil.startScan();
            }
        } else {
            CommonUtil.wtfe(TAG, "Disconnected: Error 2");
            Message message = new Message();
            message.what = CASE_ERROR;
            message.arg1 = R.string.txt_ble_connection_err;
            mHandler.sendMessage(message);
        }
    }

    @Override
    public final void onConnect() {
        CommonUtil.wtfe(TAG, "Connected");
    }

    @Override
    public final void onScanFailed() {
        CommonUtil.wtfe(TAG, "onScanFailed");
    }
}
