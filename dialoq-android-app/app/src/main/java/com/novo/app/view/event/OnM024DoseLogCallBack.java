package com.novo.app.view.event;

public interface OnM024DoseLogCallBack extends OnCallBackToView {
    void showChildFrgScreen(String tagSource, String tagChild);

    void changeTab(int tab);

    void showM023LoginScreen();

    void showM001LandingScreen();

    void showM029PairScreen();

    void showM070PairTroubleScreen();

    void showM032PairScreen();

    void updateBottomBar();

    void unregisterTimeZoneChange();

    void setupReminder();

    void stopBGScan();

    void startBGScan();

    default void hideBottomBar(boolean hide) {

    }

    default void preLoginTimeZoneChange() {

    }

    default void showGeneratingProgress () {

    }

    default void dismissGeneratingProgress () {

    }

    void showNetworkAlert();

    void doTimeZoneChange();
}
