package com.novo.app.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.model.entities.DateEntity;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM036SubDoseLogSummaryCallBack;

import java.util.List;
import java.util.Locale;

public class CalendarAdapter extends BaseRecycleAdapter<OnM036SubDoseLogSummaryCallBack, DateEntity,
        CalendarAdapter.CalendarHolder> implements OnM036SubDoseLogSummaryCallBack {
    private int colorType;

    public CalendarAdapter(int colorType, Context mContext, List<DateEntity> mListData, OnM036SubDoseLogSummaryCallBack mCallBack) {
        super(mContext, mListData, mCallBack);
        this.colorType = colorType;
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_m014_calendar;
    }

    @Override
    protected CalendarAdapter.CalendarHolder getViewHolder(int viewType, View itemView) {
        return new CalendarHolder(itemView);
    }

    @Override
    public void showM001LandingScreen(String err) {
        mCallBack.showM001LandingScreen(err);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int pos) {
        CalendarHolder holder = (CalendarHolder) viewHolder;
        DateEntity item = mListData.get(pos);

        holder.vHeader.setVisibility(item.isHeader() ? View.VISIBLE : View.GONE);
        if (item.getDailyDetail().getDoseList().isEmpty()) {
            holder.vHeader.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
        } else {
            switch (lastSuitableDose(item)) {
                case DeviceEntity.CODE_TRESIBA_100:
                    holder.vHeader.setBackgroundColor(CommonUtil.COLOR_TYPE_TRESIBA);
                    break;
                case DeviceEntity.CODE_TRESIBA_200:
                    holder.vHeader.setBackgroundColor(CommonUtil.COLOR_TYPE_TRESIBA_200);
                    break;
                case DeviceEntity.CODE_FIASP:
                    holder.vHeader.setBackgroundColor(CommonUtil.COLOR_TYPE_FIASP);
                    break;
            }
        }

        holder.tvDate.setText(String.format(Locale.getDefault(), "%d", item.getDate()));
        holder.ivNote.setVisibility(item.isNote() ? View.VISIBLE : View.GONE);
        holder.ivError.setVisibility(showError(item));

        if (holder.ivNote.getVisibility() == View.GONE) {
            ViewGroup.MarginLayoutParams marginParam = (ViewGroup.MarginLayoutParams) holder.ivError.getLayoutParams();
            marginParam.setMargins(0, marginParam.topMargin, marginParam.rightMargin, marginParam.bottomMargin);
            holder.ivError.setLayoutParams(marginParam);
        }
    }

    private int lastSuitableDose(DateEntity item) {
        if (colorType == CommonUtil.COLOR_TYPE_FIASP) {
            for (int i = 0; i < item.getDailyDetail().getDoseList().size(); i++) {
                int lastDose = Integer.parseInt(item.getDailyDetail().getDoseList().get(i).getResult().getCustom3());
                if (lastDose == DeviceEntity.CODE_FIASP) {
                    return lastDose;
                }
            }
        } else {
            for (int i = 0; i < item.getDailyDetail().getDoseList().size(); i++) {
                int lastDose = Integer.parseInt(item.getDailyDetail().getDoseList().get(i).getResult().getCustom3());
                if (lastDose == DeviceEntity.CODE_TRESIBA_100 || lastDose == DeviceEntity.CODE_TRESIBA_200) {
                    return lastDose;
                }
            }
        }
        return 0;
    }

    private int showError(DateEntity item) {
        if (item.isUnknownDrug()) {
            return View.VISIBLE;
        }
        if (item.isUnknownFiaspSize() && colorType == CommonUtil.COLOR_TYPE_FIASP) {
            return View.VISIBLE;
        }
        if (item.isUnknownTresibaSize()) {
            if (colorType == CommonUtil.COLOR_TYPE_TRESIBA || colorType == CommonUtil.COLOR_TYPE_TRESIBA_200) {
                return View.VISIBLE;
            }
        }
        return View.GONE;
    }


    public class CalendarHolder extends BaseHolder {
        View vHeader;
        TextView tvDate;
        ImageView ivNote, ivError;
        FrameLayout frMain;

        CalendarHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void onClickView(int idView) {
            switch (idView) {
                case R.id.v_m014_header:
                case R.id.tv_m014_content:
                case R.id.iv_m014_note:
                case R.id.iv_m036_error:
                    mCallBack.showSummaryDialog(mListData.get(getAdapterPosition()), mListData.get(getAdapterPosition()).getDailyDetail().getColorType());
                    break;
            }
        }

        @Override
        protected void initView() {
            frMain = findViewById(R.id.fr_m014_item_calendar, this);
            vHeader = findViewById(R.id.v_m014_header, this);
            tvDate = findViewById(R.id.tv_m014_content, this, CAApplication.getInstance().getBoldFont());
            ivNote = findViewById(R.id.iv_m014_note, this);
            ivError = findViewById(R.id.iv_m036_error, this);
        }
    }
}
