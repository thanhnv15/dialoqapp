package com.novo.app.view.widget.novocharts;

import android.content.Context;
import android.graphics.RectF;
import android.util.AttributeSet;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.highlight.BarHighlighter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.widget.novocharts.configs.NovoBarDetailsConfig;
import com.novo.app.view.widget.novocharts.datasets.BarDetailsData;
import com.novo.app.view.widget.novocharts.datasets.BarDetailsDataSet;
import com.novo.app.view.widget.novocharts.datasets.IBarDetailsDataSet;
import com.novo.app.view.widget.novocharts.providers.BarDetailsProvider;
import com.novo.app.view.widget.novocharts.renderers.NovoDetailsChartRenderer;

import java.util.ArrayList;

public class NovoDetailsChart extends BarLineChartBase<BarDetailsData> implements BarDetailsProvider {

    protected boolean mHighlightFullBarEnabled = false;
    private boolean mDrawValueAboveBar = true;
    private boolean mDrawBarShadow = false;
    private boolean mFitBars = false;

    public NovoDetailsChart(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public NovoDetailsChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NovoDetailsChart(Context context) {
        super(context);
    }

    @Override
    protected void init() {
        super.init();
        mRenderer = new NovoDetailsChartRenderer(this, mAnimator, mViewPortHandler);

        setHighlighter(new BarHighlighter(this));

        getXAxis().setSpaceMin(0.5f);
        getXAxis().setSpaceMax(0.5f);
    }

    @Override
    public BarDetailsData getBarDetailsData() {
        return mData;
    }

    @Override
    public BarData getBarData() {
        return null;
    }


    @Override
    protected void calcMinMax() {

        if (mFitBars) {
            mXAxis.calculate(mData.getXMin() - mData.getBarWidth() / 2f, mData.getXMax() + mData.getBarWidth() / 2f);
        } else {
            mXAxis.calculate(mData.getXMin(), mData.getXMax());
        }

        mAxisLeft.calculate(mData.getYMin(YAxis.AxisDependency.LEFT), mData.getYMax(YAxis.AxisDependency.LEFT));
        mAxisRight.calculate(mData.getYMin(YAxis.AxisDependency.RIGHT), mData.getYMax(YAxis.AxisDependency
                .RIGHT));
    }

    @Override
    public Highlight getHighlightByTouchPoint(float x, float y) {

        if (mData == null) {
            CommonUtil.wtfe(LOG_TAG, "Can't select by touch. No data set.");
            return null;
        } else {
            Highlight h = getHighlighter().getHighlight(x, y);
            if (h == null || !isHighlightFullBarEnabled()) return h;

            return new Highlight(h.getX(), h.getY(),
                    h.getXPx(), h.getYPx(),
                    h.getDataSetIndex(), -1, h.getAxis());
        }
    }

    public void getBarBounds(BarEntry e, RectF outputRect) {

        RectF bounds = outputRect;

        IBarDataSet set = mData.getDataSetForEntry(e);

        if (set == null) {
            bounds.set(Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE);
            return;
        }

        float y = e.getY();
        float x = e.getX();

        float barWidth = mData.getBarWidth();

        float left = x - barWidth / 2f;
        float right = x + barWidth / 2f;
        float top = y >= 0 ? y : 0;
        float bottom = y <= 0 ? y : 0;

        bounds.set(left, top, right, bottom);

        getTransformer(set.getAxisDependency()).rectValueToPixel(outputRect);
    }

    public boolean isDrawValueAboveBarEnabled() {
        return mDrawValueAboveBar;
    }

    public boolean isDrawBarShadowEnabled() {
        return mDrawBarShadow;
    }

    @Override
    public boolean isHighlightFullBarEnabled() {
        return mHighlightFullBarEnabled;
    }

    public void setHighlightFullBarEnabled(boolean enabled) {
        mHighlightFullBarEnabled = enabled;
    }

    private void setupChart() {
        setHighlightPerTapEnabled(false);
        setHighlightPerDragEnabled(false);
        getXAxis().setDrawGridLines(false);
        getXAxis().setDrawLabels(false);
        getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        getXAxis().setDrawAxisLine(false);
        getXAxis().setAxisMinimum(-0.5f);
        getXAxis().setAxisMaximum(5f);
        getXAxis().setLabelCount(5);
        getXAxis().setGranularityEnabled(true);
        getXAxis().setGranularity(1);

        getAxisLeft().setDrawGridLines(false);
        getAxisLeft().setDrawLabels(false);
        getAxisLeft().setDrawAxisLine(false);


        getAxisRight().setEnabled(false);

        setDrawGridBackground(false);
        setHighlightFullBarEnabled(true);
        getLegend().setEnabled(false);
        getDescription().setText("");
        setPinchZoom(false);
        setScaleEnabled(false);
    }

    public void setMockData(NovoBarDetailsConfig config) {

        setupChart();

        String[] times1 = new String[]{"8:00", "14:30", "20:30"};

        String[] times2 = new String[]{"9:00", "15:30", "21:30"};

        ArrayList<BarEntry> values1 = new ArrayList<>();


        float mul = 9;

        float val11 = (float) (Math.random() * mul) + mul / 3;
        float val21 = (float) (Math.random() * mul) + mul / 3;
        float val31 = (float) (Math.random() * mul) + mul / 3;

        values1.add(new BarEntry(0, new float[]{val11, val21, val31}, null, times1));


        ArrayList<BarEntry> values2 = new ArrayList<>();

        float val12 = (float) (Math.random() * mul) + mul / 3;
        float val22 = (float) (Math.random() * mul) + mul / 3;
        float val32 = (float) (Math.random() * mul) + mul / 3;

        values2.add(new BarEntry(2.75f, new float[]{val12, val22, val32}, null, times2));

        BarDetailsDataSet set1;
        set1 = new BarDetailsDataSet(values1, "");
        set1.setDrawIcons(false);
        set1.setColor(config.getFirstBarColor());
        set1.setDrawValues(false);
        set1.setUnitColor(config.getFirstValueColor());


        BarDetailsDataSet set2;
        set2 = new BarDetailsDataSet(values2, "");
        set2.setDrawIcons(false);
        set2.setColor(config.getSecondBarColor());
        set2.setDrawValues(false);
        set2.setUnitColor(config.getSecondValueColor());


        ArrayList<IBarDetailsDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        if (config.isBothType()) {
            dataSets.add(set2);
        }

        BarDetailsData data = new BarDetailsData(dataSets);
        data.setBarWidth(1f);

        setData(data);
        notifyDataSetChanged();
    }


}
