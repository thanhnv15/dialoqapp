package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RDecryptionKeyEntity;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.DecryptionKeyEntity;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.model.entities.ResultInfo;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM032PairPushReleaseCallBack;
import com.novo.app.view.widget.ProgressLoading;

import java.util.List;

public class M032PairPushReleasePresenter extends BLEPresenter<OnM032PairPushReleaseCallBack> {
    private static final String KEY_API_AS04_CREATE_ASSESSMENT_RESULT = "KEY_API_AS04_CREATE_ASSESSMENT_RESULT";
    private static final String URL_API_AS04_CREATE_ASSESSMENT_RESULT = CommonUtil.HOT_URL + "/api/v1/users/%s/assessments/%s/results?";
    private static final String BODY_AS04 = "{\"assessmentType\": \"Devices\",\"deviceId\":%s,\"reportedDate\":%s,\"result\":%s, \"notes\": %s, \"custom1\":%s,\"custom2\":%s ,\"custom3\":%s}";

    private static final String URL_API_NNDM_SYSTEM_ID_GET_DECRYPTION = "https://flexdigitalhealth-dev.apigee.net/dev1-dialoq-product/dialoq-service/api/v1/devices/%s?";
    private static final String KEY_API_NNDM_SYSTEM_ID_GET_DECRYPTION = "KEY_API_NNDM_SYSTEM_ID_GET_DECRYPTION";

    private static final String P_API_KEY = "apikey";
    private static final String ERR_500 = "ERR_500";
    private static final String TAG = M032PairPushReleasePresenter.class.getName();
    private DeviceEntity mDevice;

    public M032PairPushReleasePresenter(OnM032PairPushReleaseCallBack event) {
        super(event);
    }

    @Override
    protected void handleSuccess(String data, String tag) {
        CommonUtil.wtfi(TAG, "DATA: " + data);
        switch (tag) {
            case KEY_API_AS04_CREATE_ASSESSMENT_RESULT:
                prepareAS04(data);
                break;
            case KEY_API_NNDM_SYSTEM_ID_GET_DECRYPTION:
                prepareNNDMSystemIdForDecryption(data);
                break;
        }
    }

    private void prepareNNDMSystemIdForDecryption(String data) {
        CommonUtil.wtfi(TAG, "prepareNNDMSystemIdForDecryption..." + data);

        DecryptionKeyEntity entity = generateData(DecryptionKeyEntity.class, data);
        if (entity != null) {
            RDecryptionKeyEntity rs = CADBManager.getInstance().addRNNDMEntity(entity);
            if (rs != null) {
                getStorage().getNNMDDecryptionKey().add(rs);
            }
        }
        callAS04CreateResultForAssessment();
    }

    void prepareAS04(String data) {
        CommonUtil.wtfi(TAG, "prepareAS04..." + data);
        ResultInfo entity = generateData(ResultInfo.class, data);
        if (entity == null) {
            ProgressLoading.dismiss();
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }

        DeviceEntity item = extractDevice(entity);
        getStorage().getM025ListDevices().add(item);
        getStorage().setM029FirstPair(false);
        mListener.pairCompleted();
    }

    @Override
    public void hideLockDialog(String key) {
        // do nothing
    }

    public void hideLockDialog() {
        ProgressLoading.dismiss();
    }

    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tagRequest);
        ProgressLoading.dismiss();
        mListener.resetStateBLE();

        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        String sms = LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON);

        if (responseEntity != null && responseEntity.getErrorEntity() != null) {
            sms = responseEntity.getErrorEntity()[0].getMessage();
        }

        if (code == CommonUtil.CODE_401 && !tag.equals(KEY_API_NNDM_SYSTEM_ID_GET_DECRYPTION)) {
            mListener.showM001LandingScreen(sms);
            return;
        }

        switch (tag) {
            case KEY_API_AS04_CREATE_ASSESSMENT_RESULT:
                showNotify(sms + "(AS04");
                break;
            case KEY_API_NNDM_SYSTEM_ID_GET_DECRYPTION:
                prepareNNDMSystemIdForDecryption(null);
                break;
        }
    }

    void callAS04CreateResultForAssessment() {
        CommonUtil.wtfi(TAG, "callAS04CreateResultForAssessment");

        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_AS04_CREATE_ASSESSMENT_RESULT);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(ERR_500));
            return;
        }

        CommonUtil.wtfi(TAG, "token: " + token);
        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);

        request.addPathSegment(String.format(URL_API_AS04_CREATE_ASSESSMENT_RESULT,
                getStorage().getM007UserEntity().getUserId(), getAssessmentId() + ""));
        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY_AS04,
                mDevice.getFlagCode(),
                CommonUtil.getDateNow(CommonUtil.DATE_STYLE),
                isExistedTresiba(mDevice) ? "1" : "0",
                mDevice.getSystemId(),
                mDevice.getDeviceID(),
                mDevice.getDeviceID(),
                mDevice.getDrugCode()));
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    private boolean isExistedTresiba(DeviceEntity item) {
        return item.getDrugCode() == DeviceEntity.CODE_TRESIBA_100 || item.getDrugCode() == DeviceEntity.CODE_TRESIBA_200;
    }

    private String getAssessmentId() {
        CarePlantUserEntity entity = getStorage().getM001CarePlanUserEntity();
        String assessmentId = "";
        for (int i = 0; i < entity.getData().size(); i++) {
            List<CarePlantUserEntity.AssessmentInfo> listAssessment = entity.getData().get(i).getAssessments();
            for (int j = 0; j < listAssessment.size(); j++) {
                if (listAssessment.get(j).getType().equals(CommonUtil.DEVICE_ASSESSMENT_TYPE)) {
                    assessmentId = listAssessment.get(j).getAssessmentId();
                    return assessmentId;
                }
            }
        }
        return assessmentId;
    }

    public void callNNDMSystemIdForDecryption(DeviceEntity mDevice) {
        CommonUtil.wtfi(TAG, "callNNDMSystemIdForDecryption...");
        this.mDevice = mDevice;
        CARequest request = new CARequest(TAG, CARequest.METHOD_GET, false);
        request.addTAG(KEY_API_NNDM_SYSTEM_ID_GET_DECRYPTION);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, "token: " + token);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_NNDM_SYSTEM_ID_GET_DECRYPTION, mDevice.getSystemId().replace("0x", "")));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }
}
