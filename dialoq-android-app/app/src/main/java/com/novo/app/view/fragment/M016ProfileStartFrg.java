package com.novo.app.view.fragment;

import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.presenter.M016ProfileStartPresener;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM016ProfileStartCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

public class M016ProfileStartFrg extends BaseFragment<M016ProfileStartPresener, OnHomeBackToView> implements OnM016ProfileStartCallBack {

    public static final String TAG = M016ProfileStartFrg.class.getName();

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m016_graphic_profile);
        TextView mTvWeWould = findViewById(R.id.tv_m016_we_would_like, CAApplication.getInstance().getRegularFont());
        TextView mTvKnowMore = findViewById(R.id.tv_m016_knowing_more, CAApplication.getInstance().getRegularFont());
        if (!getStorage().isMature()) {
            mTvWeWould.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m016_we_would_like_immature)));
            mTvKnowMore.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m016_knowing_more)));
        }
        findViewById(R.id.bt_m016_ok, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m016_not_now, this, CAApplication.getInstance().getMediumFont());
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m016_profile_start;
    }

    @Override
    protected M016ProfileStartPresener getPresenter() {
        return new M016ProfileStartPresener(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //Wait for customer confirmation
    }

    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m016_ok:
                mCallBack.showFrgScreen(TAG, M018ProfileGenderFrg.TAG);
                break;
            case R.id.tv_m016_not_now:
                getStorage().getM001ProfileEntity().setGender(new TextEntity("", ""));
                getStorage().getM001ProfileEntity().setTypeOfDiabetes(new TextEntity("", ""));
                getStorage().getM001ProfileEntity().setYearDiabetes(new TextEntity("", ""));
                mCallBack.showFrgScreen(TAG, M021SignUpProfileThinkFrg.TAG);
                break;
            default:
                break;
        }
    }
}
