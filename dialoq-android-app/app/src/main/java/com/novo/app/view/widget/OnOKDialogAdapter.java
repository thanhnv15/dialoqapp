package com.novo.app.view.widget;

import com.novo.app.view.event.OnOKDialogCallBack;

public abstract class OnOKDialogAdapter implements OnOKDialogCallBack {
    @Override
    public void handleOKButton1() {

    }

    @Override
    public void handleOKButton2() {

    }

    @Override
    public void handleDialogDismiss(){

    }
}
