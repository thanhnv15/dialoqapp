package com.novo.app.presenter;

import com.novo.app.view.event.OnM071SupportInstructionCallBack;

public class M071SupportInstructionPresenter extends BasePresenter<OnM071SupportInstructionCallBack>{
    public M071SupportInstructionPresenter(OnM071SupportInstructionCallBack event) {
        super(event);
    }
}
