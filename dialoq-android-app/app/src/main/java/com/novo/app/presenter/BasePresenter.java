/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RDoseEntity;
import com.novo.app.manager.entities.RInjectionItem;
import com.novo.app.manager.entities.RTokenEntity;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.ResultInfo;
import com.novo.app.model.entities.TokenEntity;
import com.novo.app.service.CARequest;
import com.novo.app.service.WebServiceAdapter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.utils.StorageCommon;
import com.novo.app.view.event.OnActionCallBack;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.widget.NVDateUtils;
import com.novo.app.view.widget.ProgressLoading;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import okhttp3.Response;

import static com.novo.app.presenter.M025SubDoseLogPresenter.KEY_SAVE_CALLING_AS5;
import static com.novo.app.service.SyncScheduleService.KEY_SYNC_READY;
import static com.novo.app.utils.CommonUtil.P_API_KEY;
import static com.novo.app.utils.CommonUtil.wtfi;

/**
 * This app use MVP pattern for all major view component such as fragments & dialogs
 * This class is the prototype MVP `Presenter` for all corresponding `View` instances mentioned above
 * <p>
 * To communicate back to `View` instance, an object of {@link OnCallBackToView} must be provided
 * for this presenter
 * <p>
 * This class will handle common business logic which are basics for all places, avoiding boilerplate
 * code duplication, such as common httq request results handler, common error code handler, etc...
 * If there are different handling logic required, inherited classes must override default methods of
 * this class
 *
 * @param <T> specific type of {@link OnCallBackToView}, which is the delegate for `View` object
 */
public abstract class BasePresenter<T extends OnCallBackToView> implements WebServiceAdapter
        .OnRequestCallBackListener {

    /**
     * Common errors of http requests
     */
    public static final String ERR_500 = "ERR_500";
    public static final String ERR_NO_CONNECTION = "ERR_NO_INTERNET_CONNECTION";
    public static final String KEY_TOKEN_EXPIRED = "KEY_TOKEN_EXPIRED";
    static final String LOG_TEXT_TOKEN = "token: ";

    /**
     * Token refreshing & validating used in mostly presenter
     * So we put them here for avoiding using of boilerplate codes
     */
    private static final String KEY_API_IA02_REFRESH_TOKEN = "KEY_API_IA02_REFRESH_TOKEN";
    private static final String URL_API_IA02_REFRESH_TOKEN = "user-service/api/v1/authenticate?";
    private static final String BODY_IA02 = "{\"userName\": %s,\"password\": %s,\"delivery\": \"SMS\"}";

    // String tag
    private static final String TAG = BasePresenter.class.getName();

    // Flag indicating if refreshing token should be retried or not
    private static final String VALUE_FALSE = Boolean.FALSE + "";
    private static final String VALUE_TRUE = Boolean.TRUE + "";

    // Delegate for `View` MVP object
    transient T mListener;

    // Tag of request to identify which request result is for which request
    String tagRequest;

    /**
     * Constructor
     *
     * @param event delegate for `View` MVP object
     */
    public BasePresenter(T event) {
        mListener = event;
    }

    /**
     * Hanldle success response from a web request
     *
     * @param tag       identifier of sent request
     * @param data      response data for the sent request
     * @param requestId id of the request, refer to BI services for more detail
     * @param token     token used in the request
     */
    @Override
    public final void doSuccess(String tag, Response data, String requestId, String token) {
        CommonUtil.wtfi(TAG, "doSuccess .." + tag);
        //CommonUtil.wtfi(TAG, "doSuccess .." + data + requestId);
        tagRequest = tag;

        if (data == null) {
            doFailed(tag, null, -1, "");
            removeRequestInQueue(requestId);
            return;
        }

        try {
            String text = data.body().string();
            if (data.code() != 200 && data.code() != 201) {
                //IMPORTANT: work around to communicating with Dialoq Board
                if (data.code() == 401
                        && !tag.equals(M025SubDoseLogPresenter.KEY_API_NNDM_SYSTEM_ID_GET_DECRYPTION)
                        && !tag.equals(M023LoginPresenter.KEY_API_IA02_AUTHENTICATE_USER)) {
                    if (tag.equals(KEY_API_IA02_REFRESH_TOKEN)) {
                        //This is the case when the account has been deleted or the password has been reset on other device
                        if (getStorage().getCredentialLost() != null) {
                            getStorage().getCredentialLost().onCallBack(null);
                        }
                        return;
                    }
                    doTokenExpired();
                    return;
                }
                doFailed(tag, null, data.code(), text);
                removeRequestInQueue(requestId);
                return;
            }

            //for debug
            //CommonUtil.wtfi(TAG, text);
            handleTokenExpired(text);
            if (!remainExpired())
                CommonUtil.getInstance().savePrefContent(CommonUtil.KEY_RETRYING_EXPIRE_REQUEST, VALUE_FALSE);
            handleSuccess(text, tag);
            removeRequestInQueue(requestId);
        } catch (Exception e) {
            e.printStackTrace();
            doFailed(tag, e, -1, "");
            removeRequestInQueue(requestId);
            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
        }
    }

    /**
     * Tell whether newest token is still expired one
     *
     * @return true if token is expired
     */
    private boolean remainExpired() {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST) return false;
        Set<String> keySet = getStorage().getQueueRequest().keySet();
        for (String key : keySet) {
            CARequest request = (CARequest) getStorage().getQueueRequest(key)[0];
            if (request.isExpired()) return true;
        }
        return false;
    }

    /**
     * Handler should be called when token expired is detected
     *
     * @param data expired token
     */
    private void handleTokenExpired(String data) {
        //CommonUtil.wtfi(TAG, "handleTokenExpired-DATA: " + data);
        if (KEY_API_IA02_REFRESH_TOKEN.equals(tagRequest)) {
            prepareRefreshToken(data);
        }
    }

    /**
     * <b>prepareRefreshToken</b><br/>
     * Create a new API request which has expired before to call to server
     *
     * @param data String expired token
     */
    private void prepareRefreshToken(String data) {
        CADBManager.getInstance().updateRTokenEntity(data);

        CommonUtil.wtfi(TAG, "prepareIA02..." + data);
        TokenEntity tokenEntity = generateData(TokenEntity.class, data);
        if (tokenEntity == null) {
            ProgressLoading.dismiss();
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.getInstance().savePrefContent(CommonUtil.TOKEN_KEY, tokenEntity.getToken());
        CommonUtil.getInstance().savePrefContent(CommonUtil.KEY_RETRYING_EXPIRE_REQUEST, VALUE_TRUE);
        wtfi(TAG, "Refresh token success");
        //call all api in queue which has expired flag is true
        retryExpiredRequest();
    }

    private void retryExpiredRequest() {
        Set<String> keySet = getStorage().getQueueRequest().keySet();
        for (String key : keySet) {
            CARequest request = (CARequest) getStorage().getQueueRequest(key)[0];
            if (!request.isExpired()) continue;

            WebServiceAdapter.OnRequestCallBackListener event = (WebServiceAdapter.OnRequestCallBackListener) getStorage().getQueueRequest(key)[1];
            request.setExpired(false);
            CAApplication.getInstance().callRequest(request, event);
        }

        CommonUtil.getInstance().savePrefContent(CommonUtil.KEY_RETRYING_EXPIRE_REQUEST, VALUE_FALSE);
    }

    /**
     * <b>removeRequestInQueue</b> <br/>
     * Remove the request from Queue after the request was re-new with the active token
     *
     * @param requestId String
     */
    private void removeRequestInQueue(String requestId) {
        getStorage().removeQueueRequest(requestId);
    }

    /**
     * doTokenExpired
     * <br/>Handle when token is expired, there some action will be done here:
     * <br/>1. Refresh token in-case user keep checking on the "KeepMeLogin" checkbox
     * <br/>2. Call back to the View to force log out in-case  "KeepMeLogin" is not working.
     *
     * @see BasePresenter#isKeepLoggedIn()
     * @see BasePresenter#refreshToken()
     */
    private void doTokenExpired() {
        ProgressLoading.dismiss();
        CommonUtil.getInstance().clearPrefContent(KEY_SYNC_READY);
        CommonUtil.getInstance().clearPrefContent(KEY_SAVE_CALLING_AS5);

        if (isKeepLoggedIn()) {
            refreshToken();
            return;
        }

        if (!getStorage().isStateTokenExpired()) {
            getStorage().saveStateTokenExpired(true);
            HashMap<String, OnActionCallBack> callBacks = getStorage().getHomeActionCallBack();
            if (callBacks != null) {
                for (HashMap.Entry<String, OnActionCallBack> entry : callBacks.entrySet()) {
                    String key = entry.getKey();
                    String sms = "Your session has been expired. Please re-login.";
                    Objects.requireNonNull(callBacks.get(key)).onCallBack(KEY_TOKEN_EXPIRED + sms);
                }
            }
        }
    }

    /**
     * <b>refreshToken</b><br/>
     * This method creates a API request to call IA02 for purpose: Re-authenticate account to server.
     */
    private void refreshToken() {
        RTokenEntity rTokenEntity = CADBManager.getInstance().getRTokenEntity();
        CommonUtil.wtfi(TAG, "callIA02RefreshToken");

        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_IA02_REFRESH_TOKEN);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addPathSegment(URL_API_IA02_REFRESH_TOKEN);

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());

        request.addBody(generateJson(BODY_IA02, rTokenEntity.getUserName(), rTokenEntity.getPassword()));

        //call request
        CAApplication.getInstance().callRequest(request, this);

    }

    /**
     * <b>isKeepLoggedIn</b><br/>
     * <br/>This method will validate if the login section of the user are still valid or meet the expire date
     *
     * @return True if "KeepMeLogin" is still working.<br/>False if exceed maximum days(refer the days get from the config set)
     */
    private boolean isKeepLoggedIn() {
        String keep = CommonUtil.getInstance().getPrefContent(CommonUtil.KEEP);
        String start = CommonUtil.getInstance().getPrefContent(CommonUtil.LOGIN_SECTION_START_DATE);
        if (keep == null
                || !keep.equals(CommonUtil.KEEP)
                || start == null
                || start.equals(CommonUtil.LOGIN_SECTION_DO_NOT_KEEP)
                || getStorage().getM001ConfigSet() == null) {
            return false;
        } else {
            String today = CommonUtil.getDateNow(CommonUtil.DATE_NOW_DY);
            String endDate = CommonUtil.getDateNow(CommonUtil.DATE_NOW_DY, start, getStorage().getM001ConfigSet().getMaximumSection() - 1);
            Date current = NVDateUtils.stringToDateLocal(today, CommonUtil.DATE_NOW_DY);
            Date end = NVDateUtils.stringToDateLocal(endDate, CommonUtil.DATE_NOW_DY);
            return current.compareTo(end) < 0;
        }
    }

    /**
     * Handle success response data of requested server requests
     * If this function get called, that means the response is valid and success
     *
     * @param data response data from server
     * @see #handleSuccess(String, String) for the scenario
     * <p>
     * Every server request will be sent by this base class, also, this class will first
     * handle responses of those requests for common errors
     * In case of success responses inherited classes must override this method for handling
     */
    protected void handleSuccess(String data, String tag) {
        // do nothing
    }

    protected final void showNotify(int text) {
        if (mListener != null) mListener.showAlertDialog(text);
    }

    protected final void showNotify(String text) {
        if (text == null) return;
        if (mListener != null) mListener.showAlertDialog(text);
    }

    /**
     * Handle error response of requested server requests
     * If this function get called, that means the response is invalid and/or some error has occurred
     *
     * @see com.novo.app.service.WebServiceAdapter.OnRequestCallBackListener#doFailed(String, Exception, int, String)
     * <p>
     * Every server request will be sent by this base class, also, this class will first
     * handle responses of those requests for common errors
     * In case of specific error handler, inherited classes must override this method for handling
     */
    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        tagRequest = tag;
        CommonUtil.wtfe(TAG, "doFailed .." + obj);
        String txtErr = LangMgr.getInstance().getLangList().get(ERR_500);
        mListener.showAlertDialog(txtErr);
    }

    @Override
    public final void showLockDialog() {
        CommonUtil.wtfd(TAG, "showLockDialog ..");
        mListener.showLockDialog();
    }

    @Override
    public void hideLockDialog(String key) {
        CommonUtil.wtfe(TAG, "hideLockDialog..." + key);
        ProgressLoading.dismiss();
    }

    final <J> J generateData(Class<J> tokenModelClass, String data) {
        return CommonUtil.generateData(tokenModelClass, data);
    }

    public final StorageCommon getStorage() {
        return CAApplication.getInstance().getStorageCommon();
    }

    String generateJson(String body, Object... data) {
        try {
            for (int i = 0; i < data.length; i++) {
                if (data[i] instanceof String) {
                    data[i] = "\"" + data[i] + "\"";
                }
            }
            String out = String.format(Locale.US, body, data);
            CommonUtil.wtfi(TAG, "BODY: " + out);
            return out;
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
        }
        return null;
    }

    synchronized void extractDeviceList(int assessmentId, List<RDoseEntity> mListDataInfo) {
        if (CommonUtil.mDebugType == CommonUtil.LOG_TYPE.TEST
                && !getStorage().getM025ListDevices().isEmpty()) {
            return;
        }

        List<DeviceEntity> mListTemp = new ArrayList<>();
        for (int i = 0; i < mListDataInfo.size(); i++) {
            DeviceEntity deviceEntity = extractDevice(mListDataInfo.get(i));
            deviceEntity.setAssessmentId(assessmentId + "");
            mListTemp.add(deviceEntity);
        }

        getStorage().getM025ListDevices().clear();
        getStorage().getM025ListDevices().addAll(mListTemp);
    }

    private synchronized DeviceEntity extractDevice(RDoseEntity dataInfo) {
        DeviceEntity deviceEntity = new DeviceEntity();
        int drugCode = dataInfo.getCustom3() == null ? DeviceEntity.CODE_UNKNOWN : Integer.parseInt(dataInfo.getCustom3());
        String deviceId = dataInfo.getCustom1();
        String reportedDate = dataInfo.getReportedDate();
        String lastUse = CommonUtil.convertToLocalDate(dataInfo.getCustom4());
        lastUse = NVDateUtils.getGroupItemTimeTitle(NVDateUtils.TODAY_AT, NVDateUtils.YESTERDAY_AT, lastUse);

        String lastDate = CommonUtil.convertToLocalDate(reportedDate);
        lastDate = NVDateUtils.getGroupItemTimeTitle(NVDateUtils.TODAY_AT, NVDateUtils.YESTERDAY_AT, lastDate);

        deviceEntity.setAssessmentId(dataInfo.getAssessmentId());
        deviceEntity.setDeviceID(deviceId);
        deviceEntity.setDrugCode(drugCode);

        if (drugCode == DeviceEntity.CODE_TRESIBA_100 || drugCode == DeviceEntity.CODE_TRESIBA_200) {
            getStorage().setTresibaExist(true);
        }

        //is tresiba
        deviceEntity.setTresiba(dataInfo.getResult() == 1);

        deviceEntity.setDrugType(drugCode);
        deviceEntity.setLastPair(lastDate);
        deviceEntity.setLastUse(lastUse);

        deviceEntity.setFlagCode(dataInfo.getDeviceId());
        deviceEntity.setSystemId(dataInfo.getNotes());

        deviceEntity.setLastPairNative(reportedDate);
        deviceEntity.setResultId(dataInfo.getResultId());
        deviceEntity.setNotes(dataInfo.getNotes());
        return deviceEntity;
    }

    synchronized DeviceEntity extractDevice(ResultInfo dataInfo) {
        DeviceEntity deviceEntity = new DeviceEntity();
        int drugCode = dataInfo.getCustom3() == null ? DeviceEntity.CODE_UNKNOWN : Integer.parseInt(dataInfo.getCustom3());
        String deviceId = dataInfo.getCustom1();
        String reportedDate = dataInfo.getReportedDate();
        String lastUse = CommonUtil.convertToLocalDate(dataInfo.getCustom4());
        lastUse = NVDateUtils.getGroupItemTimeTitle(NVDateUtils.TODAY_AT, NVDateUtils.YESTERDAY_AT, lastUse);

        String lastDate = CommonUtil.convertToLocalDate(reportedDate);
        lastDate = NVDateUtils.getGroupItemTimeTitle(NVDateUtils.TODAY_AT, NVDateUtils.YESTERDAY_AT, lastDate);

        deviceEntity.setAssessmentId(dataInfo.getAssessmentId());
        deviceEntity.setDeviceID(deviceId);
        deviceEntity.setDrugCode(drugCode);

        if (drugCode == DeviceEntity.CODE_TRESIBA_100 || drugCode == DeviceEntity.CODE_TRESIBA_200) {
            getStorage().setTresibaExist(true);
        }

        //is tresiba
        deviceEntity.setTresiba(dataInfo.getResult() == 1);

        deviceEntity.setDrugType(drugCode);
        deviceEntity.setLastPair(lastDate);
        deviceEntity.setLastUse(lastUse);

        deviceEntity.setFlagCode(dataInfo.getDeviceId());
        deviceEntity.setSystemId(dataInfo.getNotes());

        deviceEntity.setLastPairNative(reportedDate);
        deviceEntity.setResultId(dataInfo.getResultId());
        deviceEntity.setNotes(dataInfo.getNotes());
        return deviceEntity;
    }

    /**
     * This method will update info from a RInjectionItem to new DeviceEntity item
     *
     * @param dataInfo RInjectionItem
     * @return DeviceEntity
     */
    DeviceEntity updateDevice(RInjectionItem dataInfo) {
        DeviceEntity deviceEntity = new DeviceEntity();
        int drugCode = dataInfo.getCustom3() == null ? DeviceEntity.CODE_UNKNOWN : Integer.parseInt(dataInfo.getCustom3());
        String deviceId = dataInfo.getCustom1();
        String reportedDate = dataInfo.getReportedDate();
        String lastUse = CommonUtil.convertToLocalDate(dataInfo.getCustom4());
        lastUse = NVDateUtils.getGroupItemTimeTitle(NVDateUtils.TODAY_AT, NVDateUtils.YESTERDAY_AT, lastUse);

        String lastDate = CommonUtil.convertToLocalDate(reportedDate);
        lastDate = NVDateUtils.getGroupItemTimeTitle(NVDateUtils.TODAY_AT, NVDateUtils.YESTERDAY_AT, lastDate);

        deviceEntity.setAssessmentId(dataInfo.getAssessmentId());
        deviceEntity.setDeviceID(deviceId);
        deviceEntity.setDrugCode(drugCode);

        if (drugCode == DeviceEntity.CODE_TRESIBA_100 || drugCode == DeviceEntity.CODE_TRESIBA_200) {
            getStorage().setTresibaExist(true);
        }

        //is tresiba
        deviceEntity.setTresiba(dataInfo.getResult() == 1);

        deviceEntity.setDrugType(drugCode);
        deviceEntity.setLastPair(lastDate);
        deviceEntity.setLastUse(lastUse);

        deviceEntity.setFlagCode(dataInfo.getDeviceId());
        deviceEntity.setSystemId(dataInfo.getNotes());

        deviceEntity.setLastPairNative(reportedDate);
        deviceEntity.setResultId(dataInfo.getResultId());
        deviceEntity.setNotes(dataInfo.getNotes());
        return deviceEntity;
    }

    protected String getAssessmentId(String assessmentType) {
        CarePlantUserEntity entity = getStorage().getM001CarePlanUserEntity();
        String assessmentId = "";

        for (int i = 0; i < entity.getData().size(); i++) {
            List<CarePlantUserEntity.AssessmentInfo> listAssessment = entity.getData().get(i).getAssessments();
            for (int j = 0; j < listAssessment.size(); j++) {
                if (listAssessment.get(j).getType().equals(assessmentType)) {
                    assessmentId = listAssessment.get(j).getAssessmentId();
                    return assessmentId;
                }
            }
        }
        return assessmentId;
    }
}
