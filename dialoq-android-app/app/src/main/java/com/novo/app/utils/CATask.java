package com.novo.app.utils;

import android.os.AsyncTask;

import com.novo.app.view.event.OnCallBackToView;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

public final class CATask<K, G extends CATask.OnTaskCallBackToView> extends AsyncTask<String, Object, Object> {
    private static final long SLEEP_5000_SECONDS = 5000;
    private static final String TAG = CATask.class.getName();
    private WeakReference<K> father;
    private G mCallBack;
    private long sleepTime;
    private AtomicBoolean flag;

    public CATask(WeakReference<K> reference, G callBack) {
        this(reference, callBack, SLEEP_5000_SECONDS);
    }

    public CATask(WeakReference<K> reference, G callBack, long time) {
        mCallBack = callBack;
        father = reference;
        sleepTime = time;
    }

    /***
     * Just call method once time
     * @param value boolean
     */
    public void setLoop(boolean value) {
        if (flag == null) {
            flag = new AtomicBoolean(value);
        }
    }

    public void cancel() {
        if (flag == null) return;
        flag.set(false);
        mCallBack = null;
    }

    public K getFather() {
        return father == null ? null : father.get();
    }

    @Override
    protected Object doInBackground (String... data) {
        CommonUtil.wtfd(TAG, "doInBackground...start");
        if (flag == null && mCallBack != null) {
            return mCallBack.doBGTask(data[0]);
        }

        while (flag != null && mCallBack != null && flag.get() && getFather() != null) {
            if (father != null && father.get() == null) {
                cancel();
                return null;
            }
            if (data != null && data.length > 0) {
                mCallBack.doBGTask(data[0]);
            } else {
                mCallBack.doBGTask(null);
            }
            try {
                Thread.sleep(sleepTime);
            } catch (Exception e) {
                CommonUtil.wtfe(TAG, e.getLocalizedMessage());
                return null;
            }
        }
        CommonUtil.wtfd(TAG, "doInBackground...done");
        return null;
    }

    public void updateUI(Object... data) {
        publishProgress(data);
    }

    @Override
    protected void onProgressUpdate(Object... values) {
        if (mCallBack != null) {
            mCallBack.updateUITask(values);
        }
    }

    @Override
    protected void onPostExecute(Object data) {
        if (mCallBack != null) {
            mCallBack.doneTask(data);
        }
    }

    public interface OnTaskCallBackToView extends OnCallBackToView {
        Object doBGTask(String key) ;

        default void updateUITask(Object[] values){}

        default void doneTask(Object data){}
    }
}
