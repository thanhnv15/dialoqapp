package com.novo.app.view.event;

public interface OnM025SubDoseLogCallBack extends OnCallBackToView {
    default void dataAS05Ready() {

    }

    default void closeRefresh() {

    }

    default void toIncompleteDoseDetail(Object tag) {

    }

    default void toDetailNote(Object tag) {

    }

    void refreshData();
    void reloadData();

    default void toReminderPausedDetail(Object tag){}

    void setReminderNotification(String minDate, String maxDate);

    default void getAS5NoteReady(){}
}
