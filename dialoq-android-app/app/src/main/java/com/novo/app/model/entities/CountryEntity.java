package com.novo.app.model.entities;

import java.io.Serializable;

public class CountryEntity implements Serializable {
    private TextEntity countryCode;
    private TextEntity countryName;
    private TextEntity adultTerm;
    private TextEntity adultVersion;
    private TextEntity adultUpdate;
    private TextEntity childTerm;
    private TextEntity childVersion;
    private TextEntity childUpdate;
    private TextEntity age;
    private TextEntity minAge;

    public CountryEntity(TextEntity... textEntities) {
        if (textEntities == null || textEntities.length == 0) return;
        this.countryCode = textEntities[0];

        if (textEntities.length > 1) {
            this.countryName = textEntities[1];
        }
        if (textEntities.length > 2) {
            this.adultTerm = textEntities[2];
        }
        if (textEntities.length > 3) {
            this.adultVersion = textEntities[3];
        }
        if (textEntities.length > 4) {
            this.adultUpdate = textEntities[4];
        }
        if (textEntities.length > 5) {
            this.childTerm = textEntities[5];
        }
        if (textEntities.length > 6) {
            this.childVersion = textEntities[6];
        }
        if (textEntities.length > 7) {
            this.childUpdate = textEntities[7];
        }
        if (textEntities.length > 8) {
            this.age = textEntities[8];
        }
        if (textEntities.length > 9) {
            this.minAge = textEntities[9];
        }
    }

    public TextEntity getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(TextEntity countryCode) {
        this.countryCode = countryCode;
    }

    public TextEntity getCountryName() {
        return countryName;
    }

    public void setCountryName(TextEntity countryName) {
        this.countryName = countryName;
    }

    public TextEntity getAdultTerm() {
        return adultTerm;
    }

    public void setAdultTerm(TextEntity adultTerm) {
        this.adultTerm = adultTerm;
    }

    public TextEntity getAdultVersion() {
        return adultVersion;
    }

    public void setAdultVersion(TextEntity adultVersion) {
        this.adultVersion = adultVersion;
    }

    public TextEntity getAdultUpdate() {
        return adultUpdate;
    }

    public void setAdultUpdate(TextEntity adultUpdate) {
        this.adultUpdate = adultUpdate;
    }

    public TextEntity getChildTerm() {
        return childTerm;
    }

    public void setChildTerm(TextEntity childTerm) {
        this.childTerm = childTerm;
    }

    public TextEntity getChildVersion() {
        return childVersion;
    }

    public void setChildVersion(TextEntity childVersion) {
        this.childVersion = childVersion;
    }

    public TextEntity getChildUpdate() {
        return childUpdate;
    }

    public void setChildUpdate(TextEntity childUpdate) {
        this.childUpdate = childUpdate;
    }

    public TextEntity getAge() {
        return age;
    }

    public void setAge(TextEntity age) {
        this.age = age;
    }

    public TextEntity getMinAge() {
        return minAge;
    }

    public void setMinAge(TextEntity minAge) {
        this.minAge = minAge;
    }
}
