package com.novo.app.presenter;

import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RReminderEntity;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM065MoreCallBack;
import com.novo.app.view.widget.ProgressLoading;

public class M065MorePresenter extends BasePresenter<OnM065MoreCallBack> {
    private static final String TAG = M065AddEditReminderPresenter.class.getName();


    public M065MorePresenter(OnM065MoreCallBack event) {
        super(event);
    }

    void prepareAS10() {
        CommonUtil.wtfi(TAG, "prepareAS10...");
        RReminderEntity entity = CADBManager.getInstance().getRReminderEntity();
        if (entity == null) {
            mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            ProgressLoading.dismiss();
            return;
        }

        getStorage().setM065ScheduleEntity(entity);
        mListener.changeState();
    }

    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tagRequest);

        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        String sms = LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON);
        if (responseEntity != null) {
            mListener.showAlertDialog(sms);
        } else if (CommonUtil.getInstance().isConnectToNetwork()){
            showNotify(LangMgr.getInstance().getLangList().get(ERR_NO_CONNECTION));
        }

    }

    public void activeReminder(String time, String activeState) {
        RReminderEntity entity = CADBManager.getInstance().getRReminderEntity();
        CommonUtil.wtfi(TAG, "cancelReminder: RReminderEntity " + entity);
        if (entity == null) {
            return;
        }
        String scheduleNotificationId = getStorage().getM065ScheduleEntity()
                .getScheduleNotificationId();

        CommonUtil.getInstance().prepareReminder(entity, time, scheduleNotificationId);
        if (CADBManager.getInstance().editRReminderEntity(entity)) {
            prepareAS10();
        }
    }
}
