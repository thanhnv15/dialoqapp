package com.novo.app.view.fragment;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M026SupportPresenter;
import com.novo.app.view.base.BaseActivity;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM026SupportCallBack;

public class M026SupportFrg extends BaseFragment<M026SupportPresenter, OnM024DoseLogCallBack> implements OnM026SupportCallBack {
    public static final String TAG = M026SupportFrg.class.getName();

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m026_support_title, this, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m026_support_dialoq, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m026_support_about, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.iv_m026_support_about, this);
        findViewById(R.id.tv_m026_support_instructions, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.iv_m026_support_instructions, this);
        findViewById(R.id.tv_m026_support_troubleshooting, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.iv_m026_support_troubleshooting, this);
        findViewById(R.id.tv_m026_support_additional, CAApplication.getInstance().getBoldFont());

        findViewById(R.id.tv_m026_support_video, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.iv_m026_support_video, this);
        findViewById(R.id.tv_m026_support_support, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.iv_m026_support_support, this);

        findViewById(R.id.tr_m026_support_about, this);
        findViewById(R.id.tr_m026_support_instructions, this);
        findViewById(R.id.tr_m026_support_troubleshooting, this);
        findViewById(R.id.tr_m026_support_video, this);
        findViewById(R.id.tr_m026_support_support, this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m026_support;
    }

    @Override
    protected M026SupportPresenter getPresenter() {
        return new M026SupportPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    @Override
    protected void setTagCurrentFrg() {
        ((BaseActivity) getActivity()).setTagCurrentFrg(M024DoseLogFrg.TAG);
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tr_m026_support_about:
            case R.id.tv_m026_support_about:
            case R.id.iv_m026_support_about:
                mCallBack.showChildFrgScreen(TAG, M046SupportAboutFrg.TAG);
                break;
            case R.id.tr_m026_support_instructions:
            case R.id.tv_m026_support_instructions:
            case R.id.iv_m026_support_instructions:
                mCallBack.showChildFrgScreen(TAG, M071SupportInstructionFrg.TAG);
                break;
            case R.id.tr_m026_support_troubleshooting:
            case R.id.tv_m026_support_troubleshooting:
            case R.id.iv_m026_support_troubleshooting:
                mCallBack.showChildFrgScreen(TAG, M047SupportTroubleshootingFrg.TAG);
                break;
            case R.id.tr_m026_support_video:
            case R.id.tv_m026_support_video:
            case R.id.iv_m026_support_video:
                mCallBack.showChildFrgScreen(TAG, M048SupportVideoFrg.TAG);
                break;
            case R.id.tr_m026_support_support:
            case R.id.tv_m026_support_support:
            case R.id.iv_m026_support_support:
                mCallBack.showChildFrgScreen(TAG, M049SupportContactFrg.TAG);
                break;
            default:
                break;
        }
    }

    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }
}
