package com.novo.app.model.entities;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.google.gson.annotations.SerializedName;
import com.novo.app.CAApplication;
import com.novo.app.utils.CommonUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConfigEntity implements Serializable {

    private static final String KEY_SUPPORT_LANGUAGE = "supported.language";
    private static final String KEY_DEFAULT_LANGUAGE = "default.language";
    private static final String KEY_CONFIRM_CODE = "confirmation.code";
    private static final String KEY_PASSWORD_MAX = "max.password";
    private static final String KEY_PASSWORD_MIN = "min.password";
    private static final String KEY_COUNTRY_LIST = "country.list";
    private static final String KEY_GENDER_LIST = "supported.gender";
    private static final String KEY_DIABETE_LIST = "supported.diabetes";
    private static final String KEY_DIABETE_YEAR_LIST = "diabetes.living.year.list";
    private static final String KEY_TROUBLE_SHOOTING_LIST = "SUPPORT.TROUBLESHOOTING";
    private static final String KEY_VIDEO_LIST = "SUPPORT.VIDEOS";

    private static final String KEY_BLACK_LIST_DEVICE = "blacklist.device.android";
    private static final String KEY_BLACK_LIST_COUNTRIES = "blacklist.countries";
    private static final String KEY_BLACK_LIST_COUNTRIES_AND_VERSION = "blacklist.country.and.dialoq.version.android";
    private static final String KEY_BLACK_LIST_ALL = "blacklist.all";
    private static final String KEY_BLACK_LIST_VERSION = "blacklist.dialoq.version.android";

    private static final String KEY_WHITE_LIST_DEVICE = "whitelist.device.android";
    private static final String KEY_WHITE_LIST_COUNTRIES = "whitelist.countries";
    private static final String KEY_WHITE_LIST_ALL = "whitelist.all";
    private static final String KEY_MAX_DEVICE_PAIR = "MORE.DEVICE.MAX.DEVICE.PAIR";
    private static final String KEY_VERSION_APP = "android.version";
    private static final String KEY_MAXIMUM_SECTION = "login.remember.me.max.days.number";
    private static final String KEY_LOW_NUMBER = "memory.low.number";
    private static final String KEY_PRIVACY = "privacy.version";

    private static final String KEY_INTRO_VIDEO = "ONBOARDING.SIGNUP.VIDEO.SCREEN.VIDEOURL";


    private List<TextEntity> languageEntity = new ArrayList<>();
    private List<TextEntity> genderEntity = new ArrayList<>();
    private List<TextEntity> diabeteEntity = new ArrayList<>();
    private List<TextEntity> yearEntity = new ArrayList<>();
    private List<CountryEntity> countryEntity = new ArrayList<>();
    private List<BlacklistCountryEntity> blacklistCountryEntity = new ArrayList<>();
    private List<VersionEntity> versionEntity = new ArrayList<>();
    private List<CountryVersionEntity> countryVersionEntity = new ArrayList<>();

    private AcceptDeviceEntity blackList;
    private AcceptDeviceEntity whiteList;

    private List<TopicEntity> listQnA = new ArrayList<>();
    private List<String> listTopics = new ArrayList<>();

    private List<TopicEntity> listVideos = new ArrayList<>();
    private List<String> listVideoType = new ArrayList<>();

    private TextEntity defaultLanguage;
    private String confirmCode;
    private String versionApp;
    private String lowNumber;
    private String privacy;
    private int maxPass;
    private int minPass;
    private boolean isAppDisable;

    @SerializedName("configSetId")
    private String configSetId;
    @SerializedName("configCode")
    private String configCode;
    @SerializedName("category")
    private String category;
    @SerializedName("version")
    private String version;
    @SerializedName("description")
    private String description;
    @SerializedName("isPublic")
    private String isPublic;
    @SerializedName("isLatest")
    private String isLatest;
    @SerializedName("createdOn")
    private String createdOn;
    @SerializedName("createdBy")
    private String createdBy;
    @SerializedName("data")
    private List<DataEntity> dataEntity;
    private int maxPairDevices;
    private int maximumSection;

    private TextEntity mIFU;
    private TextEntity mIntroVideo;

    public void loadConfigSet() {
        //load config set
        if (!languageEntity.isEmpty()) return;
        languageEntity.clear();

        for (int i = 0; i < getDataEntity().size(); i++) {
            ConfigEntity.DataEntity entity = getDataEntity().get(i);

            //get language default
            if (entity.getKey().equals(ConfigEntity.KEY_DEFAULT_LANGUAGE)) {
                String[] keyDefault = entity.getValue().split("=");
                defaultLanguage = new TextEntity(keyDefault[0], keyDefault[1]);
                break;
            }
        }

        blackList = new AcceptDeviceEntity();
        whiteList = new AcceptDeviceEntity();

        for (int i = 0; i < getDataEntity().size(); i++) {
            ConfigEntity.DataEntity entity = getDataEntity().get(i);
            if (entity.getType()!=null && entity.getType().equalsIgnoreCase("PDF")) {
                mIFU = new TextEntity(entity.getValue(),"");
            }
            switch (entity.getKey()) {
                case KEY_BLACK_LIST_VERSION:
                    getVersionEntityList(entity);
                    break;
                case KEY_BLACK_LIST_COUNTRIES_AND_VERSION:
                    getCountryVersionEntityList(entity);
                    break;
                case KEY_VERSION_APP:
                    versionApp = entity.getValue();
                    break;
                case KEY_MAXIMUM_SECTION:
                    maximumSection = CommonUtil.getInt(entity.getValue());
                    break;
                case KEY_PRIVACY:
                    privacy = entity.getValue();
                    break;
                case KEY_LOW_NUMBER:
                    lowNumber = entity.getValue();
                    break;
                case KEY_MAX_DEVICE_PAIR:
                    maxPairDevices = CommonUtil.getInt(entity.getValue());
                    break;
                case KEY_SUPPORT_LANGUAGE:
                    getSupportLanguage(entity);
                    break;
                case KEY_CONFIRM_CODE:
                    confirmCode = entity.getValue();
                    break;
                case KEY_PASSWORD_MAX:
                    maxPass = CommonUtil.getInt(entity.getValue());
                    break;
                case KEY_PASSWORD_MIN:
                    minPass = CommonUtil.getInt(entity.getValue());
                    break;
                case KEY_COUNTRY_LIST:
                    getCountriesEntityList(entity);
                    break;
                case KEY_TROUBLE_SHOOTING_LIST:
                    String value = entity.getValue();
                    String[] dataItem = value.split(";");
                    listTopics.addAll(Arrays.asList(dataItem));
                    break;
                case KEY_BLACK_LIST_DEVICE:
                    blackList.setDevices(entity.value.toUpperCase());
                    break;
                case KEY_BLACK_LIST_COUNTRIES:
                    getBlacklistCountriesEntity(entity);
                    break;
                case KEY_BLACK_LIST_ALL:
                    blackList.setAll(entity.value.equalsIgnoreCase("TRUE"));
                    break;
                case KEY_WHITE_LIST_DEVICE:
                    whiteList.setDevices(entity.value.toUpperCase());
                    break;
                case KEY_WHITE_LIST_COUNTRIES:
                    whiteList.setCountries(entity.value.toUpperCase());
                    break;
                case KEY_WHITE_LIST_ALL:
                    whiteList.setAll(entity.value.equalsIgnoreCase("TRUE"));
                    break;
                case KEY_VIDEO_LIST:
                    String valueVideo = entity.getValue();
                    String[] dataItemVideo = valueVideo.split(";");
                    listVideoType.addAll(Arrays.asList(dataItemVideo));
                    break;
                case KEY_GENDER_LIST:
                    makeGenderList(entity);
                    break;
                case KEY_DIABETE_LIST:
                    makeDiabeteList(entity);
                    break;
                case KEY_DIABETE_YEAR_LIST:
                    makeDiabeteYearList(entity);
                    break;
                case KEY_INTRO_VIDEO:
                    mIntroVideo = new TextEntity(entity.getValue(),"");
                default:
                    break;
            }
            //make FAQ
            makeFAQ(entity);
            makeVideoList(entity);

            // Check for app disable from the ConfigSet
            if (entity.getKey().equals(ConfigEntity.KEY_BLACK_LIST_ALL)) {
                String value = entity.getValue();
                if (value.equals("true")) {
                    isAppDisable = true;
                    CommonUtil.wtfi("ConfigSet", "App is being disable");
                    // Add code to disable app here
                } else {
                    isAppDisable = false;
                    CommonUtil.wtfi("ConfigSet", "App is being enable");
                    // Add code to enable app here
                }
            }
        }
    }


    private void makeDiabeteYearList(DataEntity entity) {
        String value = entity.getValue();

        String[] dataItem = value.split(";");
        for (String aDataItem : dataItem) {
            String[] data = aDataItem.split("=");
            yearEntity.add(new TextEntity(data[0], data[1]));
        }
    }

    private void makeDiabeteList(DataEntity entity) {
        String value = entity.getValue();

        String[] dataItem = value.split(";");
        for (String aDataItem : dataItem) {
            String[] data = aDataItem.split("=");
            diabeteEntity.add(new TextEntity(data[0], data[1]));
        }
    }

    private void makeGenderList(DataEntity entity) {
        String value = entity.getValue();

        String[] dataItem = value.split(";");
        for (String aDataItem : dataItem) {
            String[] data = aDataItem.split("=");
            genderEntity.add(new TextEntity(data[0], data[1]));
        }
    }

    // list topic = string list
    private void makeFAQ(DataEntity entity) {
        for (int k = 0; k < listTopics.size(); k++) {
            String keyTopic = listTopics.get(k);
            if (entity.getKey().equals(keyTopic)) {
                String vl = entity.getValue();

                String[] dataQ = vl.split(";");
                List<TextEntity> listItemQA = new ArrayList<>();
                for (String item : dataQ) {
                    String[] data = item.split("=");
                    listItemQA.add(new TextEntity(data[0], data[1]));
                    CommonUtil.wtfi("listItemQA.....", String.valueOf(listItemQA.size()));
                }
                if (listItemQA.size() == 1) {
                    listQnA.add(new TopicEntity(keyTopic, listItemQA, false));
                } else {
                    listQnA.add(new TopicEntity(keyTopic, listItemQA, true));
                }
            }
        }
    }

    private void makeVideoList(DataEntity entity) {
        for (int v = 0; v < listVideoType.size(); v++) {
            String keyVideoType = listVideoType.get(v);
            if (entity.getKey().equals(keyVideoType)) {
                String vl = entity.getValue();
                String[] dataV = vl.split(";");
                List<TextEntity> listItemVideo = new ArrayList<>();
                for (String item : dataV) {
                    String[] data = item.split("=");
                    listItemVideo.add(new TextEntity(data[0], data[1]));
                }
                listVideos.add(new TopicEntity(keyVideoType, listItemVideo));
            }
        }
    }

    private void getCountriesEntityList(DataEntity entity) {
        String value = entity.getValue();

        String[] dataItem = value.split(";");
        for (String s : dataItem) {
            String[] field = s.split(",");
            List<TextEntity> fieldList = new ArrayList<>();
            for (String s1 : field) {
                String[] data = s1.split("=");
                fieldList.add(new TextEntity(data[0], data[1]));
            }

            TextEntity[] arrData = new TextEntity[fieldList.size()];
            fieldList.toArray(arrData);
            countryEntity.add(new CountryEntity(arrData));
        }
    }

    private void getBlacklistCountriesEntity(DataEntity entity) {
        String value = entity.getValue();

        String[] dataItem = value.split(";");
        for (String s : dataItem) {
            String[] field = s.split(",");
            List<TextEntity> fieldList = new ArrayList<>();
            for (String s1 : field) {
                String[] data = s1.split("=");
                fieldList.add(new TextEntity(data[0], data[1]));
            }
            blacklistCountryEntity.add(new BlacklistCountryEntity(fieldList.get(0)));
        }
        blackList.setCountries(entity.value.toUpperCase());
    }

    private void getVersionEntityList(DataEntity entity) {
        String value = entity.getValue();

        String[] dataItem = value.split(";");
        for (String s : dataItem) {
            String[] field = s.split(",");
            List<TextEntity> fieldList = new ArrayList<>();
            for (String s1 : field) {
                String[] data = s1.split("=");
                fieldList.add(new TextEntity(data[0], data[1]));
            }
            versionEntity.add(new VersionEntity(fieldList.get(0), fieldList.get(1)));
        }
        blackList.setVersion(entity.value.toUpperCase());
    }

    private void getCountryVersionEntityList(DataEntity entity) {
        String value = entity.getValue();

        String[] dataItem = value.split(";");
        for (String s : dataItem) {
            String[] field = s.split(",");
            List<TextEntity> fieldList = new ArrayList<>();
            for (String s1 : field) {
                String[] data = s1.split("=");
                fieldList.add(new TextEntity(data[0], data[1]));
            }
            countryVersionEntity.add(new CountryVersionEntity(fieldList.get(0), fieldList.get(1), fieldList.get(2)));
        }
        blackList.setVersion(entity.value.toUpperCase());
        blackList.setCountries(entity.value.toUpperCase());
    }

    private void getSupportLanguage(DataEntity entity) {
        String value = entity.getValue();

        String[] dataItem = value.split(";");
        for (String aDataItem : dataItem) {
            String[] data = aDataItem.split("=");
            languageEntity.add(new TextEntity(data[0], data[1]));
        }
    }

    public String getPrivacy() {
        return privacy;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private List<DataEntity> getDataEntity() {
        return dataEntity;
    }

    public void setDataEntity(List<DataEntity> dataEntity) {
        this.dataEntity = dataEntity;
    }

    public List<TextEntity> getLanguageEntity() {
        return languageEntity;
    }

    public void setLanguageEntity(List<TextEntity> languageEntity) {
        this.languageEntity = languageEntity;
    }

    public List<CountryEntity> getCountryEntity() {
        return countryEntity;
    }

    public void setCountryEntity(List<CountryEntity> countryEntity) {
        this.countryEntity = countryEntity;
    }

    public List<TopicEntity> getListQnA() {
        return listQnA;
    }

    public void setListQnA(List<TopicEntity> listQnA) {
        this.listQnA = listQnA;
    }

    public TextEntity getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(TextEntity defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public AcceptDeviceEntity getBlackList() {
        return blackList;
    }

    public void setBlackList(AcceptDeviceEntity blackList) {
        this.blackList = blackList;
    }

    public AcceptDeviceEntity getWhiteList() {
        return whiteList;
    }

    public void setWhiteList(AcceptDeviceEntity whiteList) {
        this.whiteList = whiteList;
    }

    public List<String> getListTopics() {
        return listTopics;
    }

    public void setListTopics(List<String> listTopics) {
        this.listTopics = listTopics;
    }

    public String getConfirmCode() {
        return confirmCode;
    }

    public void setConfirmCode(String confirmCode) {
        this.confirmCode = confirmCode;
    }

    public int getMaxPass() {
        return maxPass;
    }

    public void setMaxPass(int maxPass) {
        this.maxPass = maxPass;
    }

    public int getMinPass() {
        return minPass;
    }

    public void setMinPass(int minPass) {
        this.minPass = minPass;
    }

    public boolean isAppDisable() {
        return isAppDisable;
    }

    public void setAppDisable(boolean appDisable) {
        isAppDisable = appDisable;
    }

    public String getConfigSetId() {
        return configSetId;
    }

    public void setConfigSetId(String configSetId) {
        this.configSetId = configSetId;
    }

    public String getConfigCode() {
        return configCode;
    }

    public void setConfigCode(String configCode) {
        this.configCode = configCode;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(String isPublic) {
        this.isPublic = isPublic;
    }

    public String getIsLatest() {
        return isLatest;
    }

    public void setIsLatest(String isLatest) {
        this.isLatest = isLatest;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public List<TopicEntity> getListVideos() {
        return listVideos;
    }

    public void setListVideos(List<TopicEntity> listVideos) {
        this.listVideos = listVideos;
    }

    public List<String> getListVideoType() {
        return listVideoType;
    }

    public void setListVideoType(List<String> listVideoType) {
        this.listVideoType = listVideoType;
    }

    public int getMaxPairDevices() {
        return maxPairDevices;
    }

    public void setMaxPairDevices(int maxPairDevices) {
        this.maxPairDevices = maxPairDevices;
    }

    public List<TextEntity> getGenderEntity() {
        return genderEntity;
    }

    public void setGenderEntity(List<TextEntity> genderEntity) {
        this.genderEntity = genderEntity;
    }

    public List<TextEntity> getDiabeteEntity() {
        return diabeteEntity;
    }

    public void setDiabeteEntity(List<TextEntity> diabeteEntity) {
        this.diabeteEntity = diabeteEntity;
    }

    public List<TextEntity> getYearEntity() {
        return yearEntity;
    }

    public void setYearEntity(List<TextEntity> yearEntity) {
        this.yearEntity = yearEntity;
    }

    public boolean isNewVersion() {
        try {
            PackageInfo pInfo = CAApplication.getInstance().getPackageManager().getPackageInfo(CAApplication.getInstance().getPackageName(), 0);
            return pInfo.versionName.compareTo(versionApp) < 0;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isNotEnoughSpace() {
        return CommonUtil.getInstance().getFreeSpace() <= Long.parseLong(lowNumber);
    }

    public boolean isPrivacyWarning() {
        String savedPrivacy = CommonUtil.getInstance().getPrefContent(KEY_PRIVACY);
        CommonUtil.getInstance().savePrefContent(KEY_PRIVACY, privacy);
        return savedPrivacy == null || savedPrivacy.compareTo(privacy) < 0;
    }

    public List<VersionEntity> getVersionEntity() {
        return versionEntity;
    }

    public void setVersionEntity(List<VersionEntity> versionEntity) {
        this.versionEntity = versionEntity;
    }

    public List<BlacklistCountryEntity> getBlacklistCountryEntity() {
        return blacklistCountryEntity;
    }

    public void setBlacklistCountryEntity(List<BlacklistCountryEntity> blacklistCountryEntity) {
        this.blacklistCountryEntity = blacklistCountryEntity;
    }

    public List<CountryVersionEntity> getCountryVersionEntity() {
        return countryVersionEntity;
    }

    public void setCountryVersionEntity(List<CountryVersionEntity> countryVersionEntity) {
        this.countryVersionEntity = countryVersionEntity;
    }

    public TextEntity getmIFU() {
        return mIFU;
    }

    public TextEntity getmIntroVideo() {
        return mIntroVideo;
    }

    public int getMaximumSection() {
        return maximumSection;
    }

    public void setMaximumSection(int maximumSection) {
        this.maximumSection = maximumSection;
    }


    public class DataEntity implements Serializable {
        @SerializedName("type")
        private String type;
        @SerializedName("key")
        private String key;
        @SerializedName("value")
        private String value;
        @SerializedName("description")
        private String description;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
