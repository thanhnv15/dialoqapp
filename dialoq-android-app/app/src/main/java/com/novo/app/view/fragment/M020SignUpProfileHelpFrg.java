package com.novo.app.view.fragment;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M020SignUpPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.dialog.M051MorePrivacyPolicyDialog;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM020SignUpCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

import java.util.ArrayList;
import java.util.List;

public class M020SignUpProfileHelpFrg extends BaseFragment<M020SignUpPresenter, OnHomeBackToView> implements OnM020SignUpCallBack {

    public static final String TAG = M020SignUpProfileHelpFrg.class.getName();
    private M051MorePrivacyPolicyDialog morePrivacyDialog;
    private Button mBtNext;
    private CheckBox mCbAgree;
    private String privacyNotice;

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m020_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m020_description, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m020_cancel, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m020_agree, CAApplication.getInstance().getRegularFont());
        mCbAgree = findViewById(R.id.cb_m020_agree, this);

        mBtNext = findViewById(R.id.bt_m020_next, this, CAApplication.getInstance().getMediumFont());

        TextView mTvDetail = findViewById(R.id.tv_m020_detail, this, CAApplication.getInstance().getRegularFont());
        if (!getStorage().isMature()) {
            mTvDetail.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m020_detail_immature)));
        }
        privacyNotice = getString(R.string.lang_m020_detail_link);
        privacyNotice = LangMgr.getInstance().getLangList().get(privacyNotice);
        List<String> highlight = new ArrayList<>();
        highlight.add(privacyNotice);

        setClickableText(mTvDetail, highlight, R.color.colorM020Teal);
        getStorage().getM001ProfileEntity().setStreetAddress1(CommonUtil.getDateNow(CommonUtil.DATE_STYLE));
        validateTerm();
    }

    private void validateTerm() {
        if (mCbAgree.isChecked()) {
            mBtNext.setEnabled(true);
            return;
        }
        mBtNext.setEnabled(false);
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m020_signup_profile_help;
    }

    @Override
    protected M020SignUpPresenter getPresenter() {
        return new M020SignUpPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        // Wait for customer confirmation
    }

    @Override
    public void onLinkClicked() {
        if (getStorage().getLinkTag() != null && getStorage().getLinkTag().equals(privacyNotice)) {
            if (morePrivacyDialog == null) {
                morePrivacyDialog = new M051MorePrivacyPolicyDialog(mContext, true);
            }
            morePrivacyDialog.show();
        }
        getStorage().setLinkTag(null);
    }

    @Override
    public void backToPreviousScreen() {
        //This function isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m020_detail:
                onLinkClicked();
                break;
            case R.id.cb_m020_agree:
                validateTerm();
                break;
            case R.id.bt_m020_next:
                getStorage().getM001ProfileEntity().setAllErgies(CommonUtil.AGREE_VALUE);
                mCallBack.showFrgScreen(TAG, M016ProfileStartFrg.TAG);
                break;
            case R.id.tv_m020_cancel:
                getStorage().getM001ProfileEntity().setAllErgies(CommonUtil.NOT_AGREE_VALUE);
                mCallBack.showFrgScreen(TAG, M016ProfileStartFrg.TAG);
                break;
            default:
                break;
        }
    }
}
