package com.novo.app.view.fragment;

import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M064MorePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.dialog.M065MoreReminderAddDialog;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM064MoreCallBack;
import com.novo.app.view.event.OnM065ReminderCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.widget.ProgressLoading;

public class M064MoreReminderEmptyFrg extends BaseFragment<M064MorePresenter, OnM024DoseLogCallBack> implements OnM064MoreCallBack, OnM065ReminderCallBack {
    public static final String TAG = M064MoreReminderEmptyFrg.class.getName();
    private M065MoreReminderAddDialog mDialog;

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m064_back, this);
        findViewById(R.id.tv_m064_title, CAApplication.getInstance().getMediumFont());
        TextView mTvDescription = findViewById(R.id.tv_m064_description, CAApplication.getInstance().getRegularFont());
        String highLight = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m034_smart_reminder));
        highLightText(mTvDescription, highLight, R.color.colorM015DarkBlue, true);

        findViewById(R.id.tv_m064_detail_1, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m064_detail_2, CAApplication.getInstance().getRegularFont());

        findViewById(R.id.bt_m064_set, this, CAApplication.getInstance().getMediumFont());
    }

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m064_more_reminder_empty;
    }

    @Override
    protected M064MorePresenter getPresenter() {
        return new M064MorePresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This function isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        //This function isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m064_set:
                String title = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m064_more_reminder_empty_dialog_title));
                String msg = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m064_more_reminder_empty_dialog_message));
                String bt1 = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m064_more_reminder_empty_dialog_ok));
                String bt2 = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m064_more_reminder_empty_dialog_donot_allow));
                CommonUtil.getInstance().showDialog(mContext, title, msg, bt1, bt2, new OnOKDialogCallBack() {
                    @Override
                    public void handleOKButton1() {
                        showAddDialog();
                    }
                });
                break;
            case R.id.iv_m064_back:
                mCallBack.showChildFrgScreen(TAG, M051MoreFrg.TAG);
            default:
                break;
        }
    }

    private void showAddDialog() {
        mDialog = new M065MoreReminderAddDialog(mContext, false);
        mDialog.setOnCallBack(this);
        mDialog.show();
    }


    @Override
    public void showAlert(String sms) {
        getStorage().setM065ReminderChanged(true);
        mCallBack.showChildFrgScreen(TAG, M065MoreReminderNotEmptyFrg.TAG);
        ProgressLoading.dismiss();
        mDialog.dismiss();
    }

    @Override
    public void closeDialog(String key) {
        //This feature isn't require here
    }

    @Override
    public void toM051More() {
        //This feature isn't require here
    }
}
