package com.novo.app.view.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.ble.BLEInfoEntity;
import com.novo.app.ble.DeviceScanUtil;
import com.novo.app.ble.DoseItem;
import com.novo.app.ble.OnBLECallBack;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.presenter.M025UpdatingPresenter;
import com.novo.app.presenter.M032PairPushReleasePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseBLEFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM025UpdatingCallBack;
import com.novo.app.view.event.OnM032PairPushReleaseCallBack;
import com.novo.app.view.widget.NVDateUtils;
import com.novo.app.view.widget.OnOKDialogAdapter;

import static com.novo.app.model.entities.DeviceEntity.SYS_EOL_WARNING;

public class M032PairPushReleaseFrg extends BaseBLEFragment<M032PairPushReleasePresenter, OnHomeBackToView>
        implements OnM032PairPushReleaseCallBack, OnBLECallBack, OnM025UpdatingCallBack {

    public static final String TAG = M032PairPushReleaseFrg.class.getName();
    private DeviceEntity mItem;
    private BLEInfoEntity mData;

    @Override
    protected void initViews() {
        super.initViews();
        TextView mTvTitle = findViewById(R.id.tv_m032_title, this, CAApplication.getInstance().getBoldFont());
        String highLightTitle;
        if (!getStorage().getM025ListDevices().isEmpty()) {
            mTvTitle.setText(LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m032_title)));
            highLightTitle = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m032_push));
        } else {
            mTvTitle.setText(LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m032_title_finally)));
            highLightTitle = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m032_push_nocap));
        }
        highLightText(mTvTitle, highLightTitle, R.color.colorM001Cyan, true);

        findViewById(R.id.tv_m032_nothing, this, CAApplication.getInstance().getMediumFont());

        ImageView ivBg = findViewById(R.id.iv_m032_bg);
//        Glide.with(mContext).load(R.drawable.bg_m032_push).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(ivBg);

        mHandler = new Handler(msg -> {
            doMessage(msg);
            return false;
        });

        mScanUtil = new DeviceScanUtil();
        mScanUtil.setCallBack(this);
        mCallBack.stopScan();
    }

    @Override
    protected void doMessage(Message msg) {
        if (msg == null) {
            mScanUtil.startScan();
            return;
        }

        mItem = isExistDevicePaired();
        if (mItem != null) {
            CommonUtil.getInstance().showDialog(mContext,
                    LangMgr.getInstance().getLangList().get(getString(R.string.lang_m042_more_devices_dialog_title)),
                    LangMgr.getInstance().getLangList().get(getString(R.string.lang_m032_more_devices_exist_dialog_message)),
                    LangMgr.getInstance().getLangList().get(getString(R.string.lang_m032_more_devices_exist_dialog_close)),
                    null,
                    null);

            startAgain();
            return;
        }
        BLEInfoEntity bleInfoEntity = (BLEInfoEntity) msg.obj;
        bleInfoEntity.setDeviceName(mDeviceName);

        mData = bleInfoEntity;
        showMatchingDialog(bleInfoEntity.getSystemId());
    }

    private void showMatchingDialog(String systemId) {
        LinearLayout customTitle = (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.view_m032_matching, null);
        TextView idNumber = customTitle.findViewById(R.id.tv_m032_id_number);
        idNumber.setText(systemId);

        String msg = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m032_message_matching));
        String yes = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m032_message_matching_yes));
        String no = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m032_message_matching_no));

        AlertDialog mAlert = new AlertDialog.Builder(mContext).create();
        mAlert.setCustomTitle(customTitle);
        mAlert.setMessage(msg);

        mAlert.setButton(AlertDialog.BUTTON_POSITIVE, yes, (dialog, which) -> {
            pairContinue();
        });

        mAlert.setButton(AlertDialog.BUTTON_NEGATIVE, no, (dialog, which) -> {
            startAgain();
        });

        mAlert.setOnDismissListener(dialog -> startAgain());
        mAlert.show();

        TextView mTvMessage = mAlert.findViewById(android.R.id.message);
        TextView mTvTitle = mAlert.findViewById(mContext.getResources().getIdentifier("alertTitle", "id", "android"));
        Button bt1 = mAlert.getButton(DialogInterface.BUTTON_POSITIVE);
        Button bt2 = mAlert.getButton(DialogInterface.BUTTON_NEGATIVE);

        mTvTitle.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        mTvMessage.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        bt1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        bt2.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
    }

    private void pairContinue() {
        if (mData.getDoseItems() == null || mData.getDoseItems().isEmpty()) {
            DeviceEntity entity = new DeviceEntity();
            entity.setDrugType(DeviceEntity.CODE_NO_DOSE);
            entity.setDrugCode(DeviceEntity.CODE_NO_DOSE);
            addDevice32(entity);
        } else {
            initDrugListData(mData);
        }

        DoseItem lastDose;
        if (mData.getDoseItems() == null || mData.getDoseItems().isEmpty()) {
            lastDose = new DoseItem(0, 0, System.currentTimeMillis(), -1, 1);

        } else {
            lastDose = mData.getDoseItems().get(mData.getDoseItems().size() - 1);
        }

        int drugCode = lastDose.getDrugType();
        String flagCode = (mData.getSystemFlag() != null && mData.getSystemFlag().equals(SYS_EOL_WARNING + "")) ? "1" : "0";

        String lastPair = NVDateUtils.getGroupItemDateTitle(NVDateUtils.TODAY_AT, NVDateUtils.YESTERDAY_AT, CommonUtil.getDateNow(CommonUtil.DATE_STYLE));

        DeviceEntity deviceEntity = new DeviceEntity(mData.getSystemId(),
                flagCode, mData.getDeviceName(),
                drugCode, "",
                lastPair, CommonUtil.getDateNow(CommonUtil.DATE_STYLE));

        deviceEntity.setDrugCode(drugCode);
        mPresenter.callNNDMSystemIdForDecryption(deviceEntity);
    }

    private void addDevice32(DeviceEntity entity) {
        boolean isExist = false;

        for (int j = 0; j < getStorage().getM032PairedDevice().size(); j++) {
            if (getStorage().getM032PairedDevice().get(j).getDrugCode() == entity.getDrugCode()) {
                isExist = true;
                break;
            }
        }

        if (!isExist) {
            getStorage().getM032PairedDevice().add(entity);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m032_pair_push_release;
    }

    @Override
    protected M032PairPushReleasePresenter getPresenter() {
        return new M032PairPushReleasePresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }


    @Override
    protected void onClickView(int idView) {
        if (idView == R.id.tv_m032_nothing) {
            mCallBack.showFrgScreen(TAG, M070PairTroubleFrg.TAG);
        }
    }

    @Override
    public void showM001LandingScreen(String sms) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, sms);
    }

    @Override
    public void pairCompleted() {

        if (mData.getSystemFlag() == null
                || mData.getSystemFlag().equals(DeviceEntity.SYS_EOL + "")
                || mData.getSystemFlag().equals(DeviceEntity.SYS_ERR + "")) {
            CommonUtil.getInstance().showDialog(mContext, "EOL", "EOL", "ok", "Cancel", null);
            syncSuccess();
            return;
        }
        if (mData.getDoseItems() == null || mData.getDoseItems().isEmpty()) {
            syncSuccess();
            return;
        }
        try {
            mItem = isExistDevicePaired();
            if (mItem == null) {
                syncSuccess();
                return;
            }

            int mLastNumber = getLastNumber(mItem.getDeviceID());
            CommonUtil.wtfi(TAG, "lastNumber:..." + mLastNumber);
            if (mLastNumber < mData.getDoseItems().size()) {
                M025UpdatingPresenter presenter = new M025UpdatingPresenter(this);
                presenter.syncDataOffline(mItem, mLastNumber, mData);
            } else {
                syncSuccess();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getLastNumber(String deviceID) {
        if (getStorage().getM025ListResult() == null || getStorage().getM025ListResult().isEmpty())
            return 0;
        int max = -1;
        for (int i = 0; i < getStorage().getM025ListResult().size(); i++) {
            DataInfo item = getStorage().getM025ListResult().get(i);
            int number;
            try {
                number = Integer.parseInt(item.getResult().getNotes());
            } catch (Exception e) {
                number = -1;
            }
            if (item.getResult().getCustom1() != null && item.getResult().getCustom1().equals(deviceID) && number > max) {
                max = number;
            }
        }
        return max;
    }

    @Override
    public void resetStateBLE() {
        startAgain();
    }

    @Override
    public void syncSuccess() {
        CommonUtil.wtfi(TAG, "syncSuccess...");
        mPresenter.hideLockDialog();
        mCallBack.showFrgScreen(TAG, M033PairSuccessFrg.TAG);
    }

    @Override
    public void syncFailed() {
        CommonUtil.wtfe(TAG, "syncFailed...");
        mPresenter.hideLockDialog();
        mCallBack.showFrgScreen(TAG, M033PairSuccessFrg.TAG);
    }

    private void initDrugListData(BLEInfoEntity data) {
        if (data != null && data.getDoseItems() != null && !data.getDoseItems().isEmpty()) {
            int type = data.getDoseItems().get(data.getDoseItems().size() - 1).getDrugType();
            DeviceEntity entity = new DeviceEntity();
            entity.setDrugType(type);
            entity.setDrugCode(type);

            addDevice32(entity);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mCallBack.startScan();
        CommonUtil.wtfe(TAG, "onDestroyView()....");
    }
}
