package com.novo.app.view.event;

public interface OnLanguageCallBack extends OnCallBackToView {

    default void filterLanguage() {
    }

    void configLanguageDone();

    default void updateBottomBar() {
    }

    void filterLanguageNoConnection();
}
