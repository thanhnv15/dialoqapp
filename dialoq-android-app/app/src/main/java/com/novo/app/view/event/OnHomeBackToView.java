/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.view.event;

import com.novo.app.model.entities.ConfigEntity;

/**
 * Created by ThanhNV42 on 6/9/2017.
 * Prototype callbacks use by {@link com.novo.app.view.activity.HomeActivity}
 * when there are common events, typically triggered to initial preparing data,
 * view's presentation and/or android system events
 */
public interface OnHomeBackToView extends OnM025UpdatingCallBack {

    void showFrgScreen(String tagSource, String tagChild);

    void closeApp();

    void hideBottomBar();

    void loadConfigFail();

    void loadConfigSuccess(ConfigEntity configEntity);

    void dataLanguageReady(String key);

    void showWaringDialog(String txt, boolean isActive);

    void checkBlockApp(int isActive);

    void doTimeZoneChange();

    void unregisterTimeZoneChange();

    void pauseReminder();

    void setupReminder();

    void stopScan();

    void startScan();

    void preLoginTimeZoneChange();

    void cancelReminder();

    void registrationCancel(String tag);

    void showLandingFrgScreen();

    void loginSuccess();

    void showNetworkAlert();

    void reloadConfigSet();

    default void detectAdultUser() {
    }

    void getLanguageError();

    void downloadIntroVideo(String url);
}
