/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.utils;

import android.widget.Toast;

import com.novo.app.CAApplication;
import com.novo.app.manager.entities.RDecryptionKeyEntity;
import com.novo.app.manager.entities.RReminderEntity;
import com.novo.app.model.entities.CarePlanTemplateEntity;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.ConfigEntity;
import com.novo.app.model.entities.CountryEntity;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.DateEntity;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.PatientEntity;
import com.novo.app.model.entities.ProfileEntity;
import com.novo.app.model.entities.ResultInfo;
import com.novo.app.model.entities.ShareDataEntity;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.model.entities.VersionEntity;
import com.novo.app.service.CARequest;
import com.novo.app.service.WebServiceAdapter;
import com.novo.app.view.event.OnActionCallBack;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.event.OnM002SignUpChooseLanguageCallBack;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StorageCommon implements Serializable {
    private transient WeakReference<CAApplication> reference;
    private ConfigEntity m001ConfigSet;
    private ProfileEntity m001ProfileEntity = new ProfileEntity();
    private PatientEntity m014PatientEntity;
    private CarePlanTemplateEntity m001CarePlanTemplateEntity;
    private boolean m014VerifyFail;
    private boolean m077VerifyFail;
    private String[] m025VideoData;
    private UserEntity mUserEntity;
    private CarePlantUserEntity m001CarePlanUserEntity;
    private List<DataInfo> m025ListResult;
    private DataInfo m025ResultDataItem;
    private ResultInfo m062ResultInfoItem;
    private String[] m028VideoData;
    private List<DateEntity> list7DayData;
    private List<DateEntity> list14DayData;
    private List<DateEntity> list28DayData;
    private String m038UserName;
    private String m039Verify;
    private boolean m039VerifyFail;
    private boolean m029FirstPair;
    private boolean isM065ReminderChanged;
    private boolean isAppActive;

    private String mFlag;
    private boolean m001FirstCheckBox;
    private int m047ItemPosition;
    private String m048VideoPath;
    private String preTAG;
    private ArrayList<DeviceEntity> m032PairedDevice;
    private boolean isM042AddNewDevice;
    private DeviceEntity m042CurrentItem;
    private String m025NoteResultId;
    private boolean isFirstStart = true;
    private ArrayList<DeviceEntity> m025ListDevices;
    private boolean m042IsCheckIn;
    private boolean isMature;
    private String linkTag;
    private CountryEntity userCountryEntity;
    private String mEOLSystemIdWarning;
    private String mEOLSystemId;
    private String m042ResultId;
    private boolean isTresibaExist;
    private boolean isUpdateM025;
    private boolean isM33ContinuePairing;
    private VersionEntity versionEntity;
    private transient OnActionCallBack mLoginSessionExpire;
    private transient OnActionCallBack mCredentialLost;
    private transient HashMap<String, OnActionCallBack> m0DoseCallBack = new HashMap<>();
    private transient OnM002SignUpChooseLanguageCallBack m002CallBack;
    private ShareDataEntity m068TresibaShareData;
    private int m068TypeDate;
    private transient List<RDecryptionKeyEntity> mNNMDDecryptionKey;
    private String lastTresibaDose;
    private ShareDataEntity m068FiaspShareData;
    private String mKeyTagNotify;
    private String mConfigSetData;
    private transient HashMap<String, OnActionCallBack> mHomeActionCallBack = new HashMap<>();
    private boolean mStateTokenExpired;
    private transient OnActionCallBack mCallBackToSync;
    private boolean mCallLoggedOut;
    private String languageKeyToGet;
    private TextEntity m071PDFPath;
    private TextEntity m075UserLoginInfo;
    private String m077VerifyCode;
    private transient RReminderEntity mM065ScheduleEntity;
    private DataInfo m063Note;
    private transient Map<String, Object[]> mQueueRequest = Collections.synchronizedMap(new HashMap<>());
    private boolean m023FlagUsedTouchID;
    private boolean hasBeenForcedLogOut;
    private String m078ErrorMessage;
    private transient List<OnActionCallBack> mDialogCallBack = new ArrayList<>();


    public StorageCommon(WeakReference<CAApplication> reference) {
        this.reference = reference;
    }

    public WeakReference<CAApplication> getReference() {
        return reference;
    }

    public void setReference(WeakReference<CAApplication> reference) {
        this.reference = reference;
    }

    public ConfigEntity getM001ConfigSet() {
        return m001ConfigSet;
    }

    public void setM001ConfigSet(ConfigEntity configEntity) {
        m001ConfigSet = configEntity;
    }

    public ProfileEntity getM001ProfileEntity() {
        return m001ProfileEntity;
    }

    public void setM001ProfileEntity(ProfileEntity m001ProfileEntity) {
        this.m001ProfileEntity = m001ProfileEntity;
    }

    public PatientEntity getM014PatientEntity() {
        return m014PatientEntity;
    }

    public void setM014PatientEntity(PatientEntity entity) {
        m014PatientEntity = entity;
    }

    public CarePlanTemplateEntity getM001CarePlanTemplateEntity() {
        return m001CarePlanTemplateEntity;
    }

    public void setM001CarePlanTemplateEntity(CarePlanTemplateEntity entity) {
        m001CarePlanTemplateEntity = entity;
    }

    public boolean getM077VerifyFail() {
        return m077VerifyFail;
    }

    public void setM077VerifyFail(boolean m077VerifyFail) {
        this.m077VerifyFail = m077VerifyFail;
    }

    public void clearAllDataLogin() {
        m001ProfileEntity = new ProfileEntity();
        m014PatientEntity = null;
        m014VerifyFail = false;
        m077VerifyFail = false;
        mStateTokenExpired = false;
        m025ListResult = null;
        m025VideoData = null;
        m062ResultInfoItem = null;
        mUserEntity = null;
        m025ResultDataItem = null;
        m028VideoData = null;
        list7DayData = null;
        list14DayData = null;
        list28DayData = null;
        m038UserName = null;
        m039Verify = null;
        m063Note = null;
        m029FirstPair = false;
        isM065ReminderChanged = false;
        m001FirstCheckBox = false;
        isM042AddNewDevice = false;
        m047ItemPosition = 0;
        m001CarePlanTemplateEntity = null;
        m042CurrentItem = null;
        m025ListDevices = null;
        m032PairedDevice = null;
        mM065ScheduleEntity = null;
        preTAG = null;
        m048VideoPath = null;
        mEOLSystemIdWarning = null;
    }

    public void clearAllData() {
        clearAllDataLogin();
        isFirstStart = true;
    }

    public boolean getM014VerifyFail() {
        return m014VerifyFail;
    }

    public void setM014VerifyFail(boolean m014VerifyFail) {
        this.m014VerifyFail = m014VerifyFail;
    }

    public String[] getM025VideoData() {
        return m025VideoData;
    }

    public void setM025VideoData(String[] m025VideoData) {
        this.m025VideoData = m025VideoData;
    }

    public String[] getM028VideoData() {
        return m028VideoData;
    }

    public void setM028VideoData(String[] m028VideoData) {
        this.m028VideoData = m028VideoData;
    }

    public UserEntity getM007UserEntity() {
        return mUserEntity;
    }

    public void setM007UserEntity(UserEntity entity) {
        if (mUserEntity == null) {
            mUserEntity = entity;
        } else {
            mUserEntity.copyObj(entity);
        }
    }

    public CarePlantUserEntity getM001CarePlanUserEntity() {
        return m001CarePlanUserEntity;
    }

    public void setM001CarePlanUserEntity(CarePlantUserEntity entity) {
        m001CarePlanUserEntity = entity;
    }


    public DataInfo getM025ResultDataItem() {
        return m025ResultDataItem;
    }

    public void setM025ResultDataItem(DataInfo tag) {
        m025ResultDataItem = tag;
    }

    public ResultInfo getM062ResultInfoItem() {
        return m062ResultInfoItem;
    }

    public void setM062ResultInfoItem(ResultInfo resultInfo) {
        m062ResultInfoItem = resultInfo;
    }

    public List<DataInfo> getM025ListResult() {
        return m025ListResult;
    }

    public void setM025ListResult(List<DataInfo> m025ListResult) {
        this.m025ListResult = m025ListResult;
    }

    public void set7DayData(List<DateEntity> entities) {
        this.list7DayData = entities;
    }

    public List<DateEntity> getList7DayData() {
        return list7DayData;
    }

    public void set14DayData(List<DateEntity> entities) {
        this.list14DayData = entities;
    }

    public List<DateEntity> getList14DayData() {
        return list14DayData;
    }

    public void set28DayData(List<DateEntity> entities) {
        this.list28DayData = entities;
    }

    public List<DateEntity> getList28DayData() {
        return list28DayData;
    }

    public boolean isM039VerifyFail() {
        return m039VerifyFail;
    }

    public void setM039VerifyFail(boolean m039VerifyFail) {
        this.m039VerifyFail = m039VerifyFail;
    }

    public String getM038UserName() {
        return m038UserName;
    }

    public void setM038UserName(String userName) {
        this.m038UserName = userName;
    }

    public String getM039VerifyCode() {
        return m039Verify;
    }

    public void setM039Verify(String m039Verify) {
        this.m039Verify = m039Verify;
    }

    public boolean isAppActive() {
        return isAppActive;
    }

    public void setAppActive(boolean isActive) {
        isAppActive = isActive;
    }

    public String getFlag() {
        if (mFlag == null) mFlag = "";
        return mFlag;
    }

    public void setFlag(String mFlag) {
        this.mFlag = mFlag;
    }

    public boolean isM001FirstCheckBox() {
        return m001FirstCheckBox;
    }

    public void setM001FirstCheckBox(boolean m001FirstCheckBox) {
        this.m001FirstCheckBox = m001FirstCheckBox;
    }

    public int getM047ItemPosition() {
        return m047ItemPosition;
    }

    public void setM047ItemPosition(int adapterPosition) {
        this.m047ItemPosition = adapterPosition;
    }

    public String getM048VideoPath() {
        return m048VideoPath;
    }

    public void setM048VideoPath(String url) {
        this.m048VideoPath = url;
    }

    public boolean isM029FirstPair() {
        return m029FirstPair;
    }

    public void setM029FirstPair(boolean m029FirstPair) {
        this.m029FirstPair = m029FirstPair;
    }

    public String getPreTAG() {
        return preTAG;
    }

    public void setPreTAG(String preTAG) {
        this.preTAG = preTAG;
    }

    public boolean isM042AddNewDevice() {
        return isM042AddNewDevice;
    }

    public void setM042AddNewDevice(boolean m042AddNewDevice) {
        isM042AddNewDevice = m042AddNewDevice;
    }

    public DeviceEntity getM042CurrentItem() {
        return m042CurrentItem;
    }

    public void setM042CurrentItem(DeviceEntity entity) {
        this.m042CurrentItem = entity;
    }

    public List<DeviceEntity> getM032PairedDevice() {
        if (m032PairedDevice == null) m032PairedDevice = new ArrayList<>();
        return m032PairedDevice;
    }

    public void setM032PairedDevice(List<DeviceEntity> deviceEntityList) {
        this.m032PairedDevice = (ArrayList<DeviceEntity>) deviceEntityList;
    }

    public boolean isM065ReminderChanged() {
        return isM065ReminderChanged;
    }

    public void setM065ReminderChanged(boolean m065ReminderChanged) {
        isM065ReminderChanged = m065ReminderChanged;
    }

    public String getM025NoteResultId() {
        return m025NoteResultId;
    }

    public void setM025NoteResultId(String resultId) {
        this.m025NoteResultId = resultId;
    }

    public boolean isFirstStart() {
        return isFirstStart;
    }

    public void setFirstStart(boolean firstStart) {
        this.isFirstStart = firstStart;
    }

    /**
     * This method will return the list of paired device
     *
     * @return List<DeviceEntity>
     */
    public List<DeviceEntity> getM025ListDevices() {
        if (m025ListDevices == null) m025ListDevices = new ArrayList<>();
        return m025ListDevices;
    }

    public void setM025ListDevices(List<DeviceEntity> entityList) {
        this.m025ListDevices = (ArrayList<DeviceEntity>) entityList;
    }

    public void setM042CheckIn(boolean checkedIn) {
        m042IsCheckIn = checkedIn;
    }

    public boolean isM042IsCheckIn() {
        return m042IsCheckIn;
    }

    public boolean isMature() {
        return isMature;
    }

    public void setMature(boolean mature) {
        isMature = mature;
    }

    public String getLinkTag() {
        return linkTag;
    }

    public void setLinkTag(String tag) {
        this.linkTag = tag;
    }

    public CountryEntity getUserCountryEntity() {
        return userCountryEntity;
    }

    public void setUserCountryEntity(CountryEntity countryEntity) {
        this.userCountryEntity = countryEntity;
    }

    public VersionEntity getVersionEntity() {
        return versionEntity;
    }

    public void setVersionEntity(VersionEntity versionEntity) {
        this.versionEntity = versionEntity;
    }

    /**
     * This method will get flag of SystemIdEOLWarning from Pen/Board
     * @return mEOLSystemIdWarning String
     */
    public String getSystemIdEOLWarning() {
        return mEOLSystemIdWarning;
    }

    /**
     * This method will set a value to the mEOLSystemIdWarning field
     * @param  vl String
     */
    public void setSystemIdEOLWarning(String vl) {
        mEOLSystemIdWarning = vl;
    }

    public void setResultId(String resultId) {
        this.m042ResultId = resultId;
    }

    public String getM042ResultId() {
        return m042ResultId;
    }

    public RReminderEntity getM065ScheduleEntity() {
        return mM065ScheduleEntity;
    }

    public void setM065ScheduleEntity(RReminderEntity entity) {
        mM065ScheduleEntity = entity;
    }

    public boolean isTresibaExist() {
        return isTresibaExist;
    }

    public void setTresibaExist(boolean vl) {
        isTresibaExist = vl;
    }

    public boolean isUpdateM025() {
        if (!isUpdateM025) return false;
        isUpdateM025 = false;
        return true;
    }

    public void setUpdateM025(boolean updateM025) {
        isUpdateM025 = updateM025;
    }

    public boolean isM33ContinuePairing() {
        return isM33ContinuePairing;
    }

    public void setM33ContinuePairing(boolean m33ContinuePairing) {
        isM33ContinuePairing = m33ContinuePairing;
    }

    public void callBackDoseCallBack(Object data) {
        if (m0DoseCallBack == null) {

            //for testing
            try {
                Toast.makeText(CAApplication.getInstance(), "Could not call back to view for UI updating", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }

        for (HashMap.Entry<String, OnActionCallBack> entry : m0DoseCallBack.entrySet()) {
            Toast.makeText(CAApplication.getInstance(), "UI updating screen: " + entry.getKey(), Toast.LENGTH_SHORT).show();
            entry.getValue().onCallBack(data);
        }
    }

    public void setM0DoseCallBack(String key, OnActionCallBack callBack) {
        if (m0DoseCallBack == null) {
            m0DoseCallBack = new HashMap<>();
        }
        if (m0DoseCallBack.get(key) != null) {
            m0DoseCallBack.remove(key);
        }

        m0DoseCallBack.put(key, callBack);
    }

    public ShareDataEntity getM068TresibaShareData() {
        return m068TresibaShareData;
    }

    public void setM068TresibaShareData(ShareDataEntity m068TresibaShareData) {
        this.m068TresibaShareData = m068TresibaShareData;
    }

    public ShareDataEntity getM068FiaspShareData() {
        return m068FiaspShareData;
    }

    public void setM068FiaspShareData(ShareDataEntity m068FiaspShareData) {
        this.m068FiaspShareData = m068FiaspShareData;
    }

    public int getM068TypeDate() {
        return m068TypeDate;
    }

    public void setM068TypeDate(int key) {
        this.m068TypeDate = key;
    }

    /**
     * This method will return the list of RDecryptionKeyEntity
     *
     * @return List<RDecryptionKeyEntity>
     */
    public List<RDecryptionKeyEntity> getNNMDDecryptionKey() {
        if (mNNMDDecryptionKey == null) {
            mNNMDDecryptionKey = new ArrayList<>();
        }
        return mNNMDDecryptionKey;
    }

    public String getLastTresibaDose() {
        return lastTresibaDose;
    }

    public void setLastTresibaDose(String lastTresibaDose) {
        this.lastTresibaDose = lastTresibaDose;
    }

    public String getKeyTagNotify() {
        return mKeyTagNotify;
    }

    public void setKeyTagNotify(String keyTag) {
        mKeyTagNotify = keyTag;
    }

    public String getConfigSetData() {
        return mConfigSetData;
    }

    public void setConfigSetData(String data) {
        mConfigSetData = data;
    }

    public void clearHomeCallBack() {
        if (mHomeActionCallBack == null) return;
        mHomeActionCallBack.clear();
    }

    public void addHomeCallBack(String key, OnActionCallBack onActionCallBack) {
        if (mHomeActionCallBack == null) {
            mHomeActionCallBack = new HashMap<>();
        }

        if (onActionCallBack == null) {
            mHomeActionCallBack.clear();
            return;
        }
        if (!mHomeActionCallBack.containsKey(key)) {
            mHomeActionCallBack.put(key, onActionCallBack);
        }
    }

    /**
     * <b>getHomeActionCallBack</b><br/>
     * This method will return the list of ActionCallBack
     *
     * @return HashMap<String, OnActionCallBack>
     */
    public HashMap<String, OnActionCallBack> getHomeActionCallBack() {
        return mHomeActionCallBack;
    }

    public void saveStateTokenExpired(boolean isVL) {
        mStateTokenExpired = isVL;
    }

    public boolean isStateTokenExpired() {
        return mStateTokenExpired;
    }

    public OnActionCallBack getCallBackToSync() {
        return mCallBackToSync;
    }

    public void setCallBackToSync(OnActionCallBack mCallBackToSync) {
        this.mCallBackToSync = mCallBackToSync;
    }

    public String getSystemIdEOL() {
        return mEOLSystemId;
    }

    public void setSystemIdEOL(String value) {
        this.mEOLSystemId = value;
    }

    public boolean isCallLoggedOut() {
        return mCallLoggedOut;
    }

    public void setCallLoggedOut(boolean vl) {
        mCallLoggedOut = vl;
    }

    public String getLanguageKeyToGet() {
        return languageKeyToGet;
    }

    public void setLanguageKeyToGet(String languageKeyToGet) {
        this.languageKeyToGet = languageKeyToGet;
    }

    public OnM002SignUpChooseLanguageCallBack getM002CallBack() {
        return m002CallBack;
    }

    public void setM002CallBack(OnM002SignUpChooseLanguageCallBack m002CallBack) {
        this.m002CallBack = m002CallBack;
    }

    public TextEntity getM071PDFPath() {
        return m071PDFPath;
    }

    public void setM071PDFPath(TextEntity m071PDFPath) {
        this.m071PDFPath = m071PDFPath;
    }

    public TextEntity getM075UserLoginInfo() {
        return m075UserLoginInfo;
    }

    public void setM075UserLoginInfo(TextEntity loginInfo) {
        m075UserLoginInfo = loginInfo;
    }

    public String getM077VerifyCode() {
        return m077VerifyCode;
    }

    public void setM077VerifyCode(String m075VerifyCode) {
        this.m077VerifyCode = m075VerifyCode;
    }

    public void callBackDoseToCloseRefresh() {
        if (m0DoseCallBack == null) {
            return;
        }
        for (HashMap.Entry<String, OnActionCallBack> entry : m0DoseCallBack.entrySet()) {
            entry.getValue().closeRefresh();
        }
    }

    public void removeM0DoseCallBack(String tag) {
        if (m0DoseCallBack == null) {
            return;
        }
        m0DoseCallBack.remove(tag);
    }

    public DataInfo getM063Note() {
        return m063Note;
    }

    public void setM063Note(DataInfo data) {
        m063Note = data;
    }

    public synchronized Object[] getQueueRequest(String requestId) {
        return mQueueRequest.get(requestId);
    }

    public synchronized void removeQueueRequest(String requestId) {
        mQueueRequest.remove(requestId);
    }

    /**
     * <b>addQueueRequest</b><br/>
     * <br/>This method will add a request to the Queue for re-newing later when the token is expired
     *
     * @param event   WebServiceAdapter.OnRequestCallBackListener
     * @param request CARequest
     */
    public synchronized void addQueueRequest(WebServiceAdapter.OnRequestCallBackListener event, CARequest request) {
        this.mQueueRequest.put(request.getRequestID(), new Object[]{request, event});
    }

    public synchronized Map<String, Object[]> getQueueRequest() {
        return mQueueRequest;
    }

    public OnActionCallBack getLoginSessionExpire() {
        return mLoginSessionExpire;
    }

    public void setLoginSessionExpire(OnActionCallBack mLoginSessionExpire) {
        this.mLoginSessionExpire = mLoginSessionExpire;
    }

    public boolean getFlagUsedTouchID() {
        return m023FlagUsedTouchID;
    }

    public void setFlagUsedTouchID(boolean flagUsedTouchID) {
        m023FlagUsedTouchID = flagUsedTouchID;
    }

    public OnActionCallBack getCredentialLost() {
        return mCredentialLost;
    }

    public void setCredentialLost(OnActionCallBack mCredentialLost) {
        this.mCredentialLost = mCredentialLost;
    }

    public boolean hasBeenForcedLogOut() {
        return hasBeenForcedLogOut;
    }

    public void setHasBeenForcedLogOut(boolean hasBeenForcedLogOut) {
        this.hasBeenForcedLogOut = hasBeenForcedLogOut;
    }

    public <H extends OnCallBackToView> String getM078ErrorMessage() {
        return m078ErrorMessage;
    }

    public void setM078ErrorMessage(String message) {
        this.m078ErrorMessage = message;
    }

    public void removeDialogCallBack(OnActionCallBack callBack) {
        mDialogCallBack.remove(callBack);
    }

    public void addDialogCallBack(OnActionCallBack callBack) {
        if (!mDialogCallBack.contains(callBack)) {
            mDialogCallBack.add(callBack);
        }
    }

    public void notifyDialogCallBack() {
        for (int i = 0; i < mDialogCallBack.size(); i++) {
            mDialogCallBack.get(i).onCallBack(null);
        }
    }
}