package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM058MoreCommunicationCallBack;

import static com.novo.app.utils.CommonUtil.P_API_KEY;

public class M058MoreCommunicationPresenter extends BasePresenter<OnM058MoreCommunicationCallBack> {
    private static final String KEY_API_UN04_UPDATE_PATIENT = "KEY_API_UN04_UPDATE_PATIENT";
    private static final String URL_API_UN04_UPDATE_PATIENT = "user-service/api/v1/users/patients/%s?";
    private static final String BODY_UN04 = "{\"firstName\":%s,\"lastName\":%s,\"email\":%s,\"gender\": %s,\"dateOfBirth\":%s,\"mobilePhoneNumber\": \"84978678205\",\"country\":%s,\"medicationHistory\":%s,\"familyHistory\":%s,\"personalHistory\":%s,\"allergies\":%s,\"race\":%s,\"streetAddress1\":%s,\"streetAddress2\":%s}";
    private static final String TAG = M058MoreCommunicationPresenter.class.getName();


    public M058MoreCommunicationPresenter(OnM058MoreCommunicationCallBack event) {
        super(event);
    }

    @Override
    protected void handleSuccess(String data, String tag) {
        CommonUtil.wtfi(TAG, "DATA: " + data);
        if(tag.equals(KEY_API_UN04_UPDATE_PATIENT)){
            prepareUN04(data);
        }
    }

    protected void prepareUN04(String data) {
        CommonUtil.wtfi(TAG, "prepareUN04..." + data);
        UserEntity entity = generateData(UserEntity.class, data);
        if (entity == null) {
            showNotify(LangMgr.getInstance().getLangList().get(ERR_500));
            return;
        }
        getStorage().getM007UserEntity().setStreetAddress2(entity.getStreetAddress2());
        getStorage().getM007UserEntity().setRace(entity.getRace());
        mListener.updateUserChoice();
        mListener.showDialog();
    }

    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tagRequest);

        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        String sms = LangMgr.getInstance().getLangList().get(ERR_500);
        if (responseEntity != null) {
            if(tag.equals(KEY_API_UN04_UPDATE_PATIENT)){
                showNotify(sms);
            }
        } else if (CommonUtil.getInstance().isConnectToNetwork()) {
            showNotify(LangMgr.getInstance().getLangList().get(ERR_NO_CONNECTION));
        }
        mListener.stateReverse();
    }

    public void callIUN04UpdatePatient() {
        CommonUtil.wtfi(TAG, "callIUN04UpdatePatient");

        CARequest request = new CARequest(TAG, CARequest.METHOD_PUT);
        request.addTAG(KEY_API_UN04_UPDATE_PATIENT);

        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, "token: " + token);
        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);

        UserEntity entity = getStorage().getM007UserEntity();
        request.addPathSegment(String.format(URL_API_UN04_UPDATE_PATIENT, entity.getUserId() + ""));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY_UN04, entity.getFirstName(), entity.getLastName(),
                entity.getEmail(), entity.getGender(), entity.getDateOfBirth(), entity.getCountry(),
                entity.getMedicationHistory(), entity.getFamilyHistory(), entity.getPersonalHistory(),
                entity.getAllergies(), entity.getRace(), entity.getStreetAddress1(), entity.getStreetAddress2()));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }
}
