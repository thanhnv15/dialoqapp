package com.novo.app.model.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class TopicEntity implements Serializable {
    private String topicKey;
    private List<TextEntity> listQnA;
    private boolean isManyQuestion;

    public TopicEntity(String topicKey, List<TextEntity> listQA) {
        this.topicKey = topicKey;
        this.listQnA = listQA;
    }

    public TopicEntity(String topicKey, List<TextEntity> listQnA, boolean isManyQuestion) {
        this.topicKey = topicKey;
        this.listQnA = listQnA;
        this.isManyQuestion = isManyQuestion;
    }

    public TopicEntity(String topicKey, Map<String, String> listValue) {
    }

    public String getTopicKey() {
        return topicKey;
    }

    public List<TextEntity> getListQnA() {
        return listQnA;
    }

    public boolean isManyQuestion() {
        return isManyQuestion;
    }

}
