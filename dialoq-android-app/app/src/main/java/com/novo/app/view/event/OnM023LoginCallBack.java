/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.view.event;

/**
 * Created by ThanhNV42 on 01/14/2019
 * Callbacks for updating view in user login session
 */
public interface OnM023LoginCallBack extends OnCallBackToView {
    void loginSuccess();

    /**
     * Called when finger print authentication finished
     */
    default void updateFingerPrintResult(){

    }

    /**
     * Called requesting view instance to show a dialog with some text
     * @param txt string id of message text which will be shown
     * @param TAG designated tag for the dialog
     */
    void showDialog(int txt, String TAG);

    /**
     * Called requesting view instance to show a dialog with some text
     * @param txt string which will be shown
     * @param TAG designated tag for the dialog
     */
    void showDialog(String txt, String TAG);

    /**
     * Show login result with invalid app's version
     *
     * @param recallCode state of installed app's version, possible states are:
     * @see com.novo.app.utils.CommonUtil#RECALL_VERSION,
     * @see com.novo.app.utils.CommonUtil#RECALL_COUNTRY, and
     * @see com.novo.app.utils.CommonUtil#RECALL_VERSION
     */
    default void showM001LandingFrg(int recallCode) {

    }

    /**
     * Request View instance to show wrong account login error
     */
    default void wrongAccount() {
    }
}
