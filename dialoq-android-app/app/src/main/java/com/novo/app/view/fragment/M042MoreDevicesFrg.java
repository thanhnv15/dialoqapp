package com.novo.app.view.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.ble.BLEInfoEntity;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.presenter.M042MorePresenter;
import com.novo.app.view.adapter.M042MoreDevicesAdapter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.dialog.M042MoreDeviceDialog;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM042MoreCallBack;

import java.util.ArrayList;
import java.util.List;

public class M042MoreDevicesFrg extends BaseFragment<M042MorePresenter, OnM024DoseLogCallBack> implements OnM042MoreCallBack {
    public static final String TAG = M042MoreDevicesFrg.class.getName();
    private View vAlert;
    private M042MoreDeviceDialog mDetailDialog;
    private RecyclerView rvDeviceList;
    private TextView mTvEmpty;
    private Button mBtAdd;
    private List<DeviceEntity> mListData = new ArrayList<>();

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m042_title, CAApplication.getInstance().getMediumFont());
        mBtAdd = findViewById(R.id.bt_m042_add, this, CAApplication.getInstance().getMediumFont());
        mBtAdd.setEnabled(false);
        findViewById(R.id.iv_m042_back, this);
        vAlert = findViewById(R.id.v_m042_alert, this);
        vAlert.setVisibility(View.GONE);
        if (getStorage().isM042AddNewDevice()) vAlert.setVisibility(View.VISIBLE);
        mTvEmpty = findViewById(R.id.tv_m042_empty, CAApplication.getInstance().getRegularFont());

        rvDeviceList = findViewById(R.id.rv_m042_device_list);
        rvDeviceList.setLayoutManager(new LinearLayoutManager(mContext));

        getStorage().getM032PairedDevice().clear();
        getStorage().setM042CheckIn(true);

        getStorage().setM0DoseCallBack(TAG, data -> {
            if (mListData == null || !(data instanceof BLEInfoEntity)) return;
            BLEInfoEntity entity = (BLEInfoEntity) data;
            for (int i = 0; i < mListData.size(); i++) {
                if (mListData.get(i).getDeviceID().equals(entity.getDeviceName())) {
                    mListData.get(i).setFlagCode(entity.getSystemFlag() != null
                            && entity.getSystemFlag().equals(DeviceEntity.SYS_EOL_WARNING + "") ? "1" : "0");
                    break;
                }
            }
            ((M042MoreDevicesAdapter) rvDeviceList.getAdapter()).setData(mListData);
        });

        updateList();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getStorage().setM042AddNewDevice(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getStorage().removeM0DoseCallBack(TAG);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m042_more_device;
    }

    @Override
    protected M042MorePresenter getPresenter() {
        return new M042MorePresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m042_back:
                mCallBack.showChildFrgScreen(TAG, M051MoreFrg.TAG);
                break;
            case R.id.v_m042_alert:
                vAlert.setVisibility(View.GONE);
                break;
            case R.id.bt_m042_add:
                mCallBack.showM029PairScreen();
                break;
            default:
                break;
        }
    }

    @Override
    public void showDeviceDetail() {
        if (mDetailDialog != null && mDetailDialog.isShowing()) return;
        mDetailDialog = new M042MoreDeviceDialog(mContext, false);
        mDetailDialog.setOnCallBack(this);
        mDetailDialog.show();
    }

    @Override
    public void showTroubleShooting() {
        mCallBack.showChildFrgScreen(TAG, M047SupportTroubleshootingFrg.TAG);
    }

    @Override
    public void updateList() {
        getStorage().setTresibaExist(false);
        vAlert.setVisibility(View.GONE);
        if (getStorage().isM042AddNewDevice()) vAlert.setVisibility(View.VISIBLE);

        mListData.clear();
        mListData.addAll(getStorage().getM025ListDevices());
        if (!mListData.isEmpty()) mTvEmpty.setVisibility(View.GONE);
        else mTvEmpty.setVisibility(View.VISIBLE);

        M042MoreDevicesAdapter adapter = new M042MoreDevicesAdapter(mContext, mListData, this);
        rvDeviceList.setAdapter(adapter);

        for (int i = 0; i < mListData.size(); i++) {
            if (mListData.get(i).getDrugType().equals(DeviceEntity.TRESIBA))
                getStorage().setTresibaExist(true);
        }

        if (mListData.size() < 3) {
            mBtAdd.setEnabled(true);
        } else {
            mBtAdd.setEnabled(false);
        }
    }

    @Override
    public void closeDeviceDetail() {
        //This feature isn't require here
    }

}
