package com.novo.app.view.widget.novocharts.providers;

import com.github.mikephil.charting.interfaces.dataprovider.BarDataProvider;
import com.novo.app.view.widget.novocharts.datasets.BarDetailsData;

public interface BarDetailsProvider extends BarDataProvider {
    BarDetailsData getBarDetailsData();
}
