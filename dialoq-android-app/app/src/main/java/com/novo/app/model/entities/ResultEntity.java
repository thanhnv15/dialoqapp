package com.novo.app.model.entities;

import com.google.gson.annotations.SerializedName;
import com.novo.app.model.BaseModel;

import java.io.Serializable;

public class ResultEntity extends BaseModel {
    @SerializedName("userId")
    private int userId;
    @SerializedName("assessmentId")
    private int assessmentId;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("type")
    private String type;
    @SerializedName("instructions")
    private String instructions;
    @SerializedName("unitOfMeasure")
    private String unitOfMeasure;
    @SerializedName("events")
    private EventInfo events;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getAssessmentId() {
        return assessmentId;
    }

    public void setAssessmentId(int assessmentId) {
        this.assessmentId = assessmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public EventInfo getEvents() {
        return events;
    }

    public void setEvents(EventInfo events) {
        this.events = events;
    }

    public class EventInfo implements Serializable {
        @SerializedName("totalCount")
        private String totalCount;
        @SerializedName("data")
        private DataInfo[] data;

        public String getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(String totalCount) {
            this.totalCount = totalCount;
        }

        public DataInfo[] getData() {
            if (data == null) {
                setData(new DataInfo[]{});
            }
            return data;
        }


        public void setData(DataInfo[] data) {
            this.data = data;
        }

    }
}
