package com.novo.app.view.adapter;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.utils.CommonUtil;


public abstract class BaseHolder extends RecyclerView.ViewHolder implements View.OnClickListener, Animation.AnimationListener {

    private static final CharSequence PLACE_HOLDER = "PLACEHOLDER";
    private static final String TAG = "HOLDER";
    private final View mRootView;
    protected boolean isAnimEnd = true;
    protected Animation mAnim;
    private int mId;

    public BaseHolder(View itemView) {
        super(itemView);
        mRootView = itemView;
        mAnim = AnimationUtils.loadAnimation(CAApplication.getInstance(), R.anim.alpha);
        mAnim.setAnimationListener(this);
        initView();
    }

    protected abstract void initView();

    @Override
    public final void onClick(View v) {
        if (!isAnimEnd) return;
        isAnimEnd = false;
        mId = v.getId();
        v.startAnimation(mAnim);
    }

    @Override
    public final void onAnimationStart(Animation animation) {

    }

    @Override
    public final void onAnimationEnd(Animation animation) {
        onClickView(mId);
        isAnimEnd = true;
    }

    protected void onClickView(int idView) {

    }

    @Override
    public final void onAnimationRepeat(Animation animation) {

    }

    protected final <G extends View> G findViewById(int id) {
        return findViewById(id, null, true, null);
    }

    protected final <G extends View> G findViewById(int id, Typeface typeFace) {
        return findViewById(id, null, true, typeFace);
    }

    protected final <G extends View> G findViewById(int id, View.OnClickListener event) {
        return findViewById(id, event, null);
    }

    protected final <G extends View> G findViewById(int id, View.OnClickListener event, Typeface typeFace) {
        return findViewById(id, event, true, typeFace);
    }

    protected final <G extends View> G findViewById(int id, View.OnClickListener event, boolean enable, Typeface typeFace) {
        G view = mRootView.findViewById(id);
        if (view == null) return null;

        view.setOnClickListener(event);
        view.setEnabled(enable);
        if (typeFace != null && view instanceof TextView) {
            ((TextView) view).setTypeface(typeFace);
        }
        if (view.getContentDescription() == null) {
            CommonUtil.wtfe(TAG, "No. ContentDescription");
        } else if (view instanceof TextView) {
            String key = view.getContentDescription().toString();
            if (LangMgr.getInstance().getCurrentLang() == null) {
                CommonUtil.wtfe(TAG, "Err: No current language");
            } else {
                if (key.contains(PLACE_HOLDER)) {
                    ((TextView) view).setHint(LangMgr.getInstance().getLangList().get(key));
                } else {
                    ((TextView) view).setText(LangMgr.getInstance().getLangList().get(key));
                }
            }
        }
        return view;
    }

}
