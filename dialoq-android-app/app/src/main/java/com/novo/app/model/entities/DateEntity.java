package com.novo.app.model.entities;

import java.io.Serializable;

public class DateEntity implements Serializable {
    private int date;
    private boolean isHeader;
    private boolean isNote;
    private boolean isUnknownDrug;
    private boolean isUnknownFiaspSize;
    private boolean isUnknownTresibaSize;
    private int color;
    private int position;
    private DailyDetailEntity dailyDetail;
    private String stringDate;

    public int getDate() {
        return date;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public boolean isNote() {
        return isNote;
    }

    public int getColor() {
        return color;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public void setNote(boolean note) {
        isNote = note;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public DailyDetailEntity getDailyDetail() {
        return dailyDetail;
    }

    public void setDailyDetail(DailyDetailEntity dailyDetail) {
        this.dailyDetail = dailyDetail;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getStringDate() {
        return stringDate;
    }

    public void setStringDate(String stringDate) {
        this.stringDate = stringDate;
    }

    public boolean isUnknownDrug() {
        return isUnknownDrug;
    }

    public void setUnknownDrug(boolean unknownDrug) {
        isUnknownDrug = unknownDrug;
    }

    public boolean isUnknownFiaspSize() {
        return isUnknownFiaspSize;
    }

    public void setUnknownFiaspSize(boolean unknownFiaspSize) {
        isUnknownFiaspSize = unknownFiaspSize;
    }

    public boolean isUnknownTresibaSize() {
        return isUnknownTresibaSize;
    }

    public void setUnknownTresibaSize(boolean unknownTresibaSize) {
        isUnknownTresibaSize = unknownTresibaSize;
    }
}
