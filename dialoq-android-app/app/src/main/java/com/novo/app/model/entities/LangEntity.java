package com.novo.app.model.entities;

import com.google.gson.annotations.SerializedName;
import com.novo.app.model.BaseModel;

import java.io.Serializable;

public class LangEntity extends BaseModel {
    @SerializedName("configSetId")
    private String configSetId;
    @SerializedName("configCode")
    private String configCode;
    @SerializedName("category")
    private String category;
    @SerializedName("version")
    private String version;
    @SerializedName("description")
    private String description;
    @SerializedName("isPublic")
    private boolean isPublic;
    @SerializedName("isLatest")
    private boolean isLatest;
    @SerializedName("createdOn")
    private String createdOn;
    @SerializedName("createdBy")
    private String createdBy;
    @SerializedName("data")
    private LangInfo[] data;

    public String getConfigSetId() {
        return configSetId;
    }

    public void setConfigSetId(String configSetId) {
        this.configSetId = configSetId;
    }

    public String getConfigCode() {
        return configCode;
    }

    public void setConfigCode(String configCode) {
        this.configCode = configCode;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public boolean isLatest() {
        return isLatest;
    }

    public void setLatest(boolean latest) {
        isLatest = latest;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LangInfo[] getData() {
        return data;
    }

    public void setData(LangInfo[] data) {
        this.data = data;
    }

    public class LangInfo implements Serializable {
        @SerializedName("key")
        private String key;
        @SerializedName("value")
        private String value;
        @SerializedName("description")
        private String description;
        @SerializedName("type")
        private String type;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
