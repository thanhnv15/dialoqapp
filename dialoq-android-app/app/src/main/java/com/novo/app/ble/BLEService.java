package com.novo.app.ble;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.novo.app.CAApplication;
import com.novo.app.manager.entities.RDecryptionKeyEntity;
import com.novo.app.model.entities.DecryptionKeyEntity;
import com.novo.app.utils.CommonUtil;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class BLEService extends Service {
    public static final String DEVICE_NAME_UUID = "00002A24-0000-1000-8000-00805F9B34FB";
    public static final String SERIAL_NUMBER_UUID = "00002A25-0000-1000-8000-00805F9B34FB";
    public static final String BLE_SOFTWARE_REVISION_UUID = "00002A28-0000-1000-8000-00805F9B34FB";
    public static final String HARDWARE_VERSION_UUID = "00002A27-0000-1000-8000-00805F9B34FB";
    public static final String FIRMWARE_VERSION_UUID = "00002A26-0000-1000-8000-00805F9B34FB";
    public static final String SYSTEM_ID_UUID = "00002A23-0000-1000-8000-00805F9B34FB";
    public static final String SYSTEM_FLAG = "0001000E-0000-1000-8000-00805F9B0000";

    public static final String LAST_SEQUENCE_NUMBER_CHAR_UUID = "00010012-0000-1000-8000-00805F9B0000";
    public static final int TYPE_GET_INFO = 1;
    public static final int TYPE_GET_DOSE = 2;

    public static final int STATE_IDLE = 102;
    public static final int STATE_PARING = 103;
    public static final int STATE_PARED = 104;

    private static final String FIRST_SEQUENCE_NUMBER_CHAR_UUID = "00010013-0000-1000-8000-00805F9B0000";
    private static final String NUM_FILE_CHUNKS_CHAR_UUID = "00010014-0000-1000-8000-00805F9B0000";
    private static final String FILE_CHUNK_ID_CHAR_UUID = "00010015-0000-1000-8000-00805F9B0000";
    private static final String FILE_CHUNK_CHAR_UUID = "00010016-0000-1000-8000-00805F9B0000";
    private final static String TAG = BLEService.class.getSimpleName();
    private static final int CSC_LENGTH = 16;
    private static final String AES_IV_CHUNK = "Revival_IV__AES ";
    private static final String AES_KEY = "5265766976616C5F4B65795F41455320";
    private static final int REVIVAL_RECORD_SIZE = 11;
    private final IBinder mBinder = new LocalBinder();
    private final Handler mHandler;
    private BLEInfoEntity mData;
    private int mState;
    private int mTypeGetData;
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private ArrayList<BluetoothGattCharacteristic> mGattCharacteristics = new ArrayList<>();
    private byte mNumberFileChunk;
    private OnBLECallBack mCallBack;
    private boolean mIsStillGetInfo = false;
    private String mStateIdle = "mState = STATE_IDLE";
    private byte mNumLastSequence;
    private int mIndexInfo;
    public static final String DESCRIPTION_KEY = "getDescriptionKey....";

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                if (mCallBack != null) mCallBack.onConnect();
                mState = STATE_PARING;
                mTypeGetData = TYPE_GET_INFO;
                mIndexInfo = 0;
                mData = new BLEInfoEntity();

                CommonUtil.wtfi(TAG, "mState = STATE_PARING");
                CommonUtil.wtfi(TAG, "Connected to GATT server.");
                CommonUtil.wtfi(TAG, "Attempting to start service discovery:" + mBluetoothGatt.discoverServices());
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                CommonUtil.wtfi(TAG, "Disconnected from GATT server.");
                if (mCallBack != null) mCallBack.onDisconnect();
                CommonUtil.wtfi(TAG, mStateIdle);
                mState = STATE_IDLE;
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                mGattCharacteristics.clear();
                displayGattServices();
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                processCharacteristic(characteristic);
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic vl, int status) {
            CommonUtil.wtfi(TAG, "onCharacteristicWrite...FIRST_SEQUENCE_NUMBER_CHAR_UUID: " + vl.getUuid().toString());

            switch (vl.getUuid().toString().toUpperCase()) {
                case FIRST_SEQUENCE_NUMBER_CHAR_UUID:
                    BluetoothGattCharacteristic character = getCharacter(NUM_FILE_CHUNKS_CHAR_UUID);
                    if (character != null) {
                        CommonUtil.wtfi(TAG, "writeLastSequenceNumber...NUM_FILE_CHUNKS_CHAR_UUID");
                        character.getDescriptors().get(0).setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                        mBluetoothGatt.readDescriptor(character.getDescriptors().get(0));
                        break;
                    }
                    break;

                case FILE_CHUNK_ID_CHAR_UUID:
                    character = getCharacter(FILE_CHUNK_ID_CHAR_UUID);
                    if (character != null) {
                        character.getDescriptors().get(0).setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                        mBluetoothGatt.readDescriptor(character.getDescriptors().get(0));
                    }
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor des, int status) {
            CommonUtil.wtfi(TAG, "onDescriptorRead...FIRST_SEQUENCE_NUMBER_CHAR_UUID: " + des.getCharacteristic().getUuid().toString());
            CommonUtil.wtfi(TAG, "onDescriptorRead...FIRST_SEQUENCE_NUMBER_CHAR_UUID: BluetoothGattDescriptor: " + Arrays.toString(des.getValue()));
            switch (des.getCharacteristic().getUuid().toString().toUpperCase()) {
                case NUM_FILE_CHUNKS_CHAR_UUID:
                    mBluetoothGatt.readCharacteristic(des.getCharacteristic());
                    break;
                case FILE_CHUNK_ID_CHAR_UUID:
                    for (int i = 0; i < mGattCharacteristics.size(); i++) {
                        BluetoothGattCharacteristic characteristic = mGattCharacteristics.get(i);
                        if (characteristic.getUuid().toString().toUpperCase().equals(FILE_CHUNK_CHAR_UUID)) {
                            mBluetoothGatt.readCharacteristic(characteristic);
                        }
                    }
                    break;
                default:
                    break;
            }

        }
    };

    public BLEService() {
        mState = STATE_IDLE;
        mTypeGetData = TYPE_GET_INFO;
        mData = new BLEInfoEntity();
        CommonUtil.wtfi(TAG, mStateIdle);
        mHandler = new Handler(msg -> {
            CommonUtil.wtfi(TAG, "mHandler: mTypeGetData" + mTypeGetData);
            if (mTypeGetData == TYPE_GET_INFO) {
                readInfoData();
            }
            return false;
        });
    }

    public void setCallBack(OnBLECallBack mCallBack) {
        this.mCallBack = mCallBack;
    }

    public void readData() {
        CommonUtil.wtfi(TAG, "readData...Start read dose log...");
        for (int i = 0; i < mGattCharacteristics.size(); i++) {
            final BluetoothGattCharacteristic characteristic = mGattCharacteristics.get(i);

            CommonUtil.wtfi(TAG, "mGattCharacteristics...uuid: " + characteristic.getUuid().toString());
            if (characteristic.getUuid().toString().toUpperCase().equals(BLEService.LAST_SEQUENCE_NUMBER_CHAR_UUID)) {
                final BluetoothGattDescriptor clientCharacteristicConfigurationDescriptor = new BluetoothGattDescriptor(characteristic.getUuid(),
                        BluetoothGattDescriptor.PERMISSION_READ | BluetoothGattDescriptor.PERMISSION_WRITE);
                clientCharacteristicConfigurationDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                characteristic.addDescriptor(clientCharacteristicConfigurationDescriptor);
                mBluetoothGatt.readCharacteristic(characteristic);
                return;
            }
        }
    }

    private void displayGattServices() {
        CommonUtil.wtfi(TAG, "displayGattServices()");
        if (mGattCharacteristics.size() == 0) {
            for (BluetoothGattService gattService : mBluetoothGatt.getServices()) {
                List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
                mGattCharacteristics.addAll(gattCharacteristics);
            }
        }

        mHandler.sendMessage(Message.obtain());
    }

    public void readInfoData() {
        CommonUtil.wtfi(TAG, "readInfoData...mIndexInfo: " + mIndexInfo);

        if (mData.getDeviceName() != null && !mData.getDeviceName().isEmpty()
                && mData.getSoftwareRevision() != null && !mData.getSoftwareRevision().isEmpty()
                && mData.getHardwareRevision() != null && !mData.getHardwareRevision().isEmpty()
                && mData.getFirmwareRevision() != null && !mData.getFirmwareRevision().isEmpty()
                && mData.getSystemId() != null && !mData.getSystemId().isEmpty()
                && mData.getSystemFlag() != null && !mData.getSystemFlag().isEmpty() || mIndexInfo >= 7) {
            mTypeGetData = TYPE_GET_DOSE;
            readData();
            return;
        }

        for (int i = 0; i < mGattCharacteristics.size(); i++) {
            final BluetoothGattCharacteristic characteristic = mGattCharacteristics.get(i);
            switch (characteristic.getUuid().toString().toUpperCase()) {
                case DEVICE_NAME_UUID:
                    if (mData.getDeviceName() != null && !mData.getDeviceName().isEmpty()) {
                        continue;
                    }
                    initReadingData(characteristic);
                    return;
                case SERIAL_NUMBER_UUID:
                    if (mData.getSerialNumber() != null && !mData.getSerialNumber().isEmpty()) {
                        continue;
                    }
                    initReadingData(characteristic);
                    return;
                case BLE_SOFTWARE_REVISION_UUID:
                    if (mData.getSoftwareRevision() != null && !mData.getSoftwareRevision().isEmpty()) {
                        continue;
                    }
                    initReadingData(characteristic);
                    return;
                case HARDWARE_VERSION_UUID:
                    if (mData.getHardwareRevision() != null && !mData.getHardwareRevision().isEmpty()) {
                        continue;
                    }
                    initReadingData(characteristic);
                    return;
                case FIRMWARE_VERSION_UUID:
                    if (mData.getFirmwareRevision() != null && !mData.getFirmwareRevision().isEmpty()) {
                        continue;
                    }
                    initReadingData(characteristic);
                    return;
                case SYSTEM_ID_UUID:
                    if (mData.getSystemId() != null && !mData.getSystemId().isEmpty()) {
                        continue;
                    }
                    initReadingData(characteristic);
                    return;
                case SYSTEM_FLAG:
                    if (mData.getSystemFlag() != null && !mData.getSystemFlag().isEmpty()) {
                        continue;
                    }
                    initReadingData(characteristic);
                    return;
                default:
                    break;
            }
        }
    }

    private void initReadingData(BluetoothGattCharacteristic characteristic) {
        mIndexInfo++;
        final BluetoothGattDescriptor clientCharacteristicConfigurationDescriptor = new BluetoothGattDescriptor(characteristic.getUuid(),
                BluetoothGattDescriptor.PERMISSION_READ | BluetoothGattDescriptor.PERMISSION_WRITE);
        clientCharacteristicConfigurationDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        characteristic.addDescriptor(clientCharacteristicConfigurationDescriptor);
        mBluetoothGatt.readCharacteristic(characteristic);
    }

    private void processCharacteristic(final BluetoothGattCharacteristic characteristic) {
        switch (mTypeGetData) {
            case TYPE_GET_DOSE:
                processDoseLog(characteristic);
                break;
            case TYPE_GET_INFO:
                processInfo(characteristic);
                break;
            default:
                disconnectBLE();
                break;
        }
    }

    private void processInfo(BluetoothGattCharacteristic characteristic) {
        byte[] data = characteristic.getValue();
        CommonUtil.wtfd(TAG, "getUuid(): " + characteristic.getUuid().toString());
        CommonUtil.wtfd(TAG, "DATA BYTE: " + Arrays.toString(data));
        CommonUtil.wtfi(TAG, "UUID_LIST: " + characteristic.getUuid().toString().toUpperCase());
        switch (characteristic.getUuid().toString().toUpperCase()) {
            case DEVICE_NAME_UUID:
                mData.setDeviceName(new String(data));
                CommonUtil.wtfi(TAG, "DEVICE_NAME_UUID: " + mData.getDeviceName());
                break;
            case SERIAL_NUMBER_UUID:
                mData.setSerialNumber(new String(data));
                CommonUtil.wtfi(TAG, "SERIAL_NUMBER_UUID: " + mData.getSerialNumber());
                break;
            case BLE_SOFTWARE_REVISION_UUID:
                mData.setSoftwareRevision(new String(data));
                CommonUtil.wtfi(TAG, "BLE_SOFTWARE_REVISION_UUID: " + mData.getSoftwareRevision());
                break;
            case HARDWARE_VERSION_UUID:
                mData.setHardwareRevision(new String(data));
                CommonUtil.wtfi(TAG, "HARDWARE_VERSION_UUID: " + mData.getHardwareRevision());
                break;
            case FIRMWARE_VERSION_UUID:
                mData.setFirmwareRevision(new String(data));
                CommonUtil.wtfi(TAG, "FIRMWARE_VERSION_UUID: " + mData.getFirmwareRevision());
                break;
            case SYSTEM_ID_UUID:
                mData.setSystemId(new String(data));
                CommonUtil.wtfi(TAG, "SYSTEM_ID_UUID: " + mData.getSystemId());
                break;
            case SYSTEM_FLAG:
                mData.setSystemFlag(data[0] + "");
                CommonUtil.wtfi(TAG, "SYSTEM_FLAG_UUID: " + mData.getSystemFlag());
                break;
            default:
                break;
        }

        for (int i = 0; i < mGattCharacteristics.size(); i++) {
            BluetoothGattCharacteristic cha = mGattCharacteristics.get(i);
            if (characteristic.getUuid().toString().toUpperCase().equals(characteristic.getUuid().toString().toUpperCase())) {
                mGattCharacteristics.remove(cha);
                break;
            }
        }

        readInfoData();
    }

    private void processDoseLog(BluetoothGattCharacteristic characteristic) {
        switch (characteristic.getUuid().toString().toUpperCase()) {
            case LAST_SEQUENCE_NUMBER_CHAR_UUID:
                CommonUtil.wtfi(TAG, "LAST_SEQUENCE_NUMBER_CHAR_UUID");
                writeLastSequenceNumber(characteristic.getValue());
                return;
            case NUM_FILE_CHUNKS_CHAR_UUID:
                CommonUtil.wtfi(TAG, "NUM_FILE_CHUNKS_CHAR_UUID");
                writeNumberChunkFile(characteristic.getValue());
                return;
            default:
                break;
        }

        byte[] data = characteristic.getValue();
        CommonUtil.wtfi(TAG, "getUuid(): " + characteristic.getUuid().toString());
        CommonUtil.wtfi(TAG, "DATA BYTE: " + Arrays.toString(data));

        if (characteristic.getUuid().toString().toUpperCase().equals(FILE_CHUNK_CHAR_UUID)) {
            if (data.length > 0) {
                try {
                    decrypt(data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (mData.getDoseItems().size() >= mNumLastSequence) {
                CommonUtil.wtfi(TAG, "mState = STATE_PARED");
                if (mCallBack != null) mCallBack.onReceiveDoseLog(mData);
                disconnectBLE();
                return;
            }

            startWriteFirstSequence(mData.getDoseItems().size() + 1);
        }
    }

    private void writeNumberChunkFile(byte[] value) {
        mNumberFileChunk = value[0];
        if (mNumberFileChunk == 0) {
            CommonUtil.wtfe(TAG, "mNumberFileChunk error: 0");
            startWriteFirstSequence(1);
            return;
        }

        CommonUtil.wtfe(TAG, "mNumberFileChunk:" + mNumberFileChunk);
        CommonUtil.wtfe(TAG, "mIndexNumberFileChunk:" + 1);
        startWriteFileChunkId();
    }

    private void startWriteFileChunkId() {
        BluetoothGattCharacteristic character = getCharacter(FILE_CHUNK_ID_CHAR_UUID);
        if (character != null) {
            character.setValue(new byte[]{1});
            mBluetoothGatt.writeCharacteristic(character);
        }
    }

    private BluetoothGattCharacteristic getCharacter(String key) {
        for (int i = 0; i < mGattCharacteristics.size(); i++) {
            BluetoothGattCharacteristic characteristic = mGattCharacteristics.get(i);
            if (characteristic.getUuid().toString().toUpperCase().equals(key)) {
                return characteristic;
            }
        }
        return null;
    }

    private void writeLastSequenceNumber(byte[] value) {
        if (value == null || value.length == 0) return;
        mNumLastSequence = value[0];
        CommonUtil.wtfe(TAG, "mNumLastSequence:" + mNumLastSequence);
        if (mNumLastSequence == 0) {

            CommonUtil.wtfi(TAG, "mState = STATE_PARED");
            if (mCallBack != null) mCallBack.onReceiveDoseLog(mData);
            disconnectBLE();
            return;
        }

        startWriteFirstSequence(1);
    }

    private void startWriteFirstSequence(int start) {
        CommonUtil.wtfi(TAG, "mIndexNumLastSequence: " + 1);
        BluetoothGattCharacteristic character = getCharacter(FIRST_SEQUENCE_NUMBER_CHAR_UUID);
        if (character != null) {
            character.setValue(start, BluetoothGattCharacteristic.FORMAT_UINT16, 0);
            mBluetoothGatt.writeCharacteristic(character);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        close();
        return super.onUnbind(intent);
    }

    public boolean init() {
        mData.getDoseItems().clear();
        resetTypeGet();

        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                CommonUtil.wtfe(TAG, "Unable to initialize BluetoothManager.");
                return true;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            CommonUtil.wtfe(TAG, "Unable to obtain a BluetoothAdapter.");
            return true;
        }

        return false;
    }

    public void connect(final String address) {
        if (mState == STATE_PARING && !mIsStillGetInfo) return;

        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return;
        }
        if (address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            CommonUtil.wtfd(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            boolean isConnect = mBluetoothGatt.connect();
            if (isConnect) {
                mGattCharacteristics.clear();
                displayGattServices();
            }
            return;
        }

        BluetoothDevice mDevice = mBluetoothAdapter.getRemoteDevice(address);
        if (mDevice == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return;
        }

        CommonUtil.wtfi(TAG, "myDevice.createBond(): " + mDevice.createBond());
        CommonUtil.wtfi(TAG, "myDevice device.getBondState(): " + mDevice.getBondState());
        mBluetoothGatt = mDevice.connectGatt(this, false, mGattCallback);
        CommonUtil.wtfd(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
    }

    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    private void decrypt(byte[] data) throws DecoderException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException, InvalidKeyException {
        CommonUtil.wtfi(TAG, "Input: " + Arrays.toString(data));

        byte[] keyDataFromHex = Hex.decodeHex(getDescriptionKey());
        Key key = new SecretKeySpec(keyDataFromHex, "AES");
        IvParameterSpec iv = new IvParameterSpec(AES_IV_CHUNK.getBytes(StandardCharsets.UTF_8));
        Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
        cipher.init(Cipher.DECRYPT_MODE, key, iv);

        byte[] rawOutput = cipher.doFinal(data);
        //Remove un-used section:
        int crcCheck = rawOutput.length - CSC_LENGTH;
        CommonUtil.wtfi(TAG, "CRC check: " + crcCheck);

        byte[] output = Arrays.copyOfRange(rawOutput, 0, crcCheck);
        CommonUtil.wtfi(TAG, "Output: " + Arrays.toString(output));

        int numberOfLogs = output.length / REVIVAL_RECORD_SIZE;
        CommonUtil.wtfi(TAG, "Number of logs: " + numberOfLogs);

        //Removing the padding:
        int expectedBytesCount = numberOfLogs * REVIVAL_RECORD_SIZE;
        byte[] dataWithoutPadding = Arrays.copyOfRange(output, 0, expectedBytesCount);

        //Parsing the cleaned data:
        for (int i = 1; i <= numberOfLogs; i++) {
            int start = (i - 1) * REVIVAL_RECORD_SIZE;
            int end = i * REVIVAL_RECORD_SIZE;

            byte[] singleDoseData = Arrays.copyOfRange(dataWithoutPadding, start, end);
            //Parse data to Dose Log:
            parseSingleDoseLog(singleDoseData);
        }
    }

    private char[] getDescriptionKey() {
        CommonUtil.wtfi(TAG, DESCRIPTION_KEY);
        for (int i = 0; i < CAApplication.getInstance().getStorageCommon().getNNMDDecryptionKey().size(); i++) {
            RDecryptionKeyEntity item = CAApplication.getInstance().getStorageCommon().getNNMDDecryptionKey().get(i);
            if (item.getSystemId().equals(mData.getSystemId().replace("0x", ""))
                    && item.getDecryptionKey() != null && !item.getDecryptionKey().isEmpty()) {
                CommonUtil.wtfi(TAG, DESCRIPTION_KEY + item.getSystemId());
                CommonUtil.wtfi(TAG, DESCRIPTION_KEY + item.getDecryptionKey());
                return item.getDecryptionKey().toCharArray();
            }
        }

        return AES_KEY.toCharArray();
    }

    private byte[] reverseArr(byte[] data) {
        byte[] arrData = new byte[data.length];
        for (int i = data.length - 1; i >= 0; i--) {
            arrData[data.length - 1 - i] = data[i];
        }

        CommonUtil.wtfi(TAG, "reverse arr: " + Arrays.toString(arrData));

        return arrData;
    }

    private void parseSingleDoseLog(byte[] singleDoseLogData) {
        CommonUtil.wtfi(TAG, "Single item: " + Arrays.toString(singleDoseLogData));

        //Parse Dose Amount:
        byte[] doseAmountBytes = Arrays.copyOfRange(singleDoseLogData, 6, 8);
        doseAmountBytes = reverseArr(doseAmountBytes);
        int doseAmount = new BigInteger(doseAmountBytes).intValue();

        //Parse Drug Type:
        byte[] drugTypeBytes = Arrays.copyOfRange(singleDoseLogData, 8, 9);
        drugTypeBytes = reverseArr(drugTypeBytes);
        int drugType = new BigInteger(drugTypeBytes).intValue();

        //Parse status flag:
        byte[] statusFlagBytes = Arrays.copyOfRange(singleDoseLogData, 9, 11);
        statusFlagBytes = reverseArr(statusFlagBytes);
        int statusFlag = new BigInteger(statusFlagBytes).intValue();

        //Parse sequence number:
        byte[] sequenceNumberBytes = Arrays.copyOfRange(singleDoseLogData, 0, 2);
        sequenceNumberBytes = reverseArr(sequenceNumberBytes);
        int sequenceNumber = new BigInteger(sequenceNumberBytes).intValue();

        //Parse time:
        byte[] timeBytes = Arrays.copyOfRange(singleDoseLogData, 2, 6);
        timeBytes = reverseArr(timeBytes);

        long currentTimeSince1970 = new Date().getTime();
        long timeSinceDose = new BigInteger(timeBytes).longValue();
        long calculatedTime = currentTimeSince1970 - (timeSinceDose * 1000);

        CommonUtil.wtfi(TAG, "Dose amount : " + doseAmount);
        CommonUtil.wtfi(TAG, "Sequence number : " + sequenceNumber);
        CommonUtil.wtfi(TAG, "Time since dose : " + calculatedTime);
        CommonUtil.wtfi(TAG, "Drug type : " + drugType);
        CommonUtil.wtfi(TAG, "Status flag : " + statusFlag);

        if (sequenceNumber == 0) return;
        mData.getDoseItems().add(new DoseItem(doseAmount, sequenceNumber, calculatedTime, drugType, statusFlag));
    }

    private void resetTypeGet() {
        CommonUtil.wtfi(TAG, "resetTypeGet()....");
        mNumberFileChunk = 0;
        mBluetoothDeviceAddress = null;
    }

    public void disconnectBLE() {
        CommonUtil.wtfi(TAG, "disconnectBLE()...");
        if (mBluetoothGatt != null) {
            mBluetoothGatt.disconnect();
        }

        mData = new BLEInfoEntity();
        resetTypeGet();
        mIndexInfo = 0;
        mTypeGetData = TYPE_GET_INFO;
        mState = STATE_IDLE;
        mIsStillGetInfo = false;
        CommonUtil.wtfi(TAG, mStateIdle);
    }

    public void setType(int typeGetInfo) {
        mTypeGetData = typeGetInfo;
    }

    public boolean isPaired() {
        CommonUtil.wtfi(TAG, "mState = " + mState);
        return this.mState == STATE_PARED && mIsStillGetInfo;
    }

    public void resetBLE() {
        CommonUtil.wtfi(TAG, "resetBLE()....");
        mIsStillGetInfo = true;
        mBluetoothGatt.disconnect();
    }

    public class LocalBinder extends Binder {
        public BLEService getService() {
            return BLEService.this;
        }
    }
}
