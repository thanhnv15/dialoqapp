package com.novo.app.view.fragment;

import android.text.Editable;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M045SignUpPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseEditText;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM045SignUpCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.TextAdapter;

import java.util.Objects;

public class M045SignUpEmailConfirmFrg extends BaseFragment<M045SignUpPresenter, OnHomeBackToView> implements OnM045SignUpCallBack {

    public static final String TAG = M045SignUpEmailConfirmFrg.class.getName();
    private TableRow mTrError;
    private Button mBtNext;
    private BaseEditText mEdtEmail;
    private TextView mTvError;

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m045_logo);
        findViewById(R.id.iv_m045_back, this);
        findViewById(R.id.tv_m045_account_setup, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m045_description, CAApplication.getInstance().getRegularFont());
        mTvError = findViewById(R.id.tv_m045_error, CAApplication.getInstance().getRegularFont());

        findViewById(R.id.tv_m045_cancel, this, CAApplication.getInstance().getBoldFont());

        mBtNext = findViewById(R.id.bt_m045_next, this, CAApplication.getInstance().getRegularFont());
        mBtNext.setEnabled(false);

        mTrError = findViewById(R.id.tr_m045_error);
        mTrError.setVisibility(View.GONE);

        mEdtEmail = findViewById(R.id.tv_m045_email, CAApplication.getInstance().getRegularFont());
        if (!mEdtEmail.getText().toString().isEmpty()) mBtNext.setEnabled(true);
        onEmailChange();
        onEmailFocusChange();
        onKeyBoardDismiss();
    }

    private void onKeyBoardDismiss() {
        mEdtEmail.setOnEditTextImeBackListener((ctrl, text) -> {
            mEdtEmail.clearFocus();
        });

        mEdtEmail.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEdtEmail.clearFocus();
            }
            return false;
        });
    }

    private void onEmailFocusChange() {
        mEdtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                boolean show = false;
                String email = Objects.requireNonNull(mEdtEmail.getText()).toString();
                if (!hasFocus) {
                    if (CommonUtil.isNotEmail(email) || !email.equals(getStorage().getM001ProfileEntity().getEmail())) {
                        show = true;
                    }
                    showError(show, mEdtEmail.getText().toString().isEmpty());
                } else {
                    showError(false, mEdtEmail.getText().toString().isEmpty());
                    mBtNext.setEnabled(false);
                }
            }
        });
    }

    private void onEmailChange() {
        mEdtEmail.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                CommonUtil.getInstance().initTypeFace(mEdtEmail);
                showError(false, Objects.requireNonNull(mEdtEmail.getText()).toString().isEmpty());
                mBtNext.setEnabled(!CommonUtil.isNotEmail(mEdtEmail.getText().toString())
                        && editable.toString().equals(getStorage().getM001ProfileEntity().getEmail()));
            }
        });
    }

    private void showError(boolean show, boolean isEmpty) {
        if (show) {
            int color;
            String msg = "";
            if (isEmpty) {
                msg = getString(R.string.txt_m009_email_require);
                color = getResources().getColor(R.color.colorBlack);
            } else {
                String email = Objects.requireNonNull(mEdtEmail.getText()).toString();
                if (CommonUtil.isNotEmail(email)) {
                    msg = LangMgr.getInstance().getLangList().get(getString(R.string.lang_err_invalid_email));
                } else if (!email.equals(getStorage().getM001ProfileEntity().getEmail())) {
                    msg = getString(R.string.txt_m045_invalid);
                }
                color = getResources().getColor(R.color.colorM009RustyRed);
            }
            mTvError.setText(msg);
            mEdtEmail.setTextColor(color);
            mTrError.setVisibility(View.VISIBLE);
        } else {
            mEdtEmail.setTextColor(getResources().getColor(R.color.colorBlack));
            mTrError.setVisibility(View.GONE);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m045_signup_email_confirm;
    }

    @Override
    protected M045SignUpPresenter getPresenter() {
        return new M045SignUpPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //Wait for customer confirmation
    }

    @Override
    public void backToPreviousScreen() {
        mCallBack.showFrgScreen(TAG, M009SignUpEmailFrg.TAG);
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m045_next:
                CommonUtil.getInstance().forceHideKeyBoard(mEdtEmail);
                mCallBack.showFrgScreen(TAG, M010SignUpPasswordFrg.TAG);
                break;

            case R.id.tv_m045_cancel:
                CommonUtil.getInstance().forceHideKeyBoard(mEdtEmail);
                mCallBack.registrationCancel(TAG);
                break;

            case R.id.iv_m045_back:
                CommonUtil.getInstance().forceHideKeyBoard(mEdtEmail);
                backToPreviousScreen();
                break;
            default:
                break;
        }
    }
}
