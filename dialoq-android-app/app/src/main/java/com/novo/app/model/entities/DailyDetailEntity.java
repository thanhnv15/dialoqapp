package com.novo.app.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DailyDetailEntity implements Serializable {
    private List<DataInfo> noteList = new ArrayList<>();
    private List<DataInfo> doseList = new ArrayList<>();
    private List<DataInfo> successList = new ArrayList<>();
    private String drugName;
    private String date;
    private int colorType;
    private int position;

    public DailyDetailEntity() {

    }

    public List<DataInfo> getNoteList() {
        return noteList;
    }

    public void setNoteList(List<DataInfo> noteList) {
        this.noteList = noteList;
    }

    public List<DataInfo> getDoseList() {
        return doseList;
    }

    public void setDoseList(List<DataInfo> doseList) {
        this.doseList = doseList;
    }

    public String getDrugName(){
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public List<DataInfo> getSuccessList() {
        return successList;
    }

    public void setSuccessList(List<DataInfo> successList) {
        this.successList = successList;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getColorType() {
        return colorType;
    }

    public void setColorType(int colorType) {
        this.colorType = colorType;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
