package com.novo.app.view.fragment;

import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.presenter.M008SignUpPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.CASpinnerAdapter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM008SignUpCallBack;
import com.novo.app.view.event.OnSpinnerCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

import java.util.ArrayList;
import java.util.List;

public class M008SignUpDiabetesTypeFrg extends BaseFragment<M008SignUpPresenter, OnHomeBackToView> implements OnM008SignUpCallBack, OnSpinnerCallBack {

    public static final String TAG = M008SignUpDiabetesTypeFrg.class.getName();
    private static final int KEY_SPINNER_DIABETES_TYPE = 2;
    private Button mBtNext;
    private TextView mTvDiabetes;
    private Spinner mSpinnerDiabetes;

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m008_logo);
        findViewById(R.id.iv_m008_back, this);
        findViewById(R.id.tv_m008_account_setup, CAApplication.getInstance().getBoldFont());
        TextView mTvDescription = findViewById(R.id.tv_m008_description, CAApplication.getInstance().getRegularFont());
        if (!getStorage().isMature())
            mTvDescription.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m008_description_immature)));
        findViewById(R.id.tv_m008_cancel, this, CAApplication.getInstance().getMediumFont());
        mBtNext = findViewById(R.id.bt_m008_next, this, CAApplication.getInstance().getMediumFont());
        mBtNext.setEnabled(false);

        mTvDiabetes = findViewById(R.id.tv_m008_diabetes, this, CAApplication.getInstance().getRegularFont());
        if (getStorage().getM001ProfileEntity().getTypeOfDiabetes() != null && !getStorage().getM001ProfileEntity().getTypeOfDiabetes().getValue().equals("")) {
            mTvDiabetes.setText(getStorage().getM001ProfileEntity().getTypeOfDiabetes().getValue());
            mTvDiabetes.setTypeface(CAApplication.getInstance().getBoldFont());
            mBtNext.setEnabled(true);
        }

        findViewById(R.id.iv_m008_diabetes, this);

        mSpinnerDiabetes = findViewById(R.id.spin_m008_diabetes);
        CASpinnerAdapter<OnSpinnerCallBack> adapter = new CASpinnerAdapter<>(KEY_SPINNER_DIABETES_TYPE, prepareListData()
                , mContext);
        adapter.setOnSpinnerCallBack(this);
        mSpinnerDiabetes.setAdapter(adapter);
        mTvDiabetes.setTag(getStorage().getM001ProfileEntity().getTypeOfDiabetes());
    }

    private List<TextEntity> prepareListData() {
        List<TextEntity> data = getStorage().getM001ConfigSet().getDiabeteEntity();
        List<TextEntity> listData = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            String key = data.get(i).getKey();
            String value = LangMgr.getInstance().getLangList().get(data.get(i).getValue());
            listData.add(new TextEntity(key, value));
        }
        return listData;
    }


    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.frg_m008_signup_diabetes_type;
    }

    @Override
    protected M008SignUpPresenter getPresenter() {
        return new M008SignUpPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //Wait for customer confirmation
    }

    @Override
    public void backToPreviousScreen() {
        mCallBack.showFrgScreen(TAG, M018ProfileGenderFrg.TAG);
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m008_next:
                getStorage().getM001ProfileEntity().setTypeOfDiabetes(((TextEntity) mTvDiabetes.getTag()));
                mCallBack.showFrgScreen(TAG, M019SignUpHowLongFrg.TAG);
                break;
            case R.id.tv_m008_cancel:
                getStorage().getM001ProfileEntity().setTypeOfDiabetes(new TextEntity("", ""));
                mCallBack.showFrgScreen(TAG, M019SignUpHowLongFrg.TAG);
                break;
            case R.id.iv_m008_back:
                backToPreviousScreen();
                break;
            case R.id.tv_m008_diabetes:
            case R.id.iv_m008_diabetes:
                mSpinnerDiabetes.performClick();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(int key, int pos, Object data) {
        if (key == KEY_SPINNER_DIABETES_TYPE) {
            TextEntity entity = ((List<TextEntity>) data).get(pos);
            mTvDiabetes.setText(entity.getValue());
            mTvDiabetes.setTag(entity);

            mTvDiabetes.setTypeface(CAApplication.getInstance().getBoldFont());
            mBtNext.setEnabled(true);
            closeSpinner(mSpinnerDiabetes);
        }
    }
}
