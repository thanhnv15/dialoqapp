package com.novo.app.manager;

import com.novo.app.CAApplication;
import com.novo.app.utils.StorageCommon;

/**
 * Created by ThanhNv on 4/23/2017.
 */
public abstract class BaseMgr {
    public final StorageCommon getStorage() {
        return CAApplication.getInstance().getStorageCommon();
    }
}
