package com.novo.app.view.event;

public interface OnM065ReminderCallBack extends OnCallBackToView {
    default void showAlert(String sms) {

    }

    void closeDialog(String key);

    void toM051More();
}
