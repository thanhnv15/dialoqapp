package com.novo.app.model.entities;

import com.google.gson.annotations.SerializedName;
import com.novo.app.model.BaseModel;

public class PatientEntity extends BaseModel {
    @SerializedName("firstName")
    private String firstName;
    @SerializedName("middleName")
    private String middleName;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("email")
    private String email;
    @SerializedName("mobilePhoneNumber")
    private String mobilePhoneNumber;
    @SerializedName("userId")
    private int userId;
    @SerializedName("userName")
    private String userName;
    @SerializedName("gender")
    private String gender;
    @SerializedName("dateOfBirth")
    private String dateOfBirth;
    @SerializedName("phoneNumber")
    private String phoneNumber;
    @SerializedName("faxNumber")
    private String faxNumber;
    @SerializedName("addressType")
    private String addressType;
    @SerializedName("country")
    private String country;
    @SerializedName("state")
    private String state;
    @SerializedName("city")
    private String city;
    @SerializedName("streetAddress1")
    private String streetAddress1;
    @SerializedName("streetAddress2")
    private String streetAddress2;
    @SerializedName("zipCode")
    private String zipCode;
    @SerializedName("mobileDeviceId")
    private String[] mobileDeviceId;
    @SerializedName("zipcodePrefix3Chars")
    private String zipcodePrefix3Chars;
    @SerializedName("ssn")
    private String ssn;
    @SerializedName("driverLicenseNumber")
    private String driverLicenseNumber;
    @SerializedName("emergencyPhoneNumber")
    private String[] emergencyPhoneNumber;
    @SerializedName("nationality")
    private String nationality;
    @SerializedName("maritalStatus")
    private String maritalStatus;
    @SerializedName("birthplace")
    private String birthplace;
    @SerializedName("preferredPharmacyName")
    private String[] preferredPharmacyName;
    @SerializedName("medicalRecordNumber")
    private String medicalRecordNumber;
    @SerializedName("healthPlanBeneficiaryNumber")
    private String healthPlanBeneficiaryNumber;
    @SerializedName("medicationHistory")
    private String medicationHistory;
    @SerializedName("allergies")
    private String allergies;
    @SerializedName("familyHistory")
    private String familyHistory;
    @SerializedName("personalHistory")
    private String personalHistory;
    @SerializedName("race")
    private String race;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    public void setMobilePhoneNumber(String mobilePhoneNumber) {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetAddress1() {
        return streetAddress1;
    }

    public void setStreetAddress1(String streetAddress1) {
        this.streetAddress1 = streetAddress1;
    }

    public String getStreetAddress2() {
        return streetAddress2;
    }

    public void setStreetAddress2(String streetAddress2) {
        this.streetAddress2 = streetAddress2;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String[] getMobileDeviceId() {
        return mobileDeviceId;
    }

    public void setMobileDeviceId(String[] mobileDeviceId) {
        this.mobileDeviceId = mobileDeviceId;
    }

    public String getZipcodePrefix3Chars() {
        return zipcodePrefix3Chars;
    }

    public void setZipcodePrefix3Chars(String zipcodePrefix3Chars) {
        this.zipcodePrefix3Chars = zipcodePrefix3Chars;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getDriverLicenseNumber() {
        return driverLicenseNumber;
    }

    public void setDriverLicenseNumber(String driverLicenseNumber) {
        this.driverLicenseNumber = driverLicenseNumber;
    }

    public String[] getEmergencyPhoneNumber() {
        return emergencyPhoneNumber;
    }

    public void setEmergencyPhoneNumber(String[] emergencyPhoneNumber) {
        this.emergencyPhoneNumber = emergencyPhoneNumber;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public String[] getPreferredPharmacyName() {
        return preferredPharmacyName;
    }

    public void setPreferredPharmacyName(String[] preferredPharmacyName) {
        this.preferredPharmacyName = preferredPharmacyName;
    }

    public String getMedicalRecordNumber() {
        return medicalRecordNumber;
    }

    public void setMedicalRecordNumber(String medicalRecordNumber) {
        this.medicalRecordNumber = medicalRecordNumber;
    }

    public String getHealthPlanBeneficiaryNumber() {
        return healthPlanBeneficiaryNumber;
    }

    public void setHealthPlanBeneficiaryNumber(String healthPlanBeneficiaryNumber) {
        this.healthPlanBeneficiaryNumber = healthPlanBeneficiaryNumber;
    }

    public String getMedicationHistory() {
        return medicationHistory;
    }

    public void setMedicationHistory(String medicationHistory) {
        this.medicationHistory = medicationHistory;
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    public String getFamilyHistory() {
        return familyHistory;
    }

    public void setFamilyHistory(String familyHistory) {
        this.familyHistory = familyHistory;
    }

    public String getPersonalHistory() {
        return personalHistory;
    }

    public void setPersonalHistory(String personalHistory) {
        this.personalHistory = personalHistory;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }
}
