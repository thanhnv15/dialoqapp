package com.novo.app.view.event;

public interface OnM042MoreCallBack extends OnCallBackToView {
    void showDeviceDetail();

    void showTroubleShooting();

    void updateList();

    void closeDeviceDetail();
}
