package com.novo.app.view.fragment;

import android.text.Editable;
import android.widget.Button;
import android.widget.EditText;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M005AccountSetUpNamePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM005AccountSetUpNameCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.TextAdapter;

public class M005AccountSetUpNameFrg extends BaseFragment<M005AccountSetUpNamePresenter, OnHomeBackToView> implements OnM005AccountSetUpNameCallBack {

    public static final String TAG = M005AccountSetUpNameFrg.class.getName();

    private EditText mEditFirstName;
    private Button btnNext;

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m005_logo);
        findViewById(R.id.tv_m005_cancel, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m005_account_setup, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m005_what_name, CAApplication.getInstance().getRegularFont());
        btnNext = findViewById(R.id.bt_m005_next, this, false, CAApplication.getInstance().getMediumFont());

        mEditFirstName = findViewById(R.id.ed_m005_first_name, CAApplication.getInstance().getRegularFont());
        if (getStorage().getM001ProfileEntity().getFirstName() != null) {
            mEditFirstName.setText(getStorage().getM001ProfileEntity().getFirstName());
            mEditFirstName.setTypeface(CAApplication.getInstance().getBoldFont());
        }

        if (!mEditFirstName.getText().toString().isEmpty()) btnNext.setEnabled(true);
        mEditFirstName.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0 && editable.length() < 100 && !editable.toString().equals(" ")) {
                    btnNext.setEnabled(true);
                    mEditFirstName.setTypeface(CAApplication.getInstance().getBoldFont());
                    getStorage().getM001ProfileEntity().setFirstName(mEditFirstName.getText().toString());
                } else if (editable.length() == 0) {
                    btnNext.setEnabled(false);
                    mEditFirstName.setTypeface(CAApplication.getInstance().getRegularFont());
                }
            }
        });
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m005_account_set_up_1st_name;
    }

    @Override
    protected M005AccountSetUpNamePresenter getPresenter() {
        return new M005AccountSetUpNamePresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m005_next:
                CommonUtil.getInstance().forceHideKeyBoard(mEditFirstName);
                mCallBack.showFrgScreen(TAG, M007AccountSetUpLastNameFrg.TAG);
                getStorage().getM001ProfileEntity().setFirstName(mEditFirstName.getText().toString());
                break;
            case R.id.tv_m005_cancel:
                CommonUtil.getInstance().forceHideKeyBoard(mEditFirstName);
                mCallBack.registrationCancel(TAG);
                break;
            default:
                break;
        }
    }

    @Override
    public void backToPreviousScreen() {
        mCallBack.showFrgScreen(TAG, M004AccountSetUpFrg.TAG);
    }
}
