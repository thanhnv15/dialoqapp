/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.view.dialog;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.model.entities.DailyDetailEntity;
import com.novo.app.model.entities.DateEntity;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.NoteEntity;
import com.novo.app.model.entities.ResultInfo;
import com.novo.app.presenter.M036DoseDetailPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.DoseLogAdapter;
import com.novo.app.view.adapter.M036DoseDetailAdapter;
import com.novo.app.view.adapter.M036DoseNoteAdapter;
import com.novo.app.view.base.BaseDialog;
import com.novo.app.view.event.OnM036DoseDetailCallBack;
import com.novo.app.view.event.OnM036DoseDetailDialog;
import com.novo.app.view.widget.NVDateUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

public class M036DoseDetailDialog extends BaseDialog<M036DoseDetailPresenter, OnM036DoseDetailDialog> implements OnM036DoseDetailCallBack {
    private static final String TAG = M036DoseDetailDialog.class.getName();
    private TextView tvDrugName;
    private TextView tvDrugDate;
    private TextView tvNoDose;
    private RecyclerView rvChart;
    private DailyDetailEntity mEntryData;
    private DateEntity mItem;
    private int colorType;
    private int currentPos;
    private boolean isShowingTresiba;

    public M036DoseDetailDialog(Context context, boolean isCancel) {
        super(context, isCancel, R.style.dialog_style);
    }

    @Override
    public void showM001LandingScreen(String err) {
        mCallBack.showM001LandingScreen(err);
    }

    public void setUpInfo(DateEntity item, int colorType) {
        this.mItem = item;
        this.colorType = colorType;
        this.mEntryData = item.getDailyDetail();
        this.currentPos = mEntryData.getPosition();
    }


    private String convertedName(int colorType) {
        String drugName = "";
        if (colorType == CommonUtil.COLOR_TYPE_TRESIBA || colorType == CommonUtil.COLOR_TYPE_TRESIBA_200) {
            drugName = DeviceEntity.TRESIBA;
            isShowingTresiba = true;
        } else if (colorType == CommonUtil.COLOR_TYPE_FIASP) {
            drugName = DeviceEntity.FIASP;
            isShowingTresiba = false;
        }
        return drugName;
    }

    private String convertedDate(String dateToConvert) {
        String drugDate = dateToConvert;
        drugDate = drugDate + "T00:00:00" + TimeZone.getDefault().getDisplayName(false, TimeZone.SHORT);
        drugDate = NVDateUtils.getGroupItemDateTitle(drugDate);
        drugDate = drugDate.substring(drugDate.indexOf(',') + 1);
        return drugDate;
    }

    @Override
    protected M036DoseDetailPresenter getPresenter() {
        return new M036DoseDetailPresenter(this);
    }

    @Override
    protected void initViews() {
        tvDrugName = findViewById(R.id.tv_m036_drug_name, CAApplication.getInstance().getBoldFont());
        tvDrugDate = findViewById(R.id.tv_m036_date, CAApplication.getInstance().getRegularFont());
        tvNoDose = findViewById(R.id.tv_m036_no_dose, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m036_note, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m036_priming, CAApplication.getInstance().getRegularFont());

        rvChart = findViewById(R.id.rv_m036_detail);

        findViewById(R.id.iv_m036_close, this);
        findViewById(R.id.iv_m036_prev, this);
        findViewById(R.id.iv_m036_next, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initData();
    }

    @Override
    public void initData() {
        if (mItem != null) {
            tvDrugName.setText(convertedName(colorType));
            tvDrugDate.setText(convertedDate(mItem.getStringDate()));

            List<ResultInfo> listDose = new ArrayList<>();
            if (!mEntryData.getDoseList().isEmpty()) {
                for (int i = 0; i < mEntryData.getDoseList().size(); i++) {
                    int drugCode = Integer.parseInt(mEntryData.getDoseList().get(i).getResult().getCustom3());
                    int flagCode = Integer.parseInt(mEntryData.getDoseList().get(i).getResult().getCustom4());
                    if (CommonUtil.isUnknownDrug(String.valueOf(drugCode), String.valueOf(flagCode))) {
                        listDose.add(mEntryData.getDoseList().get(i).getResult());
                    } else if (isShowingTresiba) {
                        if (drugCode != DeviceEntity.CODE_FIASP)
                            listDose.add(mEntryData.getDoseList().get(i).getResult());
                    } else {
                        if (drugCode != DeviceEntity.CODE_TRESIBA_100 && drugCode != DeviceEntity.CODE_TRESIBA_200)
                            listDose.add(mEntryData.getDoseList().get(i).getResult());
                    }
                }
            }

            List<NoteEntity> listNote = new ArrayList<>();
            for (int i = 0; i < mEntryData.getNoteList().size(); i++) {
                if (mEntryData.getNoteList().get(i).getResult().getAssessmentType().equals(DoseLogAdapter.TYPE_ASSESSMENT_NOTE)) {
                    String time = NVDateUtils.getOnlyTime(mEntryData.getNoteList().get(i).getResult().getReportedDate());
                    String note = mEntryData.getNoteList().get(i).getResult().getNotes();
                    listNote.add(new NoteEntity(time, note));
                }
            }

            if (!listDose.isEmpty()) {
                rvChart.setVisibility(View.VISIBLE);
                tvNoDose.setVisibility(View.GONE);

                LinearLayoutManager chartLayoutManager = new LinearLayoutManager(mContext);
                chartLayoutManager.setStackFromEnd(true);
                rvChart.setLayoutManager(chartLayoutManager);
                M036DoseDetailAdapter chartAdapter = new M036DoseDetailAdapter(mContext, listDose, this);
                rvChart.setAdapter(chartAdapter);
            } else {
                findViewById(R.id.tv_m036_priming).setVisibility(View.GONE);
                rvChart.setVisibility(View.GONE);
                tvNoDose.setVisibility(View.VISIBLE);
            }

            RecyclerView mRvNote = findViewById(R.id.rv_m036_note);
            mRvNote.setLayoutManager(new LinearLayoutManager(mContext));
            M036DoseNoteAdapter noteAdapter = new M036DoseNoteAdapter(mContext, listNote, this);
            mRvNote.setAdapter(noteAdapter);
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.view_m036_dose_detail;
    }

    @Override
    public void backToPreviousScreen() {
        //Do nothing here
    }

    @Override
    protected void onClickView(int idView) {
        CommonUtil.wtfi(TAG, "currentPos" + currentPos);
        switch (idView) {
            case R.id.iv_m036_close:
                dismiss();
                break;
            case R.id.iv_m036_next:
                switch (mCallBack.getCurrentPage()) {
                    case 0:
                        if (currentPos < 6)
                            mCallBack.toNextDay(colorType, currentPos);
                        break;
                    case 1:
                        if (currentPos < 13)
                            mCallBack.toNextDay(colorType, currentPos);
                        break;
                    case 2:
                        if (currentPos < 27)
                            mCallBack.toNextDay(colorType, currentPos);
                        break;
                }
                break;
            case R.id.iv_m036_prev:
                if (currentPos > 0)
                    mCallBack.toPreviousDay(colorType, currentPos);
                break;
        }
    }
}
