package com.novo.app.ble;

import java.io.Serializable;
import java.text.SimpleDateFormat;

public class DoseItem implements Serializable {
    private final SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private int doseAmount, sequenceNumber;
    private long calculatedTime;
    private int drugType;
    private int statusFlag;

    public DoseItem(int doseAmount, int sequenceNumber, long calculatedTime, int drugType, int statusFlag) {
        this.doseAmount = doseAmount;
        this.sequenceNumber = sequenceNumber;
        this.calculatedTime = calculatedTime;
        this.drugType = drugType;
        this.statusFlag = statusFlag;
    }

    public int getDoseAmount() {
        return doseAmount;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public long getCalculatedTime() {
        return calculatedTime;
    }

    public int getDrugType() {
        return drugType;
    }

    public int getStatusFlag() {
        return statusFlag;
    }

    @Override
    public String toString() {
        return "\ndoseType: " + drugType +
                "\nstatusFlag: " + statusFlag +
                "\ndoseAmount: " + doseAmount / 100 +
                "\nsequenceNumber: " + sequenceNumber +
                "\ncalculatedTime:" + df.format(calculatedTime) + "\n";
    }
}
