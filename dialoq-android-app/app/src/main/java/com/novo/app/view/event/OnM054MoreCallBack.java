package com.novo.app.view.event;

public interface OnM054MoreCallBack extends OnCallBackToView {
    default void showLoginScreen() {

    }

    default void backToAboutMe() {

    }

    void showDialog();

    void updateSuccess();

    void updateFailed();
}
