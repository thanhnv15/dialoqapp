/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.presenter;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RTokenEntity;
import com.novo.app.model.entities.CarePlanTemplateEntity;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.ConfigEntity;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.model.entities.TokenEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM040RecoveryCallBack;
import com.novo.app.view.widget.ProgressLoading;

public class M040RecoveryPresenter extends BasePresenter<OnM040RecoveryCallBack> {

    public static final String KEY_API_IA02_AUTHENTICATE_USER = "KEY_API_IA02_AUTHENTICATE_USER";
    public static final String KEY_BLOCKED_BY_COUNTRY = "BLOCKED_BY_COUNTRY";
    private static final String URL_API_IA04_RESET_PASSWORD_STEP_3 = "user-service/api/v1/reset-password?";
    private static final String KEY_API_IA04_RESET_PASSWORD_STEP_3 = "KEY_API_IA04_RESET_PASSWORD_STEP_3";
    private static final String BODY_IA04 = "{\"action\": \"performReset\",\"userName\": %s,\"token\": %s,\"password\": %s}";
    private static final String URL_API_IA02_AUTHENTICATE_USER = "user-service/api/v1/authenticate?";
    private static final String BODY_IA02 = "{\"userName\": %s,\"password\": %s,\"delivery\": \"SMS\"}";
    private static final String KEY_UN09_GET_USER_LOGGED_IN_PROFILE = "KEY_UN09_GET_USER_LOGGED_IN_PROFILE";
    private static final String URL_API_UN09_GET_USER_LOGGED_IN_PROFILE = "user-service/api/v1/users/profile?";
    private static final String KEY_UN7_GET_USER_LOGGED_IN_PROFILE = "KEY_UN07_GET_USER_LOGGED_IN_PROFILE";
    private static final String URL_API_UN7_GET_USER_LOGGED_IN_PROFILE = "user-service/api/v1/users/%s?";
    private static final String KEY_API_CP03_GET_CARE_PLAN_USER = "KEY_API_CP03_GET_CARE_PLAN_USER";
    private static final String URL_API_CP03_GET_CARE_PLAN_USER = "care-plan-service/api/v1/users/%s/care-plans?";
    private static final String KEY_API_CP01_GET_ALL_CARE_PLAN = "KEY_API_CP01_GET_ALL_CARE_PLAN";
    private static final String URL_API_CP01_GET_ALL_CARE_PLAN = "care-plan-service/api/v1/care-plans?";
    private static final String KEY_API_CP02_CREATE_CARE_PLAN = "KEY_API_CP02_CREATE_CARE_PLAN";
    private static final String URL_API_CP02_CREATE_CARE_PLAN = "care-plan-service/api/v1/users/%s/care-plans?";
    private static final String BODY_CP02 = "{\"careplanTemplateId\": %s}";
    private static final String DIALOG_TEMPLATE = "Dialoq Diabetes";
    private static final String USER_TERM = "TERM";
    private static final String P_API_KEY = "apikey";
    private static final String FAIL_TIMES_KEY = "fail_times";
    private static final String TAG = M040RecoveryPresenter.class.getName();

    private boolean mIsNoCarePlan;

    private String mUserName;
    private String mPassword;
    private transient RTokenEntity rTokenEntity;

    /**
     * The constructor of M038RecoveryPresenter
     *
     * @param event is the interface listen to this class callback
     */
    public M040RecoveryPresenter(OnM040RecoveryCallBack event) {
        super(event);
    }

    /**
     * Handle each successful request
     *
     * @see BasePresenter#handleSuccess(String, String)
     */
    @Override
    protected void handleSuccess(String data, String tag) {
        CommonUtil.wtfi(TAG, "DATA: " + data);
        switch (tag) {
            case KEY_API_IA04_RESET_PASSWORD_STEP_3:
                prepareIA04();
                break;
            case KEY_API_IA02_AUTHENTICATE_USER:
                prepareIA02(data);
                break;
            case KEY_UN09_GET_USER_LOGGED_IN_PROFILE:
                prepareUN09(data);
                break;
            case KEY_UN7_GET_USER_LOGGED_IN_PROFILE:
                prepareUN07(data);
                break;
            case KEY_API_CP03_GET_CARE_PLAN_USER:
                prepareCP03(data);
                break;
            case KEY_API_CP02_CREATE_CARE_PLAN:
                prepareCP02(data);
                break;
            case KEY_API_CP01_GET_ALL_CARE_PLAN:
                prepareCP01(data);
                break;
            default:
                break;
        }
    }

    /**
     * Prepare user's data after successfully reset password
     * Make call request to perform login action
     */
    private void prepareIA04() {
        CommonUtil.getInstance().savePrefContent(M023LoginPresenter.FAIL_TIMES_KEY, 0);
        callIA02AuthenticateUser(mUserName, mPassword);
    }

    private void prepareCP03(String data) {
        CommonUtil.wtfi(TAG, "prepareCP03..." + data);
        CarePlantUserEntity entity = generateData(CarePlantUserEntity.class, data);
        if (entity == null || entity.getData() == null || entity.getData().isEmpty()) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }

        if (rTokenEntity != null) {
            rTokenEntity.setDataCP03(data);
            CADBManager.getInstance().addRTokenEntity(rTokenEntity);
        }
        getStorage().setM001CarePlanUserEntity(entity);
        mListener.loginSuccess();
    }

    /**
     * Handle the data received after create user's care plan
     * Make call request to perform get user's care plan action
     *
     * @param data the response data received after a successful request
     */
    private void prepareCP02(String data) {
        CommonUtil.wtfi(TAG, "prepareCP02..." + data);
        CarePlanTemplateEntity entity = generateData(CarePlanTemplateEntity.class, data);
        if (entity == null) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            ProgressLoading.dismiss();
            return;
        }
        if (rTokenEntity != null) {
            rTokenEntity.setDataCP02(data);
        }
        getStorage().getM001CarePlanTemplateEntity().setId(entity.getId());
        callCP03GetCarePlanOfUser();
    }

    /**
     * Handle the data received after get all the care plan template
     * Make call request to perform create user's care plan action
     *
     * @param data the response data received after a successful request
     */
    void prepareCP01(String data) {
        CommonUtil.wtfi(TAG, "prepareCP01..." + data);
        CarePlanTemplateEntity entity = generateData(CarePlanTemplateEntity.class, data);
        if (entity == null) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            ProgressLoading.dismiss();
            return;
        }
        if (rTokenEntity != null) {
            rTokenEntity.setDataCP01(data);
        }
        getStorage().setM001CarePlanTemplateEntity(entity);
        callCP02CreateCarePlanForUser();
    }

    private void callCP02CreateCarePlanForUser() {
        CommonUtil.wtfi(TAG, "callCP02CreateCarePlanForUser");

        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_CP02_CREATE_CARE_PLAN);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(ERR_500));
            return;
        }
        CommonUtil.wtfi(TAG, LOG_TEXT_TOKEN + token);
        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_CP02_CREATE_CARE_PLAN, getStorage().getM007UserEntity().getUserId() + ""));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY_CP02, getCarePlanTemplateId()));
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    private String getCarePlanTemplateId() {
        if (getStorage().getM001CarePlanTemplateEntity() == null || getStorage().getM001CarePlanTemplateEntity().getData() == null
                || getStorage().getM001CarePlanTemplateEntity().getData().isEmpty()) return null;

        for (int i = 0; i < getStorage().getM001CarePlanTemplateEntity().getData().size(); i++) {
            CarePlanTemplateEntity.DataInfo careInfo = getStorage().getM001CarePlanTemplateEntity().getData().get(i);
            if (careInfo.getName().equals(DIALOG_TEMPLATE)) {
                return careInfo.getCareplanTemplateId() + "";
            }
        }
        return null;
    }

    /**
     * Handle the data received after successfully "get user's full profile"
     * After received the full version of user's profile, the app will start to check if the user
     * should be prevent form using app
     *
     * @param data the response data received after a successful request
     */
    private void prepareUN07(String data) {
        CommonUtil.wtfi(TAG, "prepareUN07..." + data);
        UserEntity entity = generateData(UserEntity.class, data);
        if (entity == null) {
            ProgressLoading.dismiss();
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        if (entity.getState() != null && entity.getState().equals(CommonUtil.INACTIVE)) {
            ProgressLoading.dismiss();
            mListener.showM001LandingScreen("Invalid Account");
            return;
        }
        if (rTokenEntity != null) {
            rTokenEntity.setDataUN07(data);
        }
        getStorage().setM007UserEntity(entity);
        CADBManager.getInstance().addRUserEntity(entity);
        checkBlacklistCountry(CommonUtil.IN_CASE_NOT_OFFLINE_MODE);
    }

    /**
     * Compare the user's country with the configuration setting to decide if the user should
     * be prevent from using app or not
     *
     * @param isOfflineMode is the condition to check if the phone is connect to the internet or not
     * @return true if user's country should be block or false if not
     */
    private boolean checkBlacklistCountry(boolean isOfflineMode) {
        ConfigEntity configSet = getStorage().getM001ConfigSet();
        String country = getStorage().getM007UserEntity().getCountry().toUpperCase();
        CommonUtil.wtfi("CountryUser", "CountryUser:" + country);
        CommonUtil.wtfi("BlacklistCountry", "Blacklist countries:" + getStorage().getM001ConfigSet().getBlacklistCountryEntity().get(0).getBlacklistCountry().getValue().toUpperCase());

        for (int i = 0; i < getStorage().getM001ConfigSet().getBlacklistCountryEntity().size(); i++) {
            String blacklistCountryConfigset = configSet.getBlacklistCountryEntity().get(i).getBlacklistCountry().getValue().toUpperCase();
            if (country.equals(blacklistCountryConfigset)) {
                mListener.showM001LandingFrg(CommonUtil.RECALL_COUNTRY);
                getStorage().setAppActive(false);
                ProgressLoading.dismiss();
                CommonUtil.wtfi(TAG, "This country login is in the black list");
                return false;
            }
        }
        setupUserCountry(getStorage().getM007UserEntity().getCountry());
        return checkCountryVersionLockApp(isOfflineMode);
    }

    private void setupUserCountry(String userCountry) {
        for (int i = 0; i < getStorage().getM001ConfigSet().getCountryEntity().size(); i++) {
            if (getStorage().getM001ConfigSet().getCountryEntity().get(i).getCountryCode().getValue().equals(userCountry)) {
                getStorage().setUserCountryEntity(getStorage().getM001ConfigSet().getCountryEntity().get(i));
                return;
            }
        }
    }

    /**
     * Compare the user's app version with the configuration setting to decide if the user should
     * be prevent from using app or not
     *
     * @param isOfflineMode is the condition to check if the phone is connect to the internet or not
     * @return true if user's app version should be block or false if not
     */
    private boolean checkCountryVersionLockApp(boolean isOfflineMode) {
        String country = getStorage().getM007UserEntity().getCountry().toUpperCase();
        ConfigEntity configSet = getStorage().getM001ConfigSet();
        String versionApp = "";
        try {
            PackageInfo pInfo = CAApplication.getInstance().getPackageManager().getPackageInfo(CAApplication.getInstance().getPackageName(), 0);
            versionApp = pInfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        CommonUtil.wtfi("BlockCountry", "Country:" + country);
        CommonUtil.wtfi("BlockCountry", "Blocked countries:" + getStorage().getM001ConfigSet().getBlackList().getCountries());
        if (getStorage().getM001ConfigSet().getBlackList().getCountries().contains(country)) {
            ProgressLoading.dismiss();
            mListener.showDialog(R.string.txt_m023_block_country, KEY_BLOCKED_BY_COUNTRY);
            return true;
        }

        for (int i = 0; i < getStorage().getM001ConfigSet().getCountryVersionEntity().size(); i++) {
            String countryConfigSet = configSet.getCountryVersionEntity().get(i).getCountryCode().getValue();
            String versionConfigSet = configSet.getCountryVersionEntity().get(i).getVersionName().getValue();
            String comparisonMode = configSet.getCountryVersionEntity().get(i).getVersionComparison().getValue();

            if (countryConfigSet.equals(getStorage().getUserCountryEntity().getCountryCode().getValue())) {
                switch (comparisonMode) {
                    case CommonUtil.RECALL_GREATER:
                        if (gotoLandingPage((versionApp.compareTo(versionConfigSet) > 0)))
                            return true;
                        break;
                    case CommonUtil.RECALL_EQUAL:
                        if (gotoLandingPage((versionApp.compareTo(versionConfigSet) == 0)))
                            return true;
                        break;
                    case CommonUtil.RECALL_LESS:
                        if (gotoLandingPage((versionApp.compareTo(versionConfigSet) < 0)))
                            return true;
                        break;
                    case CommonUtil.RECALL_GREATER_OR_EQUAL:
                        if (gotoLandingPage((versionApp.compareTo(versionConfigSet) >= 0)))
                            return true;
                        break;
                    case CommonUtil.RECALL_LESS_OR_EQUAL:
                        if (gotoLandingPage((versionApp.compareTo(versionConfigSet) <= 0)))
                            return true;
                        break;
                    default:
                        break;
                }
            }
        }

        CommonUtil.wtfi("M023Presenter", "Starting callCP03");
        return checkForAcceptedTerm(isOfflineMode);
    }

    /**
     * Check for the input condition to decide the app should redirect the user to the Landing screen
     *
     * @param isBack is the result of the checking comparison.
     * @return true the app will redirect the user to the Landing screen and false if not.
     */
    private boolean gotoLandingPage(boolean isBack) {
        if (!isBack) return false;
        mListener.showM001LandingFrg(CommonUtil.RECALL_COUNTRY_VERSION);
        getStorage().setAppActive(false);
        CommonUtil.wtfi(TAG, "Check for recall country and version");
        ProgressLoading.dismiss();
        return true;
    }

    /**
     * Checking if the user has accepted with the Term and Condition
     *
     * @param isOfflineMode is to decide if the app is in offline mode or not
     * @return true if the user has accepted with the Term and Condition
     * or false if the user has not
     */
    private boolean checkForAcceptedTerm(boolean isOfflineMode) {
        if (!getStorage().getM007UserEntity().getFamilyHistory().contains(USER_TERM)) {
            ProgressLoading.dismiss();
            mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(ERR_500));
            return true;
        }

        if (CommonUtil.getInstance().isConnectToNetwork() && isOfflineMode == CommonUtil.IN_CASE_NOT_OFFLINE_MODE) {
            callCP03GetCarePlanOfUser();
        }
        return false;
    }

    /**
     * Make a request to the backend service to perform get user's care plan action
     */
    private void callCP03GetCarePlanOfUser() {
        CommonUtil.wtfi(TAG, "callCP03GetAllCarePlanTemplete");

        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(KEY_API_CP03_GET_CARE_PLAN_USER);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, LOG_TEXT_TOKEN + token);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_CP03_GET_CARE_PLAN_USER, getStorage().getM007UserEntity().getUserId()));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Make a request to the backend service to perform get user's profile action
     */
    private void callIUN07GetUserLoggedInProfile() {
        CommonUtil.wtfi(TAG, "callIUN07GetUserLoggedInProfile");

        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(KEY_UN7_GET_USER_LOGGED_IN_PROFILE);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            CommonUtil.wtfi(TAG, LOG_TEXT_TOKEN + token);
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, LOG_TEXT_TOKEN + token);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_UN7_GET_USER_LOGGED_IN_PROFILE,
                getStorage().getM007UserEntity().getUserId()));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Make a request to the backend service to perform login action with the email and password
     * that the user has provided
     *
     * @param userName is the email that the user has inputted or pre - filed by previous login
     * @param pass     is the password which has been successfully retrieved after reset password
     */
    void callIA02AuthenticateUser(String userName, String pass) {
        rTokenEntity = new RTokenEntity();
        rTokenEntity.setUserName(userName);
        rTokenEntity.setPassword(pass);

        CommonUtil.wtfi(TAG, "callIA02AuthenticateUser");

        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_IA02_AUTHENTICATE_USER);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addPathSegment(URL_API_IA02_AUTHENTICATE_USER);

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());

        request.addBody(generateJson(BODY_IA02, userName, pass));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Handle the data received after successfully authenticate with the provided email and password
     * Make call request to perform get user's profile action
     *
     * @param data the response data received after a successful request
     */
    private void prepareIA02(String data) {
        if (rTokenEntity != null) {
            rTokenEntity.setDataIA02(data);
            rTokenEntity.setConfigSet(getStorage().getConfigSetData());
        }

        CommonUtil.getInstance().savePrefContent(FAIL_TIMES_KEY, 0);
        CommonUtil.wtfi(TAG, "prepareIA02..." + data);
        TokenEntity tokenEntity = generateData(TokenEntity.class, data);
        if (tokenEntity == null) {
            ProgressLoading.dismiss();
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }

        CommonUtil.getInstance().savePrefContent(CommonUtil.TOKEN_KEY, tokenEntity.getToken());
        CommonUtil.getInstance().savePrefContent(CommonUtil.USER_NAME, mUserName);
        callIUN09GetUserLoggedInProfile();
    }

    /**
     * Make a request to the backend service to perform get all care plan template action
     */
    private void callCP01GetAllCarePlanTemplete() {
        CommonUtil.wtfi(TAG, "callCP01GetAllCarePlanTemplate");

        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(KEY_API_CP01_GET_ALL_CARE_PLAN);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, LOG_TEXT_TOKEN + token);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(URL_API_CP01_GET_ALL_CARE_PLAN);

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Make a request to the backend service to perform get user profile action
     */
    private void callIUN09GetUserLoggedInProfile() {
        CommonUtil.wtfi(TAG, "callIUN09GetUserLoggedInProfile");

        ProgressLoading.dontShow();
        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(KEY_UN09_GET_USER_LOGGED_IN_PROFILE);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, LOG_TEXT_TOKEN + token);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(URL_API_UN09_GET_USER_LOGGED_IN_PROFILE);

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Handle the data received after successfully get user's profile
     * Make call request to perform "get user's full profile" action
     *
     * @param data the response data received after a successful request
     */
    private void prepareUN09(String data) {
        CommonUtil.wtfi(TAG, "prepareUN09..." + data);
        if (rTokenEntity != null) {
            rTokenEntity.setDataUN09(data);
        }
        UserEntity entity = generateData(UserEntity.class, data);
        if (entity == null) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        getStorage().setM007UserEntity(entity);
        callIUN07GetUserLoggedInProfile();
    }

    /**
     * Handle each unsuccessful request
     *
     * @see BasePresenter#doFailed(String, Exception, int, String)
     */
    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tagRequest);

        if (code == 204) {
            mIsNoCarePlan = true;
            if (KEY_API_CP03_GET_CARE_PLAN_USER.equals(tag)) {
                callCP01GetAllCarePlanTemplete();
            }
            return;
        }

        ProgressLoading.dismiss();
        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        String sms;

        if (responseEntity != null) {
            sms = responseEntity.getErrorEntity()[0].getMessage();
            switch (tag) {
                case KEY_API_CP01_GET_ALL_CARE_PLAN:
                case KEY_API_CP02_CREATE_CARE_PLAN:
                    mListener.showM001LandingScreen(sms);
                    break;
                case KEY_API_IA02_AUTHENTICATE_USER:
                    switch (responseEntity.getErrorEntity()[0].getCode()) {
                        case CommonUtil.ERROR_401:
                            int failTimes = CommonUtil.getInstance().getIntPrefContent(FAIL_TIMES_KEY, 0);
                            mListener.wrongAccount(CommonUtil.ERROR_401);
                            CommonUtil.getInstance().savePrefContent(FAIL_TIMES_KEY, failTimes + 1);
                            break;
                        case CommonUtil.ERROR_ERR_401:
                            mListener.wrongAccount(CommonUtil.ERROR_ERR_401);
                            break;
                        default:
                            mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(ERR_500));
                            break;
                    }
                    break;
                case KEY_UN09_GET_USER_LOGGED_IN_PROFILE:
                case KEY_API_IA04_RESET_PASSWORD_STEP_3:
                    mListener.onIA04Fail(LangMgr.getInstance().getLangList().get(ERR_500));
                    break;
                default:
                    break;
            }
        } else if (!CommonUtil.getInstance().isConnectToNetwork()) {
            mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(ERR_NO_CONNECTION));
        }
    }

    /**
     * Make a request to the backend service to perform reset password action
     *
     * @param userName is the email that the user has provided
     * @param password is the new password that user want to set
     * @param token    is the confirmation code which was sent to the user's email
     */
    public void callIA04ResetPasswordStep3(String userName, String token, String password) {
        CommonUtil.wtfi(TAG, "callIA04ResetPasswordStep3");

        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_IA04_RESET_PASSWORD_STEP_3);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addPathSegment(URL_API_IA04_RESET_PASSWORD_STEP_3);

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        mUserName = userName;
        mPassword = password;
        request.addBody(generateJson(BODY_IA04, userName, token, password));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Keep the progress bar while calling API
     *
     * @param key is the tag key of each request
     */
    @Override
    public void hideLockDialog(String key) {
        if (KEY_API_CP03_GET_CARE_PLAN_USER.equals(key)) {
            if (mIsNoCarePlan) return;
        }
    }

}
