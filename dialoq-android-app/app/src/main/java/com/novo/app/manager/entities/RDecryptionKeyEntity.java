package com.novo.app.manager.entities;

import io.realm.RealmObject;

public class RDecryptionKeyEntity extends RealmObject {
    private String systemId;
    private String decryptionKey;

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public String getDecryptionKey() {
        return decryptionKey;
    }

    public void setDecryptionKey(String decryptionKey) {
        this.decryptionKey = decryptionKey;
    }
}
