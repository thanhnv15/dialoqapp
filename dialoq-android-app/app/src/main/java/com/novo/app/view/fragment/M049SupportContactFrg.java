package com.novo.app.view.fragment;

import android.content.Intent;
import android.net.Uri;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M049SupportContactPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM049SupportContactCallBack;

import java.util.ArrayList;
import java.util.List;

import static com.novo.app.view.fragment.M027FAQFrg.*;

public class M049SupportContactFrg extends BaseFragment<M049SupportContactPresenter, OnM024DoseLogCallBack> implements OnM049SupportContactCallBack {
    public static final String TAG = M049SupportContactFrg.class.getName();
    private String troubleshooting, instructions;
    private TextView mTvPhoneNumber;

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m049_back, this);
        findViewById(R.id.tv_m049_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.iv_m049_communication);
        findViewById(R.id.iv_m049_phone);
        mTvPhoneNumber = findViewById(R.id.tv_m049_phone_number, this, CAApplication.getInstance().getRegularFont());

        TextView mTvDescription = findViewById(R.id.tv_m049_description, this, CAApplication.getInstance().getRegularFont());
        List<String> highlight = new ArrayList<>();

        troubleshooting = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m049_support_contact_troubleshooting));
        instructions = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m049_support_contact_instructions));
        highlight.add(troubleshooting);
        highlight.add(instructions);
        setClickableText(mTvDescription, highlight, R.color.colorM020Teal);
    }

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m049_support_contact;
    }

    @Override
    protected M049SupportContactPresenter getPresenter() {
        return new M049SupportContactPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        if (getTagSource().equals(M027FAQFrg.TAG)){
            CommonUtil.getInstance().defineBackTag(getTagSource(), TAG);
            return;
        }
        if (getTagSource().equals(M050TroubleshootingSingleTopicFrg.TAG)){
            CommonUtil.getInstance().defineBackTag(getTagSource(), TAG);
            return;
        }
        if (getTagSource().equals(M026SupportFrg.TAG)){
            CommonUtil.getInstance().defineBackTag(getTagSource(), TAG);
        }
    }

    @Override
    public void backToPreviousScreen() {
        String backTag = CommonUtil.getInstance().doBackFlowWithBackTag(TAG);
        if (backTag.equals(M027FAQFrg.TAG)) {
            mCallBack.showChildFrgScreen(TAG, M027FAQFrg.TAG);
        } else if (backTag.equals((M050TroubleshootingSingleTopicFrg.TAG))) {
            mCallBack.showChildFrgScreen(TAG, M050TroubleshootingSingleTopicFrg.TAG);
        } else if (backTag.equals((M026SupportFrg.TAG))) {
            mCallBack.showChildFrgScreen(TAG, M026SupportFrg.TAG);
        }
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m049_back:
                if (getStorage().getPreTAG() != null && getStorage().getPreTAG().equals(M070PairTroubleFrg.TAG)) {
                    mCallBack.showM070PairTroubleScreen();
                    return;
                }
                backToPreviousScreen();
                break;
            case R.id.tv_m049_description:
                onLinkClicked();
                break;
            case R.id.tv_m049_phone_number:
                startPhoneCall(mTvPhoneNumber.getText().toString());
                break;
            default:
                break;
        }
    }

    private void startPhoneCall(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+number));
        startActivity(intent);
    }

    @Override
    public void onLinkClicked() {
        if (getStorage().getLinkTag() != null && getStorage().getLinkTag().equals(troubleshooting)) {
            mCallBack.showChildFrgScreen(TAG, M047SupportTroubleshootingFrg.TAG);
        }
        if (getStorage().getLinkTag() != null && getStorage().getLinkTag().equals(instructions)) {
            mCallBack.showChildFrgScreen(TAG, M071SupportInstructionFrg.TAG);
        }
        getStorage().setLinkTag(null);
    }
}
