/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM080FirstLoginDidntGetCallBack;
import com.novo.app.view.widget.ProgressLoading;

public class M080FirstLoginDidntGetPresenter extends BasePresenter<OnM080FirstLoginDidntGetCallBack> {
    private static final String KEY_API_VERIFY_EMAIL = "KEY_API_VERIFY_EMAIL";
    private static final String URL_API_IA04_RESET_PASSWORD_STEP_1 = "user-service/api/v1/reset-password?";
    private static final String BODY_IA04 = "{\"action\": \"initiateReset\",\"userName\": %s,\"delivery\": \"SMTP\"}";
    private static final String TAG = M080FirstLoginDidntGetPresenter.class.getName();

    /**
     * The constructor of M080FirstLoginDidntGetPresenter
     *
     * @param event is the interface listen to this class callback
     */
    public M080FirstLoginDidntGetPresenter(OnM080FirstLoginDidntGetCallBack event) {
        super(event);
    }

    /**
     * Make a request to the backend service to send a confirmation email to the email that the user
     * has inputted
     */
    public void requestCodeIA04() {
        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_VERIFY_EMAIL);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addPathSegment(URL_API_IA04_RESET_PASSWORD_STEP_1);

        request.addHeaders(CommonUtil.P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY_IA04, getStorage().getM075UserLoginInfo().getKey()));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Handle successful request response
     * If any error while parsing the response data, request View instance to show an error message
     * If the successfully parsing data, request View instance to proceed to the next screen by making
     * callback {@link OnM080FirstLoginDidntGetCallBack#sendCodeSuccess()}
     *
     * @see BasePresenter#handleSuccess(String, String)
     */
    @Override
    protected void handleSuccess(String data, String tag) {
        if (KEY_API_VERIFY_EMAIL.equals(tag)) {
            CommonUtil.wtfi(TAG, "DATA: " + data);
            ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
            if (responseEntity == null) {
                mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(ERR_500));
                mListener.showM077FirstLoginCode();
                return;
            }

            //For abnormal case due to server error
            mListener.sendCodeSuccess();
        }
    }

    /**
     * Handle each unsuccessful request
     * If the received error code is 400, request the View instance to show error
     * by making callback {@link OnM080FirstLoginDidntGetCallBack#sendCodeFailed(String)}
     * If encounter other errors, make a callback to the View instance to display error message
     *
     * @see BasePresenter#doFailed(String, Exception, int, String)
     */
    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tagRequest);
        ProgressLoading.dismiss();

        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        if (responseEntity != null && tag.equals(KEY_API_VERIFY_EMAIL)) {
            String sms = responseEntity.getErrorEntity()[0].getMessage();
            String responseCode = Integer.toString(code);
            switch (responseCode) {
                case CommonUtil.ERROR_400:
                    mListener.sendCodeFailed(sms);
                    break;
                case CommonUtil.ERROR_401:
                case CommonUtil.ERROR_403:
                default:
                    showNotify(LangMgr.getInstance().getLangList().get(ERR_500));
                    break;
            }
        } else if (!CommonUtil.getInstance().isConnectToNetwork()) {
            showNotify(LangMgr.getInstance().getLangList().get(ERR_NO_CONNECTION));
        }
    }

}
