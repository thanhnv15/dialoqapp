package com.novo.app.model.entities;

/**
 * Created by KieuMinhDuc on 3/28/2019.
 */
public class DiabeteEntity {
    private TextEntity entity;
    private boolean isSelected;

    public DiabeteEntity(TextEntity entity) {
        this.entity = entity;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public TextEntity getEntity() {
        return entity;
    }

    public void setEntity(TextEntity entity) {
        this.entity = entity;
    }
}
