package com.novo.app.view.fragment;

import android.content.res.AssetFileDescriptor;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M023LoginPresenter;
import com.novo.app.utils.CATask;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM023LoginCallBack;
import com.novo.app.view.event.OnM028PairVideoCallBack;
import com.novo.app.view.widget.ProgressLoading;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ThanhNv on 14/12/2017.
 */
public class M028PairVideoFrg extends BaseFragment<M023LoginPresenter, OnHomeBackToView> implements OnM028PairVideoCallBack, TextureView.SurfaceTextureListener, MediaPlayer.OnPreparedListener, View.OnClickListener, View.OnTouchListener, MediaPlayer.OnCompletionListener, SeekBar.OnSeekBarChangeListener, CATask.OnTaskCallBackToView, OnCallBackToView, MediaPlayer.OnVideoSizeChangedListener, OnM023LoginCallBack {
    public static final String TAG = M028PairVideoFrg.class.getName();

    private static final int PLAY_STATE = 0;
    private static final int STOP_STATE = 1;
    private static final long TIME_HIDE = 3000;
    private static final long TIME_DELAY = 500;
    private static final String KEY_CURRENT_TIME = "KEY_CURRENT_TIME";

    private static final int IDLE = 0;
    private static final int PAUSED = 1;
    private static final int PLAYED = 2;

    private static final String VIDEO_INTRO_PATH = "video/IntroVideo.mp4";
    private static final String VIDEO_INTRO_TITLE = "Introduce about DoseTracker";

    private MediaPlayer mPlayer;

    private ImageView ivPlay;
    private TextView tvDuration, tvStart;
    private SeekBar seekBar;
    private TextureView mTextureView;
    private View mTrPlayer;
    private Handler mHandler = new Handler();
    private CATask<M028PairVideoFrg, M028PairVideoFrg> mTimer;
    private int mState;

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mTrPlayer.setVisibility(View.GONE);
        }
    };

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m028_logo);
        findViewById(R.id.tv_m028_skip, this, CAApplication.getInstance().getMediumFont());

        mTextureView = findViewById(R.id.texture_m028_video);
        mTextureView.setOnTouchListener(this);

        ivPlay = findViewById(R.id.iv_m028_play);
        tvStart = findViewById(R.id.tv_m028_start);
        mTrPlayer = findViewById(R.id.tr_m028_player);

        ivPlay.setOnClickListener(this);
        tvDuration = findViewById(R.id.tv_m028_duration);
        seekBar = findViewById(R.id.seek_bar_m028);
        seekBar.setOnSeekBarChangeListener(this);
        mTextureView.setSurfaceTextureListener(this);
        mState = IDLE;

        getStorage().setM028VideoData(new String[]{VIDEO_INTRO_PATH, VIDEO_INTRO_TITLE});
    }

    @Override
    public int getLayoutId() {
        return R.layout.frg_m028_pair_video;
    }

    @Override
    protected String getTAG() {
        return null;
    }

    @Override
    protected M023LoginPresenter getPresenter() {
        return new M023LoginPresenter(this);
    }


    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        CommonUtil.wtfi(TAG, "onPrepared....");
        String time = CommonUtil.getInstance().getPrefContent(KEY_CURRENT_TIME);
        if (time != null) {
            try {
                int timeNow = Integer.parseInt(time);
                CommonUtil.getInstance().clearPrefContent(KEY_CURRENT_TIME);
                mPlayer.seekTo(timeNow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        playVideo();

        mTimer = new CATask<>(new WeakReference<>(this), this, TIME_DELAY);
        mTimer.setLoop(true);
        seekBar.setMax(mPlayer.getDuration());
        findViewById(R.id.tr_m028_player).setVisibility(View.VISIBLE);

        mTimer.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        new Handler().postDelayed(() -> findViewById(R.id.v_bg_white).setVisibility(View.GONE), 1000);

        mHandler.postDelayed(mRunnable, TIME_HIDE);
        tvDuration.setText(getDuration(mPlayer.getDuration()));
        ProgressLoading.dismiss();
    }

    private String getDuration(int time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss");
        return dateFormat.format(new Date(time));
    }

    @Override
    public void onStart() {
        mTextureView.setVisibility(View.VISIBLE);
        if (mState == PAUSED) {
            playVideo();
        }
        super.onStart();
    }

    @Override
    protected void defineBackKey() {
        // This feature isn't require here
    }

    @Override
    public void onStop() {
        if (mState == PLAYED) {
            pauseVideo();
        }
        super.onStop();
    }

    public void stopVideo() {
        if (mPlayer != null) {
            mPlayer.reset();
            mPlayer.release();
            mPlayer = null;
            mTimer.cancel();
            mState = IDLE;
        }
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m028_play:
                if (mPlayer != null && mPlayer.isPlaying()) {
                    pauseVideo();
                } else if (mPlayer != null) {
                    playVideo();
                }
                break;
            case R.id.tv_m028_skip:
                stopVideo();
                if (CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME) != null && CommonUtil.getInstance().getPrefContent(CommonUtil.PASS) != null){
                    mPresenter.callIA02AuthenticateUser(CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME), CommonUtil.getInstance().getPrefContent(CommonUtil.PASS));
                } else {
                    mPresenter.callIA02AuthenticateUser(getStorage().getM001ProfileEntity().getEmail(), getStorage().getM001ProfileEntity().getPassword());
                }
                break;
            default:
                break;
        }
    }


    @Override
    public void backToPreviousScreen() {
        // do nothing
    }

    public void pauseVideo() {
        if (mPlayer != null) {
            mPlayer.pause();
            mState = PAUSED;
            ivPlay.setImageLevel(PLAY_STATE);
        }
    }

    private void playVideo() {
        CommonUtil.wtfi(TAG, "playVideo....");
        mPlayer.start();
        mState = PLAYED;
        ivPlay.setImageLevel(STOP_STATE);
    }

    private void initVideo(Surface s) {
        CommonUtil.wtfi(TAG, "initVideo....");
        //get video info
        if (getStorage() == null) return;
        if (getStorage().getM028VideoData() == null) return;
        String videoPath = getStorage().getM028VideoData()[0];
        try (AssetFileDescriptor afd = CAApplication.getInstance().getAssets().openFd(videoPath)) {

            if (mPlayer != null) {
                mPlayer.stop();
                mPlayer.reset();
            }

            mPlayer = new MediaPlayer();
            mPlayer.setOnPreparedListener(this);
            mPlayer.setOnCompletionListener(this);
            mPlayer.setOnVideoSizeChangedListener(this);
            mPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mPlayer.setSurface(s);
            mPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
            ProgressLoading.dismiss();
        }
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (mTrPlayer.getVisibility() == View.GONE) {
                    mTrPlayer.setVisibility(View.VISIBLE);
                    mHandler.removeCallbacks(mRunnable);
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mTrPlayer.getVisibility() == View.VISIBLE) {
                    mHandler.postDelayed(mRunnable, TIME_HIDE);
                }
                break;
        }
        return true;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        ivPlay.setImageLevel(PLAY_STATE);
        if (CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME) != null && CommonUtil.getInstance().getPrefContent(CommonUtil.PASS) != null){
            mPresenter.callIA02AuthenticateUser(CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME), CommonUtil.getInstance().getPrefContent(CommonUtil.PASS));
        } else {
            mPresenter.callIA02AuthenticateUser(getStorage().getM001ProfileEntity().getEmail(), getStorage().getM001ProfileEntity().getPassword());
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        // This feature isn't require here
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // This feature isn't require here
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (mPlayer != null) {
            mPlayer.seekTo(seekBar.getProgress());
            playVideo();
        }
    }

    @Override
    public Object doBGTask(String key) {
        try {
            Thread.sleep(TIME_DELAY);
            if (mPlayer == null) {
                mTimer.cancel();
                return false;
            }
            if (mPlayer.isPlaying()) {
                mTimer.updateUI(mPlayer.getCurrentPosition(), mPlayer.getDuration());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void updateUITask(Object[] values) {
        tvStart.setText(getDuration((int) values[0]));
        seekBar.setProgress((int) values[0]);
    }

    @Override
    public void doneTask(Object data) {
        // This feature isn't require here
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        ProgressLoading.show(mContext);
        initVideo(new Surface(surface));
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        // This feature isn't require here
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        // This feature isn't require here
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
        CommonUtil.wtfi(TAG, "WIDTH: " + width);
        CommonUtil.wtfi(TAG, "HEIGHT: " + height);
        int wSurface = mTextureView.getWidth();
        int hSurface = mTextureView.getHeight();

        float boxWidth = wSurface;
        float boxHeight = hSurface;
        float wr = boxWidth / (float) width;
        float hr = boxHeight / (float) height;
        float ar = (float) width / (float) height;

        if (wr > hr) {
            wSurface = (int) (boxHeight * ar);
        } else {
            hSurface = (int) (boxWidth / ar);
        }
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(wSurface, hSurface);
        params.gravity = Gravity.CENTER;
        mTextureView.setLayoutParams(params);
    }

    @Override
    public void showM001LandingScreen(String sms) {
        // This feature isn't require here
    }

    @Override
    public void loginSuccess() {
        mCallBack.showFrgScreen(TAG, M024DoseLogFrg.TAG);
        mCallBack.startScan();
        CAApplication.getInstance().startSyncService();
    }

    @Override
    public void updateFingerPrintResult() {
        // This feature isn't require here
    }

    @Override
    public void showDialog(int txt, String tag) {
        // This feature isn't require here
    }

    @Override
    public void showDialog(String txt, String tag) {
        // This feature isn't require here
    }
}


