package com.novo.app.view.fragment;

import android.widget.RadioButton;
import android.widget.Switch;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M068SharePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM068ShareCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.widget.ProgressLoading;

public class M068ShareChooseFrg extends BaseFragment<M068SharePresenter, OnM024DoseLogCallBack> implements OnM068ShareCallBack {
    public static final String TAG = M068ShareChooseFrg.class.getName();
    public static final int KEY_LAST_7_DAYS = 0;
    public static final int KEY_LAST_14_DAYS = 1;
    public static final int KEY_LAST_28_DAYS = 2;
    private RadioButton mRb7Day;
    private RadioButton mRb14Day;
    private RadioButton mRb28Day;

    private Switch mSwSharingNote;

    @Override
    protected M068SharePresenter getPresenter() {
        return new M068SharePresenter(this);
    }

    @Override
    protected void initViews() {
        mCallBack.changeTab(CommonUtil.TAB_MENU_SHARE);
        findViewById(R.id.tv_m068_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m068_description, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m068_date_range, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m068_7_days, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m068_14_days, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m068_28_days, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m068_note, CAApplication.getInstance().getRegularFont());

        mRb7Day = findViewById(R.id.rb_m068_7_days, this);
        mRb14Day = findViewById(R.id.rb_m068_14_days, this);
        mRb28Day = findViewById(R.id.rb_m068_28_days, this);

        findViewById(R.id.bt_m068_next, this, CAApplication.getInstance().getMediumFont());
        mSwSharingNote = findViewById(R.id.sw_m068_switch, this);
    }

    @Override
    protected void initData() {
        getStorage().setM068TypeDate(KEY_LAST_28_DAYS);
    }

    @Override
    public int getLayoutId() {
        return R.layout.frg_m068_share_choose;
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.rb_m068_7_days:
            case R.id.tv_m068_7_days:
                mRb14Day.setChecked(false);
                mRb28Day.setChecked(false);
                getStorage().setM068TypeDate(KEY_LAST_7_DAYS);
                break;
            case R.id.rb_m068_14_days:
            case R.id.tv_m068_14_days:
                mRb7Day.setChecked(false);
                mRb28Day.setChecked(false);
                getStorage().setM068TypeDate(KEY_LAST_14_DAYS);
                break;
            case R.id.rb_m068_28_days:
            case R.id.tv_m068_28_days:
                mRb7Day.setChecked(false);
                mRb14Day.setChecked(false);
                getStorage().setM068TypeDate(KEY_LAST_28_DAYS);
                break;
            case R.id.bt_m068_next:
                CommonUtil.getInstance().showDialog(mContext, "Data Privacy Disclaimer", "Please be aware - you are solely responsible for sharing data.", "OK", "Cancel", new OnOKDialogCallBack() {
                    @Override
                    public void handleOKButton1() {
                        mCallBack.showGeneratingProgress();
                        mPresenter.initCondition(getStorage().getM068TypeDate(), mSwSharingNote.isChecked());
                    }
                });
                break;
        }
    }

    @Override
    public void dataAS05Ready() {
        mCallBack.showChildFrgScreen(TAG, M069SharePreviewFrg.TAG);

    }

    @Override
    public void generateFailed() {
        mCallBack.dismissGeneratingProgress();
    }

    @Override
    public void showLockDialog() {
        ProgressLoading.dontShow();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallBack.dismissGeneratingProgress();
    }
}
