package com.novo.app.presenter;

import com.novo.app.view.event.OnM001LandingPageCallBack;

public class M001LandingPagePresenter extends BasePresenter<OnM001LandingPageCallBack> {

    public M001LandingPagePresenter(OnM001LandingPageCallBack event) {
        super(event);
    }

}
