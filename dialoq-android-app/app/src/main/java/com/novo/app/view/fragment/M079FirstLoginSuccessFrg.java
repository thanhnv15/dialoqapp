/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.view.fragment;

import android.os.Handler;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M079FirstLoginSuccessPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM079FirstLoginCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

import java.util.Objects;

/**
 * First Login Success Fragment
 * <p>
 * This fragment is 'View' object in MVP pattern
 * This fragment uses an {@link M079FirstLoginSuccessPresenter} as linked Presenter
 * Callbacks to this fragment will be defined by {@link OnM079FirstLoginCallBack} interface
 * <p>
 * This fragment is used in Reset password features of dialog app, where user has to input
 * email to receive confirmation code
 */
public class M079FirstLoginSuccessFrg extends BaseFragment<M079FirstLoginSuccessPresenter, OnHomeBackToView> implements OnM079FirstLoginCallBack {
    public static final String TAG = M079FirstLoginSuccessFrg.class.getName();
    private static final long DELAY_TIME = 2000;

    /**
     * Initiate views component for First Login Success Fragment
     *
     * @see BaseFragment#initViews()
     */
    @Override
    protected void initViews() {
        findViewById(R.id.tv_m079_description, CAApplication.getInstance().getBoldFont());
        getStorage().getM001ProfileEntity().setVerifyCode(null);
        CommonUtil.getInstance().savePrefContent(CommonUtil.IS_LOGIN, CommonUtil.IS_LOGIN_TRUE);
        CommonUtil.getInstance().savePrefContent(CommonUtil.USER_NAME, Objects.requireNonNull(getStorage().getM075UserLoginInfo().getKey()));
        CommonUtil.getInstance().savePrefContent(CommonUtil.PASS, Objects.requireNonNull(getStorage().getM075UserLoginInfo().getValue()));
        new Handler().postDelayed(() -> mCallBack.showFrgScreen(TAG, M024DoseLogFrg.TAG), DELAY_TIME);
    }

    /**
     * Get layout id for First Login Success Fragment
     *
     * @return layout id
     * @see BaseFragment#getLayoutId()
     */
    @Override
    protected int getLayoutId() {
        return R.layout.frg_m079_first_login_success;
    }

    /**
     * Get presenter for this fragment
     *
     * @return {@link M079FirstLoginSuccessPresenter} instance
     * @see BaseFragment#getPresenter()
     */
    @Override
    protected M079FirstLoginSuccessPresenter getPresenter() {
        return new M079FirstLoginSuccessPresenter(this);
    }

    /**
     * Handle callback from {@link M079FirstLoginSuccessPresenter}
     * {@link OnHomeBackToView#showM001LandingScreen(String err)}
     *
     * @param message error string description
     */
    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    /**
     * @return tag in {@link String}
     * @see BaseFragment#getTAG()
     */
    @Override
    protected String getTAG() {
        return TAG;
    }

    /**
     * @see BaseFragment#defineBackKey()
     */
    @Override
    protected void defineBackKey() {
        //Wait for customer confirmation
    }
}
