package com.novo.app.presenter;

import com.novo.app.view.event.OnM010SignUpCallBack;

public class M010SignUpPresenter extends BasePresenter<OnM010SignUpCallBack> {

    public M010SignUpPresenter(OnM010SignUpCallBack event) {
        super(event);
    }

}
