/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Build;

import com.novo.app.manager.CADBManager;
import com.novo.app.manager.Migration;
import com.novo.app.service.CARequest;
import com.novo.app.service.SyncScheduleService;
import com.novo.app.service.WebServiceAdapter;
import com.novo.app.service.WebServiceUtil;
import com.novo.app.utils.CommonUtil;
import com.novo.app.utils.NetworkChangeReceiver;
import com.novo.app.utils.StorageCommon;
import com.novo.app.view.event.OnWebServiceUtilCallBack;

import java.lang.ref.WeakReference;
import java.security.SecureRandom;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public final class CAApplication extends Application {
    private static final String TAG = CAApplication.class.getName();
    private static final String KEY_ENCRYPT_REALM = "KEY_ENCRYPT_REALM";
    private static CAApplication instance;
    private WebServiceAdapter webServiceAdapter;
    private StorageCommon storageCommon;
    private Typeface mBoldFont;
    private Typeface mRegularFont;
    private Typeface mMediumFont;
    private Typeface mHeavyFont;
    private Typeface mLightFont;
    private Realm realm;
    private BroadcastReceiver mNetworkChangeReceiver;

    @SuppressWarnings("squid:S3010")
    public CAApplication() {
        if (instance == null) {
            instance = this;
        }
    }

    public static CAApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        CommonUtil.wtfd(TAG, "onCreate...");
        if (!CommonUtil.getInstance().isUnitTest()) {
            Realm.init(this);
            RealmConfiguration config = new RealmConfiguration.Builder()
                    //.encryptionKey(getSavedKeys())
                    .schemaVersion(CADBManager.VERSION_DB).migration(new Migration())
                    .build();

            realm = Realm.getInstance(config);
        }
        mNetworkChangeReceiver = new NetworkChangeReceiver();
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(mNetworkChangeReceiver, intentFilter);

        webServiceAdapter = new WebServiceAdapter();
        storageCommon = new StorageCommon(new WeakReference<>(this));
        mBoldFont = Typeface.createFromAsset(getAssets(), "font/Roboto-Bold.ttf");
        mRegularFont = Typeface.createFromAsset(getAssets(), "font/Roboto-Regular.ttf");
        mLightFont = Typeface.createFromAsset(getAssets(), "font/Roboto-Light.ttf");
        mMediumFont = Typeface.createFromAsset(getAssets(), "font/Roboto-Medium.ttf");
        mHeavyFont = Typeface.createFromAsset(getAssets(), "font/Roboto-Medium.ttf");
    }

    public void startSyncService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(new Intent(this, SyncScheduleService.class));
        } else {
            startService(new Intent(this, SyncScheduleService.class));
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unregisterReceiver(mNetworkChangeReceiver);
    }

    private byte[] getSavedKeys() {
        String keys = CommonUtil.getInstance().getPrefContent(KEY_ENCRYPT_REALM);
        CommonUtil.wtfi(TAG, "getSavedKeys...keys: " + keys);
        byte[] key = new byte[64];
        if (keys == null) {
            new SecureRandom().nextBytes(key);
            keys = "";
            for (int i = 0; i < key.length - 1; i++) {
                keys += key[i] + "_o_";
            }

            keys += key[key.length - 1];
            CommonUtil.wtfi(TAG, "getSavedKeys...saved keys: " + keys);
            CommonUtil.getInstance().savePrefContent(KEY_ENCRYPT_REALM, keys);
        } else {
            String[] sKeys = keys.split("_o_");
            for (int i = 0; i < sKeys.length; i++) {
                key[i] = Byte.parseByte(sKeys[i]);
            }
        }


        return key;
    }

    /**
     * <b>callRequest</b><br/>
     * <br/>1. Perform calling a request API to server
     * <br/>2. Put a request to Queue for "KeepMeLogin" feature
     * @param request CARequest
     * @param event WebServiceAdapter.OnRequestCallBackListener 
     * @see WebServiceUtil#callRequest(OnWebServiceUtilCallBack, CARequest)
     */
    public void callRequest(CARequest request, WebServiceAdapter.OnRequestCallBackListener event) {
        boolean isNewRequest = false;
        if (request.getRequestID() == null || request.getRequestID().equals("") || getStorageCommon().getQueueRequest(request.getRequestID())[0] == null) {
            isNewRequest = true;
            request.addRequestID(System.currentTimeMillis() + "");
        }
        request.setToken(CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY) != null ? CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY) : "null");
        webServiceAdapter.put(request.getKey(), event);
        WebServiceUtil webServiceUtil = new WebServiceUtil();
        webServiceUtil.setOnExecuteListener(CAApplication.getInstance().getWebServiceAdapter());
        webServiceUtil.callRequest(null, request);
        if (isNewRequest) {
            getStorageCommon().addQueueRequest(event, request);
        }
    }

    public StorageCommon getStorageCommon() {
        if (storageCommon == null) {
            storageCommon = new StorageCommon(new WeakReference<>(this));
        }
        return storageCommon;
    }

    public Typeface getRegularFont() {
        return mRegularFont;
    }

    public Typeface getBoldFont() {
        return mBoldFont;
    }

    public Typeface getMediumFont() {
        return mMediumFont;
    }

    public Typeface getLightFont() {
        return mLightFont;
    }

    public Typeface getHeavyFont() {
        return mHeavyFont;
    }

    public WebServiceAdapter getWebServiceAdapter() {
        return webServiceAdapter;
    }

    public Realm getRealm() {
        return realm;
    }
}
