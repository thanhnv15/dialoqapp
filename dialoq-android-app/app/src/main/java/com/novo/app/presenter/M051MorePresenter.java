package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RReminderEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM051MoreCallBack;

import static com.novo.app.utils.CommonUtil.P_API_KEY;

public class M051MorePresenter extends BasePresenter<OnM051MoreCallBack> {
    private static final String KEY_API_IA03_LOG_OUT = "KEY_API_IA03_LOG_OUT";
    private static final String URL_API_IA03_LOG_OUT = "user-service/api/v1/logout?";

    private static final String TAG = M051MorePresenter.class.getName();

    public M051MorePresenter(OnM051MoreCallBack event) {
        super(event);
    }

    @Override
    protected void handleSuccess(String data, String tag) {
        CommonUtil.wtfi(TAG, "DATA: " + data);
        if (KEY_API_IA03_LOG_OUT.equals(tag)) {
            doLogOut();
        }
    }

    private void doLogOut() {
        CommonUtil.getInstance().doLogOut();
        getStorage().setFlagUsedTouchID(true);
        mListener.showM023Login();
    }

    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tagRequest);

        if (KEY_API_IA03_LOG_OUT.equals(tag)) {
            if (!CommonUtil.getInstance().isConnectToNetwork()) {
                getStorage().setCallLoggedOut(false);
                mListener.showNetworkAlert();
            }
        }
    }

    public void callIA03LogOut() {
        CommonUtil.wtfi(TAG, "callIUNIA03LogOut");

        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_IA03_LOG_OUT);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, "token: " + token);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(URL_API_IA03_LOG_OUT);
        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    void callAS029GetListSchedulesOfAssessment() {
        RReminderEntity entity = CADBManager.getInstance().getRReminderEntity();
        if (entity == null) {
            mListener.showEmptyReminder();
        } else {
            mListener.showReminder();
        }
    }

//    @Override
//    protected void doTokenExpired(String tag, int code, String text, String requestId) {
//        doLogOut();
//    }

}
