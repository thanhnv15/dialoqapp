/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.view.adapter;

import android.content.Context;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM025SubDoseLogCallBack;
import com.novo.app.view.widget.NVDateUtils;

import java.util.List;

/**
 * Dose Log Adapter
 * <p>
 * This adapter working as an Adapter object of the RecycleView
 * Callbacks to this adapter will be defined by {@link OnM025SubDoseLogCallBack} interface
 */
public class DoseLogAdapter extends BaseRecycleAdapter<OnM025SubDoseLogCallBack, DataInfo, DoseLogAdapter.LangHolder> {
    public static final String TYPE_ASSESSMENT_INJECTION = "Injections";
    public static final String TYPE_ASSESSMENT_NOTE = "Notes";
    public static final String TYPE_ASSESSMENT_EMPTY_LOG = "Empty Log";
    public static final String TYPE_ASSESSMENT_OTHER = "Others";

    private static final String TAG = DoseLogAdapter.class.getName();

    private static final int TYPE_M025_TIME = 101;
    private static final int TYPE_M025_DRUG = 102;
    private static final int TYPE_M025_NOTE = 103;
    private static final int TYPE_M025_INCOMPLETE = 104;
    private static final int TYPE_M025_EMPTY_ALERT = 105;
    private static final int TYPE_M025_OTHER = 106;
    private static final int NO_LAYOUT = -1;
    private static final String TEXT_FORMAT = "%s %s";
    private String errDose, errPrefix;

    /**
     * Constructor for the adapter
     *
     * @param mContext  is the context object of the app
     * @param mListData is the list date of the Recycle view
     * @param mCallBack is the interface contain the callback method between the adapter layer and view layer
     */
    public DoseLogAdapter(Context mContext, List<DataInfo> mListData, OnM025SubDoseLogCallBack mCallBack) {
        super(mContext, mListData, mCallBack);
        errDose = LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m025_err_dose));
        errPrefix = LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m025_err_dose_prefix));
    }

    /**
     * Get the layout's id for each type of holder
     *
     * @param viewType is the code for each type of view
     * @return layout's id
     * @see BaseRecycleAdapter#getLayoutId(int)
     */
    @Override
    protected int getLayoutId(int viewType) {
        switch (viewType) {
            case TYPE_M025_TIME:
                return R.layout.item_m025_time;
            case TYPE_M025_DRUG:
                return R.layout.item_m025_drug;
            case TYPE_M025_NOTE:
                return R.layout.item_m025_note;
            case TYPE_M025_OTHER:
            case TYPE_M025_INCOMPLETE:
                return R.layout.item_m025_incomplete;
            case TYPE_M025_EMPTY_ALERT:
                return R.layout.item_m025_isnt_up_to_date;
            default:
                break;
        }
        return NO_LAYOUT;
    }

    /**
     * Base on assessment type to decide which type of layout should be displayed for each log item.
     *
     * @param position is the position of the log item
     * @return type code of the suitable layout
     */
    @Override
    public int getItemViewType(int position) {
        CommonUtil.wtfd(TAG, "getItemViewType..." + position);
        CommonUtil.wtfd(TAG, "getItemViewType...mListData.get(position).getResult().getAssessmentType(): " + mListData.get(position).getResult().getAssessmentType());
        switch (mListData.get(position).getResult().getAssessmentType()) {
            case TYPE_ASSESSMENT_OTHER:
                return TYPE_M025_OTHER;
            case TYPE_ASSESSMENT_INJECTION:

                if (CommonUtil.isUnknownDrug(mListData.get(position).getResult().getCustom3(),
                        mListData.get(position).getResult().getCustom4()) ||
                        CommonUtil.isUnknownDoseSize(mListData.get(position).getResult().getResult(),
                                mListData.get(position).getResult().getCustom4())) {
                    return TYPE_M025_INCOMPLETE;
                }
                return TYPE_M025_DRUG;
            case TYPE_ASSESSMENT_NOTE:
                return TYPE_M025_NOTE;
            case TYPE_ASSESSMENT_EMPTY_LOG:
                return TYPE_M025_EMPTY_ALERT;
            default:
                break;
        }
        return NO_LAYOUT;
    }

    /**
     * Create a ViewHolder for each item of the RecycleView
     *
     * @param viewType is the type code of the item
     * @param itemView is the View object of the item
     * @return ViewHolder for the item
     */
    @Override
    protected DoseLogAdapter.LangHolder getViewHolder(int viewType, View itemView) {
        return new DoseLogAdapter.LangHolder(itemView);
    }

    /**
     * Initiate data and view components when binding each item into the RecycleView
     *
     * @param holder   is the ViewHolder of the item
     * @param position is the position of the item in the RecycleView
     */
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        DoseLogAdapter.LangHolder item = (DoseLogAdapter.LangHolder) holder;
        DataInfo data = mListData.get(position);
        String datesPrevious = null;
        if (position > 0) {
            datesPrevious = NVDateUtils.getGroupItemDateTitle(mListData.get(position - 1).getResult().getReportedDate());
        }

        String date = NVDateUtils.getGroupItemDateTitle(data.getResult().getReportedDate());
        String time = NVDateUtils.getOnlyTime(data.getResult().getReportedDate());

        item.lnDate.removeAllViews();
        item.lnDate.setTag(data);
        boolean isHide = false;
        if (!date.equals(datesPrevious)) {
            initViewData(item, date, position == 0);
            isHide = true;
        }

        switch (data.getResult().getAssessmentType()) {
            case TYPE_ASSESSMENT_OTHER:
                initOther(isHide, item, data, time);
                break;
            case TYPE_ASSESSMENT_INJECTION:
                if (CommonUtil.isUnknownDrug(data.getResult().getCustom3(), data.getResult().getCustom4())
                        || CommonUtil.isUnknownDoseSize(data.getResult().getResult(), data.getResult().getCustom4())) {
                    initViewDrug(isHide, item, data, R.drawable.ic_m025_incomplete, time);
                    return;
                }

                switch (data.getResult().getCustom3()) {
                    case DeviceEntity.CODE_TRESIBA_100 + "":
                        initViewDrug(isHide, item, data, R.drawable.ic_tresiba, time);
                        break;
                    case DeviceEntity.CODE_TRESIBA_200 + "":
                        initViewDrug(isHide, item, data, R.drawable.ic_m012_drug_t, time);
                        break;
                    case DeviceEntity.CODE_FIASP + "":
                        initViewDrug(isHide, item, data, R.drawable.ic_fiasp, time);
                        break;
                    default:
                        initViewDrug(isHide, item, data, R.drawable.ic_m025_incomplete, time);
                        break;
                }
                break;
            case TYPE_ASSESSMENT_NOTE:
                initViewNote(isHide, item, data, time);
                break;
            case TYPE_ASSESSMENT_EMPTY_LOG:
                initViewNotUpToDate(item, data);
                break;
            default:
                break;
        }
    }

    /**
     * Initiate the data for each item of the RecycleView
     *
     * @param item        is the ViewHolder of the item
     * @param date        is the reported date of the item
     * @param isFirstItem is the indicator to tell if this is the first item
     */
    private void initViewData(LangHolder item, String date, boolean isFirstItem) {
        CommonUtil.wtfi(TAG, "initViewData...");
        View view = View.inflate(mContext, R.layout.item_m025_time, null);
        ((TextView) view.findViewById(R.id.tv_m025_date)).setText(date);
        if (!isFirstItem) {
            View vLine = View.inflate(mContext, R.layout.item_m025_line, null);
            item.lnDate.addView(vLine);
        }
        item.lnDate.addView(view);
    }

    /**
     * Initiate the view components of the Note item
     *
     * @param isHide is this item will be hide on the RecycleView
     * @param item   is the ViewHolder of the item
     * @param data   is the item's data
     * @param time   is the time to be displayed for the item
     */
    private void initViewNote(boolean isHide, LangHolder item, DataInfo data, String time) {
        CommonUtil.wtfi(TAG, "initViewNote...");
        item.vLine.setVisibility(isHide ? View.GONE : View.VISIBLE);
        item.tvTime.setText(time);
        item.tvNote.setText(String.format("%s", data.getResult().getNotes()));
        item.ivNote.setImageResource(R.drawable.ic_note);
    }

    /**
     * Initiate the view components of the Empty place holder
     *
     * @param item is the ViewHolder of the item
     * @param data is the item's data
     */
    private void initViewNotUpToDate(LangHolder item, DataInfo data) {
        CommonUtil.wtfi(TAG, "initViewNotUpToDate...");
        item.tvNotUpToDate.setText(data.getResult().getCustom1());
        if (item.vEmptyLine != null) {
            item.vEmptyLine.setVisibility((mListData == null || mListData.size() > 1) ? View.GONE : View.VISIBLE);
        }

        CommonUtil.wtfi(TAG, "initViewNotUpToDate...DONE");
    }

    /**
     * Initiate the view components of the Valid dose item
     *
     * @param isHide is this item will be hide on the RecycleView
     * @param item   is the ViewHolder of the item
     * @param data   is the item's data
     * @param icon   is the layout's id of the item icon
     * @param time   is the time to be displayed for the item
     */
    private void initViewDrug(boolean isHide, LangHolder item, DataInfo data, int icon, String time) {
        CommonUtil.wtfd(TAG, "initViewDrug...");
        item.vLine.setVisibility(isHide ? View.GONE : View.VISIBLE);
        item.tvTime.setText(time);
        String text = DeviceEntity.getDrugType(data.getResult().getCustom3());
        item.tvBrand.setText(text);

        if (CommonUtil.isUnknownDrug(data.getResult().getCustom3(), data.getResult().getCustom4()) ||
                CommonUtil.isUnknownDoseSize(data.getResult().getResult(), data.getResult().getCustom4())) {

            item.ivBrand.setImageResource(R.drawable.ic_m025_incomplete);
            item.tvBrand.setText(String.format(TEXT_FORMAT, errPrefix, errDose));
            item.tvBrand.setTypeface(CAApplication.getInstance().getRegularFont());
            String highLight = errPrefix;
            highLightText(item.tvBrand, highLight, R.color.colorBlack, true);

        } else {
            item.ivBrand.setImageResource(icon);
            item.tvUnit.setText(String.format(TEXT_FORMAT, data.getResult().getResult() + "", data.getResult().getResult() > 1 ? LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m025_units)) : LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m025_unit))));
        }
    }

    /**
     * Initiate the view components of the Other assessment item
     *
     * @param isHide is this item will be hide on the RecycleView
     * @param item   is the ViewHolder of the item
     * @param data   is the item's data
     * @param time   is the time to be displayed for the item
     */
    private void initOther(boolean isHide, LangHolder item, DataInfo data, String time) {
        CommonUtil.wtfi(TAG, "initOther...");

        item.vLine.setVisibility(isHide ? View.GONE : View.VISIBLE);
        item.tvTime.setText(time);
        String text = DeviceEntity.getDrugType(data.getResult().getCustom3());
        item.tvBrand.setText(text);

        item.ivBrand.setImageResource(R.drawable.ic_m025_incomplete);
        item.tvBrand.setText(String.format(TEXT_FORMAT, errPrefix, LangMgr.getInstance().getLangList().get(data.getResult().getCustom1())));
        item.tvBrand.setTypeface(CAApplication.getInstance().getRegularFont());
        String highLight = mContext.getString(R.string.txt_m025_error_highlight);
        highLightText(item.tvBrand, highLight, R.color.colorBlack, true);
    }

    /**
     * Object working as a placeholder for each item of the RecycleView
     */
    public class LangHolder extends BaseHolder {
        TextView tvTime, tvDate, tvBrand, tvUnit, tvNote, tvDevice, tvStatus, tvNotUpToDate;
        ImageView ivBrand, ivArrowIncomplete, ivArrowNote, ivNote;
        LinearLayout lnDate, lnNote;
        View vLine, vEmptyLine;

        /**
         * Constructor to initiate the ViewHolder
         *
         * @param itemView
         */
        LangHolder(View itemView) {
            super(itemView);
        }

        /**
         * Handle when the user taps on a view component of the item
         *
         * @param idView the layout's id of the view component is being taped on
         */
        @Override
        protected void onClickView(int idView) {
            if (idView == R.id.iv_m025_incomplete_arrow
                    || idView == R.id.iv_m025_note_arrow
                    || idView == R.id.tv_m025_brand
                    || idView == R.id.ln_m025_main_item
                    || idView == R.id.tv_m025_note) {
                DataInfo data = (DataInfo) lnDate.getTag();
                switch (data.getResult().getAssessmentType()) {
                    case TYPE_ASSESSMENT_OTHER:
                        mCallBack.toReminderPausedDetail(lnDate.getTag());
                        break;
                    case TYPE_ASSESSMENT_INJECTION:
                        if (CommonUtil.isUnknownDrug(data.getResult().getCustom3(), data.getResult().getCustom4()) ||
                                CommonUtil.isUnknownDoseSize(data.getResult().getResult(), data.getResult().getCustom4())) {
                            mCallBack.toIncompleteDoseDetail(lnDate.getTag());
                        }

                        break;
                    case TYPE_ASSESSMENT_NOTE:
                        getStorage().setM025NoteResultId(mListData.get(getAdapterPosition()).getResult().getResultId());
                        mCallBack.toDetailNote(lnDate.getTag());
                        break;
                    default:
                        break;
                }
            }
        }

        /**
         * Initiate the view component of a placeholder.
         */
        @Override
        protected void initView() {
            vEmptyLine = findViewById(R.id.v_m025_empty_line);
            vLine = findViewById(R.id.iv_m025_line);
            tvTime = findViewById(R.id.tv_m031_time, CAApplication.getInstance().getRegularFont());
            tvDate = findViewById(R.id.tv_m025_date, CAApplication.getInstance().getHeavyFont());
            tvBrand = findViewById(R.id.tv_m025_brand, this, CAApplication.getInstance().getBoldFont());
            tvNotUpToDate = findViewById(R.id.tv_m025_no_dose, CAApplication.getInstance().getRegularFont());
            tvNote = findViewById(R.id.tv_m025_note, this, CAApplication.getInstance().getRegularFont());
            tvUnit = findViewById(R.id.tv_m025_unit, CAApplication.getInstance().getRegularFont());
            tvDevice = findViewById(R.id.tv_m025_device, CAApplication.getInstance().getBoldFont());
            tvStatus = findViewById(R.id.tv_m025_status, CAApplication.getInstance().getRegularFont());
            ivBrand = findViewById(R.id.iv_m025_brand);
            ivArrowIncomplete = findViewById(R.id.iv_m025_incomplete_arrow, this);
            ivArrowNote = findViewById(R.id.iv_m025_note_arrow, this);
            ivNote = findViewById(R.id.iv_m025_note);
            lnDate = findViewById(R.id.ln_m025_item_date);
            lnNote = findViewById(R.id.ln_m025_main_item, this);
            findViewById(R.id.tr_m025_incomplete);
        }
    }
}