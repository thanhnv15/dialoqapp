package com.novo.app.presenter;

import com.novo.app.view.event.OnM034ReminderAllowAccessCallBack;

public class M034ReminderAllowAccessPresenter extends BasePresenter<OnM034ReminderAllowAccessCallBack>{
    public M034ReminderAllowAccessPresenter(OnM034ReminderAllowAccessCallBack event) {
        super(event);
    }
}
