package com.novo.app.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.novo.app.CAApplication;
import com.novo.app.view.event.OnActionCallBack;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class NetworkChangeReceiver extends BroadcastReceiver {

    public static final String ACTION_SHOW_LANDING_ACTIVE_KEY = "ACTION_SHOW_LANDING_ACTIVE_KEY";
    public static final String ACTION_NETWORK_LOST = "ACTION_NETWORK_LOST";
    public static final String ACTION_NETWORK_RESTORED = "ACTION_NETWORK_RESTORED";
    public static final String KEY_SAVE_TIME_OFFLINE = "KEY_SAVE_TIME_OFFLINE";
    public static final String KEY_SAVE_FLAG_OFFLINE = "KEY_SAVE_FLAG_OFFLINE";

    private static final String TAG = NetworkChangeReceiver.class.getName();
    private static final String KEY_SAVE_INNER_FLAG_OFFLINE = "KEY_SAVE_INNER_FLAG_OFFLINE";

    @Override
    public void onReceive(final Context context, final Intent intent) {
        boolean status = CommonUtil.getInstance().isConnectToNetwork();
        CommonUtil.wtfe(TAG, "onReceive...NetworkChangeReceiver..." + status);
        if (status) {
            CommonUtil.getInstance().savePrefContent(KEY_SAVE_FLAG_OFFLINE, "false");
            CommonUtil.getInstance().clearPrefContent(KEY_SAVE_INNER_FLAG_OFFLINE);
            callBack(ACTION_NETWORK_RESTORED);
        } else {
            String flagInnerOffline = CommonUtil.getInstance().getPrefContent(KEY_SAVE_INNER_FLAG_OFFLINE);
            if (flagInnerOffline != null && flagInnerOffline.equals("true")) {
                return;
            }

            //save time
            CommonUtil.getInstance().savePrefContent(KEY_SAVE_TIME_OFFLINE, System.currentTimeMillis() + "");
            CommonUtil.getInstance().savePrefContent(KEY_SAVE_INNER_FLAG_OFFLINE, "true");
            HashMap<String, OnActionCallBack> callBack = CAApplication.getInstance().getStorageCommon().getHomeActionCallBack();
            if (callBack != null && !callBack.keySet().isEmpty()) {
                callBack(ACTION_NETWORK_LOST);
            } else {
                CommonUtil.getInstance().savePrefContent(KEY_SAVE_FLAG_OFFLINE, "true");
            }
        }
    }

    private void callBack(String action) {
        HashMap<String, OnActionCallBack> callBacks = CAApplication.getInstance().getStorageCommon().getHomeActionCallBack();
        for (HashMap.Entry<String , OnActionCallBack> entry : callBacks.entrySet()){
            String key = entry.getKey();
            Objects.requireNonNull(callBacks.get(key)).onCallBack(action);
        }
    }
}