package com.novo.app.presenter;

import com.novo.app.ble.BLEInfoEntity;
import com.novo.app.ble.DoseItem;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.entities.RInjectionEntity;
import com.novo.app.manager.entities.RInjectionItem;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.DoseLogAdapter;
import com.novo.app.view.event.OnM025UpdatingCallBack;
import com.novo.app.view.widget.NVDateFormat;
import com.novo.app.view.widget.NVDateUtils;
import com.novo.app.view.widget.ProgressLoading;

import io.realm.RealmList;

public class M025UpdatingPresenter<T extends OnM025UpdatingCallBack> extends BLEPresenter<T> {

    private static final String TAG = M025SubDoseLogPresenter.class.getName();
    private DeviceEntity mDevices;
    private BLEInfoEntity mBLEInfoEntity;

    public M025UpdatingPresenter(T event) {
        super(event);
    }

    public void syncDataOffline(DeviceEntity devices, int mLastNumSync, BLEInfoEntity bLEInfoEntity) {
        CommonUtil.wtfi(TAG, "syncDataOffline...");
        ProgressLoading.dontShow();
        mDevices = devices;
        mBLEInfoEntity = bLEInfoEntity;
        getStorage().setM042CurrentItem(devices);

        if (mLastNumSync < 0) {
            mLastNumSync = 0;
        }
        long time = System.currentTimeMillis();
        RInjectionEntity entity = new RInjectionEntity();
        entity.setLastNumSync(mLastNumSync);
        entity.setDeviceName(bLEInfoEntity.getDeviceName());
        entity.setFirmwareRevision(bLEInfoEntity.getFirmwareRevision());
        entity.setHardwareRevision(bLEInfoEntity.getHardwareRevision());
        entity.setSerialNumber(bLEInfoEntity.getSerialNumber());
        entity.setSoftwareRevision(bLEInfoEntity.getSoftwareRevision());
        entity.setSystemFlag(bLEInfoEntity.getSystemFlag());
        entity.setSystemId(bLEInfoEntity.getSystemId());
        entity.setAssessmentId(getAssessmentId(CommonUtil.INJECTION_ASSESSMENT_TYPE) + "");
        entity.setTime(time);

        //DEVICE ID,result,REPORTED DATE,activity id,notes,custom 1,custom 2,custom 3,custom 4
        RealmList<RInjectionItem> realmList = new RealmList<>();
        for (int i = mLastNumSync; i < bLEInfoEntity.getDoseItems().size(); i++) {
            DoseItem item = bLEInfoEntity.getDoseItems().get(i);
            RInjectionItem doseEntity = new RInjectionItem();
            doseEntity.setDeviceId("");
            doseEntity.setAssessmentType(DoseLogAdapter.TYPE_ASSESSMENT_INJECTION);
            doseEntity.setResult(item.getDoseAmount() / 100);
            doseEntity.setReportedDate(CommonUtil.getSDate(bLEInfoEntity.getDoseItems().get(i).getCalculatedTime(), CommonUtil.DATE_STYLE));
            doseEntity.setActivityId("");
            doseEntity.setNotes(item.getSequenceNumber() + "");
            doseEntity.setCustom1(bLEInfoEntity.getDeviceName());

            doseEntity.setCustom2(time+"");
            doseEntity.setCustom3(item.getDrugType() + "");
            doseEntity.setCustom4(item.getStatusFlag() + "");

            if (item.getDrugType() == DeviceEntity.CODE_TRESIBA_100 || item.getDrugType() == DeviceEntity.CODE_TRESIBA_200) {
                String lastTresibaDose = CommonUtil.getSDate(bLEInfoEntity.getDoseItems().get(i).getCalculatedTime(), CommonUtil.DATE_STYLE);
                lastTresibaDose = NVDateUtils.dateToStringUniversal(NVDateUtils.stringToDateUniversal(lastTresibaDose, NVDateFormat.TIME_RANGE_FORMAT), NVDateFormat.TIME_RANGE_FORMAT);
                getStorage().setLastTresibaDose(lastTresibaDose);
            }

            realmList.add(doseEntity);
        }

        if (realmList.size() == 0) {
            mListener.syncFailed();
            return;
        }

        entity.setInjectionItems(realmList);
        entity.setDevice(callAS6UpdateResultOffline());

        boolean rs = CADBManager.getInstance().addRInjectionEntity(entity);
        if (rs) {
            mListener.syncSuccess();
        } else {
            mListener.syncFailed();
        }
    }

    private RInjectionItem callAS6UpdateResultOffline() {
        RInjectionItem entity = new RInjectionItem();
        String assessmentId = String.valueOf(getAssessmentId(CommonUtil.DEVICE_ASSESSMENT_TYPE));
        String resultId = getStorage().getM042CurrentItem().getResultId();

        entity.setAssessmentId(assessmentId);
        entity.setResultId(resultId);
        entity.setDeviceId(mBLEInfoEntity.getSystemFlag() != null
                && mBLEInfoEntity.getSystemFlag().equals(DeviceEntity.SYS_EOL_WARNING + "") ? "1" : "0");

        entity.setAssessmentType(CommonUtil.DEVICE_ASSESSMENT_TYPE);
        entity.setResult(isExistedTresiba() ? 1 : 0);
        entity.setReportedDate(mDevices.getLastPairNative());
        entity.setNotes(mDevices.getSystemId());
        entity.setCustom1(mDevices.getDeviceID());
        entity.setCustom2(System.currentTimeMillis() + "");
        entity.setCustom3(mBLEInfoEntity.getDoseItems().get(mBLEInfoEntity.getDoseItems().size() - 1).getDrugType() + "");
        entity.setCustom4(CommonUtil.getDateNow(CommonUtil.DATE_STYLE));

        CommonUtil.getInstance().savePrefContent(CommonUtil.LAST_TRESIBA_DOSE, getStorage().getLastTresibaDose());
        return entity;
    }

    private boolean isExistedTresiba() {
        if (mBLEInfoEntity == null || mBLEInfoEntity.getDoseItems() == null
                || mBLEInfoEntity.getDoseItems().isEmpty())
            return false;
        for (int i = 0; i < mBLEInfoEntity.getDoseItems().size(); i++) {
            if (mBLEInfoEntity.getDoseItems().get(i).getDrugType() == DeviceEntity.CODE_TRESIBA_100
                    || mBLEInfoEntity.getDoseItems().get(i).getDrugType() == DeviceEntity.CODE_TRESIBA_200) {
                return true;
            }
        }
        return false;
    }
}
