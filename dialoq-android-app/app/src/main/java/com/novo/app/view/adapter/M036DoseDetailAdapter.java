package com.novo.app.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.ResultInfo;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM036DoseDetailCallBack;
import com.novo.app.view.widget.NVDateUtils;

import java.util.List;

public class M036DoseDetailAdapter extends BaseRecycleAdapter<OnM036DoseDetailCallBack,
        ResultInfo, M036DoseDetailAdapter.DoseHolder> implements OnM036DoseDetailCallBack {
    public static final String TAG = M036DoseDetailAdapter.class.getName();
    private static final int TYPE_M036_NORMAL = 101;
    private static final int TYPE_M036_UNKNOWN_DRUG_TYPE = 102;
    private static final int TYPE_M036_UNKNOWN_DOSE_SIZE = 103;
    private static final int NO_LAYOUT = -1;
    private final String unit;


    public M036DoseDetailAdapter(Context mContext, List<ResultInfo> mListData, OnM036DoseDetailCallBack mCallBack) {
        super(mContext, mListData, mCallBack);
        this.unit = "%s " + LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m025_units));
    }

    @Override
    public void showM001LandingScreen(String err) {
        mCallBack.showM001LandingScreen(err);
    }

    @Override
    protected int getLayoutId(int viewType) {
        switch (viewType) {
            case TYPE_M036_NORMAL:
                return R.layout.item_m036_dose;
            case TYPE_M036_UNKNOWN_DRUG_TYPE:
                return R.layout.item_m036_unknown_type;
            case TYPE_M036_UNKNOWN_DOSE_SIZE:
                return R.layout.item_m036_unknown_size;
            default:
                break;
        }
        return NO_LAYOUT;
    }

    @Override
    protected DoseHolder getViewHolder(int viewType, View itemView) {
        return new M036DoseDetailAdapter.DoseHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        M036DoseDetailAdapter.DoseHolder item = (M036DoseDetailAdapter.DoseHolder) holder;

        ResultInfo data = mListData.get(position);

        if (CommonUtil.isUnknownDrug(data.getCustom3(), data.getCustom4())
                || CommonUtil.isUnknownDoseSize(data.getResult(), data.getCustom4())) {
            initViewUnknown(item, data, NVDateUtils.getOnlyTime(data.getReportedDate()));
        } else {
            initViewNormal(item, data, NVDateUtils.getOnlyTime(data.getReportedDate()));
        }

    }

    private void initViewUnknown(DoseHolder item, ResultInfo resultInfo, String time) {
        item.tvTime.setText(time);
        item.isEnable = false;
        item.trDialog.setVisibility(View.GONE);

        if (CommonUtil.isUnknownDrug(resultInfo.getCustom3(), resultInfo.getCustom4())) {
            item.tvUnknown.setText(LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m036_unknown_drug_type)));
        } else {
            if (resultInfo.getCustom3().equals(DeviceEntity.CODE_FIASP + "")) {
                item.vDoseUnknown.setBackgroundResource(R.drawable.bg_unknown_size_fiasp_chart);
                item.vConnectLine.setBackgroundColor(mContext.getResources().getColor(R.color.colorFiasp));
            }
            item.tvUnknown.setText(LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m036_unknown_dose_size)));
        }
    }

    private void initViewNormal(DoseHolder item, ResultInfo resultInfo, String time) {
        int height;
        if (resultInfo.getResult() <= 1) {
            height = (int) (resultInfo.getResult() * mContext.getResources().getDimension(R.dimen.d_60));
        } else {
            height = (int) (mContext.getResources().getDimension(R.dimen.d_60) + (resultInfo.getResult() - 1) * mContext.getResources().getDimension(R.dimen.d_30));
        }

        item.tvTime.setText(time);
        item.tvDose.setText(String.format(unit, resultInfo.getResult() + ""));
        ViewGroup.LayoutParams params = item.mainView.getLayoutParams();
        params.height = height;
        item.mainView.setLayoutParams(params);

        if (resultInfo.getCustom3().equals(DeviceEntity.CODE_TRESIBA_100 + "")) {
            item.vDose.setBackgroundResource(R.color.colorTresiba100);
            item.vLine.setBackgroundResource(R.color.colorTresiba100);
        } else if (resultInfo.getCustom3().equals(DeviceEntity.CODE_TRESIBA_200 + "")) {
            item.vDose.setBackgroundResource(R.color.colorTresiba200);
            item.vLine.setBackgroundResource(R.color.colorTresiba200);
        }else if (resultInfo.getCustom3().equals(DeviceEntity.CODE_FIASP + "")) {
            item.vDose.setBackgroundResource(R.color.colorFiasp);
            item.vLine.setBackgroundResource(R.color.colorFiasp);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (CommonUtil.isUnknownDrug(mListData.get(position).getCustom3(), mListData.get(position).getCustom4())) {
            return TYPE_M036_UNKNOWN_DRUG_TYPE;
        } else if (CommonUtil.isUnknownDoseSize(mListData.get(position).getResult(), mListData.get(position).getCustom4())) {
            return TYPE_M036_UNKNOWN_DOSE_SIZE;
        } else if (!CommonUtil.isUnknownDrug(mListData.get(position).getCustom3(), mListData.get(position).getCustom4())) {
            return TYPE_M036_NORMAL;
        }
        return NO_LAYOUT;
    }

    public class DoseHolder extends BaseHolder {
        TextView tvTime;
        TextView tvDose;
        TextView vDose;
        TextView vDoseUnknown;
        TextView tvUnknown;
        TableRow trDialog;
        View vLine;
        View vConnectLine;

        View mainView;
        private boolean isEnable = false;

        DoseHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void onClickView(int idView) {
            if (idView == R.id.v_m036_dose_unknown) {
                if (!isEnable) {
                    trDialog.setVisibility(View.VISIBLE);
                    isEnable = true;
                } else {
                    trDialog.setVisibility(View.GONE);
                    isEnable = false;
                }
            }
        }

        @Override
        protected void initView() {
            tvTime = findViewById(R.id.tv_m036_dose_time, CAApplication.getInstance().getRegularFont());
            tvDose = findViewById(R.id.tv_m036_dose, CAApplication.getInstance().getRegularFont());
            tvUnknown = findViewById(R.id.tv_m036_unknown, CAApplication.getInstance().getMediumFont());
            vDose = findViewById(R.id.v_m036_dose);
            vDoseUnknown = findViewById(R.id.v_m036_dose_unknown, this);
            vLine = findViewById(R.id.v_m036_line);
            vConnectLine = findViewById(R.id.v_m036_connect_line);
            trDialog = findViewById(R.id.tr_m036_dialog, this);
            mainView = findViewById(R.id.tr_m036_dose);
        }
    }

}