package com.novo.app.view.event;

public interface OnM014SignUpCallBack extends OnCallBackToView {
    void showM023Login();

    void showM013SignUpCode(String sms);

    void callAPISignUpSuccess();
}
