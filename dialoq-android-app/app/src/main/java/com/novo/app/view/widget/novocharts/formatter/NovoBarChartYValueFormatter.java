package com.novo.app.view.widget.novocharts.formatter;

import com.github.mikephil.charting.formatter.ValueFormatter;
import com.novo.app.manager.LangMgr;
import com.novo.app.utils.CommonUtil;

import java.util.Locale;

public class NovoBarChartYValueFormatter extends ValueFormatter {
    private static final String TAG = NovoBarChartYValueFormatter.class.getName();
    private static final String LABEL_UNIT = "SUMMARY.SCREEN.LABEL.UNITS";
    private int maxValue;

    public NovoBarChartYValueFormatter(int maxValue) {
        this.maxValue = maxValue;
    }

    @Override
    public String getFormattedValue(boolean isMaxValue, float value) {
        if (isMaxValue) {
            CommonUtil.wtfi(TAG, "GET MAX VALUE");
            return LangMgr.getInstance().getLangList().get(LABEL_UNIT);
        }
        if (value == 0) {
            return "";
        }
        return String.format(Locale.US, "%.0f", value);
    }

    @Override
    public String getFormattedValue(float value) {
        if (value == 0) {
            return "";
        }
        if ((int) value % 2 == 0) {
            return String.format(Locale.US, "%.0f", value);
        }
        return "";
    }

}
