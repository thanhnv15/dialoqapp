package com.novo.app.presenter;

import com.novo.app.view.event.OnM047SupportTroubleshootingCallBack;

public class M047SupportTroubleshootingPresenter extends BasePresenter<OnM047SupportTroubleshootingCallBack> {
    public M047SupportTroubleshootingPresenter(OnM047SupportTroubleshootingCallBack event) {
        super(event);
    }
}
