package com.novo.app.view.fragment;

import android.print.PdfPrint;
import android.print.PrintAttributes;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M069SharePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM069ShareCallBack;

import java.io.File;
import java.util.Calendar;
import java.util.Locale;

public class M069SharePreviewFrg extends BaseFragment<M069SharePresenter, OnM024DoseLogCallBack> implements OnM069ShareCallBack {
    public static final String TAG = M069SharePreviewFrg.class.getName();
    public static final int KEY_LAST_7_DAYS = 0;
    public static final int KEY_LAST_14_DAYS = 1;
    public static final int KEY_LAST_28_DAYS = 2;
    private static final String BOTH_DRUG_FORMAT = "{\"fileName\": \"%s\", \"dataChartJson\":%s, \"dataChartJson2\":%s}";
    private static final String SINGLE_TRESIBA_FORMAT = "{\"fileName\": \"%s\", \"dataChartJson\":%s}";
    private static final String SINGLE_FIASP_FORMAT = "{\"fileName\": \"%s\", \"dataChartJson2\":%s}";
    private WebView mWebView;
    private String path;

    @Override
    protected M069SharePresenter getPresenter() {
        return new M069SharePresenter(this);
    }

    @Override
    protected void initViews() {
        mCallBack.changeTab(CommonUtil.TAB_MENU_SHARE);
        mCallBack.hideBottomBar(true);
        findViewById(R.id.tv_m069_title, CAApplication.getInstance().getBoldFont());
        TextView mTvDescription = findViewById(R.id.tv_m069_description, CAApplication.getInstance().getRegularFont());
        switch (getStorage().getM068TypeDate()) {
            case KEY_LAST_7_DAYS:
                mTvDescription.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m069_description_7)));
                break;
            case KEY_LAST_14_DAYS:
                mTvDescription.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m069_description_14)));
                break;
            case KEY_LAST_28_DAYS:
                mTvDescription.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m069_description_28)));
                break;
        }

        findViewById(R.id.bt_m069_share, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m069_cancel, this, CAApplication.getInstance().getMediumFont());
        mWebView = findViewById(R.id.wv_m069_detail);
        initWebView();
    }

    private void createWebPrintJob(WebView webView) {
        String jobName = getString(R.string.app_name) + " Document";
        PrintAttributes attributes = new PrintAttributes.Builder()
                .setMediaSize(PrintAttributes.MediaSize.ISO_A4)
                .setResolution(new PrintAttributes.Resolution("pdf", "pdf", 600, 600))
                .setMinMargins(PrintAttributes.Margins.NO_MARGINS).build();

        path = CAApplication.getInstance().getExternalFilesDir(null) + "";
        File file = new File(path);
        PdfPrint pdfPrint = new PdfPrint(attributes);
        pdfPrint.print(webView.createPrintDocumentAdapter(jobName), file, generateReportName());
    }

    private void initWebView() {
        mWebView.setWebViewClient(new MyCustomWebView());
        WebSettings settings = mWebView.getSettings();

        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);

        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setAllowUniversalAccessFromFileURLs(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.loadUrl("file:///android_asset/report.html");

    }

    private void sendDataToWebView(String data) {
        mWebView.evaluateJavascript("receiveData(" + data + ");", value -> {
            //Do nothing
        });
        mCallBack.dismissGeneratingProgress();
    }

    @Override
    public int getLayoutId() {
        return R.layout.frg_m069_share_preview;
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        if (idView == R.id.bt_m069_share) {
            createWebPrintJob(mWebView);
            CommonUtil.getInstance().sharingPDF(path + generateReportName(), mContext);
        } else if (idView == R.id.tv_m069_cancel) {
            CommonUtil.getInstance().deletePDF(generateReportName());
            mCallBack.showChildFrgScreen(TAG, M068ShareChooseFrg.TAG);
        }
    }

    private String generateReportName() {
        int date = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
        String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        year = year.substring(year.length() - 2);

        return String.format(Locale.ENGLISH, "/Dialoq_summary_%02d%02d%02d.pdf", date, month, Integer.valueOf(year));
    }

    @Override
    public void onDetach() {
        mCallBack.hideBottomBar(false);
        super.onDetach();
    }

    private class MyCustomWebView extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            String totalChart = String.format(BOTH_DRUG_FORMAT, generateReportName().substring(1), getStorage().getM068TresibaShareData().getJson(), getStorage().getM068FiaspShareData().getJson());
            if (!getStorage().getM068TresibaShareData().isEmpty() && getStorage().getM068FiaspShareData().isEmpty()) {
                totalChart = String.format(SINGLE_TRESIBA_FORMAT, generateReportName().substring(1), getStorage().getM068TresibaShareData().getJson());
            }
            if (getStorage().getM068TresibaShareData().isEmpty() && !getStorage().getM068FiaspShareData().isEmpty()) {
                totalChart = String.format(SINGLE_FIASP_FORMAT, generateReportName().substring(1), getStorage().getM068FiaspShareData().getJson());
            }
            sendDataToWebView(totalChart);
        }
    }
}
