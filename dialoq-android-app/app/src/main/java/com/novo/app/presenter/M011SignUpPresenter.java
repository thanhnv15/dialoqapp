package com.novo.app.presenter;

import com.novo.app.view.event.OnM011SignUpCallBack;

public class M011SignUpPresenter extends BasePresenter<OnM011SignUpCallBack> {
    public M011SignUpPresenter(OnM011SignUpCallBack event) {
        super(event);
    }
}
