package com.novo.app.model.entities;

public class NoteEntity {
    private String time;
    private String note;

    public NoteEntity(String time, String dose) {
        this.time = time;
        this.note = dose;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
