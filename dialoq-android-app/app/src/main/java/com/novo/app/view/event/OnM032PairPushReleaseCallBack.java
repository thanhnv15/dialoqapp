package com.novo.app.view.event;

public interface OnM032PairPushReleaseCallBack extends OnCallBackToView {
    void pairCompleted();

    void resetStateBLE();
}
