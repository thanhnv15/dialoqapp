package com.novo.app.presenter;

import com.novo.app.view.event.OnM015SignUpCallBack;

public class M015SignUpPresenter extends BasePresenter<OnM015SignUpCallBack> {
    public M015SignUpPresenter(OnM015SignUpCallBack event) {
        super(event);
    }
}
