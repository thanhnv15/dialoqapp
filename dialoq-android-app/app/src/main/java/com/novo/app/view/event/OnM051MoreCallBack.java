package com.novo.app.view.event;

public interface OnM051MoreCallBack extends OnCallBackToView {
    default void showM023Login() {
    }

    default void showReminder() {
    }

    default void showEmptyReminder() {
    }

    default void showNetworkAlert() {
    }

    default void deleteSuccess() {

    }
}
