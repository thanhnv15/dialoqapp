package com.novo.app.view.fragment;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M023LoginPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM023LoginCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.ProgressLoading;

public class M022SignUpDoneFrg extends BaseFragment<M023LoginPresenter, OnHomeBackToView> implements OnM023LoginCallBack {

    public static final String TAG = M022SignUpDoneFrg.class.getName();

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m022_back, this);
        findViewById(R.id.tv_m022_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m022_description, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m022_later, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.bt_m022_next, this, CAApplication.getInstance().getMediumFont());
        mPresenter.callIA02AuthenticateUser(getStorage().getM001ProfileEntity().getEmail(), getStorage().getM001ProfileEntity().getPassword());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m022_signup_done;
    }

    @Override
    protected M023LoginPresenter getPresenter() {
        return new M023LoginPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        // Wait for customer confirmation
    }

    @Override
    public void backToPreviousScreen() {
        mCallBack.showFrgScreen(TAG, M021SignUpProfileThinkFrg.TAG);
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m022_next:
                getStorage().setM029FirstPair(true);
                mCallBack.showFrgScreen(TAG, M029PairAttachFrg.TAG);
                break;
            case R.id.tv_m022_later:
                mCallBack.showFrgScreen(TAG, M024DoseLogFrg.TAG);
                break;
            case R.id.iv_m022_back:
                backToPreviousScreen();
                break;
            default:
                break;
        }
    }

    @Override
    public void showM001LandingScreen(String sms) {
        // This feature isn't require here
    }

    @Override
    public void loginSuccess() {
        CommonUtil.getInstance().savePrefContent(CommonUtil.USER_NAME, getStorage().getM001ProfileEntity().getEmail());
        CommonUtil.getInstance().savePrefContent(CommonUtil.PASS, getStorage().getM001ProfileEntity().getPassword());
        CommonUtil.getInstance().savePrefContent(CommonUtil.IS_LOGIN,CommonUtil.IS_LOGIN_TRUE);
        ProgressLoading.dismiss();
        CAApplication.getInstance().startSyncService();
    }

    @Override
    public void updateFingerPrintResult() {
        // This feature isn't require here
    }

    @Override
    public void showDialog(int txt, String tag) {
        CommonUtil.getInstance().showDialog(mContext, txt, new OnOKDialogCallBack() {
        });
    }

    @Override
    public void showDialog(String txt, String tag) {
        CommonUtil.getInstance().showDialog(mContext, txt, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton1() {
                if (tag.equals(M023LoginPresenter.KEY_API_IA02_AUTHENTICATE_USER)) {
                    mCallBack.showFrgScreen(tag, M023LoginFrg.TAG);
                }
                if (tag.equals(M023LoginPresenter.KEY_BLOCKED_BY_COUNTRY)) {
                    getStorage().setAppActive(true);
                    mCallBack.showFrgScreen(tag, M001LandingPageFrg.TAG);
                }
            }
        });
    }
}
