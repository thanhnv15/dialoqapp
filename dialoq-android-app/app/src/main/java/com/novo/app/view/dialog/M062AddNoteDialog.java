/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.view.dialog;

import android.content.Context;
import android.text.Editable;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M062AddNotePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseDialog;
import com.novo.app.view.event.OnActionCallBack;
import com.novo.app.view.event.OnM025SubDoseLogCallBack;
import com.novo.app.view.event.OnM062AddNoteCallBack;
import com.novo.app.view.widget.NVDateFormat;
import com.novo.app.view.widget.NVDateUtils;
import com.novo.app.view.widget.ProgressLoading;
import com.novo.app.view.widget.TextAdapter;

public class M062AddNoteDialog extends BaseDialog<M062AddNotePresenter, OnM025SubDoseLogCallBack> implements OnM062AddNoteCallBack {

    public static final String TAG = M062AddNoteDialog.class.getName();

    private TextView tvNoteDate;
    private TextView tvNoteTime;
    private EditText edtInputNote;
    private Button mBtSave;
    private String dateNote;

    public M062AddNoteDialog(Context context, boolean isCancel) {
        super(context, isCancel, R.style.dialog_style);
    }

    @Override
    public void showM001LandingScreen(String err) {
        mCallBack.showM001LandingScreen(err);
    }

    @Override
    protected M062AddNotePresenter getPresenter() {
        return new M062AddNotePresenter(this);
    }

    @Override
    protected void warningForTimeZoneChange() {
        CommonUtil.wtfi(TAG, "warningForTimeZoneChange...");
        String timeFormat = android.text.format.DateFormat.is24HourFormat(mContext) ? NVDateFormat.TIME_SIMPLE_FORMAT_24 : NVDateFormat.TIME_SIMPLE_FORMAT;
        tvNoteTime.setText(CommonUtil.getDateNow(timeFormat));

        initReportDate();
    }

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m062_note_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m062_note_description, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m062_note_date_time_title, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m062_note_date_time_breaker, this, CAApplication.getInstance().getRegularFont());
        tvNoteDate = findViewById(R.id.tv_m062_note_date, this, CAApplication.getInstance().getRegularFont());
        tvNoteTime = findViewById(R.id.tv_m062_note_time, this, CAApplication.getInstance().getRegularFont());

        edtInputNote = findViewById(R.id.edt_m062_edit_note, CAApplication.getInstance().getRegularFont());
        edtInputNote.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                mBtSave.setEnabled(editable.length() > 0);
            }
        });
        mBtSave = findViewById(R.id.bt_m062_save, this, false, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.iv_m062_close_note, this, null);
        tvNoteDate.setText(NVDateUtils.TODAY);

        warningForTimeZoneChange();
    }

    private void initReportDate() {
        CommonUtil.getInstance().dateVerify(mContext, null, tvNoteDate, tvNoteTime, new OnActionCallBack() {
            @Override
            public void onCallBack(Object data) {
                dateNote = (String) data;
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.view_m062_add_note;
    }


    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m062_note_date:
            case R.id.tv_m062_note_time:
            case R.id.tv_m062_note_date_time_breaker:
                CommonUtil.getInstance().showLimitedDatePicker(mContext, tvNoteDate, tvNoteTime, data -> dateNote = (String) data);
                break;
            case R.id.bt_m062_save:
                CommonUtil.getInstance().forceHideKeyBoard(edtInputNote);
                //String note = CommonUtil.getInstance().convertSpecialChar(edtInputNote.getText().toString());
                CommonUtil.wtfi(TAG, "Note to push:" + edtInputNote.getText().toString() + ", date: " + dateNote);
                mPresenter.addNewNoteToDB(CommonUtil.ASSESSMENT_TYPE_NOTE, edtInputNote.getText().toString(), dateNote);
                break;
            case R.id.iv_m062_close_note:
                CommonUtil.getInstance().forceHideKeyBoard(edtInputNote);
                edtInputNote.setText("");
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void showDoseLog(String data) {
        ProgressLoading.dismiss();
        if (data == null || data.isEmpty()) {
            mCallBack.showAlertDialog(LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m062_add_note_failed)));
        } else {
            dismiss();
            mCallBack.refreshData();
        }

    }

    @Override
    public void showAddNoteFail() {
        ProgressLoading.dismiss();
        mCallBack.showAlertDialog(LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m062_add_note_failed)));
    }
}
