package com.novo.app.view.fragment;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M004AccountSetUpPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM004AccountSetUpCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

public class M004AccountSetUpFrg extends BaseFragment<M004AccountSetUpPresenter, OnHomeBackToView> implements OnM004AccountSetUpCallBack {

    public static final String TAG = M004AccountSetUpFrg.class.getName();

    @Override
    protected void initViews() {
        getStorage().clearAllDataLogin();
        findViewById(R.id.iv_m004_logo);
        findViewById(R.id.tv_m004_to_get_started, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.bt_m004_next, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m004_cancel, this, CAApplication.getInstance().getMediumFont());
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m004_account_set_up;
    }

    @Override
    protected M004AccountSetUpPresenter getPresenter() {
        return new M004AccountSetUpPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        // Wait for confirmation from customer
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m004_next:
                mCallBack.showFrgScreen(TAG, M006AccountSetUpCountryFrg.TAG);
                break;
            case R.id.tv_m004_cancel:
                mCallBack.registrationCancel(TAG);
                break;

            default:
                break;
        }
    }

    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }
}
