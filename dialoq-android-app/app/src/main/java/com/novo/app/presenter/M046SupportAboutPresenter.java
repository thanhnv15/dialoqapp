package com.novo.app.presenter;

import com.novo.app.view.event.OnM046SupportAboutCallBack;

public class M046SupportAboutPresenter extends BasePresenter<OnM046SupportAboutCallBack> {
    public M046SupportAboutPresenter(OnM046SupportAboutCallBack event) {
        super(event);
    }
}
