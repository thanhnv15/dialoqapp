package com.novo.app.view.event;

import android.support.design.widget.TabLayout;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.novo.app.model.entities.DateEntity;

public interface OnM036SubDoseLogSummaryCallBack extends OnCallBackToView {

    default void onNovoItemSelected(int colorType, BarData mData, Entry entry, Highlight highlight) {

    }

    default void dataAS05Ready() {

    }

    default void showSummaryDialog(DateEntity item, int colorType) {

    }

    default void onTabSelected(TabLayout.Tab tab) {

    }
}
