package com.novo.app.view.fragment;

import android.widget.Button;
import android.widget.CheckBox;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M021SignUpPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM021SignUpCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

public class M021SignUpProfileThinkFrg extends BaseFragment<M021SignUpPresenter, OnHomeBackToView> implements OnM021SignUpCallBack {

    public static final String TAG = M021SignUpProfileThinkFrg.class.getName();
    private Button mBtNext;
    private CheckBox mCbAgree;

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m021_back, this);
        findViewById(R.id.tv_m021_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m021_description, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m021_no_thanks, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m021_detail, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m021_agree, this, CAApplication.getInstance().getRegularFont());

        mCbAgree = findViewById(R.id.cb_m021_agree, this);
        mBtNext = findViewById(R.id.bt_m021_agree, this, CAApplication.getInstance().getMediumFont());
        getStorage().getM001ProfileEntity().setStreetAddress2(CommonUtil.getDateNow(CommonUtil.DATE_STYLE));
        validateTerm();
    }

    private void validateTerm() {
        if (mCbAgree.isChecked()) {
            mBtNext.setEnabled(true);
            return;
        }
        mBtNext.setEnabled(false);
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m021_signup_profile_think;
    }

    @Override
    protected M021SignUpPresenter getPresenter() {
        return new M021SignUpPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        if (getTagSource().equals(M019SignUpHowLongFrg.TAG)
                || getTagSource().equals(M016ProfileStartFrg.TAG)) {
            CommonUtil.getInstance().defineBackTag(getTagSource(), TAG);
        }
    }

    @Override
    public void backToPreviousScreen() {
        String backTag = CommonUtil.getInstance().doBackFlowWithBackTag(TAG);

        if (backTag.equals(M019SignUpHowLongFrg.TAG)) {
            mCallBack.showFrgScreen(TAG, M019SignUpHowLongFrg.TAG);
        } else if (backTag.equals(M016ProfileStartFrg.TAG)) {
            mCallBack.showFrgScreen(TAG, M016ProfileStartFrg.TAG);
        }
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.cb_m021_agree:
                validateTerm();
                break;
            case R.id.bt_m021_agree:
                getStorage().getM001ProfileEntity().setRace(CommonUtil.AGREE_VALUE);
                mPresenter.callUN04Update();
                break;
            case R.id.tv_m021_no_thanks:
                getStorage().getM001ProfileEntity().setRace(CommonUtil.NOT_AGREE_VALUE);
                mPresenter.callUN04Update();
                break;
            case R.id.iv_m021_back:
                backToPreviousScreen();
                break;
            default:
                break;
        }
    }

    @Override
    public void showM022SignUpSuccess() {
        mCallBack.showFrgScreen(TAG, M022SignUpDoneFrg.TAG);
    }
}
