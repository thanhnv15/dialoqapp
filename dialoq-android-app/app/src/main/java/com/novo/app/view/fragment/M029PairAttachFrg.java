package com.novo.app.view.fragment;

import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M029PairAttachPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM029PairAttachCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

public class M029PairAttachFrg extends BaseFragment<M029PairAttachPresenter, OnHomeBackToView> implements OnM029PairAttachCallBack {

    public static final String TAG = M029PairAttachFrg.class.getName();

    @Override
    protected void initViews() {
        TextView mTvTitle = findViewById(R.id.tv_m029_title, CAApplication.getInstance().getBoldFont());
        String highLightTitle = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m029_attach));
        highLightText(mTvTitle, highLightTitle, R.color.colorM001Cyan);

        findViewById(R.id.bt_m029_continue,this);
    }
    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m029_pair_attach;
    }

    @Override
    protected M029PairAttachPresenter getPresenter() {
        return new M029PairAttachPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        // This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        // This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        if (idView == R.id.bt_m029_continue){
            if (getStorage().isM029FirstPair()){
                mCallBack.showFrgScreen(TAG, M030PairRepeatVideoFrg.TAG);
            } else {
                mCallBack.showFrgScreen(TAG, M032PairPushReleaseFrg.TAG);
            }
        }
    }
}
