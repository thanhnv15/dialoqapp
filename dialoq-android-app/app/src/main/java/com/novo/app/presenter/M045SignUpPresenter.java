package com.novo.app.presenter;

import com.novo.app.view.event.OnM045SignUpCallBack;

public class M045SignUpPresenter extends BasePresenter<OnM045SignUpCallBack> {
    public M045SignUpPresenter(OnM045SignUpCallBack event) {
        super(event);
    }
}
