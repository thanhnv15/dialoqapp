/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.view.fragment;

import android.app.AlertDialog;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M080FirstLoginDidntGetPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM080FirstLoginDidntGetCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

/**
 * First Login Didn't Get Code Fragment
 * <p>
 * This fragment is 'View' object in MVP pattern
 * This fragment uses an {@link M080FirstLoginDidntGetPresenter} as linked Presenter
 * Callbacks to this fragment will be defined by {@link OnM080FirstLoginDidntGetCallBack} interface
 * <p>
 * This fragment is used in Reset password features of dialog app, where user has to input
 * email to receive confirmation code
 */
public class M080FirstLoginDidntGetFrg extends BaseFragment<M080FirstLoginDidntGetPresenter, OnHomeBackToView> implements OnM080FirstLoginDidntGetCallBack {
    public static final String TAG = M080FirstLoginDidntGetFrg.class.getName();
    private TextView mTvEmail;
    private AlertDialog mAlertDialog;


    /**
     * Handle callback from {@link M080FirstLoginDidntGetPresenter}
     * {@link OnHomeBackToView#showM001LandingScreen(String err)}
     *
     * @param message error string description
     */
    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    /**
     * Initiate views component for First Login Didn't Get Code Fragment
     *
     * @see BaseFragment#initViews()
     */
    @Override
    protected void initViews() {
        findViewById(R.id.iv_m080_close, this);
        findViewById(R.id.tv_m080_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m080_guide, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m080_suggest_1, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m080_suggest_2, CAApplication.getInstance().getRegularFont());

        findViewById(R.id.bt_m080_try_again, this, CAApplication.getInstance().getMediumFont());
        mTvEmail = findViewById(R.id.tv_m080_email, CAApplication.getInstance().getBoldFont());
        mTvEmail.setText(getStorage().getM075UserLoginInfo().getKey());
    }

    /**
     * Get layout id for First Login Didn't Get Code Fragment
     *
     * @return layout id
     * @see BaseFragment#getLayoutId()
     */
    @Override
    protected int getLayoutId() {
        return R.layout.frg_m080_first_login_didnt_get;
    }

    /**
     * Get presenter for this fragment
     *
     * @return {@link M080FirstLoginDidntGetPresenter} instance
     * @see BaseFragment#getPresenter()
     */
    @Override
    protected M080FirstLoginDidntGetPresenter getPresenter() {
        return new M080FirstLoginDidntGetPresenter(this);
    }

    /**
     * @return tag in {@link String}
     * @see BaseFragment#getTAG()
     */
    @Override
    protected String getTAG() {
        return TAG;
    }

    /**
     * @see BaseFragment#defineBackKey()
     */
    @Override
    protected void defineBackKey() {
        // This feature isn't require here
    }

    /**
     * @param idView id of clicked view
     * @see BaseFragment#onClickView(int)
     */
    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m080_close:
                getStorage().setM078ErrorMessage(null);
                mCallBack.showFrgScreen(TAG, M077FirstLoginCodeSentFrg.TAG);
                break;
            case R.id.bt_m080_try_again:
                getStorage().setM078ErrorMessage(null);
                mPresenter.requestCodeIA04();
                break;
        }
    }

    /**
     * Handle callback from {@link OnM080FirstLoginDidntGetCallBack}
     * {@link OnM080FirstLoginDidntGetCallBack#showM077FirstLoginCode()}
     */
    @Override
    public void showM077FirstLoginCode() {
        mCallBack.showFrgScreen(TAG, M077FirstLoginCodeSentFrg.TAG);
    }

    /**
     * Handle callback from {@link OnM080FirstLoginDidntGetCallBack}
     * {@link OnM080FirstLoginDidntGetCallBack#sendCodeSuccess()}
     */
    @Override
    public void sendCodeSuccess() {
        mCallBack.showFrgScreen(TAG, M077FirstLoginCodeSentFrg.TAG);
    }

    /**
     * Handle callback from {@link OnM080FirstLoginDidntGetCallBack}
     * {@link OnM080FirstLoginDidntGetCallBack#sendCodeFailed(String)}
     */
    @Override
    public void sendCodeFailed(String message) {
        if (mAlertDialog != null && mAlertDialog.isShowing()) return;
        mAlertDialog = CommonUtil.getInstance().showDialog(mContext, message, new OnOKDialogCallBack() {
        });
    }
}
