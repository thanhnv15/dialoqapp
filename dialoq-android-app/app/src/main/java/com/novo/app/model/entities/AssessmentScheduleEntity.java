package com.novo.app.model.entities;

import com.google.gson.annotations.SerializedName;
import com.novo.app.model.BaseModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AssessmentScheduleEntity extends BaseModel {
    @SerializedName("totalCount")
    private int totalCount;
    @SerializedName("data")
    private List<DataInfo> data;
    @SerializedName("paging")
    private PageInfo pageInfo;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<DataInfo> getData() {
        if (data == null) {
            data = new ArrayList<>();
        }
        return data;
    }

    public void setData(List<DataInfo> data) {
        this.data = data;
    }

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public class DataInfo implements Serializable {

        @SerializedName("scheduleId")
        private long scheduleId;
        @SerializedName("scheduleName")
        private String scheduleName;
        @SerializedName("freqType")
        private String freqType;
        @SerializedName("freqInterval")
        private String freqInterval;
        @SerializedName("freqSubdayType")
        private String freqSubdayType;
        @SerializedName("startTime")
        private String startTime;
        @SerializedName("startDate")
        private String startDate;
        @SerializedName("endDate")
        private String endDate;
        @SerializedName("scheduleNotifications")
        private List<ScheduleNotification> scheduleNotifications;

        public long getScheduleId() {
            return scheduleId;
        }

        public void setScheduleId(long scheduleId) {
            this.scheduleId = scheduleId;
        }

        public String getScheduleName() {
            return scheduleName;
        }

        public void setScheduleName(String scheduleName) {
            this.scheduleName = scheduleName;
        }


        public String getFreqType() {
            return freqType;
        }

        public void setFreqType(String freqType) {
            this.freqType = freqType;
        }

        public String getFreqInterval() {
            return freqInterval;
        }

        public void setFreqInterval(String freqInterval) {
            this.freqInterval = freqInterval;
        }

        public String getFreqSubdayType() {
            return freqSubdayType;
        }

        public void setFreqSubdayType(String freqSubdayType) {
            this.freqSubdayType = freqSubdayType;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public List<ScheduleNotification> getScheduleNotifications() {
            return scheduleNotifications;
        }

        public void setScheduleNotifications(List<ScheduleNotification> scheduleNotifications) {
            this.scheduleNotifications = scheduleNotifications;
        }

        public class ScheduleNotification implements Serializable {
            @SerializedName("scheduleNotificationId")
            private String scheduleNotificationId;
            @SerializedName("notificationChannel")
            private String notificationChannel;
            @SerializedName("notificationType")
            private String notificationType;
            @SerializedName("reminderTime")
            private String reminderTime;

            public String getScheduleNotificationId() {
                return scheduleNotificationId;
            }

            public void setScheduleNotificationId(String scheduleNotificationId) {
                this.scheduleNotificationId = scheduleNotificationId;
            }

            public String getNotificationChannel() {
                return notificationChannel;
            }

            public void setNotificationChannel(String notificationChannel) {
                this.notificationChannel = notificationChannel;
            }

            public String getNotificationType() {
                return notificationType;
            }

            public void setNotificationType(String notificationType) {
                this.notificationType = notificationType;
            }

            public String getReminderTime() {
                return reminderTime;
            }

            public void setReminderTime(String reminderTime) {
                this.reminderTime = reminderTime;
            }
        }
    }

    public class PageInfo implements Serializable {
        @SerializedName("first")
        private String first;
        @SerializedName("previous")
        private String previous;
        @SerializedName("next")
        private String next;
        @SerializedName("last")
        private String last;

        public String getFirst() {
            return first;
        }

        public void setFirst(String first) {
            this.first = first;
        }

        public String getPrevious() {
            return previous;
        }

        public void setPrevious(String previous) {
            this.previous = previous;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public String getLast() {
            return last;
        }

        public void setLast(String last) {
            this.last = last;
        }
    }
}
