package com.novo.app.view.event;

public interface OnM036DoseDetailDialog extends OnCallBackToView{
    void toPreviousDay(int colorType, int currentPos);

    void toNextDay(int colorType, int currentPos);

    int getCurrentPage();
}
