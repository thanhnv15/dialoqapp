package com.novo.app.view.fragment;

import android.text.Editable;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M061MorePasswordPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseEditText;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM061MorePasswordCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.ProgressLoading;
import com.novo.app.view.widget.TextAdapter;

import java.util.Objects;

public class M061MorePasswordFrg extends BaseFragment<M061MorePasswordPresenter, OnM024DoseLogCallBack> implements OnM061MorePasswordCallBack {
    public static final String TAG = M061MorePasswordFrg.class.getName();

    private TextView tvPasswordTitle, tvPasswordError, tvPasswordShow, tvForgotPassword;
    private Button btnUpdatePassword;
    private ImageView ivPasswordError;
    private BaseEditText edPassword;
    private TableRow trError;

    private boolean passCorrected = false;
    private boolean isVerifiedPassword = false;

    private String newPassword;

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m061_back, this);
        findViewById(R.id.tv_m061_title, CAApplication.getInstance().getMediumFont());

        btnUpdatePassword = findViewById(R.id.bt_m061_update, this, CAApplication.getInstance().getMediumFont());
        btnUpdatePassword.setEnabled(false);
        tvPasswordTitle = findViewById(R.id.tv_m061_description, CAApplication.getInstance().getRegularFont());
        tvPasswordShow = findViewById(R.id.tv_m061_show, this, CAApplication.getInstance().getRegularFont());
        ivPasswordError = findViewById(R.id.iv_m061_error, this);
        tvPasswordError = findViewById(R.id.tv_m061_error, CAApplication.getInstance().getRegularFont());
        tvForgotPassword = findViewById(R.id.tv_m061_forgot, this, CAApplication.getInstance().getMediumFont());
        trError = findViewById(R.id.tr_m061_error, this);

        edPassword = findViewById(R.id.tv_m061_password, CAApplication.getInstance().getRegularFont());
        edPassword.setHint(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m061_more_my_account_passwordplacaeholder)));

        onPasswordChange();
        onPasswordFocusChange();
        onKeyBoardDismiss();
    }

    /**
     * Handle app's behaviors when the virtual keyboard is dismiss
     */
    private void onKeyBoardDismiss() {
        edPassword.setOnEditTextImeBackListener((ctrl, text) -> {
            edPassword.clearFocus();
            String passCorrect = getStorage().getM001ProfileEntity().getPassword();
            if (Objects.requireNonNull(edPassword.getText()).toString().equals(passCorrect))
                btnUpdatePassword.setEnabled(true);
        });

        edPassword.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                edPassword.clearFocus();
            }
            return false;
        });
    }

    /**
     * Handle app's behavior when the input password is change
     */
    private void onPasswordChange() {
        edPassword.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                CommonUtil.getInstance().initTypeFace(edPassword);
                if (!editable.toString().equals("")) {
                    validatePass(editable.toString());
                    enableSignIn();
                    showPassError(false);
                }
                if (editable.toString().isEmpty()) {
                    trError.setVisibility(View.GONE);
                    btnUpdatePassword.setEnabled(false);
                }
            }
        });
    }

    /**
     * Handle app's behaviors when the focus of the password field is changed
     */
    private void onPasswordFocusChange() {
        edPassword.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                String passWord = Objects.requireNonNull(edPassword.getText()).toString();
                validatePass(passWord);
            } else {
                showPassError(false);
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m061_more_password;
    }

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected M061MorePasswordPresenter getPresenter() {
        return new M061MorePasswordPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This function doesn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m061_back:
                mCallBack.showChildFrgScreen(TAG, M051MoreFrg.TAG);
                break;

            case R.id.bt_m061_update:
                if (isVerifiedPassword) {
                    // update new password
                    ProgressLoading.show(getActivity());
                    newPassword = edPassword.getText().toString();
                    getPresenter().callingChangePassword(CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME), CommonUtil.getInstance().getPrefContent(CommonUtil.PASS), newPassword);
                } else {
                    // verify password
                    if (edPassword.getText().toString().equals(CommonUtil.getInstance().getPrefContent(CommonUtil.PASS))) {
                        isVerifiedPassword = true;
                        tvPasswordTitle.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m061_more_my_account_password_new_create_a_new_password)));
                        tvPasswordTitle.setTypeface(CAApplication.getInstance().getBoldFont());
                        btnUpdatePassword.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m061_more_my_account_update_button)));
                        trError.setVisibility(View.GONE);
                        edPassword.setText("");
                        tvForgotPassword.setVisibility(View.GONE);
                        hidePassword();
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.txt_m061_current_pass_not_correct), Toast.LENGTH_SHORT).show();
                        CommonUtil.getInstance().showDialog(mContext, getString(R.string.txt_m061_current_pass_not_correct), new OnOKDialogCallBack() {
                        });
                    }
                    edPassword.setTypeface(CAApplication.getInstance().getRegularFont());
                }
                break;

            case R.id.tv_m061_forgot:
                CommonUtil.getInstance().forceHideKeyBoard(edPassword);
                break;

            case R.id.tv_m061_show:
                if (tvPasswordShow.getText().toString().equals(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m023_show)))) {
                    showPassword();
                } else {
                    hidePassword();
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void changePasswordSuccess() {
        CommonUtil.getInstance().savePrefContent(CommonUtil.PASS, newPassword);
        CommonUtil.getInstance().showDialog(mContext,
                LangMgr.getInstance().getLangList().get(getString(R.string.lang_m061_more_my_account_password_success_dialog_message)),
                new OnOKDialogAdapter() {
                    @Override
                    public void handleOKButton1() {
                        mCallBack.showM001LandingScreen();
                    }
                });
    }

    /**
     * Display the password for the user to see
     */
    private void showPassword() {
        edPassword.setInputType(InputType.TYPE_CLASS_TEXT);
        edPassword.setSelection(edPassword.getText().length());
        tvPasswordShow.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m023_hide)));
    }

    /**
     * Hide the password from the user
     */
    private void hidePassword() {
        edPassword.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
        edPassword.setSelection(edPassword.getText().length());
        tvPasswordShow.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m023_show)));
    }

    /**
     * Validate the input password base on the password policy and handle App's behaviors for each case
     *
     * @param pass is the password that the user has inputted
     */
    private void validatePass(String pass) {
        if (pass.isEmpty()) {
            btnUpdatePassword.setEnabled(false);
            showPassError(false);
            passCorrected = false;
            return;
        }
        btnUpdatePassword.setEnabled(false);

        passCorrected = pass.length() >= getStorage().getM001ConfigSet().getMinPass();
        showPassError(!passCorrected);
    }

    /**
     * Display the error message when the inputted password is invalid
     *
     * @param show = true when the error message need to be shown and false if not
     */
    private void showPassError(boolean show) {
        if (show) {
            passCorrected = false;
            trError.setVisibility(View.VISIBLE);
            tvPasswordError.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
            ivPasswordError.setImageResource(R.drawable.ic_m009_error);
        } else {
            trError.setVisibility(View.GONE);
            tvPasswordError.setTextColor(getResources().getColor(R.color.colorBlack));
            ivPasswordError.setImageResource(R.drawable.ic_m061_passed);
        }
    }

    /**
     * Check the validation date of the inputted password to enable the "Update password" button
     */
    private void enableSignIn() {
        if (passCorrected) {
            btnUpdatePassword.setEnabled(true);
            return;
        }
        btnUpdatePassword.setEnabled(false);
    }

    @Override
    public void showDialog() {
        CommonUtil.getInstance().showDialog(mContext, LangMgr.getInstance().getLangList().get(getString(R.string.lang_m061_more_my_account_password_fail_dialog_message)), null);
    }
}
