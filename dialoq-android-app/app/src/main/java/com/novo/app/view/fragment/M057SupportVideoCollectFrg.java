package com.novo.app.view.fragment;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M057SupportVideoCollectPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM057SupportVideoCollectCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

public class M057SupportVideoCollectFrg extends BaseFragment<M057SupportVideoCollectPresenter, OnM024DoseLogCallBack> implements OnM057SupportVideoCollectCallBack {
    public static final String TAG = M057SupportVideoCollectFrg.class.getName();

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m057_back, this);
        findViewById(R.id.tv_m057_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m057_type_here, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.bt_m057_send, this, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m057_cancel, this, CAApplication.getInstance().getBoldFont());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m057_support_video_collect;
    }

    @Override
    protected M057SupportVideoCollectPresenter getPresenter() {
        return new M057SupportVideoCollectPresenter(this);
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showM001LandingScreen();
            }
        }, message);
    }


    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {

    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m057_back:
                mCallBack.showChildFrgScreen(TAG, M051MoreFrg.TAG);
                break;
            case R.id.bt_m057_send:

                break;
            case R.id.tv_m057_cancel:

                break;
            default:
                break;
        }
    }
}
