package com.novo.app.view.event;

/**
 * Created by HungPLD1 on 10/17/2019
 * Callbacks for updating view in when user input email to get confirmation email
 */
public interface OnM080FirstLoginDidntGetCallBack extends OnCallBackToView {
    /**
     * Request View instance redirect the user to the show the M077FirstLoginCode Fragment
     */
    void showM077FirstLoginCode();

    /**
     * Request View instance to proceed to the next screen
     */
    void sendCodeSuccess();

    /**
     * Request View instance to display an alert message
     *
     * @param message is the message should be display
     */
    void sendCodeFailed(String message);
}
