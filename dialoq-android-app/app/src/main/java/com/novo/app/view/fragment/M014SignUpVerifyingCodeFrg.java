package com.novo.app.view.fragment;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M014SignUpPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM014SignUpCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;


public class M014SignUpVerifyingCodeFrg extends BaseFragment<M014SignUpPresenter, OnHomeBackToView> implements OnM014SignUpCallBack {

    public static final String TAG = M014SignUpVerifyingCodeFrg.class.getName();

    @Override
    protected void initViews() {
        findViewById(R.id.pb_m014);
        findViewById(R.id.tv_m014_description, this, CAApplication.getInstance().getRegularFont());
        mPresenter.callUN42ValidateCode(getStorage().getM001ProfileEntity().getVerifyCode());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m014_signup_verifying_code;
    }

    @Override
    protected M014SignUpPresenter getPresenter() {

        return new M014SignUpPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        //This feature isn't require here
    }

    @Override
    public void showM023Login() {
        mCallBack.showFrgScreen(TAG, M023LoginFrg.TAG);
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    public void showM013SignUpCode(String sms) {
        getStorage().setM014VerifyFail(true);
        mCallBack.showFrgScreen(TAG, M013SignUpCodeSentFrg.TAG);
    }

    @Override
    public void callAPISignUpSuccess() {
        getStorage().getM001ProfileEntity().setVerifyCode(null);
        mCallBack.showFrgScreen(TAG, M015SignUpSuccessFrg.TAG);
    }
}
