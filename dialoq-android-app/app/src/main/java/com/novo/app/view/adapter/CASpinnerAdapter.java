package com.novo.app.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.view.event.OnSpinnerCallBack;

import java.util.List;

public class CASpinnerAdapter<T extends OnSpinnerCallBack> extends BaseAdapter {
    private static final int DEFAULT_KEY = -1;
    private List<TextEntity> mData;
    private Context mContext;
    private T mListener;
    private int key;

    public CASpinnerAdapter(int key, List<TextEntity> mData, Context mContext) {
        this.key = key;
        this.mData = mData;
        this.mContext = mContext;
    }

    public void setOnSpinnerCallBack(T event) {
        this.mListener = event;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public TextEntity getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CAHolder holder;
        if (convertView == null) {
            convertView = View.inflate(mContext, R.layout.item_spinner, null);
            holder = new CAHolder();
            holder.mTvText = convertView.findViewById(R.id.tv_spinner);
            holder.mTvText.setTextColor(Color.WHITE);
            convertView.setTag(holder);
        }
        holder = (CAHolder) convertView.getTag();
        TextEntity entity = mData.get(position);

        holder.mTvText.setText(entity.getValue());
        holder.mTvText.setTag(entity);

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        View view = View.inflate(mContext, R.layout.item_spinner, null);
        TextView tvText = view.findViewById(R.id.tv_spinner);
        tvText.setTypeface(CAApplication.getInstance().getRegularFont());
        tvText.setText(mData.get(position).getValue());
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onItemClick(key, position, mData);
                }
            }
        });
        return view;
    }

    private static class CAHolder {
        private TextView mTvText;
    }
}
