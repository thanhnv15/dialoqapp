/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.view.fragment;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M078FirstLoginPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM078FirstLoginCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

/**
 * First Login Verifying Code Fragment
 * <p>
 * This fragment is 'View' object in MVP pattern
 * This fragment uses an {@link M078FirstLoginPresenter} as linked Presenter
 * Callbacks to this fragment will be defined by {@link OnM078FirstLoginCallBack} interface
 * <p>
 * This fragment is used in Reset password features of dialog app, where user has to input
 * email to receive confirmation code
 */
public class M078FirstLoginVerifyingCodeFrg extends BaseFragment<M078FirstLoginPresenter, OnHomeBackToView> implements OnM078FirstLoginCallBack {
    public static final String TAG = M078FirstLoginVerifyingCodeFrg.class.getName();

    /**
     * Initiate views component for First Login Verifying Code Fragment
     *
     * @see BaseFragment#initViews()
     */
    @Override
    protected void initViews() {
        findViewById(R.id.pb_m078);
        findViewById(R.id.tv_m078_description, this, CAApplication.getInstance().getRegularFont());
        mPresenter.callM078ValidateCode(getStorage().getM077VerifyCode());
    }

    /**
     * Get layout id for First Login Verifying Code Fragment
     *
     * @return layout id
     * @see BaseFragment#getLayoutId()
     */
    @Override
    protected int getLayoutId() {
        return R.layout.frg_m078_first_login_verifying_code;
    }

    /**
     * Get presenter for this fragment
     *
     * @return {@link M078FirstLoginPresenter} instance
     * @see BaseFragment#getPresenter()
     */
    @Override
    protected M078FirstLoginPresenter getPresenter() {
        return new M078FirstLoginPresenter(this);
    }

    /**
     * @return tag in {@link String}
     * @see BaseFragment#getTAG()
     */
    @Override
    protected String getTAG() {
        return TAG;
    }

    /**
     * @see BaseFragment#defineBackKey()
     */
    @Override
    protected void defineBackKey() {
        // This feature isn't require here
    }

    /**
     * Handle callback from {@link M078FirstLoginPresenter}
     * {@link OnHomeBackToView#showM001LandingScreen(String err)}
     *
     * @param message error string description
     */
    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    /**
     * Handle callback from {@link OnM078FirstLoginCallBack}
     * {@link OnM078FirstLoginCallBack#verifyCodeFail(String)}
     */
    @Override
    public void verifyCodeFail(String message) {
        mCallBack.showFrgScreen(TAG, M077FirstLoginCodeSentFrg.TAG);
        getStorage().setM078ErrorMessage(message);
    }

    /**
     * Handle callback from {@link OnM078FirstLoginCallBack}
     * {@link OnM078FirstLoginCallBack#verifyCodeSuccess()}
     */
    @Override
    public void verifyCodeSuccess() {
        mCallBack.showFrgScreen(TAG, M079FirstLoginSuccessFrg.TAG);
        getStorage().getM001ProfileEntity().setVerifyCode(null);
        getStorage().setM077VerifyCode(null);
        getStorage().setM078ErrorMessage(null);
    }

    /**
     * Handle callback from {@link OnM078FirstLoginCallBack}
     * {@link OnM078FirstLoginCallBack#showAlertDialog(String)}
     */
    @Override
    public void showAlertDialog(String txtErr) {
        CommonUtil.getInstance().showDialog(mContext, txtErr, new OnOKDialogCallBack() {
            @Override
            public void handleDialogDismiss() {
                mCallBack.showFrgScreen(TAG, M077FirstLoginCodeSentFrg.TAG);
            }
        });
    }
}
