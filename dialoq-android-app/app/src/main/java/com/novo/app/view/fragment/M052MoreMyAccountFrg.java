package com.novo.app.view.fragment;

import android.content.Intent;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M052MorePresenter;
import com.novo.app.service.SyncScheduleService;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM052MoreCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;

public class M052MoreMyAccountFrg extends BaseFragment<M052MorePresenter, OnM024DoseLogCallBack> implements OnM052MoreCallBack, OnCallBackToView {
    public static final String TAG = M052MoreMyAccountFrg.class.getName();

    @Override
    public void showM001LandingScreen(String message) {
       mCallBack.showM001LandingScreen();
    }

    @Override
    protected void initViews() {
        mCallBack.changeTab(CommonUtil.TAB_MENU_MORE);
        findViewById(R.id.tv_m052_title, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m052_login_info, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m052_profile_info, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m052_manage, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m052_verified, CAApplication.getInstance().getBoldFont());

        findViewById(R.id.tv_m052_email, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m052_password, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m052_about_me, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m052_delete_account, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m052_product_improvement, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m052_communication, this, CAApplication.getInstance().getRegularFont());

        findViewById(R.id.iv_m052_email, this);
        findViewById(R.id.iv_m052_password, this);
        findViewById(R.id.iv_m052_about_me, this);
        findViewById(R.id.iv_m052_delete_account, this);
        findViewById(R.id.iv_m052_product_improvement, this);
        findViewById(R.id.iv_m052_communication, this);
        findViewById(R.id.iv_m052_back, this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m052_more_my_account;
    }

    @Override
    protected M052MorePresenter getPresenter() {
        return new M052MorePresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {

    }

    @Override
    public void backToPreviousScreen() {
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m052_back:
                mCallBack.showChildFrgScreen(TAG, M051MoreFrg.TAG);
                break;
            case R.id.tv_m052_email:
            case R.id.iv_m052_email:
            case R.id.tv_m052_password:
            case R.id.iv_m052_password:
                mCallBack.showChildFrgScreen(TAG, M061MorePasswordFrg.TAG);
                break;

            case R.id.tv_m052_delete_account:
            case R.id.iv_m052_delete_account:
                CommonUtil.getInstance().showDialog(mContext,
                        LangMgr.getInstance().getLangList().get(getString(R.string.lang_m052_more_my_account_delete_account_dialog_title)),
                        LangMgr.getInstance().getLangList().get(getString(R.string.lang_m052_more_my_account_delete_account_dialog_message)),
                        LangMgr.getInstance().getLangList().get(getString(R.string.lang_m052_more_my_account_delete_account_dialog_sure)),
                        LangMgr.getInstance().getLangList().get(getString(R.string.lang_m052_more_my_account_delete_account_dialog_cancel)),
                        new OnOKDialogCallBack() {
                    @Override
                    public void handleOKButton1() {
                        getStorage().getM007UserEntity().setState(CommonUtil.INACTIVE);
                        mPresenter.callDA01DeleteUserAccount();
                    }
                });
                break;

            case R.id.tv_m052_product_improvement:
            case R.id.iv_m052_product_improvement:
                mCallBack.showChildFrgScreen(TAG, M059MoreProductFrg.TAG);
                break;

            case R.id.tv_m052_communication:
            case R.id.iv_m052_communication:
                mCallBack.showChildFrgScreen(TAG, M058MoreCommunicationFrg.TAG);
                break;

            case R.id.tv_m052_about_me:
            case R.id.iv_m052_about_me:
                mCallBack.showChildFrgScreen(TAG, M053MoreAboutMeFrg.TAG);
                break;
        }
    }

    @Override
    public void toLandingScreen() {
        CommonUtil.getInstance().clearPrefContent(CommonUtil.KEEP);
        CommonUtil.getInstance().clearPrefContent(CommonUtil.USER_NAME);
        CommonUtil.getInstance().clearPrefContent(CommonUtil.PASS);
        mCallBack.showM001LandingScreen();
    }

    @Override
    public void showLoginScreen() {
        mCallBack.showM023LoginScreen();
    }

    @Override
    public void stopService() {
        mContext.stopService(new Intent(mContext, SyncScheduleService.class));
    }
}
