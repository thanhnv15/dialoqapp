/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.view.fragment;

import android.text.Editable;
import android.text.InputFilter;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M039RecoveryPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseEditText;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM039RecoveryCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.TextAdapter;

/**
 * Recovery Code Sent Fragment
 * <p>
 * This fragment is 'View' object in MVP pattern
 * This fragment uses an {@link M039RecoveryPresenter} as linked Presenter
 * Callbacks to this fragment will be defined by {@link OnM039RecoveryCallBack} interface
 * <p>
 * This fragment is used in Reset password features of dialog app, where user has to input
 * email to receive confirmation code
 */
public class M039RecoveryCodeSentFrg extends BaseFragment<M039RecoveryPresenter, OnHomeBackToView> implements OnM039RecoveryCallBack {
    public static final String TAG = M039RecoveryCodeSentFrg.class.getName();

    // View items used in Recovery Email Fragment
    private BaseEditText mEdtVerificationCode;
    private Button mBtVerify;
    private TableRow mTrError;
    private TextView mTvCenterLine, mTvTopLine;

    // Minimum length of a valid confirmation code
    private int codeLength;

    /**
     * Handle callback from {@link M039RecoveryPresenter}
     * {@link OnHomeBackToView#showM001LandingScreen(String err)}
     *
     * @param message error string description
     */
    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    /**
     * Initiate views component for Recovery Email Fragment
     *
     * @see BaseFragment#initViews()
     */
    @Override
    protected void initViews() {
        codeLength = Integer.parseInt(getStorage().getM001ConfigSet().getConfirmCode());
        findViewById(R.id.tv_m039_code_sent, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m039_description, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m039_error, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m039_didnt_get, this, CAApplication.getInstance().getBoldFont());

        mTvCenterLine = findViewById(R.id.tv_m039_line_center);
        mTvTopLine = findViewById(R.id.tv_m039_line_top);

        mBtVerify = findViewById(R.id.bt_m039_verify, this, CAApplication.getInstance().getBoldFont());
        mBtVerify.setEnabled(false);

        mTrError = findViewById(R.id.tr_m039_error);
        mTrError.setVisibility(View.GONE);

        mEdtVerificationCode = findViewById(R.id.tv_m039_detail, CAApplication.getInstance().getRegularFont());
        mEdtVerificationCode.setFilters(new InputFilter[]{new InputFilter.LengthFilter(codeLength)});
        mEdtVerificationCode.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                String code = editable.toString();
                CommonUtil.getInstance().initTypeFace(mEdtVerificationCode);
                if (code.isEmpty() || code.length() < codeLength) {
                    mTrError.setVisibility(View.GONE);
                    mBtVerify.setEnabled(false);
                } else {
                    mBtVerify.setEnabled(true);
                }
            }
        });

        if (getStorage().isM039VerifyFail()) {
            getStorage().setM039VerifyFail(false);
            mTrError.setVisibility(View.VISIBLE);
            mEdtVerificationCode.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
        }
        onKeyBoardDismiss();
        onTextViewFocusChange();
    }

    /**
     * Reset layout when keyboard dismissed
     */
    private void onKeyBoardDismiss() {
        mEdtVerificationCode.setOnEditTextImeBackListener((ctrl, text) -> {
            mEdtVerificationCode.clearFocus();
            showLine(true);
            validateCode();
        });

        mEdtVerificationCode.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEdtVerificationCode.clearFocus();
                showLine(true);
                validateCode();
            }
            return false;
        });
    }

    /**
     * Reset layout when the focus of {@link #mEdtVerificationCode} changed
     */
    private void onTextViewFocusChange() {
        mEdtVerificationCode.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                showLine(true);
                CommonUtil.getInstance().forceHideKeyBoard(mEdtVerificationCode);
            } else {
                showLine(false);
            }
        });
    }

    /**
     * Change the distance between views so every view component can be visible when showing keyboard
     *
     * @param show true if the keyboard is not showing
     */
    private void showLine(boolean show) {
        if (show) {
            mTvCenterLine.setVisibility(View.VISIBLE);
            mTvTopLine.setVisibility(View.VISIBLE);
        } else {
            mTvCenterLine.setVisibility(View.GONE);
            mTvTopLine.setVisibility(View.GONE);
        }
    }

    /**
     * Check if typed in confirmation code is valid or not
     * Also update UI showing invalid confirmation code if necessary
     */
    private void validateCode() {
        if (mEdtVerificationCode.getText().toString().isEmpty() || mEdtVerificationCode.getText().toString().length() < codeLength) {
            mEdtVerificationCode.setTextColor(getResources().getColor(R.color.colorBlack));
            mTrError.setVisibility(View.VISIBLE);
            mBtVerify.setEnabled(false);
            return;
        }

        if (mEdtVerificationCode.getText().length() == codeLength) {
            mEdtVerificationCode.setTextColor(getResources().getColor(R.color.colorBlack));
            mTrError.setVisibility(View.GONE);
        } else {
            mEdtVerificationCode.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
            mTrError.setVisibility(View.VISIBLE);
        }
        mBtVerify.setEnabled(true);
    }

    /**
     * Get layout id for Recovery Email Fragment
     *
     * @return layout id
     * @see BaseFragment#getLayoutId()
     */
    @Override
    protected int getLayoutId() {
        return R.layout.frg_m039_recovery_code_sent;
    }

    /**
     * Get presenter for this fragment
     *
     * @return {@link M039RecoveryPresenter} instance
     * @see BaseFragment#getPresenter()
     */
    @Override
    protected M039RecoveryPresenter getPresenter() {
        return new M039RecoveryPresenter(this);
    }

    /**
     * @return tag in {@link String}
     * @see BaseFragment#getTAG()
     */
    @Override
    protected String getTAG() {
        return TAG;
    }

    /**
     * @see BaseFragment#defineBackKey()
     */
    @Override
    protected void defineBackKey() {
        //Wait for customer confirmation
    }

    /**
     * Handle callback from {@link OnM039RecoveryCallBack}
     * {@link OnM039RecoveryCallBack#onIA04Fail(String sms)}
     *
     * @param message is the message to show in the dialog
     */
    @Override
    public void onIA04Fail(String message) {
        CommonUtil.getInstance().showDialog(mContext, message, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton1() {
            }
        });
        mBtVerify.setEnabled(false);
    }

    /**
     * Handle callback from {@link OnM039RecoveryCallBack}
     * {@link OnM039RecoveryCallBack#showM040RecoveryPasswordScreen()}
     */
    @Override
    public void showM040RecoveryPasswordScreen() {
        mCallBack.showFrgScreen(TAG, M040RecoveryPasswordFrg.TAG);
    }

    /**
     * @param idView id of clicked view
     * @see BaseFragment#onClickView(int)
     */
    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m039_verify:
                mEdtVerificationCode.clearFocus();
                getStorage().setM039Verify(mEdtVerificationCode.getText().toString());
                mPresenter.callIA04ResetPasswordStep2(getStorage().getM038UserName(), getStorage().getM039VerifyCode());
                break;

            case R.id.tv_m039_didnt_get:
                mEdtVerificationCode.clearFocus();
                mCallBack.showFrgScreen(TAG, M038RecoveryEmailFrg.TAG);
                break;

            default:
                break;
        }
    }

}
