/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.hardware.fingerprint.FingerprintManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.FileProvider;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.entities.RReminderEntity;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.DoseTimingEntity;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.service.ReminderReceiver;
import com.novo.app.view.activity.HomeActivity;
import com.novo.app.view.adapter.DoseLogAdapter;
import com.novo.app.view.event.OnActionCallBack;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.fragment.M024DoseLogFrg;
import com.novo.app.view.widget.NVDateFormat;
import com.novo.app.view.widget.NVDateUtils;
import com.novo.app.view.widget.OnOKDialogAdapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static com.novo.app.presenter.M025SubDoseLogPresenter.KEY_SAVE_CALLING_AS5;
import static com.novo.app.service.SyncScheduleService.KEY_SYNC_READY;
import static com.novo.app.view.dialog.M065MoreReminderAddDialog.EMPTY;
import static com.novo.app.view.dialog.M065MoreReminderAddDialog.EXISTED;

/**
 * Created by ThanhNv on 7/4/2017.
 */
public class CommonUtil {
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String VALUE_CONTENT_TYPE = "application/json";
    public static final String TOKEN_KEY = "access_token";
    public static final String AUTHOR = "Authorization";
    public static final String SUCCESS_VALUE = "true";
    public static final String DATE_NOW_DY = "yyyy-MM-dd";
    public static final String DATE_TIME_NOW_DY = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_STYLE = "yyyy-MM-dd'T'HH:mm:ssZ";
    public static final String TIME_STYLE = "HH:mm";

    public static final String HOT_URL = "assessment-service";

    public static final String INACTIVE = "inactive";
    public static final int AGREE_VALUE = 1;
    public static final int NOT_AGREE_VALUE = 0;
    public static final String GENDER_DEFAULT = "male";
    public static final String LANG_KEY = "lang_key";
    public static final String LANG_VALUE = "lang_value";
    public static final TextEntity DEFAULT_LANG = new TextEntity("en", "English");
    public static final String IS_LOGIN = "IS_LOGIN";
    public static final String IS_LOGIN_TRUE = "LOGIN_TRUE";
    public static final String IS_LOGIN_FALSE = "LOGIN_FALSE";
    public static final String USER_NAME = "USER_NAME";
    public static final String PASS = "PASS";
    public static final String KEEP = "REMEMBERED";
    public static final int BLACK_LIST = 0;
    public static final int RECALL_VERSION = 2;
    public static final int RECALL_COUNTRY = 3;
    public static final int RECALL_COUNTRY_VERSION = 4;
    public static final int GREY_LIST = 1;
    public static final int WHITE_LIST = -1;
    public static final int CODE_204 = 204;
    public static final String ERROR_500 = "500";
    public static final String ERROR_400 = "400";
    public static final String ERROR_401 = "401";
    public static final String ERROR_403 = "403";
    public static final String ERROR_ERR_401 = "ERR_401";
    public static final int TAB_MENU_DOSE_LOG = 0;
    public static final int TAB_MENU_SUMMARY = 1;
    public static final int TAB_MENU_SHARE = 2;
    public static final int TAB_MENU_SUPPORT = 3;
    public static final int TAB_MENU_MORE = 4;
    public static final int COLOR_TYPE_TRESIBA = -3547901;
    public static final int COLOR_TYPE_TRESIBA_200 = -9202176;
    public static final int COLOR_TYPE_FIASP = -1688529;
    public static final int COLOR_TYPE_NO_DOSE = -1788529;

    public static final String DEVICE_ASSESSMENT_TYPE = "Devices";
    public static final String INJECTION_ASSESSMENT_TYPE = "Injections";
    public static final String NOTES_ASSESSMENT_TYPE = "Notes";
    public static final String OTHERS_ASSESSMENT_TYPE = "Others";

    public static final String REMINDER_HOUR = "REMINDER_HOUR";
    public static final String REMINDER_MIN = "REMINDER_MIN";
    public static final String REMINDER_TIME_SET = "REMINDER_TIME_SET";
    public static final String REMINDER_TODAY = "REMINDER_TODAY";
    public static final String REMINDER_SET = "REMINDER_SET";
    public static final String REMINDER_EXIST = "REMINDER_EXIST";
    public static final String REMINDER_INACTIVE = "[EMAIL]";
    public static final String REMINDER_ACTIVE = "[PUSH_APPLICATION]";

    public static final String P_API_KEY = "apikey";
    public static final String ASSESSMENT_TYPE_NOTE = "Notes";
    public static final String ERR_UNKNOWN_REASON = "ERR_500";
    public static final int CODE_401 = 401;
    public static final String PREF_TIMEZONE = "PREF_TIMEZONE";
    public static final String TRUE_CASE = "true";

    public static final String RECALL_GREATER = "greater";
    public static final String RECALL_GREATER_OR_EQUAL = "greater.or.equal";
    public static final String RECALL_LESS_OR_EQUAL = "less.or.equal";
    public static final String RECALL_LESS = "less";
    public static final String RECALL_EQUAL = "equal";
    public static final String LAST_TRESIBA_DOSE = "LAST_TRESIBA_DOSE";
    public static final String KEY_NOTIFY = "KEY_NOTIFY";
    public static final String KEY_NOTIFY_7_DAY = "KEY_NOTIFY_7_DAY";
    public static final String KEY_NOTIFY_14_DAY = "KEY_NOTIFY_14_DAY";
    public static final String KEY_NOTIFY_14_DAY_LAST_DOSE = "KEY_NOTIFY_14_DAY_LAST_DOSE";
    public static final String KEY_IFU_URL = "KEY_IFU_URL";
    public static final String KEY_IFU_PATH = "KEY_IFU_PATH";
    public static final String KEY_INTRO_URL = "KEY_INTRO_URL";
    public static final String KEY_INTRO_PATH = "KEY_INTRO_PATH";
    public static final boolean IN_CASE_NOT_OFFLINE_MODE = false;
    public static final boolean IN_CASE_OFFLINE_MODE = true;
    public static final int HOUR_REMINDER = 9;
    public static final int MINUTE_REMINDER = 0;
    public static final int SECOND_REMINDER = 0;
    public static final String LOGIN_SECTION_START_DATE = "LOGIN_SECTION_START_DATE";
    public static final String LOGIN_SECTION_DO_NOT_KEEP = "NONE";
    public static final String KEY_RETRYING_EXPIRE_REQUEST = "KEY_RETRYING_EXPIRE_REQUEST";
    private static final int NOTIFICATION_ID_MIN = 0;
    private static final int NOTIFICATION_ID_MAX = 1;
    private static final String TAG = CommonUtil.class.getName();
    private static final String UNDEFINE_TAG = "UNDEFINE_TAG";
    private static final String PREF_SAVE_NAME = "ca_pref_saved";
    private static final String IMAGE_SPAN = "${battery}";
    private static final int ALARM_REQUEST_CODE = 1590;
    private static final int ALARM_REQUEST_CODE_LAST_DOSE = 1595;
    private static final int ALARM_REQUEST_NOTIFICATION = 15900;
    private static final String PATH = "PATH: ";
    private static final String DEFAULT = "default";
    public static String BASE_URL = "https://flexdigitalhealth-dev.apigee.net/dev1-dialoq-product/";
    public static LOG_TYPE mDebugType = LOG_TYPE.TEST;
    private static CommonUtil instance;
    private Map<String, String> mBackFlow;
    private String mApiKey;

    // Background level indicating that user typed in a valid input string in a EditText view
    public static final int CODE_COLOR_ERROR = 1;
    public static final int CODE_COLOR_NORMAL = 0;

    /**
     * CommonUtil
     * private constructor for singleton
     */
    private CommonUtil() {
        mBackFlow = new HashMap<>();
    }

    /**
     * getInstance
     * return instance for singleton pattern
     *
     * @return instance
     */
    public static CommonUtil getInstance() {
        if (instance == null) {
            instance = new CommonUtil();
        }
        return instance;
    }

    public static void wtfi(String tag, String text) {
        if (text == null) return;
        if (mDebugType == LOG_TYPE.INFO || mDebugType == LOG_TYPE.DEBUG) {
            Log.i(tag, text);
        } else {
            System.out.println(tag + ":" + text);
        }
    }

    public static void wtfe(String tag, String text) {
        if (text == null) return;
        if (mDebugType == LOG_TYPE.INFO || mDebugType == LOG_TYPE.DEBUG) {
            Log.e(tag, text);
        } else {
            System.out.println(tag + ":" + text);
        }
    }

    public static void wtfd(String tag, String text) {
        if (text == null) return;
        if (mDebugType == LOG_TYPE.INFO || mDebugType == LOG_TYPE.DEBUG) {
            Log.d(tag, text);
        } else {
            System.out.println(tag + ":" + text);
        }
    }

    public static <T> T generateData(Class<T> clazz, String data) {
        try {
            Gson gson = new Gson();
            return gson.fromJson(data, clazz);
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
        }
        return null;
    }

    public static boolean isNotEmail(String input) {
        if (input.contains("@")) {
            String[] tmp = input.split("@");
            if (tmp.length < 2) {
                return true;
            }
            if (tmp[0].length() > 64)
                return true;
        }
        if (input.length() > 100)
            return true;
        return !input.matches("^([a-zA-Z0-9í'_+\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1," +
                "3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,24}|[0-9]{1,3})(\\]?)(;" +
                "([a-zA-Z0-9í'_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(" +
                "([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,24}|[0-9]{1,3})(\\]?))*$");
    }

    public static String getDateNow(String dateStyle) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(dateStyle, Locale.getDefault());
            dateFormat.setTimeZone(TimeZone.getDefault());
            return dateFormat.format(new Date(System.currentTimeMillis()));
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
        }
        return null;
    }

    public static String getZDateNow() {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("ZZZZ", Locale.getDefault());
            dateFormat.setTimeZone(TimeZone.getDefault());
            return dateFormat.format(new Date(System.currentTimeMillis()));
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
        }
        return null;
    }

    public static String getDateNow(String dateStyle, int nextDays) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(dateStyle, Locale.getDefault());
            Date dt = new Date(System.currentTimeMillis());
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            c.add(Calendar.DAY_OF_YEAR, nextDays);
            return dateFormat.format(c.getTime().getTime());
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
        }
        return null;
    }

    public static long getDateNowInt(int nextDays) {
        try {
            Date dt = new Date(System.currentTimeMillis());
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            c.add(Calendar.DAY_OF_YEAR, nextDays);
            return c.getTime().getTime();
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
        }
        return -1;
    }

    public static long getDateNowInt(String dateStyle, String time, int nextDays) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(dateStyle, Locale.getDefault());
            dateFormat.setTimeZone(TimeZone.getDefault());
            Date dt = dateFormat.parse(time);
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            c.add(Calendar.DAY_OF_YEAR, nextDays);
            return c.getTime().getTime();
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
        }
        return -1;
    }

    public static String getDateNow(String dateStyle, String time, int nextDays) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(dateStyle, Locale.getDefault());
            dateFormat.setTimeZone(TimeZone.getDefault());
            Date dt = dateFormat.parse(time);
            Calendar c = Calendar.getInstance();
            c.setTime(dt);
            c.add(Calendar.DAY_OF_YEAR, nextDays);
            return dateFormat.format(c.getTime().getTime());
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
        }
        return null;
    }

    public static long getDateAfter(long time, int nextDays) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(time));
        c.add(Calendar.DAY_OF_YEAR, nextDays);
        return c.getTime().getTime();
    }

    public static int getInt(String value) {
        return (int) getDouble(value);
    }

    private static double getDouble(String value) {
        try {
            return Double.parseDouble(value);
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
        }
        return 0;
    }

    public static String convertShortDate(String date) {
        if (date.contains("T")) {
            date = date.substring(0, date.indexOf("T"));
        } else if (date.contains("+")) {
            date = date.substring(0, date.indexOf("+"));
        } else if (date.contains("-")) {
            date = date.substring(0, date.indexOf("-"));
        }
        return date;
    }

    public static String convertShortTime(String date) {
        if (date != null) {
            if (date.contains("T")) {
                if (date.contains("Z")) {
                    date = date.substring(date.indexOf("T") + 1, date.indexOf("Z") - 3);
                } else if (date.contains("+")) {
                    date = date.substring(date.indexOf("T") + 1, date.indexOf("+") - 3);
                } else if (date.contains("-")) {
                    date = date.substring(date.indexOf("T") + 1, date.lastIndexOf("-") - 3);
                }
            }
        }
        return date;
    }

    /**
     * Change the date string to new date format (yyyy-MM-dd'T'HH:mm:ssZ)
     *
     * @param date String
     * @return new converted date string
     */
    public static String convertToLocalDate(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_STYLE, Locale.getDefault());
        try {
            simpleDateFormat.setTimeZone(TimeZone.getDefault());
            Date newDate = simpleDateFormat.parse(date);
            return simpleDateFormat.format(newDate);
        } catch (Exception e) {
            return date;
        }
    }

    @SuppressLint("SimpleDateFormat")
    public static String toLocalTime(String time) {
        try {
            DateFormat utcFormat = new SimpleDateFormat(TIME_STYLE);
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = utcFormat.parse(time);
            DateFormat deviceFormat = new SimpleDateFormat(TIME_STYLE);
            deviceFormat.setTimeZone(TimeZone.getDefault()); //Device timezone

            return deviceFormat.format(date);

        } catch (Exception e) {
            return null;
        }
    }

    public static int getMinute(String strDate, int minHour) {
        SimpleDateFormat sf = new SimpleDateFormat(DATE_STYLE, Locale.getDefault());
        try {
            Date date = sf.parse(strDate);
            Calendar c = Calendar.getInstance();
            c.setTime(date);

            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            if (hour < minHour) {
                hour = hour + 24;
            }
            return hour * 60 + minute;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Convert activity date, report date of each DataInfo from UTC time zone to local time zone
     *
     * @param listDataInfo ArrayList<DataInfo>
     */
    public static void convertToLocalDateData(ArrayList<DataInfo> listDataInfo) {
        for (int i = 0; i < listDataInfo.size(); i++) {
            String actDate = listDataInfo.get(i).getActivityDate();
            listDataInfo.get(i).setActivityDate(CommonUtil.convertToLocalDate(actDate));
            if (listDataInfo.get(i).getResult() != null) {
                String reportDate = listDataInfo.get(i).getResult().getReportedDate();
                listDataInfo.get(i).getResult().setReportedDate(CommonUtil.convertToLocalDate(reportDate));
            }
        }
    }

    public static String getSDate(long date, String dateStyle) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(dateStyle, Locale.getDefault());
            return dateFormat.format(new Date(date));
        } catch (Exception e) {
            CommonUtil.wtfe(TAG, e.getLocalizedMessage());
        }
        return null;
    }

    private static String formatDate(int year, int month, int day) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(year, month, day);
        Date date = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

        return sdf.format(date);
    }

    /**
     * Base on the status flag being store at custom3 and custom4 field of an Assessment result
     * (Assessment type must be Injections) to decide if it is an error dose with unknown drug type or not
     *
     * @param custom3 is the drug's code of that log
     * @param custom4 is the status flag code that log
     * @return true if the combination of custom3 and custom4 is belong to an error dose or false if not
     */
    public static boolean isUnknownDrug(String custom3, String custom4) {
        if (custom4 != null
                && (custom4.equals(DeviceEntity.DRUG_MISSING)
                || custom4.equals(DeviceEntity.EOL_WARNING_DRUG_MISSING + ""))) return true;

        switch (custom3) {
            case DeviceEntity.CODE_TRESIBA_100 + "":
            case DeviceEntity.CODE_TRESIBA_200 + "":
            case DeviceEntity.CODE_FIASP + "":
                return false;
        }
        return true;
    }

    /**
     * Base on the status flag being store at custom4 and result field of an Assessment result
     * (Assessment type must be Injections) to decide if it is an error dose with unknown dose size or not
     *
     * @param result  is the dose size of that log
     * @param custom4 is the status flag code that log
     * @return true if the combination of result and custom4 is belong to an error dose or false if not
     */
    public static boolean isUnknownDoseSize(long result, String custom4) {
        if (custom4 != null && (custom4.equals(DeviceEntity.DOSE_MISSING)
                || custom4.equals(DeviceEntity.EOL_WARNING_DOSE_MISSING + ""))) return true;
        return result == 0;
    }

    public void showNotify(int smsErr) {
        Toast.makeText(CAApplication.getInstance(), smsErr, Toast.LENGTH_SHORT).show();
    }

    public final StorageCommon getStorage() {
        return CAApplication.getInstance().getStorageCommon();
    }

    public void insertImage(TextView tv, int imageId) {
        String text = tv.getText().toString();
        if (!text.contains(IMAGE_SPAN)) return;
        int lineHeight = tv.getLineHeight();
        Drawable drawable = CAApplication.getInstance().getDrawable(imageId);
        if (drawable == null) return;

        int oldHeight = drawable.getIntrinsicHeight();
        int newWidth = (int) (drawable.getIntrinsicWidth() * 1F * lineHeight / oldHeight);
        drawable.setBounds(0, 0, newWidth, lineHeight);

        ImageSpan imageSpan = new ImageSpan(drawable);
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(text);
        ssBuilder.setSpan(
                imageSpan,
                text.indexOf(CommonUtil.IMAGE_SPAN),
                text.indexOf(CommonUtil.IMAGE_SPAN) + CommonUtil.IMAGE_SPAN.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        tv.setText(ssBuilder);
    }

    public void defineBackTag(String backTag, String key) {
        mBackFlow.put(key, backTag);
    }

    public void savePrefContent(Context context, String key, String text) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_SAVE_NAME, MODE_PRIVATE).edit();

        editor.putString(key, text);
        editor.apply();
    }

    public void savePrefContent(String key, int value) {
        savePrefContent(CAApplication.getInstance(), key, value + "");
    }

    public void savePrefContent(String key, String text) {
        savePrefContent(CAApplication.getInstance(), key, text);
    }

    public String getPrefContent(String key) {
        return getPrefContent(key, false);
    }

    public String getPrefContent(String key, boolean isDeleted) {
        String result = CAApplication.getInstance().getSharedPreferences(PREF_SAVE_NAME, MODE_PRIVATE).getString(key, null);
        if (result != null && isDeleted) {
            SharedPreferences.Editor editor = CAApplication.getInstance()
                    .getSharedPreferences(PREF_SAVE_NAME, MODE_PRIVATE).edit();
            editor.remove(key);
            editor.apply();
        }
        return result;
    }

    public int getIntPrefContent(String key, boolean isDeleted, int defaultVL) {
        String vl = getPrefContent(key, isDeleted);
        if (vl == null) return defaultVL;
        try {
            return Integer.parseInt(vl);
        } catch (Exception ignored) {
        }
        return defaultVL;
    }

    public long getLongPrefContent(String key, boolean isDeleted, int defaultVL) {
        String vl = getPrefContent(key, isDeleted);
        if (vl == null) return defaultVL;
        try {
            return Long.parseLong(vl);
        } catch (Exception ignored) {
        }
        return defaultVL;
    }

    public int getIntPrefContent(String key, int defaultVL) {
        return getIntPrefContent(key, false, defaultVL);
    }

    public long getLongPrefContent(String key, int defaultVL) {
        return getLongPrefContent(key, false, defaultVL);
    }

    public String doBackFlowWithBackTag(String key) {
        CommonUtil.wtfi(TAG, "doBackFlowWithBackTag " + mBackFlow.toString());
        if (mBackFlow.containsKey(key)) {
            String backTag = mBackFlow.get(key);
            mBackFlow.remove(key);
            return backTag;
        }
        return UNDEFINE_TAG;
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    public void showDialog(Context context, String title, int message, int bt1Name, int
            bt2Name, final OnOKDialogCallBack onOKDialogCallBack) {

        AlertDialog mAlert = new AlertDialog.Builder(context).create();
        doKeepDialog(mAlert);
        mAlert.setTitle(title);
        mAlert.setMessage(CAApplication.getInstance().getString(message));
        if (bt1Name > -1) {
            mAlert.setButton(AlertDialog.BUTTON_POSITIVE, CAApplication.getInstance().getString(bt1Name), (dialogInterface, i) -> {
                if (onOKDialogCallBack == null) return;
                onOKDialogCallBack.handleOKButton1();
            });
        }
        if (bt2Name > -1) {
            mAlert.setButton(AlertDialog.BUTTON_NEGATIVE, CAApplication.getInstance().getString(bt2Name), (dialogInterface, i) -> {
                if (onOKDialogCallBack == null) return;
                onOKDialogCallBack.handleOKButton2();
            });
        }
        mAlert.setOnDismissListener(dialog -> {
            if (onOKDialogCallBack == null) return;
            onOKDialogCallBack.handleDialogDismiss();
        });
        mAlert.show();
        fixDialogTextSize(mAlert, context);
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    public void showDialog(Context context, int title, int message, int bt1Name, int
            bt2Name, final OnOKDialogCallBack onOKDialogCallBack) {
        showDialog(context, CAApplication.getInstance().getString(title), message, bt1Name, bt2Name, onOKDialogCallBack);
    }

    public long getFreeSpace() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
        long megAvailable = bytesAvailable / (1024 * 1024);

        CommonUtil.wtfd(TAG, "Available MB : " + megAvailable);
        return megAvailable;
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    public void showDialog(Context context, String title, String message, String bt1Name, String
            bt2Name, final OnOKDialogCallBack onOKDialogCallBack) {
        AlertDialog mAlert = new AlertDialog.Builder(context).create();
        mAlert.setCancelable(false);
        mAlert.setCanceledOnTouchOutside(false);
        doKeepDialog(mAlert);
        mAlert.setTitle(title);
        mAlert.setMessage(message);

        if (bt1Name != null) {
            mAlert.setButton(AlertDialog.BUTTON_POSITIVE, bt1Name, (dialogInterface, i) -> {
                if (onOKDialogCallBack == null) return;
                onOKDialogCallBack.handleOKButton1();
            });
        }
        if (bt2Name != null) {
            mAlert.setButton(AlertDialog.BUTTON_NEGATIVE, bt2Name, (dialogInterface, i) -> {
                if (onOKDialogCallBack == null) return;
                onOKDialogCallBack.handleOKButton2();
            });
        }
        mAlert.setOnDismissListener(dialog -> {
            if (onOKDialogCallBack == null) return;
            onOKDialogCallBack.handleDialogDismiss();
        });
        mAlert.show();
        fixDialogTextSize(mAlert, context);
    }

    private void fixDialogTextSize(AlertDialog mAlert, Context context) {
        Objects.requireNonNull(mAlert.getWindow()).getAttributes();
        TextView msg = mAlert.findViewById(android.R.id.message);
        TextView title = mAlert.findViewById(context.getResources().getIdentifier("alertTitle", "id", "android"));
        Button bt1 = mAlert.getButton(DialogInterface.BUTTON_POSITIVE);
        Button bt2 = mAlert.getButton(DialogInterface.BUTTON_NEGATIVE);

        if (title != null)
            title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        if (msg != null)
            msg.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        if (bt1 != null)
            bt1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        if (bt2 != null)
            bt2.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    public void showDialog(Context context, int message) {
        showDialog(context, message, null);
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    public void showDialog(Context context, int message, final OnOKDialogCallBack onOKDialogCallBack) {
        AlertDialog mAlert = new AlertDialog.Builder(context).create();
        doKeepDialog(mAlert);
        mAlert.setTitle(R.string.app_name);
        mAlert.setMessage(CAApplication.getInstance().getString(message));
        mAlert.setButton(AlertDialog.BUTTON_POSITIVE, CAApplication.getInstance().getText(R.string.txt_confirm_got_it), (dialogInterface, i) -> {
            if (onOKDialogCallBack == null) return;
            onOKDialogCallBack.handleOKButton1();
        });
        mAlert.show();
        fixDialogTextSize(mAlert, context);
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    public AlertDialog showDialog(Context context, String message) {
        return showDialog(context, message, null);
    }

    public AlertDialog showDialog(Context context, String message, final OnOKDialogCallBack onOKDialogCallBack) {
        AlertDialog mAlert = new AlertDialog.Builder(context).create();
        doKeepDialog(mAlert);
        mAlert.setTitle(R.string.app_name);
        mAlert.setMessage(message);
        mAlert.setButton(AlertDialog.BUTTON_POSITIVE, CAApplication.getInstance().getText(R.string.txt_confirm_got_it), (dialogInterface, i) -> {
            if (onOKDialogCallBack == null) return;
            onOKDialogCallBack.handleOKButton1();
        });

        if (context instanceof Activity) {
            if (((Activity) context).isDestroyed()) return null;
        }
        mAlert.setOnDismissListener(dialog -> {
            if (onOKDialogCallBack == null) return;
            onOKDialogCallBack.handleDialogDismiss();
        });
        mAlert.show();
        fixDialogTextSize(mAlert, context);
        return mAlert;
    }

    // Prevent dialog dismiss when orientation changes
    public void doKeepDialog(Dialog dialog) {
        if (dialog.getWindow() == null) return;
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
    }

    public void clearPrefContent(String key) {
        SharedPreferences.Editor editor = CAApplication.getInstance()
                .getSharedPreferences(PREF_SAVE_NAME, MODE_PRIVATE).edit();
        editor.remove(key);
        editor.apply();
    }

    public boolean isBluetoothOn() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            return false;
        } else {
            return mBluetoothAdapter.isEnabled();
        }
    }

    public void showDateChoosing(Context mContext, TextView mTvDate, String dob, OnCallBackToView mCallBack) {
        final Calendar c = Calendar.getInstance();

        if (dob != null) {
            String[] attribute = dob.split("-");
            int year = Integer.parseInt(attribute[0]);
            int month = Integer.parseInt(attribute[1]) - 1;
            int date = Integer.parseInt(attribute[2]);
            c.set(Calendar.YEAR, year);
            c.set(Calendar.MONTH, month);
            c.set(Calendar.DAY_OF_MONTH, date);
        }

        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        int minAge = Integer.parseInt(getStorage().getUserCountryEntity().getMinAge().getValue());
        Calendar tmpCalendar = Calendar.getInstance();
        tmpCalendar.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR) - minAge);
        tmpCalendar.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH));
        tmpCalendar.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                mTvDate.setText(formatDate(year, monthOfYear, dayOfMonth));
                mTvDate.setTypeface(CAApplication.getInstance().getBoldFont());
                getStorage().getM001ProfileEntity().setYearBorn(formatDate(year, monthOfYear, dayOfMonth));

                String dateOfBirth = String.format(Locale.getDefault(), "%d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);
                getStorage().getM001ProfileEntity().setDateOfBirth(dateOfBirth);

                if (mCallBack != null) mCallBack.dateSet();
            }
        }, mYear, mMonth, mDay);

        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMaxDate(tmpCalendar.getTimeInMillis());
    }

    public void showTimeChoosing(Context mContext, TextView tvTime) {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMin = c.get(Calendar.MINUTE);

        int hour = CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_HOUR, -1);
        int min = CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_MIN, -1);
        if (hour > -1 && min > -1) {
            mHour = hour;
            mMin = min;
        }

        TimePickerDialog mTimePickerDialog = new TimePickerDialog(mContext, (view, hourOfDay, minute) -> {
            boolean isPM = (hourOfDay >= 12);
            tvTime.setText(String.format(Locale.getDefault(),
                    "%02d:%02d %s", (hourOfDay == 12 || hourOfDay == 0) ? 12 : hourOfDay % 12, minute, isPM ? "PM" : "AM"));
            savePrefContent(REMINDER_HOUR, hourOfDay);
            savePrefContent(REMINDER_MIN, minute);
        }, mHour, mMin, android.text.format.DateFormat.is24HourFormat(mContext));
        mTimePickerDialog.show();

    }

    public void showNotification(Context mContext, String title, String content) {
        NotificationManager mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(DEFAULT,
                    title,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(CAApplication.getInstance().getString(R.string.txt_channel_notification));
            mNotificationManager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext, DEFAULT);
        mBuilder.setSmallIcon(R.drawable.ic_m034_reminder);
        mBuilder.setColor(mContext.getResources().getColor(R.color.colorM001Cyan));
        mBuilder.setContentTitle(title);
        mBuilder.setContentText(content);

        Intent intent = new Intent(mContext, HomeActivity.class);
        PendingIntent pi = PendingIntent.getActivity(mContext, ALARM_REQUEST_CODE, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        mBuilder.setContentIntent(pi);
        mNotificationManager.notify(0, mBuilder.build());
    }

    void showNotification(Context mContext, String title, String content, String keyTag) {
        NotificationManager mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(DEFAULT,
                    title,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(CAApplication.getInstance().getString(R.string.txt_channel_notification));
            mNotificationManager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext, DEFAULT);
        mBuilder.setSmallIcon(R.drawable.ic_plus);
        mBuilder.setColor(mContext.getResources().getColor(R.color.colorM001Cyan));
        mBuilder.setContentTitle(title);
        mBuilder.setContentText(content);
        mBuilder.setAutoCancel(true);

        switch (keyTag) {
            case KEY_NOTIFY_7_DAY:
            case KEY_NOTIFY_14_DAY:
                Intent intent = new Intent(mContext, HomeActivity.class);
                intent.putExtra(KEY_NOTIFY, keyTag);
                intent.setAction(keyTag);
                PendingIntent pi = PendingIntent.getActivity(mContext, ALARM_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                mBuilder.setContentIntent(pi);
                mNotificationManager.notify(NOTIFICATION_ID_MIN, mBuilder.build());
                break;
            case KEY_NOTIFY_14_DAY_LAST_DOSE:
                Intent intent1 = new Intent(mContext, HomeActivity.class);
                intent1.putExtra(KEY_NOTIFY, keyTag);
                PendingIntent pi1 = PendingIntent.getActivity(mContext, ALARM_REQUEST_CODE_LAST_DOSE, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
                mBuilder.setContentIntent(pi1);
                mNotificationManager.notify(NOTIFICATION_ID_MAX, mBuilder.build());
                break;
        }
    }

    public void showNotification(Context mContext) {
        String title = "Dialoq";
        int hour = CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_HOUR, -1);
        int min = CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_MIN, -1);

        boolean isPM = (hour >= 12);
        String content = "You set your Tresiba® dose for " + String.format(Locale.getDefault(),
                "%02d:%02d %s", (hour == 12 || hour == 0) ? 12 : hour % 12, min, isPM ? "PM" : "AM") + ". Did you take your Tresiba®";
        wtfi(TAG, "ALARM FIRED");
        showNotification(mContext, title, content);
    }

    public void setAppReminder(Context mContext) {
        CommonUtil.wtfi(TAG, "setAppReminder...");
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(mContext, ReminderReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(mContext, ALARM_REQUEST_CODE, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        long time1 = System.currentTimeMillis();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        int hour = getIntPrefContent(REMINDER_HOUR, -1);
        int minute = getIntPrefContent(REMINDER_MIN, -1);
        CommonUtil.wtfi(TAG, "REMINDER_HOUR: " + hour);
        CommonUtil.wtfi(TAG, "REMINDER_MIN: " + minute);

        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
        } else {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 24 * 60 * 60 * 1000, alarmIntent);
        }

        CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_EXIST, EXISTED + "");
        CommonUtil.getInstance().savePrefContent(REMINDER_SET, "true");
        CommonUtil.getInstance().savePrefContent(REMINDER_TODAY, time1 + "");
        CommonUtil.getInstance().savePrefContent(REMINDER_TIME_SET, calendar.getTime().getTime() + "");
    }

    public void setReminderNotification(Context mContext, String minDate, String maxDate) {
        CommonUtil.wtfi(TAG, "setReminderNotification....");
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(mContext, NotificationReceiver.class);

        // String Date Plus
        String mMinPlus7 = CommonUtil.getDateNow(CommonUtil.DATE_NOW_DY, minDate, 7);
        String mMinPlus14 = CommonUtil.getDateNow(CommonUtil.DATE_NOW_DY, minDate, 14);
        String mMaxPlus14 = CommonUtil.getDateNow(CommonUtil.DATE_NOW_DY, maxDate, 14);
        String mCurrent = CommonUtil.getDateNow(CommonUtil.DATE_TIME_NOW_DY);

        // convert to Date
        Date mMin7 = getDateAfter(mMinPlus7);
        Date mMin14 = getDateAfter(mMinPlus14);
        Date mMax14 = getDateAfter(mMaxPlus14);
        Date mCurrentDate = NVDateUtils.stringToDateLocal(mCurrent, CommonUtil.DATE_TIME_NOW_DY);
        Date minCurrent = getDateAfter(mCurrent);

        CommonUtil.getInstance().savePrefContent(KEY_NOTIFY_7_DAY, mMinPlus7);
        CommonUtil.getInstance().savePrefContent(KEY_NOTIFY_14_DAY, mMinPlus14);
        CommonUtil.getInstance().savePrefContent(KEY_NOTIFY_14_DAY_LAST_DOSE, mMaxPlus14);

        if (minCurrent != null
                && mCurrentDate != null
                && mCurrentDate.before(minCurrent)) {
            mCurrentDate = minCurrent;
        }
        String email = getStorage().getM007UserEntity().getEmail();
        CommonUtil.wtfd(TAG, "setReminderNotification....mMin7: " + mMin7);
        CommonUtil.wtfd(TAG, "setReminderNotification....mMin14: " + mMin14);
        CommonUtil.wtfd(TAG, "setReminderNotification....mMax14: " + mMax14);
        CommonUtil.wtfd(TAG, "setReminderNotification....email: " + email);

        Calendar calendarCurrent = Calendar.getInstance();
        calendarCurrent.setTime(mCurrentDate);
        int getHours = calendarCurrent.get(Calendar.HOUR_OF_DAY);
        int getMinutes = calendarCurrent.get(Calendar.MINUTE);
        int getSeconds = calendarCurrent.get(Calendar.SECOND);

        if (mCurrentDate != null && (mMin7.before(mCurrentDate) || mMin7.equals(mCurrentDate))
                && (getHours == HOUR_REMINDER && getMinutes == MINUTE_REMINDER && getSeconds == SECOND_REMINDER)) {

            intent.putExtra(KEY_NOTIFY, KEY_NOTIFY_7_DAY + email);
            PendingIntent alarmIntent = PendingIntent.getBroadcast(mContext, ALARM_REQUEST_NOTIFICATION, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, mMin7.getTime(), alarmIntent);
            }
            CommonUtil.getInstance().savePrefContent(KEY_NOTIFY_7_DAY + email, "true");
        }
        if (mCurrentDate != null && (mMin14.before(mCurrentDate) || mMin14.equals(mCurrentDate))
                && (getHours == HOUR_REMINDER && getMinutes == MINUTE_REMINDER && getSeconds == SECOND_REMINDER)) {

            intent.putExtra(KEY_NOTIFY, KEY_NOTIFY_14_DAY);
            PendingIntent alarmIntent = PendingIntent.getBroadcast(mContext, ALARM_REQUEST_NOTIFICATION, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, mMin14.getTime(), alarmIntent);
            } else {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, mMin14.getTime(), alarmIntent);
            }
            CommonUtil.getInstance().savePrefContent(KEY_NOTIFY_14_DAY + email, "true");
        }

        if (mCurrentDate != null && (mMax14.before(mCurrentDate) || mMax14.equals(mCurrentDate))
                && (getHours == HOUR_REMINDER && getMinutes == MINUTE_REMINDER && getSeconds == SECOND_REMINDER)) {

            alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
            CommonUtil.wtfd(TAG, "setReminderNotification....KEY_NOTIFY_14_DAY_LAST_DOSE: " + mMax14);
            intent = new Intent(mContext, NotificationPlusReminder.class);
            PendingIntent alarmIntent = PendingIntent.getBroadcast(mContext, ALARM_REQUEST_NOTIFICATION, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            alarmManager.cancel(alarmIntent);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, mMax14.getTime(), alarmIntent);
            } else {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, mMin14.getTime(), alarmIntent);
            }
        }
    }

    Date getDateAfter(String txtDate) {
        Date date = NVDateUtils.stringToDateLocal(txtDate, CommonUtil.DATE_NOW_DY);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, HOUR_REMINDER);
        calendar.set(Calendar.MINUTE, MINUTE_REMINDER);
        return calendar.getTime();
    }

    public void cancelAppReminder(boolean isPauseReminder, Context mContext, Switch mSwitch) {
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(mContext, ReminderReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, ALARM_REQUEST_CODE, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        alarmManager.cancel(pendingIntent);

        if (!isPauseReminder) {
            CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_EXIST, EMPTY + "");
            getStorage().setM065ScheduleEntity(null);
        }
        CommonUtil.getInstance().savePrefContent(CommonUtil.REMINDER_SET, "false");
        if (mSwitch != null) {
            mSwitch.setChecked(false);
        }
    }

    public String getAPIKey() {
        if (mApiKey == null) {
            mApiKey = CAApplication.getInstance().getString(R.string.api_key);
        }
        return mApiKey;
    }

    public void backAndNotify(Context mContext, OnOKDialogCallBack mCallBack, String message) {
        showDialog(mContext, mContext.getString(R.string.txt_dialog), message, null,
                mContext.getString(R.string.txt_confirm_got_it), mCallBack);
    }

    @SuppressWarnings("squid:S4042")
    public void saveObjData(Object obj, String fileName) {
        try {
            String path = CAApplication.getInstance().getExternalFilesDir(null) + "/" + fileName;
            wtfi(TAG, PATH + path);
            File file = new File(path);

            file.delete();
            file.createNewFile();

            FileOutputStream out = null;
            ObjectOutputStream output = null;
            try {
                out = new FileOutputStream(file);
                output = new ObjectOutputStream(out);
                output.writeObject(obj);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
                if (output != null) {
                    try {
                        output.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            wtfi(TAG, "saveObjData...OK");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("squid:S4042")
    public File saveFileText(String data, String fileName) {
        try {
            String path = CAApplication.getInstance().getExternalFilesDir(null) + "/" + fileName;
            wtfi(TAG, PATH + path);
            File file = new File(path);
            file.delete();
            file.createNewFile();

            FileWriter out = null;
            try {
                out = new FileWriter(file);
                out.write(data);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            wtfi(TAG, "saveFileText...OK");
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public <T> T getObjData(String nameFile) {
        try {
            String path = CAApplication.getInstance().getExternalFilesDir(null) + "/" + nameFile;
            wtfi(TAG, PATH + path);
            File file = new File(path);

            if (!file.exists()) {
                CommonUtil.wtfe(TAG, "getObjData...Err: File not found " + file.getPath());
                return null;
            }

            FileInputStream in = null;
            ObjectInputStream input = null;
            Object obj = null;
            try {
                in = new FileInputStream(file);
                input = new ObjectInputStream(in);
                obj = input.readObject();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            wtfi(TAG, "getObjData...OK");
            return (T) obj;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getTextAssetFile(String fileName) {
        AssetManager assetManager = CAApplication.getInstance().getAssets();
        try {
            InputStream in = null;
            try {
                in = assetManager.open("unitest/" + fileName);
                byte[] buff = new byte[1024];
                int len;
                StringBuilder text = new StringBuilder();
                while ((len = in.read(buff)) > 0) {
                    text.append(new String(buff, 0, len));
                }
                return text.toString();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isConnectToNetwork() {
        ConnectivityManager connectivityManager = (ConnectivityManager) CAApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
    }

    public void checkBluetoothOn(Context context) {
        if (!CommonUtil.getInstance().isBluetoothOn()) {
            CommonUtil.getInstance().showDialog(context,
                    CAApplication.getInstance().getString(R.string.txt_alert_ble_off_title),
                    CAApplication.getInstance().getString(R.string.txt_alert_ble_off_content),
                    CAApplication.getInstance().getString(R.string.txt_alert_ble_off_cancel),
                    CAApplication.getInstance().getString(R.string.txt_alert_ble_off_settings), new OnOKDialogAdapter() {
                        @Override
                        public void handleOKButton2() {
                            context.startActivity(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE));
                        }
                    });
        }
    }

    public void showLimitedDatePicker(Context mContext, TextView tvDate, TextView tvTime, OnActionCallBack event) {
        long maxDate = System.currentTimeMillis();

        Calendar noteCal = Calendar.getInstance();
        if (tvDate.getTag() != null) {
            Date dateToSet = NVDateUtils.stringToDateLocal((String) tvDate.getTag(), NVDateFormat.TIME_RANGE_FORMAT);
            noteCal.setTime(dateToSet);
        }
        int mYear = noteCal.get(Calendar.YEAR);
        int mMonth = noteCal.get(Calendar.MONTH);
        int mDay = noteCal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, (view, year, monthOfYear, dayOfMonth) -> {
            noteCal.set(Calendar.YEAR, year);
            noteCal.set(Calendar.MONTH, monthOfYear);
            noteCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            showLimitedTimePicker(mContext, noteCal, tvDate, tvTime, event);
        }, mYear, mMonth, mDay);

        datePickerDialog.getDatePicker().setMaxDate(maxDate);
        datePickerDialog.show();
    }

    private void showLimitedTimePicker(Context mContext, Calendar noteCal, TextView tvDate, TextView tvTime, OnActionCallBack event) {
        int mHour = noteCal.get(Calendar.HOUR_OF_DAY);
        int mMin = noteCal.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(mContext, (timePicker, hourOfDay, minute) -> {
            noteCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
            noteCal.set(Calendar.MINUTE, minute);
            dateVerify(mContext, noteCal, tvDate, tvTime, event);
        }, mHour, mMin, android.text.format.DateFormat.is24HourFormat(mContext));
        timePickerDialog.show();
    }

    public void dateVerify(Context mContext, Calendar noteCal, TextView tvDate, TextView tvTime, OnActionCallBack event) {
        try {
            String currentDate = getDateNow(DATE_STYLE);
            SimpleDateFormat currentDateFormat = new SimpleDateFormat(DATE_STYLE, Locale.getDefault());
            Calendar currentCal = Calendar.getInstance();
            currentCal.setTime(currentDateFormat.parse(currentDate));
            String timeFormat;
            if (android.text.format.DateFormat.is24HourFormat(mContext)) {
                timeFormat = NVDateFormat.TIME_SIMPLE_FORMAT_24;
            } else {
                timeFormat = NVDateFormat.TIME_SIMPLE_FORMAT;
            }

            if (noteCal == null) {
                String time = new SimpleDateFormat(DATE_STYLE, Locale.getDefault()).format(currentCal.getTime());

                event.onCallBack(time);
                tvDate.setText(NVDateUtils.getNoteDetailsTimeString(time));
                tvTime.setText(new SimpleDateFormat(timeFormat, Locale.getDefault()).format(currentCal.getTime()));
                return;
            }

            int noteToCurrent = noteCal.compareTo(currentCal);
            if (noteToCurrent > 0) {
                showDialog(mContext, R.string.txt_m062_dialog_select_hour, new OnOKDialogCallBack() {
                    @Override
                    public void handleOKButton1() {
                        showLimitedDatePicker(mContext, tvDate, tvTime, event);
                    }
                });
            } else {
                String time = new SimpleDateFormat(DATE_STYLE, Locale.getDefault()).format(noteCal.getTime());

                event.onCallBack(time);
                tvDate.setText(NVDateUtils.getNoteDetailsTimeString(time));
                tvTime.setText(new SimpleDateFormat(timeFormat, Locale.getDefault()).format(noteCal.getTime()));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void sharingPDF(String path, Context mContext) {
        Intent intentShareFile = new Intent(Intent.ACTION_SEND);
        Uri contentUri = FileProvider.getUriForFile(mContext, "com.novo.app.my.package.name.provider", new File(path));
        intentShareFile.setType("application/pdf");

        intentShareFile.putExtra(Intent.EXTRA_STREAM, contentUri);
        intentShareFile.putExtra(Intent.EXTRA_SUBJECT, "Dialoq's Summary report");
        intentShareFile.putExtra(Intent.EXTRA_TEXT, "PDF file to share has been attached below");
        intentShareFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intentShareFile.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);


        mContext.startActivity(Intent.createChooser(intentShareFile, "Share File"));
        CommonUtil.wtfi(TAG, contentUri.getPath());
    }

    @SuppressWarnings("squid:S4042")
    public void deletePDF(String fileName) {
        String path = CAApplication.getInstance().getExternalFilesDir(null) + fileName;
        File file = new File(path);
        if (file.exists()) {
            file.delete();
        }
    }

    public void initTypeFace(EditText view) {
        if (view.getText().toString().isEmpty()) {
            view.setTypeface(CAApplication.getInstance().getRegularFont());
        } else {
            view.setTypeface(CAApplication.getInstance().getBoldFont());
        }
    }

    private List<DoseTimingEntity> prepareTimeData(List<DataInfo> inputData) {
        // Create list for Tresiba's dose only (Tresiba 100, Tresiba 200, Unknown drug type, Unknown dose size)
        List<DataInfo> listTresiba = new ArrayList<>();
        for (int i = 0; i < inputData.size(); i++) {
            DataInfo item = inputData.get(i);
            if (item.getResult().getAssessmentType().equals(DoseLogAdapter.TYPE_ASSESSMENT_INJECTION) && !item.getResult().getCustom3().equals(DeviceEntity.CODE_FIASP + "")) {
                listTresiba.add(item);
            }
        }

        // Sort by increase time
        Collections.sort(listTresiba, (o1, o2)
                -> CommonUtil.convertShortTime(o1.getResult().getReportedDate())
                .compareTo(CommonUtil.convertShortTime(o2.getResult().getReportedDate())));

        // Convert all reportedDate become time only and round down the time
        List<DoseTimingEntity> doseTime = new ArrayList<>();
        for (int i = 0; i < listTresiba.size(); i++) {
            DataInfo item = listTresiba.get(i);
            String groupedTime = CommonUtil.convertToLocalDate(item.getResult().getReportedDate());
            if (groupedTime == null) {
                wtfe(TAG, "Null reported date");
                continue;
            }
            groupedTime = CommonUtil.convertShortTime(groupedTime);
            String[] minute = groupedTime.split(":");

            if (Integer.valueOf(minute[1]) > 30) minute[1] = "30";
            if (Integer.valueOf(minute[1]) < 30) minute[1] = "00";

            groupedTime = minute[0] + ":" + minute[1];
            doseTime.add(new DoseTimingEntity(groupedTime, 0));
        }
        return doseTime;
    }

    private List<DoseTimingEntity> createTimeList(List<DataInfo> inputData) {
        List<DoseTimingEntity> doseTime = prepareTimeData(inputData);
        // Create list time range
        List<DoseTimingEntity> listTimeRange = new ArrayList<>();
        for (int i = 0; i < doseTime.size(); i++) {
            DoseTimingEntity item = doseTime.get(i);
            if (i == 0 || !item.getTime().equals(doseTime.get(i - 1).getTime())) {
                listTimeRange.add(new DoseTimingEntity(item.getTime(), 0));
                Log.i("CREATE TIME RANGE LIST", item.getTime());
            }
        }

        // Add count number to list time range
        for (int i = 0; i < listTimeRange.size(); i++) {
            DoseTimingEntity item = listTimeRange.get(i);
            for (int j = 0; j < doseTime.size(); j++) {
                if (doseTime.get(j).getTime().equals(item.getTime())) {
                    item.setDoseCount(item.getDoseCount() + 1);
                }
            }
        }
        return listTimeRange;
    }

    public String calcTimingDose(List<DataInfo> inputData) {
        String timing = "";
        List<DoseTimingEntity> listTimeRange = createTimeList(inputData);
        // Calculating for total shots in time range and minimum shots (5%)
        int totalShot = 0;
        for (int i = 0; i < listTimeRange.size(); i++) {
            totalShot = totalShot + listTimeRange.get(i).getDoseCount();
        }
        if (totalShot != 0) {
            double minShot = (100 * totalShot * 0.1 / 2);
            // Remove time range when contain number of shots are under 5% and init start time
            String startTime = "";
            String endTime = "";

            for (int i = 0; i < listTimeRange.size(); i++) {
                if (listTimeRange.get(i).getDoseCount() * 100 < minShot) {
                    listTimeRange.remove(i);
                } else {
                    startTime = listTimeRange.get(i).getTime();
                    break;
                }
            }

            // Extend time range till it reach 90% of total shot
            double requirePercentage = 0;
            for (int i = 0; i < listTimeRange.size(); i++) {
                double percentOfShot = 10000 * (double) listTimeRange.get(i).getDoseCount() / totalShot;
                requirePercentage = requirePercentage + percentOfShot;
                if (requirePercentage >= 90 * 100 || i == listTimeRange.size() - 1) {
                    endTime = listTimeRange.get(i).getTime();
                    break;
                }
            }

            String[] time = endTime.split(":");
            if (time[1].equals("00")) {
                time[1] = "30";
            } else {
                time[0] = String.format(Locale.getDefault(), "%2d", Integer.valueOf(time[0]) + 1);
                time[1] = "00";
            }
            endTime = time[0] + ":" + time[1];
            // Show the result
            timing = NVDateUtils.getDoseTimingString(startTime) + " - " + NVDateUtils.getDoseTimingString(endTime);
        }
        return timing;
    }

    public void forceHideKeyBoard(View focusView) {
        Log.d(TAG, "forceHideKeyBoard...");
        if (focusView != null) {
            InputMethodManager imm = (InputMethodManager) CAApplication.getInstance()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
        }
    }

    public void doLogOut() {
        CommonUtil.wtfi(TAG, "doLogOut...");
        CommonUtil.getInstance().clearPrefContent(TOKEN_KEY);
//        CADBManager.getInstance().deleteAllRTokenEntity();
//        CADBManager.getInstance().deleteAllRDose();
//        CADBManager.getInstance().deleteAllRNote();
//        CADBManager.getInstance().deleteAllInjection();

        CommonUtil.getInstance().clearPrefContent(KEY_SYNC_READY);
        CommonUtil.getInstance().clearPrefContent(KEY_SAVE_CALLING_AS5);
        //CommonUtil.getInstance().clearPrefContent(M023LoginFrg.KEY_FIRST_TIME);
        CommonUtil.getInstance().clearPrefContent(M024DoseLogFrg.KEY_SAVE_LOGGED_IN);
        getStorage().setCallLoggedOut(false);
    }

    public boolean isUnitTest() {
        String device = Build.DEVICE;
        String product = Build.PRODUCT;
        if (device == null) {
            device = "";
        }

        if (product == null) {
            product = "";
        }
        return device.equals("robolectric") && product.equals("robolectric");
    }

    public void prepareReminder(RReminderEntity entity, String time, String scheduleNotificationId) {
        String startDate = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, 1);
        String endDate = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, 3650);

        entity.setScheduleName(System.currentTimeMillis() + "");
        entity.setFreqType("DAILY");
        entity.setFreqInterval("");
        entity.setFreqSubdayType("SPECIFIED_TIME");
        entity.setStartTime(time);
        entity.setStartDate(startDate);
        entity.setEndDate(endDate);
        entity.setScheduleNotificationId(scheduleNotificationId);
        entity.setNotificationChannel(CommonUtil.REMINDER_ACTIVE);
        entity.setNotificationType("REMIND_TAKE_MEDICATION");
        entity.setReminderTime("300");

        entity.setStatusFlag((entity.getStatusFlag() != null && entity.getStatusFlag().equals(RReminderEntity.STATE_ADD))
                ? RReminderEntity.STATE_ADD : RReminderEntity.STATE_EDIT);
    }

    public boolean isMature() {
        String dateOfBirth;
        if (getStorage().getM007UserEntity() != null) {
            dateOfBirth = getStorage().getM007UserEntity().getDateOfBirth();
        } else {
            dateOfBirth = getStorage().getM001ProfileEntity().getDateOfBirth();
        }

        dateOfBirth = dateOfBirth.substring(0, 4);
        int birthYear = Integer.parseInt(dateOfBirth);
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear - birthYear >= Integer.parseInt(getStorage().getUserCountryEntity().getAge().getValue());
    }

    public String encodeNoteContent(String notes) {
        String encodedString = "";
        if (notes != null)
            encodedString = Base64.encodeToString(notes.getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
        return encodedString;
    }

    public String decodeNoteContent(String notes) {
        String decodedString = "";
        if (notes != null)
            try {
                decodedString = new String(Base64.decode(notes, Base64.NO_WRAP), StandardCharsets.UTF_8);
            } catch (Exception e) {
                decodedString = notes;
                e.printStackTrace();
            }
        return decodedString;
    }

    public boolean hasEnrolledFingerprints() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            FingerprintManager fingerprintManager = (FingerprintManager) CAApplication.getInstance().getSystemService(FINGERPRINT_SERVICE);
            if (fingerprintManager == null || !fingerprintManager.isHardwareDetected()) {
                showNotify(R.string.txt_m005_not_support_fingerprint);
            } else {
                if (ActivityCompat.checkSelfPermission(CAApplication.getInstance(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                    showNotify(R.string.txt_m005_not_support_fingerprint);
                } else {
                    return fingerprintManager.hasEnrolledFingerprints();
                }
            }
        }
        return false;
    }

    public enum LOG_TYPE {
        INFO, DEBUG, TEST
    }
}
