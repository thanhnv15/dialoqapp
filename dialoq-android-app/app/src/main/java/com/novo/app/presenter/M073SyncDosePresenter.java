/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RInjectionEntity;
import com.novo.app.manager.entities.RInjectionItem;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM073SyncDoseCallBack;
import com.novo.app.view.widget.NVDateFormat;
import com.novo.app.view.widget.NVDateUtils;
import com.novo.app.view.widget.ProgressLoading;

import java.io.File;
import java.util.Collections;
import java.util.List;

import static com.novo.app.utils.CommonUtil.P_API_KEY;

public class M073SyncDosePresenter extends BasePresenter<OnM073SyncDoseCallBack> {
    private static final String KEY_API_AS6_UPDATE_RESULT = "KEY_API_AS6_UPDATE_RESULT";
    private static final String KEY_API_73_SYNC_RESULT = "KEY_API_73_SYNC_RESULT";
    private static final String URL_API_SYNC_RESULT = CommonUtil.HOT_URL + "/api/v1/assessments/%s/results/upload?";
    private static final String FILE_CSV = "file_sync.csv";
    private static final String URL_API_AS6_UPDATE_RESULT = CommonUtil.HOT_URL + "/api/v1/users/%s/assessments/%s/results/%s?";
    private static final String BODY_AS6 = "{ \"deviceId\": %s,  \"assessmentType\": %s,  \"result\": %s, \"reportedDate\": %s, \"notes\": %s,  \"custom1\": %s, \"custom2\": %s, \"custom3\": %s,\"custom4\": %s}";
    private static final String TAG = M073SyncDosePresenter.class.getName();
    private volatile int mCount;
    private transient List<RInjectionEntity> mListDose;

    public M073SyncDosePresenter(OnM073SyncDoseCallBack event, List<RInjectionEntity> lisDose) {
        super(event);
        this.mListDose = lisDose;

        if (mListDose != null && !mListDose.isEmpty()) {
            Collections.sort(mListDose);
            mCount = mListDose.size();
        }
    }

    @Override
    protected void handleSuccess(String data, String tag) {
        if (tag.contains(KEY_API_73_SYNC_RESULT)) {
            callAS6UpdateResult();
        } else if (tag.equals(KEY_API_AS6_UPDATE_RESULT)) {
            mListener.syncDoseSuccess(mCount);
        }
    }

    private String createTemFile() {
        if (mListDose == null || mListDose.size() == 0) {
            return null;
        }

        boolean isTresibaExist = false;
        StringBuilder data = new StringBuilder("DEVICE ID,result,REPORTED DATE,activity id,notes,custom 1,custom 2,custom 3,custom 4\n");
        for (int j = 0; j < mListDose.size(); j++) {
            RInjectionEntity entity = mListDose.get(j);
            if (entity.getInjectionItems() == null || entity.getInjectionItems().size() == 0) continue;
            CommonUtil.wtfi(TAG, "createTemFile...mLastNumSync: " + entity.getLastNumSync());

            for (int i = 0; i < entity.getInjectionItems().size(); i++) {
                RInjectionItem item = entity.getInjectionItems().get(i);
                assert item != null;

                data.append(",").append(item.getResult())
                        .append(",").append(item.getReportedDate())
                        .append(",,")
                        .append(item.getNotes()).append(",")
                        .append(item.getCustom1())
                        .append(",").append(item.getCustom2())
                        .append(",").append(item.getCustom3())
                        .append(",").append(item.getCustom4());

                if (j == mListDose.size() - 1) {
                    if (!isTresibaExist && item.getCustom3().equals(DeviceEntity.CODE_TRESIBA_100 + "") || item.getCustom3().equals(DeviceEntity.CODE_TRESIBA_200 + "")) {
                        String lastTresibaDose = item.getReportedDate();
                        lastTresibaDose = NVDateUtils.dateToStringUniversal(NVDateUtils.stringToDateUniversal(lastTresibaDose, NVDateFormat.TIME_RANGE_FORMAT), NVDateFormat.TIME_RANGE_FORMAT);
                        getStorage().setLastTresibaDose(lastTresibaDose);
                        isTresibaExist = true;
                    }
                }

                if (j < mListDose.size() - 1 || i < entity.getInjectionItems().size() - 1) {
                    data.append("\n");
                }
            }
        }

        File file = CommonUtil.getInstance().saveFileText(data.toString(), FILE_CSV);
        if (file != null) {
            return file.getPath();
        }


        return null;
    }

    public synchronized void syncData() {
        CommonUtil.wtfi(TAG, "syncData...");
        ProgressLoading.dontShow();
        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);

        if (token == null || token.isEmpty()) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }

        CommonUtil.wtfi(TAG, "token: " + token);
        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_SYNC_RESULT, getAssessmentId(CommonUtil.INJECTION_ASSESSMENT_TYPE)));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());

        request.addTAG(KEY_API_73_SYNC_RESULT);

        String filePath = createTemFile();
        CommonUtil.wtfi(TAG, "createTemFile:" + filePath);
        request.addFile("file", filePath);
        if (filePath == null) {
            mListener.syncFailed("Sync dose failed!");
            return;
        }

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tag);
        mListener.syncFailed("Sync dose failed!");
    }

    private void callAS6UpdateResult() {
        CommonUtil.wtfi(TAG, "callAS6UpdateResult");
        ProgressLoading.dontShow();
        CARequest request = new CARequest(TAG, CARequest.METHOD_PUT);
        request.addTAG(KEY_API_AS6_UPDATE_RESULT);

        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }

        CommonUtil.wtfi(TAG, "token: " + token);
        RInjectionEntity entity = mListDose.get(0);
        RInjectionItem device = entity.getDevice();

        if (device == null) {
            CommonUtil.wtfe(TAG, "callAS6UpdateResult...device: null");
            return;
        }

        String assessmentId = String.valueOf(getAssessmentId(CommonUtil.DEVICE_ASSESSMENT_TYPE));
        String resultId = device.getResultId();

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);

        request.addPathSegment(String.format(URL_API_AS6_UPDATE_RESULT,
                getStorage().getM007UserEntity().getUserId() + "",
                assessmentId,
                resultId));


        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY_AS6,
                entity.getSystemFlag() != null
                        && entity.getSystemFlag().equals(DeviceEntity.SYS_EOL_WARNING + "") ? "1" : "0",
                CommonUtil.DEVICE_ASSESSMENT_TYPE,
                isExistedTresiba(entity) ? "1" : "0",
                device.getReportedDate(),
                device.getNotes(),
                device.getCustom1(),
                device.getCustom2(),
                entity.getInjectionItems().get(entity.getInjectionItems().size() - 1).getCustom3(),
                CommonUtil.getDateNow(CommonUtil.DATE_STYLE)));
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    private boolean isExistedTresiba(RInjectionEntity entity) {
        if (entity == null || entity.getInjectionItems() == null
                || entity.getInjectionItems().isEmpty())
            return false;
        for (int i = 0; i < entity.getInjectionItems().size(); i++) {
            if (entity.getInjectionItems().get(i).getCustom3().equals(DeviceEntity.CODE_TRESIBA_100 + "")
                    || entity.getInjectionItems().get(i).getCustom3().equals(DeviceEntity.CODE_TRESIBA_200 + "")) {
                return true;
            }
        }
        return false;
    }
}
