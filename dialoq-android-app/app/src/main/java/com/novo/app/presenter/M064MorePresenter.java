package com.novo.app.presenter;

import com.novo.app.view.event.OnM064MoreCallBack;

public class M064MorePresenter extends BasePresenter<OnM064MoreCallBack> {
    public M064MorePresenter(OnM064MoreCallBack event) {
        super(event);
    }
}
