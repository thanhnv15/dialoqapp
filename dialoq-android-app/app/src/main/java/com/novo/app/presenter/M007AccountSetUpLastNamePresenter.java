package com.novo.app.presenter;

import com.novo.app.view.event.OnM007AccountSetUpLastNameCallBack;

public class M007AccountSetUpLastNamePresenter extends BasePresenter<OnM007AccountSetUpLastNameCallBack> {
    public M007AccountSetUpLastNamePresenter(OnM007AccountSetUpLastNameCallBack event) {
        super(event);
    }
}
