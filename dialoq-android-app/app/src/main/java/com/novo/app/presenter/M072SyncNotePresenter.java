/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.entities.RDoseEntity;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM072SyncNoteCallBack;
import com.novo.app.view.widget.ProgressLoading;

import java.io.File;
import java.util.List;

import static com.novo.app.service.SyncScheduleService.KEY_APP_NOTE_ADD_DELETE;
import static com.novo.app.service.SyncScheduleService.KEY_APP_NOTE_ADD_DELETE_TIME;
import static com.novo.app.utils.CommonUtil.P_API_KEY;

public class M072SyncNotePresenter extends BasePresenter<OnM072SyncNoteCallBack> {
    private static final String KEY_API_AS6_UPDATE_RESULT = "KEY_API_AS6_UPDATE_RESULT";
    private static final String URL_API_AS6_UPDATE_RESULT = CommonUtil.HOT_URL + "/api/v1/users/%s/assessments/%s/results/%s?";
    private static final String BODY_AS6 = "{ \"deviceId\": \"\", \"assessmentType\": %s, \"reportedDate\": %s,\"notes\": %s,\"custom1\": %s,\"custom2\": %s}";
    private static final String URL_API_SYNC_RESULT = CommonUtil.HOT_URL + "/api/v1/assessments/%s/results/upload?";
    private static final String FILE_CSV_NOTE = "file_notes_sync.csv";

    private static final String KEY_API_AS23_DELETE_RESULT = "KEY_API_AS23_DELETE_RESULT";
    private static final String URL_API_AS23_DELETE_RESULT = "assessment-service/api/v1/users/%s/assessments/%s/results/%s?";
    private static final String TAG = M072SyncNotePresenter.class.getName();
    private static final String KEY_API_SYNC_RESULT = "KEY_API_SYNC_RESULT";
    private static final String TOKEN = "token: ";
    private transient List<RDoseEntity> mListNote;
    private int indexDel, indexEdit;
    private int sumEdit, sumDel;
    private int sumAdd;

    public M072SyncNotePresenter(OnM072SyncNoteCallBack event, List<RDoseEntity> mListNote) {
        super(event);
        this.mListNote = mListNote;
    }

    @Override
    public void doFailed(String tag, Exception obj, int i, String s) {
        mListener.syncFailed("Sync note failed!");
    }

    @Override
    protected void handleSuccess(String text, String tag) {
        if (KEY_API_SYNC_RESULT.equals(tag)) {
            updateEditNote();
            CommonUtil.wtfi(TAG, "upADDNoteData...NOTE ADD success!");
            return;
        } else if (tag.contains(KEY_API_AS6_UPDATE_RESULT)) {
            indexEdit++;
            CommonUtil.wtfi(TAG, "EditNoteData...NOTE EDIT success!" + indexEdit);
            String resultId = tag.replace(KEY_API_AS6_UPDATE_RESULT, "");
            CADBManager.getInstance().resetFlagNote(resultId);

            if (sumEdit == indexEdit) {
                deleteNote();
            }
        } else if (tag.contains(KEY_API_AS23_DELETE_RESULT)) {
            indexDel++;
            CommonUtil.wtfi(TAG, "DeleteNoteData...NOTE DEL success!" + indexDel);
            String resultId = tag.replace(KEY_API_AS23_DELETE_RESULT, "");
            CADBManager.getInstance().deleteRNote(resultId);
        }

        if (indexDel + indexEdit == sumEdit + sumDel) {
            mListener.syncNoteSuccess(sumAdd, sumEdit, sumDel);
        }
    }

    @Override
    public void hideLockDialog(String key) {
        //do nothing
    }


    private String createTemFile() {
        StringBuilder data = new StringBuilder("DEVICE ID,result,REPORTED DATE,activity id,notes,custom 1,custom 2,custom 3,custom 4\n");
        boolean isExistNote = false;
        sumAdd = 0;
        for (int i = 0; i < mListNote.size(); i++) {
            RDoseEntity item = mListNote.get(i);
            if (item.getStatusFlag().equals(RDoseEntity.STATE_ADD)) {
                sumAdd++;
                data.append(item.getDeviceId() == null ? "" : item.getDeviceId())
                        .append(",").append(item.getResult())
                        .append(",").append(item.getReportedDate())
                        .append(",").append(item.getActivityId() == null ? "" : item.getActivityId())
                        .append(",").append(CommonUtil.getInstance().encodeNoteContent(item.getNotes()))
                        .append(",").append(item.getCustom1())
                        .append(",").append(item.getCustom2())
                        .append(",").append(item.getCustom3())
                        .append(",").append(item.getCustom4());
                if (i < mListNote.size() - 1) {
                    data.append("\n");
                }
                isExistNote = true;
            }
        }

        if (isExistNote) {
            File file = CommonUtil.getInstance().saveFileText(data.toString(), FILE_CSV_NOTE);
            if (file != null) {
                return file.getPath();
            }
        }

        return null;
    }

    public void upADDNoteData() {
        CommonUtil.wtfi(TAG, "upADDNoteData...");
        ProgressLoading.dontShow();
        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_SYNC_RESULT);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);

        if (token == null || token.isEmpty()) {
            return;
        }
        String assessmentNoteID = getAssessmentId(CommonUtil.NOTES_ASSESSMENT_TYPE);
        if (assessmentNoteID.equals("")) return;

        CommonUtil.wtfi(TAG, TOKEN + token);
        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_SYNC_RESULT, assessmentNoteID));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());

        String filePath = createTemFile();
        if (filePath == null) {
            updateEditNote();
            CommonUtil.wtfi(TAG, "upADDNoteData...NOTE ADD success!");
            CommonUtil.getInstance().clearPrefContent(KEY_APP_NOTE_ADD_DELETE);
            CommonUtil.getInstance().clearPrefContent(KEY_APP_NOTE_ADD_DELETE_TIME);
            return;
        }

        CommonUtil.wtfi(TAG, "createTemFile:" + filePath);
        request.addFile("file", filePath);

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    @Override
    protected String getAssessmentId(String assessmentType) {
        CarePlantUserEntity entity = CAApplication.getInstance().getStorageCommon().getM001CarePlanUserEntity();
        String assessmentId = "";
        if (entity == null) return "";
        for (int i = 0; i < entity.getData().size(); i++) {
            List<CarePlantUserEntity.AssessmentInfo> listAssessment = entity.getData().get(i).getAssessments();
            for (int j = 0; j < listAssessment.size(); j++) {
                if (listAssessment.get(j).getType().equals(assessmentType)) {
                    assessmentId = listAssessment.get(j).getAssessmentId();
                    return assessmentId;
                }
            }
        }
        return assessmentId;
    }


    private void deleteNote() {
        CommonUtil.wtfi(TAG, "deleteNote...");
        sumDel = 0;
        indexDel = 0;
        for (int i = 0; i < mListNote.size(); i++) {
            RDoseEntity item = mListNote.get(i);
            if (item.getStatusFlag().equals(RDoseEntity.STATE_DEL)) {
                sumDel++;
                delEachNote(item.getResultId());
            }
        }

        if (sumDel == 0) {
            mListener.syncNoteSuccess(sumAdd, sumEdit, sumDel);
        }
    }

    private void delEachNote(String resultId) {
        CommonUtil.wtfi(TAG, "delEachNote...");
        CARequest request = new CARequest(TAG, CARequest.METHOD_DELETE);
        request.addTAG(KEY_API_AS23_DELETE_RESULT + resultId);
        ProgressLoading.dontShow();
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            return;
        }
        CommonUtil.wtfi(TAG, TOKEN + token);
        request.addHeaders(CommonUtil.AUTHOR, token);

        request.addPathSegment(String.format(URL_API_AS23_DELETE_RESULT,
                getStorage().getM007UserEntity().getUserId() + "",
                getNoteAssessmentId() + "",
                resultId));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    private void updateEditNote() {
        CommonUtil.wtfi(TAG, "updateEditNote...");
        sumEdit = 0;
        indexEdit = 0;
        for (int i = 0; i < mListNote.size(); i++) {
            RDoseEntity item = mListNote.get(i);
            if (item.getStatusFlag().equals(RDoseEntity.STATE_EDIT)) {
                sumEdit++;
                updateEachNote(item.getAssessmentType(), item.getResultId(),
                        CommonUtil.getInstance().encodeNoteContent(item.getNotes()),
                        item.getReportedDate(),
                        item.getCustom1(),
                        item.getCustom2());
            }
        }

        if (sumEdit == 0) {
            deleteNote();
        }
    }


    private void updateEachNote(String assessmentType, String resultId, String notes, String reportDate, String time, String custom2) {
        CommonUtil.wtfi(TAG, "editNoteInDB");
        CARequest request = new CARequest(TAG, CARequest.METHOD_PUT);
        request.addTAG(KEY_API_AS6_UPDATE_RESULT + resultId);
        ProgressLoading.dontShow();
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            return;
        }

        CommonUtil.wtfi(TAG, TOKEN + token);
        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);

        request.addPathSegment(String.format(URL_API_AS6_UPDATE_RESULT,
                getStorage().getM007UserEntity().getUserId() + "",
                getNoteAssessmentId(),
                resultId));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY_AS6, assessmentType, reportDate, notes, time, custom2));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    private String getNoteAssessmentId() {
        CarePlantUserEntity entity = getStorage().getM001CarePlanUserEntity();
        for (int i = 0; i < entity.getData().size(); i++) {
            for (int f = 0; f < entity.getData().get(i).getAssessments().size(); f++) {
                String type = entity.getData().get(i).getAssessments().get(f).getType();
                if (type.equals(CommonUtil.ASSESSMENT_TYPE_NOTE)) {
                    return entity.getData().get(i).getAssessments().get(f).getAssessmentId();
                }
            }
        }
        return "";
    }
}
