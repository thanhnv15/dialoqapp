/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.view.dialog;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.presenter.M042MorePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseDialog;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.event.OnM042MoreCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;

import java.util.List;

public class M042MoreDeviceDialog extends BaseDialog<M042MorePresenter, OnM042MoreCallBack> implements OnCallBackToView, OnM042MoreCallBack {
    private DeviceEntity entity;

    public M042MoreDeviceDialog(Context context, boolean isCancel) {
        super(context, isCancel, R.style.dialog_style);
    }

    @Override
    public void showM001LandingScreen(String err) {
        mCallBack.showM001LandingScreen(err);
    }

    @Override
    protected void initViews() {
        entity = getStorage().getM042CurrentItem();
        getStorage().setM042AddNewDevice(false);
        findViewById(R.id.iv_m042_detail_back, this);
        findViewById(R.id.iv_m042_low_battery_arrow, this);

        View mVAlert = findViewById(R.id.v_m042_detail_alert);
        if (entity.isLowBattery()) mVAlert.setVisibility(View.VISIBLE);
        else mVAlert.setVisibility(View.GONE);

        findViewById(R.id.tv_m042_detail_title, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m042_detail_last_used, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m042_last_paired, CAApplication.getInstance().getBoldFont());

        TextView mTvID = findViewById(R.id.tv_m042_detail_device_id, CAApplication.getInstance().getBoldFont());
        mTvID.setText(entity.getDeviceID());

        findViewById(R.id.ln_m042_last_use).setVisibility(entity.getLastUse() == null || entity.getLastUse().isEmpty() ? View.GONE : View.VISIBLE);

        TextView mTvLastUse = findViewById(R.id.tv_m042_detail_time, CAApplication.getInstance().getRegularFont());
        if (entity.getLastUse() != null && !entity.getLastUse().isEmpty()) {
            mTvLastUse.setText(String.format("%s with ", entity.getLastUse()));
        }

        TextView mTvLastPaired = findViewById(R.id.tv_m042_paired_time, CAApplication.getInstance().getRegularFont());
        mTvLastPaired.setText(entity.getLastPair());

        TextView mTvDrug = findViewById(R.id.tv_m042_detail_drug, CAApplication.getInstance().getRegularFont());
        ImageView mIvDrug = findViewById(R.id.iv_m042_detail_drug);

        ImageView mIvBattery = findViewById(R.id.iv_m042_detail_battery);
        TextView mTvRemove = findViewById(R.id.tv_m042_detail_remove, this, CAApplication.getInstance().getMediumFont());
        mTvRemove.setText(String.format(LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m042_more_devices_detail_textview_remove)), entity.getDeviceID()));

        if (!entity.isLowBattery()) mIvBattery.setVisibility(View.GONE);

        switch (entity.getDrugCode() + "") {
            case DeviceEntity.CODE_FIASP + "":
                mIvDrug.setImageResource(R.drawable.ic_fiasp);
                mTvDrug.setText(mContext.getString(R.string.txt_fiasp));
                break;
            case DeviceEntity.CODE_TRESIBA_100 + "":
                mIvDrug.setImageResource(R.drawable.ic_tresiba);
                mTvDrug.setText(mContext.getString(R.string.txt_tresiba));
                break;
            case DeviceEntity.CODE_TRESIBA_200 + "":
                mIvDrug.setImageResource(R.drawable.ic_m012_drug_t);
                mTvDrug.setText(mContext.getString(R.string.txt_tresiba));
                break;
            case DeviceEntity.CODE_UNKNOWN + "":
                mIvDrug.setVisibility(View.GONE);
                mTvDrug.setText(mContext.getString(R.string.txt_unknown));
                break;
        }

    }

    @Override
    public int getLayoutId() {
        return R.layout.view_m042_device_detail;
    }

    @Override
    protected M042MorePresenter getPresenter() {
        return new M042MorePresenter(this);
    }

    @Override
    public void backToPreviousScreen() {
    }

    @Override
    protected void onClickView(int idView) {
        getStorage().setM042AddNewDevice(false);
        switch (idView) {
            case R.id.iv_m042_detail_back:
                dismiss();
                break;
            case R.id.iv_m042_low_battery_arrow:
                mCallBack.showTroubleShooting();
                new Handler().postDelayed(() -> dismiss(), 500);
                break;
            case R.id.tv_m042_detail_remove:
                CommonUtil.getInstance().showDialog(mContext,
                        LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m042_more_devices_remove_dialog_textview_title)).replace("${device_name}", entity.getDeviceID()),
                        LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m042_more_devices_remove_dialog_textview_description)),
                        LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m061_more_devices_remove_dialog_textview_remove)),
                        LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m061_more_devices_remove_dialog_textview_cancel)),
                        new OnOKDialogCallBack() {
                            @Override
                            public void handleOKButton1() {
                                getStorage().setResultId(entity.getResultId());
                                mPresenter.callAS23DeleteAssessmentResult(entity);
                            }
                        });
                break;
        }
    }

    @Override
    public void showDeviceDetail() {
        //This feature isn't require here
    }

    @Override
    public void showTroubleShooting() {
        //This feature isn't require here
    }

    @Override
    public void updateList() {
        //This feature isn't require here
    }

    @Override
    public void closeDeviceDetail() {
        List<DeviceEntity> mListDevice = getStorage().getM025ListDevices();
        for (int i = 0; i < mListDevice.size(); i++) {
            if (mListDevice.get(i).getResultId().equals(entity.getResultId())) {
                getStorage().getM025ListDevices().remove(i);
                break;
            }
        }

        mCallBack.updateList();
        dismiss();
    }
}
