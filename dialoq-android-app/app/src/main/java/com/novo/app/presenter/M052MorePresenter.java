package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM052MoreCallBack;
import com.novo.app.view.fragment.M023LoginFrg;

import static com.novo.app.utils.CommonUtil.P_API_KEY;

public class M052MorePresenter extends BasePresenter<OnM052MoreCallBack> {
    private static final String KEY_API_DA01_DELETE_ACCOUNT = "KEY_API_DA01_DELETE_ACCOUNT";
    private static final String URL_API_DA01_DELETE_ACCOUNT = CommonUtil.BASE_URL + "dialoq-service/api/v1/delete?";

    private static final String KEY_API_UN04_UPDATE_PATIENT = "KEY_API_UN04_UPDATE_PATIENT";
    private static final String URL_API_UN04_UPDATE_PATIENT = "user-service/api/v1/users/patients/%s?";
    private static final String BODY_UN04 = "{\"firstName\":%s,\"lastName\":%s,\"email\":%s,\"gender\": %s,\"dateOfBirth\":%s,\"mobilePhoneNumber\": \"84978678205\",\"country\":%s,\"medicationHistory\":%s,\"familyHistory\":%s,\"personalHistory\":%s,\"allergies\":%s,\"race\":%s,\"streetAddress1\":%s,\"streetAddress2\":%s,\"state\":%s}";
    private static final String TAG = M052MorePresenter.class.getName();

    public M052MorePresenter(OnM052MoreCallBack event) {
        super(event);
    }

    @Override
    protected void handleSuccess(String data, String tag) {
        CommonUtil.wtfi(TAG, "DATA: " + data);
        if (tag.equals(KEY_API_UN04_UPDATE_PATIENT)) {
            mListener.toLandingScreen();
        } else if (tag.equals(KEY_API_DA01_DELETE_ACCOUNT)) {
            if (data != null && data.contains(CommonUtil.TRUE_CASE)) {
                CommonUtil.getInstance().savePrefContent(CommonUtil.USER_NAME, "");
                CADBManager.getInstance().deleteRUserProfile();
                CADBManager.getInstance().deleteAllRDose();
                CADBManager.getInstance().deleteAllNNDM();
                CADBManager.getInstance().delAllReminder();
                CommonUtil.getInstance().clearPrefContent(M023LoginFrg.KEY_FIRST_TIME);

                mListener.stopService();
                mListener.toLandingScreen();
            } else {
                mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            }
        }
    }

    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tagRequest);

        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        String sms = LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON);
        if (responseEntity != null) {
            sms = responseEntity.getErrorEntity()[0].getMessage();
        }

        if (tag.equals(KEY_API_DA01_DELETE_ACCOUNT)) {
            mListener.showAlertDialog(sms);
        }
    }

    public void callDA01DeleteUserAccount() {
        CommonUtil.wtfi(TAG, "callDA01DeleteUserAccount");

        CARequest request = new CARequest(TAG, CARequest.METHOD_POST, false);
        request.addTAG(KEY_API_DA01_DELETE_ACCOUNT);

        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, "token: " + token);
        request.addPathSegment(URL_API_DA01_DELETE_ACCOUNT);
        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    public void callIUN04UpdatePatient() {
        CommonUtil.wtfi(TAG, "callIUN04UpdatePatient");

        CARequest request = new CARequest(TAG, CARequest.METHOD_PUT);
        request.addTAG(KEY_API_UN04_UPDATE_PATIENT);

        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, "token: " + token);
        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);

        UserEntity entity = getStorage().getM007UserEntity();
        request.addPathSegment(String.format(URL_API_UN04_UPDATE_PATIENT, entity.getUserId() + ""));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY_UN04, entity.getFirstName(), entity.getLastName(),
                entity.getEmail(), entity.getGender(), entity.getDateOfBirth(), entity.getCountry(),
                entity.getMedicationHistory(), entity.getFamilyHistory(), entity.getPersonalHistory(),
//                entity.getAllergies(), entity.getRace(), entity.getStreetAddress1(), entity.getStreetAddress2(), entity.getState(), entity.getPreferredPharmacyName()));
                entity.getAllergies(), entity.getRace(), entity.getStreetAddress1(), entity.getStreetAddress2(), entity.getState()));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }
}
