package com.novo.app.view.fragment;

import android.text.Editable;
import android.widget.Button;
import android.widget.EditText;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M054MorePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM054MoreCallBack;
import com.novo.app.view.widget.TextAdapter;

public class M054MoreNameFrg extends BaseFragment<M054MorePresenter, OnM024DoseLogCallBack> implements OnM054MoreCallBack {
    public static final String TAG = M054MoreNameFrg.class.getName();
    private EditText mEdtUserFirst;
    private EditText mEdtUserLast;

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m054_first, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m054_last, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.iv_m054_back, this);
        findViewById(R.id.tv_m054_title, CAApplication.getInstance().getBoldFont());

        Button mBtUpdate = findViewById(R.id.bt_m054_update, this);
        mBtUpdate.setEnabled(false);

        mEdtUserFirst = findViewById(R.id.tv_m054_user_first_name, CAApplication.getInstance().getBoldFont());
        mEdtUserLast = findViewById(R.id.tv_m054_user_last_name, CAApplication.getInstance().getBoldFont());

        mEdtUserFirst.setText(CADBManager.getInstance().getRUserProfile().getFirstName());
        mEdtUserLast.setText(CADBManager.getInstance().getRUserProfile().getLastName());

        mEdtUserFirst.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (!mEdtUserFirst.getText().toString().isEmpty()) {
                    mBtUpdate.setEnabled(true);
                } else mBtUpdate.setEnabled(false);
            }
        });
        mEdtUserLast.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (!mEdtUserLast.getText().toString().isEmpty()) {
                    mBtUpdate.setEnabled(true);
                } else mBtUpdate.setEnabled(false);
            }
        });

    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m054_more_name;
    }

    @Override
    protected M054MorePresenter getPresenter() {
        return new M054MorePresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {

    }

    @Override
    public void backToPreviousScreen() {
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m054_back:
                mCallBack.showChildFrgScreen(TAG, M053MoreAboutMeFrg.TAG);
                CommonUtil.getInstance().forceHideKeyBoard(mEdtUserLast);
                break;
            case R.id.bt_m054_update:
                getStorage().getM007UserEntity().setFirstName(mEdtUserFirst.getText().toString());
                getStorage().getM007UserEntity().setLastName(mEdtUserLast.getText().toString());
                CommonUtil.getInstance().forceHideKeyBoard(mEdtUserLast);
                mPresenter.editUserInDB(getStorage().getM007UserEntity());
                break;
        }
    }

    @Override
    public void showLoginScreen() {
        mCallBack.showM023LoginScreen();
    }

    @Override
    public void backToAboutMe() {
        mCallBack.showChildFrgScreen(TAG, M053MoreAboutMeFrg.TAG);
    }

    @Override
    public void showDialog() {
        CommonUtil.getInstance().showDialog(mContext, LangMgr.getInstance().getLangList().get(getString(R.string.lang_m054_more_my_account_about_name)), null);
    }

    @Override
    public void updateSuccess() {
        CADBManager.getInstance().setupUserProfile(getStorage().getM007UserEntity());
        mCallBack.showChildFrgScreen(TAG, M053MoreAboutMeFrg.TAG);
    }

    @Override
    public void updateFailed() {
        CommonUtil.getInstance().showDialog(mContext, LangMgr.getInstance().getLangList().get(getString(R.string.lang_m054_more_my_account_about_name)), null);
    }
}
