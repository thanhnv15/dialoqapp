package com.novo.app.view.fragment;

import android.os.Handler;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M015SignUpPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM015SignUpCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

public class M015SignUpSuccessFrg extends BaseFragment<M015SignUpPresenter, OnHomeBackToView> implements OnM015SignUpCallBack {

    public static final String TAG = M015SignUpSuccessFrg.class.getName();
    private static final long DELAY_TIME = 2000;

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m015_description, CAApplication.getInstance().getBoldFont());
        getStorage().getM001ProfileEntity().setVerifyCode(null);
        new Handler().postDelayed(() -> mCallBack.showFrgScreen(TAG, M020SignUpProfileHelpFrg.TAG), DELAY_TIME);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m015_signup_success;
    }

    @Override
    protected M015SignUpPresenter getPresenter() {

        return new M015SignUpPresenter(this);
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //Wait for customer confirmation
    }

    @Override
    public void backToPreviousScreen() {
//        Do not back to previous screen
    }
}
