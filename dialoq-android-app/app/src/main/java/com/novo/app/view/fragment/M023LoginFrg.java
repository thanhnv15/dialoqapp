/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.view.fragment;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.Editable;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TableRow;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.ProfileEntity;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.presenter.M023LoginPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseEditText;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.dialog.M023FingerPrintDialog;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM023LoginCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.TextAdapter;

import java.util.Objects;

/**
 * <b></b>Login Fragment</b><br/>
 * <p>
 * <br/>This fragment is 'View' object in MVP pattern
 * <br/>This fragment uses an {@link M023LoginPresenter} as linked Presenter
 * <br/>Callbacks to this fragment will be defined by {@link OnM023LoginCallBack} interface
 * <p>
 * <br/><br/>This fragment is used in login session features of dialog app, where user has to input
 * name / password for logging in
 */
public class M023LoginFrg extends BaseFragment<M023LoginPresenter, OnHomeBackToView> implements OnM023LoginCallBack {

    public static final String TAG = M023LoginFrg.class.getName();

    // Key for getting a boolean value indicates that this is the first time logging in or not
    public static final String KEY_FIRST_TIME = "KEY_FIRST_TIME";

    // Dialog for finger print authentication, in case of devices has this feature
    private M023FingerPrintDialog mFingerprintDialog;

    // Text boxes & Views used in login session
    private BaseEditText mEdtEmail;
    private BaseEditText mEdtPass;
    private TableRow mTrEmailError;
    private TableRow mTrPassError;
    private Button mBtSignIn;
    private TextView mTvShow;

    // Holder for holding logged in user's information
    private ProfileEntity mProfileEntity;

    // Indicating that email and/or password is valid or not
    private boolean emailCorrected = false;
    private boolean passCorrected = false;

    // Boolean value represents for 'remember me' checkbox  status
    private String keep;

    // Other view components
    private TextView mTvPassError;
    private TextView mTvEmailError;
    private TextView mTvTopLine;
    private TextView mTvBottomLine;
    private TableRow mTrLinePassWord;


    @Override
    protected void initViews() {
        getStorage().clearAllDataLogin();
        CommonUtil.getInstance().clearPrefContent(M024DoseLogFrg.KEY_SAVE_LOGGED_IN);

        mProfileEntity = new ProfileEntity();
        findViewById(R.id.iv_m023_finger_print, this);
        findViewById(R.id.iv_m023_back, this);
        findViewById(R.id.tv_m023_keep_detail, CAApplication.getInstance().getRegularFont());

        mTvTopLine = findViewById(R.id.tv_m023_line_top);
        mTvBottomLine = findViewById(R.id.tv_m023_line_bottom);

        mTrEmailError = findViewById(R.id.tr_m023_email_error);
        mTrPassError = findViewById(R.id.tr_m023_password_error);
        mTrEmailError.setVisibility(View.GONE);
        mTrPassError.setVisibility(View.GONE);

        mTvShow = findViewById(R.id.tv_m023_show, this, CAApplication.getInstance().getRegularFont());
        mTvEmailError = findViewById(R.id.tv_m023_email_error, CAApplication.getInstance().getRegularFont());
        mTvPassError = findViewById(R.id.tv_m023_login_error, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m023_keep, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m023_forgot, this, CAApplication.getInstance().getMediumFont());

        mTrLinePassWord = findViewById(R.id.tr_m023_pass_error);
        CheckBox mCbKeep = findViewById(R.id.cb_m023_keep, this);
        keep = CommonUtil.getInstance().getPrefContent(CommonUtil.KEEP);

        if (keep != null && keep.equals(CommonUtil.KEEP)) {
            mCbKeep.setChecked(true);
        }

        mBtSignIn = findViewById(R.id.bt_m023_sign_in, this, CAApplication.getInstance().getMediumFont());
        mBtSignIn.setEnabled(false);

        mEdtEmail = findViewById(R.id.tv_m023_email, CAApplication.getInstance().getRegularFont());
        if (CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME) != null && !CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME).equals("")) {
            mEdtEmail.setText(CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME));
            mEdtEmail.setTypeface(CAApplication.getInstance().getBoldFont());
            mEdtEmail.setFocusable(false);
            mEdtEmail.setEnabled(false);
            mEdtEmail.setCursorVisible(false);
            emailCorrected = true;

            validateEmail(mEdtEmail.getText().toString());
        } else {
            mEdtEmail.addTextChangedListener(new TextAdapter() {
                @Override
                public void afterTextChanged(Editable editable) {
                    validateEmail(editable.toString());
                }
            });

            mEdtEmail.setOnFocusChangeListener((v, hasFocus) -> showErrorEmailUI(hasFocus));
        }
        mEdtPass = findViewById(R.id.tv_m023_password, CAApplication.getInstance().getRegularFont());
        mEdtPass.setOnFocusChangeListener((v, hasFocus) -> showErrorPasswordUI(hasFocus));
        mEdtPass.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                CommonUtil.getInstance().initTypeFace(mEdtPass);
                validatePass(editable.toString());
            }
        });
        onKeyBoardDismiss();
        String isFirstTimeLogin = CommonUtil.getInstance().getPrefContent(KEY_FIRST_TIME);

        if (isFirstTimeLogin != null && isFirstTimeLogin.equals(Boolean.TRUE + "")
                && !getStorage().getFlagUsedTouchID()
                && !mEdtEmail.getText().toString().isEmpty()) {
            getStorage().setM001ProfileEntity(mProfileEntity);
            if (mFingerprintDialog == null) {
                mFingerprintDialog = new M023FingerPrintDialog(mContext, true);
                mFingerprintDialog.setOnCallBack(M023LoginFrg.this);
            }
            mFingerprintDialog.show();
        }
    }

    /**
     * Reset layout when keyboard dismissed
     */
    private void onKeyBoardDismiss() {
        mEdtEmail.setOnEditTextImeBackListener((ctrl, text) -> {
            mEdtEmail.clearFocus();
            showLine(true);
        });

        mEdtEmail.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEdtEmail.clearFocus();
                showLine(true);
            }
            return false;
        });

        mEdtPass.setOnEditTextImeBackListener((ctrl, text) -> {
            mEdtPass.clearFocus();
            showLine(true);
        });

        mEdtPass.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE
                    || event == null
                    || event.getKeyCode() == KeyEvent.FLAG_EDITOR_ACTION) {
                mEdtPass.clearFocus();
                showLine(true);
            }
            return false;
        });
    }

    /**
     * Show email error layout differently in case of having/not having focus
     *
     * @param hasFocus indicating if {@link #mEdtEmail} having focus or not
     */
    private void showErrorEmailUI(boolean hasFocus) {
        if (!hasFocus) {
            String email = Objects.requireNonNull(mEdtEmail.getText()).toString();
            showEmailError(CommonUtil.isNotEmail(email));
            showLine(true);
        } else {
            showEmailError(false);
            showLine(false);
        }
    }

    /**
     * Show password error layout differently in case of having/not having focus
     *
     * @param hasFocus indicating if {@link #mEdtPass} having focus or not
     */
    private void showErrorPasswordUI(boolean hasFocus) {
        if (!hasFocus) {
            showLine(true);
            String pass = Objects.requireNonNull(mEdtPass.getText()).toString();
            checkPassWord(pass);
        } else {
            showPassError(false);
            showAccountError(false);
            showLine(false);
            validatePass(mEdtPass.getText().toString());
        }
    }

    /**
     * Check if typed in password is valid or not
     * Also update UI showing invalid password if necessary
     * Check result will be stored in {@link #passCorrected}
     *
     * @param pass password to be checked (in {@link String})
     */
    private void checkPassWord(String pass) {
        if (pass.isEmpty()) {
            mBtSignIn.setEnabled(false);
            passCorrected = false;
        } else {
            showPassError(false);
            passCorrected = true;
            mBtSignIn.setEnabled(emailCorrected);
        }
    }

    /**
     * Check if typed in user email is valid or not
     * Also update UI showing invalid email if necessary
     * Check result will be stored in {@link #emailCorrected}
     *
     * @param email email address to be checked (in {@link String})
     */
    private void validateEmail(String email) {
        if (email.isEmpty()) {
            mBtSignIn.setEnabled(false);
            mTvEmailError.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_err_empty_email)));
            showEmailError(false);
            emailCorrected = false;
            return;
        }
        mTvEmailError.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_err_invalid_email)));
        CommonUtil.getInstance().initTypeFace(mEdtEmail);
        emailCorrected = !CommonUtil.isNotEmail(email);
        mBtSignIn.setEnabled(passCorrected && emailCorrected);
        mProfileEntity.setEmail(email);
        showEmailError(false);
    }

    /**
     * Show or hide email error layout
     *
     * @param show true if email error layout should be shown
     */
    private void showEmailError(boolean show) {
        if (show) {
            emailCorrected = CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME) != null;
            mTrEmailError.setVisibility(View.VISIBLE);
            mEdtEmail.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
            mEdtEmail.getBackground().setLevel(CommonUtil.CODE_COLOR_ERROR);
        } else {
            mTrEmailError.setVisibility(View.GONE);
            emailCorrected = true;
            mEdtEmail.setTextColor(getResources().getColor(R.color.colorBlack));
            mEdtEmail.getBackground().setLevel(CommonUtil.CODE_COLOR_NORMAL);
        }
    }

    /**
     * Show or hide password error layout
     *
     * @param show true if password error layout should be shown
     */
    private void showPassError(boolean show) {
        if (show) {
            passCorrected = false;
            mTrPassError.setVisibility(View.VISIBLE);
            mEdtPass.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
            mTrLinePassWord.getBackground().setLevel(CommonUtil.CODE_COLOR_ERROR);
        } else {
            mTrPassError.setVisibility(View.GONE);
            passCorrected = true;
            mEdtPass.setTextColor(getResources().getColor(R.color.colorBlack));
            mTrLinePassWord.getBackground().setLevel(CommonUtil.CODE_COLOR_NORMAL);
        }
    }

    /**
     * Show or hide account error layout
     *
     * @param show true if account error layout should be shown
     */
    private void showAccountError(boolean show) {
        if (show) {
            emailCorrected = CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME) != null;
            passCorrected = false;
            mTrPassError.setVisibility(View.VISIBLE);
            mEdtEmail.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
            mEdtPass.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
            mTrLinePassWord.getBackground().setLevel(CommonUtil.CODE_COLOR_ERROR);
            mEdtEmail.getBackground().setLevel(CommonUtil.CODE_COLOR_ERROR);
        } else {
            if (!CommonUtil.isNotEmail(mEdtEmail.getText().toString())) emailCorrected = true;
            if (!mEdtPass.getText().toString().isEmpty()) passCorrected = true;
            mTrPassError.setVisibility(View.GONE);
            mEdtEmail.setTextColor(getResources().getColor(R.color.colorBlack));
            mEdtPass.setTextColor(getResources().getColor(R.color.colorBlack));
            mTrLinePassWord.getBackground().setLevel(CommonUtil.CODE_COLOR_NORMAL);
            mEdtEmail.getBackground().setLevel(CommonUtil.CODE_COLOR_NORMAL);
        }
    }

    /**
     * Show or hide login text boxes & button top/bottom padding
     *
     * @param isShow true if there should be padding
     */
    private void showLine(boolean isShow) {
        if (isShow) {
            mTvTopLine.setVisibility(View.VISIBLE);
            mTvBottomLine.setVisibility(View.VISIBLE);
        } else {
            mTvTopLine.setVisibility(View.GONE);
            mTvBottomLine.setVisibility(View.GONE);
        }
    }

    /**
     * Check if a password valid/invalid according password rule
     * Check result will be stored in {@link #passCorrected}
     * Also UI will be updated according to valid/invalid password
     *
     * @param pass password to be checked (in {@link String}
     */
    private void validatePass(String pass) {
        if (pass.isEmpty()) {
            mBtSignIn.setEnabled(false);
            passCorrected = false;
        } else {
            passCorrected = true;
            mBtSignIn.setEnabled(emailCorrected);
            mProfileEntity.setPassword(pass);
            showPassError(false);
        }
    }

    /**
     * Get layout id for Login fragment
     *
     * @return layout id
     * @see BaseFragment#getLayoutId()
     */
    @Override
    protected int getLayoutId() {
        return R.layout.frg_m023_login;
    }

    /**
     * Get presenter for this fragment
     *
     * @return {@link M023LoginPresenter} instance
     * @see BaseFragment#getPresenter()
     */
    @Override
    protected M023LoginPresenter getPresenter() {
        return new M023LoginPresenter(this);
    }

    /**
     * @return tag in {@link String}
     * @see BaseFragment#getTAG()
     */
    @Override
    protected String getTAG() {
        return TAG;
    }

    /**
     * @see BaseFragment#defineBackKey()
     */
    @Override
    protected void defineBackKey() {
        if (getTagSource().equals(M001LandingPageFrg.TAG)) {
            CommonUtil.getInstance().defineBackTag(getTagSource(), TAG);
        }
    }

    /**
     * @see BaseFragment#backToPreviousScreen()
     */
    @Override
    public void backToPreviousScreen() {
        mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
    }

    /**
     * @param idView id of clicked view
     * @see BaseFragment#onClickView(int)
     */
    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m023_show:
                if (mTvShow.getText().toString().equals(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m023_show)))) {
                    mEdtPass.setInputType(InputType.TYPE_CLASS_TEXT);
                    mTvShow.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m023_hide)));

                } else {
                    mEdtPass.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    mEdtPass.setSelection(Objects.requireNonNull(mEdtPass.getText()).length());
                    mTvShow.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m023_show)));
                }
                CommonUtil.getInstance().initTypeFace(mEdtPass);
                break;

            case R.id.iv_m023_back:
                CommonUtil.getInstance().forceHideKeyBoard(mEdtEmail);
                CommonUtil.getInstance().forceHideKeyBoard(mEdtPass);
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
                break;

            case R.id.iv_m023_finger_print:
                if (CommonUtil.getInstance().getPrefContent(KEY_FIRST_TIME) == null
                        || CommonUtil.getInstance().getPrefContent(KEY_FIRST_TIME).equals(Boolean.FALSE + "")) {
                    // First time login
                    if (!CommonUtil.getInstance().hasEnrolledFingerprints()) {
                        //Don't have Fingerprint on phone
                        CommonUtil.getInstance().showDialog(mContext, R.string.txt_dialog,
                                R.string.txt_m23_first_time_login_fingerprint,
                                R.string.txt_m023_bt_ok,
                                R.string.txt_m023_bt_not_now, new OnOKDialogCallBack() {
                                    @Override
                                    public void handleOKButton1() {
                                        startActivity(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS));
                                    }
                                });
                    } else {
                        // Have fingerprint on phone
                        CommonUtil.getInstance().showDialog(mContext, R.string.txt_m23_signin_to_turn_on_fingerprint);
                    }
                } else {

                    if (!CommonUtil.getInstance().hasEnrolledFingerprints()) {
                        CommonUtil.getInstance().showDialog(mContext, R.string.txt_dialog,
                                R.string.txt_m23_first_time_login_fingerprint,
                                R.string.txt_m023_bt_ok,
                                R.string.txt_m023_bt_not_now, new OnOKDialogCallBack() {
                                    @Override
                                    public void handleOKButton1() {
                                        startActivity(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS));
                                    }
                                });
                    } else {
                        getStorage().setM001ProfileEntity(mProfileEntity);
                        if (mFingerprintDialog == null) {
                            mFingerprintDialog = new M023FingerPrintDialog(mContext, true);
                            mFingerprintDialog.setOnCallBack(this);
                        }
                        mFingerprintDialog.show();
                    }
                }
//                if (!CommonUtil.getInstance().hasEnrolledFingerprints()) {
//                    if (mEdtEmail.getText() == null || mEdtEmail.getText().toString().isEmpty() || !emailCorrected) {
//                        CommonUtil.getInstance().showDialog(mContext, R.string.txt_dialog,
//                                R.string.txt_m23_first_time_login_fingerprint,
//                                R.string.txt_m023_bt_ok,
//                                R.string.txt_m023_bt_not_now, new OnOKDialogCallBack() {
//                                    @Override
//                                    public void handleOKButton1() {
//                                        startActivity(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS));
//                                    }
//                                });
//                    } else if (passCorrected) {
//                        CommonUtil.getInstance().showDialog(mContext, R.string.txt_dialog,
//                                R.string.txt_m23_first_time_login_fingerprint,
//                                R.string.txt_m023_bt_ok,
//                                R.string.txt_m023_bt_not_now, new OnOKDialogCallBack() {
//                                    @Override
//                                    public void handleOKButton1() {
//                                        startActivity(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS));
//                                    }
//
////                                    @Override
////                                    public void handleOKButton2() {
////                                        mPresenter.callIA02AuthenticateUser(Objects.requireNonNull(mEdtEmail.getText()).toString(),
////                                                Objects.requireNonNull(mEdtPass.getText()).toString());
////                                    }
//                                });
//                    }
//                } else {
//                    String email = CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME);
//                    String pass = CommonUtil.getInstance().getPrefContent(CommonUtil.PASS);
//                    if (email == null || email.isEmpty() || pass == null || pass.isEmpty()) {
//                        CommonUtil.getInstance().showDialog(mContext, R.string.txt_m23_signin_to_turn_on_fingerprint);
//                    } else {
//                        getStorage().setM001ProfileEntity(mProfileEntity);
//                        if (mFingerprintDialog == null) {
//                            mFingerprintDialog = new M023FingerPrintDialog(mContext, true);
//                            mFingerprintDialog.setOnCallBack(this);
//                        }
//                        mFingerprintDialog.show();
//                    }
//                }
                break;

            case R.id.bt_m023_sign_in:
                mEdtEmail.clearFocus();
                mEdtPass.clearFocus();
                hideKeyboard(mContext);
                if (!CommonUtil.getInstance().hasEnrolledFingerprints()) {
                    CommonUtil.getInstance().showDialog(mContext, R.string.txt_dialog,
                            R.string.txt_m23_first_time_login_fingerprint,
                            R.string.txt_m023_bt_ok,
                            R.string.txt_m023_bt_not_now, new OnOKDialogCallBack() {
                                @Override
                                public void handleOKButton1() {
                                    startActivity(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS));
                                }

                                @Override
                                public void handleDialogDismiss() {
                                    doLogin();
                                }
                            });
                } else {
                    doLogin();
                }
                break;
            case R.id.tv_m023_forgot:
                CommonUtil.getInstance().forceHideKeyBoard(mEdtEmail);
                CommonUtil.getInstance().forceHideKeyBoard(mEdtPass);
                if (!CommonUtil.getInstance().isConnectToNetwork()) {
                    String msg = LangMgr.getInstance().getLangList().get(getString(R.string.lang_err_no_internet_reset_password));
                    CommonUtil.getInstance().showDialog(mContext, msg, null);
                    break;
                }
                mCallBack.showFrgScreen(TAG, M038RecoveryEmailFrg.TAG);
                break;
            case R.id.cb_m023_keep:
                doKeepMeLogin();
                break;
            default:
                break;
        }
    }

    /**
     * <b>doLogin</b><br/>
     * <br/> login into the app when user tap on "SignIn" button
     * <br/> This method handles using Fingerprint to login when user want to use it
     */
    private void doLogin() {
        CommonUtil.getInstance().forceHideKeyBoard(mEdtEmail);
        CommonUtil.getInstance().forceHideKeyBoard(mEdtPass);

        if (CommonUtil.getInstance().getIntPrefContent(M023LoginPresenter.FAIL_TIMES_KEY, 0) >= 6) {
            mCallBack.showFrgScreen(TAG, M038RecoveryEmailFrg.TAG);
            return;
        }

//        if (!CommonUtil.getInstance().hasEnrolledFingerprints()) {
//            CommonUtil.getInstance().showDialog(mContext, R.string.txt_dialog,
//                    R.string.txt_m23_first_time_login_fingerprint,
//                    R.string.txt_m023_bt_ok,
//                    R.string.txt_m023_bt_not_now, new OnOKDialogCallBack() {
//                        @Override
//                        public void handleOKButton1() {
//                            startActivity(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS));
//                        }
//
//                        @Override
//                        public void handleOKButton2() {
//                            mPresenter.callIA02AuthenticateUser(Objects.requireNonNull(mEdtEmail.getText()).toString(),
//                                    Objects.requireNonNull(mEdtPass.getText()).toString());
//                        }
//                    });
//        } else {
//            String isFirstTimeLogin = CommonUtil.getInstance().getPrefContent(KEY_FIRST_TIME);
//            String email = mEdtEmail.getText().toString();
//            String pass = mEdtPass.getText().toString();
//
//            if (!email.isEmpty() && !pass.isEmpty() && (isFirstTimeLogin == null || isFirstTimeLogin.equals(Boolean.FALSE + ""))) {
//                CommonUtil.getInstance().showDialog(mContext, getString(R.string.txt_dialog),
//                        String.format(getString(R.string.txt_m23_signin_using_fingerprint), email),
//                        getString(R.string.txt_m023_bt_ok),
//                        getString(R.string.txt_m023_bt_not_now), new OnOKDialogCallBack() {
//                            @Override
//                            public void handleOKButton1() {
//                                getStorage().setM001ProfileEntity(mProfileEntity);
//                                if (mFingerprintDialog == null) {
//                                    mFingerprintDialog = new M023FingerPrintDialog(mContext, true);
//                                    mFingerprintDialog.setOnCallBack(M023LoginFrg.this);
//                                }
//                                mFingerprintDialog.show();
//                            }
//
//                            @Override
//                            public void handleOKButton2() {
//                                mPresenter.callIA02AuthenticateUser(Objects.requireNonNull(mEdtEmail.getText()).toString(),
//                                        Objects.requireNonNull(mEdtPass.getText()).toString());
//                            }
//                        });
//
//                return;
//            }
//
//            mPresenter.callIA02AuthenticateUser(Objects.requireNonNull(mEdtEmail.getText()).toString(),
//                    Objects.requireNonNull(mEdtPass.getText()).toString());
//        }
        mPresenter.callIA02AuthenticateUser(Objects.requireNonNull(mEdtEmail.getText()).toString(),
                Objects.requireNonNull(mEdtPass.getText()).toString());
    }

    /**
     * <b>doKeepMeLogin</b><br/>
     * <br/> Save state of user's action when he check/uncheck on the "Keep me login" checkbox
     */
    private void doKeepMeLogin() {
        keep = CommonUtil.getInstance().getPrefContent(CommonUtil.KEEP);
        if (keep == null || !keep.equals(CommonUtil.KEEP)) {
            CommonUtil.getInstance().savePrefContent(CommonUtil.KEEP, CommonUtil.KEEP);
            return;
        }
        CommonUtil.getInstance().savePrefContent(CommonUtil.KEEP, "");
    }

    /**
     * Handle callback from {@link OnM023LoginCallBack}
     * {@link OnM023LoginCallBack#showM001LandingScreen(String err)}
     *
     * @param sms error string description
     */
    @Override
    public void showM001LandingScreen(String sms) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, sms);
    }

    /**
     * Handle callback from {@link OnM023LoginCallBack}
     * {@link OnM023LoginCallBack#loginSuccess()}
     */
    @Override
    public void loginSuccess() {
        autoKeepLoginSection(CommonUtil.getInstance().getPrefContent(CommonUtil.KEEP) != null && CommonUtil.getInstance().getPrefContent(CommonUtil.KEEP).equalsIgnoreCase(CommonUtil.KEEP));
        CAApplication.getInstance().startSyncService();
        if (Objects.requireNonNull(mEdtEmail.getText().toString()).equals(CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME))) {
            mCallBack.showFrgScreen(TAG, M024DoseLogFrg.TAG);
            CommonUtil.getInstance().savePrefContent(CommonUtil.IS_LOGIN, CommonUtil.IS_LOGIN_TRUE);
            CommonUtil.getInstance().savePrefContent(CommonUtil.USER_NAME, Objects.requireNonNull(mEdtEmail.getText()).toString());
            CommonUtil.getInstance().savePrefContent(CommonUtil.PASS, Objects.requireNonNull(mEdtPass.getText()).toString());
        } else {
            getStorage().setM075UserLoginInfo(new TextEntity(mEdtEmail.getText().toString(), mEdtPass.getText().toString()));
            mCallBack.showFrgScreen(TAG, M076FirstLoginCodeFrg.TAG);
        }
    }

    /**
     * set auto-Keep-Login-Section flag
     *
     * @param keepLoggedIn true if currently date time should be saved for auto-keep-login-section feature
     */
    private void autoKeepLoginSection(boolean keepLoggedIn) {
        if (keepLoggedIn) {
            CommonUtil.getInstance().savePrefContent(CommonUtil.LOGIN_SECTION_START_DATE, CommonUtil.getDateNow(CommonUtil.DATE_NOW_DY));
        } else {
            CommonUtil.getInstance().savePrefContent(CommonUtil.LOGIN_SECTION_START_DATE, CommonUtil.LOGIN_SECTION_DO_NOT_KEEP);
        }
    }

    /**
     * Handle callback from {@link OnM023LoginCallBack}
     * {@link OnM023LoginCallBack#updateFingerPrintResult()}
     */
    @Override
    public void updateFingerPrintResult() {
        String p = CommonUtil.getInstance().getPrefContent(CommonUtil.PASS);
        if (p != null && !p.isEmpty()) {
            mEdtPass.setText(p);
        }

        String pass = mEdtPass.getText().toString();
        if (pass.isEmpty()) {
            showNotify(R.string.txt_finger_print_first);
            showNotify(LangMgr.getInstance().getLangList().get(getString(R.string.lang_err_first_login)));
            return;
        }

        mEdtPass.setText(pass);
        mFingerprintDialog.dismiss();
        mPresenter.callIA02AuthenticateUser(Objects.requireNonNull(mEdtEmail.getText()).toString(), Objects.requireNonNull(mEdtPass.getText()).toString());
    }

    /**
     * Handle callback from {@link OnM023LoginCallBack}
     *
     * @param txt string id of message text which will be shown (txt)
     * @param tag dialog tag
     * @see OnM023LoginCallBack#showDialog(int, String)
     */
    @Override
    public void showDialog(int txt, String tag) {
        showDialog(getString(txt), tag);
    }

    /**
     * @param txt string which will be shown (txt)
     * @param tag dialog tag
     * @see OnM023LoginCallBack#showDialog(String, String)
     */
    @Override
    public void showDialog(String txt, String tag) {
        CommonUtil.getInstance().showDialog(mContext, txt, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton1() {
                if (tag.equals(M023LoginPresenter.KEY_API_IA02_AUTHENTICATE_USER)) {
                    mCallBack.showFrgScreen(tag, M023LoginFrg.TAG);
                }
                if (tag.equals(M023LoginPresenter.KEY_BLOCKED_BY_COUNTRY)) {
                    getStorage().setAppActive(true);
                    mCallBack.showFrgScreen(tag, M001LandingPageFrg.TAG);
                }
            }
        });
    }

    /**
     * Handle callback from {@link OnM023LoginCallBack}
     *
     * @see OnM023LoginCallBack#showM001LandingFrg
     */
    @Override
    public void showM001LandingFrg(int recallCode) {
        switch (recallCode) {
            case CommonUtil.RECALL_COUNTRY_VERSION:
                String versionApp = "";
                try {
                    PackageInfo pInfo = CAApplication.getInstance().getPackageManager().getPackageInfo(CAApplication.getInstance().getPackageName(), 0);
                    versionApp = pInfo.versionName;

                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                String country = getStorage().getM007UserEntity().getCountry();
                String textVersion = getString(R.string.txt_m023_recall_country_version).replace("${dialoq_version}", versionApp).replace("${country_name}", country);

                CommonUtil.getInstance().showDialog(mContext, textVersion, new OnOKDialogCallBack() {
                    @Override
                    public void handleOKButton1() {
                        mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
                    }
                });
                getStorage().setAppActive(false);
                mCallBack.cancelReminder();
                break;
            case CommonUtil.RECALL_COUNTRY:
                String recallCountry = getStorage().getM007UserEntity().getCountry();
                String textCountry = getString(R.string.txt_m023_recall_country).replace("${country_name}", recallCountry);
                CommonUtil.getInstance().showDialog(mContext, textCountry, new OnOKDialogCallBack() {
                    @Override
                    public void handleOKButton1() {
                        mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
                    }
                });
                getStorage().setAppActive(false);
                mCallBack.cancelReminder();
                break;
            default:
                break;
        }
    }

    /**
     * Handle callback from {@link OnM023LoginCallBack}
     *
     * @see OnM023LoginCallBack#wrongAccount
     */
    @Override
    public void wrongAccount() {
        mTvPassError.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_err_ia2_err_401)));
        showAccountError(true);
        mBtSignIn.setEnabled(false);
    }
}
