package com.novo.app.view.widget.novocharts.configs;

public class NovoBarDetailsConfig {

    private int firstBarColor;
    private int secondBarColor;
    private int firstValueColor;
    private int secondValueColor;
    private boolean isBothType;

    public int getFirstBarColor() {
        return firstBarColor;
    }

    public void setFirstBarColor(int firstBarColor) {
        this.firstBarColor = firstBarColor;
    }

    public int getSecondBarColor() {
        return secondBarColor;
    }

    public void setSecondBarColor(int secondBarColor) {
        this.secondBarColor = secondBarColor;
    }

    public int getFirstValueColor() {
        return firstValueColor;
    }

    public void setFirstValueColor(int firstValueColor) {
        this.firstValueColor = firstValueColor;
    }

    public int getSecondValueColor() {
        return secondValueColor;
    }

    public void setSecondValueColor(int secondValueColor) {
        this.secondValueColor = secondValueColor;
    }

    public boolean isBothType() {
        return isBothType;
    }

    public void setBothType(boolean bothType) {
        isBothType = bothType;
    }
}
