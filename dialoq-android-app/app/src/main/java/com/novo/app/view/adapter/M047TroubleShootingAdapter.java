package com.novo.app.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.TopicEntity;
import com.novo.app.view.event.OnM047SupportTroubleshootingCallBack;

import java.util.List;

public class M047TroubleShootingAdapter extends BaseRecycleAdapter<OnM047SupportTroubleshootingCallBack, TopicEntity, M047TroubleShootingAdapter.TopicHolder> implements OnM047SupportTroubleshootingCallBack {
    public static final String TAG = M047TroubleShootingAdapter.class.getName();

    public M047TroubleShootingAdapter(Context mContext, List<TopicEntity> mListData, OnM047SupportTroubleshootingCallBack mCallBack) {
        super(mContext, mListData, mCallBack);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_m047_troubleshooting_topic;
    }

    @Override
    protected TopicHolder getViewHolder(int viewType, View itemView) {
        return new TopicHolder(itemView);
    }

    @Override
    public void showM001LandingScreen(String err) {
        mCallBack.showM001LandingScreen(err);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        TopicHolder item = (TopicHolder) holder;

        TopicEntity data = mListData.get(position);
        item.tvTopic.setText(LangMgr.getInstance().getLangList().get(data.getTopicKey()));
    }

    @Override
    public void toSingleQA() {
    }

    @Override
    public void toMultipleQA() {

    }

    public class TopicHolder extends BaseHolder {
        TableRow trTopic;
        TextView tvTopic;
        ImageView ivArrow;

        TopicHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void onClickView(int idView) {
            switch (idView) {
                case R.id.tr_m047_topic:
                case R.id.iv_m047_arrow:
                case R.id.tv_m047_topic:
                    if (!mListData.get(getAdapterPosition()).isManyQuestion()) {
                        mCallBack.toSingleQA();
                    } else {
                        mCallBack.toMultipleQA();
                    }
                    getStorage().setM047ItemPosition(getAdapterPosition());
                    break;
            }
        }

        @Override
        protected void initView() {
            trTopic = findViewById(R.id.tr_m047_topic, this);
            ivArrow = findViewById(R.id.iv_m047_arrow, this);
            tvTopic = findViewById(R.id.tv_m047_topic, this, CAApplication.getInstance().getRegularFont());
        }
    }

}