package com.novo.app.view.event;

public interface OnM041ReminderAddCallBack extends OnCallBackToView{
    void reminderAdded();
}
