package com.novo.app.model.entities;

import com.google.gson.annotations.SerializedName;
import com.novo.app.model.BaseModel;

public class DecryptionKeyEntity extends BaseModel {
    @SerializedName("systemId")
    private String systemId;
    @SerializedName("decryptionKey")
    private String decryptionKey;

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public String getDecryptionKey() {
        return decryptionKey;
    }

    public void setDecryptionKey(String decryptionKey) {
        this.decryptionKey = decryptionKey;
    }
}
