package com.novo.app.model.entities;

public class DoseTimingEntity {
    private String time;
    private int doseCount;

    public DoseTimingEntity(String time, int doseCount) {
        this.time = time;
        this.doseCount = doseCount;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getDoseCount() {
        return doseCount;
    }

    public void setDoseCount(int doseCount) {
        this.doseCount = doseCount;
    }
}
