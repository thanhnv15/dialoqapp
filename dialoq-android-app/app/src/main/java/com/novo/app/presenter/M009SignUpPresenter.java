package com.novo.app.presenter;

import com.novo.app.view.event.OnM009SignUpCallBack;

public class M009SignUpPresenter extends BasePresenter<OnM009SignUpCallBack> {
    public M009SignUpPresenter(OnM009SignUpCallBack event) {
        super(event);
    }
}
