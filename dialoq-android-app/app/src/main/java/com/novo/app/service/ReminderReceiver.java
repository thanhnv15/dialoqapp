/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.format.DateUtils;

import com.novo.app.utils.CommonUtil;
import com.novo.app.view.widget.NVDateFormat;
import com.novo.app.view.widget.NVDateUtils;

import java.util.Calendar;
import java.util.Date;

public class ReminderReceiver extends BroadcastReceiver {
    private static final String TAG = ReminderReceiver.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        int hour = CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_HOUR, -1);
        int min = CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_MIN, -1);
        long timeSet = CommonUtil.getInstance().getLongPrefContent(CommonUtil.REMINDER_TIME_SET, -1);
        long today = CommonUtil.getInstance().getLongPrefContent(CommonUtil.REMINDER_TODAY, -1);

        CommonUtil.wtfi(TAG, CommonUtil.getSDate(timeSet, CommonUtil.TIME_STYLE));

        String lastTresibaDose = CommonUtil.getInstance().getPrefContent(CommonUtil.LAST_TRESIBA_DOSE);
        Date dateOfLastDose = NVDateUtils.stringToDateLocal(lastTresibaDose, NVDateFormat.TIME_RANGE_FORMAT);

        Calendar c = Calendar.getInstance();
        c.setTime(dateOfLastDose);
        c.add(Calendar.HOUR_OF_DAY, 8);


        if (DateUtils.isToday(timeSet) && (timeSet < c.getTimeInMillis() || timeSet < today)) {
            CommonUtil.wtfi(TAG, "DON'T PUSH REMINDER: " + hour + ":" + min);
        } else if (System.currentTimeMillis() > c.getTimeInMillis()) {
            c = Calendar.getInstance();
            int currentHour = c.get(Calendar.HOUR_OF_DAY);
            int currentMin = c.get(Calendar.MINUTE);

            if (currentHour == hour && currentMin == min || currentHour == hour && currentMin == min + 1) {
                CommonUtil.getInstance().showNotification(context);
            }
        }
    }
}
