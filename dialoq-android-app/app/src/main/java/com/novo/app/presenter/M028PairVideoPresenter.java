package com.novo.app.presenter;

import com.novo.app.view.event.OnM028PairVideoCallBack;

public class M028PairVideoPresenter extends BasePresenter<OnM028PairVideoCallBack> {
    public M028PairVideoPresenter(OnM028PairVideoCallBack event) {
        super(event);
    }
}
