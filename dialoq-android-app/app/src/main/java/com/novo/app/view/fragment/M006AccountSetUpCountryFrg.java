package com.novo.app.view.fragment;

import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.CountryEntity;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.presenter.M006AccountSetUpCountryPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.CASpinnerAdapter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM006AccountSetUpCountryCallBack;
import com.novo.app.view.event.OnSpinnerCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

import java.util.ArrayList;
import java.util.List;

public class M006AccountSetUpCountryFrg extends BaseFragment<M006AccountSetUpCountryPresenter, OnHomeBackToView> implements OnM006AccountSetUpCountryCallBack, OnSpinnerCallBack {

    public static final String TAG = M006AccountSetUpCountryFrg.class.getName();

    private static final int KEY_SPINNER_DATE_RANGER_TYPE = 2;
    private Spinner mSpinnerCountry;
    private TextView mTvCountry;
    private Button mBtNext;
    private boolean isItemClicked;

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m006_account_setup, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m006_where_live, CAApplication.getInstance().getRegularFont());
        mBtNext = findViewById(R.id.bt_m006_next, this, CAApplication.getInstance().getBoldFont());
        mBtNext.setEnabled(false);
        findViewById(R.id.tv_m006_cancel, this, CAApplication.getInstance().getMediumFont());

        mTvCountry = findViewById(R.id.tv_m006_country, this, CAApplication.getInstance().getRegularFont());
        String hint = LangMgr.getInstance().getLangList().get(mTvCountry.getContentDescription());
        if (getStorage().getM001ProfileEntity().getCountry() != null) {
            mTvCountry.setText(getStorage().getM001ProfileEntity().getCountry().getValue());
            mTvCountry.setTypeface(CAApplication.getInstance().getBoldFont());
        }

        if (!mTvCountry.getText().toString().equals(hint)) mBtNext.setEnabled(true);
        findViewById(R.id.iv_m006_country, this);
        findViewById(R.id.iv_m006_back, this);

        mSpinnerCountry = findViewById(R.id.spin_m006_date_range);
        CASpinnerAdapter<OnSpinnerCallBack> adapter = new CASpinnerAdapter<>(KEY_SPINNER_DATE_RANGER_TYPE,
                makeCountriesList(), mContext);
        adapter.setOnSpinnerCallBack(this);
        mSpinnerCountry.setAdapter(adapter);
    }

    private List<TextEntity> makeCountriesList() {
        List<TextEntity> countriesList = new ArrayList<>();
        List<CountryEntity> data = getStorage().getM001ConfigSet().getCountryEntity();
        for (int i = 0; i < data.size(); i++) {
            countriesList.add(new TextEntity(data.get(i).getCountryCode().getValue(), data.get(i).getCountryName().getValue()));
        }
        return countriesList;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m006_account_set_up_country;
    }

    @Override
    protected M006AccountSetUpCountryPresenter getPresenter() {
        return new M006AccountSetUpCountryPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m006_back:
                backToPreviousScreen();
                break;
            case R.id.tv_m006_country:
            case R.id.iv_m006_country:
                mSpinnerCountry.performClick();
                break;
            case R.id.bt_m006_next:
                if (isItemClicked) {
                    getStorage().getM001ProfileEntity().setCountry((TextEntity) mTvCountry.getTag());
                    setUserCountryEntity(((TextEntity) mTvCountry.getTag()).getKey());
                }
                mCallBack.showFrgScreen(TAG, M017ProfileBornFrg.TAG);
                break;
            case R.id.tv_m006_cancel:
                mCallBack.registrationCancel(TAG);
                break;
            default:
                break;
        }
    }

    private void setUserCountryEntity(String key) {
        List<CountryEntity> data = getStorage().getM001ConfigSet().getCountryEntity();
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getCountryCode().getValue().equals(key)) {
                getStorage().setUserCountryEntity(data.get(i));
                return;
            }
        }
    }

    @Override
    public void backToPreviousScreen() {
        mCallBack.showFrgScreen(TAG, M004AccountSetUpFrg.TAG);
    }

    @Override
    public void onItemClick(int key, int pos, Object data) {
        isItemClicked = true;
        if (key == KEY_SPINNER_DATE_RANGER_TYPE) {
            TextEntity entity = ((List<TextEntity>) data).get(pos);
            mTvCountry.setText(entity.getValue());
            mTvCountry.setTag(entity);

            mTvCountry.setTypeface(CAApplication.getInstance().getBoldFont());
            mBtNext.setEnabled(true);
            closeSpinner(mSpinnerCountry);
        }
    }
}
