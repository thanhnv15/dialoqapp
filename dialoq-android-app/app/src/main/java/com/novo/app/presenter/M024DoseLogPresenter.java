package com.novo.app.presenter;

import com.novo.app.view.event.OnM024DoseLogCallBack;

public class M024DoseLogPresenter extends BasePresenter<OnM024DoseLogCallBack> {

    public M024DoseLogPresenter(OnM024DoseLogCallBack event) {
        super(event);
    }
}
