package com.novo.app.model.entities;

import com.google.gson.annotations.SerializedName;
import com.novo.app.model.BaseModel;

import java.io.Serializable;
import java.util.List;

public class CarePlanTemplateEntity extends BaseModel {
    @SerializedName("totalCount")
    private int totalCount;
    @SerializedName("data")
    private List<DataInfo> data;
    @SerializedName("paging")
    private PageInfo pageInfo;

    @SerializedName("id")
    private IdInfo id;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<DataInfo> getData() {
        return data;
    }

    public void setData(List<DataInfo> data) {
        this.data = data;
    }

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public IdInfo getId() {
        return id;
    }

    public void setId(IdInfo id) {
        this.id = id;
    }

    public class DataInfo implements Serializable {

        @SerializedName("careplanTemplateId")
        private String careplanTemplateId;
        @SerializedName("name")
        private String name;
        @SerializedName("description")
        private String description;

        public String getCareplanTemplateId() {
            return careplanTemplateId;
        }

        public void setCareplanTemplateId(String careplanTemplateId) {
            this.careplanTemplateId = careplanTemplateId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    private class PageInfo implements Serializable {
        @SerializedName("first")
        private String first;
        @SerializedName("previous")
        private String previous;
        @SerializedName("next")
        private String next;
        @SerializedName("last")
        private String last;

        public String getFirst() {
            return first;
        }

        public void setFirst(String first) {
            this.first = first;
        }

        public String getPrevious() {
            return previous;
        }

        public void setPrevious(String previous) {
            this.previous = previous;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public String getLast() {
            return last;
        }

        public void setLast(String last) {
            this.last = last;
        }
    }

    private class IdInfo implements Serializable {
        @SerializedName("userDid")
        private int userDid;
        @SerializedName("careplanTemplateId")
        private int careplanTemplateId;

        public int getUserDid() {
            return userDid;
        }

        public void setUserDid(int userDid) {
            this.userDid = userDid;
        }

        public int getCareplanTemplateId() {
            return careplanTemplateId;
        }

        public void setCareplanTemplateId(int careplanTemplateId) {
            this.careplanTemplateId = careplanTemplateId;
        }
    }
}
