package com.novo.app.view.event;

public interface OnM063EditNoteCallBack extends OnCallBackToView{
    void refreshDataM025();

    void showEditSuccess();

    void showEditFailed();

    void showEditHasBeenDeleted();
}
