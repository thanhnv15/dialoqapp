package com.novo.app.view.event;

import com.novo.app.model.entities.TextEntity;

public interface OnM002SignUpChooseLanguageCallBack extends OnCallBackToView {
    void clickOnItem(TextEntity mData);

    void updateView();

    void revertChoice();
}
