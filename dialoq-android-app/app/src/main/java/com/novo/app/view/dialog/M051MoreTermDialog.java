/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.view.dialog;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M051MoreMandatoryPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.utils.NetworkChangeReceiver;
import com.novo.app.view.base.BaseDialog;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.event.OnM051MoreCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.widget.NVDateUtils;

public class M051MoreTermDialog extends BaseDialog<M051MoreMandatoryPresenter, OnM051MoreCallBack> implements OnCallBackToView, OnM051MoreCallBack {
    private static final String TAG = M051MoreTermDialog.class.getName();
    private TextView mTvAgreeDate;
    private Button mBtWithdraw;

    public M051MoreTermDialog(Context context, boolean isCancel) {
        super(context, isCancel, R.style.dialog_style);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getStorage().addHomeCallBack(TAG, this::preventWithdraw);
    }

    private void preventWithdraw(Object data) {
        if (data == null) {
            return;
        }
        if (mBtWithdraw.getVisibility() == View.VISIBLE) {
            switch ((String) data) {
                case NetworkChangeReceiver.ACTION_NETWORK_RESTORED:
                    enableWithDrawButton(true);
                    break;
                case NetworkChangeReceiver.ACTION_NETWORK_LOST:
                    enableWithDrawButton(false);
                    break;
            }
        }
    }

    private void enableWithDrawButton(boolean enable) {
        if (enable) {
            mBtWithdraw.setEnabled(true);
            mBtWithdraw.setTextColor(mContext.getResources().getColor(R.color.colorM001Cyan));
        } else {
            mBtWithdraw.setEnabled(false);
            mBtWithdraw.setTextColor(mContext.getResources().getColor(R.color.colorGraySuperLight));
        }
    }

    @Override
    public void showM001LandingScreen(String err) {
        mCallBack.showM001LandingScreen(err);
    }

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m051_back, this);
        findViewById(R.id.tv_m051_title, CAApplication.getInstance().getMediumFont());

        TextView mTvLastUpdate = findViewById(R.id.tv_m051_last_update, CAApplication.getInstance().getRegularFont());
        mTvLastUpdate.setTypeface(mTvLastUpdate.getTypeface(), Typeface.ITALIC);
        TextView mTvContent = findViewById(R.id.tv_m051_content, CAApplication.getInstance().getRegularFont());

        if (getStorage().getUserCountryEntity() != null) {
            String lastUpdate;
            String content;
            if (CommonUtil.getInstance().isMature()) {
                lastUpdate = getStorage().getUserCountryEntity().getAdultUpdate().getValue();
                content = getStorage().getUserCountryEntity().getAdultTerm().getValue();
            } else {
                lastUpdate = getStorage().getUserCountryEntity().getChildUpdate().getValue();
                content = getStorage().getUserCountryEntity().getChildTerm().getValue();
            }
            content = LangMgr.getInstance().getLangList().get(content);
            mTvLastUpdate.setText(getLastUpdate(mTvLastUpdate.getContentDescription().toString(), lastUpdate));
            mTvContent.setText(content);
        }

        mBtWithdraw = findViewById(R.id.bt_m051_withdraw, this, CAApplication.getInstance().getMediumFont());
        if (getStorage().getM007UserEntity() != null && getStorage().getM007UserEntity().getRegistrationDate() != null) {
            mTvAgreeDate = findViewById(R.id.tv_m051_tc_agree_date, CAApplication.getInstance().getBoldFont());
            mTvAgreeDate.setVisibility(View.VISIBLE);
            mTvAgreeDate.setText(getAgreeDate());

            mBtWithdraw.setVisibility(View.VISIBLE);
            enableWithDrawButton(CommonUtil.getInstance().isConnectToNetwork());

            TextView mTvNote = findViewById(R.id.tv_m051_note, CAApplication.getInstance().getRegularFont());
            mTvNote.setVisibility(View.VISIBLE);
            String highLight = mContext.getString(R.string.txt_m051_note_highlight);
            highLightText(mTvNote, highLight, R.color.colorBlack, true);
        }
    }

    private String getLastUpdate(String placeHolder, String lastUpdate) {
        placeHolder = LangMgr.getInstance().getLangList().get(placeHolder);
        String date = NVDateUtils.getLastUpdatedTerm(lastUpdate);
        placeHolder = placeHolder.substring(0, placeHolder.indexOf('$')) + date;
        return placeHolder;
    }

    private String getAgreeDate() {
        String agreeDate = mTvAgreeDate.getText().toString();
        String date = getStorage().getM007UserEntity().getRegistrationDate();
        date = date.replace("Z", "+0000");
        date = NVDateUtils.getRegistrationDate(date);
        agreeDate = agreeDate.substring(0, agreeDate.indexOf('$')) + date;
        return agreeDate;
    }

    @Override
    public int getLayoutId() {
        return R.layout.view_m051_more_term;
    }

    @Override
    protected M051MoreMandatoryPresenter getPresenter() {
        return new M051MoreMandatoryPresenter(this);
    }

    @Override
    public void backToPreviousScreen() {
        // This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        if (idView == R.id.iv_m051_back) {
            dismiss();
        } else if (idView == R.id.bt_m051_withdraw) {
            String title = LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m051_mandatory_revoke_title));
            String msg = "";
            if (!CommonUtil.getInstance().isMature())
                msg = LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m051_mandatory_revoke_message));
            String yes = LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m051_mandatory_revoke_yes));
            String no = LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m051_mandatory_revoke_no));
            CommonUtil.getInstance().showDialog(mContext, title, msg, yes, no, new OnOKDialogCallBack() {
                @Override
                public void handleOKButton1() {
                    mPresenter.callDA01DeleteUserAccount();
                }
            });
        }
    }

    @Override
    public void deleteSuccess() {
        mCallBack.deleteSuccess();
        dismiss();
    }
}
