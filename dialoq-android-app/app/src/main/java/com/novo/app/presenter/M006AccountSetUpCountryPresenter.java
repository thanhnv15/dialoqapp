package com.novo.app.presenter;

import com.novo.app.view.event.OnM006AccountSetUpCountryCallBack;

public class M006AccountSetUpCountryPresenter extends BasePresenter<OnM006AccountSetUpCountryCallBack> {
    public M006AccountSetUpCountryPresenter(OnM006AccountSetUpCountryCallBack event) {
        super(event);
    }
}
