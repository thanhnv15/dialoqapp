package com.novo.app.model.entities;

import com.google.gson.GsonBuilder;

import java.io.Serializable;
import java.util.List;

public class ShareDataEntity implements Serializable {

    private Info info;
    private List<DateData> dateData;
    private List<NoteData> noteData;
    private boolean isEmpty;

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public List<DateData> getDateData() {
        return dateData;
    }

    public void setDateData(List<DateData> dateData) {
        this.dateData = dateData;
    }

    public List<NoteData> getNoteData() {
        return noteData;
    }

    public void setNoteData(List<NoteData> noteData) {
        this.noteData = noteData;
    }

    public String getJson() {
        return new GsonBuilder().create().toJson(this, ShareDataEntity.class);
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }

    public class Info implements Serializable {
        private String email;
        private String name;
        private String startDate;
        private String endDate;
        private String takenPercent;
        private String timeEarlyTaken;
        private String timeLateTaken;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTakenPercent() {
            return takenPercent;
        }

        public void setTakenPercent(String takenPercent) {
            this.takenPercent = takenPercent;
        }

        public String getTimeEarlyTaken() {
            return timeEarlyTaken;
        }

        public void setTimeEarlyTaken(String timeEarlyTaken) {
            this.timeEarlyTaken = timeEarlyTaken;
        }

        public String getTimeLateTaken() {
            return timeLateTaken;
        }

        public void setTimeLateTaken(String timeLateTaken) {
            this.timeLateTaken = timeLateTaken;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }
    }

    public class DateData implements Serializable {
        private String date;
        private String dateNum;
        private List<Unit> unit;
        private String note;
        private String priming;
        private String error;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getDateNum() {
            return dateNum;
        }

        public void setDateNum(String dateNum) {
            this.dateNum = dateNum;
        }

        public List<Unit> getUnit() {
            return unit;
        }

        public void setUnit(List<Unit> unit) {
            this.unit = unit;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getPriming() {
            return priming;
        }

        public void setPriming(String priming) {
            this.priming = priming;
        }

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }

        public class Unit implements Serializable{
            private String value;
            private String time;

            public Unit() {
            }

            public Unit(String value, String time) {
                this.value = value;
                this.time = time;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }
        }
    }

    public class NoteData implements Serializable {
        private String date;
        private String note;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }
    }
}
