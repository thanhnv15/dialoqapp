package com.novo.app.view.event;

public interface OnM058MoreCommunicationCallBack extends OnCallBackToView{
    void updateUserChoice();

    void showLoginScreen();

    void showDialog();

    void stateReverse();
}
