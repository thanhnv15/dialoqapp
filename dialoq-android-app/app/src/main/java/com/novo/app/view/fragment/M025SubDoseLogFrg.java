/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.view.fragment;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.ResultInfo;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.presenter.M025SubDoseLogPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.DoseLogAdapter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.dialog.M062AddNoteDialog;
import com.novo.app.view.dialog.M063DetailNoteDialog;
import com.novo.app.view.event.OnM002SignUpChooseLanguageCallBack;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM025SubDoseLogCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.widget.NVDateUtils;
import com.novo.app.view.widget.ProgressLoading;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;

import static com.novo.app.service.SyncScheduleService.KEY_SYNC_READY;

/**
 * Sub Dose Log Fragment
 * <p>
 * This fragment is 'View' object in MVP pattern
 * This fragment uses an {@link M025SubDoseLogPresenter} as linked Presenter
 * Callbacks to this fragment will be defined by {@link OnM025SubDoseLogCallBack} interface
 * <p>
 * This fragment is use for display user dose log
 */
public class M025SubDoseLogFrg extends BaseFragment<M025SubDoseLogPresenter, OnM024DoseLogCallBack>
        implements OnM025SubDoseLogCallBack, OnM002SignUpChooseLanguageCallBack {
    public static final String TAG = M025SubDoseLogFrg.class.getName();
    public static final String KEY_FIRST_TIME_OPEN_APP = "KEY_FIRST_TIME_OPEN_APP";
    private static final String VIDEO_INTRO_PATH = "video/Novo_Nordisk_Dialoq_video_1.mp4";
    private static final String VIDEO_INTRO_TITLE = "Introduce about DoseTracker";

    private static final int ALERT_NOT_PAIR = 0;
    private static final int ALERT_EMPTY_LOG = 1;
    private static final int ALERT_OUT_OF_DATE_LOG = 2;
    private static final int ALERT_EOL_WARNING = 3;
    private static final int ALERT_EOL = 4;
    private static final int ALERT_UPDATING = 5;
    private static final int ALERT_UPDATED = 6;
    private static final CharSequence X_DAYS = "30";
    private RecyclerView mRvDoseLog;
    private SwipyRefreshLayout mRefresh;
    private LinearLayout lnAlert;
    private DataInfo emptyData = new DataInfo();

    /**
     * Initiate views component for Sub Dose Log Fragment
     *
     * @see BaseFragment#initViews()
     */
    @Override
    protected void initViews() {
        CommonUtil.wtfd(TAG, "initViews...M25");
        getStorage().setM042CheckIn(false);
        mCallBack.changeTab(CommonUtil.TAB_MENU_DOSE_LOG);
        findViewById(R.id.tv_m025_title, CAApplication.getInstance().getBoldFont());
        lnAlert = findViewById(R.id.ln_m025_alert_holder);

        mRvDoseLog = findViewById(R.id.rv_m025_dose_log);
        mRvDoseLog.setLayoutManager(new LinearLayoutManager(mContext));

        findViewById(R.id.tr_m025_add, this);
        findViewById(R.id.iv_m025_add, this);
        findViewById(R.id.tv_m025_add, this, CAApplication.getInstance().getBoldFont());
        mRefresh = findViewById(R.id.sw_refresh);
        mRefresh.setColorSchemeResources(R.color.colorPrimaryDark);
        mRefresh.setOnRefreshListener((direction) -> new Handler().post(() -> {
            if (direction == SwipyRefreshLayoutDirection.TOP) {
                pullToRefresh();
            } else {
                if (CommonUtil.getInstance().isConnectToNetwork()) {
                    mPresenter.loadMore();
                }
            }
        }));

        if (!CommonUtil.getInstance().isConnectToNetwork()) {
            refreshData();
        } else {
            if (CommonUtil.getInstance().getPrefContent(KEY_FIRST_TIME_OPEN_APP) == null) {
                refreshData();

                mPresenter.initTime(false);
                pullToRefresh();
                CommonUtil.getInstance().savePrefContent(KEY_FIRST_TIME_OPEN_APP, "true");
            } else {
                refreshData();
            }
        }
    }

    /**
     * @param savedInstanceState
     * @see Fragment#onCreate(Bundle)
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonUtil.wtfi(TAG, "onCreate...");
    }

    /**
     * Handle the action when the user pull down the dose log to refresh data
     */
    private void pullToRefresh() {
        String key = CommonUtil.getInstance().getPrefContent(KEY_SYNC_READY);
        if (key != null && key.equals("true")) {
            CommonUtil.wtfe(TAG, "Syncing data is going on!, don't pull to refresh");
            showNotify(R.string.txt_err_sync_not_ready);
            mRefresh.setRefreshing(false);
            return;
        }

        if (getStorage().getCallBackToSync() != null) {
            getStorage().getCallBackToSync().onCallBack(null);

            new Handler().postDelayed(() -> {
                if (mRefresh.isRefreshing()) {
                    mRefresh.setRefreshing(false);
                }
            }, 5000);
        } else {
            refreshData();
        }
    }

    /**
     * @see Fragment#onResume()
     */
    @Override
    public void onResume() {
        getStorage().setM0DoseCallBack(TAG, data -> {
            try {
                initAlert(ALERT_UPDATING);
                if (data == null || !((boolean) data)) {
                    reloadData();
                } else {
                    refreshData();
                }
            } catch (Exception e) {
                //JUST ignore temporary
                e.printStackTrace();
            }
        });
        super.onResume();
    }

    /**
     * Handle callback from {@link OnM025SubDoseLogCallBack}
     * {@link com.novo.app.view.event.OnCallBackToView#showM001LandingScreen(String)}
     *
     * @param message error string description
     */
    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    /**
     * Get layout id for Sub Dose Log Fragment
     *
     * @return layout id
     * @see BaseFragment#getLayoutId()
     */
    @Override
    protected int getLayoutId() {
        return R.layout.frg_m025_dose_log_drug;
    }

    /**
     * Get presenter for this fragment
     *
     * @return {@link M025SubDoseLogPresenter} instance
     * @see BaseFragment#getPresenter()
     */
    @Override
    protected M025SubDoseLogPresenter getPresenter() {
        return new M025SubDoseLogPresenter(this);
    }

    /**
     * @return tag in {@link String}
     * @see BaseFragment#getTAG()
     */
    @Override
    protected String getTAG() {
        return TAG;
    }

    /**
     * @see BaseFragment#defineBackKey()
     */
    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    /**
     * @param idView id of clicked view
     * @see BaseFragment#onClickView(int)
     */
    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m025_show_me:
                mCallBack.showM029PairScreen();
                break;
            case R.id.tv_m025_see_how:
            case R.id.tv_m025_guide_normal:
            case R.id.iv_m025_normal_arrow:
            case R.id.tr_m025_normal_alert:
                if (CommonUtil.getInstance().isBluetoothOn()) {
                    getStorage().setM025VideoData(new String[]{VIDEO_INTRO_PATH, VIDEO_INTRO_TITLE});
                    mCallBack.showChildFrgScreen(TAG, M025ShowMeHowVideoFrg.TAG);
                } else {
                    CommonUtil.getInstance().checkBluetoothOn(mContext);
                }
                break;
            case R.id.iv_m025_close_alert:
                closeAlert(lnAlert);
                break;
            case R.id.tv_m025_add:
            case R.id.tr_m025_add:
            case R.id.iv_m025_add:
                M062AddNoteDialog mAddNoteDialog = new M062AddNoteDialog(mContext, false);
                mAddNoteDialog.setOnCallBack(this);
                mAddNoteDialog.show();
                break;
            default:
                break;
        }
    }

    /**
     * Create animation when user tap on the button to close an remind banner
     *
     * @param view is the remind banner need to apply this closing animation
     */
    private void closeAlert(View view) {
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", 2000f);
        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
                anim.setDuration(1500);
                view.startAnimation(anim);

                float value = view.getMeasuredHeight();
                ObjectAnimator riseUpAnimation = ObjectAnimator.ofFloat(mRvDoseLog, "translationY", -value);
                riseUpAnimation.setDuration(1500);
                riseUpAnimation.start();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                //This feature isn't require here
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                //This feature isn't require here
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                //This feature isn't require here
            }
        });
        animation.setDuration(1500);
        animation.start();
    }

    /**
     * Initiate View component of the remind banner.
     *
     * @param alertType using to distinguish between each type of remind banner
     */
    private void initAlert(int alertType) {
        View view;
        switch (alertType) {
            case ALERT_NOT_PAIR:
                view = View.inflate(mContext, R.layout.alert_m025_not_paired, null);
                lnAlert.removeAllViews();
                lnAlert.addView(view);
                findViewById(R.id.tv_m025_show_me, this, CAApplication.getInstance().getMediumFont());
                TextView mTvGuide = findViewById(R.id.tv_m025_guide, CAApplication.getInstance().getRegularFont());
                String highLight = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m025_pair_notice_highlight));
                highLightText(mTvGuide, highLight, R.color.colorDarkBlue, true);
                break;
            case ALERT_EMPTY_LOG:
                view = View.inflate(mContext, R.layout.alert_m025_remind, null);
                lnAlert.removeAllViews();
                lnAlert.addView(view);
                findViewById(R.id.tv_m025_guide, CAApplication.getInstance().getBoldFont());
                findViewById(R.id.tv_m025_see_how, this, CAApplication.getInstance().getMediumFont());
                findViewById(R.id.iv_m025_close_alert, this);
                break;
            case ALERT_OUT_OF_DATE_LOG:
                view = View.inflate(mContext, R.layout.alert_m025_normal, null);
                lnAlert.removeAllViews();
                lnAlert.addView(view);
                findViewById(R.id.tr_m025_normal_alert, this);
                findViewById(R.id.iv_m025_normal_arrow, this);
                mTvGuide = findViewById(R.id.tv_m025_guide_normal, this, CAApplication.getInstance().getRegularFont());
                String formattedText = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m025_dont_see_all_your_dose));
                formattedText = formattedText + "\n" + LangMgr.getInstance().getLangList().get(getString(R.string.lang_m025_tap_here_to_learn));
                mTvGuide.setText(formattedText);
                String highLightGuide = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m025_dont_see_all_your_dose));
                highLightText(mTvGuide, highLightGuide, R.color.colorDarkBlue, true);
                break;
            case ALERT_EOL_WARNING:
                view = View.inflate(mContext, R.layout.alert_m025_eol_warning, null);
                lnAlert.removeAllViews();
                lnAlert.addView(view);
                findViewById(R.id.tv_m025_guide, CAApplication.getInstance().getBoldFont());
                new Handler().postDelayed(() -> {
                    getStorage().setSystemIdEOLWarning(null);
                    alertHandler();
                }, 5000);
                break;
            case ALERT_EOL:
                view = View.inflate(mContext, R.layout.alert_m025_eol, null);
                lnAlert.removeAllViews();
                lnAlert.addView(view);
                findViewById(R.id.tv_m025_guide, CAApplication.getInstance().getBoldFont());
                getStorage().setSystemIdEOL(null);
                new Handler().postDelayed(() -> {
                    getStorage().setSystemIdEOL(null);
                    alertHandler();
                }, 5000);
                break;
            case ALERT_UPDATING:
                view = View.inflate(mContext, R.layout.alert_m025_updating, null);
                lnAlert.removeAllViews();
                lnAlert.addView(view);
                findViewById(R.id.tv_m025_guide, CAApplication.getInstance().getBoldFont());
                break;
            case ALERT_UPDATED:
                view = View.inflate(mContext, R.layout.alert_m025_updated, null);
                lnAlert.removeAllViews();
                lnAlert.addView(view);
                findViewById(R.id.tv_m025_guide, CAApplication.getInstance().getBoldFont());
                new Handler().postDelayed(this::alertHandler, 5000);
                break;
            default:
                break;
        }
    }

    /**
     * Handle callback from {@link OnM025SubDoseLogCallBack}
     * {@link OnM025SubDoseLogCallBack#dataAS05Ready()}
     */
    @Override
    public void dataAS05Ready() {
//        if (isAutoSync) initAlert(ALERT_UPDATED);
        mCallBack.preLoginTimeZoneChange();
        ProgressLoading.dismiss();
        getStorage().setM025ListResult(mPresenter.getListDataInfo());
        DoseLogAdapter adapter;
        if (getStorage().getM025ListResult().isEmpty() || !isUpToDate()) {
            ArrayList<DataInfo> tmpList = new ArrayList<>();
            tmpList.clear();
            tmpList.addAll(getStorage().getM025ListResult());
            tmpList.add(0, emptyData);
            adapter = new DoseLogAdapter(mContext, tmpList, this);
        } else {
            adapter = new DoseLogAdapter(mContext, mPresenter.getListDataInfo(), this);
        }

        mRvDoseLog.setAdapter(adapter);
        mRefresh.setRefreshing(false);

        mCallBack.setupReminder();
        alertHandler();

        showEOLWarning();

        showPrivacyWarning();

        mPresenter.prepareDoseDataInfo();
    }

    /**
     * Compare the version of Privacy Policy that the user has accepted with the latest version.
     * If these 2 version doesn't match, display a dialog message to inform the user to check and accept
     * with the latest version.
     */
    private void showPrivacyWarning() {
        if (getStorage().getM001ConfigSet().isPrivacyWarning()) {
            CommonUtil.getInstance().showDialog(mContext,
                    CAApplication.getInstance().getString(R.string.app_name),
                    CAApplication.getInstance().getString(R.string.txt_privacy_warning_content),
                    CAApplication.getInstance().getString(R.string.txt_privacy_warning_ok), null,
                    new OnOKDialogCallBack() {
                        @Override
                        public void handleOKButton1() {
                            mCallBack.showChildFrgScreen(TAG, M052MoreMyAccountFrg.TAG);
                        }
                    });
        }
    }

    /**
     * After synchronize with the Dialoq cap, if the app received EOL warning flag, display a
     * dialog message to inform the user about it.
     */
    private void showEOLWarning() {
        if (getStorage().getSystemIdEOLWarning() != null
                && CommonUtil.getInstance().getPrefContent(getStorage().getSystemIdEOLWarning()) == null) {
            CommonUtil.getInstance().savePrefContent(getStorage().getSystemIdEOLWarning(), "1");
            getStorage().setSystemIdEOLWarning(null);

            String sms = LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m025_eol_warning_message));
            CommonUtil.getInstance().showDialog(mContext,
                    LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m025_eol_warning_title)),
                    sms != null ? sms.replace("${day_number}", X_DAYS) : "",
                    LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m025_eol_warning_cancel)),
                    LangMgr.getInstance().getLangList().get(CAApplication.getInstance().getString(R.string.lang_m025_eol_warning_support)),
                    new OnOKDialogCallBack() {
                        @Override
                        public void handleOKButton2() {
                            mCallBack.changeTab(CommonUtil.TAB_MENU_SUPPORT);
                            mCallBack.showChildFrgScreen(TAG, M047SupportTroubleshootingFrg.TAG);
                        }
                    });
        }
    }

    /**
     * Checking dose log data to detect if the current date have any dose.
     *
     * @return true if the current date have at least one dose or false if not.
     */
    private boolean isUpToDate() {
        if (getStorage().getM025ListResult().isEmpty()) {
            return false;
        }
        for (int j = 0; j < getStorage().getM025ListResult().size(); j++) {
            String type = getStorage().getM025ListResult().get(j).getResult().getAssessmentType();
            String date = getStorage().getM025ListResult().get(j).getResult().getReportedDate();
            if (type != null && type.equals(DoseLogAdapter.TYPE_ASSESSMENT_INJECTION) && NVDateUtils.isToday(date))
                return true;
        }
        return false;
    }

    /**
     * Detect the state of dose data, number of paired devices, device's battery status to display
     * a suitable remind banner.
     */
    private void alertHandler() {
        int alertCode;
        if (getStorage().getSystemIdEOL() != null) {
            alertCode = ALERT_EOL;
        } else {
            if (getStorage().getM025ListDevices().isEmpty()) {
                alertCode = ALERT_NOT_PAIR;
            } else {
                if (getStorage().getM025ListResult().isEmpty()) {
                    alertCode = ALERT_EMPTY_LOG;
                } else {
                    alertCode = ALERT_OUT_OF_DATE_LOG;
                }
            }
        }

        initAlert(alertCode);
        createEmptyHolder();
    }


    /**
     * Create an empty entity working as a place holder with a message inside if there isn't
     * any dose log for the current day
     */
    private void createEmptyHolder() {
        if (!isUpToDate()) {
            String message;
            if (getStorage().getM025ListResult().isEmpty()) {
                message = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m025_dose_log_is_empty));
            } else {
                message = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m025_no_dose_today));
            }
            ResultInfo emptyInfo = new ResultInfo();
            emptyInfo.setAssessmentType(DoseLogAdapter.TYPE_ASSESSMENT_EMPTY_LOG);
            emptyInfo.setReportedDate(CommonUtil.getDateNow(CommonUtil.DATE_STYLE));
            emptyInfo.setCustom1(message);
            emptyData.setResult(emptyInfo);
        }
    }

    /**
     * Handle callback from {@link OnM025SubDoseLogCallBack}
     * {@link OnM025SubDoseLogCallBack#toReminderPausedDetail(Object)}
     *
     * @param tag is the input data to initiate view data of {@link M067TimeChangeDetailFrg}
     */
    @Override
    public void toReminderPausedDetail(Object tag) {
        if (tag == null) {
            return;
        }

        getStorage().setM025ResultDataItem((DataInfo) tag);
        mCallBack.showChildFrgScreen(TAG, M067TimeChangeDetailFrg.TAG);
    }

    /**
     * Handle callback from {@link OnM025SubDoseLogCallBack}
     * {@link OnM025SubDoseLogCallBack#toIncompleteDoseDetail(Object)}
     *
     * @param tag is the input data to initiate view data of {@link M031DoseDetailFrg}
     */
    @Override
    public void toIncompleteDoseDetail(Object tag) {
        if (tag == null) {
            return;
        }

        getStorage().setM025ResultDataItem((DataInfo) tag);
        mCallBack.showChildFrgScreen(TAG, M031DoseDetailFrg.TAG);
    }

    /**
     * Handle callback from {@link OnM025SubDoseLogCallBack}
     * {@link OnM025SubDoseLogCallBack#toIncompleteDoseDetail(Object)}
     *
     * @param tag is the input data to initiate view data of {@link M063DetailNoteDialog}
     */
    @Override
    public void toDetailNote(Object tag) {
        if (tag == null) {
            return;
        }

        try {
            getStorage().setM063Note((DataInfo) tag);
            M063DetailNoteDialog m063DetailNoteDialog = new M063DetailNoteDialog(mContext, false);
            m063DetailNoteDialog.setOnCallBack(this);
            m063DetailNoteDialog.show();
        } catch (Exception e) {
            CommonUtil.getInstance().showDialog(mContext, LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON), null);
        }
    }

    /**
     * Handle callback from {@link OnM025SubDoseLogCallBack}
     * {@link OnM025SubDoseLogCallBack#closeRefresh()}
     */
    @Override
    public void closeRefresh() {
        mRefresh.setRefreshing(false);
    }

    /**
     * Handle callback from {@link OnM002SignUpChooseLanguageCallBack}
     * {@link OnM002SignUpChooseLanguageCallBack#clickOnItem(TextEntity)}
     */
    @Override
    public void clickOnItem(TextEntity mData) {
        //This feature isn't require here
    }

    /**
     * Handle callback from {@link OnM002SignUpChooseLanguageCallBack}
     * {@link OnM002SignUpChooseLanguageCallBack#updateView()}
     */
    @Override
    public void updateView() {
        //This feature isn't require here
    }

    /**
     * Handle callback from {@link OnM002SignUpChooseLanguageCallBack}
     * {@link OnM002SignUpChooseLanguageCallBack#revertChoice()}
     */
    @Override
    public void revertChoice() {
        //This feature isn't require here
    }

    /**
     * Handle callback from {@link OnM025SubDoseLogCallBack}
     * {@link OnM025SubDoseLogCallBack#refreshData()}
     */
    @Override
    public void refreshData() {
        CommonUtil.wtfi(TAG, "refreshData()...");
        mPresenter.initTime(false);
        mPresenter.resetData();
        mPresenter.loadOffline();
        mRefresh.setRefreshing(false);
    }

    /**
     * Handle callback from {@link OnM025SubDoseLogCallBack}
     * {@link OnM025SubDoseLogCallBack#reloadData()}
     */
    @Override
    public void reloadData() {
        mRefresh.setRefreshing(false);
        mPresenter.initTime(false);
        mPresenter.resetData();

        CommonUtil.wtfi(TAG, "reloadData()...");
        if (CommonUtil.getInstance().isConnectToNetwork()) {
            mPresenter.doAllAS05GetResult();
        } else {
            mPresenter.loadOffline();
        }
    }

    /**
     * Handle callback from {@link OnM025SubDoseLogCallBack}
     * {@link OnM025SubDoseLogCallBack#setReminderNotification(String, String)}
     */
    @Override
    public void setReminderNotification(String minDate, String maxDate) {
        CommonUtil.getInstance().setReminderNotification(mContext, minDate, maxDate);
    }

    /**
     * @see Fragment#onDestroyView()
     */
    @Override
    public void onDestroyView() {
        CommonUtil.wtfe(TAG, "onDestroyView...");
        getStorage().removeM0DoseCallBack(TAG);
        super.onDestroyView();
    }
}
