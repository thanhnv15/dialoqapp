package com.novo.app.view.fragment;

import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M003LoginIntroVideoPresenter;
import com.novo.app.utils.CATask;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnCallBackToView;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM003LoginIntroVideoCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.ProgressLoading;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;

public class M003LoginIntroVideoFrg extends BaseFragment<M003LoginIntroVideoPresenter
        , OnHomeBackToView> implements OnM003LoginIntroVideoCallBack, TextureView.SurfaceTextureListener
        , MediaPlayer.OnPreparedListener, View.OnClickListener, View.OnTouchListener, MediaPlayer.OnCompletionListener
        , SeekBar.OnSeekBarChangeListener, CATask.OnTaskCallBackToView, OnCallBackToView
        , MediaPlayer.OnVideoSizeChangedListener {
    public static final String TAG = M003LoginIntroVideoFrg.class.getName();

    private static final int PLAY_STATE = 0;
    private static final int STOP_STATE = 1;
    private static final long TIME_HIDE = 3000;
    private static final long TIME_DELAY = 500;
    private static final String KEY_CURRENT_TIME = "KEY_CURRENT_TIME";

    private static final int IDLE = 0;
    private static final int PAUSED = 1;
    private static final int PLAYED = 2;

    private static final String VIDEO_INTRO_PATH = "video/IntroVideo.mp4";
    private static final String VIDEO_INTRO_TITLE = "Introduce about DoseTracker";

    private MediaPlayer mPlayer;

    private ImageView ivPlay;
    private TextView tvDuration, tvStart;
    private SeekBar seekBar;
    private TextureView mTextureView;
    private View mTrPlayer;
    private Handler mHandler = new Handler();
    private CATask<M003LoginIntroVideoFrg, M003LoginIntroVideoFrg> mTimer;
    private int mState;
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mTrPlayer.setVisibility(View.GONE);
        }
    };

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m003_logo);
        findViewById(R.id.bt_m003_next, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m003_cancel, this, CAApplication.getInstance().getMediumFont());

        mTextureView = findViewById(R.id.texture_m003_video);
        mTextureView.setOnTouchListener(this);

        ivPlay = findViewById(R.id.iv_m003_play);
        tvStart = findViewById(R.id.tv_m003_start);
        mTrPlayer = findViewById(R.id.tr_m003_player);

        ivPlay.setOnClickListener(this);
        tvDuration = findViewById(R.id.tv_m003_duration);
        seekBar = findViewById(R.id.seek_bar_m003);
        seekBar.setOnSeekBarChangeListener(this);
        mTextureView.setSurfaceTextureListener(this);
        mState = IDLE;

        getStorage().setM028VideoData(new String[]{VIDEO_INTRO_PATH, VIDEO_INTRO_TITLE});
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m003_login_intro_video;
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected M003LoginIntroVideoPresenter getPresenter() {
        return new M003LoginIntroVideoPresenter(this);
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        CommonUtil.wtfi(TAG, "onPrepared....");
        String time = CommonUtil.getInstance().getPrefContent(KEY_CURRENT_TIME);
        if (time != null) {
            try {
                int timeNow = Integer.parseInt(time);
                CommonUtil.getInstance().clearPrefContent(KEY_CURRENT_TIME);
                mPlayer.seekTo(timeNow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        playVideo();

        mTimer = new CATask<>(new WeakReference<>(this), this, TIME_DELAY);
        mTimer.setLoop(true);
        seekBar.setMax(mPlayer.getDuration());
        findViewById(R.id.tr_m003_player).setVisibility(View.VISIBLE);

        mTimer.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        new Handler().postDelayed(() -> findViewById(R.id.v_bg_white).setVisibility(View.GONE), 1000);

        mHandler.postDelayed(mRunnable, TIME_HIDE);
        tvDuration.setText(getDuration(mPlayer.getDuration()));
        ProgressLoading.dismiss();
    }

    private String getDuration(int time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss");
        return dateFormat.format(new Date(time));
    }

    @Override
    public void onStart() {
        mTextureView.setVisibility(View.VISIBLE);
        if (mState == PAUSED) {
            playVideo();
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (mState == PLAYED) {
            pauseVideo();
        }
        super.onStop();
    }

    public void stopVideo() {
        if (mPlayer != null) {
            mPlayer.reset();
            mPlayer.release();
            mPlayer = null;
            mTimer.cancel();
            mState = IDLE;
        }
    }

    @Override
    protected void defineBackKey() {
        mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
    }

    @Override
    public void backToPreviousScreen() {
        //This method isn't require here
    }

    public void pauseVideo() {
        if (mPlayer != null) {
            mPlayer.pause();
            mState = PAUSED;
            ivPlay.setImageLevel(PLAY_STATE);
        }
    }

    private void playVideo() {
        CommonUtil.wtfi(TAG, "playVideo....");
        mPlayer.start();
        mState = PLAYED;
        ivPlay.setImageLevel(STOP_STATE);
    }

    private void initVideo(Surface s) {
        String videoPath = CommonUtil.getInstance().getPrefContent(CommonUtil.KEY_INTRO_PATH);
        String videoUrl = CommonUtil.getInstance().getPrefContent(CommonUtil.KEY_INTRO_URL);
        File file = new File(videoPath);
        if (CommonUtil.getInstance().isConnectToNetwork()) {
            if (!file.exists()) {
                mCallBack.downloadIntroVideo(videoUrl);
            }else{
                prepareVideo(s,videoPath);
            }
        }else {
            if (!file.exists()){
                prepareVideo(s,videoPath);
            }else {
                CommonUtil.getInstance().showDialog(getActivity(),R.string.lang_err_408,null);
            }
        }
    }

    private void prepareVideo(Surface surface,String path) {
                CommonUtil.wtfi(TAG, "initVideo....");
        //get video info
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.reset();
        }

        mPlayer = new MediaPlayer();
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnCompletionListener(this);
        mPlayer.setOnVideoSizeChangedListener(this);
        try {
            mPlayer.setDataSource(path);
        } catch (IOException e) {
            e.printStackTrace();
            ProgressLoading.dismiss();
        }
        mPlayer.setSurface(surface);
        mPlayer.prepareAsync();
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            if (mTrPlayer.getVisibility() == View.GONE) {
                mTrPlayer.setVisibility(View.VISIBLE);
                mHandler.removeCallbacks(mRunnable);
            }
        }
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            if (mTrPlayer.getVisibility() == View.VISIBLE) {
                mHandler.postDelayed(mRunnable, TIME_HIDE);
            }
        }
        return true;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        ivPlay.setImageLevel(PLAY_STATE);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        //This method isn't require here
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        //This method isn't require here
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (mPlayer != null) {
            mPlayer.seekTo(seekBar.getProgress());
            playVideo();
        }
    }

    @Override
    public Object doBGTask(String key) {
        try {
            Thread.sleep(TIME_DELAY);
            if (mPlayer == null) {
                mTimer.cancel();
                return false;
            }
            if (mPlayer.isPlaying()) {
                mTimer.updateUI(mPlayer.getCurrentPosition(), mPlayer.getDuration());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void updateUITask(Object[] values) {
        tvStart.setText(getDuration((int) values[0]));
        seekBar.setProgress((int) values[0]);
    }

    @Override
    public void doneTask(Object data) {
        //This method isn't require here
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        ProgressLoading.show(mContext);
        initVideo(new Surface(surface));
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        //This method isn't require here
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        //This method isn't require here
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
        CommonUtil.wtfi(TAG, "WIDTH: " + width);
        CommonUtil.wtfi(TAG, "HEIGHT: " + height);
        int wSurface = mTextureView.getWidth();
        int hSurface = mTextureView.getHeight();

        float boxWidth = wSurface;
        float boxHeight = hSurface;
        float wr = boxWidth / (float) width;
        float hr = boxHeight / (float) height;
        float ar = (float) width / (float) height;

        if (wr > hr) {
            wSurface = (int) (boxHeight * ar);
        } else {
            hSurface = (int) (boxWidth / ar);
        }
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(wSurface, hSurface);
        params.gravity = Gravity.CENTER;
        mTextureView.setLayoutParams(params);
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m003_next:
                mCallBack.showFrgScreen(TAG, M004AccountSetUpFrg.TAG);
                break;
            case R.id.tv_m003_cancel:
                mCallBack.registrationCancel(TAG);
                break;
            default:
                break;
        }
    }
}
