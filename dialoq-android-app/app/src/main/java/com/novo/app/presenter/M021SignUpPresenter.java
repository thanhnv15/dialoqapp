package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.PatientEntity;
import com.novo.app.model.entities.ProfileEntity;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM021SignUpCallBack;

import static com.novo.app.utils.CommonUtil.P_API_KEY;

public class M021SignUpPresenter extends BasePresenter<OnM021SignUpCallBack> {
    private static final String KEY_API_UN04_UPDATE_PATIENT = "KEY_API_IA02_AUTHENTICATE_USER";
    private static final String URL_API_UN04_UPDATE_PATIENT = "user-service/api/v1/users/patients/%s?";
    private static final String BODY_UN04 = "{\"userName\": %s,\"passWord\":%s,\"firstName\":%s,\"lastName\":%s,\"email\":%s," +
            "\"gender\":%s,\"dateOfBirth\":%s,\"personalHistory\":%s,\"allergies\":%s,\"race\":%s,\"mobilePhoneNumber\": \"84978678205\"," +
            "\"medicationHistory\": %s,\"country\": %s,\"streetAddress1\": %s,\"streetAddress2\": %s, \"familyHistory\": %s}";
    private static final String TAG = M021SignUpPresenter.class.getName();


    public M021SignUpPresenter(OnM021SignUpCallBack event) {
        super(event);
    }

    @Override
    protected void handleSuccess(String data, String tag) {
        CommonUtil.wtfi(TAG, "DATA: " + data);
        if (KEY_API_UN04_UPDATE_PATIENT.equals(tag)) {
            prepareUN04(data);
        }
    }

    protected void prepareUN04(String data) {
        CommonUtil.wtfi(TAG, "prepareUN04..." + data);
        PatientEntity patientEntity = generateData(PatientEntity.class, data);
        if (patientEntity == null) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            mListener.showM022SignUpSuccess();
            return;
        }
        getStorage().setM014PatientEntity(patientEntity);
        mListener.showM022SignUpSuccess();
    }

    public void callUN04Update() {
        CommonUtil.wtfi(TAG, "callIUN04UpdatePatient");
        CARequest request = new CARequest(TAG, CARequest.METHOD_PUT);
        request.addTAG(KEY_API_UN04_UPDATE_PATIENT);

        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, "token: " + token);
        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);

        ProfileEntity entity = getStorage().getM001ProfileEntity();
        String termEntity = entity.getTermVersion().getKey() + "=" + entity.getTermVersion().getValue();
        request.addPathSegment(String.format(URL_API_UN04_UPDATE_PATIENT, entity.getUserId() + ""));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY_UN04, entity.getEmail(), entity.getPassword(),
                entity.getFirstName(), entity.getLastName(), entity.getEmail(),
                entity.getGender() == null ? null : entity.getGender().getKey(),
                entity.getDateOfBirth(),
                entity.getYearDiabetes() == null ? null : entity.getYearDiabetes().getKey(),
                entity.getAllErgies(), entity.getRace(),
                entity.getTypeOfDiabetes() == null ? null : entity.getTypeOfDiabetes().getKey(),
                entity.getCountry().getKey(), entity.getStreetAddress1(), entity.getStreetAddress2(), termEntity));

        //call request
        CAApplication.getInstance().callRequest(request, this);
     }

    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        if (KEY_API_UN04_UPDATE_PATIENT.equals(tag)) {
            CommonUtil.wtfi(TAG, "DATA-ERR: " + data);

            ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
            if (responseEntity == null) {
                showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
                return;
            }
            mListener.showM022SignUpSuccess();
        }
    }

    @Override
    public void hideLockDialog(String key) {
        // Do not hide lock dialog
    }
}
