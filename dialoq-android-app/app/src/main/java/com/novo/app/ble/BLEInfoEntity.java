package com.novo.app.ble;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BLEInfoEntity implements Serializable {
    private String deviceName, serialNumber, softwareRevision, hardwareRevision, firmwareRevision, systemId, systemFlag;
    private List<DoseItem> doseItems = new ArrayList<>();

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getSoftwareRevision() {
        return softwareRevision;
    }

    public void setSoftwareRevision(String softwareRevision) {
        this.softwareRevision = softwareRevision;
    }

    public String getHardwareRevision() {
        return hardwareRevision;
    }

    public void setHardwareRevision(String hardwareRevision) {
        this.hardwareRevision = hardwareRevision;
    }

    public String getFirmwareRevision() {
        return firmwareRevision;
    }

    public void setFirmwareRevision(String firmwareRevision) {
        this.firmwareRevision = firmwareRevision;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public List<DoseItem> getDoseItems() {
        return doseItems;
    }

    public void setDoseItems(List<DoseItem> doseItems) {
        this.doseItems = doseItems;
    }

    @Override
    public String toString() {
        return "\ndeviceName: " + deviceName +
                "\nserialNumber: " + serialNumber +
                "\nsoftwareRevision: " + softwareRevision +
                "\nhardwareRevision: " + hardwareRevision +
                "\nfirmwareRevision: " + firmwareRevision +
                "\nsystemId:" + systemId + "\n";
    }

    public String getSystemFlag() {
        if (systemFlag == null) {
            systemFlag = "";
        }
        return systemFlag;
    }

    public void setSystemFlag(String systemFlag) {
        this.systemFlag = systemFlag;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
