/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.view.dialog;

import android.content.Context;
import android.text.Editable;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M063EditNotePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseDialog;
import com.novo.app.view.event.OnM063EditNoteCallBack;
import com.novo.app.view.event.OnM063NoteDetailCallBack;
import com.novo.app.view.widget.NVDateUtils;
import com.novo.app.view.widget.TextAdapter;

public class M063EditNoteDialog extends BaseDialog<M063EditNotePresenter, OnM063NoteDetailCallBack> implements OnM063EditNoteCallBack {
    public static final String TAG = M063EditNoteDialog.class.getName();
    private TextView tvNoteDate;
    private TextView tvNoteTime;
    private EditText edtInputNote;
    private Button mBtSave;
    private String dateNote;
    private boolean isFirstTime = true;

    public M063EditNoteDialog(Context context, boolean isCancel) {
        super(context, isCancel, R.style.dialog_style);
    }

    @Override
    public void showM001LandingScreen(String err) {
        mCallBack.showM001LandingScreen(err);
    }

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m063_v_edit_note, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m063_v_note_date_time, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m063_v_note_date_time_breaker, this, CAApplication.getInstance().getRegularFont());
        tvNoteDate = findViewById(R.id.tv_m063_v_detail_note_date, this, CAApplication.getInstance().getRegularFont());
        tvNoteTime = findViewById(R.id.tv_m063_v_note_time, this, CAApplication.getInstance().getRegularFont());
        edtInputNote = findViewById(R.id.edt_m063_v_edit_note);
        mBtSave = findViewById(R.id.bt_m063_v_save, this, false, CAApplication.getInstance().getRegularFont());
        edtInputNote.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (isFirstTime) {
                    isFirstTime = false;
                    return;
                }

                mBtSave.setEnabled(editable.length() > 0);
            }
        });
        findViewById(R.id.iv_m063_v_close_note, this, null);
        mBtSave.setEnabled(false);
    }

    @Override
    public int getLayoutId() {
        return R.layout.view_m063_edit_note;
    }

    @Override
    protected M063EditNotePresenter getPresenter() {
        return new M063EditNotePresenter(this);
    }

    @Override
    protected void warningForTimeZoneChange() {
        initData();
    }

    @Override
    protected void initData() {
        dateNote = getStorage().getM063Note().getResult().getReportedDate();
        tvNoteDate.setText(NVDateUtils.getNoteDetailsTimeString(dateNote));
        tvNoteDate.setTag(dateNote);
        tvNoteTime.setText(NVDateUtils.getOnlyTime(dateNote));
        edtInputNote.setText(getStorage().getM063Note().getResult().getNotes());
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m063_v_detail_note_date:
            case R.id.tv_m063_v_note_time:
            case R.id.tv_m063_v_note_date_time_breaker:
                CommonUtil.getInstance().showLimitedDatePicker(mContext, tvNoteDate, tvNoteTime, data -> dateNote = (String) data);
                break;
            case R.id.bt_m063_v_save:
                CommonUtil.getInstance().forceHideKeyBoard(edtInputNote);
                mPresenter.editNoteInDB(edtInputNote.getText().toString(), getStorage().getM063Note(), dateNote);
                break;
            case R.id.iv_m063_v_close_note:
                CommonUtil.getInstance().forceHideKeyBoard(edtInputNote);
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void refreshDataM025() {
        mCallBack.refreshDataM025();
        dismiss();
    }

    @Override
    public void showEditSuccess() {
        refreshDataM025();
    }

    @Override
    public void showEditFailed() {
        mCallBack.editNoteFailed();
        mCallBack.showAlertDialog(LangMgr.getInstance().getLangList().get(mContext.getString(R.string.lang_m063_update_note_failed)));
        dismiss();
    }

    @Override
    public void showEditHasBeenDeleted() {
        refreshDataM025();
        mCallBack.showAlertDialog("This note is no longer available");
    }
}
