package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM043SignUpDidntGetCallBack;

public class M043SignUpDidntGetPresenter extends BasePresenter<OnM043SignUpDidntGetCallBack> {
    private static final String KEY_API_VERIFY_EMAIL = "KEY_API_VERIFY_EMAIL";
    private static final String URL_API_UN42_VERIFY_EMAIL = "user-service/api/v1/users/verify-email?";
    private static final String BODY = "{\"action\":\"initiateVerification\",\"email\":%s}";
    private static final String TAG = M043SignUpDidntGetPresenter.class.getName();

    public M043SignUpDidntGetPresenter(OnM043SignUpDidntGetCallBack event) {
        super(event);
    }

    public void sendCodeUN42() {
        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_VERIFY_EMAIL);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addPathSegment(URL_API_UN42_VERIFY_EMAIL);

        request.addHeaders(CommonUtil.P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY, getStorage().getM001ProfileEntity().getEmail()));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    @Override
    protected void handleSuccess(String data, String tag) {
        if (KEY_API_VERIFY_EMAIL.equals(tag)) {
            CommonUtil.wtfi(TAG, "DATA: " + data);
            ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
            if (responseEntity == null) {
                mListener.showAlertDialog(R.string.txt_token_err);
                mListener.showM013SignUpCode();
                return;
            }
            mListener.verifyEmailSuccess(responseEntity.getSuccess());
        }
    }

    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        if (KEY_API_VERIFY_EMAIL.equals(tag)) {
            CommonUtil.wtfi(TAG, "DATA-ERR: " + data);

            ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
            if (responseEntity == null) {
                mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(ERR_500));
                return;
            }

            mListener.verifyEmailFail(responseEntity.getErrorEntity()[0].getMessage());
        }
    }
}
