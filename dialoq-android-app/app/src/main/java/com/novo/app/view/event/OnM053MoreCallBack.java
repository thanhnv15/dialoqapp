package com.novo.app.view.event;

public interface OnM053MoreCallBack extends OnCallBackToView {
    void showLoginScreen();

    void showDialog();

    void updateSuccess();

    void updateFailed();

}
