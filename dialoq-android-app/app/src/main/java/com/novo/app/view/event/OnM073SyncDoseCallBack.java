package com.novo.app.view.event;

public interface OnM073SyncDoseCallBack extends OnCallBackToView{
    void syncDoseSuccess(int index);

    void syncFailed(String sms);
}
