/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.utils;

import android.Manifest;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;

import com.novo.app.CAApplication;

/**
 * This helps communicating with device's Fingerprint module for bio-authentication feature
 * Fingerprint authentication will be done here, the authentication result handling task will be
 * delegated to an instance of {@link OnActionCallBack}
 */
@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    /**
     * Possible different authentication results
     * These possibilities are corresponding for callbacks from {@link FingerprintManager.AuthenticationCallback}
     * @see FingerprintManager.AuthenticationCallback
     * TODO {@link FingerprintManager} deprecated
     */
    public static final int CODE_ERR = -1; /** @see FingerprintManager.AuthenticationCallback#onAuthenticationError(int, CharSequence) */
    public static final int CODE_HELP = 1; /** @see FingerprintManager.AuthenticationCallback#onAuthenticationHelp(int, CharSequence) */
    public static final int CODE_FAIL = 2; /** @see FingerprintManager.AuthenticationCallback#onAuthenticationFailed() */
    public static final int CODE_SUCCESS = 3; /** @see FingerprintManager.AuthenticationCallback#onAuthenticationSucceeded(FingerprintManager.AuthenticationResult)  */

    // Calback delegate when authentication finished
    private OnActionCallBack callBack;

    /**
     * Used while authenticating fingerprint, this is required for using {@link FingerprintManager}
     * This enable fingerprint authentication cancellable
     */
    private CancellationSignal mCancellationSignal;

    // Constructor
    public FingerprintHandler(OnActionCallBack callBack) {
        this.callBack = callBack;
    }

    /**
     * Start fingerprint authenticating
     * @param manager FingerprintManager
     * @param cryptoObject crypto
     */
    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        mCancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(CAApplication.getInstance(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, mCancellationSignal, 0, this, null);
    }

    /**
     * Handler for {@link FingerprintManager.AuthenticationCallback}
     */
    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        this.update(CODE_ERR, "Fingerprint authentication error\n" + errString, false);
    }

    /**
     * Handler for {@link FingerprintManager.AuthenticationCallback}
     */
    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        this.update(CODE_HELP, helpString.toString(), false);
    }

    /**
     * Handler for {@link FingerprintManager.AuthenticationCallback}
     */
    @Override
    public void onAuthenticationFailed() {
        this.update(CODE_FAIL, "Fingerprint authentication failed.", false);
    }

    /**
     * Handler for {@link FingerprintManager.AuthenticationCallback}
     */
    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        this.update(CODE_SUCCESS, "Fingerprint authentication succeeded.", true);
    }

    /**
     * Update fingerprint authenticating result
     * @param code authentication result code: {@link #CODE_ERR}, {@link #CODE_HELP}, {@link #CODE_FAIL}, {@link #CODE_SUCCESS}
     * @param sms authentication result message (in {@link String})
     * @param success true if authenticated successfully
     */
    public void update(int code, String sms, boolean success) {
        callBack.updateFingerPrintResult(code, sms, success);
    }

    /**
     * Cancel authentication
     */
    public void cancel() {
        if (mCancellationSignal != null) {
            mCancellationSignal.cancel();
        }
    }

    /**
     * Callback prototype for delegating authentication results
     */
    public interface OnActionCallBack {
        void updateFingerPrintResult(int code, String sms, boolean success);
    }
}
