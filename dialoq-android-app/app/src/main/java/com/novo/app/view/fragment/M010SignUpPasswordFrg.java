package com.novo.app.view.fragment;

import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M010SignUpPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseEditText;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM010SignUpCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.TextAdapter;

import java.util.Objects;

public class M010SignUpPasswordFrg extends BaseFragment<M010SignUpPresenter, OnHomeBackToView> implements OnM010SignUpCallBack {

    public static final String TAG = M010SignUpPasswordFrg.class.getName();
    private TextView mTvError;
    private Button mBtNext;
    private BaseEditText mEdtPass;
    private TextView mTvShow, mTvNextLine;
    private ImageView mIvError;

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m010_logo);
        findViewById(R.id.iv_m010_back, this);
        findViewById(R.id.tv_m010_account_setup, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m010_description, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m010_cancel, this, CAApplication.getInstance().getBoldFont());

        mTvNextLine = findViewById(R.id.tv_m010_next_line);
        mBtNext = findViewById(R.id.bt_m010_next, this, CAApplication.getInstance().getRegularFont());
        mBtNext.setEnabled(false);

        mTvError = findViewById(R.id.tv_m010_error, CAApplication.getInstance().getRegularFont());
        mIvError = findViewById(R.id.iv_m010_error);
        mIvError.setVisibility(View.GONE);
        findViewById(R.id.tv_m010_tip, CAApplication.getInstance().getRegularFont());

        mEdtPass = findViewById(R.id.tv_m010_password, CAApplication.getInstance().getRegularFont());
        mEdtPass.setText(getStorage().getM001ProfileEntity().getPassword());
        if (!mEdtPass.getText().toString().isEmpty()) {
            validatePass(mEdtPass.getText().toString());
        }
        onPasswordChange();
        onPasswordFocusChange();
        onKeyBoardDismiss();

        mTvShow = findViewById(R.id.tv_m010_show, this, CAApplication.getInstance().getRegularFont());
    }

    /**
     * Handle App's behavior when the virtual keyboard is dismiss
     */
    private void onKeyBoardDismiss() {
        mEdtPass.setOnEditTextImeBackListener((ctrl, text) -> {
            String pass = Objects.requireNonNull(mEdtPass.getText()).toString();
            mEdtPass.clearFocus();
            mTvNextLine.setVisibility(View.VISIBLE);
            validatePass(pass);
        });
        mEdtPass.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                String pass = Objects.requireNonNull(mEdtPass.getText()).toString();
                mTvNextLine.setVisibility(View.VISIBLE);
                mEdtPass.clearFocus();
                validatePass(pass);
            }
            return false;
        });
    }

    /**
     * Handle App's behavior when the focus of the input password field changed
     */
    private void onPasswordFocusChange() {
        mEdtPass.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                mTvNextLine.setVisibility(View.VISIBLE);
            } else {
                mTvNextLine.setVisibility(View.GONE);
            }
            String pass = Objects.requireNonNull(mEdtPass.getText()).toString();
            validatePass(pass);
        });
    }

    /**
     * Reset view component when the input password is change
     */
    private void onPasswordChange() {
        mEdtPass.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                CommonUtil.getInstance().initTypeFace(mEdtPass);
                validatePass(mEdtPass.getText().toString());
                mTvError.setTextColor(getResources().getColor(R.color.colorBlack));
                mEdtPass.setTextColor(getResources().getColor(R.color.colorBlack));
            }
        });
    }

    /**
     * Validate the input password if it match with the password policy
     * and handle App's behaviors for each case
     *
     * @param pass
     */
    private void validatePass(String pass) {
        if (pass.isEmpty()) {
            mBtNext.setEnabled(false);
            if (!mEdtPass.isFocused()) {
                mTvError.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
                mEdtPass.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
            } else {
                mIvError.setVisibility(View.GONE);
            }
            return;
        }
        if (pass.length() < getStorage().getM001ConfigSet().getMinPass()) {
            mIvError.setVisibility(View.VISIBLE);
            mTvError.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
            mEdtPass.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
            mBtNext.setEnabled(false);
        } else {
            mIvError.setVisibility(View.GONE);
            mTvError.setTextColor(getResources().getColor(R.color.colorBlack));
            mBtNext.setEnabled(true);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m010_signup_password;
    }

    @Override
    protected M010SignUpPresenter getPresenter() {
        return new M010SignUpPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        mCallBack.showFrgScreen(TAG, M045SignUpEmailConfirmFrg.TAG);
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m010_show:
                if (mTvShow.getText().toString().equals(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m010_show)))) {
                    mEdtPass.setInputType(InputType.TYPE_CLASS_TEXT);
                    mTvShow.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m023_hide)));
                } else {
                    if (!TextUtils.isEmpty(mEdtPass.getText())) {
                        mEdtPass.setInputType(InputType.TYPE_CLASS_TEXT |
                                InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    } else {
                        mEdtPass.setInputType(InputType.TYPE_CLASS_TEXT);
                    }
                    mEdtPass.setSelection(mEdtPass.getText().length());
                    mTvShow.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_m010_show)));
                }
                break;

            case R.id.bt_m010_next:
                CommonUtil.getInstance().forceHideKeyBoard(mEdtPass);
                getStorage().getM001ProfileEntity().setPassword(mEdtPass.getText().toString());
                mCallBack.showFrgScreen(TAG, M012SignUpEmailCodeFrg.TAG);
                break;

            case R.id.tv_m010_cancel:
                CommonUtil.getInstance().forceHideKeyBoard(mEdtPass);
                mCallBack.registrationCancel(TAG);
                break;

            case R.id.iv_m010_back:
                CommonUtil.getInstance().forceHideKeyBoard(mEdtPass);
                backToPreviousScreen();
                break;

            default:
                break;
        }
    }
}
