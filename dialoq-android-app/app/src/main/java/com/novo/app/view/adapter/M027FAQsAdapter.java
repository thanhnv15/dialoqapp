package com.novo.app.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.model.entities.FAQEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM027FAQOnCallBack;

import java.util.List;

import static com.novo.app.model.entities.FAQEntity.LEVEL_DEFAULT;


public class M027FAQsAdapter extends BaseRecycleAdapter<OnM027FAQOnCallBack, FAQEntity, M027FAQsAdapter.FAQHolder> {

    private static final int LEVEL_OPEN = 1;
    private static final int LEVEL_CLOSE = 0;
    private static final int LEVEL_GONE = 2;
    private static final CharSequence HREF_LINK = "href";


    public M027FAQsAdapter(Context mContext, List<FAQEntity> mListData, OnM027FAQOnCallBack mCallBack) {
        super(mContext, mListData, mCallBack);
    }

    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_m027_faqs_question;
    }

    @Override
    protected FAQHolder getViewHolder(int viewType, View itemView) {
        return new FAQHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        FAQHolder mViewHolder = (FAQHolder) holder;
        FAQEntity entity = mListData.get(position);

        mViewHolder.itemView.setTag(entity);
        if (entity.getText() != null && !entity.getText().isEmpty() && entity.getText().contains(HREF_LINK)) {
            mViewHolder.mTvText.setText(Html.fromHtml(entity.getText()));
            mViewHolder.mTvText.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
            mViewHolder.mTvText.setText(entity.getText());
        }

        CommonUtil.getInstance().insertImage(mViewHolder.mTvText, entity.getImage());
        if (entity.getLevel() == LEVEL_DEFAULT) {
            showItemView(true, mViewHolder);
            mViewHolder.mTvText.setTypeface(CAApplication.getInstance().getBoldFont());
            mViewHolder.mLine.setVisibility(entity.isShowChild() ? View.GONE : View.VISIBLE);
            mViewHolder.mIvLevel.setImageLevel(entity.isShowChild() ? LEVEL_OPEN : LEVEL_CLOSE);
        } else {
            showItemView(entity.isShow(), mViewHolder);

            if (entity.isShow()) {
                mViewHolder.mTvText.setTypeface(CAApplication.getInstance().getRegularFont());
                if (entity.isLast()) {
                    mViewHolder.mLine.setVisibility(entity.isShow() ? View.VISIBLE : View.GONE);
                    mViewHolder.mIvLevel.setImageLevel(LEVEL_GONE);
                } else {
                    mViewHolder.mLine.setVisibility(entity.isShowChild() ? View.GONE : View.VISIBLE);
                    mViewHolder.mIvLevel.setImageLevel(entity.isShowChild() ? LEVEL_OPEN : LEVEL_CLOSE);
                }
            }
        }
    }

    private void showItemView(boolean isShow, FAQHolder mViewHolder) {
        mViewHolder.itemView.setVisibility(isShow ? View.VISIBLE : View.GONE);
        mViewHolder.mTvText.setVisibility(isShow ? View.VISIBLE : View.GONE);
        mViewHolder.mLine.setVisibility(isShow ? View.VISIBLE : View.GONE);
        mViewHolder.mIvLevel.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    private void updateListData(FAQEntity entity) {
        for (int i = 0; i < mListData.size(); i++) {
            FAQEntity item = mListData.get(i);
            if (item.getRoot() != null && item.getRoot().equals(entity)) {
                item.setShow(entity.isShowChild());
                if (!item.isShow()) {
                    item.setShowChild(false);
                    updateListData(item);
                }
            }
        }
    }

    public void filterList(List<FAQEntity> filteredList) {
        mListData = filteredList;
        notifyDataSetChanged();
    }


    class FAQHolder extends BaseHolder {
        TextView mTvText;
        ImageView mIvLevel;
        View mLine;

        FAQHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void onClickView(int idView) {
            switch (idView) {
                case R.id.tv_m027_faq_title:
                case R.id.iv_m027_faq_level:
                    FAQEntity entity = (FAQEntity) itemView.getTag();
                    entity.setShowChild(!entity.isShowChild());
                    updateListData(entity);
                    notifyItemRangeChanged(0, mListData.size());
                    break;
                default:
                    break;
            }
        }

        @Override
        protected void initView() {
            mTvText = findViewById(R.id.tv_m027_faq_title);
            mIvLevel = findViewById(R.id.iv_m027_faq_level, this);
            mLine = findViewById(R.id.v_m027_faq_line_divider);
        }
    }
}
