package com.novo.app.presenter;

import com.novo.app.view.event.OnM016ProfileStartCallBack;

public class M016ProfileStartPresener extends BasePresenter<OnM016ProfileStartCallBack> {
    public M016ProfileStartPresener(OnM016ProfileStartCallBack event) {
        super(event);
    }
}
