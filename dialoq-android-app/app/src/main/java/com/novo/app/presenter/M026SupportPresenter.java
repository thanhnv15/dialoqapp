package com.novo.app.presenter;

import com.novo.app.view.event.OnM026SupportCallBack;

public class M026SupportPresenter extends BasePresenter<OnM026SupportCallBack> {
    public M026SupportPresenter(OnM026SupportCallBack event) {
        super(event);
    }
}
