/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

package com.novo.app.view.fragment;

import android.text.Editable;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M038RecoveryPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseEditText;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM038RecoveryCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;
import com.novo.app.view.widget.TextAdapter;

import java.util.Objects;

/**
 * Recovery Email Fragment
 * <p>
 * This fragment is 'View' object in MVP pattern
 * This fragment uses an {@link M038RecoveryPresenter} as linked Presenter
 * Callbacks to this fragment will be defined by {@link OnM038RecoveryCallBack} interface
 * <p>
 * This fragment is used in Reset password features of dialog app, where user has to input
 * email to receive confirmation code
 */
public class M038RecoveryEmailFrg extends BaseFragment<M038RecoveryPresenter, OnHomeBackToView> implements OnM038RecoveryCallBack {

    public static final String TAG = M038RecoveryEmailFrg.class.getName();

    // View items used in Recovery Email Fragment
    private TableRow mTrError;
    private Button mBtSend;
    private BaseEditText mEdtEmail;
    private TextView mTvError;
    private TextView mTVLineTop, mTvLineBottom;

    /**
     * Handle callback from {@link M038RecoveryPresenter}
     * {@link OnHomeBackToView#showM001LandingScreen(String err)}
     *
     * @param message error string description
     */
    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    /**
     * Initiate views component for Recovery Email Fragment
     *
     * @see BaseFragment#initViews()
     */
    @Override
    protected void initViews() {
        findViewById(R.id.tv_m038_account_recovery, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m038_description, CAApplication.getInstance().getRegularFont());

        findViewById(R.id.tv_m038_detail, CAApplication.getInstance().getRegularFont());
        mTvError = findViewById(R.id.tv_m038_error, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m038_cancel, this, CAApplication.getInstance().getBoldFont());

        mBtSend = findViewById(R.id.bt_m038_send, this, CAApplication.getInstance().getRegularFont());
        mBtSend.setEnabled(false);

        mTVLineTop = findViewById(R.id.tv_m038_line_top);
        mTvLineBottom = findViewById(R.id.tv_m038_line_bottom);

        mTrError = findViewById(R.id.tr_m038_error);
        mTrError.setVisibility(View.GONE);

        mEdtEmail = findViewById(R.id.tv_m038_email, CAApplication.getInstance().getRegularFont());
        if (CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME) != null && !CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME).equals("")) {
            mEdtEmail.setText(CommonUtil.getInstance().getPrefContent(CommonUtil.USER_NAME));
            mEdtEmail.setTypeface(CAApplication.getInstance().getBoldFont());
            mEdtEmail.setFocusable(false);
            mEdtEmail.setEnabled(false);
            mEdtEmail.setCursorVisible(false);
            mBtSend.setEnabled(true);
        } else {
            mEdtEmail.addTextChangedListener(new TextAdapter() {
                @Override
                public void afterTextChanged(Editable editable) {
                    String email = editable.toString();
                    CommonUtil.getInstance().initTypeFace(mEdtEmail);

                    if (CommonUtil.isNotEmail(email)) {
                        mEdtEmail.setTextColor(getResources().getColor(R.color.colorM009RustyRed));
                        mTvError.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_err_invalid_email)));
                        mTrError.setVisibility(View.VISIBLE);
                        mBtSend.setEnabled(false);
                    } else {
                        mEdtEmail.setTextColor(getResources().getColor(R.color.colorBlack));
                        mTrError.setVisibility(View.GONE);
                        mBtSend.setEnabled(true);
                    }
                    if (email.isEmpty()) {
                        mTvError.setText(LangMgr.getInstance().getLangList().get(getString(R.string.lang_err_empty_email)));
                    }
                }
            });
            onEmailChange();
            onEmailFocusChange();
            onKeyBoardDismiss();
        }
    }

    /**
     * Reset layout when keyboard dismissed
     */
    private void onKeyBoardDismiss() {
        mEdtEmail.setOnEditTextImeBackListener((ctrl, text) -> {
            mEdtEmail.clearFocus();
            showLine(true);
        });

        mEdtEmail.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEdtEmail.clearFocus();
                showLine(true);
            }
            return false;
        });
    }

    /**
     * Reset layout when the focus of {@link #mEdtEmail} changed
     */
    private void onEmailFocusChange() {
        mEdtEmail.setOnFocusChangeListener((v, hasFocus) -> {
            String email = Objects.requireNonNull(mEdtEmail.getText()).toString();
            if (!hasFocus) {
                showLine(true);
                CommonUtil.getInstance().forceHideKeyBoard(mEdtEmail);
                showError(CommonUtil.isNotEmail(email) && !email.isEmpty());
            } else {
                showLine(false);
                showError(false);
                mBtSend.setEnabled(false);
            }
        });
    }

    /**
     * Change the distance between views so every view component can be visible when showing keyboard
     *
     * @param isShow true if the keyboard is not showing
     */
    private void showLine(boolean isShow) {
        if (isShow) {
            mTVLineTop.setVisibility(View.VISIBLE);
            mTvLineBottom.setVisibility(View.VISIBLE);
        } else {
            mTVLineTop.setVisibility(View.GONE);
            mTvLineBottom.setVisibility(View.GONE);
        }
    }

    /**
     * Reset layout when the input text of {@link #mEdtEmail} changed
     */
    private void onEmailChange() {
        mEdtEmail.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                CommonUtil.getInstance().initTypeFace(mEdtEmail);
                showError(false);
                mBtSend.setEnabled(!CommonUtil.isNotEmail(editable.toString()));
            }
        });
    }

    /**
     * Show or hide email error layout with the corresponding message
     *
     * @param show true if account error layout should be shown
     */
    private void showError(boolean show) {
        if (show) {
            int color = getResources().getColor(R.color.colorM009RustyRed);
            String msg = LangMgr.getInstance().getLangList().get(getString(R.string.lang_err_email_do_not_match));
            mTvError.setText(msg);
            mEdtEmail.setTextColor(color);
            mTrError.setVisibility(View.VISIBLE);
            mEdtEmail.getBackground().setLevel(CommonUtil.CODE_COLOR_ERROR);
        } else {
            mEdtEmail.setTextColor(getResources().getColor(R.color.colorBlack));
            mTrError.setVisibility(View.GONE);
            mEdtEmail.getBackground().setLevel(CommonUtil.CODE_COLOR_NORMAL);
        }
    }

    /**
     * Get layout id for Recovery Email Fragment
     *
     * @return layout id
     * @see BaseFragment#getLayoutId()
     */
    @Override
    protected int getLayoutId() {
        return R.layout.frg_m038_recovery_email;
    }

    /**
     * Handle callback from {@link OnM038RecoveryCallBack}
     * {@link OnM038RecoveryCallBack#onIA04Fail()}
     */
    @Override
    public void onIA04Fail() {
        showError(true);
        mBtSend.setEnabled(false);
    }

    /**
     * Get presenter for this fragment
     *
     * @return {@link M038RecoveryPresenter} instance
     * @see BaseFragment#getPresenter()
     */
    @Override
    protected M038RecoveryPresenter getPresenter() {
        return new M038RecoveryPresenter(this);
    }

    /**
     * @return tag in {@link String}
     * @see BaseFragment#getTAG()
     */
    @Override
    protected String getTAG() {
        return TAG;
    }

    /**
     * @see BaseFragment#defineBackKey()
     */
    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    /**
     * @param idView id of clicked view
     * @see BaseFragment#onClickView(int)
     */
    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m038_send:
                mEdtEmail.clearFocus();
                getStorage().setM038UserName(mEdtEmail.getText().toString());
                mPresenter.callIA04ResetPasswordStep1(mEdtEmail.getText().toString());
                break;

            case R.id.tv_m038_cancel:
                mEdtEmail.clearFocus();
                CommonUtil.getInstance().forceHideKeyBoard(mEdtEmail);
                mCallBack.showFrgScreen(TAG, M023LoginFrg.TAG);
                break;

            default:
                break;
        }
    }

    /**
     * Handle callback from {@link OnM038RecoveryCallBack}
     * {@link OnM038RecoveryCallBack#showM039RecoveryCodeSent()}
     */
    @Override
    public void showM039RecoveryCodeSent() {
        mCallBack.showFrgScreen(TAG, M039RecoveryCodeSentFrg.TAG);
    }
}
