package com.novo.app.view.event;

public interface OnOKDialogCallBack {
   default void handleOKButton1(){}

   default void handleOKButton2(){}

   default void handleDialogDismiss(){}
}
