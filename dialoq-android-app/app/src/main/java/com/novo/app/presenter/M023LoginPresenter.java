/// Flex Digital Health CONFIDENTIAL
/// Copyright (c) 2017-2018 Flex Digital Health, All Rights Reserved.
///
/// NOTICE:  All information contained herein is, and remains the property of Flex Digital Health. The intellectual and technical concepts contained
/// herein are proprietary to Flex Digital Health and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
/// Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
/// from Flex Digital Health.  Access to the source code contained herein is hereby forbidden to anyone except current Flex Digital Health employees, managers or contractors who have executed
/// Confidentiality and Non-disclosure agreements explicitly covering such access.
///
/// The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
/// information that is confidential and/or proprietary, and is a trade secret, of  Flex Digital Health Incorporated.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
/// OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
/// LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
/// TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
package com.novo.app.presenter;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.CADBManager;
import com.novo.app.manager.LangMgr;
import com.novo.app.manager.entities.RTokenEntity;
import com.novo.app.model.entities.CarePlanTemplateEntity;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.ConfigEntity;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.model.entities.TokenEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM023LoginCallBack;
import com.novo.app.view.widget.ProgressLoading;

/**
 * Presenter which handle all the business logic of user login functions
 * Sequences/Steps for logging in should be refered on Jama system, here are only the code portion
 * which implement those.
 * For logging in business specification & designation please go to Jama
 * <p>
 * This class acts as `Presenter` in MVP pattern
 */
public class M023LoginPresenter extends BasePresenter<OnM023LoginCallBack> {

    // Tag string for this presenter
    public static final String TAG = M023LoginPresenter.class.getName();

    // Tag string for authenticate request
    public static final String KEY_API_IA02_AUTHENTICATE_USER = "KEY_API_IA02_AUTHENTICATE_USER";

    // Shared preference key for number of failed authentication
    // TODO caution, there are M040RecoveryPresenter#FAIL_TIMES_KEY have the same value
    public static final String FAIL_TIMES_KEY = "fail_times";

    // String tag for showing dialog indicating app is not available in country
    /**
     * TODO this tag might duplicated with {@link M040RecoveryPresenter#KEY_BLOCKED_BY_COUNTRY}
     */
    public static final String KEY_BLOCKED_BY_COUNTRY = "BLOCKED_BY_COUNTRY";

    // URL for authentication request
    // TODO many other `Presenters` define the same value
    private static final String URL_API_IA02_AUTHENTICATE_USER = "user-service/api/v1/authenticate?";

    // String template for authentication request body
    private static final String BODY_IA02 = "{\"userName\": %s,\"password\": %s,\"delivery\": \"SMS\"}";

    // String tag for 'get user logged in profile' request
    private static final String KEY_UN09_GET_USER_LOGGED_IN_PROFILE = "KEY_UN09_GET_USER_LOGGED_IN_PROFILE";

    // Server URL for 'get user logged in profile' request
    private static final String URL_API_UN09_GET_USER_LOGGED_IN_PROFILE = "user-service/api/v1/users/profile?";

    // String tag for 'get user profile' request
    // TODO rename this tag since UN7 is for getting user profile (not logged in)
    private static final String KEY_UN7_GET_USER_LOGGED_IN_PROFILE = "KEY_UN07_GET_USER_LOGGED_IN_PROFILE";

    // Server URL for 'get user profile' request
    // TODO rename this tag since UN7 is for getting user profile (not logged in)
    private static final String URL_API_UN7_GET_USER_LOGGED_IN_PROFILE = "user-service/api/v1/users/%s?";

    // String tag for 'get user care plan' request
    private static final String KEY_API_CP03_GET_CARE_PLAN_USER = "KEY_API_CP03_GET_CARE_PLAN_USER";

    // Server URL for 'get user care plan' request
    private static final String URL_API_CP03_GET_CARE_PLAN_USER = "care-plan-service/api/v1/users/%s/care-plans?";

    // String tag for 'get all user care plan' request
    private static final String KEY_API_CP01_GET_ALL_CARE_PLAN = "KEY_API_CP01_GET_ALL_CARE_PLAN";

    // Server URL for 'get all user care plan' request
    private static final String URL_API_CP01_GET_ALL_CARE_PLAN = "care-plan-service/api/v1/care-plans?";

    // String tag for 'create care plan' request
    private static final String KEY_API_CP02_CREATE_CARE_PLAN = "KEY_API_CP02_CREATE_CARE_PLAN";

    // Server URL for 'create care plan' request
    private static final String URL_API_CP02_CREATE_CARE_PLAN = "care-plan-service/api/v1/users/%s/care-plans?";

    // String template for 'create care plan' request body
    private static final String BODY_CP02 = "{\"careplanTemplateId\": %s}";

    // String pattern used for recognizing a care plan is an 'Dialoq Diabetes'
    private static final String DIALOG_TEMPLATE = "Dialoq Diabetes";

    // String pattern represents if user has accepted terms & conditions
    private static final String USER_TERM = "TERM";

    // Request param name for API key that needs for server requests
    private static final String P_API_KEY = "apikey";
    // String tag used for logging token only
    private static final String TOKEN = "token: ";
    // Object that holds the whole information of logging in process
    private transient RTokenEntity rTokenEntity;

    /**
     * Constructor
     * A delegation of linked `View` object should be provided along with every new instance of
     * this class
     *
     * @param event delegation of `View` object in MVP pattern
     */
    public M023LoginPresenter(OnM023LoginCallBack event) {
        super(event);
    }

    /**
     * Handle success responses of sent server requests
     *
     * @param data response data from server
     * @see BasePresenter#handleSuccess(String, String)
     */
    @Override
    protected void handleSuccess(String data, String tag) {
        CommonUtil.wtfi(TAG, "DATA: " + data);
        switch (tag) {
            case KEY_API_IA02_AUTHENTICATE_USER:
                prepareIA02(data);
                break;
            case KEY_UN09_GET_USER_LOGGED_IN_PROFILE:
                prepareUN09(data);
                break;
            case KEY_UN7_GET_USER_LOGGED_IN_PROFILE:
                prepareUN07(data);
                break;
            case KEY_API_CP03_GET_CARE_PLAN_USER:
                prepareCP03(data);
                break;
            case KEY_API_CP02_CREATE_CARE_PLAN:
                prepareCP02(data);
                break;
            case KEY_API_CP01_GET_ALL_CARE_PLAN:
                prepareCP01(data);
                break;
            default:
                break;
        }
    }

    /**
     * Actions when CP03 API get user care plan got succeeded
     *
     * @param data user care plan data received from server
     */
    void prepareCP03(String data) {
        CommonUtil.wtfi(TAG, "prepareCP03..." + data);
        CarePlantUserEntity entity = generateData(CarePlantUserEntity.class, data);
        if (entity == null || entity.getData() == null || entity.getData().isEmpty()) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }

        if (rTokenEntity != null) {
            rTokenEntity.setDataCP03(data);
            CADBManager.getInstance().addRTokenEntity(rTokenEntity);
        }
        getStorage().setM001CarePlanUserEntity(entity);
        mListener.loginSuccess();
    }

    /**
     * Actions when CP02 API create care plan for user got succeeded
     *
     * @param data created user care plan data received from server
     */
    void prepareCP02(String data) {
        CommonUtil.wtfi(TAG, "prepareCP02..." + data);
        CarePlanTemplateEntity entity = generateData(CarePlanTemplateEntity.class, data);
        if (entity == null) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            ProgressLoading.dismiss();
            return;
        }
        if (rTokenEntity != null) {
            rTokenEntity.setDataCP02(data);
        }
        getStorage().getM001CarePlanTemplateEntity().setId(entity.getId());
        callCP03GetCarePlanOfUser();
    }

    /**
     * Actions when CP01 API get user care plan got succeeded
     *
     * @param data user care plan data received from server
     */
    void prepareCP01(String data) {
        CommonUtil.wtfi(TAG, "prepareCP01..." + data);
        CarePlanTemplateEntity entity = generateData(CarePlanTemplateEntity.class, data);
        if (entity == null) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            ProgressLoading.dismiss();
            return;
        }
        if (rTokenEntity != null) {
            rTokenEntity.setDataCP01(data);
        }
        getStorage().setM001CarePlanTemplateEntity(entity);
        callCP02CreateCarePlanForUser();
    }

    /**
     * Create and send `create care plan for user` request to server
     */
    void callCP02CreateCarePlanForUser() {
        CommonUtil.wtfi(TAG, "callCP02CreateCarePlanForUser");

        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_CP02_CREATE_CARE_PLAN);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(ERR_500));
            return;
        }
        CommonUtil.wtfi(TAG, TOKEN + token);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_CP02_CREATE_CARE_PLAN, getStorage().getM007UserEntity().getUserId() + ""));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY_CP02, getCarePlanTemplateId()));
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Get id of user care plan template, which should first be saved and available in common storage
     *
     * @return care plan template id, in {@link String}
     */
    private String getCarePlanTemplateId() {
        if (getStorage().getM001CarePlanTemplateEntity() == null || getStorage().getM001CarePlanTemplateEntity().getData() == null
                || getStorage().getM001CarePlanTemplateEntity().getData().isEmpty()) return null;

        for (int i = 0; i < getStorage().getM001CarePlanTemplateEntity().getData().size(); i++) {
            CarePlanTemplateEntity.DataInfo careInfo = getStorage().getM001CarePlanTemplateEntity().getData().get(i);
            if (careInfo.getName().equals(DIALOG_TEMPLATE)) {
                return careInfo.getCareplanTemplateId() + "";
            }
        }
        return null;
    }

    /**
     * Actions when UN07 API get user profile got succeeded
     *
     * @param data user profile data received from server
     */
    void prepareUN07(String data) {
        CommonUtil.wtfi(TAG, "prepareUN07..." + data);
        UserEntity entity = generateData(UserEntity.class, data);
        if (entity == null) {
            ProgressLoading.dismiss();
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        if (entity.getState() != null && entity.getState().equals(CommonUtil.INACTIVE)) {
            ProgressLoading.dismiss();
            mListener.showM001LandingScreen("Invalid Account");
            return;
        }
        if (rTokenEntity != null) {
            rTokenEntity.setDataUN07(data);
        }
        getStorage().setM007UserEntity(entity);
        CADBManager.getInstance().addRUserEntity(entity);
        checkBlacklistCountry(CommonUtil.IN_CASE_NOT_OFFLINE_MODE);
    }

    /**
     * Check if current version of this app is valid/invalid in current country
     * This also check if country is in blacklist, or user has not accepted to term of services
     *
     * @param isOfflineMode true if user is currently online, other wise offline
     * @return true if current app version is validated
     */
    boolean checkBlacklistCountry(boolean isOfflineMode) {
        ConfigEntity configSet = getStorage().getM001ConfigSet();
        String country = getStorage().getM007UserEntity().getCountry().toUpperCase();
        CommonUtil.wtfi("CountryUser", "CountryUser:" + country);
        CommonUtil.wtfi("BlacklistCountry", "Blacklist countries:" + getStorage().getM001ConfigSet().getBlacklistCountryEntity().get(0).getBlacklistCountry().getValue().toUpperCase());

        for (int i = 0; i < getStorage().getM001ConfigSet().getBlacklistCountryEntity().size(); i++) {
            String blacklistCountryConfigset = configSet.getBlacklistCountryEntity().get(i).getBlacklistCountry().getValue().toUpperCase();
            if (country.equals(blacklistCountryConfigset)) {
                mListener.showM001LandingFrg(CommonUtil.RECALL_COUNTRY);
                getStorage().setAppActive(false);
                ProgressLoading.dismiss();
                CommonUtil.wtfi(TAG, "This country login is in the black list");
                return false;
            }
        }
        setupUserCountry(getStorage().getM007UserEntity().getCountry());
        return checkCountryVersionLockApp(isOfflineMode);
    }

    /**
     * Set user country to common storage
     * There may have many country in configuration set, but only one will be set
     *
     * @param userCountry country will be set (in {@link String})
     */
    void setupUserCountry(String userCountry) {
        for (int i = 0; i < getStorage().getM001ConfigSet().getCountryEntity().size(); i++) {
            if (getStorage().getM001ConfigSet().getCountryEntity().get(i).getCountryCode().getValue().equals(userCountry)) {
                getStorage().setUserCountryEntity(getStorage().getM001ConfigSet().getCountryEntity().get(i));
                return;
            }
        }
    }

    /**
     * Check if current version of Dialog app is invalid
     * Rules to check is from configuration set, which is get from server
     *
     * @param isOfflineMode true if user is offline, otherwise online
     * @return true if current app version is validated
     */
    boolean checkCountryVersionLockApp(boolean isOfflineMode) {
        String country = getStorage().getM007UserEntity().getCountry().toUpperCase();
        ConfigEntity configSet = getStorage().getM001ConfigSet();
        String versionApp = "";
        try {
            PackageInfo pInfo = CAApplication.getInstance().getPackageManager().getPackageInfo(CAApplication.getInstance().getPackageName(), 0);
            versionApp = pInfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        CommonUtil.wtfi("BlockCountry", "Country:" + country);
        CommonUtil.wtfi("BlockCountry", "Blocked countries:" + getStorage().getM001ConfigSet().getBlackList().getCountries());
        if (getStorage().getM001ConfigSet().getBlackList().getCountries().contains(country)) {
            ProgressLoading.dismiss();
            mListener.showDialog(R.string.txt_m023_block_country, KEY_BLOCKED_BY_COUNTRY);
            return true;
        }

        for (int i = 0; i < getStorage().getM001ConfigSet().getCountryVersionEntity().size(); i++) {
            String countryConfigSet = configSet.getCountryVersionEntity().get(i).getCountryCode().getValue();
            String versionConfigSet = configSet.getCountryVersionEntity().get(i).getVersionName().getValue();
            String comparisonMode = configSet.getCountryVersionEntity().get(i).getVersionComparison().getValue();

            if (countryConfigSet.equals(getStorage().getUserCountryEntity().getCountryCode().getValue())) {
                switch (comparisonMode) {
                    case CommonUtil.RECALL_GREATER:
                        if (gotoLandingPage((versionApp.compareTo(versionConfigSet) > 0)))
                            return true;
                        break;
                    case CommonUtil.RECALL_EQUAL:
                        if (gotoLandingPage((versionApp.compareTo(versionConfigSet) == 0)))
                            return true;
                        break;
                    case CommonUtil.RECALL_LESS:
                        if (gotoLandingPage((versionApp.compareTo(versionConfigSet) < 0)))
                            return true;
                        break;
                    case CommonUtil.RECALL_GREATER_OR_EQUAL:
                        if (gotoLandingPage((versionApp.compareTo(versionConfigSet) >= 0)))
                            return true;
                        break;
                    case CommonUtil.RECALL_LESS_OR_EQUAL:
                        if (gotoLandingPage((versionApp.compareTo(versionConfigSet) <= 0)))
                            return true;
                        break;
                    default:
                        break;
                }
            }
        }

        CommonUtil.wtfi("M023Presenter", "Starting callCP03");
        return checkForAcceptedTerm(isOfflineMode);
    }

    /**
     * Try to go to login result screen due to invalidated app version
     *
     * @param isBack true if current app is not valid, and login result should be showed up
     *               otherwise do nothing
     * @return true if login result screen started showing up
     */
    boolean gotoLandingPage(boolean isBack) {
        if (!isBack) return false;
        mListener.showM001LandingFrg(CommonUtil.RECALL_COUNTRY_VERSION);
        getStorage().setAppActive(false);
        CommonUtil.wtfi(TAG, "Check for recall country and version");
        ProgressLoading.dismiss();
        return true;
    }

    /**
     * Check if user has accepted term of services or not
     * In case of DID accepted, start CP03 getting user care plan
     *
     * @param isOfflineMode true if user is offline, otherwise online
     * @return TRUE if user did NOT accepted, otherwise false
     */
    boolean checkForAcceptedTerm(boolean isOfflineMode) {
        if (!getStorage().getM007UserEntity().getFamilyHistory().contains(USER_TERM)) {
            ProgressLoading.dismiss();
            mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(ERR_500));
            return true;
        }

        if (CommonUtil.getInstance().isConnectToNetwork() && isOfflineMode == CommonUtil.IN_CASE_NOT_OFFLINE_MODE) {
            callCP03GetCarePlanOfUser();
        }
        return false;
    }

    /**
     * Request to get care plan of user
     */
    void callCP03GetCarePlanOfUser() {
        CommonUtil.wtfi(TAG, "callCP03GetAllCarePlanTemplete");

        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(KEY_API_CP03_GET_CARE_PLAN_USER);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, TOKEN + token);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_CP03_GET_CARE_PLAN_USER, getStorage().getM007UserEntity().getUserId()));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Request to get user profile
     */
    void callIUN07GetUserLoggedInProfile() {
        CommonUtil.wtfi(TAG, "callIUN07GetUserLoggedInProfile");

        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(KEY_UN7_GET_USER_LOGGED_IN_PROFILE);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            CommonUtil.wtfi(TAG, TOKEN + token);
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, TOKEN + token);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_UN7_GET_USER_LOGGED_IN_PROFILE,
                getStorage().getM007UserEntity().getUserId()));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Request to authenticate user
     *
     * @param userName user name
     * @param pass     password
     */
    public void callIA02AuthenticateUser(String userName, String pass) {
        rTokenEntity = new RTokenEntity();
        rTokenEntity.setUserName(userName);
        rTokenEntity.setPassword(pass);

        CommonUtil.wtfi(TAG, "callIA02AuthenticateUser");

        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_IA02_AUTHENTICATE_USER);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addPathSegment(URL_API_IA02_AUTHENTICATE_USER);

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());

        request.addBody(generateJson(BODY_IA02, userName, pass));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Actions when user authentication request got succeeded
     *
     * @param data authentication data received from server
     */
    void prepareIA02(String data) {
        if (rTokenEntity != null) {
            rTokenEntity.setDataIA02(data);
            rTokenEntity.setConfigSet(getStorage().getConfigSetData());
        }

        CommonUtil.getInstance().savePrefContent(FAIL_TIMES_KEY, 0);
        CommonUtil.wtfi(TAG, "prepareIA02..." + data);
        TokenEntity tokenEntity = generateData(TokenEntity.class, data);
        if (tokenEntity == null) {
            ProgressLoading.dismiss();
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }

        CommonUtil.getInstance().savePrefContent(CommonUtil.TOKEN_KEY, tokenEntity.getToken());
        callIUN09GetUserLoggedInProfile();
    }

    /**
     * Request to get user care plan template
     */
    void callCP01GetAllCarePlanTemplete() {
        CommonUtil.wtfi(TAG, "callCP01GetAllCarePlanTemplate");

        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(KEY_API_CP01_GET_ALL_CARE_PLAN);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, TOKEN + token);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(URL_API_CP01_GET_ALL_CARE_PLAN);

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Request to get user logged in profile
     * This different from UN07 when user did authenticated successfully
     */
    void callIUN09GetUserLoggedInProfile() {
        CommonUtil.wtfi(TAG, "callIUN09GetUserLoggedInProfile");

        ProgressLoading.dontShow();
        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(KEY_UN09_GET_USER_LOGGED_IN_PROFILE);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, TOKEN + token);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(URL_API_UN09_GET_USER_LOGGED_IN_PROFILE);

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    /**
     * Actions when user logged in profile successfully got from server
     *
     * @param data user logged in profile
     */
    void prepareUN09(String data) {
        CommonUtil.wtfi(TAG, "prepareUN09..." + data);
        if (rTokenEntity != null) {
            rTokenEntity.setDataUN09(data);
        }
        UserEntity entity = generateData(UserEntity.class, data);
        if (entity == null) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        getStorage().setM007UserEntity(entity);
        callIUN07GetUserLoggedInProfile();
    }

    /**
     * Handler server request errors
     *
     * @see BasePresenter#doFailed(String, Exception, int, String)
     */
    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tagRequest);

        if (code == 204) {
            if (KEY_API_CP03_GET_CARE_PLAN_USER.equals(tag)) {
                callCP01GetAllCarePlanTemplete();
            }
            return;
        }

        ProgressLoading.dismiss();
        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        String sms;

        if (responseEntity != null) {
            sms = responseEntity.getErrorEntity()[0].getMessage();
            switch (tag) {
                case KEY_API_CP01_GET_ALL_CARE_PLAN:
                case KEY_API_CP02_CREATE_CARE_PLAN:
                    mListener.showM001LandingScreen(sms);
                    break;
                case KEY_API_IA02_AUTHENTICATE_USER:
                    String mCode = responseEntity.getErrorEntity()[0].getCode();
                    if (mCode != null && mCode.equals(CommonUtil.ERROR_ERR_401)) {
                        mListener.wrongAccount();
                    } else {
                        mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(ERR_500));
                    }
                    break;
                case KEY_UN09_GET_USER_LOGGED_IN_PROFILE:
                    mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(ERR_500));
                    break;
                default:
                    break;
            }
        } else {
            if (CommonUtil.getInstance().isConnectToNetwork()) {
                mListener.showAlertDialog(LangMgr.getInstance().getLangList().get(ERR_500));
            } else {
                mListener.showAlertDialog(R.string.txt_login_failed_network_err);
            }
        }
    }

    /**
     * Override {@link BasePresenter#hideLockDialog(String)} in order to keep progressing dialog
     * Logging in session is a long sequence of many APIs called continuously, so it's better to
     * keep progressing dialog on screen until the last API call got succeeded
     */
    @Override
    public void hideLockDialog(String key) {
        //do nothing
    }
}
