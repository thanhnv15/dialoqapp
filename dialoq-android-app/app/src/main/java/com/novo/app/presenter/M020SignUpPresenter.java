package com.novo.app.presenter;

import com.novo.app.view.event.OnM020SignUpCallBack;

public class M020SignUpPresenter extends BasePresenter<OnM020SignUpCallBack> {
    public M020SignUpPresenter(OnM020SignUpCallBack event) {
        super(event);
    }
}
