package com.novo.app.view.fragment;

import android.app.AlertDialog;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M043SignUpDidntGetPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM043SignUpDidntGetCallBack;
import com.novo.app.view.event.OnOKDialogCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

public class M043SignUpDidntGetFrg extends BaseFragment<M043SignUpDidntGetPresenter, OnHomeBackToView> implements OnM043SignUpDidntGetCallBack {
    public static final String TAG = M043SignUpDidntGetFrg.class.getName();
    private TextView mTvEmail;
    private AlertDialog mAlertDialog;

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m043_close, this);
        findViewById(R.id.tv_m043_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m043_guide, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m043_suggest_1, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m043_suggest_2, CAApplication.getInstance().getRegularFont());

        findViewById(R.id.bt_m043_try_again, this, CAApplication.getInstance().getMediumFont());
        mTvEmail = findViewById(R.id.tv_m043_email, CAApplication.getInstance().getBoldFont());
        mTvEmail.setText(getStorage().getM001ProfileEntity().getEmail());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m043_signup_didnt_get;
    }

    @Override
    protected M043SignUpDidntGetPresenter getPresenter() {
        return new M043SignUpDidntGetPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {

    }

    @Override
    public void backToPreviousScreen() {
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m043_close:
                mCallBack.showFrgScreen(TAG, M009SignUpEmailFrg.TAG);
                break;
            case R.id.bt_m043_try_again:
                getStorage().getM001ProfileEntity().setEmail(mTvEmail.getText().toString());
                mPresenter.sendCodeUN42();
                break;
        }
    }

    @Override
    public void showM013SignUpCode() {
        mCallBack.showFrgScreen(TAG, M013SignUpCodeSentFrg.TAG);
    }

    @Override
    public void verifyEmailSuccess(String success) {
        if (success.equals(CommonUtil.SUCCESS_VALUE)) {
            mCallBack.showFrgScreen(TAG, M013SignUpCodeSentFrg.TAG);
        } else {
            showNotify(LangMgr.getInstance().getLangList().get(getString(R.string.lang_err_unknown_reason)));
        }
    }

    @Override
    public void verifyEmailFail(String message) {
        if (mAlertDialog != null && mAlertDialog.isShowing()) return;
        mAlertDialog = CommonUtil.getInstance().showDialog(mContext, message, new OnOKDialogCallBack() {
        });
    }
}
