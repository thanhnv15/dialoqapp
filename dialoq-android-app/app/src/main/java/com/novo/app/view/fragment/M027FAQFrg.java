package com.novo.app.view.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.FAQEntity;
import com.novo.app.presenter.M027FAQPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.M027FAQsAdapter;
import com.novo.app.view.base.BaseActivity;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM027FAQOnCallBack;
import com.novo.app.view.widget.TextAdapter;

import java.util.ArrayList;
import java.util.List;

public class M027FAQFrg extends BaseFragment<M027FAQPresenter, OnM024DoseLogCallBack> implements OnM027FAQOnCallBack {
    public static final String TAG = M027FAQFrg.class.getName();
    private EditText mEditSearch;
    private M027FAQsAdapter mAdapter;
    private List<FAQEntity> mFaqList = new ArrayList<>();
    private TextView mTvCancel;
    private TableRow mTrSearch;
    private ImageView mIvBack;


    @Override
    protected void initViews() {
        mIvBack = findViewById(R.id.iv_m027_back, this);
        findViewById(R.id.iv_m027_search, this);
        findViewById(R.id.bt_m027_send, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.iv_m027_sub_search);
        findViewById(R.id.ln_m027_search);
        mTrSearch = findViewById(R.id.tr_m027_search);
        mEditSearch = findViewById(R.id.ed_m027_search, CAApplication.getInstance().getRegularFont());
        mTvCancel = findViewById(R.id.tv_m027_cancel, this, CAApplication.getInstance().getRegularFont());
        TextView tvTitle = findViewById(R.id.tv_m027_title, CAApplication.getInstance().getBoldFont());
        String title = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m047_support_title_troubleshooting));
        tvTitle.setText(title);

        RecyclerView rvFAQs = findViewById(R.id.rv_m027_faqs);
        rvFAQs.setLayoutManager(new LinearLayoutManager(mContext));
        mFaqList = mPresenter.getData("");
        mAdapter = new M027FAQsAdapter(mContext, mFaqList, this);
        rvFAQs.setAdapter(mAdapter);

        inputText();
    }

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m027_back:
                CommonUtil.getInstance().forceHideKeyBoard(mIvBack);
                backToPreviousScreen();
                break;
            case R.id.iv_m027_search:
                mTrSearch.setVisibility(View.VISIBLE);
                break;
            case R.id.bt_m027_send:
                mCallBack.showChildFrgScreen(TAG, M049SupportContactFrg.TAG);
                break;
            case R.id.tv_m027_cancel:
                mEditSearch.getText().clear();
                hideKeyboard(mContext);
                mTrSearch.setVisibility(View.GONE);
                filterData("");
                break;
            default:
                break;
        }
    }

    private void inputText() {
        mEditSearch.addTextChangedListener(new TextAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                mTvCancel.setVisibility(View.VISIBLE);
                filterData(editable.toString());
            }
        });
    }

    private void filterData(String text) {
        mAdapter.filterList(mPresenter.getData(text.toUpperCase()));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m027_support_faqs;
    }

    @Override
    protected M027FAQPresenter getPresenter() {
        return new M027FAQPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void setTagCurrentFrg() {
        ((BaseActivity) getActivity()).setTagCurrentFrg(M024DoseLogFrg.TAG);
    }

    @Override
    protected void defineBackKey() {
        if (getTagSource().equals(M047SupportTroubleshootingFrg.TAG)
                || getTagSource().equals(M049SupportContactFrg.TAG)) {
            CommonUtil.getInstance().defineBackTag(getTagSource(), TAG);
        }
    }

    @Override
    public void backToPreviousScreen() {
        String backTag = CommonUtil.getInstance().doBackFlowWithBackTag(TAG);
        if (backTag.equals(M047SupportTroubleshootingFrg.TAG)
                || backTag.equals(M049SupportContactFrg.TAG)) {
            mCallBack.showChildFrgScreen(M026SupportFrg.TAG, M047SupportTroubleshootingFrg.TAG);
        }
    }
}
