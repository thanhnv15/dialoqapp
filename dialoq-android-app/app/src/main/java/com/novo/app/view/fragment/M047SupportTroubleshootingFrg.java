package com.novo.app.view.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M047SupportTroubleshootingPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.adapter.M047TroubleShootingAdapter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM047SupportTroubleshootingCallBack;

public class M047SupportTroubleshootingFrg extends BaseFragment<M047SupportTroubleshootingPresenter, OnM024DoseLogCallBack> implements OnM047SupportTroubleshootingCallBack {
    public static final String TAG = M047SupportTroubleshootingFrg.class.getName();

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m047_back, this);
        findViewById(R.id.tv_m047_title, CAApplication.getInstance().getBoldFont());

        RecyclerView rvTroubleshooting = findViewById(R.id.rv_m047_troubleshooting);
        rvTroubleshooting.setLayoutManager(new LinearLayoutManager(mContext));
        M047TroubleShootingAdapter adapter = new M047TroubleShootingAdapter(mContext, getStorage().getM001ConfigSet().getListQnA(), this);
        rvTroubleshooting.setAdapter(adapter);
    }

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m047_support_troubleshooting;
    }

    @Override
    protected M047SupportTroubleshootingPresenter getPresenter() {
        return new M047SupportTroubleshootingPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        if (getTagSource().equals(M026SupportFrg.TAG)
                || getTagSource().equals(M031DoseDetailFrg.TAG)
                || getTagSource().equals(M049SupportContactFrg.TAG)) {
            CommonUtil.getInstance().defineBackTag(getTagSource(), TAG);
        }
    }

    @Override
    public void backToPreviousScreen() {
        String backTag = CommonUtil.getInstance().doBackFlowWithBackTag(TAG);
        if (backTag.equals(M026SupportFrg.TAG)) {
            mCallBack.showChildFrgScreen(TAG, M026SupportFrg.TAG);
            return;
        } else if (backTag.equals(M031DoseDetailFrg.TAG)) {
            mCallBack.showChildFrgScreen(TAG, M031DoseDetailFrg.TAG);
            return;
        } else if (backTag.equals(M049SupportContactFrg.TAG)) {
            mCallBack.showChildFrgScreen(M027FAQFrg.TAG, M049SupportContactFrg.TAG);
            return;
        }
        mCallBack.showChildFrgScreen(TAG, getTagSource());
    }

    @Override
    protected void onClickView(int idView) {
        if (idView == R.id.iv_m047_back) {
            backToPreviousScreen();
        }
    }

    @Override
    public void toSingleQA() {
        mCallBack.showChildFrgScreen(TAG, M050TroubleshootingSingleTopicFrg.TAG);
    }

    @Override
    public void toMultipleQA() {
        mCallBack.showChildFrgScreen(TAG, M027FAQFrg.TAG);
    }

}
