package com.novo.app.view.widget.novocharts.formatter;

import android.graphics.drawable.Drawable;

import com.github.mikephil.charting.formatter.ValueFormatter;
import com.novo.app.model.entities.DateEntity;
import com.novo.app.view.widget.novocharts.utils.NovoChartUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NovoXValueFormatter extends ValueFormatter {

    private String[] mArrayDate;
    private Drawable noteIconDrawable;
    private List<DateEntity> dateEntityList;
    public NovoXValueFormatter(Date startDate, Date endDate, Drawable noteIconDrawable, List<DateEntity> dateEntityList) {
        mArrayDate = NovoChartUtils.getDateStringsWithTimeRange(startDate, endDate);
        this.noteIconDrawable = noteIconDrawable;
        this.dateEntityList = dateEntityList;
    }

    public NovoXValueFormatter(Date startDate, Date endDate) {
        mArrayDate = NovoChartUtils.getDateStringsWithTimeRange(startDate, endDate);
    }

    @Override
    public String getFormattedValue(float value) {
        HashMap<String, Boolean> result = new HashMap<>();

        int index = (int) value;
        if (index < mArrayDate.length) {
            result.put(mArrayDate[index], dateEntityList.get(index).isNote());
            return mArrayDate[index];
        } else {
            return "";
        }
    }

    public Drawable getNoteIconDrawable() {
        return noteIconDrawable;
    }

    public String[] getmArrayDate() {
        return mArrayDate;
    }

    public Map<String, Boolean> getHashMapDateNote() {
        Map<String, Boolean> result = new HashMap<>();

        for (int i = 0 ; i < mArrayDate.length; i++) {
            result.put(mArrayDate[i], dateEntityList.get(i).isNote());
        }

        return result;
    }
}
