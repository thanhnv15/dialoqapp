package com.novo.app.model.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResultInfo implements Serializable {
    @SerializedName("resultId")
    private String resultId;
    @SerializedName("deviceId")
    private String deviceId;
    @SerializedName("assessmentId")
    private String assessmentId;
    @SerializedName("assessmentType")
    private String assessmentType;
    @SerializedName("result")
    private long result;
    @SerializedName("reportedDate")
    private String reportedDate;
    @SerializedName("notes")
    private String notes;
    @SerializedName("custom1")
    private String custom1;
    @SerializedName("custom2")
    private String custom2;
    @SerializedName("custom3")
    private String custom3;
    @SerializedName("custom4")
    private String custom4;

    public String getResultId() {
        return resultId;
    }

    public void setResultId(String resultId) {
        this.resultId = resultId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAssessmentType() {
        return assessmentType;
    }

    public void setAssessmentType(String assessmentType) {
        this.assessmentType = assessmentType;
    }

    public long getResult() {
        return result;
    }

    public void setResult(long result) {
        this.result = result;
    }

    public String getReportedDate() {
        return reportedDate;
    }

    public void setReportedDate(String reportedDate) {
        this.reportedDate = reportedDate;
    }

    public String getAssessmentId() {
        return assessmentId;
    }

    public void setAssessmentId(String assessmentId) {
        this.assessmentId = assessmentId;
    }

    public String getCustom3() {
        return custom3;
    }

    public void setCustom3(String custom3) {
        this.custom3 = custom3;
    }

    public String getCustom4() {
        return custom4;
    }

    public void setCustom4(String custom4) {
        this.custom4 = custom4;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCustom1() {
        return custom1;
    }

    public void setCustom1(String custom1) {
        this.custom1 = custom1;
    }

    public String getCustom2() {
        return custom2;
    }

    public void setCustom2(String custom2) {
        this.custom2 = custom2;
    }

}
