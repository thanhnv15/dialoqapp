package com.novo.app.model.entities;

import com.novo.app.model.BaseModel;

import java.util.List;

public class VideoEntity extends BaseModel {
    private String keyVideo;
    private List<TextEntity> listVideo;

    public VideoEntity(String keyVideo, List<TextEntity> listVideo) {
        this.keyVideo = keyVideo;
        this.listVideo = listVideo;
    }

    public String getKeyVideo() {
        return keyVideo;
    }

    public List<TextEntity> getListVideo() {
        return listVideo;
    }
}
