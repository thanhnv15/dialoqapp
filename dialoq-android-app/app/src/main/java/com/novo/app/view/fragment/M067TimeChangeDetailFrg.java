package com.novo.app.view.fragment;

import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M067TimeChangePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM067TimeChangeCallBack;
import com.novo.app.view.widget.NVDateUtils;

import java.util.Locale;
import java.util.TimeZone;

public class M067TimeChangeDetailFrg extends BaseFragment<M067TimeChangePresenter, OnM024DoseLogCallBack> implements OnM067TimeChangeCallBack {
    public static final String TAG = M067TimeChangeDetailFrg.class.getName();


    @Override
    protected M067TimeChangePresenter getPresenter() {
        return new M067TimeChangePresenter(this);
    }

    @Override
    public void showM001LandingScreen(String message) {
        mCallBack.showM001LandingScreen(message);
    }

    @Override
    protected void initViews() {
        mCallBack.changeTab(CommonUtil.TAB_MENU_DOSE_LOG);
        String date = NVDateUtils.getDoseDetailsTimeString(getStorage().getM025ResultDataItem().getResult().getReportedDate());
        TextView mTvTime = findViewById(R.id.tv_m067_time, CAApplication.getInstance().getRegularFont());
        mTvTime.setText(date);

        findViewById(R.id.tv_m067_reminder_pause, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m067_what_happened, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m067_content_1, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m067_reminder_setting, this, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m067_current_time_zone, CAApplication.getInstance().getMediumFont());

        TextView mTvTimeZone = findViewById(R.id.tv_m067_time_zone, CAApplication.getInstance().getRegularFont());
        String timeZone = getStorage().getM025ResultDataItem().getResult().getCustom2();

        if (timeZone != null) {
            TimeZone tz = TimeZone.getTimeZone(timeZone);

            timeZone = tz.getDisplayName(Locale.getDefault()) + " " + CommonUtil.getZDateNow();
            mTvTimeZone.setText(timeZone);
        }

        findViewById(R.id.iv_m067_back, this);
    }


    @Override
    public int getLayoutId() {
        return R.layout.frg_m067_time_change_detail;
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        //This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        //This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m067_back:
                mCallBack.showChildFrgScreen(TAG, M025SubDoseLogFrg.TAG);
                break;

            case R.id.tv_m067_reminder_setting:
                mCallBack.showChildFrgScreen(TAG, M065MoreReminderNotEmptyFrg.TAG);
                mCallBack.changeTab(CommonUtil.TAB_MENU_MORE);
                break;
            default:
                break;
        }
    }

}
