package com.novo.app.view.fragment;

import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.presenter.M035ReminderDonePresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM035ReminderDoneCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

import java.util.Locale;

public class M035ReminderDoneFrg extends BaseFragment<M035ReminderDonePresenter, OnHomeBackToView> implements OnM035ReminderDoneCallBack {
    public static final String TAG = M035ReminderDoneFrg.class.getName();

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m035_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m035_description, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m035_edit_reminder, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.tv_m035_drug, CAApplication.getInstance().getBoldFont());
        TextView mTvTime = findViewById(R.id.tv_m035_time, CAApplication.getInstance().getLightFont());
        mTvTime.setText(currentReminder());

        findViewById(R.id.bt_m035_done, this, CAApplication.getInstance().getMediumFont());

        findViewById(R.id.iv_m035_drug);
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    private String currentReminder() {
        int hour = CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_HOUR, -1);
        int min = CommonUtil.getInstance().getIntPrefContent(CommonUtil.REMINDER_MIN, -1);
        boolean isPM = (hour >= 12);
        return String.format(Locale.getDefault(), "%02d:%02d %s", (hour == 12 || hour == 0) ? 12 : hour % 12, min, isPM ? "PM" : "AM");
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m035_reminder_done;
    }

    @Override
    protected M035ReminderDonePresenter getPresenter() {
        return new M035ReminderDonePresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        // This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        // This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.bt_m035_done:
                mCallBack.showFrgScreen(TAG, M044PairCompleteFrg.TAG);
                break;
            case R.id.tv_m035_edit_reminder:
                mCallBack.showFrgScreen(TAG, M041ReminderAddFrg.TAG);
                break;
        }
    }
}
