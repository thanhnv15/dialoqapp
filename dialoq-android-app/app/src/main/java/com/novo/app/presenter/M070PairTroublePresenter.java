package com.novo.app.presenter;

import com.novo.app.view.event.OnM070PairTrouble;

public class M070PairTroublePresenter extends BasePresenter<OnM070PairTrouble> {
    public M070PairTroublePresenter(OnM070PairTrouble event) {
        super(event);
    }
}
