package com.novo.app.manager;

import android.content.res.Configuration;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.model.entities.ConfigEntity;
import com.novo.app.model.entities.LangEntity;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnLanguageCallBack;

import java.util.HashMap;
import java.util.Locale;

public class LangMgr extends BaseMgr {

    public static final String LANGUAGE_FILE = "lang_";
    private static final String TAG = LangMgr.class.getName();
    private static final String ENGLISH = "English";
    private static final String FRENCH = "Français";
    private static final String DEUTSCHE = "Deutsche";
    private static final String DANSK = "Dansk";

    private static LangMgr mInstance;
    private HashMap<String, String> mLangList = new HashMap<>();
    private OnLanguageCallBack callBack;
    private String mCurrentLang;

    private LangMgr() {
        //for singleton
    }

    public static LangMgr getInstance() {
        if (mInstance == null) {
            mInstance = new LangMgr();
        }
        return mInstance;
    }

    public void setCallBack(OnLanguageCallBack callBack) {
        this.callBack = callBack;
    }

    public void configLanguage(TextEntity mData) {
        if (mData == null) {
            callBack.filterLanguage();
            return;
        }
        mData = verifyData(mData);
        if (mData == null) {
            callBack.filterLanguage();
            return;
        }

        CommonUtil.wtfi(TAG, "DATA - LANG: " + mData.getKey());
        if (initLanguage(mData.getKey())) {
            callBack.configLanguageDone();
        } else {
            if (CommonUtil.getInstance().isConnectToNetwork()){
                callBack.filterLanguage();
            } else {
                callBack.filterLanguageNoConnection();
            }
        }
    }

    private TextEntity verifyData(TextEntity mData) {
        ConfigEntity configEntity = getStorage().getM001ConfigSet();
        if(configEntity == null) return null;

        for (int i = 0; i < configEntity.getLanguageEntity().size(); i++) {
            TextEntity item = configEntity.getLanguageEntity().get(i);
            if (mData.getValue().equals(item.getValue())) {
                if (mData.getKey().equals(item.getKey())) {
                    return mData;
                } else {
                    return item;
                }
            }
        }
        return mData;
    }


    public void dataLanguageReady(String key) {
        if (initLanguage(key)) {
            callBack.configLanguageDone();
        }
    }

    private void configLocate(Locale locale) {
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        CAApplication.getInstance().getResources().updateConfiguration(config, null);
    }

    private boolean initLanguage(String key) {
        String data = CommonUtil.getInstance().getObjData(LANGUAGE_FILE + key);
        if (data == null || data.isEmpty()) {
            getStorage().setLanguageKeyToGet(key);
            return false;
        }

        LangEntity langEntity = CommonUtil.generateData(LangEntity.class, data);
        if (langEntity == null || langEntity.getData() == null) {
            CommonUtil.getInstance().showNotify(R.string.txt_system_err);
            return false;
        }

        if (!langEntity.getConfigCode().contains(key)) {
            return false;
        }

        mLangList.clear();
        for (int i = 0; i < langEntity.getData().length; i++) {
            LangEntity.LangInfo entity = langEntity.getData()[i];
            mLangList.put(entity.getKey(), entity.getValue());
        }

        callBack.updateBottomBar();
        switch (key) {
            case "en":
            case "de":
            case "dk":
            case "it":
            case "ty":
                mCurrentLang = ENGLISH;
                LangMgr.getInstance().configLocate(Locale.forLanguageTag("en-US"));
                CommonUtil.getInstance().savePrefContent(CommonUtil.LANG_KEY, "en");
                CommonUtil.getInstance().savePrefContent(CommonUtil.LANG_VALUE, ENGLISH);
                break;
            case "fr":
                mCurrentLang = FRENCH;
                LangMgr.getInstance().configLocate(Locale.forLanguageTag("fr-FR"));
                CommonUtil.getInstance().savePrefContent(CommonUtil.LANG_KEY, "fr");
                CommonUtil.getInstance().savePrefContent(CommonUtil.LANG_VALUE, FRENCH);
                break;
            default:
                break;
        }
        return true;
    }

    public HashMap<String, String> getLangList() {
        return mLangList;
    }

    public String getCurrentLang() {
        return mCurrentLang;
    }
}
