package com.novo.app.presenter;

import com.novo.app.view.event.OnM004AccountSetUpCallBack;

public class M004AccountSetUpPresenter extends BasePresenter<OnM004AccountSetUpCallBack> {
    public M004AccountSetUpPresenter(OnM004AccountSetUpCallBack event) {
        super(event);
    }
}
