package com.novo.app.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.view.event.OnM048SupportVideoCallBack;

import java.util.List;

public class M048VideoAdapter extends BaseRecycleAdapter<OnM048SupportVideoCallBack, TextEntity, M048VideoAdapter.VideoHolder> implements OnM048SupportVideoCallBack {
    public static final String TAG = M048VideoAdapter.class.getName();
    private static final int NO_LAYOUT = -1;
    private static final int TYPE_LAYOUT = 0;
    private static final int NAME_LAYOUT = 1;
    private static final String TYPE_VALUE = "VideoType";

    public M048VideoAdapter(Context mContext, List<TextEntity> mListData, OnM048SupportVideoCallBack mCallBack) {
        super(mContext, mListData, mCallBack);
    }

    @Override
    public void showM001LandingScreen(String err) {
        mCallBack.showM001LandingScreen(err);
    }

    @Override
    protected int getLayoutId(int viewType) {
        switch (viewType) {
            case TYPE_LAYOUT:
                return R.layout.item_m048_video_type;
            case NAME_LAYOUT:
                return R.layout.item_m048_video;
            default:
                break;
        }
        return NO_LAYOUT;
    }

    @Override
    protected VideoHolder getViewHolder(int viewType, View itemView) {
        return new VideoHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        if (mListData.get(position).getValue().equals(TYPE_VALUE)) {
            return TYPE_LAYOUT;
        } else return NAME_LAYOUT;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        VideoHolder item = (VideoHolder) holder;
        TextEntity data = mListData.get(position);

        if (data.getValue().equals(TYPE_VALUE)) {
            initViewType(item, data);
            return;
        }
        initViewName(item, data);
    }

    private void initViewType(VideoHolder item, TextEntity data) {
        item.tvVideoType.setText(LangMgr.getInstance().getLangList().get(data.getKey()));
    }

    private void initViewName(VideoHolder item, TextEntity data) {
        item.tvVideoName.setText(data.getKey());
    }

    @Override
    public void toVideoPlayer(String url) {

    }

    public class VideoHolder extends BaseHolder {
        TableRow trVideo;
        TextView tvVideoName;
        TextView tvVideoType;
        ImageView ivArrow;

        VideoHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void onClickView(int idView) {
            switch (idView) {
                case R.id.tr_m048_video:
                case R.id.iv_m048_arrow:
                case R.id.tv_m048_video_name:
                    mCallBack.toVideoPlayer(mListData.get(getAdapterPosition()).getValue());
                    break;
            }
        }

        @Override
        protected void initView() {
            trVideo = findViewById(R.id.tr_m048_video, this);
            ivArrow = findViewById(R.id.iv_m048_arrow, this);
            tvVideoName = findViewById(R.id.tv_m048_video_name, this, CAApplication.getInstance().getRegularFont());
            tvVideoType = findViewById(R.id.tv_m048_video_type, this, CAApplication.getInstance().getBoldFont());
        }
    }

}