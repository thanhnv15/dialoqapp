package com.novo.app.presenter;

import com.novo.app.view.event.OnM013SignUpCallBack;

public class M013SignUpPresenter extends BasePresenter<OnM013SignUpCallBack> {

    public M013SignUpPresenter(OnM013SignUpCallBack event) {
        super(event);
    }
}
