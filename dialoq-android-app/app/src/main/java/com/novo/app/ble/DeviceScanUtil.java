/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.novo.app.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.ParcelUuid;

import com.novo.app.utils.CommonUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class DeviceScanUtil {
    private static final String TAG = DeviceScanUtil.class.getName();
    private static final String UUID_CA = "00000007-0000-1000-8000-00805f9b0000";

    private OnBLECallBack mCallBack;

    private BluetoothAdapter mBluetoothAdapter;
    private ScanCallback callback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            CommonUtil.wtfd(TAG,"onScanResult..." + callbackType + ":" + result);
            if (result.getScanRecord() != null && result.getScanRecord().getServiceUuids() != null
                    && result.getScanRecord().getServiceUuids().contains(ParcelUuid.fromString(UUID_CA))) {

                if (result.getScanRecord().getDeviceName() == null || result.getScanRecord().getDeviceName().isEmpty())
                    return;

                CommonUtil.wtfd(TAG, "onScanResult...Address" + result.getDevice().getAddress());
                CommonUtil.wtfd(TAG, "onScanResult...Name" + Objects.requireNonNull(result.getScanRecord()).getDeviceName());
                CommonUtil.wtfd(TAG, "onScanResult...Uuid" + result.getScanRecord().getServiceUuids());

                BluetoothDevice myDevice = result.getDevice();
                if (mCallBack != null) {
                    mCallBack.onStartPairing(myDevice, result.getScanRecord().getDeviceName());
                    stopScan();
                }
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            mCallBack.onScanFailed();
            CommonUtil.wtfd(TAG, "onScanFailed...");
        }
    };

    public void setCallBack(OnBLECallBack mCallBack) {
        this.mCallBack = mCallBack;
    }

    public void stopScan() {
        if (mBluetoothAdapter == null) return;
        try {
            mBluetoothAdapter.getBluetoothLeScanner().stopScan(callback);
        }catch (Exception ignore){}
    }

    public boolean initBLE(Context context) {
        CommonUtil.wtfd(TAG, "initView...");

        final BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            if (mCallBack != null) mCallBack.onRequestPermission();
            return false;
        }
        return true;
    }

    public void startScan() {
        CommonUtil.wtfd(TAG, "startScan...");
        Set<BluetoothDevice> pairedDevice = mBluetoothAdapter.getBondedDevices();
        if (pairedDevice.size() > 0) {
            for (BluetoothDevice device : pairedDevice) {
                CommonUtil.wtfd(TAG, "initView:-Bonded" + device.getName() + "\n" + device.getAddress());
            }
        }
        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .build();

        List<ScanFilter> filters = new ArrayList<>();
        filters.add(new ScanFilter.Builder().setServiceUuid(new ParcelUuid(UUID.fromString(UUID_CA))).build());
        mBluetoothAdapter.getBluetoothLeScanner().startScan(filters, settings, callback);
    }

    public void disconnectBLE() {
        if (mBluetoothAdapter == null) return;

        mBluetoothAdapter.cancelDiscovery();
    }
}