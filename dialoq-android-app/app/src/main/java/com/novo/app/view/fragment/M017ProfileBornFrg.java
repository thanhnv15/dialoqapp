package com.novo.app.view.fragment;

import android.widget.Button;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M017ProfileBornPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM017ProfileBornCallBack;
import com.novo.app.view.widget.NVDateFormat;
import com.novo.app.view.widget.NVDateUtils;
import com.novo.app.view.widget.OnOKDialogAdapter;

import java.util.Calendar;
import java.util.Date;

public class M017ProfileBornFrg extends BaseFragment<M017ProfileBornPresenter, OnHomeBackToView> implements OnM017ProfileBornCallBack {
    public static final String TAG = M017ProfileBornFrg.class.getName();
    private TextView mTvDOB;
    private Button mBtNext;

    @Override
    protected void initViews() {
        findViewById(R.id.tv_m017_account_setup, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m017_what_your_dob, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m017_cancel, this, CAApplication.getInstance().getMediumFont());

        mBtNext = findViewById(R.id.bt_m017_next, this, CAApplication.getInstance().getMediumFont());
        mBtNext.setEnabled(false);

        mTvDOB = findViewById(R.id.tv_m017_born, this, CAApplication.getInstance().getRegularFont());

        String hint = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m017_born));
        if (getStorage().getM001ProfileEntity().getYearBorn() != null) {
            mTvDOB.setText(getStorage().getM001ProfileEntity().getYearBorn());
            mTvDOB.setTypeface(CAApplication.getInstance().getBoldFont());
        }

        if (!mTvDOB.getText().toString().equals(hint)) mBtNext.setEnabled(true);
        findViewById(R.id.iv_m017_born, this);
        findViewById(R.id.iv_m017_back, this);

    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    private boolean isMature() {
        String dateOfBirth = getStorage().getM001ProfileEntity().getDateOfBirth();
        int birthYear = Integer.parseInt(dateOfBirth.substring(0, 4));
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        String thisBirthDay = dateOfBirth.replace(String.valueOf(birthYear), String.valueOf(currentYear));

        Date userDOB = NVDateUtils.stringToDateLocal(thisBirthDay, NVDateFormat.LAST_UPDATED_TIME_FORMAT);
        Date today = new Date(System.currentTimeMillis());
        return ((currentYear - birthYear) >= Integer.parseInt(getStorage().getUserCountryEntity().getAge().getValue()) && userDOB.compareTo(today) < 1);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m017_profile_born;
    }

    @Override
    protected M017ProfileBornPresenter getPresenter() {
        return new M017ProfileBornPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        mCallBack.showFrgScreen(TAG, M016ProfileStartFrg.TAG);
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m017_born:
            case R.id.iv_m017_born:
                CommonUtil.getInstance().showDateChoosing(mContext, mTvDOB, getStorage().getM001ProfileEntity().getDateOfBirth(), this);
                break;
            case R.id.bt_m017_next:
                getStorage().getM001ProfileEntity().setYearBorn(mTvDOB.getText().toString());
                if (isMature()) {
                    getStorage().setMature(true);
                    mCallBack.showFrgScreen(TAG, M011SignUpDataFrg.TAG);
                } else {
                    getStorage().setMature(false);
                    mCallBack.showFrgScreen(TAG, M066SignUpBelowFrg.TAG);
                }
                break;
            case R.id.iv_m017_back:
                mCallBack.showFrgScreen(TAG, M006AccountSetUpCountryFrg.TAG);
                break;
            case R.id.tv_m017_cancel:
                getStorage().getM001ProfileEntity().setDateOfBirth("");
                mCallBack.registrationCancel(TAG);
                break;
            default:
                break;
        }
    }

    @Override
    public void dateSet() {
        mBtNext.setEnabled(true);
    }
}
