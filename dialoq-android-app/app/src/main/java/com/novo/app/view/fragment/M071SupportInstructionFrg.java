package com.novo.app.view.fragment;

import android.os.AsyncTask;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;
import com.github.barteksc.pdfviewer.listener.OnRenderListener;
import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M071SupportInstructionPresenter;
import com.novo.app.utils.CATask;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnM024DoseLogCallBack;
import com.novo.app.view.event.OnM071SupportInstructionCallBack;
import com.novo.app.view.widget.ProgressLoading;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;


public class M071SupportInstructionFrg extends BaseFragment<M071SupportInstructionPresenter
        , OnM024DoseLogCallBack> implements OnM071SupportInstructionCallBack
        , OnErrorListener, OnPageErrorListener, OnRenderListener, CATask.OnTaskCallBackToView {

    public static final String TAG = M071SupportInstructionFrg.class.getName();
    private PDFView mIvPDF;
    private CATask<M071SupportInstructionFrg, M071SupportInstructionFrg> mTaskDL;

    @Override
    protected void initViews() {
        findViewById(R.id.iv_m071_back, this);
        findViewById(R.id.tv_m071_title, CAApplication.getInstance().getBoldFont());
        mIvPDF = findViewById(R.id.iv_m071_pdf_view);
        initPDF();
        ProgressLoading.show(mContext);
    }

    private void initPDF() {
        String url = CommonUtil.getInstance().getPrefContent(CommonUtil.KEY_IFU_URL);
        String path = CommonUtil.getInstance().getPrefContent(CommonUtil.KEY_IFU_PATH);
        File file = new File(path);
        if (CommonUtil.getInstance().isConnectToNetwork()) {
            if (!file.exists()) {
                downloadPDF(url);
            } else {
                mIvPDF.fromFile(file).onRender(this).load();
            }
        } else {
            if (!file.exists()) {
                showNotify(R.string.txt_lost_network);
                return;
            } else {
                mIvPDF.fromFile(file).onRender(this).load();
            }
        }

    }

    private void downloadPDF(String url) {
        mTaskDL = new CATask<>(new WeakReference<>(this), this);
        mTaskDL.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.frg_m071_support_instruction;
    }

    @Override
    protected M071SupportInstructionPresenter getPresenter() {
        return new M071SupportInstructionPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        // This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        if (idView == R.id.iv_m071_back) mCallBack.showChildFrgScreen(TAG, getTagSource());
    }

    @Override
    public void onError(Throwable t) {
        ProgressLoading.dismiss();
        CommonUtil.getInstance().showDialog(mContext, LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON), null);
    }

    @Override
    public void onPageError(int page, Throwable t) {
        ProgressLoading.dismiss();
        CommonUtil.getInstance().showDialog(mContext, LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON), null);
    }

    @Override
    public void onInitiallyRendered(int nbPages) {
        ProgressLoading.dismiss();
    }

    @SuppressWarnings("squid:S4042")
    @Override
    public Object doBGTask(String key){
        FileOutputStream out = null;
        try {
            if (key == null) {
                return new Object();
            }
            String ex = key.substring(key.lastIndexOf(".") + 1);
            if (!ex.equalsIgnoreCase("PDF")) {
                return new Object();
            }
            URLConnection conn = new URL(key).openConnection();
            InputStream in = conn.getInputStream();
            int len;
            byte[] buff = new byte[1024];
            String folderName = CommonUtil.getInstance().getPrefContent(CommonUtil.LANG_KEY);
            String fileName = key.substring(key.lastIndexOf("/") + 1);
            String path = CAApplication.getInstance().getExternalFilesDir(null) + "/" + folderName +"/"+ fileName;
            CommonUtil.wtfi(TAG, "PATH: downloadPath: " + path);

            File file = new File(path);
            mTaskDL.updateUI(file);
            file.delete();
            file.createNewFile();
            out = new FileOutputStream(file);
            len = in.read(buff);

            while (len > 0) {
                out.write(buff, 0, len);
                len = in.read(buff);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new Object();
    }

    @Override
    public void doneTask(Object data) {
        boolean isDownloadDone = (boolean) data;
        if (isDownloadDone){
            String path = CommonUtil.getInstance().getPrefContent(CommonUtil.KEY_IFU_PATH);
            File file = new File(path);
            mIvPDF.fromFile(file).onRender(this).load();
        }
    }
}
