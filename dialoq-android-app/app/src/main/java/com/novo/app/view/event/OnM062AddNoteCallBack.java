package com.novo.app.view.event;

public interface OnM062AddNoteCallBack extends OnCallBackToView {
    void showDoseLog(String data);

    void showAddNoteFail();
}
