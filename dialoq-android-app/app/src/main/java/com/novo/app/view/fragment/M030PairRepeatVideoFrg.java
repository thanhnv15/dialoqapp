package com.novo.app.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M030PairRepeatVideoPresenter;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM030PairRepeatVideoCallBack;
import com.novo.app.view.widget.OnOKDialogAdapter;

public class M030PairRepeatVideoFrg extends BaseFragment<M030PairRepeatVideoPresenter, OnHomeBackToView> implements OnM030PairRepeatVideoCallBack {
    public static final String TAG = M030PairRepeatVideoFrg.class.getName();
    private static final int REQUEST_ENABLE_BT = 103;

    @Override
    protected void initViews() {
        TextView mTvDescription = findViewById(R.id.tv_m030_description, CAApplication.getInstance().getBoldFont());
        String highlight = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m030_switched_on));
        highLightText(mTvDescription, highlight, R.color.colorM001Cyan);

        findViewById(R.id.tv_m030_again, this, CAApplication.getInstance().getMediumFont());
        findViewById(R.id.bt_m030_next, this, CAApplication.getInstance().getMediumFont());

        getStorage().setM33ContinuePairing(false);
    }

    private void setBluetoothOn() {
        CommonUtil.getInstance().checkBluetoothOn(mContext);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_OK && CommonUtil.getInstance().isBluetoothOn()) {
            mCallBack.showFrgScreen(TAG, M032PairPushReleaseFrg.TAG);
        }
    }

    @Override
    public void showM001LandingScreen(String message) {
        CommonUtil.getInstance().backAndNotify(mContext, new OnOKDialogAdapter() {
            @Override
            public void handleOKButton2() {
                mCallBack.showFrgScreen(TAG, M001LandingPageFrg.TAG);
            }
        }, message);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m030_pair_repeat_video;
    }

    @Override
    protected M030PairRepeatVideoPresenter getPresenter() {
        return new M030PairRepeatVideoPresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        // This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        // This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.tv_m030_again:
                mCallBack.showFrgScreen(TAG, M029PairAttachFrg.TAG);
                break;
            case R.id.bt_m030_next:
                if (CommonUtil.getInstance().isBluetoothOn()) {
                    getStorage().getM032PairedDevice().clear();
                    mCallBack.showFrgScreen(TAG, M032PairPushReleaseFrg.TAG);
                } else {
                    setBluetoothOn();
                }
                break;
            default:
                break;
        }
    }
}
