package com.novo.app.view.fragment;

import android.widget.TextView;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.presenter.M023LoginPresenter;
import com.novo.app.presenter.M025SubDoseLogPresenter;
import com.novo.app.presenter.M070PairTroublePresenter;
import com.novo.app.view.base.BaseFragment;
import com.novo.app.view.event.OnHomeBackToView;
import com.novo.app.view.event.OnM023LoginCallBack;
import com.novo.app.view.event.OnM070PairTrouble;

import org.apache.commons.codec.language.bm.Lang;

import java.util.ArrayList;
import java.util.List;

public class M070PairTroubleFrg extends BaseFragment<M070PairTroublePresenter, OnHomeBackToView> implements OnM070PairTrouble {
    public static final String TAG = M070PairTroubleFrg.class.getName();
    private String contact;

    @Override
    protected void initViews() {
        getStorage().getM032PairedDevice().clear();
        findViewById(R.id.iv_m070_close, this);
        findViewById(R.id.tv_m070_title, CAApplication.getInstance().getBoldFont());
        findViewById(R.id.tv_m070_guide, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m070_suggest_1, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m070_suggest_2, CAApplication.getInstance().getRegularFont());
        findViewById(R.id.tv_m070_suggest_3, CAApplication.getInstance().getRegularFont());
        TextView mTvContactSupport = findViewById(R.id.tv_m070_suggest_4, this, CAApplication.getInstance().getRegularFont());
        contact = LangMgr.getInstance().getLangList().get(getString(R.string.lang_m070_contact_support));
        List<String> highlight = new ArrayList<>();
        highlight.add(contact);
        setClickableText(mTvContactSupport, highlight, R.color.colorM020Teal);

        findViewById(R.id.bt_m070_close, this, CAApplication.getInstance().getMediumFont());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m070_pair_trouble;
    }

    @Override
    protected M070PairTroublePresenter getPresenter() {
        return new M070PairTroublePresenter(this);
    }

    @Override
    protected String getTAG() {
        return TAG;
    }

    @Override
    protected void defineBackKey() {
        // This feature isn't require here
    }

    @Override
    public void backToPreviousScreen() {
        // This feature isn't require here
    }

    @Override
    protected void onClickView(int idView) {
        switch (idView) {
            case R.id.iv_m070_close:
                mCallBack.showFrgScreen(TAG, M024DoseLogFrg.TAG);
                break;
            case R.id.bt_m070_close:
                mCallBack.showFrgScreen(TAG, M032PairPushReleaseFrg.TAG);
                break;
            case R.id.tv_m070_suggest_4:
                onLinkClicked();
                break;
        }
    }

    @Override
    public void onLinkClicked() {
        if (getStorage().getLinkTag() != null && getStorage().getLinkTag().equals(contact)) {
            getStorage().setPreTAG(TAG);
            mCallBack.showFrgScreen(TAG, M024DoseLogFrg.TAG);
        }
        getStorage().setLinkTag(null);
    }
}
