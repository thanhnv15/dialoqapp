package com.novo.app.presenter;

import com.novo.app.view.event.OnM019SignUpCallBack;

public class M019SignUpPresenter extends BasePresenter<OnM019SignUpCallBack> {
    public M019SignUpPresenter(OnM019SignUpCallBack event) {
        super(event);
    }
}
