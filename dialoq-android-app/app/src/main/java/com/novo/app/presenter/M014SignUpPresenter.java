package com.novo.app.presenter;

import com.novo.app.CAApplication;
import com.novo.app.R;
import com.novo.app.manager.LangMgr;
import com.novo.app.model.entities.CarePlanTemplateEntity;
import com.novo.app.model.entities.PatientEntity;
import com.novo.app.model.entities.ProfileEntity;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.model.entities.TokenEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.service.CARequest;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM014SignUpCallBack;
import com.novo.app.view.widget.ProgressLoading;

import static com.novo.app.utils.CommonUtil.P_API_KEY;

public class M014SignUpPresenter extends BasePresenter<OnM014SignUpCallBack> {
    private static final String KEY_API_UN42_VERIFY_EMAIL = "KEY_API_UN42_VERIFY_EMAIL";
    private static final String URL_API_UN42_VERIFY_EMAIL = "user-service/api/v1/users/verify-email?";
    private static final String BODY_UN42 = "{\"email\":%s,\"emailVerificationToken\":%s,\"action\":\"validateToken\"}";

    private static final String KEY_API_IA02_AUTHENTICATE_USER = "KEY_API_IA02_AUTHENTICATE_USER";
    private static final String URL_API_IA02_AUTHENTICATE_USER = "user-service/api/v1/authenticate?";
    private static final String BODY_IA02 = "{\"userName\": %s,\"password\": %s,\"delivery\": \"SMS\"}";

    private static final String KEY_API_UN03_CREATE_PATIENT = "KEY_API_UN03_CREATE_PATIENT";
    private static final String URL_API_UN03_CREATE_PATIENT = "user-service/api/v1/users/patients?";
    private static final String BODY_UN03 = "{\"userName\": %s,\"passWord\": %s,\"firstName\": %s,\"middleName\": \"\",\"lastName\": %s,\"email\":%s,\"dateOfBirth\": %s,\"mobilePhoneNumber\": \"84978678205\"}";

    private static final String KEY_API_UN04_UPDATE_PATIENT = "KEY_API_UN04_UPDATE_PATIENT";
    private static final String URL_API_UN04_UPDATE_PATIENT = "user-service/api/v1/users/patients/%s?";
    private static final String BODY_UN04 = "{\"userName\": %s,\"passWord\":%s,\"firstName\":%s,\"lastName\":%s,\"email\":%s,\"gender\":\"male\",\"dateOfBirth\": %s,\"mobilePhoneNumber\": \"84978678205\",\"country\": %s, \"familyHistory\": %s}";

    private static final String KEY_API_CO14_GET_LATEST_TERM = "KEY_API_CO14_GET_LATEST_TERM";
    private static final String URL_API_CO14_GET_LATEST_TERM = "configuration-service/api/v1/configurations/term-conditions?";

    private static final String KEY_API_CP01_GET_ALL_CARE_PLAN = "KEY_API_CP01_GET_ALL_CARE_PLAN";
    private static final String URL_API_CP01_GET_ALL_CARE_PLAN = "care-plan-service/api/v1/care-plans?size=10000&sort=careplanTemplateId&";

    private static final String KEY_API_CP02_CREATE_CARE_PLAN = "KEY_API_CP02_CREATE_CARE_PLAN";
    private static final String URL_API_CP02_CREATE_CARE_PLAN = "care-plan-service/api/v1/users/%s/care-plans?";
    private static final String DIALOG_TEMPLATE = "Dialoq Diabetes";
    private static final String BODY_CP02 = "{\"careplanTemplateId\": %s}";
    private static final String P_CURRENT = "current";
    private static final String TAG = M014SignUpPresenter.class.getName();

    private static final String TOKEN = "token: ";


    public M014SignUpPresenter(OnM014SignUpCallBack event) {
        super(event);
    }

    @Override
    protected void handleSuccess(String data, String tag) {
        CommonUtil.wtfi(TAG, "DATA: " + data);
        switch (tag) {
            case KEY_API_UN42_VERIFY_EMAIL:
                prepareUN42(data);
                break;
            case KEY_API_UN03_CREATE_PATIENT:
                prepareUN03(data);
                break;
            case KEY_API_IA02_AUTHENTICATE_USER:
                prepareIA02(data);
                break;
            case KEY_API_UN04_UPDATE_PATIENT:
                prepareUN04(data);
                break;
            case KEY_API_CP01_GET_ALL_CARE_PLAN:
                prepareCP01(data);
                break;
            case KEY_API_CP02_CREATE_CARE_PLAN:
                prepareCP02(data);
                break;
            default:
                break;
        }
    }

    protected void prepareUN42(String data) {
        CommonUtil.wtfi(TAG, "DATA: " + data);

        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        if (responseEntity == null) {
            mListener.showM013SignUpCode(CAApplication.getInstance().getString(R.string.txt_token_err));
            return;
        }
        callUN03CreatePatient();
    }

    protected void prepareCP02(String data) {
        CommonUtil.wtfi(TAG, "prepareCP02..." + data);
        CarePlanTemplateEntity entity = generateData(CarePlanTemplateEntity.class, data);
        if (entity == null) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            mListener.showM023Login();
            return;
        }
        getStorage().getM001CarePlanTemplateEntity().setId(entity.getId());
        mListener.callAPISignUpSuccess();
    }

    protected void prepareCP01(String data) {
        CommonUtil.wtfi(TAG, "prepareCP01..." + data);
        CarePlanTemplateEntity entity = generateData(CarePlanTemplateEntity.class, data);
        if (entity == null) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            mListener.showM023Login();
            return;
        }
        getStorage().setM001CarePlanTemplateEntity(entity);
        callCP02CreateCarePlanForUser();
    }

    protected void prepareUN03(String data) {
        CommonUtil.wtfi(TAG, "prepareUN03..." + data);
        UserEntity entity = generateData(UserEntity.class, data);
        if (entity == null) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(ERR_500));
            return;
        }
        getStorage().getM001ProfileEntity().setUserId(entity.getUserId());
        getStorage().setM007UserEntity(entity);
        callIA02AuthenticateUser();
    }

    protected void prepareIA02(String data) {
        CommonUtil.wtfi(TAG, "prepareIA02..." + data);
        TokenEntity tokenEntity = generateData(TokenEntity.class, data);
        if (tokenEntity == null) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.getInstance().savePrefContent(CommonUtil.TOKEN_KEY, tokenEntity.getToken());
        callIUN04UpdatePatient();
    }

    protected void prepareUN04(String data) {
        CommonUtil.wtfi(TAG, "prepareUN04..." + data);
        PatientEntity patientEntity = generateData(PatientEntity.class, data);
        if (patientEntity == null) {
            showNotify(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            mListener.showM023Login();
            return;
        }
        getStorage().setM014PatientEntity(patientEntity);
        callCP01GetAllCarePlanTemplate();
    }

    protected void callCP02CreateCarePlanForUser() {
        CommonUtil.wtfi(TAG, "callCP02CreateCarePlanForUser");

        ProgressLoading.dontShow();
        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_CP02_CREATE_CARE_PLAN);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, TOKEN + token);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(String.format(URL_API_CP02_CREATE_CARE_PLAN, getStorage().getM001ProfileEntity().getUserId() + ""));

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY_CP02, getCarePlanTemplateId()));
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    protected String getCarePlanTemplateId() {
        if (getStorage().getM001CarePlanTemplateEntity() == null || getStorage().getM001CarePlanTemplateEntity().getData() == null
                || getStorage().getM001CarePlanTemplateEntity().getData().isEmpty()) return null;

        for (int i = 0; i < getStorage().getM001CarePlanTemplateEntity().getData().size(); i++) {
            CarePlanTemplateEntity.DataInfo careInfo = getStorage().getM001CarePlanTemplateEntity().getData().get(i);
            if (careInfo.getName().equals(DIALOG_TEMPLATE)) {
                return careInfo.getCareplanTemplateId() + "";
            }
        }
        return null;
    }

    protected void callCP01GetAllCarePlanTemplate() {
        CommonUtil.wtfi(TAG, "callCP01GetAllCarePlanTemplate");

        ProgressLoading.dontShow();
        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(KEY_API_CP01_GET_ALL_CARE_PLAN);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, TOKEN + token);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(URL_API_CP01_GET_ALL_CARE_PLAN);

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    void callICO14GetLatestTermAndCondition() {
        CommonUtil.wtfi(TAG, "callICO14GetLatestTermAndCondition");

        ProgressLoading.dontShow();
        CARequest request = new CARequest(TAG, CARequest.METHOD_GET);
        request.addTAG(KEY_API_CO14_GET_LATEST_TERM);
        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG, TOKEN + token);
        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);
        request.addPathSegment(URL_API_CO14_GET_LATEST_TERM);

        request.addQueryParams(P_CURRENT, "true");
        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    protected void callIUN04UpdatePatient() {
        CommonUtil.wtfi(TAG, "callIUN04UpdatePatient");

        ProgressLoading.dontShow();
        CARequest request = new CARequest(TAG, CARequest.METHOD_PUT);
        request.addTAG(KEY_API_UN04_UPDATE_PATIENT);

        String token = CommonUtil.getInstance().getPrefContent(CommonUtil.TOKEN_KEY);
        if (token == null || token.isEmpty()) {
            mListener.showM001LandingScreen(LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON));
            return;
        }
        CommonUtil.wtfi(TAG,  TOKEN + token);
        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addHeaders(CommonUtil.AUTHOR, token);

        ProfileEntity entity = getStorage().getM001ProfileEntity();
        request.addPathSegment(String.format(URL_API_UN04_UPDATE_PATIENT, entity.getUserId() + ""));
        String termEntity = entity.getTermVersion().getKey() + "=" + entity.getTermVersion().getValue();
        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY_UN04, entity.getEmail(), entity.getPassword(),
                entity.getFirstName(), entity.getLastName(), entity.getEmail(), entity.getDateOfBirth(),
                entity.getCountry().getKey(), termEntity));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    protected void callUN03CreatePatient() {
        CommonUtil.wtfi(TAG, "callUN03CreatePatient");

        ProgressLoading.dontShow();
        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_UN03_CREATE_PATIENT);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addPathSegment(URL_API_UN03_CREATE_PATIENT);

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());

        ProfileEntity entity = getStorage().getM001ProfileEntity();
        request.addBody(generateJson(BODY_UN03, entity.getEmail(), entity.getPassword(), entity.getFirstName(), entity.getFirstName(), entity.getEmail(), entity.getDateOfBirth()));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    protected void callIA02AuthenticateUser() {
        CommonUtil.wtfi(TAG, "callIA02AuthenticateUser");

        ProgressLoading.dontShow();
        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_IA02_AUTHENTICATE_USER);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addPathSegment(URL_API_IA02_AUTHENTICATE_USER);

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());

        ProfileEntity entity = getStorage().getM001ProfileEntity();
        request.addBody(generateJson(BODY_IA02, entity.getEmail(), entity.getPassword()));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    public void callUN42ValidateCode(String code) {
        CommonUtil.wtfi(TAG, "validateCode..." + code);

        ProgressLoading.dontShow();
        CARequest request = new CARequest(TAG, CARequest.METHOD_POST);
        request.addTAG(KEY_API_UN42_VERIFY_EMAIL);

        request.addHeaders(CommonUtil.CONTENT_TYPE, CommonUtil.VALUE_CONTENT_TYPE);
        request.addPathSegment(URL_API_UN42_VERIFY_EMAIL);

        request.addHeaders(P_API_KEY, CommonUtil.getInstance().getAPIKey());
        request.addBody(generateJson(BODY_UN42, getStorage().getM001ProfileEntity().getEmail(), code));

        //call request
        CAApplication.getInstance().callRequest(request, this);
    }

    @Override
    public void doFailed(String tag, Exception obj, int code, String data) {
        CommonUtil.wtfi(TAG, "DATA-ERR: " + data);
        CommonUtil.wtfi(TAG, "tagRequest: " + tagRequest);

        ResponseEntity responseEntity = generateData(ResponseEntity.class, data);
        String sms = LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON);
        if (responseEntity != null) {
            sms = LangMgr.getInstance().getLangList().get(CommonUtil.ERR_UNKNOWN_REASON);
        } else if (CommonUtil.getInstance().isConnectToNetwork()){
            showNotify(LangMgr.getInstance().getLangList().get(ERR_NO_CONNECTION));
        }

        switch (tag) {
            case KEY_API_UN42_VERIFY_EMAIL:
                mListener.showM013SignUpCode(sms);
                break;
            case KEY_API_UN03_CREATE_PATIENT:
            case KEY_API_IA02_AUTHENTICATE_USER:
            case KEY_API_UN04_UPDATE_PATIENT:
            case KEY_API_CO14_GET_LATEST_TERM:
                mListener.showM001LandingScreen(sms);
                break;
            case KEY_API_CP01_GET_ALL_CARE_PLAN:
            case KEY_API_CP02_CREATE_CARE_PLAN:
                mListener.showM001LandingScreen(sms);
                break;
            default:
                break;
        }
    }
}
