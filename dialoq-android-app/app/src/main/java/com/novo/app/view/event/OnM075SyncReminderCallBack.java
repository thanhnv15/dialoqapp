package com.novo.app.view.event;

public interface OnM075SyncReminderCallBack extends OnCallBackToView{
    void syncReminderSuccess();

    void syncFailed(String note);

    void syncReminderReady();

    void delReminderSuccess();
}
