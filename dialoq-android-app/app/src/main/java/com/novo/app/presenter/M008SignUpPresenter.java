package com.novo.app.presenter;

import com.novo.app.view.event.OnM008SignUpCallBack;

public class M008SignUpPresenter extends BasePresenter<OnM008SignUpCallBack> {
    public M008SignUpPresenter(OnM008SignUpCallBack event) {
        super(event);
    }
}
