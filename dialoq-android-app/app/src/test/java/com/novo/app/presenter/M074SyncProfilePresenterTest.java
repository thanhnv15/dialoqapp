package com.novo.app.presenter;

import com.novo.app.manager.CADBManager;
import com.novo.app.manager.entities.RUserEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM074SyncProfileCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;

public class M074SyncProfilePresenterTest extends BaseTest<M074SyncProfilePresenter> implements OnM074SyncProfileCallBack {

    /**This funtion is prepare data for unitTest*/
    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M074SyncProfilePresenter(this));
    }

    /**This funtion is prepare data for unitTest*/
    private void initData(boolean isInvalidToken) {
        Mockito.when(CADBManager.getInstance().getRUserProfile()).thenReturn(initRUserProfileEntity());
        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN7"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);

        if (!isInvalidToken) CommonUtil.getInstance().savePrefContent(CommonUtil.TOKEN_KEY, "MockToken");
    }

    /**This funtion is prepare data for unitTest*/
    private RUserEntity initRUserProfileEntity() {
        RUserEntity item = new RUserEntity();
        UserEntity entity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN7"));

        item.setUserId(entity.getUserId());
        item.setStatusFlag("");
        item.setCustom1(entity.getCustom1());
        item.setFirstName(entity.getFirstName());
        item.setLastName(entity.getLastName());
        item.setEmail(entity.getEmail());
        item.setGender(entity.getGender());
        item.setDateOfBirth(entity.getDateOfBirth());
        item.setMobilePhoneNumber(entity.getMobilePhoneNumber());
        item.setCountry(entity.getCountry());
        item.setMedicationHistory(entity.getMedicationHistory());
        item.setFamilyHistory(entity.getFamilyHistory());
        item.setPersonalHistory(entity.getPersonalHistory());
        item.setAllergies(entity.getAllergies());
        item.setRace(entity.getRace());
        item.setStreetAddress1(entity.getStreetAddress1());
        item.setStreetAddress2(entity.getStreetAddress2());

        return item;
    }

    /**This funtion is test case call UN04 update patient*/
    @Test
    public void callUN04UpdatePatient() {
        initData(false);
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN04")).setResponseCode(200));
        presenter.callUN04UpdatePatient();
    }


    /**This funtion is test case call UN07 get user login profile */
    @Test
    public void callUN07GetUserLoggedInProfile() {
        initData(false);
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN7")).setResponseCode(200));
        presenter.callUN07GetUserLoggedInProfile();
    }

    /**This funtion is test case call UN04 update patient failed*/
    @Test
    public void callUN04UpdatePatientFailed() {
        initData(false);
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN04_Fail")).setResponseCode(500));
        presenter.callUN04UpdatePatient();
    }


    /**This funtion is test case call UN07 get user login profile failed*/
    @Test
    public void callUN07GetUserLoggedInProfileFailed() {
        initData(false);
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN7_Faild")).setResponseCode(500));
        presenter.callUN07GetUserLoggedInProfile();
    }

    /**This funtion is test case call UN04 update patient no token*/
    @Test
    public void callUN04UpdatePatientNoToken() {
        initData(true);
        presenter.callUN04UpdatePatient();
    }

    /**This funtion is test case call UN07 get user login profile no token*/
    @Test
    public void callUN07GetUserLoggedInProfileNoToken() {
        initData(true);
        presenter.callUN07GetUserLoggedInProfile();
    }

    /** This is last point of success thread */
    @Override
    public void syncProfileSuccess() {
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void syncFailed(String sms) {
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void getProfileSuccess(UserEntity entity) {
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void getProfileFailed(String sms) {
        Assert.assertTrue(true);
    }
}