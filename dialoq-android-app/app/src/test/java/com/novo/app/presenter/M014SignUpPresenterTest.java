package com.novo.app.presenter;

import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM014SignUpCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;

public class M014SignUpPresenterTest extends BaseTest implements OnM014SignUpCallBack {
    private static final String TAG = M014SignUpPresenterTest.class.getName();
    private M014SignUpPresenter m014SignUpPresenter;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        m014SignUpPresenter = Mockito.spy(new M014SignUpPresenter(this));

        mApp.getStorageCommon().getM001ProfileEntity().setEmail("hue200@yopmail.com");
        mApp.getStorageCommon().getM001ProfileEntity().setFirstName("hue");
        mApp.getStorageCommon().getM001ProfileEntity().setPassword("Admin@12");
        mApp.getStorageCommon().getM001ProfileEntity().setCountry(new TextEntity("en", "English"));
        mApp.getStorageCommon().getM001ProfileEntity().setTermVersion(new TextEntity("DK.TERM.ADULT", "1"));

        CarePlantUserEntity carePlantUserEntity = CommonUtil.generateData(CarePlantUserEntity.class, getTextAssetFile("CP01"));
        mApp.getStorageCommon().setM001CarePlanUserEntity(carePlantUserEntity);

    }

    @Test
    public void callICO14GetLatestTermAndCondition() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CO14")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN14")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP01")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP02")).setResponseCode(200));
        m014SignUpPresenter.callICO14GetLatestTermAndCondition();
    }

    @Test
    public void callUN42ValidateCode() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN42")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN03")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN09")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN04")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CO14")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN14")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP01")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP02")).setResponseCode(200));

        m014SignUpPresenter.callUN42ValidateCode("445644");
    }

    @Test
    public void callCP02CreateCarePlanForUser() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("CP02")));
        m014SignUpPresenter.callCP02CreateCarePlanForUser();
    }

    @Test
    public void callCP01GetAllCarePlanTemplate() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("CP01")));
        m014SignUpPresenter.callCP01GetAllCarePlanTemplate();
    }

    @Test
    public void callIUN04UpdatePatient() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("UN04")));
        m014SignUpPresenter.callIUN04UpdatePatient();
    }

    @Test
    public void callUN03CreatePatient() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("UN03")));
        m014SignUpPresenter.callUN03CreatePatient();
    }

    @Test
    public void callIA02AuthenticateUser() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("IA02")));
        m014SignUpPresenter.callIA02AuthenticateUser();
    }

    @Test
    public void prepareUN42() {
        m014SignUpPresenter.prepareUN42(null);
        m014SignUpPresenter.prepareUN42(getTextAssetFile("UN42"));
        callUN03CreatePatient();
    }

    @Test
    public void prepareUN03() {
        m014SignUpPresenter.prepareUN03(null);
        m014SignUpPresenter.prepareUN03(getTextAssetFile("UN03"));
        callIA02AuthenticateUser();
    }

    @Test
    public void prepareIA02() {
        m014SignUpPresenter.prepareIA02(null);
        m014SignUpPresenter.prepareIA02(getTextAssetFile("IA02"));
        callIUN04UpdatePatient();
    }

    @Test
    public void prepareUN04() {
        m014SignUpPresenter.prepareUN04(null);
        m014SignUpPresenter.prepareUN04(getTextAssetFile("UN04"));
        callCP01GetAllCarePlanTemplate();
    }

    @Test
    public void prepareCP01() {
        m014SignUpPresenter.prepareCP01(null);
        m014SignUpPresenter.prepareCP01(getTextAssetFile("CP01"));
        callCP02CreateCarePlanForUser();
    }

    @Test
    public void prepareCP02() {
        m014SignUpPresenter.prepareCP02(null);
        m014SignUpPresenter.prepareCP02(getTextAssetFile("CP02"));
        callAPISignUpSuccess();
    }

    @Test
    public void handleSuccess() {
        m014SignUpPresenter.handleSuccess("","");
        prepareUN42();
    }

    @Test
    public void doFailed() {
        m014SignUpPresenter.doFailed(TAG, null, -1, "");
    }

    @Override
    public void showM023Login() {
        Assert.assertTrue(true);
    }

    @Override
    public void showM001LandingScreen(String message) {
        Assert.assertTrue(true);
    }

    @Override
    public void showM013SignUpCode(String sms) {
        Assert.assertTrue(true);
    }

    @Override
    public void callAPISignUpSuccess() {
        Assert.assertTrue(true);
        CommonUtil.wtfi(TAG, "callAPISignUpSuccess-Test DONE");
    }
}