package com.novo.app.presenter;

import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM053MoreCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class M053MorePresenterTest extends BaseTest<M053MorePresenter> implements OnM053MoreCallBack {
    private static final String TAG = M053MorePresenterTest.class.getName();

    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M053MorePresenter(this));
        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN04"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);
        mApp.getStorageCommon().setM039Verify("");
    }

    @Test
    public void editUserInDB() {
        presenter.editUserInDB(mApp.getStorageCommon().getM007UserEntity());
    }

    @Test
    public void handleSuccess() {
        presenter.handleSuccess("", "");
    }

    @Test
    public void doFailed() {
        presenter.doFailed(TAG, null, -1, "");
        showAlertDialog("");
    }

    @Override
    public void showAlertDialog(int txtErr) {
        Assert.assertTrue(true);
    }

    @Override
    public void showAlertDialog(String txtErr) {
        Assert.assertTrue(true);
    }

    @Override
    public void showLoginScreen() {
        Assert.assertTrue(true);
    }

    @Override
    public void showDialog() {
        Assert.assertTrue(true);
    }

    @Override
    public void updateSuccess() {
        Assert.assertTrue(true);
    }

    @Override
    public void updateFailed() {
        Assert.assertTrue(true);
    }
}