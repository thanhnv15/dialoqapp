package com.novo.app.presenter;

import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.ResultInfo;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM032PairPushReleaseCallBack;
import com.novo.app.view.widget.NVDateUtils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import okhttp3.mockwebserver.MockResponse;

public class M032PairPushReleasePresenterTest extends BaseTest<M032PairPushReleasePresenter> implements OnM032PairPushReleaseCallBack {
    public static final String TAG = M032PairPushReleasePresenterTest.class.getName();

    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M032PairPushReleasePresenter(this));

        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN04"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);

        CarePlantUserEntity carePlantUserEntity = CommonUtil.generateData(CarePlantUserEntity.class, getTextAssetFile("CP03"));
        mApp.getStorageCommon().setM001CarePlanUserEntity(carePlantUserEntity);

        ResultInfo resultInfo = CommonUtil.generateData(ResultInfo.class, getTextAssetFile("AS05"));
        mApp.getStorageCommon().setM062ResultInfoItem(resultInfo);

        String lastPairNative = CommonUtil.getDateNow(CommonUtil.DATE_STYLE);
        String lastPair = NVDateUtils.getGroupItemDateTitle(NVDateUtils.TODAY_AT, NVDateUtils.YESTERDAY_AT, CommonUtil.getDateNow(CommonUtil.DATE_STYLE));

        List<DeviceEntity> deviceList = new ArrayList<>();
        deviceList.add(new DeviceEntity("", "", "000133", DeviceEntity.CODE_TRESIBA_100, "", lastPair, lastPairNative));
        mApp.getStorageCommon().setM032PairedDevice(deviceList);


    }


    @Test
    public void callAS04CreateResultForAssessment() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS4")).setResponseCode(200));
        presenter.callAS04CreateResultForAssessment();
    }


    @Override
    public void showM001LandingScreen(String s) {
        Assert.assertTrue(true);
    }

    @Override
    public void pairCompleted() {
        Assert.assertTrue(true);
    }

    @Override
    public void resetStateBLE() {
        Assert.assertTrue(true);
    }

}