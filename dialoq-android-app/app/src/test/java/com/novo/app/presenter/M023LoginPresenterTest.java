package com.novo.app.presenter;

import com.novo.app.model.entities.AcceptDeviceEntity;
import com.novo.app.model.entities.BlacklistCountryEntity;
import com.novo.app.model.entities.ConfigEntity;
import com.novo.app.model.entities.CountryEntity;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM023LoginCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import okhttp3.mockwebserver.MockResponse;

public class M023LoginPresenterTest extends BaseTest implements OnM023LoginCallBack {
    private static final String TAG = M023LoginPresenterTest.class.getName();
    private static final String KEY_API_CP03_GET_CARE_PLAN_USER = "KEY_API_CP03_GET_CARE_PLAN_USER";
    private M023LoginPresenter m023LoginPresenter;

    /**
     * This function prepare data for unitTest
     */
    @Before
    public void setUp() throws Exception {
        m023LoginPresenter = Mockito.spy(new M023LoginPresenter(this));
        super.setUp();
        MockitoAnnotations.initMocks(this);

        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN04"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);

        ConfigEntity configEntity = CommonUtil.generateData(ConfigEntity.class, getTextAssetFile("ConfigSet"));
        mApp.getStorageCommon().setM001ConfigSet(configEntity);

        AcceptDeviceEntity acceptDeviceEntity = new AcceptDeviceEntity();
        acceptDeviceEntity.setCountries("vn=vietnam");
        mApp.getStorageCommon().getM001ConfigSet().setBlackList(acceptDeviceEntity);

        List<BlacklistCountryEntity> blacklistCountryEntity = new ArrayList<>();
        blacklistCountryEntity.add(0, new BlacklistCountryEntity(new TextEntity("cn", "China")));
        mApp.getStorageCommon().getM001ConfigSet().setBlacklistCountryEntity(blacklistCountryEntity);

        List<CountryEntity> countryEntity = new ArrayList<>();
        countryEntity.add(0, new CountryEntity(new TextEntity("code", "cn"),
                new TextEntity("name", "China"), new TextEntity("term.adult.key", "DK.TERM.ADULT"),
                new TextEntity("term.adult.version", "1"),
                new TextEntity("term.adult.updated", "2019-08-05"),
                new TextEntity("term.children.key", "DK.TERM.CHILDREN"),
                new TextEntity("term.children.version", "2"),
                new TextEntity("term.children.updated", "2019-08-05"),
                new TextEntity("age", "13"),
                new TextEntity("minimum.using.app.age", "10")));
        countryEntity.add(1, new CountryEntity(
                new TextEntity("code", "se"),
                new TextEntity("name", "Sweden"),
                new TextEntity("term.adult.key", "GENERAL.TERMS.AND.CONDITIONS"),
                new TextEntity("term.adult.version", "1"),
                new TextEntity("term.adult.updated", "2019-08-05"),
                new TextEntity("term.children.key", "GENERAL.TERMS.AND.CONDITIONS"),
                new TextEntity("term.children.version", "2"),
                new TextEntity("term.children.updated", "2019-08-05"),
                new TextEntity("age", "13"),
                new TextEntity("minimum.using.app.age", "10")));
        mApp.getStorageCommon().getM001ConfigSet().setCountryEntity(countryEntity);
        mApp.getStorageCommon().setUserCountryEntity(mApp.getStorageCommon().getM001ConfigSet().getCountryEntity().get(0));

    }

    /**
     * This function call IA02 AuthenticateUser
     */
    @Test
    public void callIA02AuthenticateUser() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN31")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN09")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP03")).setResponseCode(200));
        m023LoginPresenter.callIA02AuthenticateUser("dialoq103@yopmail.com", "Admin@12");
    }

    /**
     * This function call IA02 AuthenticateUser Error 401
     */
    @Test
    public void testIA02AuthenticateUserErr401() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02_401")).setResponseCode(401));
        m023LoginPresenter.callIA02AuthenticateUser("dialoq103@yopmail.com", "Admin@12");
    }

    /**
     * This function call UN03 Error 403
     */
    @Test
    public void callUN31Err403() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN31_403")).setResponseCode(403));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN09")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP03")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP01")).setResponseCode(200));

        m023LoginPresenter.callIA02AuthenticateUser("hue20@yopmail.com", "Admin@12");
    }

    /**
     * This function call UN03 Error 204
     */
    @Test
    public void testCP03Err204() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN31")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN09")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP03")).setResponseCode(204));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP01")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP02")).setResponseCode(200));

        m023LoginPresenter.callIA02AuthenticateUser("hue20@yopmail.com", "Admin@12");
    }

    /**
     * This function call CP01 Get all care plan template
     */
    @Test
    public void callCP01GetAllCarePlanTemplate() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("CP01")));
        m023LoginPresenter.callCP01GetAllCarePlanTemplete();
    }

    /**
     * This function call CP01 Get all care plan template case null
     */
    @Test
    public void callCP01GetAllCarePlanTemplateCaseNull() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("CP01")));
        CommonUtil.getInstance().savePrefContent(CommonUtil.TOKEN_KEY, null);
        m023LoginPresenter.callCP01GetAllCarePlanTemplete();
    }

    /**
     * This function call CP03 Get care plan of user
     */
    @Test
    public void callCP03GetCarePlanOfUser() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("CP03")));
        CommonUtil.getInstance().savePrefContent(CommonUtil.TOKEN_KEY, null);
        m023LoginPresenter.callCP03GetCarePlanOfUser();
    }

    /**
     * This function call CP02 Get care plan of user
     */
    @Test
    public void callCP02CreateCarePlanForUser() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("CP02")));
        m023LoginPresenter.callCP02CreateCarePlanForUser();
    }

    /**
     * This function call UN09 Get user logged in profile
     */
    @Test
    public void callIUN09GetUserLoggedInProfile() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("UN09")));
        CommonUtil.getInstance().savePrefContent(CommonUtil.TOKEN_KEY, null);
        m023LoginPresenter.callIUN09GetUserLoggedInProfile();
    }

    /**
     * This function call UN07 Get user logged in profile
     */
    @Test
    public void callIUN07GetUserLoggedInProfile() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("UN7")));
        m023LoginPresenter.callIUN07GetUserLoggedInProfile();
    }

    /**
     * This function call UN07 Get user logged in profile case null
     */
    @Test
    public void callIUN07GetUserLoggedInProfileCaseNull() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("UN7")));
        CommonUtil.getInstance().savePrefContent(CommonUtil.TOKEN_KEY, null);
        m023LoginPresenter.callIUN07GetUserLoggedInProfile();
    }

    /**
     * This function check blacklist country
     */
    @Test
    public void checkBlacklistCountry() {
        m023LoginPresenter.checkBlacklistCountry(false);
    }

    /**
     * This function prepare IA02
     */
    @Test
    public void prepareIA02() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("IA02")));
        m023LoginPresenter.prepareIA02(getTextAssetFile("IA02"));
        m023LoginPresenter.prepareIA02(null);
        callIUN09GetUserLoggedInProfile();
    }

    /**
     * This function call UN09
     */
    @Test
    public void prepareUN09() {
        m023LoginPresenter.prepareUN09(getTextAssetFile("UN09"));
        callIUN07GetUserLoggedInProfile();
    }

    /**
     * This function setup user country
     */
    @Test
    public void setupUserCountry() {
        m023LoginPresenter.setupUserCountry("se");
    }

    /**
     * This function check country version lock app
     */
    @Test
    public void checkCountryVersionLockApp() {
        m023LoginPresenter.checkCountryVersionLockApp(false);
    }

    /**
     * This function go to landing page
     */
    @Test
    public void gotoLandingPage() {
        m023LoginPresenter.gotoLandingPage(true);
    }

    /**
     * This function check for accepted term
     */
    @Test
    public void checkForAcceptedTerm() {
        m023LoginPresenter.checkForAcceptedTerm(false);
    }

    /**
     * This function prepare UN07
     */
    @Test
    public void prepareUN07() {
        m023LoginPresenter.prepareUN07(null);
        m023LoginPresenter.prepareUN07(getTextAssetFile("UN7"));
    }

    /**
     * This function prepare CP03
     */
    @Test
    public void prepareCP03() {
        m023LoginPresenter.prepareCP03(null);
        m023LoginPresenter.prepareCP03(getTextAssetFile("CP03"));
        loginSuccess();
    }

    /**
     * This function prepare CP01
     */
    @Test
    public void prepareCP01() {
        m023LoginPresenter.prepareCP01(null);
        m023LoginPresenter.prepareCP01(getTextAssetFile("CP01"));
        callCP02CreateCarePlanForUser();
    }

    /**
     * This function prepare CP02
     */
    @Test
    public void prepareCP02() {
        m023LoginPresenter.prepareCP02(null);
        m023LoginPresenter.prepareCP02(getTextAssetFile("CP02"));
        callCP03GetCarePlanOfUser();
    }

    /**
     * This function prepare CP01
     */
    @Test
    public void hideLockDialog() {
        m023LoginPresenter.hideLockDialog(KEY_API_CP03_GET_CARE_PLAN_USER);
    }

    /**
     * This is last point of success thread
     */
    @Override
    public void showDialog(int txt, String TAG) {
        Assert.assertTrue(true);
    }

    /**
     * This is last point of success thread
     */
    @Override
    public void showDialog(String txt, String TAG) {
        Assert.assertTrue(true);
    }

    /**
     * This is last point of success thread
     */
    @Override
    public void updateFingerPrintResult() {
        Assert.assertTrue(true);
    }

    /**
     * This is last point of success thread
     */
    @Override
    public void loginSuccess() {
        Assert.assertTrue(true);
        CommonUtil.wtfi(TAG, "loginSuccess-Test DONE");
    }

    /**
     * This is last point of success thread
     */
    @Override
    public void showM001LandingScreen(String sms) {
        Assert.assertTrue(true);
        CommonUtil.wtfi(TAG, "happend when doFail");
    }
}