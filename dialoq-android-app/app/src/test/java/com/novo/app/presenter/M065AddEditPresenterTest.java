package com.novo.app.presenter;

import com.novo.app.manager.CADBManager;
import com.novo.app.manager.entities.RReminderEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM065ReminderCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Locale;

public class M065AddEditPresenterTest extends BaseTest<M065AddEditReminderPresenter> implements OnM065ReminderCallBack {
    public static final String TAG = M065AddEditPresenterTest.class.getName();
    private String time;
    private RReminderEntity entity;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M065AddEditReminderPresenter(this));

        //Dum data
        time = String.format(Locale.getDefault(), "%02d:%02d", 8, 30);
        String startDate = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, 1);
        String endDate = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, 3650);

        entity = new RReminderEntity();
        entity.setScheduleId(System.currentTimeMillis() + "");
        entity.setScheduleName(System.currentTimeMillis() + "");
        entity.setFreqType("DAILY");
        entity.setFreqInterval("");
        entity.setFreqSubdayType("SPECIFIED_TIME");
        entity.setStartTime(time);
        entity.setStartDate(startDate);
        entity.setEndDate(endDate);
        entity.setScheduleNotificationId("");
        entity.setNotificationChannel(CommonUtil.REMINDER_ACTIVE);
        entity.setNotificationType("REMIND_TAKE_MEDICATION");
        entity.setReminderTime("300");
        entity.setStatusFlag(RReminderEntity.STATE_ADD);

        Mockito.when(CADBManager.getInstance().delReminderEntity()).thenReturn(true);
        Mockito.when(CADBManager.getInstance().editRReminderEntity(entity)).thenReturn(true);
        Mockito.when(CADBManager.getInstance().addRReminderEntity(entity)).thenReturn(true);
        Mockito.when(CADBManager.getInstance().getRReminderEntity()).thenReturn(entity);
    }

    @Override
    public void closeDialog(String key) {
        System.out.println("===>REACH DESTINATION");
        Assert.assertTrue(true);
    }

    @Test
    public void editReminder() {
        mApp.getStorageCommon().setM065ScheduleEntity(entity);
        Mockito.when(CADBManager.getInstance().getRReminderEntity()).thenReturn(entity);
        presenter.activeReminder(time);
    }

    @Test
    public void delReminder() {
        presenter.delReminder();
    }

    @Test
    public void createReminder() {
        presenter.addReminder(time);
    }

    @Override
    public void toM051More() {
        Assert.assertTrue(true);
    }
}