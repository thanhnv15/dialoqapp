package com.novo.app.presenter;

import com.novo.app.manager.entities.RInjectionEntity;
import com.novo.app.manager.entities.RInjectionItem;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM073SyncDoseCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import okhttp3.mockwebserver.MockResponse;

public class M073SyncDosePresenterTest extends BaseTest<M073SyncDosePresenter> implements OnM073SyncDoseCallBack {
    private static final String TOKEN = "Bearer AQIC5wM2LY4Sfcw58QDhX0a3yHlAdvXblEyVJvycgt3blBY.*AAJTSQACMDIAAlNLABQtMTU5MTEyMTUwNDc2OTkzNDY2OAACUzEAAjAx*";
    List<RInjectionEntity> mListDose = new ArrayList<>();

    /**This function is prepare data for unitTest*/
    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M073SyncDosePresenter(this, mListDose));
    }

    /**This function is test case syn data failed */
    @Test
    public void synDataCaseFailed() {
        RInjectionEntity entity = new RInjectionEntity();
        RealmList<RInjectionItem> injectionItems = new RealmList<>();
        RInjectionItem item = new RInjectionItem();
        item.setCustom3(DeviceEntity.CODE_TRESIBA_100 + "");
        item.setReportedDate("2019-05-09T08:10:05+0000");

        injectionItems.add(item);
        item.setDeviceId("");

        entity.setDevice(item);
        entity.setInjectionItems(injectionItems);

        CommonUtil.getInstance().savePrefContent(CommonUtil.TOKEN_KEY, TOKEN);
        CarePlantUserEntity carePlantUserEntity = CommonUtil.generateData(CarePlantUserEntity.class, getTextAssetFile("CP03"));
        mApp.getStorageCommon().setM001CarePlanUserEntity(carePlantUserEntity);

        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN7"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);

        mListDose.add(entity);
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS30")).setResponseCode(500));
        presenter.syncData();
    }

    /**This function is test case syn dose data no dose */
    @Test
    public void synDoseDataNoDose() {
        mListDose = new ArrayList<>();
        presenter.syncData();
    }

    /**This function is test case syn dose data no token */
    @Test
    public void synDoseDataNoToken() {
        CommonUtil.getInstance().savePrefContent(CommonUtil.TOKEN_KEY, null);
        presenter.syncData();
    }

    /**This function is test case syn dose data success */
    @Test
    public void syncDoseDataSuccess() {
        RInjectionEntity entity = new RInjectionEntity();
        RealmList<RInjectionItem> injectionItems = new RealmList<>();
        RInjectionItem item = new RInjectionItem();
        item.setCustom3(DeviceEntity.CODE_TRESIBA_100 + "");
        item.setReportedDate("2019-05-09T08:10:05+0000");

        injectionItems.add(item);
        item.setDeviceId("");

        entity.setDevice(item);
        entity.setInjectionItems(injectionItems);

        CommonUtil.getInstance().savePrefContent(CommonUtil.TOKEN_KEY, TOKEN);
        CarePlantUserEntity carePlantUserEntity = CommonUtil.generateData(CarePlantUserEntity.class, getTextAssetFile("CP03"));
        mApp.getStorageCommon().setM001CarePlanUserEntity(carePlantUserEntity);

        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN7"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);

        mListDose.add(entity);
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS30")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS6")).setResponseCode(200));
        presenter.syncData();
    }

    /** This is last point of success thread */
    @Override
    public void syncDoseSuccess(int index) {
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void syncFailed(String sms) {
        Assert.assertTrue(true);
    }


}