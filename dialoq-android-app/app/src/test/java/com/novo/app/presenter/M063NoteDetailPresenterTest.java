package com.novo.app.presenter;

import com.novo.app.manager.CADBManager;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.view.event.OnM063NoteDetailCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class M063NoteDetailPresenterTest extends BaseTest<M063NoteDetailPresenter> implements OnM063NoteDetailCallBack {
    public static final String TAG = M063NoteDetailPresenterTest.class.getName();

    /**This function is prepare data for unitTest*/
    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M063NoteDetailPresenter(this));
    }

    /**This function is test case delete note in database success*/
    @Test
    public void testDelNoteInDBCaseSuccess() {
        DataInfo dataInfo = Mockito.mock(DataInfo.class);
        Mockito.when(CADBManager.getInstance().deleteRNote(dataInfo)).thenReturn(true);
        presenter.delNoteInDB(dataInfo);
    }

    /**This function is test case delete note in database failed*/
    @Test
    public void testDelNoteInDBCaseFailed() {
        DataInfo dataInfo = Mockito.mock(DataInfo.class);
        Mockito.when(CADBManager.getInstance().deleteRNote(dataInfo)).thenReturn(false);
        presenter.delNoteInDB(dataInfo);
    }

    /** This is last point of success thread */
    @Override
    public void refreshDataM025() {
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void showDeleteDialog() {
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void showDeleteFailed() {
        System.out.println("Delete note: Success case: Passed");
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void showDeleteSuccess() {
        System.out.println("Delete note: Failed case: Passed");
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void editNoteFailed() {
        Assert.assertTrue(true);
    }
}