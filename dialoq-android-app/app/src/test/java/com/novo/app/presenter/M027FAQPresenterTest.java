package com.novo.app.presenter;

import com.novo.app.model.entities.ConfigEntity;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.model.entities.TopicEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM027FAQOnCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class M027FAQPresenterTest extends BaseTest implements OnM027FAQOnCallBack {
    private static final String TAG = M027FAQPresenterTest.class.getName();


    @Before
    public void setUp() throws Exception {
        super.setUp();

        ConfigEntity configEntity = CommonUtil.generateData(ConfigEntity.class, getTextAssetFile("ConfigSet"));
        mApp.getStorageCommon().setM001ConfigSet(configEntity);

        List<TopicEntity> listQnA = new ArrayList<>();
        List<String> listTopics = new ArrayList<>();
        List<TextEntity> listValue = new ArrayList<>();

        listTopics.add(0, "TOPIC1");
        listTopics.add(1, "TOPIC2");
        listTopics.add(2, "TOPIC3");

        listValue.add(0, new TextEntity("Question1", "Answer1"));
        listValue.add(1, new TextEntity("Question2", "Answer2"));
        listValue.add(2, new TextEntity("Question3", "Answer3"));

        listQnA.add(0, new TopicEntity(listTopics.get(0), listValue));
        listQnA.add(1, new TopicEntity(listTopics.get(1), listValue));
        listQnA.add(2, new TopicEntity(listTopics.get(2), listValue));

        mApp.getStorageCommon().setM001ConfigSet(new ConfigEntity());
        mApp.getStorageCommon().getM001ConfigSet().setListQnA(listQnA);
        presenter = Mockito.spy(new M027FAQPresenter(this));
    }

    @Override
    public void showM001LandingScreen(String err) {
        Assert.assertTrue(true);
    }
}