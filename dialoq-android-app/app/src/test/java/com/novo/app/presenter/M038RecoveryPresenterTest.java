package com.novo.app.presenter;

import com.novo.app.view.event.OnM038RecoveryCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;

public class M038RecoveryPresenterTest extends BaseTest<M038RecoveryPresenter> implements OnM038RecoveryCallBack {
    public static final String TAG = M038RecoveryPresenterTest.class.getName();

    /**
     * This function prepare data for unitTest
     */
    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M038RecoveryPresenter(this));
    }

    /**This function call IA04 reset password step1*/
    @Test
    public void callIA04ResetPasswordStep1() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA04")).setResponseCode(200));
        presenter.callIA04ResetPasswordStep1("dialoq103@yopmail.com");
    }

    /**This function call IA04 reset password step1 error400*/
    @Test
    public void callIA04ResetPasswordStep1Error400() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("")).setResponseCode(400));
        presenter.callIA04ResetPasswordStep1("dialoq103@yopmail.com");
    }

    /**This function call IA04 reset password step1 error401*/
    @Test
    public void callIA04ResetPasswordStep1Error401() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("")).setResponseCode(401));
        presenter.callIA04ResetPasswordStep1("dialoq103@yopmail.com");
    }

    /**This function call IA04 reset password step1 error403*/
    @Test
    public void callIA04ResetPasswordStep1Error403() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("")).setResponseCode(403));
        presenter.callIA04ResetPasswordStep1("dialoq103@yopmail.com");
    }

    /**
     * This is last point of success case
     */
    @Override
    public void showM039RecoveryCodeSent() {
        Assert.assertTrue(true);
    }

    /** This is last point of failed case */
    @Override
    public void onIA04Fail() {
        Assert.assertTrue(true);
    }
}