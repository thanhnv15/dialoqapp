package com.novo.app.presenter;

import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM058MoreCommunicationCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;

public class M058MoreCommunicationPresenterTest extends BaseTest implements OnM058MoreCommunicationCallBack {
    private static final String TAG = M058MoreCommunicationPresenterTest.class.getName();
    private M058MoreCommunicationPresenter m058MoreCommunicationPresenter;
    private static final String KEY_API_UN04_UPDATE_PATIENT = "KEY_API_UN04_UPDATE_PATIENT";

    @Before
    public void setUp() throws Exception {
        super.setUp();
        m058MoreCommunicationPresenter = Mockito.spy(new M058MoreCommunicationPresenter(this));
        UserEntity userEntity = CommonUtil.generateData(UserEntity.class,  getTextAssetFile("UN04"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);
    }

    @Test
    public void callIUN04UpdatePatient() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("UN04")));
        m058MoreCommunicationPresenter.callIUN04UpdatePatient();
    }

    @Test
    public void prepareUN04() {
        m058MoreCommunicationPresenter.prepareUN04(null);
        m058MoreCommunicationPresenter.prepareUN04(getTextAssetFile("UN04"));
    }

    @Test
    public void handleSuccess() {
        m058MoreCommunicationPresenter.handleSuccess("","");
    }

    @Test
    public void doFailed() {
        m058MoreCommunicationPresenter.doFailed(KEY_API_UN04_UPDATE_PATIENT, null, -1, null);
        m058MoreCommunicationPresenter.doFailed(KEY_API_UN04_UPDATE_PATIENT, null, -1, "");
    }

    @Override
    public void showAlertDialog(int txtErr) {
        Assert.assertTrue(true);
    }

    @Override
    public void showAlertDialog(String txtErr) {
        Assert.assertTrue(true);
    }

    @Override
    public void updateUserChoice() {
        Assert.assertTrue(true);
    }

    @Override
    public void showLoginScreen() {
        Assert.assertTrue(true);
    }

    @Override
    public void showDialog() {
        Assert.assertTrue(true);
    }

    @Override
    public void stateReverse() {
        Assert.assertTrue(true);
    }
}