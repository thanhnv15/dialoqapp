package com.novo.app.presenter;

import com.novo.app.view.event.OnM061MorePasswordCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class M061ChangePasswordPresenterTest extends BaseTest implements OnM061MorePasswordCallBack {
    private static final String TAG = M061ChangePasswordPresenterTest.class.getName();
    private M061MorePasswordPresenter m061MorePasswordPresenter;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        m061MorePasswordPresenter = Mockito.spy(new M061MorePasswordPresenter(this));
    }

    @Test
    public void callingChangePasswordAPI() {
        m061MorePasswordPresenter.callingChangePassword("dialoq103@yopmail.com", "Admin@12", "Admin@123");
    }

    @Test
    public void doFailed() {
        m061MorePasswordPresenter.doFailed(TAG, null, -1, "");
    }

    @Test
    public void callingAuthentication(){
        m061MorePasswordPresenter.callingAuthentication("dialoq103@yopmail.com","Admin@12");
    }
    @Test
    public void handleSuccess() {
        m061MorePasswordPresenter.handleSuccess("","");
    }

    @Override
    public void changePasswordSuccess() {
        Assert.assertTrue(true);
    }

    @Override
    public void showDialog() {
        Assert.assertTrue(true);

    }
}
