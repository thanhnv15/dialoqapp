package com.novo.app.presenter;

import com.novo.app.model.entities.TextEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM021SignUpCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;

public class M021SignUpPresenterTest extends BaseTest implements OnM021SignUpCallBack {
    private static final String TAG = M021SignUpPresenterTest.class.getName();
    private static final String KEY_API_UN04_UPDATE_PATIENT = "KEY_API_IA02_AUTHENTICATE_USER";
    private M021SignUpPresenter m021SignUpPresenter;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        m021SignUpPresenter = Mockito.spy(new M021SignUpPresenter(this));

        mApp.getStorageCommon().getM001ProfileEntity().setEmail("dialoq103@yopmail.com");
        mApp.getStorageCommon().getM001ProfileEntity().setFirstName("DA Demo");
        mApp.getStorageCommon().getM001ProfileEntity().setPassword("Admin@12");
        mApp.getStorageCommon().getM001ProfileEntity().setGender(new TextEntity("MALE", "male"));
        mApp.getStorageCommon().getM001ProfileEntity().setYearBorn("1900");
        mApp.getStorageCommon().getM001ProfileEntity().setYearDiabetes(new TextEntity("",""));
        mApp.getStorageCommon().getM001ProfileEntity().setAllErgies(1);
        mApp.getStorageCommon().getM001ProfileEntity().setRace(1);
        mApp.getStorageCommon().getM001ProfileEntity().setTypeOfDiabetes(new TextEntity("type1","Type 1"));
        mApp.getStorageCommon().getM001ProfileEntity().setCountry(new TextEntity("it", "Italy"));
        mApp.getStorageCommon().getM001ProfileEntity().setTermVersion(new TextEntity("DK.TERM.ADULT", "1"));

        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN04"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);
    }

    @Test
    public void handleSuccess() {
        m021SignUpPresenter.handleSuccess("","");
    }

    @Test
    public void prepareUN04() {
        m021SignUpPresenter.prepareUN04(null);
        m021SignUpPresenter.prepareUN04(getTextAssetFile("UN04"));
    }

    @Test
    public void doFailed() {
        m021SignUpPresenter.doFailed(KEY_API_UN04_UPDATE_PATIENT, null, -1, "");
    }

    @Test
    public void callUN04Update() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN04")).setResponseCode(200));
        m021SignUpPresenter.callUN04Update();
    }

    @Test
    public void CallUN04UpdateFail() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN04_Fail")).setResponseCode(400));
        m021SignUpPresenter.callUN04Update();
    }

    public void showM023Login() {
        Assert.assertTrue(true);
    }

    @Override
    public void showM022SignUpSuccess() {
        Assert.assertTrue(true);
        CommonUtil.wtfi(TAG, "showM022SignUpSuccess-Test DONE");
    }

    @Override
    public void showAlertDialog(int txtErr) {
        Assert.assertTrue(true);
    }

    @Override
    public void showAlertDialog(String txtErr) {
        Assert.assertTrue(true);
    }

}