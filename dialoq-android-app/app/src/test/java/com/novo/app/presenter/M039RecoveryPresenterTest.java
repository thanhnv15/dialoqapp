package com.novo.app.presenter;
import com.novo.app.view.event.OnM039RecoveryCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;

public class M039RecoveryPresenterTest extends BaseTest<M039RecoveryPresenter> implements OnM039RecoveryCallBack {
    private static final String TAG = M039RecoveryPresenterTest.class.getName();

    /**This function prepare data for unitTest*/
    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M039RecoveryPresenter(this));
    }

    /**This function call IA04 reset password step2 case success*/
    @Test
    public void call04ResetPasswordStep2CaseSuccess(){
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02")).setResponseCode(200));
        presenter.callIA04ResetPasswordStep2("","");
    }

    /**This function call IA04 reset password step2 case false 400*/
    @Test
    public void call04ResetPasswordStep2CaseFalse400(){
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA04_400")).setResponseCode(400));
        presenter.callIA04ResetPasswordStep2("","");
    }

    /**This function call IA04 reset password step2 case false 401*/
    @Test
    public void call04ResetPasswordStep2CaseFalse401(){
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA04_400")).setResponseCode(401));
        presenter.callIA04ResetPasswordStep2("","");
    }

    /**This function call IA04 reset password step2 case false 403*/
    @Test
    public void call04ResetPasswordStep2CaseFalse403(){
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA04_400")).setResponseCode(403));
        presenter.callIA04ResetPasswordStep2("","");
    }

    /**
     * This is last point of success case
     */
    @Override
    public void showM040RecoveryPasswordScreen() {
        Assert.assertTrue(true);
    }

    /** This is last point of fail cases */
    @Override
    public void onIA04Fail(String message) {
        Assert.assertTrue(true);
    }
}