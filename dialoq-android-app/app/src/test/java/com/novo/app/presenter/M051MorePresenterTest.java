package com.novo.app.presenter;

import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM051MoreCallBack;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;

import static org.junit.Assert.assertTrue;

public class M051MorePresenterTest extends BaseTest<M051MorePresenter> implements OnM051MoreCallBack {
    private static final String KEY_API_IA03_LOG_OUT = "KEY_API_IA03_LOG_OUT";
    private static final String KEY_API_AS29_GET_LIST_SCHEDULES_OF_ASSESSMENT = "KEY_API_AS29_GET_LIST_SCHEDULES_OF_ASSESSMENT";

    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M051MorePresenter(this));

        CarePlantUserEntity carePlantUserEntity = CommonUtil.generateData(CarePlantUserEntity.class, getTextAssetFile("CP03"));
        mApp.getStorageCommon().setM001CarePlanUserEntity(carePlantUserEntity);
    }

    @Test
    public void handleSuccess() {
        presenter.handleSuccess(null,"");
        presenter.handleSuccess(getTextAssetFile("AS29"),"");
    }

    @Test
    public void doFailed() {
        presenter = new M051MorePresenter(this);
        presenter.doFailed(KEY_API_IA03_LOG_OUT, null, -1, "");
        presenter.doFailed(KEY_API_AS29_GET_LIST_SCHEDULES_OF_ASSESSMENT, null, CommonUtil.CODE_204, "");
    }

    @Test
    public void callIA03LogOut() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS02")));
        presenter.callIA03LogOut();
    }

    @Test
    public void callIA03LogOutError401() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS02_Failed")).setResponseCode(401));
        presenter.callIA03LogOut();
    }

    @Test
    public void callAS029GetListSchedulesOfAssessment() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS29")).setResponseCode(200));
        presenter.callAS029GetListSchedulesOfAssessment();
    }

    @Test
    public void callAS029GetListSchedulesOfAssessmentError204() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS02_Failed")).setResponseCode(204));
        presenter.callAS029GetListSchedulesOfAssessment();
    }

    @Override
    public void showAlertDialog(int txtErr) {
        assertTrue(true);
    }

    @Override
    public void showAlertDialog(String txtErr) {
        assertTrue(true);
    }

    @Override
    public void showM023Login() {
        assertTrue(true);
    }

    @Override
    public void showReminder() {
        assertTrue(true);
    }

    @Override
    public void showEmptyReminder() {
        assertTrue(true);
    }
}