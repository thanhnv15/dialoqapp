package com.novo.app.presenter;

import com.novo.app.model.entities.ConfigEntity;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.model.entities.TopicEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM048SupportVideoCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class M048SupportVideoPresenterTest extends BaseTest implements OnM048SupportVideoCallBack {
    public static final String TAG = M048SupportVideoPresenterTest.class.getName();
    private M048SupportVideoPresenter m048SupportVideoPresenter;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        m048SupportVideoPresenter = Mockito.spy(new M048SupportVideoPresenter(this));
        ConfigEntity configEntity = CommonUtil.generateData(ConfigEntity.class, getTextAssetFile("ConfigSet"));
        mApp.getStorageCommon().setM001ConfigSet(configEntity);

        List<TopicEntity> videoData = new ArrayList<>();
        List<String> listTopic = new ArrayList<>();
        List<TextEntity> listVideo = new ArrayList<>();

        listTopic.add(0, "SUPPORT.VIDEOS.GROUP1");
        listTopic.add(1, "SUPPORT.VIDEOS.GROUP2");

        listVideo.add(0, new TextEntity("VIDEOS1.NAME", "VIDEOS1.URL"));
        listVideo.add(1, new TextEntity("VIDEOS2.NAME", "VIDEOS2.URL"));

        videoData.add(0, new TopicEntity(listTopic.get(0), listVideo));
        videoData.add(1, new TopicEntity(listTopic.get(1), listVideo));

        mApp.getStorageCommon().setM001ConfigSet(new ConfigEntity());
        mApp.getStorageCommon().getM001ConfigSet().setListVideos(videoData);
    }

    @Test
    public void getData() {
        List<TextEntity> result = m048SupportVideoPresenter.getData();
        Assert.assertTrue(result.size() > 0);
        CommonUtil.wtfi(TAG, "getData-Test DONE");
    }

    @Override
    public void toVideoPlayer(String url) {
        Assert.assertTrue(true);
    }
}