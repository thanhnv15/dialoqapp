package com.novo.app.presenter;

import android.util.Log;

import com.novo.app.manager.entities.RDoseEntity;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM072SyncNoteCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import okhttp3.mockwebserver.MockResponse;

public class M072SyncNotePresenterTest extends BaseTest<M072SyncNotePresenter> implements OnM072SyncNoteCallBack {
    private static final String TAG = M072SyncNotePresenterTest.class.getName();
    private static final String TOKEN = "Bearer AQIC5wM2LY4Sfcw58QDhX0a3yHlAdvXblEyVJvycgt3blBY.*AAJTSQACMDIAAlNLABQtMTU5MTEyMTUwNDc2OTkzNDY2OAACUzEAAjAx*";
    private static final String ASSESSMENT_TYPE_NOTE = "Notes";
    private transient List<RDoseEntity> mListNote = new ArrayList<>();

    /**This function prepare data for unitTest*/
    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M072SyncNotePresenter(this, mListNote));
    }

    /**This function prepare data for unitTest*/
    private void initData(boolean isValidCarePlan) {
        CommonUtil.getInstance().savePrefContent(CommonUtil.TOKEN_KEY, TOKEN);
        if (isValidCarePlan) {
            CarePlantUserEntity carePlantUserEntity = CommonUtil.generateData(CarePlantUserEntity.class, getTextAssetFile("CP03"));
            mApp.getStorageCommon().setM001CarePlanUserEntity(carePlantUserEntity);
        }
    }

    /**This function prepare data for unitTest*/
    private List<RDoseEntity> prepareListNote() {
        List<RDoseEntity> listNote = new ArrayList<>();

        RDoseEntity newNote = new RDoseEntity();
        newNote.setAssessmentType(ASSESSMENT_TYPE_NOTE);
        newNote.setNotes("new note");
        newNote.setStatusFlag(RDoseEntity.STATE_ADD);
        newNote.setReportedDate("2019-05-09T06:10:05+0000");
        newNote.setCustom1(System.currentTimeMillis() + "");
        newNote.setCustom2(System.currentTimeMillis() + "");
        listNote.add(newNote);

        RDoseEntity editedNote = new RDoseEntity();
        editedNote.setAssessmentType(ASSESSMENT_TYPE_NOTE);
        editedNote.setNotes("edited note");
        editedNote.setStatusFlag(RDoseEntity.STATE_EDIT);
        editedNote.setReportedDate("2019-05-10T05:10:05+0000");
        editedNote.setCustom1(System.currentTimeMillis() + "");
        editedNote.setCustom2(System.currentTimeMillis() + "");
        listNote.add(editedNote);

        RDoseEntity deleteNote = new RDoseEntity();
        deleteNote.setAssessmentType(ASSESSMENT_TYPE_NOTE);
        deleteNote.setNotes("note to delete");
        deleteNote.setStatusFlag(RDoseEntity.STATE_DEL);
        deleteNote.setReportedDate("2019-05-09T08:10:05+0000");
        deleteNote.setCustom1(System.currentTimeMillis() + "");
        deleteNote.setCustom2(System.currentTimeMillis() + "");
        listNote.add(deleteNote);

        return listNote;
    }

    /**This function test case during sync*/
    @Test
    public void failDuringSync() {
        initData(true);
        mListNote.addAll(prepareListNote());
        mApp.getStorageCommon().setM007UserEntity(CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN7")));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS30")).setResponseCode(500));
        presenter.upADDNoteData();
    }

    /**This function test case add note data*/
    @Test
    public void upADDNoteData() {
        initData(true);
        mListNote.addAll(prepareListNote());
        mApp.getStorageCommon().setM007UserEntity(CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN7")));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS30")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS6")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS23")).setResponseCode(200));
        presenter.upADDNoteData();
    }

    /**This function test case add no note*/
    @Test
    public void upADDNoteDataNoNote() {
        initData(true);
        presenter.upADDNoteData();
        Log.i(presenter.getClass().getName(), "mockWebServer: End");
    }

    /**This function test case add note data empty assessmentId */
    @Test
    public void upADDNoteDataEmptyAssessmentId() {
        initData(false);
        presenter.upADDNoteData();
    }

    /**This function test case add note data null token */
    @Test
    public void upADDNoteDataNullToken() {
        CommonUtil.getInstance().savePrefContent(CommonUtil.TOKEN_KEY, null);
        presenter.upADDNoteData();
    }

    /** This is last point of success thread */
    @Override
    public void syncNoteSuccess(int sumAdd, int sumEdit, int sumDel) {
        System.out.println("Success cases: Done");
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void syncFailed(String sms) {
        System.out.println("Failed cases: Done");
        Assert.assertTrue(true);
    }
}