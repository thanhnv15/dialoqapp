package com.novo.app.presenter;

import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM052MoreCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;

public class M052MorePresenterTest extends BaseTest implements OnM052MoreCallBack {
    public static final String TAG = M052MorePresenterTest.class.getName();
    private M052MorePresenter m052MorePresenter;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        m052MorePresenter = Mockito.spy(new M052MorePresenter(this));

        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN04"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);
    }

    @Test
    public void handleSuccess() {
        m052MorePresenter.handleSuccess(getTextAssetFile("UN04"),"");
    }

    @Test
    public void doFailed() {
        m052MorePresenter.doFailed(TAG, null, 0, null);
        m052MorePresenter.doFailed(TAG, null, 0, "");
    }

    @Test
    public void callUN34DeleteUserAccount() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN34")).setResponseCode(200));
        m052MorePresenter.callDA01DeleteUserAccount();
    }

    @Test
    public void callIUN04UpdatePatient() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("UN04")));
        m052MorePresenter.callIUN04UpdatePatient();
    }

    @Override
    public void toLandingScreen() {
        Assert.assertTrue(true);
    }

    @Override
    public void showLoginScreen() {
        Assert.assertTrue(true);
    }
}