package com.novo.app.presenter;

import com.novo.app.manager.entities.RReminderEntity;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM075SyncReminderCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Locale;

import okhttp3.mockwebserver.MockResponse;

public class M075SyncReminderPresenterTest extends BaseTest<M075SyncReminderPresenter> implements OnM075SyncReminderCallBack {
    private transient RReminderEntity mEntity;

    /**This function is prepare data for unitTest*/
    @Before
    public void setUp() throws Exception {
        super.setUp();

        CarePlantUserEntity carePlantUserEntity = CommonUtil.generateData(CarePlantUserEntity.class, getTextAssetFile("CP03"));
        mApp.getStorageCommon().setM001CarePlanUserEntity(carePlantUserEntity);

        mApp.getStorageCommon().setM065ScheduleEntity(initReminderEntity());
        presenter = Mockito.spy(new M075SyncReminderPresenter(this, mEntity));
    }

    /**This function is prepare data for unitTest*/
    private void initUserEntity() {
        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN7"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);
    }

    /**This function is prepare data for unitTest*/
    private RReminderEntity initReminderEntity() {
        String time = String.format(Locale.getDefault(), "%02d:%02d", 8, 30);
        String startDate = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, 1);
        String endDate = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, 3650);

        RReminderEntity reminder = new RReminderEntity();
        reminder.setScheduleId(System.currentTimeMillis() + "");
        reminder.setScheduleName(System.currentTimeMillis() + "");
        reminder.setFreqType("DAILY");
        reminder.setFreqInterval("");
        reminder.setFreqSubdayType("SPECIFIED_TIME");
        reminder.setStartTime(time);
        reminder.setStartDate(startDate);
        reminder.setEndDate(endDate);
        reminder.setScheduleNotificationId("");
        reminder.setNotificationChannel(CommonUtil.REMINDER_ACTIVE);
        reminder.setNotificationType("REMIND_TAKE_MEDICATION");
        reminder.setReminderTime("300");
        reminder.setStatusFlag(RReminderEntity.STATE_EDIT);
        return reminder;
    }

    /**This function is test case call AS09 delete assessment schedule success */
    @Test
    public void callAS09DeleteAssessmentScheduleSuccess() {
        initUserEntity();
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS09")).setResponseCode(200));
//        mApp.getStorageCommon().setM065ScheduleEntity(initReminderEntity());
        presenter.callAS09DeleteAssessmentSchedule(initReminderEntity());
    }

    /**This function is test case call AS29 get list schedules of assessment success */
    @Test
    public void callAS029GetListSchedulesOfAssessmentSuccess() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS29")).setResponseCode(200));
        presenter.callAS029GetListSchedulesOfAssessment();
    }

    /**This function is test case call AS29 get list schedules of assessment no token */
    @Test
    public void callAS029GetListSchedulesOfAssessmentNoToken() {
        CommonUtil.getInstance().savePrefContent(mApp, CommonUtil.TOKEN_KEY, null);
        presenter.callAS029GetListSchedulesOfAssessment();
    }

    /**This function is test case call AS29 get list schedules of assessment null response */
    @Test
    public void callAS029GetListSchedulesOfAssessmentNullResponse() {
        mockWebServer.enqueue(new MockResponse().setBody("").setResponseCode(200));
        presenter.callAS029GetListSchedulesOfAssessment();
    }

    /**This function is test case call AS29 get list schedules of assessment failed */
    @Test
    public void callAS029GetListSchedulesOfAssessmentFailed() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS29")).setResponseCode(500));
        presenter.callAS029GetListSchedulesOfAssessment();
    }

    /**This function is test case call AS29 get list schedules of assessment no reminder */
    @Test
    public void callAS029GetListSchedulesOfAssessmentNoReminder() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(204));
        presenter.callAS029GetListSchedulesOfAssessment();
    }

    /**This function is test case call AS10 get list schedules success */
    @Test
    public void callAS10UpdateAssessmentScheduleSuccess() {
        initUserEntity();
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS10")).setResponseCode(200));
        presenter.callAS10UpdateAssessmentSchedule(initReminderEntity());
    }

    /**This function is test case call AS10 get list schedules no token */
    @Test
    public void callAS10UpdateAssessmentScheduleNoToken() {
        initUserEntity();
        CommonUtil.getInstance().savePrefContent(mApp, CommonUtil.TOKEN_KEY, null);
        presenter.callAS10UpdateAssessmentSchedule(initReminderEntity());
    }

    /**This function is test case call AS10 get list schedules null user */
    @Test
    public void callAS10UpdateAssessmentScheduleNullUser() {
        presenter.callAS10UpdateAssessmentSchedule(initReminderEntity());
    }


    /**This function is test case call AS02 get list schedules of assessment success */
    @Test
    public void callAS02CreateAssessmentScheduleSuccess() {
        initUserEntity();
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS02")).setResponseCode(200));
        presenter.callAS02CreateAssessmentSchedule(initReminderEntity());
    }

    /**This function is test case call AS02 get list schedules no token */
    @Test
    public void callAS02CreateAssessmentScheduleNoToken() {
        initUserEntity();
        CommonUtil.getInstance().savePrefContent(mApp, CommonUtil.TOKEN_KEY, null);
        presenter.callAS02CreateAssessmentSchedule(initReminderEntity());
    }

    /**This function is test case call AS02 get list schedules null user */
    @Test
    public void callAS02CreateAssessmentScheduleNullUser() {
        presenter.callAS02CreateAssessmentSchedule(initReminderEntity());
    }

    /**This function is test case call AS09 delete assessment schedule no token  */
    @Test
    public void callAS09DeleteAssessmentScheduleNoToken() {
        initUserEntity();
        CommonUtil.getInstance().savePrefContent(mApp, CommonUtil.TOKEN_KEY, null);
        presenter.callAS09DeleteAssessmentSchedule(initReminderEntity());
    }

    /**This function is test case call AS09 delete assessment schedule no token  */
    @Test
    public void callAS09DeleteAssessmentScheduleNoData() {
        initUserEntity();
        CommonUtil.getInstance().savePrefContent(mApp, CommonUtil.TOKEN_KEY, null);
        presenter.callAS09DeleteAssessmentSchedule(null);
    }

    /** This is last point of success thread */
    @Override
    public void syncReminderSuccess() {
        System.out.println("Sync reminder: Success cases: Passed");
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void syncFailed(String note) {
        System.out.println("Sync reminder: Failed cases: Passed");
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void syncReminderReady() {
        System.out.println("Sync get reminder: Success cases: Passed");
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void delReminderSuccess() {
        System.out.println("Sync delete reminder: Success cases: Passed");
        Assert.assertTrue(true);
    }
}