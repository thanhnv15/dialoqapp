package com.novo.app.presenter;

import com.novo.app.view.event.OnM060MoreLanguageCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class M060MoreLanguagePresenterTest extends BaseTest implements OnM060MoreLanguageCallBack {
    private static final String TAG = M059MoreProductPresenterTest.class.getName();
    private M060MoreLanguagePresenter m060MoreLanguagePresenter;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        m060MoreLanguagePresenter = Mockito.spy(new M060MoreLanguagePresenter(this));
    }

    @Test
    public void handleSuccess() {
        m060MoreLanguagePresenter.handleSuccess("","");
    }

    @Test
    public void doFailed() {
        m060MoreLanguagePresenter.doFailed(TAG, null, -1, "");
    }
    @Test
    public void filterLanguage() {
        //m060MoreLanguagePresenter.filterLanguage("dialog_en");
    }

    @Override
    public void dataLanguageReady(String data) {
        Assert.assertTrue(true);
    }
}