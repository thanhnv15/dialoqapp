package com.novo.app.presenter;

import com.novo.app.manager.CADBManager;
import com.novo.app.manager.entities.RDecryptionKeyEntity;
import com.novo.app.manager.entities.RDoseEntity;
import com.novo.app.manager.entities.RInjectionEntity;
import com.novo.app.manager.entities.RInjectionItem;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.ResultInfo;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.service.SyncScheduleService;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM025SubDoseLogCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import okhttp3.mockwebserver.MockResponse;

public class M025SubDoseLogPresenterTest extends BaseTest<M025SubDoseLogPresenter> implements OnM025SubDoseLogCallBack {
    private static final String TAG = M025SubDoseLogPresenterTest.class.getName();

    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M025SubDoseLogPresenter(this));

        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN04"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);

        CarePlantUserEntity carePlantUserEntity = CommonUtil.generateData(CarePlantUserEntity.class, getTextAssetFile("CP03"));
        mApp.getStorageCommon().setM001CarePlanUserEntity(carePlantUserEntity);
    }

    private void mockDataLoading() {
        List<RDoseEntity> mListData = new ArrayList<>();
        RDoseEntity item = new RDoseEntity();
        item.setActivityId("7c3342ae-2c0e-4418-b342-ae2c0e341855");
        item.setActivityDate("2020-01-07T04:15:01+0000");
        item.setActivityStatus("Taken");
        item.setAdherenceStatus("Adhered");
        item.setResultId("2449581a-b1a9-4cce-8958-1ab1a98cce77");
        item.setAssessmentType("Injections");
        item.setResult(10);
        item.setReportedDate("2020-01-07T02:00:24+0000");
        item.setNotes("3");
        item.setCustom1("Dialoq TESTAM");
        item.setCustom2("1578370469491");
        item.setCustom3("3");
        item.setCustom4("2048");

        mListData.add(item);

        List<RDoseEntity> mListData2 = new ArrayList<>();
        RDoseEntity item2 = new RDoseEntity();
        item2.setActivityId("543342ae-2c0e-4418-b342-ae2c0e341855");
        item2.setActivityDate("2020-01-01T08:20:01+0000");
        item2.setActivityStatus("Taken");
        item2.setAdherenceStatus("Adhered");
        item2.setResultId("1449581a-b1a9-4aaa-8958-1ab1a98cce77");
        item2.setAssessmentType("Devices");
        item2.setAssessmentId("1245");
        item2.setResult(10);
        item2.setReportedDate("2020-01-07T02:00:24+0000");
        item2.setNotes("11");
        item2.setCustom1("Dialoq TESTAM");
        item2.setCustom2("Dialoq TESTAM");

        mListData2.add(item2);
        List<RDecryptionKeyEntity> mListNNDM = new ArrayList<>();

        RDecryptionKeyEntity itemNNDM = new RDecryptionKeyEntity();
        itemNNDM.setSystemId("001465004000014E");
        itemNNDM.setDecryptionKey("78BA820A8FB62C3FACBCC6B2937293DE");
        mListNNDM.add(itemNNDM);

        List<RDoseEntity> mListNote = new ArrayList<>();
        RDoseEntity note = new RDoseEntity();
        note.setActivityId("543342asae-2c0e-4418-b342-ae2c0e341855");
        note.setActivityDate("2020-01-01T08:20:01+0000");
        note.setActivityStatus("Taken");
        note.setAdherenceStatus("Adhered");
        note.setResultId("1449581a-b1a9-4aaa-8958-1ab1a98cce77");
        note.setAssessmentType("Notes");
        note.setAssessmentId("1245");
        note.setResult(12);
        note.setReportedDate("2020-01-07T02:00:24+0000");
        note.setNotes("11");
        note.setCustom1("Dialoq TESTAM");
        note.setCustom2("Dialoq TESTAM");
        mListNote.add(note);

        List<RInjectionEntity> mListInject = new ArrayList<>();
        RInjectionEntity itemInject = new RInjectionEntity();
        RealmList<RInjectionItem> listRItem = new RealmList<>();
        RInjectionItem listRInjectItem = new RInjectionItem();
        listRInjectItem.setReportedDate("2019-05-09T06:10:05+0000");
        listRInjectItem.setAssessmentType("Injections");
        listRInjectItem.setCustom3("6");

        listRItem.add(listRInjectItem);
        itemInject.setInjectionItems(listRItem);
        RInjectionItem device = new RInjectionItem();
        device.setDeviceId("ABC");
        itemInject.setDevice(device);

        mListInject.add(itemInject);
        Mockito.when(CADBManager.getInstance().getAllInjectionItem()).thenReturn(mListInject);
        Mockito.when(CADBManager.getInstance().getAllNote()).thenReturn(mListNote);
        Mockito.when(CADBManager.getInstance().getAllNNDMEntity()).thenReturn(mListNNDM);
        Mockito.when(CADBManager.getInstance().getAllRDoseEntity(CommonUtil.INJECTION_ASSESSMENT_TYPE)).thenReturn(mListData);
        Mockito.when(CADBManager.getInstance().getAllRDoseEntity(CommonUtil.OTHERS_ASSESSMENT_TYPE)).thenReturn(new ArrayList<>());
        Mockito.when(CADBManager.getInstance().getAllRDoseEntity(CommonUtil.DEVICE_ASSESSMENT_TYPE)).thenReturn(mListData2);
    }

    @Test
    public void loadOffline() {
        mockDataLoading();
        presenter.loadOffline();
    }


    @Test
    public void handleSuccessNNDMSystemIdForDecryption() {
        mockDataLoading();
        presenter.handleSuccess("  [\n" +
                "    {\"systemId\":\"ABC0042354543\",\"decryptionKey\":\"5743594ABC39475903485043\"},\n" +
                "    {\"systemId\":\"ABC0042354543\",\"decryptionKey\":\"5743594ABC39475903485043\"}\n" +
                "  ]", M025SubDoseLogPresenter.KEY_API_NNDM_SYSTEM_ID_GET_DECRYPTION);
    }

    @Test
    public void initTime() {
        presenter.initTime(true);
    }

    @Test
    public void doAllAS05GetResultSuccess() {
        presenter.initTime(true);
        CommonUtil.getInstance().savePrefContent(SyncScheduleService.KEY_APP_NOTE_ADD_DELETE, "true");

        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS05")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS05")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS05")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS05")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS05")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS05")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS05")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS05")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS05")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS5_Devices")).setResponseCode(200));
        mApp.getStorageCommon().getNNMDDecryptionKey().clear();

        DeviceEntity itemDevice = new DeviceEntity();
        itemDevice.setDeviceID("DevA");
        itemDevice.setFlagCode("2048");
        itemDevice.setTresiba(true);
        itemDevice.setAssessmentId("1245");
        itemDevice.setDrugCode(6);
        itemDevice.setSystemId("kkhfdkjfhsdkjfdks");
        itemDevice.setResultId("1449581a-b1a9-4aaa-8958-1ab1a98cce77");
        itemDevice.setNotes("11");

        mockDataLoading();
        mApp.getStorageCommon().setM025ListDevices(new ArrayList<>());
        mApp.getStorageCommon().getM025ListDevices().add(itemDevice);
        mApp.getStorageCommon().getM025ListDevices().add(itemDevice);
        mApp.getStorageCommon().getM025ListDevices().add(itemDevice);
        mApp.getStorageCommon().getM025ListDevices().add(itemDevice);
        presenter.doAllAS05GetResult();
    }

    @Test
    public void doAllAS05GetResultFailed() {
        presenter.initTime(true);
        CommonUtil.getInstance().savePrefContent(SyncScheduleService.KEY_APP_NOTE_ADD_DELETE, "true");
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS05_Fail")).setResponseCode(400));
        mockDataLoading();
        presenter.doAllAS05GetResult();
    }

    @Test
    public void getListDataInfo() {
        presenter.getListDataInfo();
    }

    @Test
    public void loadMore() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS05")).setResponseCode(200));
        presenter.loadMore();
    }

    @Test
    public void prepareDoseDataInfo() {
        DataInfo data = new DataInfo();
        data.setActivityDate("2019-05-09T06:10:34+0000");
        data.setActivityId("3980751851473750247");
        data.setActivityStatus("Taken");
        data.setAdherenceStatus("Adhered");
        data.setName("ABC");
        data.setScheduleId("89327489234");
        ResultInfo info = new ResultInfo();
        info.setResultId("2449581a-b1a9-4cce-8958-1ab1a98cce77");
        info.setAssessmentType("Injections");
        info.setResult(10);
        info.setReportedDate("2020-01-07T02:00:24+0000");
        info.setNotes("3");
        info.setCustom1("Dialoq TESTAM");
        info.setCustom2("1578370469491");
        info.setCustom3("3");
        info.setCustom4("2048");
        data.setResult(info);

        presenter.getListDataInfo().add(data);
        presenter.prepareDoseDataInfo();
    }

    @Test
    public void resetData() {
        presenter.resetData();
    }

    @Override
    public void dataAS05Ready() {
        Assert.assertTrue(true);
        CommonUtil.wtfi(TAG, "dataAS06Ready-Test DONE");
    }

    @Override
    public void closeRefresh() {
        Assert.assertTrue(true);
    }

    @Override
    public void toIncompleteDoseDetail(Object tag) {
        Assert.assertTrue(true);
    }

    @Override
    public void toDetailNote(Object tag) {
        Assert.assertTrue(true);
    }

    @Override
    public void refreshData() {
        Assert.assertTrue(true);
    }

    @Override
    public void reloadData() {
        Assert.assertTrue(true);
    }

    @Override
    public void setReminderNotification(String minDate, String maxDate) {
        Assert.assertTrue(true);
    }
}