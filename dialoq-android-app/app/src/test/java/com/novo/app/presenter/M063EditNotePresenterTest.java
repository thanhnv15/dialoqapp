package com.novo.app.presenter;

import com.novo.app.manager.CADBManager;
import com.novo.app.manager.entities.RDoseEntity;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.ResultInfo;
import com.novo.app.view.event.OnM063EditNoteCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class M063EditNotePresenterTest extends BaseTest<M063EditNotePresenter> implements OnM063EditNoteCallBack {

    /**This function is prepare data for unitTest*/
    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M063EditNotePresenter(this));
    }

    /**This function is test case edit note in database fail*/
    @Test
    public void testEditNoteInDBFail(){
        DataInfo dataInfo = new DataInfo();
        ResultInfo resultInfo = Mockito.mock(ResultInfo.class);
        dataInfo.setResult(resultInfo);

        RDoseEntity note = new RDoseEntity();

        Mockito.when(CADBManager.getInstance().editRNoteEntity(note)).thenReturn(CADBManager.EDIT_FAILED);
        presenter.editNoteInDB("", dataInfo,"2019-05-09T08:10:05+0000");
    }

    /**This function is test case edit note in database success*/
    @Test
    public void testEditNoteInDBSuccess(){
        DataInfo dataInfo = new DataInfo();
        ResultInfo resultInfo = Mockito.mock(ResultInfo.class);
        dataInfo.setResult(resultInfo);

        RDoseEntity note = new RDoseEntity();

        Mockito.when(CADBManager.getInstance().editRNoteEntity(note)).thenReturn(CADBManager.EDIT_SUCCESS);
        presenter.editNoteInDB("", dataInfo,"2019-05-09T08:10:05+0000");
    }

    /** This is last point of success thread */
    @Override
    public void refreshDataM025() {
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void showEditSuccess() {
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void showEditFailed() {
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void showEditHasBeenDeleted() {
        Assert.assertTrue(true);
    }
}