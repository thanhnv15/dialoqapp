package com.novo.app.presenter;

import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM012SignUpCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;

public class M012SignUpPresenterTest extends BaseTest implements OnM012SignUpCallBack {
    private static final String TAG = M012SignUpPresenterTest.class.getName();
    private M012SignUpPresenter m012SignUpPresenter;
    private static final String KEY_API_VERIFY_EMAIL = "KEY_API_VERIFY_EMAIL";

    @Before
    public void setUp() throws Exception{
        super.setUp();
        m012SignUpPresenter = Mockito.spy(new M012SignUpPresenter(this));
        mApp.getStorageCommon().getM001ProfileEntity().setEmail("dialoq103@yopmail.com");
    }

    @Test
    public void sendCodeUN42() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN42")).setResponseCode(200));
        m012SignUpPresenter.sendCodeUN42();
    }

    @Test
    public void testFailCodeUN42() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN42_Fail")).setResponseCode(400));
        m012SignUpPresenter.sendCodeUN42();
    }

    @Test
    public void handleSuccess() {
        m012SignUpPresenter.handleSuccess(null,"");
        m012SignUpPresenter.handleSuccess(getTextAssetFile("UN42"),"");
    }

    @Test
    public void doFailed() {
        m012SignUpPresenter.doFailed(KEY_API_VERIFY_EMAIL, null, 0, null);
    }

    @Override
    public void showM023Login() {
        Assert.assertTrue(true);
    }

    @Override
    public void verifyEmailFail(String message) {
        Assert.assertTrue(true);
        CommonUtil.wtfi(TAG, "verifyEmailFail-Test DONE");
    }

    @Override
    public void verifyEmailSuccess(String success) {
        Assert.assertTrue(true);
        CommonUtil.wtfi(TAG, "verifyEmailSuccess-Test DONE");
    }
}