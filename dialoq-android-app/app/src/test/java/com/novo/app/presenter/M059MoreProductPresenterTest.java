package com.novo.app.presenter;

import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM059MoreProductCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;

public class M059MoreProductPresenterTest extends BaseTest implements OnM059MoreProductCallBack {
    private static final String TAG = M059MoreProductPresenterTest.class.getName();
    private static final String KEY_API_UN04_UPDATE_PATIENT = "KEY_API_UN04_UPDATE_PATIENT";
    private M059MoreProductPresenter m059MoreProductPresenter;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        m059MoreProductPresenter = Mockito.spy(new M059MoreProductPresenter(this));

        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN04"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);
    }

    @Test
    public void callIUN04UpdatePatient() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("UN04")));
        m059MoreProductPresenter.callIUN04UpdatePatient();
    }

    @Test
    public void callIUN04UpdatePatientErr400() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(400).setBody(""));
        m059MoreProductPresenter.callIUN04UpdatePatient();
    }

    @Test
    public void callIUN04UpdatePatientErr401() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(401).setBody(getTextAssetFile("UN04_Fail")));
        m059MoreProductPresenter.callIUN04UpdatePatient();
    }

    @Test
    public void prepareUN04() {
        m059MoreProductPresenter.prepareUN04(getTextAssetFile("UN04"));

    }
    @Test
    public void handleSuccess() {
        m059MoreProductPresenter.handleSuccess(KEY_API_UN04_UPDATE_PATIENT,"");
    }

    @Test
    public void doFailed() {
        m059MoreProductPresenter.doFailed(KEY_API_UN04_UPDATE_PATIENT, null, -1, null);
        m059MoreProductPresenter.doFailed(KEY_API_UN04_UPDATE_PATIENT, null, -1, getTextAssetFile("UN04"));
    }

    @Override
    public void showAlertDialog(int txtErr) {
        Assert.assertTrue(true);
    }

    @Override
    public void showAlertDialog(String txtErr) {
        Assert.assertTrue(true);
    }

    @Override
    public void updateUserChoice() {
        Assert.assertTrue(true);
    }

    @Override
    public void showLoginScreen() {
        Assert.assertTrue(true);
    }

    @Override
    public void showDialog() {
        Assert.assertTrue(true);
    }

    @Override
    public void stateReverse() {
        Assert.assertTrue(true);
    }
}