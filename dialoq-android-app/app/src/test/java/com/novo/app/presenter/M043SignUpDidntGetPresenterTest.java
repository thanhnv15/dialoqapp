package com.novo.app.presenter;

import com.novo.app.R;
import com.novo.app.model.entities.ResponseEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM043SignUpDidntGetCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;

public class M043SignUpDidntGetPresenterTest extends BaseTest implements OnM043SignUpDidntGetCallBack {
    public static final String TAG = M043SignUpDidntGetPresenterTest.class.getName();
    private M043SignUpDidntGetPresenter m043SignUpDidntGetPresenter;
    ResponseEntity responseEntity;

    private static final String KEY_API_VERIFY_EMAIL = "KEY_API_VERIFY_EMAIL";

    @Before
    public void setUp() throws Exception {
        super.setUp();
        m043SignUpDidntGetPresenter = Mockito.spy(new M043SignUpDidntGetPresenter(this));
        mApp.getStorageCommon().getM001ProfileEntity().setEmail("dialog103@yopmail.com");
        responseEntity = CommonUtil.generateData(ResponseEntity.class, getTextAssetFile("UN42"));

    }

    @Test
    public void sendCodeUN42() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody(getTextAssetFile("UN42")));
        m043SignUpDidntGetPresenter.sendCodeUN42();
    }
    @Test
    public void sendCodeUN42Fail() {
        mockWebServer.enqueue(new MockResponse().setResponseCode(820).setBody(getTextAssetFile("UN42_Fail")));
        m043SignUpDidntGetPresenter.sendCodeUN42();
    }

    @Test
    public void handleSuccessNull() {
        m043SignUpDidntGetPresenter.handleSuccess(null,"");
        showAlertDialog(R.string.txt_token_err);
        showM013SignUpCode();
    }

    @Test
    public void handleSuccessNotNull() {
        m043SignUpDidntGetPresenter.handleSuccess(getTextAssetFile("UN42"),"");
        assert responseEntity != null;
        verifyEmailSuccess(responseEntity.getSuccess());
    }

    @Test
    public void doFailed() {
        m043SignUpDidntGetPresenter.doFailed(KEY_API_VERIFY_EMAIL, null, -1, null);
        m043SignUpDidntGetPresenter.doFailed(KEY_API_VERIFY_EMAIL, null, -1, getTextAssetFile("UN42_Fail"));
    }

    @Override
    public void showAlertDialog(int txtErr) {
        Assert.assertTrue(true);
    }

    @Override
    public void showAlertDialog(String txtErr) {
        Assert.assertTrue(true);
    }

    @Override
    public void showM013SignUpCode() {
        Assert.assertTrue(true);
    }

    @Override
    public void verifyEmailSuccess(String success) {
        Assert.assertTrue(true);
        CommonUtil.wtfi(TAG, "verifyEmailSuccess-Test DONE");
    }

    @Override
    public void verifyEmailFail(String message) {
        Assert.assertTrue(true);
        CommonUtil.wtfi(TAG, "verifyEmailFail-Test DONE");
    }
}