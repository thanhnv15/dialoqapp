package com.novo.app.presenter;

import android.os.Handler;

import androidx.test.core.app.ApplicationProvider;

import com.novo.app.CAApplication;
import com.novo.app.manager.CADBManager;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.utils.CommonUtil;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import okhttp3.mockwebserver.MockWebServer;

@Config(manifest = Config.NONE, sdk = 23, application = CAApplication.class)
@RunWith(CARoboticTestRunner.class)
public class BaseTest<T extends BasePresenter> {

    protected CAApplication mApp;
    protected T presenter;

    MockWebServer mockWebServer;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        CADBManager.setInstance(Mockito.spy(CADBManager.class));

        mApp = ApplicationProvider.getApplicationContext();
        CommonUtil.getInstance().savePrefContent(mApp, CommonUtil.TOKEN_KEY, "MockToken");

        mockWebServer = new MockWebServer();
        mockWebServer.start();

        CommonUtil.mDebugType = CommonUtil.LOG_TYPE.TEST;
        CommonUtil.BASE_URL = mockWebServer.url("/").toString();
    }

    @After
    public void tearDown() throws Exception {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    mockWebServer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 1000);
//        mockWebServer.close();
    }

    String getTextAssetFile(String fileName) {
        try {
            InputStream in = BaseTest.class.getResourceAsStream("/unitest/" + fileName);
            byte[] buff = new byte[1024];
            int len;
            StringBuilder text = new StringBuilder();
            while ((len = in.read(buff)) > 0) {
                text.append(new String(buff, 0, len));
            }
            in.close();
            return text.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected String getAssessmentId(String assessmentType) {
        CarePlantUserEntity entity = mApp.getStorageCommon().getM001CarePlanUserEntity();
        String assessmentId = "";

        for (int i = 0; i < entity.getData().size(); i++) {
            List<CarePlantUserEntity.AssessmentInfo> listAssessment = entity.getData().get(i).getAssessments();
            for (int j = 0; j < listAssessment.size(); j++) {
                if (listAssessment.get(j).getType().equals(assessmentType)) {
                    assessmentId = listAssessment.get(j).getAssessmentId();
                    return assessmentId;
                }
            }
        }
        return assessmentId;
    }
}
