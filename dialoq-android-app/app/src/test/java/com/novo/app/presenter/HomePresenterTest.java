package com.novo.app.presenter;

import com.novo.app.manager.CADBManager;
import com.novo.app.manager.entities.RReminderEntity;
import com.novo.app.manager.entities.RTokenEntity;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.ConfigEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnHomeBackToView;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Locale;

import okhttp3.mockwebserver.MockResponse;

public class HomePresenterTest extends BaseTest<HomePresenter> implements OnHomeBackToView {

    private ConfigEntity configEntity;
    private RTokenEntity entity;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new HomePresenter(this));

        //Dum data
        entity = new RTokenEntity();
        entity.setUserName("mm22@yopmail.com");
        entity.setPassword("Admin@12");
        entity.setConfigSet(getTextAssetFile("ConfigSet"));
        entity.setDataCP01(getTextAssetFile("CP01"));
        entity.setDataCP02(getTextAssetFile("CP02"));
        entity.setDataCP03(getTextAssetFile("CP03"));
        entity.setDataIA02(getTextAssetFile("IA02"));
        entity.setDataUN07(getTextAssetFile("UN7"));
        entity.setDataUN09(getTextAssetFile("UN09"));

        configEntity = presenter.generateData(ConfigEntity.class, entity.getConfigSet());
        configEntity.loadConfigSet();

        mApp.getStorageCommon().setM007UserEntity(presenter.generateData(UserEntity.class, entity.getDataUN07()));
        mApp.getStorageCommon().setUserCountryEntity(configEntity.getCountryEntity().get(0));
        mApp.getStorageCommon().setM001CarePlanUserEntity(presenter.generateData(CarePlantUserEntity.class, entity.getDataCP03()));
    }

    @Test
    public void getReminder() {
        RReminderEntity reminder = initReminderEntity();
        Mockito.when(CADBManager.getInstance().getRReminderEntity()).thenReturn(reminder);
        presenter.getReminder();
    }

    private RReminderEntity initReminderEntity() {
        String time = String.format(Locale.getDefault(), "%02d:%02d", 8, 30);
        String startDate = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, 1);
        String endDate = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, 3650);

        RReminderEntity reminder = new RReminderEntity();
        reminder.setScheduleId(System.currentTimeMillis() + "");
        reminder.setScheduleName(System.currentTimeMillis() + "");
        reminder.setFreqType("DAILY");
        reminder.setFreqInterval("");
        reminder.setFreqSubdayType("SPECIFIED_TIME");
        reminder.setStartTime(time);
        reminder.setStartDate(startDate);
        reminder.setEndDate(endDate);
        reminder.setScheduleNotificationId("");
        reminder.setNotificationChannel(CommonUtil.REMINDER_ACTIVE);
        reminder.setNotificationType("REMIND_TAKE_MEDICATION");
        reminder.setReminderTime("300");
        reminder.setStatusFlag(RReminderEntity.STATE_EDIT);
        return reminder;
    }


    @Test
    public void getConfigSet() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("ConfigSet")).setResponseCode(200));
        presenter.getConfigSet();
    }



    @Test
    public void filterLanguage() {
        CommonUtil.getInstance().savePrefContent(mApp, CommonUtil.LANG_KEY, "en");
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("Language_English")).setResponseCode(200));
        presenter.filterLanguage();
    }


    @Test
    public void cancelReminder() {
        RReminderEntity reminder = initReminderEntity();
        mApp.getStorageCommon().setM065ScheduleEntity(reminder);

        Mockito.when(CADBManager.getInstance().getRReminderEntity()).thenReturn(reminder);
        Mockito.when(CADBManager.getInstance().getRReminderEntity()).thenReturn(reminder);
        Mockito.when(CADBManager.getInstance().editRReminderEntity(reminder)).thenReturn(true);

        presenter.cancelReminder();
    }

    @Test
    public void checkForAdultUser() {
        presenter.checkForAdultUser();
    }


    @Test
    public void prepareConfigSet() {
        presenter.prepareConfigSet(entity.getConfigSet());
    }

    @Test
    public void checkBlockApp() {
        presenter.checkBlockApp();
    }

    @Test
    public void loadingLoginOff() {
        mApp.getStorageCommon().setM001ConfigSet(configEntity);

        Mockito.when(CADBManager.getInstance().getRTokenEntity()).thenReturn(entity);
        presenter.loadingLoginOff();
    }

    @Override
    public void showFrgScreen(String tagSource, String tagChild) {
        Assert.assertTrue(true);
    }

    @Override
    public void closeApp() {
        Assert.assertTrue(true);
    }

    @Override
    public void hideBottomBar() {
        Assert.assertTrue(true);
    }

    @Override
    public void loadConfigFail() {
        System.out.println("===>REACH DESTINATION: loadConfigFail");
        Assert.assertTrue(true);
    }

    @Override
    public void loadConfigSuccess(ConfigEntity configEntity) {
        System.out.println("===>REACH DESTINATION: loadConfigSuccess");
        Assert.assertTrue(true);
    }

    @Override
    public void dataLanguageReady(String data) {
        Assert.assertTrue(true);
    }

    @Override
    public void showWaringDialog(String txt, boolean isActive) {
        Assert.assertTrue(true);
    }

    @Override
    public void checkBlockApp(int isActive) {
        System.out.println("===>REACH DESTINATION: checkBlockApp");
        Assert.assertTrue(true);
    }

    @Override
    public void doTimeZoneChange() {
        Assert.assertTrue(true);
    }

    @Override
    public void unregisterTimeZoneChange() {
        Assert.assertTrue(true);
    }

    @Override
    public void pauseReminder() {
        Assert.assertTrue(true);
    }

    @Override
    public void setupReminder() {
        Assert.assertTrue(true);
    }

    @Override
    public void stopScan() {
        Assert.assertTrue(true);
    }

    @Override
    public void startScan() {
        Assert.assertTrue(true);
    }

    @Override
    public void preLoginTimeZoneChange() {
        Assert.assertTrue(true);
    }

    @Override
    public void registrationCancel(String tag) {
        Assert.assertTrue(true);
    }

    @Override
    public void showLandingFrgScreen() {
        Assert.assertTrue(true);
    }

    @Override
    public void loginSuccess() {
        System.out.println("===>REACH DESTINATION: loginSuccess");
        Assert.assertTrue(true);
    }

    @Override
    public void showNetworkAlert() {
        Assert.assertTrue(true);
    }

    @Override
    public void reloadConfigSet() {
        Assert.assertTrue(true);
    }

    @Override
    public void getLanguageError() {
        Assert.assertTrue(true);
    }

    @Override
    public void downloadIntroVideo(String url) {

    }

    @Override
    public void backToPreviousScreen() {
        Assert.assertTrue(true);
    }

    @Override
    public void showLockDialog() {
        Assert.assertTrue(true);
    }

    @Override
    public void showAlertDialog(String txtErr) {
        Assert.assertTrue(true);
    }

    @Override
    public void showAlertDialog(int txtErr) {
        Assert.assertTrue(true);
    }

    @Override
    public void onLinkClicked() {
        Assert.assertTrue(true);
    }

    @Override
    public void showM001LandingScreen(String err) {
        Assert.assertTrue(true);
    }

    @Override
    public void dateSet() {
        Assert.assertTrue(true);
    }

    @Override
    public void syncSuccess() {
        Assert.assertTrue(true);
    }

    @Override
    public void syncFailed() {
        Assert.assertTrue(true);
    }
}