package com.novo.app.presenter;

import com.novo.app.utils.CommonUtil;

import org.junit.runners.model.InitializationError;
import org.robolectric.RobolectricTestRunner;

public class CARoboticTestRunner extends RobolectricTestRunner {

    public CARoboticTestRunner(Class<?> testClass) throws InitializationError {
        super(testClass);
        CommonUtil.mDebugType = CommonUtil.LOG_TYPE.TEST;
    }
}
