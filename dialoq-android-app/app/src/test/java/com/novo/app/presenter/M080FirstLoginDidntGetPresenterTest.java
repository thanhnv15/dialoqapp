package com.novo.app.presenter;

import com.novo.app.model.entities.TextEntity;
import com.novo.app.view.event.OnM080FirstLoginDidntGetCallBack;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;


public class M080FirstLoginDidntGetPresenterTest extends BaseTest<M080FirstLoginDidntGetPresenter> implements OnM080FirstLoginDidntGetCallBack {

    /**
     * This function prepare data for unitTest
     */
    @Override
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M080FirstLoginDidntGetPresenter(this));
        mApp.getStorageCommon().setM075UserLoginInfo(new TextEntity("mm2@yopmail.com", "Admin@12"));
    }

    /**
     * This function call IA04 reset password step1 and return success code 200.
     */
    @Test
    public void testRequestCodeIA04() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA04")).setResponseCode(200));
        presenter.requestCodeIA04();
    }

    /**
     * This function call IA04 reset password step1 and return code 200 but with a corrupt data that
     * that make response body become null.
     */
    @Test
    public void testRequestCodeIA04Null() {
        mockWebServer.enqueue(new MockResponse().setBody("null").setResponseCode(200));
        presenter.requestCodeIA04();
    }

    /**
     * This function call IA04 reset password step1 and return error code 400
     */
    @Test
    public void testRequestCodeIA04Err400() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA04_400")).setResponseCode(400));
        presenter.requestCodeIA04();
    }

    /**This function call IA04 reset password step1 and return error code 401*/
    @Test
    public void testRequestCodeIA04Err401() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA04_400")).setResponseCode(401));
        presenter.requestCodeIA04();
    }

    /** This is last point of success case but receive corrupted data*/
    @Override
    public void showM077FirstLoginCode() {
        Assert.assertTrue(true);
    }

    /** This is last point of success cases */
    @Override
    public void sendCodeSuccess() {
        Assert.assertTrue(true);
    }

    /** This is last point of success case */
    @Override
    public void sendCodeFailed(String message) {
        Assert.assertTrue(true);
    }
}