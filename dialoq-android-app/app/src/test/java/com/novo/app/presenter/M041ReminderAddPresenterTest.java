package com.novo.app.presenter;

import com.novo.app.manager.CADBManager;
import com.novo.app.manager.entities.RReminderEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM041ReminderAddCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Locale;

public class M041ReminderAddPresenterTest extends BaseTest<M041ReminderAddPresenter> implements OnM041ReminderAddCallBack {

    private String time;
    private RReminderEntity entity;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M041ReminderAddPresenter(this));

        //Dum data
        time = String.format(Locale.getDefault(), "%02d:%02d", 8, 30);
        String startDate = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, 1);
        String endDate = CommonUtil.getDateNow(CommonUtil.DATE_STYLE, 3650);
        entity = new RReminderEntity();
        entity.setScheduleId(System.currentTimeMillis() + "");
        entity.setScheduleName(System.currentTimeMillis() + "");
        entity.setFreqType("DAILY");
        entity.setFreqInterval("");
        entity.setFreqSubdayType("SPECIFIED_TIME");
        entity.setStartTime(time);
        entity.setStartDate(startDate);
        entity.setEndDate(endDate);
        entity.setScheduleNotificationId("");
        entity.setNotificationChannel(CommonUtil.REMINDER_ACTIVE);
        entity.setNotificationType("REMIND_TAKE_MEDICATION");
        entity.setReminderTime("300");
        entity.setStatusFlag(RReminderEntity.STATE_ADD);
    }

    @Test
    public void createReminder() {
        Mockito.when(CADBManager.getInstance().getRReminderEntity()).thenReturn(entity);
        Mockito.when(CADBManager.getInstance().addRReminderEntity(entity)).thenReturn(true);

        presenter.addReminder(time);
    }

    @Test
    public void editReminder() {
        Mockito.when(CADBManager.getInstance().editRReminderEntity(entity)).thenReturn(true);
        presenter.activeReminder();
    }

    @Override
    public void reminderAdded() {
        Assert.assertTrue(true);
    }
}