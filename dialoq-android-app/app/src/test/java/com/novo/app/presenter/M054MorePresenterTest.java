package com.novo.app.presenter;

import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM054MoreCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class M054MorePresenterTest extends BaseTest implements OnM054MoreCallBack {
    public static final String TAG = M054MorePresenter.class.getName();
    private static final String KEY_API_UN04_UPDATE_PATIENT = "KEY_API_UN04_UPDATE_PATIENT";
    private M054MorePresenter m054MorePresenter;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        m054MorePresenter = Mockito.spy(new M054MorePresenter(this));
        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN04"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);
    }

    @Test
    public void handleSuccess() {
        m054MorePresenter.handleSuccess(KEY_API_UN04_UPDATE_PATIENT,"");
    }

    @Test
    public void doFailed() {
        m054MorePresenter.doFailed(KEY_API_UN04_UPDATE_PATIENT, null, -1, null);
        m054MorePresenter.doFailed(KEY_API_UN04_UPDATE_PATIENT, null, -1, getTextAssetFile("UN42_Fail"));
    }

    @Override
    public void showAlertDialog(int txtErr) {
        Assert.assertTrue(true);
    }

    @Override
    public void showAlertDialog(String txtErr) {
        Assert.assertTrue(true);
    }

    @Override
    public void showDialog() {
        Assert.assertTrue(true);
    }

    @Override
    public void updateSuccess() {
        Assert.assertTrue(true);
    }

    @Override
    public void updateFailed() {
        Assert.assertTrue(true);
    }
}