package com.novo.app.presenter;

import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM042MoreCallBack;
import com.novo.app.view.widget.NVDateUtils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import okhttp3.mockwebserver.MockResponse;

public class M042MorePresenterTest extends BaseTest<M042MorePresenter> implements OnM042MoreCallBack {
    private static final String TAG = M042MorePresenterTest.class.getName();
    private static final String KEY_API_AS05_GET_RESULT = "KEY_API_AS05_GET_RESULT";
    private static final String KEY_API_AS23_DELETE_RESULT = "KEY_API_AS23_DELETE_RESULT";

    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M042MorePresenter(this));

        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN04"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);

        CarePlantUserEntity carePlantUserEntity = CommonUtil.generateData(CarePlantUserEntity.class, getTextAssetFile("CP03"));
        mApp.getStorageCommon().setM001CarePlanUserEntity(carePlantUserEntity);


        String lastPairNative = CommonUtil.getDateNow(CommonUtil.DATE_STYLE);
        String lastPair = NVDateUtils.getGroupItemDateTitle(NVDateUtils.TODAY_AT, NVDateUtils.YESTERDAY_AT, CommonUtil.getDateNow(CommonUtil.DATE_STYLE));

        List<DeviceEntity> deviceList = new ArrayList<>();
        deviceList.add(new DeviceEntity("", "10", "000132",
                DeviceEntity.CODE_FIASP,
                "",
                lastPair,
                lastPairNative));
        mApp.getStorageCommon().setM025ListDevices(deviceList);
    }


    @Test
    public void callAS23DeleteAssessmentResult() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS23")).setResponseCode(200));
        presenter.callAS23DeleteAssessmentResult(new DeviceEntity());
    }


    @Override
    public void showDeviceDetail() {
        Assert.assertTrue(true);
    }

    @Override
    public void showTroubleShooting() {
        Assert.assertTrue(true);
    }

    @Override
    public void updateList() {
        Assert.assertTrue(true);
    }

    @Override
    public void closeDeviceDetail() {
        Assert.assertTrue(true);
    }
}