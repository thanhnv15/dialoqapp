package com.novo.app.presenter;

import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM065MoreCallBack;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;

public class M065MorePresenterTest extends BaseTest<M065MorePresenter> implements OnM065MoreCallBack {
    private static final String TAG = M065AddEditPresenterTest.class.getName();

    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M065MorePresenter(this));

        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN04"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);

        CarePlantUserEntity carePlantUserEntity = CommonUtil.generateData(CarePlantUserEntity.class, getTextAssetFile("CP03"));
        mApp.getStorageCommon().setM001CarePlanUserEntity(carePlantUserEntity);
    }

    @Test
    public void callAS10UpdateAssessmentSchedule() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("AS10")).setResponseCode(200));
        presenter.activeReminder("21:30", CommonUtil.REMINDER_INACTIVE);
    }
}