package com.novo.app.presenter;

import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.DataInfo;
import com.novo.app.model.entities.ResultEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM068ShareCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class M068SharePresenterTest extends BaseTest implements OnM068ShareCallBack {

    private static final String KEY_API_SYNC_RESULT = "KEY_API_SYNC_RESULT";
    private static final int KEY_LAST_7_DAYS = 0;
    private static final int KEY_LAST_14_DAYS = 1;
    private M068SharePresenter m068SharePresenter;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        m068SharePresenter = Mockito.spy(new M068SharePresenter(this));

        DataInfo dataInfo = CommonUtil.generateData(DataInfo.class, getTextAssetFile("AS05"));
        mApp.getStorageCommon().setM025ResultDataItem(dataInfo);

        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN04"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);

        CarePlantUserEntity carePlantUserEntity = CommonUtil.generateData(CarePlantUserEntity.class, getTextAssetFile("CP03"));
        mApp.getStorageCommon().setM001CarePlanUserEntity(carePlantUserEntity);
    }

    @Test
    public void handleSuccess() {
        m068SharePresenter.handleSuccess("","");
    }

    @Test
    public void doAllAS05GetResult() {
        m068SharePresenter.doAllAS05GetResult();
    }

    @Test
    public void prepareAS05() {
        m068SharePresenter.prepareAS05(null);
        m068SharePresenter.prepareAS05(getTextAssetFile("AS05"));
    }

    @Test
    public void initCondition() {
        m068SharePresenter.initCondition(KEY_LAST_7_DAYS, true);
        m068SharePresenter.initCondition(KEY_LAST_14_DAYS, true);
    }

    @Test
    public void callAS05GetResult() {
        m068SharePresenter.callAS05GetResult(1);

    }

    @Test
    public void doFailed() {
        m068SharePresenter.doFailed(KEY_API_SYNC_RESULT, null, -1, "");
    }

    @Override
    public void dataAS05Ready() {
        Assert.assertTrue(true);
    }
}