package com.novo.app.presenter;

import com.novo.app.model.entities.TextEntity;
import com.novo.app.view.event.OnM078FirstLoginCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;

public class M078FirstLoginPresenterTest extends BaseTest<M078FirstLoginPresenter> implements OnM078FirstLoginCallBack {
    private String CORRECT_CODE = "0123";
    private String INCORRECT_CODE = "0000";

    /**
     * This function prepare data for unitTest
     */
    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M078FirstLoginPresenter(this));
        mApp.getStorageCommon().setM075UserLoginInfo(new TextEntity("mm2@yopmail.com", "Admin@12"));
    }

    /**This function call IA04 reset password step2 for success case*/
    @Test
    public void testRequestCodeIA04() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA04")).setResponseCode(200));
        presenter.callM078ValidateCode(CORRECT_CODE);
    }

    /**This function call IA04 reset password step2 for success case*/
    @Test
    public void testRequestCodeIA04Null() {
        mockWebServer.enqueue(new MockResponse().setBody("null").setResponseCode(200));
        presenter.callM078ValidateCode(CORRECT_CODE);
    }

    /**This function call IA04 reset password step2 for fail case by receive response code as 400*/
    @Test
    public void testRequestCodeIA04Err400() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA04_400")).setResponseCode(400));
        presenter.callM078ValidateCode(INCORRECT_CODE);
    }

    /**This function call IA04 reset password step2 for fail case by receive response code as 401*/
    @Test
    public void testRequestCodeIA04Err() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA04_400")).setResponseCode(401));
        presenter.callM078ValidateCode(CORRECT_CODE);
    }

    /** This is last point of fail cases */
    @Override
    public void verifyCodeFail(String message) {
        Assert.assertTrue(true);
    }

    /** This is last point of success case */
    @Override
    public void verifyCodeSuccess() {
        Assert.assertTrue(true);
    }
}