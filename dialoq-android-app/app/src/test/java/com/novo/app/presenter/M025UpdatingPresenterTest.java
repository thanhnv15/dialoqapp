package com.novo.app.presenter;

import com.novo.app.ble.BLEInfoEntity;
import com.novo.app.ble.DoseItem;
import com.novo.app.model.entities.CarePlantUserEntity;
import com.novo.app.model.entities.DeviceEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM025UpdatingCallBack;
import com.novo.app.view.widget.NVDateUtils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class M025UpdatingPresenterTest extends BaseTest<M025UpdatingPresenter> implements OnM025UpdatingCallBack {
    public static final String TAG = M025UpdatingPresenterTest.class.getName();
    private BLEInfoEntity bleInfoEntity = new BLEInfoEntity();

    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M025UpdatingPresenter(this));

        UserEntity userEntity = CommonUtil.generateData(UserEntity.class, getTextAssetFile("UN04"));
        mApp.getStorageCommon().setM007UserEntity(userEntity);

        CarePlantUserEntity carePlantUserEntity = CommonUtil.generateData(CarePlantUserEntity.class, getTextAssetFile("CP03"));
        mApp.getStorageCommon().setM001CarePlanUserEntity(carePlantUserEntity);

        List<DoseItem> doseItemList = new ArrayList<>();
        doseItemList.add(new DoseItem(0, 0, System.currentTimeMillis(), -1, 1));
        bleInfoEntity.setDoseItems(doseItemList);

        DeviceEntity deviceEntity = CommonUtil.generateData(DeviceEntity.class, getTextAssetFile("PairDevice"));
        mApp.getStorageCommon().setM042CurrentItem(deviceEntity);

        String lastPairNative = mApp.getStorageCommon().getM042CurrentItem().getLastPairNative();
        String lastPair = NVDateUtils.getGroupItemDateTitle(NVDateUtils.TODAY_AT, NVDateUtils.YESTERDAY_AT, CommonUtil.getDateNow(CommonUtil.DATE_STYLE));

        List<DeviceEntity> deviceList = new ArrayList<>();
        deviceList.add(new DeviceEntity("", "", "000132",
                DeviceEntity.CODE_FIASP,
                "",
                lastPair,
                lastPairNative));
        mApp.getStorageCommon().setM032PairedDevice(deviceList);
    }

    @Test
    public void handleSuccess() {
        presenter.handleSuccess("","");
    }

    @Test
    public void doFailed() {
        presenter.doFailed(TAG, null, 0, "");
    }

    @Override
    public void syncSuccess() {
        Assert.assertTrue(true);
    }

    @Override
    public void syncFailed() {
        Assert.assertTrue(true);
    }
}