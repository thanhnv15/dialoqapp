package com.novo.app.presenter;

import com.novo.app.manager.CADBManager;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM062AddNoteCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class M062AddNotePresenterTest extends BaseTest<M062AddNotePresenter> implements OnM062AddNoteCallBack {
    private static final String TAG = M062AddNotePresenterTest.class.getName();

    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M062AddNotePresenter(this));
    }

    /**This function add new note to data base success*/
    @Test
    public void addNewNoteToDBSuccess() {
        presenter.addNewNoteToDB(CommonUtil.NOTES_ASSESSMENT_TYPE, "Notes","2019-05-09T08:10:05+0000");
    }

    /**This function add new note to data base failed*/
    @Test
    public void addNewNoteToDBFailed() {
        Mockito.when(CADBManager.getInstance().addRDoseEntity(null, CommonUtil.NOTES_ASSESSMENT_TYPE)).thenReturn(false);
        presenter.addNewNoteToDB(CommonUtil.NOTES_ASSESSMENT_TYPE, "Notes","2019-05-09T08:10:05+0000");
    }

    /** This is last point of success thread */
    @Override
    public void showLockDialog() {
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void showDoseLog(String data) {
        Assert.assertTrue(true);
    }

    /** This is last point of success thread */
    @Override
    public void showAddNoteFail() {
        Assert.assertTrue(true);
    }
}