package com.novo.app.presenter;

import com.novo.app.model.entities.TextEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM076FirstLoginCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import okhttp3.mockwebserver.MockResponse;

public class M076FirstLoginPresenterTest extends BaseTest<M076FirstLoginPresenter> implements OnM076FirstLoginCallBack {
    public static final String TAG = M076FirstLoginPresenterTest.class.getName();

    /**
     * This function prepare data for unitTest
     */
    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M076FirstLoginPresenter(this));
        mApp.getStorageCommon().setM075UserLoginInfo(new TextEntity("mm2@yopmail.com", "Admin@12"));
    }

    /**
     * This function call IA04 reset password step1 and return success code 200.
     */
    @Test
    public void testRequestCodeIA04() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA04")).setResponseCode(200));
        presenter.requestCodeIA04();
    }

    /**
     * This function call IA04 reset password step1 and return code 200 but with a corrupt data that
     * that make response body become null.
     */
    @Test
    public void testRequestCodeIA04Null() {
        mockWebServer.enqueue(new MockResponse().setBody("null").setResponseCode(200));
        presenter.requestCodeIA04();
    }

    /**
     * This function call IA04 reset password step1 and return error code 400
     */
    @Test
    public void testRequestCodeIA04Err400() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA04_400")).setResponseCode(400));
        presenter.requestCodeIA04();

    }

    /**This function call IA04 reset password step1 and return error code 401*/
    @Test
    public void testRequestCodeIA04Err401() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA04_400")).setResponseCode(401));
        presenter.requestCodeIA04();
    }

    /** This is last point of success case but receive corrupted data*/
    @Override
    public void showM023Login() {
        Assert.assertTrue(true);
    }

    /** This is last point of failed cases */
    @Override
    public void verifyEmailFail(String message) {
        Assert.assertTrue(true);
        CommonUtil.wtfi(TAG, "happened when doFail");
    }

    /** This is last point of fail cases */
    @Override
    public void verifyEmailSuccess(String success) {
        Assert.assertTrue(true);
        CommonUtil.wtfi(TAG, "verifyEmail-Test DONE");
    }
}