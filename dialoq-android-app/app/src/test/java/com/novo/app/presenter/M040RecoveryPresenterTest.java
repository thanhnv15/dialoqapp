package com.novo.app.presenter;

import com.novo.app.model.entities.AcceptDeviceEntity;
import com.novo.app.model.entities.BlacklistCountryEntity;
import com.novo.app.model.entities.ConfigEntity;
import com.novo.app.model.entities.CountryEntity;
import com.novo.app.model.entities.CountryVersionEntity;
import com.novo.app.model.entities.TextEntity;
import com.novo.app.model.entities.UserEntity;
import com.novo.app.utils.CommonUtil;
import com.novo.app.view.event.OnM040RecoveryCallBack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import okhttp3.mockwebserver.MockResponse;

public class M040RecoveryPresenterTest extends BaseTest<M040RecoveryPresenter> implements OnM040RecoveryCallBack {
    public static final String TAG = M040RecoveryPresenterTest.class.getName();
    private static final String KEY_API_CP03_GET_CARE_PLAN_USER = "KEY_API_CP03_GET_CARE_PLAN_USER";
    private List<CountryEntity> countryEntity;
    private List<CountryVersionEntity> mListCountryVersionEntitie;

    /**
     * This function prepare data for unitTest
     */
    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = Mockito.spy(new M040RecoveryPresenter(this));

        ConfigEntity configEntity = CommonUtil.generateData(ConfigEntity.class, getTextAssetFile("ConfigSet"));
        mApp.getStorageCommon().setM001ConfigSet(configEntity);

        AcceptDeviceEntity acceptDeviceEntity = new AcceptDeviceEntity();
        acceptDeviceEntity.setCountries("vn=vietnam");
        mApp.getStorageCommon().getM001ConfigSet().setBlackList(acceptDeviceEntity);

        List<BlacklistCountryEntity> blacklistCountryEntity = new ArrayList<>();
        blacklistCountryEntity.add(0, new BlacklistCountryEntity(new TextEntity("cn", "China")));
        mApp.getStorageCommon().getM001ConfigSet().setBlacklistCountryEntity(blacklistCountryEntity);

        UserEntity userEntity = new UserEntity();
        userEntity.setUserId("");
        userEntity.setCountry("");
        userEntity.setFamilyHistory("");
        mApp.getStorageCommon().setM007UserEntity(userEntity);

        countryEntity = new ArrayList<>();
        countryEntity.add(0, new CountryEntity(new TextEntity("code", "cn"),
                new TextEntity("name", "China"), new TextEntity("term.adult.key", "DK.TERM.ADULT"),
                new TextEntity("term.adult.version", "1"),
                new TextEntity("term.adult.updated", "2019-08-05"),
                new TextEntity("term.children.key", "DK.TERM.CHILDREN"),
                new TextEntity("term.children.version", "2"),
                new TextEntity("term.children.updated", "2019-08-05"),
                new TextEntity("age", "13"),
                new TextEntity("minimum.using.app.age", "10")));
        countryEntity.add(1, new CountryEntity(
                new TextEntity("code", "se"),
                new TextEntity("name", "Sweden"),
                new TextEntity("term.adult.key", "GENERAL.TERMS.AND.CONDITIONS"),
                new TextEntity("term.adult.version", "1"),
                new TextEntity("term.adult.updated", "2019-08-05"),
                new TextEntity("term.children.key", "GENERAL.TERMS.AND.CONDITIONS"),
                new TextEntity("term.children.version", "2"),
                new TextEntity("term.children.updated", "2019-08-05"),
                new TextEntity("age", "13"),
                new TextEntity("minimum.using.app.age", "10")));
        mApp.getStorageCommon().getM001ConfigSet().setCountryEntity(countryEntity);
        mApp.getStorageCommon().setUserCountryEntity(mApp.getStorageCommon().getM001ConfigSet().getCountryEntity().get(0));

        mListCountryVersionEntitie = new ArrayList<>();
        mListCountryVersionEntitie.add(new CountryVersionEntity(new TextEntity("", "cn"), new TextEntity("", ""), new TextEntity("", CommonUtil.RECALL_GREATER)));
        mListCountryVersionEntitie.add(new CountryVersionEntity(new TextEntity("", "cn"), new TextEntity("", ""), new TextEntity("", CommonUtil.RECALL_EQUAL)));
        mListCountryVersionEntitie.add(new CountryVersionEntity(new TextEntity("", "cn"), new TextEntity("", ""), new TextEntity("", CommonUtil.RECALL_LESS)));
        mListCountryVersionEntitie.add(new CountryVersionEntity(new TextEntity("", "cn"), new TextEntity("", ""), new TextEntity("", CommonUtil.RECALL_GREATER_OR_EQUAL)));
        mListCountryVersionEntitie.add(new CountryVersionEntity(new TextEntity("", "cn"), new TextEntity("", ""), new TextEntity("", CommonUtil.RECALL_LESS_OR_EQUAL)));
        mApp.getStorageCommon().getM001ConfigSet().setCountryVersionEntity(mListCountryVersionEntitie);

        presenter.prepareCP01(null);
    }

    /**
     * This function call IA04 Reset password step3 case success IA04
     */
    @Test
    public void callIA04ResetPasswordStep3CaseSuccessIA04() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA04")).setResponseCode(200));
        presenter.callIA04ResetPasswordStep3("", "", "");
    }

    /**
     * This function call IA02 Authenticate user success IA02
     */
    @Test
    public void callIA02AuthenticateUserSuccessIA02() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02")).setResponseCode(200));
        presenter.callIA02AuthenticateUser("", "");
    }

    /**
     * This function call IA02 Authenticate user success UN09
     */
    @Test
    public void callIA02AuthenticateUserSuccessUN09() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN09")).setResponseCode(200));
        presenter.callIA02AuthenticateUser("", "");
    }

    /**
     * This function call IA02 Authenticate user success UN07
     */
    @Test
    public void callIA02AuthenticateUserSuccessUN07() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN09")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN7")).setResponseCode(200));
        presenter.callIA02AuthenticateUser("", "");
    }

    /**
     * This function call IA02 Authenticate user success CP01
     */
    @Test
    public void callIA02AuthenticateUserSuccessCP01() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN09")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN7")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP03_204")).setResponseCode(204));
        presenter.callIA02AuthenticateUser("", "");
    }

    /**
     * This function call IA02 Authenticate user success CP02
     */
    @Test
    public void callIA02AuthenticateUserSuccessCP02() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN09")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN7")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP03_204")).setResponseCode(204));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP01")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP02")).setResponseCode(200));
        presenter.callIA02AuthenticateUser("", "");
    }

    /**
     * This function call IA02 Authenticate user success CP03
     */
    @Test
    public void callIA02AuthenticateUserSuccessCP03() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN09")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN7")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP03_204")).setResponseCode(204));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP01")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP02")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("CP03")).setResponseCode(200));
        presenter.callIA02AuthenticateUser("", "");
    }

    /**
     * This function call IA02 Authenticate user Failed 400
     */
    @Test
    public void callIA02AuthenticateUserFailed400() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02_401")).setResponseCode(400));
        presenter.callIA02AuthenticateUser("", "");
    }

    /**
     * This function call IA02 Authenticate user Failed 500
     */
    @Test
    public void callIA02AuthenticateUserFailed500() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02_401")).setResponseCode(500));
        presenter.callIA02AuthenticateUser("", "");
    }

    /**
     * This function call IA02 Authenticate user Failed 401
     */
    @Test
    public void callIA02AuthenticateUserFailed401() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02_401")).setResponseCode(401));
        presenter.callIA02AuthenticateUser("", "");
    }

    /**
     * This function call IA04 Reset password step3 case failed
     */
    @Test
    public void callIA04ResetPasswordStep3CaseFailed() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA04_400")).setResponseCode(403));
        presenter.callIA04ResetPasswordStep3("", "", "");
    }

    /**
     * This function mock go to landing page
     */
    @Test
    public void mockGoToLandingPage() {
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("IA02")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN09")).setResponseCode(200));
        mockWebServer.enqueue(new MockResponse().setBody(getTextAssetFile("UN7")).setResponseCode(200));
        mApp.getStorageCommon().getM001ConfigSet().setCountryVersionEntity(mListCountryVersionEntitie);
        presenter.callIA02AuthenticateUser("", "");
    }

    /**
     * This is last point of success thread
     */
    @Override
    public void onIA04Fail(String message) {
        Assert.assertTrue(true);
    }

    /**
     * This is last point of success thread
     */
    @Override
    public void showM001LandingScreen(String sms) {
        Assert.assertTrue(true);
    }

    /**
     * This is last point of success thread
     */
    @Override
    public void loginSuccess() {
        Assert.assertTrue(true);
    }

    /**
     * This is last point of success thread
     */
    @Override
    public void showDialog(int textCode, String TAG) {
        Assert.assertTrue(true);
    }

    /**
     * This is last point of success thread
     */
    @Override
    public void showDialog(String text, String TAG) {
        Assert.assertTrue(true);
    }

    /**
     * This is last point of success thread
     */
    @Override
    public void showAlertDialog(String txtErr) {
        Assert.assertTrue(true);
    }

    /**
     * This is last point of success thread
     */
    @Override
    public void showAlertDialog(int txtErr) {
        Assert.assertTrue(true);
    }


}