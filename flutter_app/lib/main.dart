import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/flutter_pdf_viewer.dart';

const OPTIONS = [
  'null',
  'nightMode: true',
  'enableSwipe: false',
  'annotationRendering: true',
  'swipeHorizontal: true',
  'autoSpacing: true',
  'pageFling: true',
  'pageSnap: true',
  'enableImmersive: true',
  'autoPlay: true',
  'slideshow: true',
  'XOR encrypted',
  'forceLandscape: true',
];

class PdfLoaderSection extends StatefulWidget {
  @override
  PdfLoaderSectionState createState() => new PdfLoaderSectionState();
}

class PdfLoaderSectionState extends State<PdfLoaderSection> {
  var option = 0;

  Future<void> onPressed() async {
    String prefix = option == 10 ? "xor_" : "";

    final pdfSrc = 'assets/${prefix}file.pdf';
    final key = option == 10 ? "test" : null;

    final config = PdfViewerConfig(
      nightMode: option == 1,
      enableSwipe: option != 2,
      annotationRendering: true,
      swipeHorizontal: option == 3,
      autoSpacing: option == 4,
      pageFling: option == 5,
      pageSnap: option == 6,
      enableImmersive: option == 7,
      forceLandscape: option == 11,
      slideShow: option == 9,
      xorDecryptKey: key,
      initialPage: null,
      atExit: (pageIndex) {
        print(">> atExit($pageIndex)");
      },
    );

    await PdfViewer.loadAsset(pdfSrc, config: config);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        RaisedButton(
          child: Text("Open PDF file from ASSETS folder"),
          onPressed: onPressed,
        ),
      ],
    );
  }
}

String prettyJson(map) {
  return JsonEncoder.withIndent(' ' * 2).convert(map);
}

void main() async {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          title: const Text('Flutter PDF Viewer'),
        ),
        body: new Center(
          child: PdfLoaderSection()
        ),
      ),
    );
  }
}
