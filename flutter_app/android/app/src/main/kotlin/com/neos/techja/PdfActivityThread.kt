package com.neos.techja

import android.os.Bundle
import android.view.View
import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle
import android.content.Context
import android.net.Uri
import android.util.Log
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.upstream.ByteArrayDataSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import java.io.BufferedInputStream
import java.io.DataInputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.net.Socket
import kotlin.experimental.xor


class PdfActivityThread(
        val activity: PdfActivity,
        val opts: Bundle,
        val pdfView: PDFView,
        val scrollHandle: DefaultScrollHandle,
        val initialPage: Int) : Thread() {

    val mode = opts.getString("mode")!!
    val xorDecryptKey: String? = opts.getString("xorDecryptKey")

    fun buildConfigurator(): PDFView.Configurator? {
        val src = opts.getString("src")!!

        xorDecryptKey?.let {
            val bytes = when (mode) {
                "fromAsset" -> {
                    Log.d(TAG, "loading encrypted pdf from assets { $src }...")
                    readBytesFromAsset(activity.applicationContext, src)
                }
                else -> throw IllegalArgumentException("invalid mode: $mode.")
            }
            xorEncryptDecrypt(bytes, it)
            return pdfView.fromBytes(bytes)
        }

        return when (mode) {
            "fromAsset" -> {
                Log.d(TAG, "loading pdf from assets { $src }...")
                pdfView.fromAsset(src)
            }
            else -> throw IllegalArgumentException("invalid mode: $mode.")
        }
    }

    fun xorEncryptDecrypt(bytes: ByteArray, key: String) {
        val keyAsIntList = key.map { it.toByte() }
        val keyLength = key.length
        for (i in 0 until bytes.size) {
            bytes[i] = bytes[i] xor keyAsIntList[i % keyLength]
        }
    }

    fun readBytesFromAsset(context: Context, assetPath: String): ByteArray {
        val inputStream = context.assets.open(assetPath)
        val bytes = ByteArray(inputStream.available())

        inputStream.use { stream ->
            DataInputStream(stream).use {
                it.readFully(bytes)
            }
        }

        return bytes
    }

    override fun run() {
        pdfView.visibility = View.VISIBLE
        var configurator = buildConfigurator()!!

        configurator = configurator
                .password(opts.getString("password"))
                .nightMode(opts.getBoolean("nightMode"))
                .enableSwipe(opts.getBoolean("enableSwipe"))
                .enableAnnotationRendering(true)
                .swipeHorizontal(opts.getBoolean("swipeHorizontal"))
                .autoSpacing(opts.getBoolean("autoSpacing"))
                .pageFling(opts.getBoolean("pageFling"))
                .pageSnap(opts.getBoolean("pageSnap"))
                .onError(activity)
                .onRender(activity)
                .scrollHandle(scrollHandle)
                .defaultPage(initialPage)

        if (opts.containsKey("pages")) {
            configurator = configurator.pages(*opts.getIntArray("pages"))
        }

        configurator.load()
    }
}